<?php

//error_reporting(E_ALL);
ini_set('display_errors', 1);
error_reporting( E_CORE_ERROR | E_CORE_WARNING | E_COMPILE_ERROR | E_ERROR | E_WARNING | E_PARSE | E_USER_ERROR | E_USER_WARNING | E_RECOVERABLE_ERROR );

//ABS /корень
//переменные путей
define('ABS', dirname(__FILE__));

define('MINICART', ABS . '/minicart');

define('SYSTEM_TPL', MINICART . '/templates');
define('TPL', ABS . '/templates');
define('LIBRARIES', ABS . '/libraries');

define('ORIGINAL_IMAGES_DIR', ABS . '/files/images/original/');
define('RESIZED_IMAGES_DIR', ABS . '/files/images/resized/');

require_once(ABS . '/config/config.php');
require_once(ABS . '/minicart/core/Autoloader.php');

//автолоадер composer
require_once(ABS . '/minicart/libraries/autoload.php');

//запускаем автолоадер для подгрузки классов на лету
new \core\Autoloader();



(new \core\FrontController())->init();
