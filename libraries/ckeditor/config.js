/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here.
    // For the complete reference:
    // http://docs.ckeditor.com/#!/api/CKEDITOR.config

    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        { name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
        { name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
        { name: 'links' },
        { name: 'insert' },
        { name: 'forms' },
        { name: 'colors' },
        { name: 'about' },
        { name: 'tools' },
        { name: 'others' },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
        { name: 'styles' }

    ];

    config.allowedContent = true;

    // Remove some buttons, provided by the standard plugins, which we don't
    // need to have in the Standard(s) toolbar.
    config.removeButtons = 'Underline,Subscript,Superscript';

    // Se the most common block elements.
    config.format_tags = 'p;h1;h2;h3;pre';
    config.skin = 'bootstrapck';
    // Make dialogs simpler.
    config.removeDialogTabs = 'image:advanced;link:advanced';

    // File manager
    config.filebrowserBrowseUrl = '/addons/ckeditor/plugins/ckfsys/browser/default/browser.html?Connector=/addons/ckeditor/plugins/ckfsys/connectors/php/connector.php';
    config.filebrowserImageBrowseUrl = '/addons/ckeditor/plugins/ckfsys/browser/default/browser.html?type=Image&Connector=/addons/ckeditor/plugins/ckfsys/connectors/php/connector.php';

    //AutoGrow
    //config.extraPlugins = 'autogrow';
    //config.autoGrow_maxHeight = 370;
    //config.autoGrow_onStartup = true;
};
