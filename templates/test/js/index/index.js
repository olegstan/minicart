$(function(){
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
        speed: 800,
        centeredSlides: true,
            autoplay: 4200,
            autoplayDisableOnInteraction: false,
                loop:true
        });
});

