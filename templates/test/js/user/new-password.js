$(function(){
    var big_mes_start = '<div class="ui red message form-hint-js">';
    var big_mes_end = '</div>';

    $('#new-password-form-old-password, #lnew-password-form-new-password').focusout(function(e){
        var form = $(this).closest('form');
        var field = $(this).attr('name');
        var value = $(this).val();
        var input;

        $.ajax({
            type: 'POST',
            url: form.data('validate-action'),
            data: {
                field: field,
                value: value
            },
            beforeSend: function(){

            },
            success: function (response) {
                $('[data-input="' + field +'"]').remove();

                input = $('#new-password-form-' + field);
                input
                    .parent()
                    .parent()
                    .removeClass('error');

                if(response.result == 'success'){

                }else if(response.result == 'error'){

                    $.each(response.errors, function (k, v) {
                        input = $('#new-password-form-' + k);
                        input
                            .parent()
                            .parent()
                            .addClass('error');

                        input
                            .parent()
                            .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                    });
                }
            },
            error: function () {

            }
        });
    });

    $('#new-password-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: this.action,
            data: {
                old_password: $('#new-password-form-old-password').val(),
                new_password: $('#new-password-form-new-password').val()
            },
            beforeSend: function(){

            },
            success: function (response) {
                $('.form-hint-js').remove();

                var input;
                if(response.result == 'success'){
                    window.location.reload();
                }else if(response.result == 'error'){
                    $.each(response.success, function (k, v) {
                        input = $('#new-password-form-' + k);
                        input.parent().removeClass('error');
                    });

                    $.each(response.errors, function (k, v) {
                        input = $('#new-password-form-' + k);
                        input.parent().addClass('error');

                        input
                            .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                    });
                    $('#new-password-form')
                        .find('button')
                        .before(big_mes_start + response.error + big_mes_end);
                }
            },
            error: function () {

            }
        });
    });


});