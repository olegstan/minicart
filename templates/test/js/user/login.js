$(function(){
    var big_mes_start = '<div class="ui red message form-hint-js">';
    var big_mes_end = '</div>';

    $('#login-form-username, #login-form-password').focusout(function(e){
        var form = $(this).closest('form');
        var field = $(this).attr('name');
        var value = $(this).val();
        var input;

        $.ajax({
            type: 'POST',
            url: form.data('validate-action'),
            data: {
                field: field,
                value: value
            },
            beforeSend: function(){

            },
            success: function (response) {
                $('[data-input="' + field +'"]').remove();

                input = $('#login-form-' + field);
                input
                    .parent()
                    .parent()
                    .removeClass('error');

                if(response.result == 'success'){

                }else if(response.result == 'error'){

                    $.each(response.errors, function (k, v) {
                        input = $('#login-form-' + k);
                        input
                            .parent()
                            .parent()
                            .addClass('error');

                        input
                            .parent()
                            .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                    });
                }
            },
            error: function () {

            }
        });
    });

    $('#login-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: this.action,
            data: {
                username: $('#login-form-username').val(),
                password: $('#login-form-password').val()
            },
            beforeSend: function(){
                $('#login-form')
                    .find('button')
                    .addClass('loading');
            },
            success: function (response) {
                $('#login-form')
                    .find('button')
                    .removeClass('loading');

                $('.form-hint-js').remove();

                var input;
                if(response.result == 'success'){
                    window.location.reload();
                }else if(response.result == 'error'){
                        $.each(response.success, function (k, v) {
                            input = $('#login-form-' + k);
                            input
                                .parent()
                                .parent()
                                .removeClass('error');
                        });

                        $.each(response.errors, function (k, v) {
                            input = $('#login-form-' + k);
                            input
                                .parent()
                                .parent()
                                .addClass('error');

                            input
                                .parent()
                                .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                        });
                    $('#login-form')
                        .find('button')
                        .before(big_mes_start + response.error + big_mes_end);
                }
            },
            error: function () {

            }
        });
    });

});
