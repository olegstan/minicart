$(function(){
    $("#register-form-phone").mask("+7 (999) 999-99-99");

    var big_mes_start = '<div class="ui red message form-hint-js">';
    var big_mes_end = '</div>';

    $('#register-form-name, #register-form-phone, #register-form-email, #register-form-password').focusout(function(e){
        var form = $(this).closest('form');
        var field = $(this).attr('name');
        var value = $(this).val();
        var input;
        $.ajax({
            type: 'POST',
            url: form.data('validate-action'),
            data: {
                field: field,
                value: value
            },
            beforeSend: function(){

            },
            success: function (response) {
                $('[data-input="' + field +'"]').remove();

                input = $('#register-form-' + field);
                input
                    .parent()
                    .removeClass('error');

                if(response.result == 'success'){

                }else if(response.result == 'error'){
                    $.each(response.errors, function (k, v) {
                        input = $('#register-form-' + k);
                        input
                            .parent()
                            .addClass('error');

                        input
                            .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                    });
                }
            },
            error: function () {

            }
        });
    });

    $('#register-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: this.action,
            data: {
                name: $('#register-form-name').val(),
                phone: $('#register-form-phone').val(),
                email: $('#register-form-email').val(),
                password: $('#register-form-password').val()
            },
            beforeSend: function(){

            },
            success: function (response) {
                $('.form-hint-js').remove();

                var input;


                if(response.result == 'success'){
                    window.location.reload();
                }else if(response.result == 'error'){
                    $.each(response.success, function (k, v) {
                        input = $('#register-form-' + k);
                        input.parent().removeClass('error');
                    });

                    $.each(response.errors, function (k, v) {
                        input = $('#register-form-' + k);
                        input.parent().addClass('error');

                        input
                            .after('<div class="ui red pointing prompt label transition visible form-hint-js" data-input="' + k + '">' + v + '<div>');
                    });
                    $('#register-form')
                        .find('button')
                        .before(big_mes_start + response.error + big_mes_end);
                }
            },
            error: function () {

            }
        });
    });


});