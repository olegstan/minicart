<!DOCTYPE html>
<html>
<head>
    <base href="{$core->root}/"/>
    <title>{$core->title|escape}</title>

    {* Метатеги *}
    <meta name="description" content="{$core->meta_description|escape}"/>
    <meta name="keywords" content="{$core->meta_keywords|escape}"/>

    <!-- Standard Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    {*bootstrap*}
    <link href="{$libraries}bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>
    {*bootstrap*}

    {*font-awesome*}
    <link href="{$libraries}components-font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" media="screen"/>
    {*font-awesome*}

    <link rel="stylesheet" type="text/css" href="{$libraries}semantic-ui/dist/semantic.css">

    <link rel="stylesheet" type="text/css" href="{$path_frontend_template}css/style.css">
    <link rel="stylesheet" type="text/css" href="{$path_frontend_template}css/theme.css">

    {foreach $core->asset->headerCSS as $asset}
        <link rel="stylesheet" type="text/css" href="{$asset}">
    {/foreach}
</head>

<body data-spy="scroll" data-target=".subnav" data-offset="50" class="{$body_category_css} {$body_product_css}">

{*{if $smarty.session.admin && $smarty.session.admin == "admin"}*}
    {*{include file='../parts/admin-header.tpl'}*}
{*{/if}*}

<div id="wrap">

    {* header *}
        {include file='parts/header.tpl'}
    {* header *}

    {*<div id="viewedproducts">*}
        {* Блок генерируется через ajax *}
        {*include file='viewed-products.tpl'*}
    {*</div>*}

    <div class="needhelp">
        <div class="needhelp-header">Нужна помощь?</div>
        <div class="needhelp-text">Звоните
            {if isset($banners[11]->text)}
                {$banners[11]->text}
            {/if}
        </div>
        <div class="needhelp-add">
            {if isset($banners[13]->text)}
                {$banners[13]->text}
            {/if}
        </div>
    </div>


    {*<div class="company">*}
        {*<div class="company-inner">*}
            {*<div str-row>*}
                {*<div str-xs>*}
                    {*<div class="company-menu">*}
                        {*<div class="company-menu-header">*}
                            {*{menuname id=16}*}
                        {*</div>*}
                        {*{menu id=16 menu_type='vertical'}*}
                    {*</div>*}
                {*</div>*}
                {*<div str-xs>*}
                    {*<div class="company-menu">*}
                        {*<div class="company-menu-header">*}
                            {*{menuname id=9}*}
                        {*</div>*}
                        {*{menu id=9 menu_type='vertical'}*}
                    {*</div>*}
                {*</div>*}

                {*{if isset($banners[9]->text)}*}
                    {*{include file="parts/banner-company.tpl" id=9}*}
                {*{/if}*}

                {*{if isset($banners[6]->text)}*}
                    {*{include file="parts/banner-company.tpl" id=6}*}
                {*{/if}*}

            {*</div>*}
        {*</div>*}
    {*</div>*}



    {* footer *}
        {include file='parts/footer.tpl'}
    {* footer *}

</div>

<!-- Site Properities -->
<script type="text/javascript" src="{$libraries}jquery/dist/jquery.min.js"></script>
<script type="text/javascript" src="{$libraries}semantic-ui/dist/semantic.min.js"></script>
<script type="text/javascript" src="{$libraries}jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>

{*bootstrap*}
<script type="text/javascript" src="{$libraries}bootstrap/dist/js/bootstrap.min.js"></script>
{*bootstrap*}

{foreach $core->asset->footerJS as $asset}
    <script type="text/javascript" src="{$asset}"></script>
{/foreach}

<script src="/templates/{$core->tpl_path}/js/{$core->controller}/{$core->action}.js"></script>
<script src="/templates/{$core->tpl_path}/js/common.js"></script>

</body>
</html>