<h4 class="ui section horizontal divider header">Страница регистрации </h4>
<div class="ui  container">
    <h1>Регистрация</h1>
    <form id="register-form" action="user/register" data-validate-action="user/validate-register" method="POST" novalidate>
        <div class="ui three column grid">
            <div class="column">
                <div class="ui form">
                    <div class="field">
                        <label>Ваше Имя *</label>
                        <input id="register-form-name" type="text" name="name">
                    </div>
                    <div class="field">
                        <label>Мобильный телефон для связи</label>
                        <input id="register-form-phone" type="text" name="phone">
                    </div>
                    <div class="field">
                        <label>Электронный адрес (e-mail) *</label>
                        <input id="register-form-email" type="email" name="email">
                    </div>
                    <div class="field">
                        <label>Пароль</label>
                        <input id="register-form-password" type="password" name="password">
                    </div>
                    <div class="field">
                        <!-- Тут каптча гугла -->
                    </div>
                </div>
                <button class="big fluid ui green submit button">Зарегистрироваться</button>
            </div>
        </div>
    </form>
</div>
<div class="ui divider"></div>
