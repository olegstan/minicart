{* Шаблон бренда *}

{*breadcrumbs module='brands' id=$brand->id*}

<h1>{if $brand->frontend_name}{$brand->frontend_name}{else}{$brand->name}{/if}</h1>

{* Текстовое описание сверху *}
{if $current_page_num == 1}
	<div class="decription-category-top">
		{$brand->description}
	</div>
{/if}
{*
<h2>Категории с товарами бренда {$brand->name}:</h2>
<ul id="found-categories">
			<li class="">
			
				<a href="http://minicart.ru/catalog/samsung-80/">
                <div class="found-categories-image">
				<img alt="" src="http://minicart.ru/files/images/resized/categories/8/0/800-samsung-logo1.155x155.jpeg?f1e90a4027b79bd140753a35c2db70db">                </div>
				
			
			
				<div class="found-categories-name">Samsung</div>
                </a>
		
		</li>
	</ul>
*}
{* Далее выводим просто список товаров *}

{if $products_count > 0}
    <div id="sortview">

        <ul class="nav nav-pills" id="sort">
            <li class="disabled">Сортировать по:</li>

            {assign var="sort_page_num" value=1}
            {if $current_page_num == 'all'}{assign var="sort_page_num" value='all'}{/if}

            {foreach $sort_methods as $index=>$method}
                {if $method@first && (!array_key_exists('sort', $params_arr) || $params_arr['sort'] == $method) && $method == "position"}
                    <li><a class="btn btn-link btn-small current"><span>{$sort_methods_name[$index]}</span></a></li>
                {elseif $method@first && !array_key_exists('sort', $params_arr)}
                    <li><a data-type="{$data_type}" class="btn btn-link btn-small current" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['sort_type'=>'desc','page'=>$sort_page_num]}" data-placement="bottom" data-toggle="tooltip" data-original-title="{$sort_methods_biger[$index]}"><i class="{$sort_methods_icon_asc[$index]}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
                {elseif $params_arr['sort'] == $method}
                    <li><a data-type="{$data_type}" class="btn btn-link btn-small current" href="{$config->root_url}{$module->url}{$current_url}{if $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>$sort_page_num]}{elseif $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>$sort_page_num]}{else}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort_type'] == 'asc'}{$sort_methods_biger[$index]}{else}{$sort_methods_lower[$index]}{/if}"><i class="{if $params_arr['sort_type'] == 'desc'}{$sort_methods_icon_desc[$index]}{else}{$sort_methods_icon_asc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
                {else}
                    <li><a data-type="{$data_type}" class="btn btn-link btn-small" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == $method && $params_arr['sort_type'] == 'asc'}{$sort_methods_biger[$index]}{else}{$sort_methods_lower[$index]}{/if}"><i class="{if $params_arr['sort'] == $method && $params_arr['sort_type'] == 'desc'}{$sort_methods_icon_asc[$index]}{else}{$sort_methods_icon_desc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
                {/if}
            {/foreach}
        </ul>

        <div id="view">
            <div class="view-style">
                <div class="btn-group">
                    <a data-type="{$data_type}" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'list']}" class="btn btn-default btn-sm {if !$mode || $mode == 'list'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Списком"><i class="fa fa-bars"></i></a>
                    <a data-type="{$data_type}" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'tile']}" class="btn btn-default btn-sm {if $mode == 'tile'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Плиткой"><i class="fa fa-th"></i></a>
                </div>
            </div>
            <div class="view-how">Отображать:</div>
        </div>

    </div>

    <!-- Стиль списка товаров фильтры и прочее начало -->
    {include file='pagination-frontend.tpl'}

    {if !$mode || $mode == 'list'}
        <ul class="list">
            {foreach $brand->products as $product}
                {include file='product-list.tpl'}
            {/foreach}
        </ul>
    {/if}

    {if $mode == 'tile'}
        <ul class="plitka">
            {$mh_group = 1}
            {foreach $brand->products as $product}
                {include file='product-tile.tpl' mh_group=$mh_group}
                {if $product@iteration is div by 3}<div class="plitka-separator"></div>{$mh_group = $mh_group + 1}{/if}
            {/foreach}
        </ul>
    {/if}

    {include file='pagination-frontend.tpl'}

{else}
	Нет товаров этого бренда
{/if}


{if $current_page_num == 1}
    <div class="decription-category-bottom">
    	{$brand->description2}
    </div>
{/if}


<!-- Стиль списка товаров фильтры и прочее  конец -->
