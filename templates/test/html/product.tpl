{*{breadcrumbs module='products' id=$product->id}*}

{*{$allow_edit_module = false}*}
{*{if in_array($product_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if}*}

<div class="product-header">
	<h1>{$product->name}
        {if $allow_edit_module}
            <span>
                <a href="" class="edit btn btn-default btn-sm">
                    <i class="fa fa-pencil"></i> Редактировать товар
                </a></span>
        {/if}
    </h1>
	{*список бейджей товара*}

    {if $product->badges}
        <div class="product-list-badges">
            {foreach $product->badges as $badge}
                <span class="{$badge->css_class}">{$badge->name}</span>
            {/foreach}
        </div>
    {/if}
</div>
<div class="product-links">

	<div id="main-rating" class="product-stars">
        <input type="hidden" name="val" value="{$product->rating->avg_rating}">
        <input type="hidden" name="votes" value="{$product->rating->rating_count}"/>
        <input type="hidden" name="product_id" value="{$product->id}"/>
        <input type="hidden" name="vote-id" value="1"/>
   </div>

   <a href="" class="write-review-top dotted2" id="write-review"><i class="fa fa-pencil"></i> <span>Написать отзыв</span></a>

    <div class="buy-compare" {*if in_array($product->id, $compare_items)}style="display: none;"{/if*}>
        <a href="" class="dotted2 add_to_compare">
            <i class="fa fa-bars"></i>
            <span>Добавить к сравнению</span>
        </a>
    </div>
    <div class="buy-compare" {*if !in_array($product->id, $compare_items)}style="display:none;"{/if*}>
        <a href="" class="compare_items">Сравнить ({$compare_items|count})</a>
        &nbsp;&nbsp;
        <a href="" class="dotted2 remove_from_compare">
            <span>Убрать из сравнения</span>
        </a>
    </div>

	{*{if $user}*}
		{*<a href="#" class="dotted2" id="favorite"><i class="fa {if $product->is_favorite}fa-heart{else}fa-heart-o{/if}"></i> <span>{if $product->is_favorite}Удалить из избранного{else}Добавить в избранное{/if}</span></a>*}
    {*{else}*}
		{*<a class="dotted2" id="favorite" data-placement="bottom" data-toggle="tooltip" data-html="true" data-original-title="Авторизуйтесь, чтобы добавить товар в избранное"><i class="fa fa-heart-o"></i> <span>Добавить в избранное</span></a>*}
	{*{/if}*}

</div>

<div class="row">
	<div class="col-xs-6">
		{if $product->images}
			 {*Выводим все картинки товара с помощью fancybox *}
			<a href="{$product->image|resize:1600:1000}" class="main-image fancybox" rel="group">
				<img src="{$product->image|resize:460:460}" alt="{$product->name}" title="{$product->name}"/>
			</a>
			<ul id="more-images">
                {foreach $product->images as $img}
                    <li>
                        <a href="{$img|resize:1600:1000}" class="fancybox" rel="group">
                            <img src="{$img|resize:76:76}" title="{$product->name}" alt="{$product->name}">
                        </a>
                    </li>
                {/foreach}
			</ul>
		{/if}

        {if isset($banners[1]->text)}
            <div class="share">
                 {*Подключаем кнопки поделиться от яндекса*}
                {$banners[1]->text}
            </div>
        {/if}
    </div>




    <div class="col-xs-6">
        <div class="row">
            <div class="col-xs-6">

            {*блок покупки товара *}
                <div id="buy">
                    <div class="buy-inner">

                        {$selected_variant = $product->variant}

                        {if $product->variants|count > 1 || ($product->variants|count == 1 && !empty($product->variant->name))}
			            {*Если вариантов товара > 1 *}
                	    {*Меняем статус в зависимости от статуса варианта *}

                            <div class="buy-status">
                                <span class="in-stock product-status" {if $product->variant->stock <= 0}style="display:none;"{/if}><i class="fa fa-check"></i> Есть в наличии</span>
                                <span class="out-of-stock product-status" {if $product->variant->stock != 0}style="display:none;"{/if}><i class="fa fa-times-circle"></i> Нет в наличии</span>
                                <span class="to-order product-status" {if $product->variant->stock >= 0}style="display:none;"{/if}><i class="fa fa-truck"></i> Под заказ</span>
                            </div>


                            <div class="form-group buy-list-variants">
                                <label>Доступные варианты:</label>

                                 {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ *}

                                <select class="form-control" name="variant_id">
                                    {foreach $product->variants as $variant}
                                        <option value="{$variant->id}" data-stock="{$variant->stock}" data-price="" data-price-convert="" data-price-old="" data-price-old-convert="" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if} {if $variant->id == $variant_id}selected{$selected_variant = $variant}{/if}>{$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
                                    {/foreach}
                                </select>



                                {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ (END) *}
                                {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ *}
                                {foreach $product->variants as $variant}
                                    <div class="radio">
                                      <label>
                                        <input type="radio" name="variant_id" value="{$variant->id}" data-stock="{$variant->stock}" data-price="" data-price-convert="" data-price-old="" data-price-old-convert="" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if} {if $variant->id == $variant_id}checked{$selected_variant = $variant}{/if} {if !$variant_id && $variant@first}checked{/if}/> {$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}
                                      </label>
                                    </div>
                                {/foreach}
                                {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ (END) *}
                            </div>
		                {else}
                            {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ *}


                            <select name="variant_id" style="display: none;">
                                <option value="{$product->variant->id}" data-stock="{$product->variant->stock}" data-price="" data-price-convert="" data-price-old="" data-price-old-convert="" data-sku="{$product->variant->sku}" checked>{$product->variant->name}</option>
                            </select>

                            {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ (END) *}
                            {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ *}
                            <input type="hidden" name="variant_id" value="{$product->variant->id}" data-stock="{$product->variant->stock}" data-price="" data-price-convert="" data-price-old="" data-price-old-convert="" data-sku="{$product->variant->sku}" {if $product->variant->stock == 0}class="out-of-stock"{elseif $product->variant->stock < 0}class="to-order"{/if} checked/>
                            {*ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ (END)*}

                        {$selected_variant = $product->variant}
                        {*Если 1 вариант*}

                        <div class="buy-status">
                            {if $product->variant->stock > 0}<span class="in-stock product-status"><i class="fa fa-check"></i> Есть в наличии</span>{/if}
                            {if $product->variant->stock == 0}<span class="out-of-stock product-status"><i class="fa fa-times-circle"></i> Нет в наличии</span>{/if}
                            {if $product->variant->stock < 0}<span class="to-order product-status"><i class="fa fa-truck"></i> Под заказ</span>{/if}
                        </div>

                    {/if}

       	{*<div class="buy-oldprice" {if !$selected_variant->price_old}style="display:none;"{/if}>{if $product->currency_id}{$selected_variant->price_old|convert:$product->currency_id}{else}{$selected_variant->price_old|convert:$admin_currency->id}{/if}<span class="currency"> {$main_currency->sign}</span></div>*}
        <div class="buy-price" {if !$selected_variant->price}style="display:none;"{/if}>{if $product->currency_id}{$selected_variant->price|convert:$product->currency_id}{else}{$selected_variant->price|convert:$admin_currency->id}{/if}<span class="currency"> {$main_currency->sign}</span></div>

		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
		    <div class="buy-price-one-pcs" style="display:none;"></div>
		{/if}

		{*{if $modificators_groups}*}
			{*<div class="product-modifier">*}
				{*{foreach $modificators_groups as $gr}*}
					{*{if $gr->id == 0 || ($gr->id > 0 && !$product->modificators_mode && in_array($gr->id, $category->modificators_groups)) || ($gr->id > 0 && $product->modificators_mode && in_array($gr->id, $product->modificators_groups))}*}

						{*{if $gr->name}*}
							{*<div class="product-modifier-group">*}
							{*<label class="modifier-group-header">{$gr->name}</label>*}
						{*{/if}*}

						{*{if $gr->type == 'checkbox'}*}
						{*<div class="modifier-checkbox">*}
							{*{foreach $gr->modificators as $m}*}
								{*{if $gr->id > 0 || ($gr->id == 0 && !$product->modificators_mode && in_array($m->id, $category->modificators)) || ($gr->id == 0 && $product->modificators_mode && in_array($m->id, $product->modificators))}*}
								{*<div class="checkbox">*}
									{*<label>*}
									    {*<input type="checkbox" value="{$m->id}" name="modificators[{$gr->id}]" data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}*}
									{*</label>*}

									{*{if $m->multi_buy}*}
									{*<div class="modifier-counter">*}
									{*<div class="input-group">*}
										{*<span class="input-group-btn">*}
											{*<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>*}
										{*</span>*}
										{*<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">*}
										{*<span class="input-group-btn">*}
											{*<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>*}
										{*</span>*}
									{*</div>*}
									{*</div>*}

									{*{/if}*}
								{*</div>*}
								{*{/if}*}
							{*{/foreach}*}
							{*</div>*}
						{*{/if}*}

						{*{if $gr->type == 'radio'}*}
							{*{foreach $gr->modificators as $m}*}
								{*<div class="radio">*}
									{*<label>*}
									{*<input type="radio" name="modificators[{$gr->id}]" value="{$m->id}" {if $m@first}checked{/if} data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}*}
									{*</label>*}

									{*{if $m->multi_buy}*}
									{*<div class="input-group">*}
										{*<span class="input-group-btn">*}
											{*<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>*}
										{*</span>*}
										{*<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">*}
										{*<span class="input-group-btn">*}
											{*<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>*}
										{*</span>*}
									{*</div>*}
									{*{/if}*}
								{*</div>*}
							{*{/foreach}*}
						{*{/if}*}

						{*{if $gr->type == 'select'}*}
							{*<div class="modifier-select">*}
							{*<select name="modificators[{$gr->id}]" class="form-control">*}
								{*<option>Не выбрано</option>*}
								{*{foreach $gr->modificators as $m}*}
									{*<option value="{$m->id}" data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">*}
									{*{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}*}
									{*</option>*}
								{*{/foreach}*}
							{*</select>*}
							{*</div>*}
						{*{/if}*}

						{*{if $gr->name}*}
							{*</div>*}
						{*{/if}*}
					{*{/if}*}
				{*{/foreach}*}
			{*</div>*}
		{*{/if}*}

		{if $settings->catalog_show_multi_buy}
            <div class="price-with-amount">  Блок покупки нескольких штук товара
                <div class="kolvo">Количество:</div>
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button data-type="minus" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
                        </span>

                        <input type="text" class="form-control input-sm" placeholder=""
                                value="1" name="amount"
                                data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">

                        <span class="input-group-btn">
                            <button data-type="plus" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                    <div class="buy-price-with-amount" {if !$selected_variant->price}style="display:none;"{/if}></div>
                </div>
            </div>
		{else}
            <input type="hidden" value="1" name="amount" data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">
		{/if}

		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
            <div class="kolvo">{$settings->catalog_variable_amount_name}:</div>
                <div class="input-group">
                    <span class="input-group-btn">
                        <button data-type="minus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
                    </span>

                    <input type="text" class="form-control input-sm" placeholder=""
                            value="{$product->min_amount|string_format:'%d'}" name="var_amount"
                            data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">

                    <span class="input-group-btn">
                        <button data-type="plus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
                    </span>
                </div>

                <div class="buy-price-with-amount" {if !$selected_variant->price}style="display:none;"{/if}></div>
            </div>
		{/if}

		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
            <div class="begunok">
                <div id="modern"></div>
                <div class="clear-begunok"></div>
            </div>
		{/if}

		<a href="#" class="btn btn-success btn-lg buy-buy" {if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if}>Добавить в корзину</a>
		{*{if $product->currency_id}*}
			{*<a href="#" class="buy1 btn btn-primary" if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden" id="buy1spinner"></i></a>*}
		{*{else}*}
			{*<a href="#" class="buy1 btn btn-primary" if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden" id="buy1spinner"></i></a>*}
		{*{/if}*}

		<div class="alert alert-warning" id="no_stock" {if $selected_variant->stock_available >= 0}style="display: none;"{/if}></div>

        </div>

        <div class="product-id">

            <div id="sku" {if !$product->variant->sku}style="display:none;"{/if}>Артикул: <span>{$product->variant->sku}</span></div>
                {if $settings->use_product_id}
                    Код товара: <span>{$product->id}</span>
                {/if}
            </div>

		{*{if $smarty.session.admin && $smarty.session.admin == "admin"}*}
			{*{if $settings->add_field1_enabled && $settings->add_field1_show_frontend && $product->add_field1}*}
				{*<div class="product-add-field">{$settings->add_field1_name}: {$product->add_field1}</div>*}
			{*{/if}*}
			{*{if $settings->add_field2_enabled && $settings->add_field2_show_frontend && $product->add_field2}*}
				{*<div class="product-add-field">{$settings->add_field2_name}: {$product->add_field2}</div>*}
			{*{/if}*}
			{*{if $settings->add_field3_enabled && $settings->add_field3_show_frontend && $product->add_field3}*}
				{*<div class="product-add-field">{$settings->add_field3_name}: {$product->add_field3}</div>*}
			{*{/if}*}
		{*{/if}*}

		{*{if $smarty.session.admin && $smarty.session.admin == "admin"}*}
			{*{if $settings->add_flag1_enabled && $settings->add_flag1_show_frontend}*}
				{*<div class="product-add-flag">{if $product->add_flag1}<a href="" class="flag1-on"></a>{else}<a href="" class="flag1-off"></a>{/if}</div>*}
			{*{/if}*}
			{*{if $settings->add_flag2_enabled && $settings->add_flag2_show_frontend}*}
				{*<div class="product-add-flag">{if $product->add_flag2}<a href="" class="flag2-on"></a>{else}<a href="" class="flag2-off"></a>{/if}</div>*}
			{*{/if}*}
			{*{if $settings->add_flag3_enabled && $settings->add_flag3_show_frontend}*}
				{*<div class="product-add-flag">{if $product->add_flag3}<a href="" class="flag3-on"></a>{else}<a href="" class="flag3-off"></a>{/if}</div>*}
			{*{/if}*}
		{*{/if}*}

        </div>



    </div>

    <div class="col-xs-6">
        <div class="product-blocks">
            {if isset($banners[15]->text)}
                <div class="product-blocks-item">
                    <div class="product-blocks-item-heading">{{$banners[18]->name}}</div>
                    {$banners[15]->text}
                </div>
            {/if}
            {if isset($banners[16]->text)}
                <div class="product-blocks-item">
                    <div class="product-blocks-item-heading">{{$banners[18]->name}}</div>
                    {$banners[16]->text}
                </div>
            {/if}
            {if isset($banners[18]->text)}
                <div class="product-blocks buytype">
                    <div class="product-blocks-item-heading">{{$banners[18]->name}}</div>
                    {$banners[18]->text}
                </div>
            {/if}
        </div>
    </div>

</div>


<div class="product-description-top">
    {if $product->annotation2 or $product_tags && $tags_groups}
        <div class="product-description-header">Описание</div>

            <div class="product-description-short">
                {$product->annotation2}
            </div>

    {/if}
    {if $product_tags && $tags_groups}
        <div class="product-properties">
            <table class="table table-striped">
                <tbody>
                    {foreach $tags_groups as $tag_group}
                        {if array_key_exists($tag_group->id, $product_tags) && $tag_group->show_in_frontend}
                        <tr>
                            <td>{$tag_group->name}{if $tag_group->help_text} <a class="property-help" data-type="qtip" data-content="{$tag_group->help_text|escape}" data-title="{$tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}</td>
                            <td>
                            {foreach $product_tags[$tag_group->id] as $tag}
                                {$tag->name}{if $tag_group->postfix} {$tag_group->postfix}{/if}{if !$tag@last}, {/if}
                            {/foreach}
                            </td>
                        </tr>
                        {/if}
                    {/foreach}

            </table>
        </div>
    {/if}
</div>

    {*</div>*}
  {*</div>*}




{*<!-- Nav tabs -->*}
{*<div class="products-tabs">*}
    {*<ul class="nav nav-tabs">*}
      {*{if $count_related_products > 0 || $count_analogs_products > 0}<li class="{if !$tab || $tab == "related"}active{/if}"><a href="#related" data-toggle="tab" data-url="related">Также рекомендуем посмотреть <i class="fa fa-spinner" id="related_spinner"></i></a></li>{/if}*}
      {*{if $product->body}<li {if ($count_related_products == 0 && $count_analogs_products == 0 && !$tab) || $tab == "complete-description"}class="active"{/if}><a href="#complete-description" data-toggle="tab" data-url="complete-description">Подробные характеристики</a></li>{/if}*}
      {*{if $settings->reviews_enabled}<li {if ($count_related_products == 0 && $count_analogs_products == 0 && !$product->body && !$tab) || $tab == "reviews"}class="active"{/if}><a href="#reviews" data-toggle="tab" data-url="reviews">Отзывы{if $reviews_count>0} ({$reviews_count}){/if}</a></li>{/if}*}
	  {*<li {if $tab == "questions"}class="active"{/if}><a href="#questions" data-toggle="tab" data-url="questions">Вопрос-ответ</a></li>*}
      {*{if $product->attachments}<li {if $tab == "files"}class="active"{/if}><a href="#files" data-toggle="tab" data-url="files">Файлы</a></li>{/if}*}
    {*</ul>*}
    {**}
    {* Содержимое некоторых табов можно грузить на аяксе *}
    {*<div class="tab-content">*}
      {*{if $count_related_products > 0 || $count_analogs_products > 0}<div class="tab-pane {if !$tab || $tab == "related"}active{/if}" id="related"></div>{/if}*}
      {*{if $product->body}<div class="tab-pane{if ($count_related_products == 0 && $count_analogs_products == 0 && !$tab) || $tab == "complete-description"} active{/if}" id="complete-description">{$product->body}</div>{/if}*}
      {*{if $settings->reviews_enabled}*}
	  {*<div class="tab-pane{if ($count_related_products == 0 && $count_analogs_products == 0 && !$product->body && !$tab) || $tab == "reviews"} active{/if}" id="reviews">*}
      	{*<div class="row">*}
		{*<div class="col-xs-9">*}
            {*<div id="reviews-list">*}
            {*</div>*}
         {*</div>*}
         {*<div class="col-xs-3">*}
         	{**}
         {*</div>*}
         {*</div>*}
	  {*</div>*}
	  {*{/if}*}
	  {*<div class="tab-pane {if $tab == "questions"}active{/if}" id="questions">*}
		{*{include file='questions.tpl'}*}
	  {*</div>*}
      {**}
      {*<div class="tab-pane {if $tab == "files"}active{/if}" id="files">*}
		{*{if $product->attachments}*}
        {*<div class="product-files">*}
        	{*<div class="files-header">Файлы для скачивания:</div>*}
            {*<ul class="files">*}
                {*{foreach $product->attachments as $attach}*}
                    {*<li>*}
                        {*<div class="filename">*}
                            {*<div class="fileimage">*}
								{*{$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}*}
								{*{if in_array($attach->extension, $exts)}*}
									{*<img src="{$path_frontend_template}/img/fileext/{$attach->extension}.png">*}
								{*{else}*}
									{*<img src="{$path_frontend_template}/img/fileext/other.png">*}
								{*{/if}*}
							{*</div>*}
                            {*<a href="{$attach->filename|attachment:'products'}" target="_blank">*}
        {*{if $attach->name}{$attach->name}{if $attach->extension}.{$attach->extension}{/if}{else}{$attach->filename}{/if}</a>*}
                        {*</div>*}
                    {*</li>*}
                {*{/foreach}*}
            {*</ul>*}
        {*</div>*}
        {*{/if}*}
	  {*</div>*}
      {**}
	{*</div>*}

{*</div>*}