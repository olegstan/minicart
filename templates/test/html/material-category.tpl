{*{breadcrumbs module='materials' type='material-category' id=$category->id}*}

<h1>{$material_category->title|escape}</h1>

{*{include file='pagination-frontend.tpl'}*}

{foreach $material_category->materials as $material}
    <h2>
        <a href="{$material->url}">{$material->title|escape}</a>
    </h2>
    {if $material_category->show_date}
        <p class="material-date">{$material->date|date}</p>
    {/if}
    <div class="material-annotation">
        {if $material->image}
            <a href="{$material->url}">
            <img src="{$material->image|resize:'materials':160:160}" class="previewimage"/></a>
        {/if}
не
        {$material->description|preannotation}

        <p>
            <a href="{$material->url}" class="material-readmore materialhref btn btn-default">Подробнее</a>
        </p>
    </div>
    <div class="material-separator"></div>
{/foreach}

{$category->description}

{*{include file='pagination-frontend.tpl'}*}