{*<div class="ui grid container">*}

    {*{include file='parts/slideshow.tpl'}*}

{*</div>*}
{*{foreach from=$groups_categories_main item=group}*}
{*<div class="ui grid container">*}
    {*<div class="wide column">*}
        {*<div class="ui sub header">{$group->name}</div>*}
    {*</div>*}
{*</div>*}
{*<div class="ui five column grid container">*}
    {*{foreach from=$group->categories item=category}*}
        {*{include file='parts/category-list.tpl'}*}
    {*{/foreach}*}
{*</div>*}
{*<div class="ui hidden divider"></div>*}
{*{/foreach}*}
{*{foreach from=$groups_products_main item=group}*}
    {*<div class="ui grid container">*}
        {*<div class="wide column">*}
            {*<div class="ui sub header">{$group->name}</div>*}
        {*</div>*}
    {*</div>*}
    {*<div class="ui five column grid container">*}
        {*{foreach from=$group->products item=product}*}
            {*{include file='parts/product-list.tpl'}*}
        {*{/foreach}*}
    {*</div>*}
{*{/foreach}*}
{*<div class="ui hidden divider"></div>*}
{*<div class="ui grid container">*}

    {*<div class="four wide column">*}
        {*<div class="ui top attached header"><a href="">Новости</a>*}
        {*</div>*}
        {*<div class="ui attached segment">*}
            {*<p><a href="">Заголовок новости</a></p>*}
            {*<img class="ui medium rounded image" src="http://placehold.it/350x150">*}
            {*<p>Многие телевизоры Sony в прошлом году достигли хорошего баланса между ценой и качеством. Более того, многие модели стали просто мечтой для геймеров, предложив максимальную отзывчивость и комфортный отклик в играх.</p>*}
            {*<p><a href="#">Подробнее</a>*}
            {*</p>*}
        {*</div>*}
        {*<div class="ui attached segment">*}
            {*<p><a href="">Заголовок новости</a></p>*}
            {*<img class="ui medium rounded image" src="http://placehold.it/350x150">*}
            {*<p>Многие телевизоры Sony в прошлом году достигли хорошего баланса между ценой и качеством. Более того, многие модели стали просто мечтой для геймеров, предложив максимальную отзывчивость и комфортный отклик в играх.</p>*}
            {*<p><a href="#">Подробнее</a>*}
            {*</p>*}
        {*</div>*}
        {*<div class="ui bottom attached header"><a href="">Все новости</a></div>*}
    {*</div>*}

    {*<div class="twelve wide column">*}

        {*<div class="ui breadcrumb">*}
            {*<a class="section">Home</a>*}
            {*<span class="divider">/</span>*}
            {*<a class="section">Registration</a>*}
            {*<span class="divider">/</span>*}
            {*<div class="active section">Personal Information</div>*}
        {*</div>*}

        {*<h1 class="ui header">Главный заголовок</h1>*}
        {*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel tincidunt eros, nec venenatis ipsum. Nulla hendrerit urna ex, id sagittis mi scelerisque vitae. Vestibulum posuere rutrum interdum. Sed ut ullamcorper odio, non pharetra eros. Aenean sed lacus sed enim ornare vestibulum quis a felis. Sed cursus nunc sit amet mauris sodales tempus. Nullam mattis, dolor non posuere commodo, sapien ligula hendrerit orci, non placerat erat felis vel dui. Cras vulputate ligula ut ex tincidunt tincidunt. Maecenas eget gravida lorem. Nunc nec facilisis risus. Mauris congue elit sit amet elit varius mattis. Praesent convallis placerat magna, a bibendum nibh lacinia non.</p>*}
        {*<p>Fusce mollis sagittis elit ut maximus. Nullam blandit lacus sit amet luctus euismod. Duis luctus leo vel consectetur consequat. Phasellus ex ligula, pellentesque et neque vitae, elementum placerat eros. Proin eleifend odio nec velit lacinia suscipit. Morbi mollis ante nec dapibus gravida. In tincidunt augue eu elit porta, vel condimentum purus posuere. Maecenas tincidunt, erat sed elementum sagittis, tortor erat faucibus tellus, nec molestie mi purus sit amet tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Mauris a tincidunt metus. Fusce congue metus aliquam ex auctor eleifend.</p>*}
        {*<p>Ut imperdiet dignissim feugiat. Phasellus tristique odio eu justo dapibus, nec rutrum ipsum luctus. Ut posuere nec tortor eu ullamcorper. Etiam pellentesque tincidunt tortor, non sagittis nibh pretium sit amet. Sed neque dolor, blandit eu ornare vel, lacinia porttitor nisi. Vestibulum sit amet diam rhoncus, consectetur enim sit amet, interdum mauris. Praesent feugiat finibus quam, porttitor varius est egestas id.</p>*}


    {*</div>*}

{*</div>*}











<div class="row topblocks">
    <div class="col-xs-3">
        {if isset($banners[19]) or isset($banners[24]) or isset($banners[25]) }
            <div class="flex-vertical">

                {if isset($banners[19]->text)}
                    {include file="parts/banner.tpl" id=19}
                {/if}

                {if isset($banners[24]->text)}
                    {include file="parts/banner.tpl" id=24}
                {/if}

                {if isset($banners[25]->text)}
                    {include file="parts/banner.tpl" id=25}
                {/if}

            </div>
        {/if}
    </div>

    {include file='parts/slideshow.tpl'}

</div>

{* Группы категорий - начало *}
{foreach $groups_categories_main as $group}
    <div class="block-class">
        <legend>{$group->name}</legend>
        <ul class="category-list">
            {foreach $group->categories as $category}
            <li class="{$category->css_class}">
                <a href="{$category->front_url}">
                    <div class="category-list-image">
                        {if isset($category->image)}
                            <img src='{$category->image|resize:155:155}' alt='{$category->image->name}'>
                        {/if}
                    </div>
                    <div class="category-list-name"><span>{$category->name}</span>{$category->products_count}</div>
                </a>
            </li>
            {/foreach}
        </ul>
    </div>
{/foreach}
{* Группы категорий - конец *}


{* Группы товаров - начало *}
{foreach $groups_products_main as $group}
    <div class="block-class">
        <legend>{$group->name}</legend>
        <p>
            {$show_mode = 'tile'}
            {if $show_mode == 'list'}
                <ul class="list">
                    {foreach $group->products as $product}
                        {include file='parts/product-list.tpl'}
                    {/foreach}
                </ul>
            {/if}

            {if $show_mode == 'tile'}
                <ul class="plitka">
                    {$mh_group = 1}
                    {foreach $group->products as $product}
                        {include file='parts/product-tile.tpl' mh_group=$mh_group}
                        {if $product@iteration is div by 4}
                            <div class="plitka-separator"></div>
                            {$mh_group = $mh_group + 1}
                        {/if}
                    {/foreach}
                </ul>
            {/if}
        </p>
    </div>
{/foreach}
 {*Группы товаров - конец *}

<div class="row">
    <div class="col-xs-3">
        {include file="parts/material-category.tpl" category=$news_category footer='Все новости'}
        {include file="parts/material-category.tpl" category=$articles_category footer='Все статьи'}
    </div>


    <div class="col-xs-9">
         {*Материал главной страницы - начало *}

            {*{$allow_edit_module = false}*}
            {*{if in_array($material_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if}*}

            <h1>
                {$main_menu_item->material->title|escape}

                {if $allow_edit_module}
                    <span>
                        <a href="" class="edit btn btn-default btn-sm">
                            <i class="fa fa-pencil"></i> Редактировать материал
                        </a>
                    </span>
                {/if}
            </h1>

            {if $main_menu_item->material->parent_id > 0 && $category->show_date}
                <p class="material-date">{$main_menu_item->material->date|date}</p>
            {/if}
            <div class="material-full">
                {$main_menu_item->material->description|annotation}
            </div>

            {if $main_menu_item->material->images}
                {include file='parts/material-gallery.tpl' material=$main_menu_item->material}
            {/if}

            {if $attachments}
                <div class="product-files">
                    <div class="files-header">Файлы для скачивания:</div>
                    <ul class="files">
                        {foreach $attachments as $attach}
                            <li>
                                <div class="filename">
                                    <div class="fileimage">
                                        {$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}
                                        {if in_array($attach->extension, $exts)}
                                            <img src="{$path_frontend_template}/img/fileext/{$attach->extension}.png">
                                        {else}
                                            <img src="{$path_frontend_template}/img/fileext/other.png">
                                        {/if}
                                    </div>
                                    <a href="" target="_blank">
                                        {if $attach->name}
                                            {$attach->name}{if $attach->extension}.{$attach->extension}{/if}{else}{$attach->filename}
                                        {/if}
                                    </a>
                                </div>
                            </li>
                        {/foreach}
                    </ul>
                </div>
            {/if}
         {*Материал главной страницы - конец *}
    </div>
</div>


<div id="brands" {if $brands_all_count == 0}style="display:none;"{/if}>
    <div class="head">Бренды
        {if $brands_popular_count > 0}
            <div class="btn-group brandlinks">
                <a href="#" id="pop-brands" class="btn btn-xs current"><span>Популярные</span></a>
                <a href="#" id="all-brands" class="btn btn-xs"><span>Все</span></a>
            </div>
        {/if}
    </div>

    {if $brands_popular_count > 0}
        <div class="panel-body" id="pop-brands-list">
            <ul>
                {foreach $brands_popular as $b}
                    {if $b->count > 0}
                        <li data-id="{$b->id}">
                            <a href="{$b->front_url}" {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                            <span>{$b->count}</span>
                        </li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    {/if}

    <div class="panel-body" id="all-brands-list" {if $brands_popular_count > 0}style="display:none;"{/if}>
        <ul>
            {foreach $brands_all as $b}
                {if $b->count > 0}
                    <li data-id="{$b->id}">
                        <a href="{$b->front_url}" {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                        <span>{$b->count}</span>
                    </li>
                {/if}
            {/foreach}
        </ul>
    </div>
</div>