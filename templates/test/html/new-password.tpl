<h4 class="ui horizontal divider header">Страница смены пароля</h4>
<div class="ui container">

    <h1>Сменить пароль</h1>

    <form id="new-password-form" action="user/new-password" data-validate-action="user/validate-new-password" method="POST" novalidate>
        <div class="ui three column grid">
            <div class="column">
                <div class="ui form">
                    <div class="field">
                        <label>Старый пароль</label>
                        <div class="ui left icon input">
                            <input id="new-password-form-old-password" type="password" name="old_password" autocomplete="off">
                            <i class="lock icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <label>Новый пароль</label>
                        <div class="ui left icon input" data-content="Add users to your feed">
                            <input id="new-password-form-new-password" type="password" name="new_password" autocomplete="off">
                            <i class="lock icon"></i>
                        </div>
                    </div>
                    <button class="big fluid ui blue submit button">Изменить</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="ui divider"></div>