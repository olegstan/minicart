{*<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-home"></i></a></li>
    <li><a href="#">Новости</a></li>
    <li><a href="#">Последние новости</a></li>
	<li class="active">Новая система материалов</li>
</ol>*}

{*{breadcrumbs module='materials' type='material' id=$material->id}*}

{*{$allow_edit_module = false}*}
{*{if in_array($material_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if}*}

<h1>{$material->title}
    {if $allow_edit_module}
        <span>
            <a href="" class="edit btn btn-default btn-sm">
                <i class="fa fa-pencil"></i> Редактировать материал
            </a>
        </span>
    {/if}
</h1>
{if $material->parent_id > 0 && $category->show_date}
    <p class="material-date">{$material->date|date}</p>
{/if}
<div class="material-full">
	{$material->description|annotation}
</div>

{*{if $images_gallery}*}
	{*{include file='material-gallery.tpl'}*}
{*{/if}*}

{*{if $attachments}*}
{*<div class="product-files">*}
	{*<div class="files-header">Файлы для скачивания:</div>*}
	{*<ul class="files">*}
		{*{foreach $attachments as $attach}*}
			{*<li>*}
				{*<div class="filename">*}
					{*<div class="fileimage">*}
						{*{$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}*}
						{*{if in_array($attach->extension, $exts)}*}
							{*<img src="{$path_frontend_template}/img/fileext/{$attach->extension}.png">*}
						{*{else}*}
							{*<img src="{$path_frontend_template}/img/fileext/other.png">*}
						{*{/if}*}
					{*</div>*}
					{*<a href="{$attach->filename|attachment:'materials'}" target="_blank">*}
{*{if $attach->name}{$attach->name}{if $attach->extension}.{$attach->extension}{/if}{else}{$attach->filename}{/if}</a>*}
				{*</div>*}
			{*</li>*}
		{*{/foreach}*}
	{*</ul>*}
{*</div>*}
{*{/if}*}

{*{if $material->script_text}*}
{*{$material->script_text}*}
{*{/if}*}
