{if $material->gallery_mode == 'tile'}
	<ul class="gallery-tile">
		{foreach $material->image as $img}
			<li class="tile-item">
				<a href="{$img|resize:1920:1200}" class="fancybox" rel="group-gallery">
					<img src="{$img|resize:$material->gallery_tile_width:$material->gallery_tile_height}"/>
				</a>
			</li>
		{/foreach}
	</ul>
{else}
	<ul class="gallery-list">
		{foreach $material->image as $img}
			<li class="list-item">
				<a href="{$img|resize:1920:1200}" class="fancybox" rel="group-gallery">
					<img src="{$img|resize:$material->gallery_list_width:$material->gallery_list_height}"/>
				</a>
			</li>
		{/foreach}
	</ul>
{/if}