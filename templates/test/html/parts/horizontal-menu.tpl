<nav class="navbar navbar-default" role="navigation" id="topmenu">
    <ul class="horizontal-menu nav navbar-nav">
        {foreach $items as $item}
            <li class="menu-item active">
                <a href="{$item->url}" data-type="href">{$item->name}</a>
            </li>
        {/foreach}
    </ul>
</nav>