{if $category->materials_count > 0}
    <div id="material-category-{$category->id}" class="articles">
        <div class="head">
            <a href="{$category->url}">{$category->name}</a>
        </div>
        <ul>
            {foreach $category->materials as $material}
                <div class="date">
                    <li>
                        <div class="date"></div>
                        <a href="{$material->url}" class="news-header">{$material->title}</a>
                        <div class="news-description">
                            {if $material->image}
                                <a href="{$material->url}" class="news-header" alt="{$material->title}">
                                    <img src="{$material->image|resize:201:150}" alt="{$material->title}"/>
                                </a>
                            {/if}

                            {$material->description|preannotation}

                            <p>
                                <a href="{$material->url}" class="readmore">Подробнее</a>
                            </p>
                        </div>
                    </li>
            {/foreach}

            <a href="{$category->url}" class="allnews">{$footer} ({$category->materials_count})</a>
        </ul>
    </div>
{/if}