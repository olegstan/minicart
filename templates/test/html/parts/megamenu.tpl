{********************* Мегаменю *************************}
{*<nav class="navbar navbar-default navbar-static horizontal-navbar">*}
<nav class="navbar navbar-default navbar-static horizontal-navbar" role="navigation">
    <div class="collapse navbar-collapse js-navbar-collapse">
        <ul class="nav navbar-nav" id="categories-menu-columns">
            {foreach $categories_frontend_all as $cat_level1}
                {if $cat_level1->is_visible}
                    <li class="dropdown dropdown-large {if $cat_level1->id == $category_id}active{/if}">
                        {if empty($cat_level1->subcategories)}
                            <a class="menu{$cat_level1->id} menu-level0" href="{*$config->root_url*}{$products_module->url}{$cat_level1->url}/" data-type="href"><span>{$cat_level1->name}</span></a>
                        {else}

                            <a href="{*$config->root_url*}{$products_module->url}{$cat_level1->url}/" class="dropdown-toggle menu{$cat_level1->id} menu-level0" data-type="href"><span>{$cat_level1->name}</span> <b class="caret"></b></a>

                            {* ЕСЛИ ПЕРВЫЙ УРОВЕНЬ МЕНЮ ПРОСТОЙ *}
                            {if $cat_level1->menu_level1_type == 1}
                                <ul class="dropdown-menu dropdown0 {$cat_level1->css_class} simple-menu" id="categories-menu">
                                    {function name=categories_tree_menu level=0}
                                        {foreach $items as $item}
                                            {if $item->is_visible}
                                                {if $item->subcategories}
                                                    <li class="menu-item dropdown level{$level} {if $level>0}dropdown-submenu{/if} {if $item->id == $category_id}active{/if}">
                                                        <a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}" >{$item->name}{if $level == 0}{/if}</a>
                                                        <ul class="dropdown-menu dropdown{$level} {$item->css_class}">
                                                            {categories_tree_menu items=$item->subcategories level=$level+1}
                                                        </ul>
                                                    </li>
                                                {else}
                                                    <li class="menu-item level{$level} {if $item->id == $category_id}active{/if}"><a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}</a></li>
                                                {/if}
                                            {/if}
                                        {/foreach}
                                    {/function}
                                    {categories_tree_menu items=$cat_level1->subcategories level=0}
                                </ul>
                            {/if}

                            {if $cat_level1->menu_level1_type == 3}
                                <ul class="dropdown-menu dropdown-menu-large row">
                                    {$levels = []}
                                    {section name=levels start=1 loop=$cat_level1->menu_level1_columns+1 step=1}
                                        {$levels[] = $smarty.section.levels.index}
                                    {/section}
                                    {foreach $levels as $level}

                                        <li class="col-xs-3">
                                            <ul>
                                                {foreach $cat_level1->subcategories as $cat_level2}
                                                    {if $cat_level2->is_visible && $cat_level2->menu_level2_column == $level}
                                                        {if empty($cat_level2->subcategories)}
                                                            <li {if $cat_level2->id == $category_id}class="active"{/if}><a class="menu{$cat_level2->id} menu-level1" href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" data-type="href" {if $cat_level2->id == $category->id}class="active"{/if}>{$cat_level2->name}</a></li>
                                                        {else}
                                                            <li class="have-childs {if $cat_level2->id == $category_id}active{/if}">
                                                                <a href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" class="menu{$cat_level2->id} menu-level1 {if $cat_level2->id == $category->id}active{/if}" data-type="href">{$cat_level2->name}</a>
                                                                <ul>
                                                                    {foreach $cat_level2->subcategories as $cat_level3}
                                                                        {if $cat_level3->is_visible}
                                                                            <li {if $cat_level3->id == $category_id}class="active"{/if}><a href="{*$config->root_url*}{$products_module->url}{$cat_level3->url}/" class="menu{$cat_level3->id} menu-level2 {if $cat_level3->id == $category->id}active{/if}" data-type="href">{$cat_level3->name}</a></li>
                                                                        {/if}
                                                                    {/foreach}
                                                                </ul>
                                                            </li>
                                                            {******* РАЗДЕЛИТЕЛЬ *******}
                                                            {*<li class="divider"></li>*}
                                                        {/if}
                                                    {/if}
                                                {/foreach}
                                            </ul>
                                        </li>
                                    {/foreach}
                                </ul>
                            {/if}

                            {* ЕСЛИ ПЕРВЫЙ УРОВЕНЬ МЕГАМЕНЮ *}
                            {if $cat_level1->menu_level1_type == 2}
                                {* мегаменю *}

                                {* $cat_level1->menu_level1_columns - На сколько колонок выпадает меню *}

                                {* На какую ширину выпадает меню *}
                                {if $cat_level1->menu_level1_width == 1}
                                    {* На ширину колонок *}
                                {elseif $cat_level1->menu_level1_width == 2}
                                    {* На всю ширину *}
                                {/if}

                                {*Куда ориентировано меню*}
                                {if $cat_level1->menu_level1_align == 1}
                                {elseif $cat_level1->menu_level1_align == 2}
                                {/if}

                                {*Использовать рекламный баннер для этого пункта меню*}
                                {if $category->menu_level1_use_banner}
                                    {* Да, использовать *}
                                    {* $category->menu_level1_banner_id - код баннера *}
                                {/if}

                                <ul class="dropdown-menu dropdown-menu-large menu-columns menu-columns-{$cat_level1->menu_level1_columns} menu-width-{$cat_level1->menu_level1_width} {if $category->menu_level1_use_banner}have-banner{/if} {if $cat_level1->menu_level1_align == 1} menu-align-right {elseif $cat_level1->menu_level1_align == 2} menu-align-left {/if}">

                                    {if $cat_level1->menu_level1_width == 2}
                                    <div str-row class="margin--7">
                                        {/if}
                                        {$levels = []}
                                        {section name=levels start=1 loop=$cat_level1->menu_level1_columns+1 step=1}
                                            {$levels[] = $smarty.section.levels.index}
                                        {/section}
                                        {foreach $levels as $level}
                                            {$column_width = 1}
                                            {if $level == 1}{$column_width = $cat_level1->menu_level1_column1_width}{/if}
                                            {if $level == 2}{$column_width = $cat_level1->menu_level1_column2_width}{/if}
                                            {if $level == 3}{$column_width = $cat_level1->menu_level1_column3_width}{/if}
                                            {if $level == 4}{$column_width = $cat_level1->menu_level1_column4_width}{/if}
                                            {if $level == 5}{$column_width = $cat_level1->menu_level1_column5_width}{/if}
                                            {if $level == 6}{$column_width = $cat_level1->menu_level1_column6_width}{/if}
                                            <li
                                                    {if $cat_level1->menu_level1_width == 2}
                                            str-xs str-xs-flex="{$column_width}" class="menu-column-width"
                                                    {else}
                                                class="menu-column-width columns-width-standart"
                                                    {/if}>
                                                <ul>
                                                    {foreach $cat_level1->subcategories as $cat_level2}
                                                        {if $cat_level2->is_visible && $cat_level2->menu_level2_column == $level}
                                                            {if empty($cat_level2->subcategories)}
                                                                <li {if $cat_level2->id == $category_id}class="active"{/if}><a class="menu{$cat_level2->id} menu-level1" href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" data-type="href" {if $cat_level2->id == $category->id}class="active"{/if}>{$cat_level2->name}</a></li>
                                                            {else}
                                                                <li class="have-childs {if $cat_level2->id == $category_id}active{/if}">
                                                                    <a href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" class="menu{$cat_level2->id} menu-level1 {if $cat_level2->id == $category->id}active{/if}" data-type="href">{$cat_level2->name}</a>
                                                                    <ul class="submenu-columns-{$cat_level2->menu_level2_columns}">
                                                                        {foreach $cat_level2->subcategories as $cat_level3}
                                                                            {if $cat_level3->is_visible}
                                                                                <li {if $cat_level3->id == $category_id}class="active"{/if}><a href="{*$config->root_url*}{$products_module->url}{$cat_level3->url}/" class="menu{$cat_level3->id} menu-level2 {if $cat_level3->id == $category->id}active{/if}" data-type="href">{$cat_level3->name}</a></li>
                                                                            {/if}
                                                                        {/foreach}
                                                                    </ul>
                                                                </li>
                                                                {******* РАЗДЕЛИТЕЛЬ *******}
                                                                {*<li class="divider"></li>*}
                                                            {/if}
                                                        {/if}
                                                    {/foreach}
                                                </ul>
                                            </li>
                                        {/foreach}

                                        {if $cat_level1->menu_level1_use_banner}
                                            <div class="wide-menu-block">
                                                {*banner id=$cat_level1->menu_level1_banner_id*}
                                            </div>
                                        {/if}

                                        {if $cat_level1->menu_level1_width == 2}
                                    </div>
                                    {/if}
                                </ul>
                            {/if}
                        {/if}
                    </li>
                {/if}
            {/foreach}
        </ul>
    </div>
</nav>
