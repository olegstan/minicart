{*<div class="ui container">*}
    {*<div class="ui stackable divided equal height stackable grid">*}
        {*<div class="four wide column">*}
            {*<h4 class="ui header">About</h4>*}
            {*<div class="ui link list">*}
                {*<a href="#" class="item">Sitemap</a>*}
                {*<a href="#" class="item">Contact Us</a>*}
                {*<a href="#" class="item">Religious Ceremonies</a>*}
                {*<a href="#" class="item">Gazebo Plans</a>*}
            {*</div>*}
        {*</div>*}
        {*<div class="four wide column">*}
            {*<h4 class="ui header">Services</h4>*}
            {*<div class="ui link list">*}
                {*<a href="#" class="item">Banana Pre-Order</a>*}
                {*<a href="#" class="item">DNA FAQ</a>*}
                {*<a href="#" class="item">How To Access</a>*}
                {*<a href="#" class="item">Favorite X-Men</a>*}
            {*</div>*}
        {*</div>*}
        {*<div class="eight wide column">*}
            {*<h4 class="ui inverted header">Мы в соц.сетях</h4>*}
            {*<p>группа вконтакте</p>*}
        {*</div>*}
    {*</div>*}
{*</div>*}
{*<div class="ui four column grid container">*}
    {*<div class="four wide column">*}
        {*<a href=""><img src="http://placehold.it/200x60"></a>*}
    {*</div>*}
    {*<div class="eight wide column">*}
        {*Копирайт*}
    {*</div>*}
    {*<div class="four right aligned wide column">*}
        {*Подпись разработчиков*}
    {*</div>*}
{*</div>*}

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <a href="/" class="footer-logo"></a>
            </div>
            <div class="col-xs-6">
                {if isset($banners[3]->text)}
                    <div class="footer-copyright">
                        {$banners[3]->text}
                    </div>
                {/if}
            </div>
            <div class="col-xs-3">
                {if isset($banners[14]->text)}
                    <div class="footer-cms">
                        {$banners[14]->text}
                    </div>
                {/if}
            </div>
        </div>
    </div>
</div>