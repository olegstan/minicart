{*{if !$mode || $mode == 'list'}*}
<ul class="list">
	{foreach $category->products as $product}
		{include file='parts/product-list.tpl'}
	{/foreach}
</ul>
{*{/if}*}

{*{if $mode == 'tile'}*}
{*<ul class="plitka">*}
	{*{$mh_group = 1}*}
	{*{foreach $products as $product}*}
		{*{include file='product-tile.tpl' mh_group=$mh_group}*}
		{*{if $product@iteration is div by 3}<div class="plitka-separator"></div>{$mh_group = $mh_group + 1}{/if}*}
	{*{/foreach}*}
{*</ul>*}
{*{/if}*}