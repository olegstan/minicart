{*<div class="ui message">*}
    {*<div class="header">Системное сообщение</div>*}
    {*<p>{$flash_message}</p>*}
{*</div>*}

<div class="alert alert-success">
    <strong>Поздравляем!</strong> Теперь, используя
    <a href="/account/">личный кабинет</a>, вы можете
    <a href="/account/?mode=personal-data">сменить личные данные</a> или
    <a href="/account/?mode=change-password">пароль</a>. Также Вам доступна <a href="/account/?mode=my-orders">история заказов</a> и
    <a href="/account/?mode=notification-settings">управление оповещениями на почту и SMS</a>.
</div>