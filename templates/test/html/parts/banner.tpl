<div class="left-main {$banners[$id]->css_class}">
    {if $banners[$id]->is_link && !empty($banners[$id]->link)}
        <a href="{$banners[$id]->link}" target="{if $banners[$id]->link_in_new_window}_blank{else}_parent{/if}" title="{$banners[$id]->name}">
    {/if}
            <div class="left-main-header">
                <span>{$banners[$id]->name}</span>
            </div>
            <div class="left-main-body">
                {$banners[$id]->text}
            </div>
    {if $banners[$id]->is_link && !empty($banners[$id]->link)}
        </a>
    {/if}
</div>