{*{$allow_edit_module = false}*}
{*if in_array($product_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if*}


{*{assign var="product_additional_class" value=""}*}
{*{if $product->badges}*}
{*{foreach $product->badges as $badge}*}
{*{if $badge->css_class_product}*}
	{*{if !empty($product_additional_class)}{assign var="product_additional_class" value=$product_additional_class|cat:' '}{/if}*}
	{*{assign var="product_additional_class" value=$product_additional_class|cat:"plitka-"}*}
	{*{assign var="product_additional_class" value=$product_additional_class|cat:($badge->css_class_product)}*}
{*{/if}*}
{*{/foreach}*}
{*{/if}*}

<li class="plitka-item{if $product_additional_class} {$product_additional_class}{/if}">
    {if $product->badges}
    	<div class="badge-overlay">
            <ol class="plitka-list-badges">
                {foreach $product->badges as $badge}
                    <li>
                        <span class="{$badge->css_class}">{$badge->name}</span>
                    </li>
                {/foreach}
            </ol>
        </div>
    {/if}

    <a href="{$product->front_url}" class="plitka-image producthref">
        <img src='{$product->image|resize:220:220}' alt='{$product->name}'>
	</a>

	<div class="plitka-name-block" {if $mh_group}data-mh="ngroup-{$mh_group}"{/if}>
        <a href="{$product->front_url}" class="plitka-name producthref">{$product->name}</a> {if $allow_edit_module} <span><a href="" class="edit btn btn-default btn-xs"><i class="fa fa-pencil"></i> Редактировать товар</a></span>{/if}
        {if $product->rating->rating_count > 0}
            <div class="rating-micro micro-rate{$product->rating->avg_rating*10}" title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}"></div>
        {/if}
    </div>

	<div class="plitka-description" {if $mh_group}data-mh="dgroup-{$mh_group}"{/if}>
		{$product->annotation}

		{$show_properties = false}
		{*{if $product->tags && $tags_groups}*}
			{*{foreach $tags_groups as $tag_group}*}
				{*{if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}*}
					{*{$show_properties = true}*}
				{*{/if}*}
			{*{/foreach}*}
		{*{/if}*}

		{if $product->tags && $tags_groups && $show_properties}
			<div class="plitka-properties">
				<table class="table table-condensed">
					<tbody>
					{foreach $tags_groups as $tag_group}
						{if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}
                            <tr>
                                <td>
                                    {$tag_group->name}{if $tag_group->help_text} <a class="property-help" data-type="qtip" data-content="{$tag_group->help_text|escape}" data-title="{$tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}
                                </td>
                                <td>
                                    {foreach $product->tags_groups[$tag_group->id] as $tag}
                                        {$tag->name}{if $tag_group->postfix} {$tag_group->postfix}{/if}{if !$tag@last}, {/if}
                                    {/foreach}
                                </td>
                            </tr>
						{/if}
					{/foreach}
				</table>
			</div>
		{/if}

        {*Артикул первого варианта товара*}
        {if $product->variant->sku}
            <div class="product-sku">Артикул: {$product->variant->sku}</div>
        {/if}

        {*Код товара*}
        {if $product->id}
            <div class="product-code">Код товара: {$product->id}</div>
        {/if}

	</div>


	<div class="plitka-statusbar">
		<a class="plitka-readmore producthref" href="{$product->front_url}">Подробнее</a>
		<div class="plitka-status">
			<span class="in-stock" {if $product->variant->stock <= 0}style="display:none;"{/if}><i class="fa fa-check"></i> Есть в наличии</span>
			<span class="out-of-stock" {if $product->variant->stock != 0}style="display:none;"{/if}><i class="fa fa-times-circle"></i> Нет в наличии</span>
			<span class="to-order" {if $product->variant->stock >= 0}style="display:none;"{/if}><i class="fa fa-truck"></i> Под заказ</span>
		</div>
	</div>

	<div class="plitka-buy-block">

        {if $product->variants_count > 1}
            <div class="form-group plitka-variants">
                <label>Варианты:</label>
                <select class="form-control input-sm" name="variant_id">
                    {foreach $product->variants as $variant}
                        <option value="$variant->id}" data-stock="{$variant->stock}" data-price="" data-price-convert="" data-price-old="" data-price-old-convert="" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if}>{$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
                    {/foreach}
                </select>
            </div>
        {else}
            <select name="variant_id" style="display: none;">
                <option value="$product->variant->id}">{$product->variant->name}</option>
            </select>
             {if $product->variant}<input type="hidden" name="variant_id" value="{$product->variant->id}"/>{/if}
        {/if}

        {*<div class="plitka-old-price" {if !$product->variant->price_old}style="display:none;"{/if}>{if $product->currency_id}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($product->variant->price_old*$product->min_amount)|convert:$product->currency_id}{else}{$product->variant->price_old|convert:$product->currency_id}{/if}{else}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($product->variant->price_old*$product->min_amount)|convert:$admin_currency->id}{else}{$product->variant->price_old|convert:$admin_currency->id}{/if}{/if}<span> {$main_currency->sign}</span></div>*}
        {*<div class="plitka-price" {if !$product->variant->price}style="display:none;"{/if}>{if $product->currency_id}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($product->variant->price*$product->min_amount)|convert:$product->currency_id}{else}{$product->variant->price|convert:$product->currency_id}{/if}{else}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($product->variant->price*$product->min_amount)|convert:$admin_currency->id}{else}{$product->variant->price|convert:$admin_currency->id}{/if}{/if}<span> {$main_currency->sign}</span></div>*}

        <a href="#" class="btn btn-success btn-sm plitka-buy" data-var-amount="{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{$product->min_amount}{else}1{/if}" {if $product->variant->stock == 0}style="display:none;"{/if}>Купить</a>
	</div>
</li>