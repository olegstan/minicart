{*<div class="wide column">*}
    {*<div class="ui card">*}
        {*<a class="image" href="#">*}
            {*<img src="{$product->image|resize:350:350}">*}
        {*</a>*}
        {*<div class="content">*}
            {*<a class="header" href="#">{$product->name}</a>*}
            {*<div class="description">*}
                {*{$product->annotation}*}
            {*</div>*}
            {*<div class="meta">*}
                {*Есть в наличии*}
            {*</div>*}
        {*</div>*}

        {*<div class="extra content">*}
            {*<div class="left floated">5100 Р </div>*}
            {*<div class="right floated">*}
                {*<button class="ui tiny button">Купить</button>*}
            {*</div>*}
        {*</div>*}
    {*</div>*}
{*</div>*}

{$allow_edit_module = false}
{*if in_array($product_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if*}

{*{assign var="product_additional_class" value=""}*}
{*{if $product->badges}*}
    {*{foreach $product->badges as $badge}*}
        {*{if $badge->css_class_product}*}
            {*{if !empty($product_additional_class)}{assign var="product_additional_class" value=$product_additional_class|cat:' '}{/if}*}
            {*{assign var="product_additional_class" value=$product_additional_class|cat:"list-"}*}
            {*{assign var="product_additional_class" value=$product_additional_class|cat:($badge->css_class_product)}*}
        {*{/if}*}
    {*{/foreach}*}
{*{/if}*}
<li class="listitem{if $product->css_class} {$product->css_class}{/if}{if $product_additional_class} {$product_additional_class}{/if}">

    <div class="list-left">
        <a href="{$product->front_url}" class="list-image producthref">
            <img src='{$product->image|resize:155:155}' alt='{$product->image->name}'>
        </a>
    </div>
    <div class="list-center">
        <div class="list-name-block">
            <a href="{$product->front_url}" class="list-name producthref">{$product->name}</a> {if $allow_edit_module} <span><a href="{$config->root_url}{$product_module_admin->url}{url add=['id'=>$product->id]}" class="edit btn btn-default btn-sm"><i class="fa fa-pencil"></i> Редактировать товар</a></span>{/if}
            {if $product->rating->rating_count > 0}
                <div class="rating-micro micro-rate{$product->rating->avg_rating*10}" title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}">
                </div>
            {/if}
        </div>
        {if $product->badges}
            <ol class="list-badges">
                {foreach $product->badges as $badge}
                    <li>
                        <span class="{$badge->css_class}">{$badge->name}</span>
                    </li>
                {/foreach}
            </ol>
        {/if}
        <div class="list-description">
            {$product->annotation}

            {$tags_groups_count = 0}
            {foreach $tags_groups as $tag_group}
                {if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}
                    {$tags_groups_count = $tags_groups_count + 1}
                {/if}
            {/foreach}

            {if $product->tags && $tags_groups && $tags_groups_count>0}
            <div class="list-properties">
                <table class="table table-condensed">
                    <tbody>
                    {foreach $tags_groups as $tag_group}
                        {if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}
                            <tr>
                                <td>{$tag_group->name}{if $tag_group->help_text} <a class="property-help" data-type="qtip" data-content="{$tag_group->help_text|escape}" data-title="{$tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}</td>
                                <td>
                                {foreach $product->tags_groups[$tag_group->id] as $tag}
                                {$tag->name}{if $tag_group->postfix} {$tag_group->postfix}{/if}{if !$tag@last}, {/if}
                                {/foreach}
                                </td>
                            </tr>
                        {/if}
                    {/foreach}
                </table>
            </div>
        {/if}

        {if $product->variant->sku}
            <div class="product-sku">Артикул: {$product->variant->sku}</div>
        {/if}

        {if $product->id}
            <div class="product-code">Код товара: {$product->id}</div>
        {/if}

        </div>
        <div class="list-additional">
            <a class="list-readmore producthref" href="{$product->front_url}">Подробнее</a>

            {if $product->reviews_count}
                <a class="list-reviews" href=""><i class="fa fa-comment-o"></i> <span>Отзывы ({$product->reviews_count})</span></a>
            {/if}

        </div>
    </div>


    <div class="list-right">
        <span class="in-stock product-status" {if $product->variant->stock <= 0}style="display:none;"{/if}><i class="fa fa-check"></i> Есть в наличии</span>
        <span class="out-of-stock product-status" {if $product->variant->stock != 0}style="display:none;"{/if}><i class="fa fa-times-circle"></i> Нет в наличии</span>
        <span class="to-order product-status" {if $product->variant->stock >= 0}style="display:none;"{/if}><i class="fa fa-truck"></i> Под заказ</span>

        {if $product->variants && !empty($product->variant->name)}
            <div class="form-group list-variants">
                <label>Варианты:</label>
                <select class="form-control input-sm" name="variant_id">
                    {foreach $product->variants as $variant}
                        <option value="{$variant->id}" data-stock="{$variant->stock}" data-price="{if $product->currency_id}
                        {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                            {($variant->price*$product->min_amount)|convert:$product->currency_id:false}
                        {else}
                            {$variant->price|convert:$product->currency_id:false}
                        {/if}

                            {else}
                                {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                                    {($variant->price*$product->min_amount)|convert:$admin_currency->id:false}
                                {else}
                                     {$variant->price|convert:$admin_currency->id:false}
                                {/if}
                            {/if}" data-price-convert="{if $product->currency_id}
                            {if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price*$product->min_amount)|convert:$product->currency_id}
                            {else}
                            {$variant->price|convert:$product->currency_id}{/if}{else}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price*$product->min_amount)|convert:$admin_currency->id}{else}{$variant->price|convert:$admin_currency->id}{/if}{/if}" data-price-old="{if $product->currency_id}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price_old*$product->min_amount)|convert:$product->currency_id:false}{else}{$variant->price_old|convert:$product->currency_id:false}{/if}{else}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price_old*$product->min_amount)|convert:$admin_currency->id:false}{else}{$variant->price_old|convert:$admin_currency->id:false}{/if}{/if}" data-price-old-convert="{if $product->currency_id}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price_old*$product->min_amount)|convert:$product->currency_id}{else}{$variant->price_old|convert:$product->currency_id}{/if}{else}{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{($variant->price_old*$product->min_amount)|convert:$admin_currency->id}{else}{$variant->price_old|convert:$admin_currency->id}{/if}{/if}" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if}>{$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
                    {/foreach}
                </select>
            </div>
        {else}
            <select name="variant_id" style="display: none;">
                <option value="{$product->variant->id}">{$product->variant->name}</option>
            </select>
            { {if $product->variant}<input type="hidden" name="variant_id" value="{$product->variant->id}"/>{/if} }
        {/if}

    <div class="list-old-price"
         {if !$product->variant->price_old}
            style="display:none;"
        {/if}>
        {if $product->currency_id}
            {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                {($product->variant->price_old*$product->min_amount)|convert:$product->currency_id}
            {else}
                {$product->variant->price_old|convert:$product->currency_id}{/if}
            {else}

            {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                {($product->variant->price_old*$product->min_amount)|convert:$admin_currency->id}
            {else}
                {$product->variant->price_old|convert:$admin_currency->id}
            {/if}
        {/if}
        <span> {$main_currency->sign}</span>
    </div>

    <div class="list-price"
         {if !$product->variant->price}
            style="display:none;"
        {/if}>

        {if $product->currency_id}
            {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                {($product->variant->price*$product->min_amount)|convert:$product->currency_id}
            {else}
                {$product->variant->price|convert:$product->currency_id}
            {/if}
        {else}
            {if $settings->catalog_use_variable_amount && $product->use_variable_amount}
                {($product->variant->price*$product->min_amount)|convert:$admin_currency->id}
            {else}
                {$product->variant->price|convert:$admin_currency->id}
            {/if}
        {/if}
        <span> {$main_currency->sign}</span>
    </div>


    <a href="#" class="btn btn-success btn-sm list-buy" data-var-amount="{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{$product->min_amount}{else}1{/if}" {if $product->variant->stock == 0}style="display:none;"{/if}>В корзину</a>

    {if $product->variant->stock != 0}
        {if $product->currency_id}
        <a href="#" class="buy-one-click" data-var-amount="{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{$product->min_amount}{else}1{/if}"  {if $product->variant->stock == 0}style="display:none;"{/if}>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden buy1spinner"></i></a>
        {else}
        <a href="#" class="buy-one-click" data-var-amount="{if $settings->catalog_use_variable_amount && $product->use_variable_amount}{$product->min_amount}{else}1{/if}" {if $product->variant->stock == 0}style="display:none;"{/if}>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden buy1spinner"></i></a>
        {/if}
    {/if}

    <div class="list-compare">
        <div class="buy-compare" {*if in_array($product->id, $compare_items)}style="display: none;"{/if*}><a href="{*$config->root_url}{$compare_module->url}{url add=['action'=>'add', 'product_id'=>$product->id, 'ajax'=>1]*}" class="dotted add_to_compare">Сравнить</a></div>
        {*Ссылка ниже появляется когда что нибудь закидываем в сравнение*}

        {*
        <div class="buy-compare" {if !in_array($product->id, $compare_items)}style="display:none;"{/if}><a class="compare_items" href="{$config->root_url}{$compare_module->url}">Сравнить ({$compare_items|count})</a> или <a href="{$config->root_url}{$compare_module->url}{url add=['action'=>'delete', 'product_id'=>$product->id, 'ajax'=>1]}" class="dotted remove_from_compare">Убрать из сравнения</a></div>
        *}
    </div>

    {*if $favorite_mode}
        <a href="#" class="dotted2 delete-from-favorite" data-id="{$product->id}"><i class="fa fa-heart"></i> <span>Удалить из избранного</span></a>
    {/if*}
    </div>
</li>