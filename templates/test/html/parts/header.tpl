{*<div class="ui four column grid container">*}
    {*<div class="twelve wide column">*}
        {*<div class="ui secondary  menu">*}
            {*<a class="active item">Главная </a>*}
            {*<a class="item">О компании </a>*}
            {*<a class="item">Доставка </a>*}
        {*</div>*}
    {*</div>*}
    {*<div class="four wide column">*}
        {*<div class="ui secondary  menu">*}
            {*<div class="right menu">*}
                {*<a href="" title="Избранное" class="item"><i class="empty heart icon"></i></a>*}
                {*<a href="" title="Избранное" class="item"><i class="list icon"></i></a>*}
                {*<a class="ui item">Войти с паролем</a>*}
            {*</div>*}
        {*</div>*}
    {*</div>*}
{*</div>*}
{*<div class="ui four column grid container">*}
    {*<div class="four wide column">*}
        {*<div class="ui segment">Лого</div>*}
    {*</div>*}
    {*<div class="five wide column">*}
        {*<div class="ui fluid action input">*}
            {*<input type="text" placeholder="Поиск...">*}
            {*<div class="ui button">Поиск</div>*}
        {*</div>*}
    {*</div>*}
    {*<div class="three wide column">*}
        {*<div class="ui segment">Корзина</div>*}
    {*</div>*}
    {*<div class="four wide column">*}
        {*<div class="ui segment">Телефоны в шапке</div>*}
    {*</div>*}
{*</div>*}
{*<div class="ui column grid container">*}
    {*<div class="wide column">*}
        {*<div class="ui menu">*}
            {*<a class="item">Категория 1 </a>*}
            {*<a class="item">Категория 2 </a>*}
            {*<a class="item">Категория 3 </a>*}

            {*<div class="ui right dropdown item">*}
                {*Выпадающий список*}
                {*<i class="dropdown icon"></i>*}
                {*<div class="menu">*}
                    {*<div class="item">Applications</div>*}
                    {*<div class="item">International Students</div>*}
                    {*<div class="item">Scholarships</div>*}
                {*</div>*}
            {*</div>*}
        {*</div>*}
    {*</div>*}
{*</div>*}

<div class="topline">
    <div class="container nopadding">
        <div class="row topline-links">
            <div class="col-xs-9">
                <nav class="navbar navbar-default" role="navigation" id="topmenu">
                    {include file="parts/horizontal-menu.tpl" items=$company_menu->items}
                </nav>
            </div>
            <div class="col-xs-3">
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown cart-dropdown" id="cart-placeholder">
                        {*include file='cart-informer.tpl'*}
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <header>
        <div class="row">
            <div class="col-xs-3">
                <a href="/" class="logo"></a>
            </div>
            <div class="col-xs-6">

                <div class="row toplinks">
                    <div class="col-xs-6">

                        <ul>
                            {if !$user}
                                <li class="login-register-inline"><a href="{route controller='user' action='login'}">Войти с паролем</a></li>
                                <li class="login-register-inline"> | <a href="{route controller='user' action='register'}">Регистрация</a>
                                </li>
                            {else}
                                {* Этот блок показываем когда клиент залогинился *}
                                <li class="login-register-inline">
                                    <a href="">Личный кабинет</a>
                                </li>
                                |
                                <li class="login-register-inline">
                                    <i class="fa fa-power-off"></i> <a
                                            href="{*url add=['logout'=>1]*}">Выход</a>
                                </li>
                            {/if}
                        </ul>
                    </div>

                    <div class="col-xs-6 taright">
                        <div class="compare-body" {if !$compare_href_show}style="display:none;"{/if}>
                            <i class="fa fa-bars"></i> <a href="{$compare_href}" id="make-compare">Сравнить ({$compare_items|count})</a>
                        </div>

                        <div class="favorite-products" {if !$user || $favorites_products_count == 0}style="display: none;"{/if}>
                            <i class="fa fa-heart"></i> <a href="{*$config->root_url*}{$account_module->url}{*url add=['mode'=>'my-favorites']*}" id="favorites-informer">Избранное ({$favorites_products_count})</a>
                        </div>
                    </div>


                </div>
                <div id="search">
                    <form role="search">
                        <button id="searchCancelButton" class="searchclear" type="button serchremove" {if !$keyword} style="display:none;"{/if}><i class="fa fa-times"></i></button>
                        <input id="searchField" type="text" class="form-control" placeholder="{$settings->search_placeholder}" autocomplete="off" value="{$keyword}">
                        <button id="searchButton" type="button" class="btn btn-default">Найти</button>
                    </form>
                    <div id="refreshpart" class="addcontainer" style="display:none;"></div>

                    {* Для шаблона Modern скрываем поисковые подсазки, поскольу нужноместо для Личного кабинета *}
                    {*
                    {if $settings->search_help_text1 || $settings->search_help_text2}
                        <div class="search-helper">
                            Например, {if $settings->search_help_text1}<span id="help_msg1">{$settings->search_help_text1}</span>{/if} {if $settings->search_help_text1 && $settings->search_help_text2}или{/if} {if $settings->search_help_text2} <span id="help_msg2">{$settings->search_help_text2}</span>{/if}
                        </div>
                    {/if}
                    *}
                </div>
            </div>
            <div class="col-xs-3">
                {if $banners[2]->text}
                    <div class="phone">
                        <div class="number">{$banners[2]->text}</div>
                        <span>
                            {if $banners[12]->text}
                                {$banners[12]->text}
                            {/if} | <i class="fa fa-phone"></i> <a href="#" id="make-callback">Перезвонить?</a>
                        </span>
                    </div>
                {/if}
            </div>
        </div>
    </header>

    {* Горизонтальное меню каталога товаров начало *}

    {*<nav class="navbar white-navbar" role="navigation">*}
        {*<ul class="nav navbar-nav white-menu" id="categories-menu">*}
            {*{function name=categories_tree_menu level=0}*}
                {*{foreach $items as $item}*}
                    {*{if $item->is_visible}*}
                        {*{if $item->subcategories}*}
                            {*<li class="menu-item dropdown level{$level} {if $level>0}dropdown-submenu{/if} {if $item->id == $category_id}active{/if}">*}
                                {*<a href="*}{*$config->root_url*}{*{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}{if $level == 0}<b class="caret"></b>{/if}</a>*}
                                {*<ul class="dropdown-menu dropdown{$level} {$item->css_class}">*}
                                    {*{categories_tree_menu items=$item->subcategories level=$level+1}*}
                                {*</ul>*}
                            {*</li>*}
                        {*{else}*}
                            {*<li class="menu-item level{$level} {if $item->id == $category_id}active{/if}"><a href="*}{*$config->root_url*}{*{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}</a></li>*}
                        {*{/if}*}
                    {*{/if}*}
                {*{/foreach}*}
            {*{/function}*}
            {*{categories_tree_menu items=$categories_frontend_all level=0}*}
        {*</ul>*}
    {*</nav>*}

    {* Горизонтальное меню каталога товаров конец *}



    {********************* Мегаменю НАЧАЛО *************************}

    {include file="parts/megamenu.tpl"}

    {********************* Мегаменю КОНЕЦ *************************}

    {* Объявление на сайте *}

    {if isset($banners[4]->text)}
        <div class="panel panel-default topnews">
            <div class="panel-body">
                {$banners[4]->text}
            </div>
        </div>
    {/if}

    {* flash_message *}
        {if isset($flash_message)}
            {include file='parts/flash.tpl'}
        {/if}
    {* flash_message *}

    <div class="row" id="main-container">

        {$show_filter = false}
        {$show_categories = false}
        {if $filter_tags_groups}{$show_filter = true}{/if}
        {if !empty($current_categories_frontend)}{$show_categories = true}{/if}

        <div class="col-xs-3" id="main-left-container"
                {* ЕСЛИ ТРЕБУЕТСЯ СКРЫВАТЬ ЛЕВУЮ КОЛОНКУ КОГДА НЕТ КАТЕГОРИЙ И ФИЛЬТРА ТО ДОБАВИТЬ В УСЛОВИЕ СЛЕДУЮЩЕЕ *}
                {*|| ($module->module == "ProductsController" && !$show_filter && !$show_categories)*}

                {if $module->module=='CartController' || $module->module=='CompareController' || $module->module=='OrderController' || $module->module=='AccountController' || $module->module=='ProductController' || $module->module=="MainController" || $module->module=="LoginController" || $module->module=="RegisterController" || $module->module == "ReviewsController" || $module->module=="SitemapViewController"}style='display:none;'{/if}>
            {* Список категорий для результатов поиска начало*}
            <div id="searchcategories" {if !$finded_categories}style="display:none;"{/if}>
                {if $finded_categories}
                <div class="searchcats">Категории:</div>
                <ol>
                    {*<li>
                        <div>
                            <span></span>
                            <a class="menu61 {if !$category_id}active{/if}" href="{*$config->root_url*}{$products_module->url}{*url add=['format'=>'search', 'keyword'=>$keyword]*}">Все</a>
            </div>
            </li>*}
            {foreach $finded_categories as $categories_position}
                {foreach $categories_position as $cat}
                    <li>
                        <div>
                            <span>{$cat.count}</span>
                            <a class="menu61 {if $cat.id == $category_id}active{/if}"
                               href="{$cat.url}">{$cat.name}</a>
                        </div>
                    </li>
                {/foreach}
            {/foreach}
                </ol>
            {/if}

        </div>
        {* Список категорий для результатов поиска конец *}

        {* Список подкатегорий *}
        {if $show_categories}
            <div id="categories">
                <div class="categories-heading">{$category_header->name}</div>
                <div class="categories-body">
                    {* Дерево строится на аяксе *}
                    {$olclass = ''}
                    {foreach $current_categories_frontend[0].subcategories as $subcategory1}
                        {if $subcategory1.folder}
                            {$olclass = 'class="havechilds"'}
                        {/if}
                    {/foreach}
                    <ol {$olclass}>
                        {function name=categories_tree level=1}
                            {foreach $items as $item}
                                <li class="{if !$item.subcategories}collapsed{/if} {if $item.folder}havechild{/if}"
                                    data-id="{$item.id}"
                                    data-level="{$level}">
                                    {if $item.folder}
                                        <i class="fa fa-minus"
                                           style="{if $item.subcategories}display: block;{else}display: none;{/if}"></i>
                                        <i class="fa fa-plus"
                                           style="{if $item.subcategories}display: none;{else}display: block;{/if}"></i>
                                        <i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
                                    {/if}
                                    <div>
                                        <span>{$item.products_count}</span>
                                        <a href="{*$config->root_url*}{$products_module->url}{$item.url}"
                                           class="menu{$item.id} menu-level{$level} {if $item.id == $category_id}active{/if}">{$item.title}</a>
                                    </div>
                                    {if $item.subcategories}
                                        <ol style="display: block;">
                                            {categories_tree items=$item.subcategories level=$level+1}
                                        </ol>
                                    {/if}
                                </li>
                            {/foreach}
                        {/function}
                        {categories_tree items=$current_categories_frontend[0].subcategories}
                    </ol>
                </div>
            </div>
        {/if}
        {* Список подкатегорий конец *}

        {*include file='filter.tpl'*}

        {if $module->module=='PagesController'}

            <div class="materials-menu">
                <div class="materials-menu-header">
                    {*menuname id=1*}
                </div>
                {*menu id=1 menu_type='vertical'*}
            </div>
        {/if}

        {* Местро под дополнительный модуль под Новостями *}
        {*if {banner id=7}}
            <div class="left-module">
                {banner id=7}
            </div>
        {/if*}

    </div>

    {* ЕСЛИ ТРЕБУЕТСЯ СКРЫВАТЬ ЛЕВУЮ КОЛОНКУ КОГДА НЕТ КАТЕГОРИЙ И ФИЛЬТРА ТО ДОБАВИТЬ В УСЛОВИЕ СЛЕДУЮЩЕЕ *}
    {*|| ($module->module == "ProductsController" && !$show_filter && !$show_categories)*}

    <div class="col-xs-12" id="main-right-container">
        <div id="content">
            {$content}
        </div>
    </div>

</div>