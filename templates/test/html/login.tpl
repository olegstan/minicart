<h4 class="ui horizontal divider header">Страница входа на сайт </h4>
<div class="ui  container">

    <h1>Вход на сайт</h1>

    <form id="login-form" action="user/login" data-validate-action="user/validate-login" method="POST" novalidate>
        <div class="ui three column grid ">
            <div class="column">
                <div class="ui form">
                    <div class="field">
                        <label>Почта или номер мобильного</label>
                        <div class="ui left icon input">
                            <input id="login-form-username" type="text" name="username">
                            <i class="user icon"></i>
                        </div>
                    </div>
                    <div class="field">
                        <label>Пароль</label>
                        <div class="ui left icon input" data-content="Add users to your feed">
                            <input id="login-form-password" type="password" name="password" autocomplete="off">
                            <i class="lock icon"></i>
                        </div>
                    </div>
                    <button class="big fluid ui blue submit button">Войти</button>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="ui divider"></div>
