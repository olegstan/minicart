$(document).ready(function(){

{* развернуть категорию *}
$('#categories').on('click', 'i.fa-plus', function(){

	var li = $(this).closest('li');

	{* Свернем другие категории *}
	$('#categories > ol > li:not(.collapsed) {$nested_level}').each(function(){
		var li_find = $(this).find('li[data-id='+li.attr('data-id')+']');
		if (li_find.length == 0)
		{
			var icon_minus = $(this).find('i.fa-minus');
			if (icon_minus.length > 0)
				icon_minus.trigger('click');
		}
	});
	
	var ol = li.find('ol:first');
	var icon_col = li.find('i.fa-minus:first');
	var icon_exp = li.find('i.fa-plus:first');
	var icon_spin = li.find('i.fa-spinner:first');
	
	icon_exp.hide();
	
	{* Подгрузим на ajax'e дерево *}
	if (ol.length == 0)
	{
		icon_spin.show();
		print_lazy_result(li, li.attr('data-id'), icon_col, icon_spin);
	}
	else
	{
		ol.show();
		icon_col.show();
	}
	
	li.removeClass('collapsed');
});

{* свернуть категорию *}
$('#categories').on('click', 'i.fa-minus', function(){
	var li = $(this).closest('li');
	var ol = li.find('ol:first');
	var icon_col = li.find('i.fa-minus:first');
	var icon_exp = li.find('i.fa-plus:first');
	
	ol.hide();
	li.addClass('collapsed');
	icon_col.hide();
	icon_exp.show();
});

$('body').on('click', 'a', function(e){
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open($(this).attr('href'), '_blank');
		return false;
	}
});

{* выбор категории *}
$('#categories').on('click', 'a', function(e){

	/**if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open($(this).attr('href'), '_blank');
		return false;
	}**/
	
	return true;

	var li = $(this).closest('li');
	var ol = li.find('ol:first');
	var icon_col = li.find('i.fa-minus:first');
	var icon_exp = li.find('i.fa-plus:first');
	var icon_spin = li.find('i.fa-spinner:first');
	var id = li.attr('data-id');
	if (id == undefined)
		return false;
	id = parseInt(id);
	var params = [];
	$('#brands input:checked').each(function(){
		$(this).prop('checked', false);
	});
	if (id > 0)
	{
		params.push({
			'key': 'url',
			'value': $(this).attr('href')
		});
	}
	else
	{
		{* Свернем другие категории *}
		$('#categories li:not(.collapsed) {$nested_level}').each(function(){
			var icon_minus = $(this).find('i.fa-minus');
			icon_minus.trigger('click');
		});
	}
	
	{* Свернем другие категории *}
	$('#categories > ol > li:not(.collapsed) {$nested_level}').each(function(){
		var item = $(this).find('li[data-id='+id+']');
		if (item.length == 0)
		{
			var icon_minus = $(this).find('i.fa-minus');
			if (icon_minus.length > 0)
				icon_minus.trigger('click');
		}
	});
	
	products_request_ajax(params);
	return false;
});

{* нажатие кнопки "Показать еще" *}
$('#content').on('click', 'a.show-more', function(e){
	var url = $(this).attr('href');
	
	$('#content ul.pagination:last').closest('div.row').remove();
	$('#content a.show-more').remove();
	
	{if !$mode || $mode == 'list'}
		$('<div class="loading"><div class="loader"></div></div>').insertAfter($('#content ul.list:last'));
	{else}
		$('<div class="loading"><div class="loader"></div></div>').insertAfter($('#content ul.plitka:last'));
	{/if}
	
	var params = [{ 'key': 'url', 'value': url}];
	products_request_ajax(params, undefined, undefined, undefined, false, undefined, true);
	return false;
});

{* переход по страницам *}
$('#content').on('click', 'ul.pagination a', function(e){
	var url = $(this).attr('href');
	var type = $(this).data('type');
	var page = $(this).data('page');
	if (url == undefined)
		return false;
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open(url, '_blank');
		return false;
	}
	
	switch(type){
		case 'category':
			var params = [{ 'key': 'url', 'value': url}];
			products_request_ajax(params);
			break;
		case 'material-category':
			document.location.href = url;
			//material_request_ajax(url);
			break;
		case 'brand':
			var params = [{ 'key': 'url', 'value': url}];
			brand_request_ajax(params);
			break;
		case 'account_orders':
			return true;
		case 'account_reviews':
			return true;
		case 'reviews':
			
			$('html, body').animate({
				scrollTop: $("#reviews-list").offset().top
			}, 0);
			
			var sort_method = $('#reviews-list div.rate-sort-item.active');
			
			var mainUrl = "{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}?tab=reviews&page="+page;
			if (sort_method.length>0)
				mainUrl += "&sort=" + sort_method.data('sort');
			if (typeof history.pushState != undefined) {
				history.pushState({ mode: 'product', url: mainUrl},null,encodeURI(decodeURI(mainUrl)));
			}
		
			$.get(url, function(data){
				$('#reviews-list').html(data);
				
				$('#user-rating2-1, #user-rating2-2').rating({
					fx: 'full',
					image: '{$path_frontend_template}/img/stars/big.png',
					loader: '<i class="fa fa-spinner fa-spin"></i>',
					minimal: 1,
					{*url: '{$config->root_url}{$product_module->url}',*}
					preselectMode: true,
					click: function(data){
						var index = $(this).closest('.my-review').data('index');
						$('#rating-block-error-'+index).hide();
						$('#reviews-list input[name=rating]').val(data);
					}
				});
			});
			break;
	}
	return false;
});

{* сортировка *}
$('#content').on('click', '#sort li a', function(){
	var url = $(this).attr('href');
	var type = $(this).attr('data-type');
	if (url == undefined)
		return false;
		
	switch(type){
		case 'category':
			var params = [{ 'key': 'url', 'value': url}];
			products_request_ajax(params);
			break;
		case 'material-category':
			document.location.href = url;
			//material_request_ajax(url);
			break;
		case 'brand':
			var params = [{ 'key': 'url', 'value': url}];
			brand_request_ajax(params);
			break;
		case 'account_orders':
			return true;
	}
	return false;
});

{* изменение режима просмотра *}
$('#content').on('click', '#view a', function(){
	var url = $(this).attr('href');
	var type = $(this).attr('data-type');
	if (url == undefined)
		return false;
		
	switch(type){
		case 'category':
			var params = [{ 'key': 'url', 'value': url}];
			products_request_ajax(params);
			break;
		case 'material-category':
			document.location.href = href;
			//material_request_ajax(url);
			break;
		case 'brand':
			var params = [{ 'key': 'url', 'value': url}];
			brand_request_ajax(params);
			break;
		case 'account_orders':
			return true;
	}
	return false;
});

{* Нажатие кнопки сбросить фильтр *}
$('#wrap').on('click', '#clear-filter', function(){
	var params = [];
	params.push({
		'key': 'url',
		'value': $('div#filter').attr('data-url')
	});
	$('div#filter select').each(function(){
		$(this).val(0);
	});
	$('div#filter input:checked').each(function(){
		$(this).prop('checked', false);
	});
	$('div#filter input[data-control=rangeSlider]').each(function(){
		$(this).rangeSlider("updateSilent",{
			from: $(this).attr('data-min'),
			to: $(this).attr('data-max')
		});
	});
	products_request_ajax(params, undefined, undefined, true);
	return false;
});

{* Нажатие кнопки применить фильтр *}
$('#wrap').on('click', '#apply_products_filter', function(event, scroll_up){
	var params = [];
	$('#filter_spinner').show();
	var scroll_to_up = (scroll_up == undefined || scroll_up == true) ? true : false;
	params.push({
		'key': 'url',
		'value': $('div#filter').attr('data-url')
	});
	$('div#filter select').each(function(){
		if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
	});
	$('div#filter input:checked:visible').each(function(){
		if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
	});
	$('div#filter input[data-control=rangeSlider]').each(function(){
		if ($(this).val().length != 0 && $(this).val() != ($(this).attr('data-min')+';'+$(this).attr('data-max')))
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
			
	});
	products_request_ajax(params, undefined, undefined, true, scroll_to_up, false);
	return false;
});

function request_filter_products_count(){
	var params = [];
	params.push({
		'key': 'url',
		'value': $('div#filter').attr('data-url')
	},{
		'key': 'format',
		'value': 'products_count'
	});
	$('div#filter select').each(function(){
		if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
	});
	$('div#filter input:checked:visible').each(function(){
		if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
	});
	$('div#filter input[data-control=rangeSlider]').each(function(){
		if ($(this).val().length != 0 && $(this).val() != ($(this).attr('data-min')+';'+$(this).attr('data-max')))
			params.push({
				'key': $(this).attr('name'),
				'value': $(this).val()
			});
			
	});
	products_request_ajax(params, undefined, undefined, true, false, false);
	return false;
}

$('#wrap').on('change', 'div#filter input, div#filter select', function(){
	{*С запросом количества товаров, удовлетворяющих фильтру*}
	{*request_filter_products_count();*}
	{*Моментальное применение фильтра*}
	$('#apply_products_filter').trigger('click', false);
});

function rangeSliderStart(){
	$('input[data-control=rangeSlider]').rangeSlider({
		onChange: function(obj){        // function-callback, is called on every change
			if (!obj.input.data("disable"))
			{
				var i1 = $('#' + obj.input.attr('data-id') + "_min");
				var i2 = $('#' + obj.input.attr('data-id') + "_max");
			
				value_array = obj.input.val().split(";");
				i1.val(value_array[0]);
				i2.val(value_array[1]);
			}
		},
		onFinish: function(obj){
			{*var params = [];
			params.push({
				'key': 'url',
				'value': $('div#filter').attr('data-url')
			},{
				'key': 'format',
				'value': 'products_count'
			});
			$('div#filter select').each(function(){
				if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
			});
			$('div#filter input:checked:visible').each(function(){
				if ($(this).val().length != 0 && parseInt($(this).val()) != 0)
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
			});
			$('div#filter input[data-control=rangeSlider]').each(function(){
				if ($(this).val().length != 0 && $(this).val() != $(this).attr('data-min')+';'+$(this).attr('data-max'))
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
		
			});
			products_request_ajax(params, undefined, undefined, true);*}
			
			$('#apply_products_filter').trigger('click', false);
		}
	});
}
rangeSliderStart();

$('.container').on('click', 'div.selected-tags span.tag i.fa-times', function(){
	var mode = $(this).closest('span').data('mode');
	var name = $(this).closest('span').data('name');
	var tag = $(this).closest('span').data('tag');
	
	switch(mode){
		case "range":
			var object = $('#filter input[name='+name+']');
			object.rangeSlider("updateSilent",{
				from: object.data('min'),
				to: object.data('max')
			});
			break;
		case "checkbox":
			var object = $('#filter input[name='+name+'][value='+tag+']');
			object.prop('checked', false);
			break;
		case "radio":
			var object = $('#filter input[name='+name+'][value='+tag+']');
			object.prop('checked', false);
			object = $('#filter input[name='+name+'][value=0]');
			object.prop('checked', true);
			break;
		case "select":
			var object = $('#filter select[name='+name+']');
			object.val(0);
			break;
		case "logical":
			var object = $('#filter input[name='+name+']');
			object.prop('checked', false);
			break;
	}
	
	request_filter_products_count();
	$('#apply_products_filter').trigger('click', false);
	
	$(this).closest('span').remove();
	
	return false;
});

$('#main-container').on('keyup', 'input.rangeSlider' , function(){
	var slider = $('#'+$(this).attr('data-object'));
	value_array = slider.val().split(";");
	var i1_value = parseInt(value_array[0]);
	var i2_value = parseInt(value_array[1]);
	var min_value = parseInt(slider.attr('data-min'));
				var max_value = parseInt(slider.attr('data-max'));
	var from_value = parseInt($('#'+slider.attr('data-id')+'_min').val());
	var to_value = parseInt($('#'+slider.attr('data-id')+'_max').val());

	var new_value = parseInt($(this).val());

	if ($(this).attr('data-control') == "from")
	{
		if (new_value > i2_value)
			new_value = i2_value;
		if (new_value < min_value || !new_value)
			new_value = min_value;
		if (i2_value != to_value && (new_value <= to_value) && (to_value <= max_value))
			i2_value = to_value;
		slider.rangeSlider("updateSilent",{
			from: new_value,
			to: i2_value
		});
	}
	if ($(this).attr('data-control') == "to")
	{
		if (new_value < i1_value)
			new_value = i1_value;
		if (new_value > max_value || !new_value)
			new_value = max_value;
		if (i1_value != from_value && (min_value <= from_value) && (from_value <= new_value))
			i1_value = from_value;
		slider.rangeSlider("updateSilent",{
			from: i1_value,
			to: new_value
		});
	}
});

{* Изменение параметров фильтра
$('#content').on('change', 'form#form_tags select', function(){
	$('#apply_products_filter').trigger('click');
	return false;
});*}

{* Выбор бренда *}
$('#brands').on('click', 'a', function(e){
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open($(this).attr('href'), '_blank');
		return false;
	}
	
	$("body,html").animate({
		scrollTop:0
	}, 0);
	
	var params = [];
	params.push({
		'key': 'url',
		'value': $(this).attr('href')
	});
	
	brand_request_ajax(params);
	return false;
});

{* Переход по ссылке на карточку товара *}
$('#content').on('click', 'a.producthref', function(e){
	var url = $(this).attr('href');
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open(url, '_blank');
		return false;
	}
	
	$("body,html").animate({
		scrollTop:0
	}, 0);
	product_request_ajax(url);
	return false;
});

{* Переход по ссылке на материал *}
$('#content').on('click', 'a.materialhref', function(e){
	var url = $(this).attr('href');
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open(url, '_blank');
		return false;
	}	
	
	$("body,html").animate({
		scrollTop:0
	}, 0);
	material_request_ajax(url);
	return false;
});

$('#wrap').on('click', '#make-order, #cart', function(e){
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open('{$config->root_url}{$cart_module->url}', '_blank');
		return false;
	}
	
	if ($('#make-order').length > 0)
		cart_request_ajax();
	return false;
});

$('#make-compare').click(function(){
	compare_request_ajax();
	return false;
});

{* развернуть вертикальное меню *}
$('div.vertical-menu').on('click', 'i.fa-plus', function(){

	var li = $(this).closest('li');
	var ol = $(this).closest('div.vertical-menu');

	{* Свернем другие категории *}
	{*$('ol.vertical-menu > li:not(.collapsed)').each(function(){*}
	ol.find('> li:not(.collapsed)').each(function(){
		var li_find = $(this).find('li[data-id='+li.attr('data-id')+']');
		if (li_find.length == 0)
		{
			var icon_minus = $(this).find('i.fa-minus');
			if (icon_minus.length > 0)
				icon_minus.trigger('click');
		}
	});
	
	var ol = li.find('ol:first');
	var icon_col = li.find('i.fa-minus:first');
	var icon_exp = li.find('i.fa-plus:first');
	var icon_spin = li.find('i.fa-spinner:first');
	
	icon_exp.hide();
	
	ol.show();
	icon_col.show();
	
	li.removeClass('collapsed');
});

{* свернуть вертикальное меню *}
$('div.vertical-menu').on('click', 'i.fa-minus', function(){
	var li = $(this).closest('li');
	var ol = li.find('ol:first');
	var icon_col = li.find('i.fa-minus:first');
	var icon_exp = li.find('i.fa-plus:first');
	
	ol.hide();
	li.addClass('collapsed');
	icon_col.hide();
	icon_exp.show();
});

{* хлебные крошки *}
$('#content').on('click', 'ol.breadcrumb a', function(e){
	var url = $(this).attr('href');
	var type = $(this).attr('data-type');
	if (url == undefined)
		return false;
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open(url, '_blank');
		return false;
	}
	
	switch(type){
		case 'category':
			var params = [{
					'key': 'url',
					'value': url
				}];
			products_request_ajax(params);
			break;
		case 'material-category':
			material_request_ajax(url);
			break;
		default:
			location.href = url;
			break;
	}
	
	return false;
});

$('ul.horizontal-menu a, #categories-menu a, #categories-menu-columns a').click(function(e){
	var type = $(this).attr('data-type');
	var href = $(this).attr('href');
	
	if (e.ctrlKey)	{*При зажатом Ctrl открываем ссылку в новом окне*}
	{
		var win = window.open(href, '_blank');
		return false;
	}
	
	switch(type){
		case 'category':
			var params = [{ 'key': 'url', 'value': href}];
			products_request_ajax(params);
			break;
		case 'product':
			product_request_ajax(href);
			break;
		case 'material':
			document.location.href = href;
			//material_request_ajax(href);
			break;
		case 'material-category':
			document.location.href = href;
			//material_request_ajax(href);
			break;
		case 'href':
			document.location.href = href;
			break;
		case 'sitemap':
			document.location.href = href;
			break;
	}
	
	{*Код от полного шаблона default*}
	{*if (type != 'separator')
	{
		$(this).closest('ul').find('li.active').each(function(){
			$(this).removeClass('active');
		});
		
		$(this).closest('li').addClass('active');
		
		$('#main-left-container').show();
		if ($('#main-right-container').hasClass('col-xs-12'))
		{
			$('#main-right-container').removeClass('col-xs-12');
			$('#main-right-container').addClass('col-xs-9');
		}
	}*}
	if (type != 'separator')
	{
		$(this).closest('ul').find('li.active').each(function(){
			$(this).removeClass('active');
		});
		
		$(this).closest('li').addClass('active');
	}
	return false;
});

$('ul.horizontal-menu li.dropdown, #categories-menu li.dropdown, #categories-menu-columns li.dropdown').hover(function() {
	$(this).find('.dropdown-menu:first').stop(true, true).show();
}, function() {
	$(this).find('.dropdown-menu:first').stop(true, true).hide();
});

$('#cart-placeholder').hover(function() {
	$(this).find('.dropdown-menu:first').stop(true, true).show();
	$('#cart').addClass('hover');
}, function() {
	$(this).find('.dropdown-menu:first').stop(true, true).hide();
	$('#cart').removeClass('hover');
});

$('#cart-placeholder').on('click', 'a.incart', function(){
	var href = $(this).attr('href');
	var li = $(this).closest('li');
	var a_href = li.find('div.cart-product-name a.producthref');
	$.get(href, function(data){
		if (data.success)
		{
			if ($('#cart-placeholder li.in-cart-item').length == 1)
			{
				$('#cart').replaceWith(data.header);
				$('#cart-placeholder ul').remove();
				
				if ($('table.carttable').length > 0)
					$('table.carttable').closest('form').html('Корзина пуста');
			}
			else
			{
				li.remove();
				
				$('#cart').replaceWith(data.header);
				$('#cart-placeholder div.cart-intotal span').html(data.total_price_display + '<span class="currency">{$main_currency->sign}</span>');
				
				var tr = $('#content table.carttable div.carttable-product-name a[href="'+a_href.attr('href')+'"]');
				if (tr.length > 0)
				{
					tr.closest('tr').html('<td class="carttable-info" colspan="5">Товар '+a_href[0].outerHTML+' был удален из корзины.</td>');
					update_price();
				}	
			}
		}
	});
	return false;
});

$('#searchButton').click(function(){
	var curli = $('#refreshpart li.selected');
	if (curli.length > 0)
		curli.click();
	else
	{
		add_to_search_history($('#searchField').val());
		var params = [{ 'key': 'keyword', 'value': encodeURIComponent($('#searchField').val())}, { 'key': 'format', 'value': 'search'}];
		products_request_ajax(params, true, true);
		$('#searchField').val('');
		$('#searchField').blur();
		$('#refreshpart').hide();
		$('#searchCancelButton').hide();
	}
	return false;
});

{* нажатие Enter в поле поиска *}
$('#searchField').on('keydown', function(e){
	if (e.keyCode == 13)
	{
		$('#categories').hide();
		$('#categories-menu-columns li.active').removeClass('active');
		
		$('#searchButton').trigger('click');
		return false;
	}
});

{* поиск keyword *}
$('#searchField').on('keyup', function(e){
	var keyword = "";
	
	if ($(this).val().length > 0)
		$('#searchCancelButton').show();
	else
		$('#searchCancelButton').hide();
	
	{* down or up *}
	if (e.keyCode == 40 || e.keyCode == 38)
	{
		var curli = $('#refreshpart li.selected');
		
		if (e.keyCode == 40)
		{
			var nextli = curli.next('li:not(.object-header)');
			if (nextli.length == 0)
				nextli = $('#refreshpart li:not(.object-header):first');
		}
		if (e.keyCode == 38)
		{
			var nextli = curli.prev(':not(.object-header)');
			if (nextli.length == 0)
				nextli = $('#refreshpart li:not(.object-header):last');
		}	
			
		curli.removeClass('selected');
		nextli.addClass('selected');
	}
	else
	{
		if ($(this).val().length > 0)
		{
			$('#refreshpart').show();
			$('#searchCancelButton').show();
			keyword = $(this).val();
			
			if ($('#refreshpart').scrollTop() > 0)
				$('#refreshpart').scrollTop(0);
		}
		else
		{
			$('#refreshpart').hide();
		}
		search_request_ajax(keyword);
	}
});

$('#searchCancelButton').click(function(){
	$('#searchField').val('');
	search_request_ajax();
	$('#refreshpart').hide();
	$(this).hide();
	return false;
});

$('#refreshpart').on('mouseenter', 'li:not(.object-header)', function(){
	$('#refreshpart li.selected').removeClass('selected');
	$(this).addClass('selected');
});

$('#refreshpart').on("click", "li:not(.object-header)[data-id]", function(){
	var li = $(this);
	var object_id = li.attr('data-id');
	var object_type = li.attr('data-object');
	var object_href = li.attr('data-href');
	var object_name = li.find('span.apname').html();
	$('#searchField').val('');
	$('#searchField').blur();
	$('#refreshpart').hide();
	$('#searchCancelButton').hide();
	add_to_search_history(object_name);
	if (object_type == "product")
		product_request_ajax(object_href);
	if (object_type == "category")
	{
		var params = [{ 'key': 'url', 'value': object_href}];
		products_request_ajax(params);
	}
	return false;
});

$('#searchcategories').on('click', 'a.menu61', function(){
	var params = [{ 'key': 'url', 'value': $(this).attr('href')}];
	products_request_ajax(params);
	return false;
});

$('#help_msg1, #help_msg2').click(function(){
	$('#searchField').val($(this).html());
	$('#searchField').trigger('keyup');
	return false;
});

$('body').on('click', '#filter label[data-toggle=collapse]', function(){
	$(this).closest('div').find('i.fa-caret-right').first().toggle();
	$(this).closest('div').find('i.fa-caret-down').first().toggle();
});

$('body').on('click', '#filter i', function(){
	$(this).closest('div').find('label.labelcollapse').trigger('click');
});

$('html').click(function() {
	$('#refreshpart').hide();
});

$('#refreshpart').click(function(event){
	event.stopPropagation();
});

$('#pop-brands').click(function(){
    $(this).addClass('current');
    $('#all-brands').removeClass('current');
    $('#pop-brands-list').show();
    $('#all-brands-list').hide();
    return false;
});

$('#all-brands').click(function(){
    $(this).addClass('current');
    $('#pop-brands').removeClass('current');
    $('#pop-brands-list').hide();
    $('#all-brands-list').show();
    return false;
});

{* ФУНКЦИИ КОРЗИНЫ *}
$('#content').on('click' ,'li.listitem a.list-buy', function(){
	var variant_id = $(this).closest('li').find('select[name=variant_id] option:selected').val();
	var var_amount = $(this).attr('data-var-amount');
	{*ЕСЛИ ВАРИАНТЫ РАДИОБАТТОНЫ*}{*var variant_id = $(this).closest('li').find('input[name=variant_id]:checked').val();*}
	var href = "{$config->root_url}{$cart_module->url}?action=add_variant&variant_id="+variant_id+"&amount=1&var_amount="+var_amount;
	$.get(href, function(data){
		if (data.success)
		{
			$('#cart-placeholder').html(data.data);
			$('#after-buy-form .modal-header span.modal-title').html(data.additional_result.modal_header);
			$('#after-buy-form .modal-body').html(data.additional_result.added_form);
			$('#after-buy-form').modal();
		}
	});
	{*var obj1 = $(this).offset();
	var obj2 = $('#cart-placeholder').offset();
	var dx = obj1.left - obj2.left;
	var dy = obj1.top - obj2.top;
	var distance = Math.sqrt(dx * dx + dy * dy);
	$(this).closest('.listitem').find('a.list-image img').effect("transfer", { to: $("#cart-placeholder"), className: "transfer_class" }, distance);	
	$('.transfer_class').html($(this).closest('.listitem').find('a.list-image').html());
	$('.transfer_class').find('img').css('height', '100%');*}
	return false;
});

$('#content').on('click' ,'li.plitka-item a.plitka-buy', function(){
	var variant_id = $(this).closest('li').find('select[name=variant_id] option:selected').val();
	var var_amount = $(this).attr('data-var-amount');
	{*ЕСЛИ ВАРИАНТЫ РАДИОБАТТОНЫ*}{*var variant_id = $(this).closest('li').find('input[name=variant_id]:checked').val();*}
	var href = "{$config->root_url}{$cart_module->url}?action=add_variant&variant_id="+variant_id+"&amount=1&var_amount="+var_amount;
	$.get(href, function(data){
		if (data.success)
		{
			$('#cart-placeholder').html(data.data);
			$('#after-buy-form .modal-header span.modal-title').html(data.additional_result.modal_header);
			$('#after-buy-form .modal-body').html(data.additional_result.added_form);
			$('#after-buy-form').modal();
		}
	});
	{*var obj1 = $(this).offset();
	var obj2 = $('#cart-placeholder').offset();
	var dx = obj1.left - obj2.left;
	var dy = obj1.top - obj2.top;
	var distance = Math.sqrt(dx * dx + dy * dy);
	$(this).closest('.plitka-item').find('a.plitka-image img').effect("transfer", { to: $("#cart-placeholder"), className: "transfer_class" }, distance);	
	$('.transfer_class').html($(this).closest('.plitka-item').find('a.plitka-image').html());
	$('.transfer_class').find('img').css('height', '100%');*}
	return false;
});

$('#content').on('click' , 'a.add_to_compare', function(){
	var a = $(this);
	var href = a.attr('href');
	$.get(href, function(data){
		if (data.success)
		{
			$('#make-compare').attr('href', data.compare_href);
			$('#make-compare').html(data.compare_href_label);
			if (data.compare_href_show)
				$('#make-compare').closest('div').show();
			else
				$('#make-compare').closest('div').hide();
			a.closest('div').hide().next('.buy-compare').show().find('a.compare_items').attr('href', data.compare_href).html('Сравнить ('+data.compare_items_count+')');
			a.closest('ul.list').find('a.compare_items').each(function(){
				$(this).attr('href', data.compare_href);
				$(this).html('Сравнить ('+data.compare_items_count+')');
			});
		}
	});
	return false;
});


$('#content').on('click', 'a.remove_from_compare', function(){
	var a = $(this);
	var href = a.attr('href');
	$.get(href, function(data){
		if (data.success)
		{
			$('#make-compare').attr('href', data.compare_href);
			$('#make-compare').html(data.compare_href_label);
			if (data.compare_href_show)
				$('#make-compare').closest('div').show();
			else
				$('#make-compare').closest('div').hide();
			a.closest('div').hide().prev('.buy-compare').show();
			a.closest('ul.list').find('a.compare_items').each(function(){
				$(this).attr('href', data.compare_href);
				$(this).html('Сравнить ('+data.compare_items_count+')');
			});
		}
	});
	return false;
});

$('#wrap').on('click', 'a.cart-buy1click', function(){
	$('#cart-placeholder ul.dropdown-cart').hide();
	$('#cart').removeClass('hover');
	
	var href = '{$config->root_url}{$cart_module->url}{url add=["format"=>"oneclick", "ajax"=>1]}';
	$.get(href, function(data){
		if (data.success){
			$('#buy1click .modal-body .cart-body').html(data.data);
			$('#make-order-one-click').removeClass('disabled');
			$('#buy1click-phone-required').hide();
			
			if (data.total_price >= {$settings->cart_order_min_price}){
				$('#not-enough-block').hide();
				$('#make-order-one-click').show();
				$('#buy1click .buy1click-user-info').show();
			}
			else{
				$('#not-enough-block').show();
				$('#make-order-one-click').hide();
				$('#buy1click .buy1click-user-info').hide();
			}
			
			$('#buy1click').modal();
			$('#buy1click input[name=phone_number]').focus();
		}
	});
	
	return false;
});

$('#content').on('click', 'a.buy1', function(){
	var button = $(this);
	$('#buy1spinner').removeClass('hidden');
	button.attr('disabled', true);
	
	var variant_id = $(this).closest('#buy').find('select[name=variant_id] option:selected').val();
	{*ЕСЛИ ВАРИАНТЫ РАДИОБАТТОНЫ*}{*var variant_id = $(this).closest('#buy').find('input[name=variant_id]:checked').val();*}
	var modificators = [];
	var modificators_count = [];
	var amount = 1;
	var amount_input = $('#buy input[name=amount]');
	if (amount_input.length > 0)
		amount = amount_input.val();
	
	$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
		if (parseInt($(this).val()) > 0)
		{
			modificators.push($(this).val());
			var ic = $(this).closest('div').find('input[name^=modificators_count]');
			if (ic.length > 0)
				modificators_count.push(ic.val());
			else
				modificators_count.push(1);
		}
	});

	var var_amount = 1;
	var var_amount_input = $('#buy input[name=var_amount]');
	if (var_amount_input.length > 0)
		var_amount = var_amount_input.val(); 
	var href = "{$config->root_url}{$cart_module->url}?action=add_variant&variant_id="+variant_id+"&modificators="+modificators.join(',')+"&modificators_count="+modificators_count.join(',')+"&amount="+amount+"&var_amount="+var_amount;
	$.get(href, function(data){
		if (data.success)
		{
			$('#cart-placeholder').html(data.data);
			if (data.additional_result != undefined && data.additional_result.message == "no_stock")
			{
				$('.buy-buy').hide();
				$('.buy1').hide();
				$('#no_stock').html('Вы можете купить товар в количестве не более ' + data.additional_result.max_stock + ' штук');
				$('#no_stock').show();
			}
			
			var href = '{$config->root_url}{$cart_module->url}{url add=["format"=>"oneclick", "ajax"=>1]}';
			$.get(href, function(data){
				if (data.success){
					$('#buy1click .modal-body .cart-body').html(data.data);
					$('#buy1click input[name=variant_id]').val(variant_id);
					$('#make-order-one-click').removeClass('disabled');
					$('#buy1click-phone-required').hide();
					
					if (data.total_price >= {$settings->cart_order_min_price}){
						$('#not-enough-block').hide();
						$('#make-order-one-click').show();
						$('#buy1click .buy1click-user-info').show();
					}
					else{
						$('#not-enough-block').show();
						$('#make-order-one-click').hide();
						$('#buy1click .buy1click-user-info').hide();
					}
					
					$('#buy1click').modal();
					$('#buy1click input[name=phone_number]').focus();
				}
				$('#buy1spinner').addClass('hidden');
				button.attr('disabled', false);
			});
		}
	});
	
	return false;
});

$('#content').on('click', 'a.buy-one-click', function(){
	var button = $(this);
	button.find('.buy1spinner').removeClass('hidden');
	button.attr('disabled', true);
	
	var variant_id = $(this).closest('li').find('select[name=variant_id] option:selected').val();
	var var_amount = $(this).attr('data-var-amount');
	{*ЕСЛИ ВАРИАНТЫ РАДИОБАТТОНЫ*}{*var variant_id = $(this).closest('li').find('input[name=variant_id]:checked').val();*}
	var href = "{$config->root_url}{$cart_module->url}?action=add_variant&variant_id="+variant_id+"&amount=1&var_amount="+var_amount;
	$.get(href, function(data){
		if (data.success){
			$('#cart-placeholder').html(data.data);
			
			var href = '{$config->root_url}{$cart_module->url}{url add=["format"=>"oneclick", "ajax"=>1]}';
			$.get(href, function(data){
				if (data.success){
					$('#buy1click .modal-body .cart-body').html(data.data);
					$('#buy1click input[name=variant_id]').val(variant_id);
					$('#make-order-one-click').removeClass('disabled');
					$('#buy1click-phone-required').hide();
					
					if (data.total_price >= {$settings->cart_order_min_price}){
						$('#not-enough-block').hide();
						$('#make-order-one-click').show();
						$('#buy1click .buy1click-user-info').show();
					}
					else{
						$('#not-enough-block').show();
						$('#make-order-one-click').hide();
						$('#buy1click .buy1click-user-info').hide();
					}
					
					$('#buy1click').modal();
					$('#buy1click input[name=phone_number]').focus();
				}
				button.find('.buy1spinner').addClass('hidden');
				button.attr('disabled', false);
			});
		}
	});
	
	return false;
});

$('#make-callback').click(function(){
	$('#contact-center input[name=phone_number]').val('{if $user->phone_code && $user->phone}+7 ({$user->phone_code}) {$user->phone|phone_mask}{/if}').mask("+7 (999) 999-99-99");
	$('#contact-center input[name=user_name]').val('{$user->name}');
	$('#contact-center input[name=call_time]').val('');
	$('#contact-center textarea[name=message]').val('');
	
	$('#callback-part').show();
	$('#send-callback').show();
	$('#callback-success-part').hide();
	$('#callback-error-part').hide();
	$('#callback-close').hide();
	$('#contact-center-phone-required').hide();
	
	$('#contact-center').modal();
	if ($('#contact-center input[name=phone_number]').val() == "")
		$('#contact-center input[name=phone_number]').focus();
	else
		if ($('#contact-center input[name=user_name]').val() == "")
			$('#contact-center input[name=user_name]').focus();
		else
			$('#contact-center input[name=call_time]').focus();
	return false;
});

$('#content').on('click', 'a.review-complain', function(){
	$('#claim-part').show();
	$('#make-claim').show();
	$('#claim-success-part').hide();
	$('#claim-error-part').hide();
	$('#claim-close').hide();
	
	$('#complain-form .modal-body textarea').val('').prop('disabled', true);
	$('#complain-form .modal-body input[type=radio]:first').prop('checked', true);
	$('#complain-form input[name=claim_review_id]').val($(this).closest('.review-body').data('id'));
	$('#complain-form').modal();
	return false;
});

});