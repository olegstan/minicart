{* Функция для загрузки ветки дерева *}
{* element - куда выводить, parent_id - какую категорию вывести *}
function print_lazy_result(element, parent_id, icon_col, icon_spin, path, select_category_id){
	if (element == undefined)
	{
		if (icon_spin != undefined)
			icon_spin.hide();
		if (icon_col != undefined)
			icon_col.show();
		return false;
	}
		
	if (parent_id == undefined)
		parent_id = 0;
	var result = [];
	var href = '{$config->root_url}{$main_module->url}?lazy_load=1&parent_id='+parent_id;
	
	if (element.find('ol:first').length > 0)
	{
		if (icon_spin != undefined)
			icon_spin.hide();
		if (icon_col != undefined)
			icon_col.show();
			
		if (path != undefined && path.length > 0)
		{
			id = path[0];
			var _li = $('#categories li[data-id='+id+']');
			var _icon_col = _li.find('i.fa-minus:first');
			var _icon_exp = _li.find('i.fa-plus:first');
			var _icon_spin = _li.find('i.fa-spinner:first');
			
			var ol = _li.find('ol:first');
			
			_icon_exp.hide();
			_icon_spin.show();
			_li.removeClass('collapsed');
			ol.show();
			
			path.shift();
			print_lazy_result(_li, id, _icon_col, _icon_spin, path, select_category_id!=undefined?select_category_id:undefined);
		}
		else
			if (select_category_id != undefined)
			{
				$('#categories li[data-id='+select_category_id+'] a:first').addClass('active');
				$('#categories li[data-id='+select_category_id+'] i.fa-plus:first').trigger('click');
			}
	}
	else
		$.get(href, function(data){
			if (data == undefined || data.length == 0)
				return false;
			var ol = $('<ol></ol>');
						
			for(var i=0 ; i<data.length ; i++){
				var li = $('<li class="collapsed'+(data[i].folder?' havechild':'')+'" data-id="'+data[i].id+'" data-level="'+(parseInt(element.attr('data-level'))+1)+'"></li>');
				if (data[i].folder){
					li.append('<i class="fa fa-minus" style="display: none;"></i>');
					li.append('<i class="fa fa-plus" style="display: block;"></i>');
					li.append('<i class="fa fa-spin fa fa-spinner" style="display: none;"></i>');
				}
				li.append('<div><span>'+data[i].products_count+'</span><a href="{$config->root_url}{$products_module->url}'+data[i].url+'" class="menu'+data[i].id+' menu-level'+(parseInt(element.attr('data-level'))+1)+'">'+data[i].title+'</a></div>');
				ol.append(li);
			}
			element.append(ol);
			if (icon_spin != undefined)
				icon_spin.hide();
			if (icon_col != undefined)
				icon_col.show();
				
			if (path != undefined && path.length > 0)
			{
				id = path[0];
				var _li = $('#categories li[data-id='+id+']');
				var _icon_col = _li.find('i.fa-minus:first');
				var _icon_exp = _li.find('i.fa-plus:first');
				var _icon_spin = _li.find('i.fa-spinner:first');
				
				_icon_exp.hide();
				_icon_spin.show();
				_li.removeClass('collapsed');
				
				path.shift();
				print_lazy_result(_li, id, _icon_col, _icon_spin, path, select_category_id!=undefined?select_category_id:undefined);
			}
			else
				if (select_category_id != undefined)
				{
					$('#categories li[data-id='+select_category_id+'] a:first').addClass('active');
					$('#categories li[data-id='+select_category_id+'] i.fa-plus').trigger('click');
				}
		});
}


{* AJAX загрузка списка просмотренных товаров *}
function history_products_ajax(){
	var url = "{$config->root_url}{$products_module->url}{url add=['history_products'=>1]}";
	$.ajax({
		type: 'GET',
		url: url,
		success: function(data) {
			$('#viewedproducts').html(data);
			
			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});
		}
	});
	return false;
}

{* Копия этой функции в файле actions_functions.js*}
function rangeSliderStart(){
	$('input[data-control=rangeSlider]').rangeSlider({
		onChange: function(obj){        // function-callback, is called on every change
			if (!obj.input.data("disable"))
			{
				var i1 = $('#' + obj.input.attr('data-id') + "_min");
				var i2 = $('#' + obj.input.attr('data-id') + "_max");
			
				value_array = obj.input.val().split(";");
				i1.val(value_array[0]);
				i2.val(value_array[1]);
			}
		},
		onFinish: function(obj){
			{*var params = [];
			params.push({
				'key': 'url',
				'value': $('div#filter').attr('data-url')
			},{
				'key': 'format',
				'value': 'products_count'
			});
			$('div#filter select').each(function(){
				if ($(this).val().length != 0)
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
			});
			$('div#filter input:checked:visible').each(function(){
				if ($(this).val().length != 0)
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
			});
			$('div#filter input[data-control=rangeSlider]').each(function(){
				if ($(this).val().length != 0 && $(this).val() != $(this).attr('data-min')+';'+$(this).attr('data-max'))
					params.push({
						'key': $(this).attr('name'),
						'value': $(this).val()
					});
		
			});
			products_request_ajax(params, undefined, undefined, true);*}
			
			$('#apply_products_filter').trigger('click',false);
		}
	});
}

{* AJAX-загрузка списка товаров 
		params - параметры для отбора
		push_history - флаг добавлять ли в историю
		jump_if_one - флаг переходить ли к product_request_ajax если найдется всего 1 товар
		animate_scroll - плавная прокрутка страницы вверх
		scroll_up - прокручивать ли наверх
		open_category_in_menu - открывать ли в левом меню текущую категорию (при изменении фильтра например не надо)
		append_mode - режим работы кнопки "Показать еще", когда в список товаров добавляются товары
*}
function products_request_ajax(params, push_history, jump_if_one, animate_scroll, scroll_up, open_category_in_menu, append_mode){
	var url = "{$config->root_url}{$products_module->url}";
	var show_products_count_only = false;
	var scroll_to_up = (scroll_up == undefined || scroll_up == true) ? true : false;
	if (params != undefined && params.length>0)
	{
		url += "?";
		for(var index in params)
		{
			if (params[index].key == "url")
			{
				url = params[index].value;
				if (params.length > 1)
					url += "?";
				break;
			}
		}
		
		var index2 = 0;
		for(var index in params)
		{
			if (params[index].key == "format" && params[index].value == "products_count")
				show_products_count_only = true;
		
			if (params[index].key != "url")
			{
				if (is_array(params[index].value))
				{
					for(var idx in params[index].value)
					{
						if (index2 > 0)
							url += "&";
						url += params[index].key + "=" + params[index].value[idx];
						index2 += 1;
					}
				}
				else
				{
					if (index2 > 0)
						url += "&";
					url += params[index].key + "=" + params[index].value;
					index2 += 1;
				}
			}
		}
	}
	var full_url = url;
	if (params == undefined || params.length == 0 || (params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
		full_url += "?";
	if (params != undefined && params.length > 0 && !(params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
		full_url += "&";
	full_url += "ajax=1";
	
	if (!show_products_count_only)
	{
		if (scroll_to_up)
		{
			if (animate_scroll == undefined)
				$("body,html").animate({
					scrollTop: 0{*$('#main-right-container').offset().top*}
				}, 0);
			else
				$("body,html").animate({
					scrollTop: $('#main-right-container').offset().top
				}, 200);
		}
	
		$('a[data-toggle="tooltip"]').each(function(){
			$(this).tooltip('hide');
		});
		
		$('ul.horizontal-menu li.active').removeClass('active');

		$('#brands a.active').removeClass('active');
	}
	
	$.ajax({
		type: 'GET',
		url: full_url,
		success: function(data) {
		
			/*if (show_products_count_only)
			{
				$('#apply_products_filter').html('Показать ' + data.data + ' товаров');
				return false;
			}*/
			
			$('#filter_spinner').hide();
		
			{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
		
			if ($('#searchField').val().length == 0)
			{
				$('#refreshpart').hide();
				$('#searchCancelButton').hide();
			}
			if (data.products_categories == undefined)
				$('#searchcategories').hide();
		
			if (jump_if_one != undefined && jump_if_one && data.products_count == 1 && data.product_url.length > 0)
			{
				$('#searchcategories').hide();
				product_request_ajax(data.product_url, push_history);
				return false;
			}
		
			if (data.meta_title)
				document.title = data.meta_title;
			if (data.meta_keywords)
				$('meta[name=keywords]').prop('content', data.meta_keywords);
			if (data.meta_description)
				$('meta[name=description]').prop('content', data.meta_description);

			$('a[data-toggle="tooltip"]').each(function(){
				$(this).tooltip('hide');
			});
		
			if (typeof history.pushState != undefined && (push_history == undefined || push_history == true) && !show_products_count_only) {
				history.pushState({ mode: 'products', params: params, selected_category_id: data.category_id, category_path: data.category_path},null,encodeURI(decodeURI(url)));
			}
			
			if (append_mode != undefined && append_mode == true){
				{if !$mode || $mode == 'list'}
					$(data.data).insertAfter($('#content ul.list:last'));
					//$('#content').append(data.data);
				{else}
					$(data.data).insertAfter($('#content ul.plitka:last'));
					//$('#content').append(data.data);
				{/if}
				$('#content div.loading').remove();
			}
			else
				$('#content').html(data.data);
				
			$('body').removeClass();
			$('body').addClass(data.body_category_css);
			
			$('#main-left-container').show();
			if ($('#main-right-container').hasClass('col-xs-12'))
			{
				$('#main-right-container').removeClass('col-xs-12');
				$('#main-right-container').addClass('col-xs-9');
			}
			
			{* Раскроем нужную категорию *}
			if (data.category_path != undefined && (open_category_in_menu == undefined || open_category_in_menu == true))
			{
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					if (icon_minus.length > 0)
						icon_minus.trigger('click');
				});
				if (data.category_path.length > 0)
				{
					var path = data.category_path.split(',');
					print_lazy_result($('#categories'),0,undefined,undefined, path, data.category_id);
				}
				else
					print_lazy_result($('#categories'),0,undefined,undefined, '', data.category_id);
			}
			{* Раскроем нужную категорию(End) *}
			
			{*Выводим категории поиска*}
			if (data.products_categories != undefined)
			{
				$('#searchcategories').html('<div class="searchcats">Категории:</div>');
				var ol = $('<ol></ol>');
				for(var index in data.products_categories)
				{
					var position_array = data.products_categories[index];
					for(var index2 in position_array)
					{
						var category = position_array[index2];
						ol.append('<li><div><span>'+category.count+'</span><a class="menu61'+((category.id==data.category_id)?' active':'')+'" href="'+category.url+'">'+category.name+'</a></div></li>');
					}
				}
				$('#searchcategories').append(ol);
				$('#searchcategories').show();
			}

			if (data.filter != undefined)
			{
				$('#filter').replaceWith(data.filter);
				rangeSliderStart();
			}

			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});

			$('a[data-type="qtip"]').qtip({
				content: {
					attr: 'data-content',
					title: function(event, api) {
						return $(this).attr('data-title');
					},
					button: true
				},
				style: {
					classes: 'qtip-light qtip-rounded'
				},
				show: {
					delay: 0{*,
					event: 'hover'*}
				}{*,
				hide: {
					event: 'unfocus'
				}*}
			});
			
			$('ul.plitka .plitka-name-block').matchHeight(1);
			$('ul.plitka .plitka-description').matchHeight(1);
		}
	});
	return false;
}

{* AJAX-загрузка страницы бренда
		params - параметры для отбора
		push_history - флаг добавлять ли в историю
*}
function brand_request_ajax(params, push_history){
	var url = "{$config->root_url}{$brand_module->url}";
	if (params != undefined && params.length>0)
	{
		url += "?";
		for(var index in params)
		{
			if (params[index].key == "url")
			{
				url = params[index].value;
				if (params.length > 1)
					url += "?";
				break;
			}
		}
		
		var index2 = 0;
		for(var index in params)
		{
			if (params[index].key != "url")
			{
				if (index2 > 0)
					url += "&";
				url += params[index].key + "=" + params[index].value;
				index2 += 1;
			}
		}
	}
	var full_url = url;
	if (params == undefined || params.length == 0 || (params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
		full_url += "?";
	if (params != undefined && params.length > 0 && !(params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
		full_url += "&";
	full_url += "ajax=1";
	
	$("body,html").animate({
		scrollTop:0
	}, 0);
	
	$('a[data-toggle="tooltip"]').each(function(){
		$(this).tooltip('hide');
	});
	
	$('ul.horizontal-menu li.active').removeClass('active');
	$('#searchcategories').hide();
	
	$('#filter').hide();
	
	$.ajax({
		type: 'GET',
		url: full_url,
		success: function(data) {
		
			{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
			
			document.title = data.meta_title;
			$('meta[name=keywords]').prop('content', data.meta_keywords);
			$('meta[name=description]').prop('content', data.meta_description);

			$('a[data-toggle="tooltip"]').each(function(){
				$(this).tooltip('hide');
			});
		
			if (typeof history.pushState != undefined && (push_history == undefined || push_history == true)) {
				history.pushState({ mode: 'brand', params: params, selected_brand_id: data.brand_id},null,encodeURI(decodeURI(url)));
			}
			$('#content').html(data.data);
			$('body').removeClass();
			$('body').addClass(data.body_brand_css);
			
			$('#main-left-container').show();
			if ($('#main-right-container').hasClass('col-xs-12'))
			{
				$('#main-right-container').removeClass('col-xs-12');
				$('#main-right-container').addClass('col-xs-9');
			}
			
			{* Свернем категории *}
			$('#categories a.active').removeClass('active');
			$('#categories li:not(.collapsed) {$nested_level}').each(function(){
				var icon_minus = $(this).find('i.fa-minus');
				if (icon_minus.length > 0)
					icon_minus.trigger('click');
			});
			{* Свернем категории (End) *}
			
			$('#brands a.active').removeClass('active');
			$('#brands li[data-id='+data.brand_id+'] a').addClass('active');
			
			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});
		}
	});
	return false;
}

function product_request_ajax(url, push_history){
	var full_url = url;
	
	if (url.indexOf('?')==-1)
		full_url += "?";
	else
		full_url += "&";
	full_url += "ajax=1";
	
	$('a[data-toggle="tooltip"]').each(function(){
			$(this).tooltip('hide');
		});

	$.ajax({
		type: 'GET',
		url: full_url,
		success: function(data) {
		
			$('ul.horizontal-menu li.active').removeClass('active');
			$('#searchcategories').hide();
			$('#brands a.active').removeClass('active');
			$('#filter').hide();
		
			{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
		
			document.title = data.meta_title;
			$('meta[name=keywords]').prop('content', data.meta_keywords);
			$('meta[name=description]').prop('content', data.meta_description);
				
			$('a[data-toggle="tooltip"]').each(function(){
				$(this).tooltip('hide');
			});

			if (typeof history.pushState != undefined && (push_history == undefined || push_history == true)) {
				history.pushState({ mode: 'product', url: url, category_path: data.category_path, selected_category_id: data.category_id},null,encodeURI(decodeURI(url)));
			}
			$('#content').html(data.data);
			$('body').removeClass();
			$('body').addClass(data.body_category_css);
			$('body').addClass(data.body_product_css);
			
			$('#main-left-container').hide();
			if ($('#main-right-container').hasClass('col-xs-9'))
			{
				$('#main-right-container').removeClass('col-xs-9');
				$('#main-right-container').addClass('col-xs-12');
			}
			
			if (push_history == undefined || push_history == true)
			{
				{* Раскроем нужную категорию *}
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					if (icon_minus.length > 0)
						icon_minus.trigger('click');
				});
				
				if (data.category_path.length > 0)
				{
					var path = data.category_path.split(',');
					print_lazy_result($('#categories'),0,undefined,undefined, path, data.category_id);
				}
				else
					print_lazy_result($('#categories'),0,undefined,undefined, [], data.category_id);
					
				history_products_ajax();
			}
			$('a[data-type="qtip"]').qtip({
				content: {
					attr: 'data-content',
					title: function(event, api) {
						return $(this).attr('data-title');
					},
					button: true
				},
				style: {
					classes: 'qtip-light qtip-rounded'
				},
				show: {
					delay: 0{*,
					event: 'hover'*}
				}{*,
				hide: {
					event: 'unfocus'
				}*}
			});
			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});
		}
	});
	return false;
}

function material_request_ajax(url, push_history){
	var full_url = url;
	if (full_url.indexOf('?') == -1)
		full_url += '?ajax';
	else
		full_url += '&ajax';
	$('ul.horizontal-menu li.active').removeClass('active');
	$('#brands a.active').removeClass('active');
	$('#searchcategories').hide();
	$('#filter').hide();
	
	if (url == "{$config->root_url}" || url == "{$config->root_url}/")
		document.location.href = url;
		
	$('#main-left-container').show();
	$('#main-right-container').removeClass('col-xs-12');
	$('#main-right-container').addClass('col-xs-9');
		
	$.ajax({
		type: 'GET',
		url: full_url,
		success: function(data) {
		
			{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
		
			document.title = data.meta_title;
			$('meta[name=keywords]').prop('content', data.meta_keywords);
			$('meta[name=description]').prop('content', data.meta_description);

			if (typeof history.pushState != undefined && (push_history == undefined || push_history == true)) {
				{* Свернем категории *}
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					icon_minus.trigger('click');
				});
				
				history.pushState({ mode: 'material', url: url},null,encodeURI(decodeURI(url)));
			}
			
			$('#content').html(data.data);
			$('body').removeClass();
			$('body').addClass(data.body_category_css);
			$('body').addClass(data.body_material_css);
		}
	});
	return false;
}

function cart_request_ajax(push_history){
	var url = '{$config->root_url}/cart/';
	$('ul.horizontal-menu li.active').removeClass('active');
	$('#brands a.active').removeClass('active');
	$('#searchcategories').hide();
	$('#cart-placeholder ul.dropdown-cart').hide();
	$('#cart').removeClass('hover');
	$.ajax({
		type: 'GET',
		url: url+'?ajax=1',
		success: function(data) {
		
			{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
		
			document.title = data.meta_title;
			$('meta[name=keywords]').prop('content', data.meta_keywords);
			$('meta[name=description]').prop('content', data.meta_description);

			if (typeof history.pushState != undefined && (push_history == undefined || push_history == true)) { 
				history.pushState({ mode: 'cart', url: url},null,encodeURI(decodeURI(url)));
			}
			$('#main-left-container').hide();
			$('#main-right-container').removeClass('col-xs-9');
			$('#main-right-container').addClass('col-xs-12');
			$('#content').html(data.data);
		}
	});
	return false;
}

function compare_request_ajax(push_history){
	var url = $('#make-compare').attr('href');{*'{$config->root_url}/compare/';*}
	$('ul.horizontal-menu li.active').removeClass('active');
	$('#brands a.active').removeClass('active');
	$('#searchcategories').hide();
	$.ajax({
		type: 'GET',
		url: url+'&ajax=1',
		success: function(data) {
			if (data.success)
			{
				{if $settings->yandex_metric_counter}yaCounter{$settings->yandex_metric_counter}.hit(url, data.meta_title, null);{/if}
			
				if (typeof history.pushState != undefined && (push_history == undefined || push_history == true)) { 
					history.pushState({ mode: 'compare', url: url},null,encodeURI(decodeURI(url)));
				}
				$('#main-left-container').hide();
				$('#main-right-container').removeClass('col-xs-9');
				$('#main-right-container').addClass('col-xs-12');
				$('#content').html(data.data);
			}
		}
	});
	return false;
}

function update_cart_info(){
	$.get("{$config->root_url}{$cart_module->url}{url add=['format' => 'info', 'ajax' => 1]}", function(data){
		if (data.success)
			$('#cart-placeholder').html(data.data);
	});
}

var search_ajax_context;

{* запрос товаров на ajax'e для поиска*}
function search_request_ajax(keyword){
	if (keyword == undefined || keyword.length == 0 || keyword.length<{$settings->search_min_lenght})
	{
		$('#refreshpart').html('').hide();
		return false;
	}

	$('#brands a.active').removeClass('active');
	
	if (search_ajax_context != null)
		search_ajax_context.abort();
	else
		$('#refreshpart').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');
	
	var url = "{$config->root_url}{$products_module->url}?ajax=1&format=search_ajax&keyword="+encodeURI(decodeURI(keyword));
	search_ajax_context = $.ajax({
		type: 'GET',
		url: url,
		success: function(data) {
			if (data.success)
			{
				$('#refreshpart').html(data.data);
				if (data.products_count > 0 || data.categories_count > 0)
					$('#refreshpart').show();
				else
					$('#refreshpart').hide();
				$('#searchCancelButton').show();
			}
			search_ajax_context = null;
		}
	});
	return false;
}

{* добавление поискового запроса в историю *}
function add_to_search_history(keyword){
	var url = "{$config->root_url}{$products_module->url}?ajax=1&format=add_to_history&keyword="+encodeURI(decodeURI(keyword));
	$.ajax({
		type: 'GET',
		url: url
	});
	return false;
}