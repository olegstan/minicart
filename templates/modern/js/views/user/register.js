$(function(){
    $("#register-form-phone").mask("+7 (999) 999-99-99");

    $('#register-form').submit(function (e) {
        e.preventDefault();

        $.ajax({
            type: 'POST',
            url: this.action,
            data: {
                user: $('#register-form-user').val(),
                phone: $('#register-form-phone').val(),
                email: $('#register-form-email').val(),
                password: $('#register-form-password').val()
            },
            beforeSend: function(){

            },
            success: function (response) {

                $.each(response.success, function (k, v) {
                    $('#register-form-' + k)
                        .next()
                        .addClass('green')
                        .removeClass('red')
                        .removeClass('hidden')
                        .text(v);
                });

                if(response.success.length){

                }else if(response.error.length){
                    $.each(response.errors, function (k, v) {
                        $('#register-form-' + k)
                            .next()
                            .addClass('red')
                            .removeClass('green')
                            .removeClass('hidden')
                            .text(v);
                    });
                    $('#register-form-error')
                        .removeClass('hidden')
                        .text(response.error);
                }
            },
            error: function () {

            }
        });
    });
});