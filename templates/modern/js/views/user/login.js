$(function(){
    var first_input = $('#login-form-username')
    first_input.selectionStart = 0;
    first_input.selectionEnd = 0;
    first_input.focus();

    $('#login-form').submit(function(e){
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: this.action,
            data: {
                username: $('#login-form-username').val(),
                password: $('#login-form-password').val()
            },
            beforeSend: function(){

            },
            success: function (response) {

                $.each(response.success, function (k, v) {
                    $('#login-form-' + k)
                        .next()
                        .addClass('green')
                        .removeClass('red')
                        .removeClass('hidden')
                        .text(v);
                });

                if(response.success.length){

                }else if(response.error.length){
                    $.each(response.errors, function (k, v) {
                        $('#login-form-' + k)
                            .next()
                            .addClass('red')
                            .removeClass('green')
                            .removeClass('hidden')
                            .text(v);
                    });
                    $('#login-form-error')
                        .removeClass('hidden')
                        .text(response.error);
                }
            },
            error: function () {

            }
        });
    });
});
