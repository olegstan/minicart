{* Проверка переменной является ли она массивом *}
function is_array(object){
	if (Object.prototype.toString.call(object) === '[object Array]')
		return true;
	return false;
}