<h1>Мои заказы {if $all_orders_count}({$all_orders_count}){/if}</h1>
{include file='pagination-frontend.tpl'}
{if $orders}
    <ul class="orderlist">
        {foreach $orders as $order}
            {$show_repeat_order = false}
            {foreach $order->purchases as $purchase}
                {if $purchase->product}{$show_repeat_order = true}{/if}
            {/foreach}
            <li class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3 order-number">
                            <a href="{$config->root_url}{$order_module->url}{$order->url}/">Заказ №{$order->id}</a>
                        </div>
                        <div class="col-xs-6 orders-status">Статус:
                            <span class="label {if $order->status->group_name == "Новый"}label-success{elseif $order->status->group_name == "Отменен"}label-danger{else}label-primary{/if}">{$order->status->group_name}</span>

                            <div class="orderlist-date">
                                Дата: {$order->day_str}{*$order->date|date*}
                            </div>
                        </div>


                        <div class="col-xs-3">
                            <div class="orders-repeat">
                                {if $show_repeat_order}
									<a href="{$config->root_url}{$order_module->url}{$order->url}/{url add=['mode'=>'repeat_order']}" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i>
                                        Повторить заказ</a>
                                    {*<button type="button" class="btn btn-default btn-sm"><i class="fa fa-repeat"></i>
                                        Повторить заказ
                                    </button>*}
                                {/if}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <div class="list-of-ordered">
                        <table class="table ordertable">
                            <thead>
                            <tr>
                                <th class="ordertable-product">Товар</th>
                                <th class="ordertable-priceone">Цена</th>
                                <th class="ordertable-count">Количество</th>
                                <th class="ordertable-priceall">Стоимость</th>
                            </tr>
                            </thead>
                            <tbody>
                            {foreach $order->purchases as $purchase}
                            <tr>
                                <td class="ordertable-product">
                                    <div class="ordertable-image">
                                        {if $purchase->image}
                                            <a {if $purchase->product}href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"{/if}
                                               class="producthref">
                                                <img src="{$purchase->image->filename|resize:products:52:52}"
                                                     alt="{$purchase->image->name|escape}"/>
                                            </a>
                                        {/if}
                                    </div>
                    </div>
                    <div class="ordertable-product-name">
                        <a {if $purchase->product}href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"{/if}
                           class="producthref">{if $purchase->product}{$purchase->product->name}{else}{$purchase->product_name}{/if}</a>
                        {if $purchase->variant->name}<span>({$purchase->variant->name})</span>{elseif $purchase->variant_name}<span>({$purchase->variant_name})</span>{/if}
						{if $purchase->variant->stock == -1}
							<div class="cart-pod-zakaz">
								<span><i class="fa fa-truck"></i> Под заказ</span>
							</div>
						{/if}
                    </div>

                    {if $sku_value}
                        <div class="artikul">Артикул: <span class="sku_value">{$sku_value}</span></div>{/if}
                    <td class="ordertable-priceone">
                        {$purchase->price|convert} {$main_currency->sign}
                    </td>

                    <td class="ordertable-count">
                        {$purchase->amount} {$settings->units}
                    </td>

                    <td class="carttable-priceall">
                        {($purchase->amount*$purchase->price)|convert} {$main_currency->sign}
                    </td>
                    </tr>
                    {/foreach}

                    </table>

                </div>


                </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-3">
                                <strong>Сумма заказа:</strong>
                            </div>
                            <div class="col-xs-9">
                                <strong>{$order->total_price|convert} {$main_currency->sign}{if $order->discount>0}</strong>
                                (Скидка {if $order->discount_type == 0}{$order->discount|string_format:"%d"}%{/if}
                                = {($order->total_price_wo_discount-$order->total_price)|convert} {$main_currency->sign}) {/if}
                            </div>
                    </li>

				
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-3">
                                <strong>Способ получения: </strong>
                            </div>
                            <div class="col-xs-9">
                                {$order->delivery_method->name} {if $order->address}&mdash; {$order->address}{/if}        </div>
                        </div>
                    </li>
                    
                    <li class="list-group-item">
					<div class="row">
						<div class="col-xs-3">
							<strong>Способ оплаты: </strong>
						</div>
						<div class="col-xs-9">
							{$order->payment_method->name}
						</div>
					</div>
				</li>

                    {if $order->comment}
                        <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-3">
                                <strong>Комментарий:</strong>
                            </div>
                            <div class="col-xs-9">
                                {$order->comment}
                            </div>
                        </li>{/if}

                </ul>
            </li>
        {/foreach}
    </ul>
{else}
    Нет заказов
{/if}


{include file='pagination-frontend.tpl'}