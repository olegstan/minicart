<h1>Избранные товары {if $favorites_products_count}({$favorites_products_count}){/if}</h1>

{if $categories|count > 0}
	{foreach $categories as $category}
		<h2>{$category->name} ({$category->products|count})</h2>
		{breadcrumbs module='products' id=$category->products.0->id show_self_element=false}
		<ul class="favotite-list">
			{$mh_group = 1}
			{foreach $category->products as $product}
				{include file='../favorite-product.tpl' mh_group=$mh_group favorite_mode=true}
			{/foreach}
		</ul>
	{/foreach}
{else}
	<p>Нет товаров</p>
{/if}


{*if $products|count > 0}
<h2>Телевизоры</h2>
<ul class="favotite-list">
	{$mh_group = 1}
	{foreach $products as $product}
		{include file='../favorite-product.tpl' mh_group=$mh_group favorite_mode=true}
	{/foreach}
</ul>

{else}
	<p>Нет товаров</p>
{/if*}

<script type="text/javascript">
	$(document).ready(function(){
		$('ul.plitka').on('change', 'li.plitka-item select[name=variant_id]', function(){
			var li = $(this).closest('li');
			var option = $(this).find('option:selected');
			li.find('div.plitka-status span').each(function(){
				$(this).hide();
			});
			var stock = parseInt(option.attr('data-stock'));
			if (stock > 0)
			{
				li.find('span.in-stock').show();
				li.find('a.plitka-buy').show();
			}
			if (stock == 0)
			{
				li.find('span.out-of-stock').show();
				li.find('a.plitka-buy').hide();
			}
			if (stock < 0)
			{
				li.find('span.to-order').show();
				li.find('a.plitka-buy').show();
			}
			var price = parseFloat(option.attr('data-price'));
			var price_old = parseFloat(option.attr('data-price-old'));
			if (price > 0)
				li.find('div.plitka-price').show().html(option.attr('data-price-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-price').hide();
			if (price_old > 0)
				li.find('div.plitka-old-price').show().html(option.attr('data-price-old-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-old-price').hide();
		});
	
		$('ul.plitka .plitka-name-block').matchHeight(1);
		$('ul.plitka .plitka-description').matchHeight(1);
	});
	
	$('li.favorite-item a.delete-from-favorite').click(function(){
		var id = $(this).data('id');
		var a = $(this);
		$.ajax({
			url: "{$config->root_url}/ajax/get_data.php",
			type: "POST",
			data: {
				object: 'products',
				mode: 'product-togglefavorite',
				pid : id,
				uid : {$user->id},
				session_id: '{$smarty.session.id}'
			},
			success: function(data){
				if (data.success)
					location.reload();
			}
		});
		return false;
	});
	
	$('ul.favotite-list a.favorite-buy').click(function(){
	    var variant_id = $(this).closest('li').find('select[name=variant_id] option:selected').val();
    	var href = "http://minicart.su/cart/action=add_variant&variant_id="+variant_id+"&amount=1";
    	$.get(href, function(data){
    		if (data.success)
    			$('#cart-placeholder').html(data.data);
    	});
    	var obj1 = $(this).offset();
    	var obj2 = $('#cart-placeholder').offset();
    	var dx = obj1.left - obj2.left;
    	var dy = obj1.top - obj2.top;
    	var distance = Math.sqrt(dx * dx + dy * dy);
    	$(this).closest('.favorite-item').find('a.favorite-image img').effect("transfer", { to: $("#cart-placeholder"), className: "transfer_class" }, distance);	
    	$('.transfer_class').html($(this).closest('.favorite-item').find('a.favorite-image').html());
    	$('.transfer_class').find('img').css('height', '100%');
    	return false;
    });
</script>