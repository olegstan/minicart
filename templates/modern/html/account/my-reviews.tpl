<h1>Всего {$reviews_count} {$reviews_count|plural:'отзыв':'отзывов':'отзыва'}</h1>

{include file='pagination-frontend.tpl'}

{foreach $reviews as $review}
	<div class="review-body">
		<div class="review-title">
			<div class="review-autor">
            <div class="review-autor-header">Отзыв к товару</div>
			{if $review->product_image}
            <div class="review-product-image">            
				<img src="{$review->product_image->filename|resize:'products':52:52}">
            </div>
			{/if}
            <a href="{$config->root_url}{$product_module->url}{$review->product->url}{$settings->postfix_product_url}" class="review-product-link">{$review->product->name}</a></div>
			<div class="review-date">{$review->day_str}{*Для дат более чем вчера показываем только дату, для сегодняшних комментов показываем только время и слово Сегодня*}</div>
			<div class="review-id">id: {$review->id}</div>
		</div>
		<div class="review-body-text">
			<div class="review-topline">
				<div class="raiting rate{$review->avg_rating*10}" title="Рейтинг товара {$review->rating|string_format:"%.1f"}"></div>{if $review->short} &mdash; {$review->short}{/if}
			</div>
			
			{if $review->pluses}
			<div class="review-verdict">
				<div class="review-verdict-type">
					<div class="review-verdict-header"><i class="fa fa-plus-circle"  title="Достоинства"></i></div>
					<div class="review-verdict-text">
						{$review->pluses}
					</div>
				</div>
			</div>
			{/if}
			
			{if $review->minuses}
				<div class="review-verdict">
					<div class="review-verdict-type">
						<div class="review-verdict-header"><i class="fa fa-minus-circle" title="Недостатки"></i></div>
						<div class="review-verdict-text">
							{$review->minuses}
						</div>
					</div>
				</div>
			{/if}
			
			{if $review->comments}
				 <div class="review-verdict">
				<div class="review-verdict-type">
						<div class="review-verdict-header"><i class="fa fa-pencil"  title="Коментарий"></i></div>
						<div class="review-verdict-text">
					 		{$review->comments}
							
							{* Максимум 5 фото *}
							{if $review->images && $settings->reviews_images_visible}
								<ul class="review-images">
								{foreach $review->images as $img}
									<li><a href="{$img->filename|resize:'reviews':1000:1000}" class="fancybox" rel="group_review{$review->id}"><img src="{$img->filename|resize:'reviews':62:62}"></a></li>
								{/foreach}
								</ul>
							{/if}
						</div>
					</div>
				</div>
			{/if}
			
			{if $review->recommended}
			<div class="irecommend"><i class="fa fa-check"></i> Да - я рекомендую этот товар</div>
			{/if}
			
			
        </div>
        
        <div class="review-controls">
            <a href="{$config->root_url}{$module->url}{url add=['mode'=>'delete-review', 'id'=>$review->id, 'ajax'=>1]}" class="delete-review"><i class="fa fa-times"></i> <span>Удалить отзыв</span></a>
            <div class="review-status">
            	{if !$review->is_visible || !$review->moderated}<span class="review-moderate"><i class="fa fa-clock-o"></i> Ваш отзыв на модерации</span>
				{else}<span class="review-published"><i class="fa fa-check-circle"></i> Ваш отзыв опубликован</span>{/if}
            </div>
        </div>
        
	</div>
{/foreach}

{include file='pagination-frontend.tpl'}

<script type="text/javascript">
	
	$(document).ready(function(){
		$(".fancybox").fancybox({
        		prevEffect	: 'none',
				nextEffect	: 'none',
				helpers	: {
					title	: {
						type: 'outside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					overlay : {
         			   locked     : false
			        }
				}
		});
	});
	
	$('div.delete-review a').click(function(){
		var review = $(this).closest('.review-body');
		var href = $(this).attr('href');
		$.get(href, function(data){
			if (data.success)
				location.reload();
		});
		return false;
	});
</script>