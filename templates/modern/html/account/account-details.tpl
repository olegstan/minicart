<h1>Реквизиты для счетов</h1>

<div class="row">
	<div class="col-xs-6">
   		<div class="form-group">
        	<label>Наименование организации</label>
        	<input type="text" class="form-control" name="organization_name" value="{$user->organization_name}"/>             
		</div> 
        <hr/>
        
        <p><strong>Юридический адрес:</strong></p>
        
        <div class="form-group">
        	<label>Индекс</label>
        	<input type="text" class="form-control" name="yur_postcode" value="{$user->yur_postcode}"/>             
		</div>
        
        <div class="form-group">
        	<label>Город</label>
        	<input type="text" class="form-control" name="yur_city" value="{$user->yur_city}"/>             
		</div>
        
        <div class="form-group">
        	<label>Адрес</label>
        	<input type="text" class="form-control" name="yur_address" value="{$user->yur_address}"/>             
		</div>
        
        <div class="form-group">
        	<label>ИНН</label>
        	<input type="text" class="form-control" name="yur_inn" value="{$user->yur_inn}"/>             
		</div>
        
        <div class="form-group">
        	<label>КПП</label>
        	<input type="text" class="form-control" name="yur_kpp" value="{$user->yur_kpp}"/>             
		</div>
        
        <hr/>
        
        <div class="form-group">
        	<label>Название банка</label>
        	<input type="text" class="form-control" name="yur_bank_name" value="{$user->yur_bank_name}"/>             
		</div>
        
        <div class="form-group">
        	<label>Город (банка)</label>
        	<input type="text" class="form-control" name="yur_bank_city" value="{$user->yur_bank_city}"/>             
		</div>
        
        <div class="form-group">
        	<label>БИК</label>
        	<input type="text" class="form-control" name="yur_bank_bik" value="{$user->yur_bank_bik}"/>             
		</div>
        
        <div class="form-group">
        	<label>Корреспондентский счет</label>
        	<input type="text" class="form-control" name="yur_bank_corr_schet" value="{$user->yur_bank_corr_schet}"/>             
		</div>
        
        <div class="form-group">
        	<label>Расчетный счет</label>
        	<input type="text" class="form-control" name="yur_bank_rasch_schet" value="{$user->yur_bank_rasch_schet}"/>             
		</div>
        
        
        <div class="form-group">
    		<button type="submit" class="btn btn-default w100 btn-success">Сохранить</button>
    	</div> 
        
        
    </div>
</div>