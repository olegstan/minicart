<h1>Изменение пароля</h1>

{if $success_message=="password_changed"}
<div class="alert alert-success">
        <strong>Ваш пароль успешно изменен</strong>
</div>
{/if}


<div class="row">
	<div class="col-xs-6">
      <div class="form-group">
        <label>Старый пароль</label>
        <input type="password" name="old_password" class="form-control">
        {if $error_message=="old_password_error"}<p class="help-block-error">Старый пароль введен неверно</p>{/if}
      </div>
      
        <div class="form-group">
        <label>Новый пароль</label>
        <input type="password" name="new_password1" class="form-control">
        {if $error_message=="new_password_length"}<p class="help-block">Пароль должен содержать не менее 6 символов.</p>{/if}
      </div>
      
      
        <div class="form-group">
        <label>Новый пароль еще раз</label>
        <input type="password" name="new_password2" class="form-control">
        <p class="help-block">Введите, пожалуйста, новый пароль еще раз.</p>
        {if $error_message=="new_password_error"}<p class="help-block-error">Пароли не совпадают</p>{/if}
      </div>
    <div class="form-group">
    <button type="submit" class="btn btn-default w100 btn-success">Сохранить</button>
    </div>
	</div>

</div>