{if $error_message}
<div class="alert alert-warning">
{if $error_message == "empty_email"}Электронный адрес не введен
{elseif $error_message == "empty_name"}Имя не введено
{elseif $error_message == "user_exists"}Пользователь с такими данными уже сушествует
{else}{$error_message}
{/if}
</div>
{/if}

<h1>Личные данные</h1>

<div class="row">
	<div class="col-xs-6">
        <div class="form-group">
        	<label>Имя</label>
        	<input type="text" name="name" class="form-control" value="{$user->name}"/>
		</div>
        
		<div class="form-group">
			<label>Электронный адрес</label>
			<input type="text" name="email" class="form-control" value="{$user->email}"/>        
		</div>
                
        <div class="form-group">
        	<label>Мобильный телефон</label>
        	<input type="text" name="phone" id="phone" class="form-control" value="{if $user->phone_code && $user->phone}+7 ({$user->phone_code}) {$user->phone|phone_mask}{/if}"/>
		</div>
        
        <div class="form-group">
        	<label>Дополнительный телефон</label>
        	<input type="text" name="phone2" id="phone2" class="form-control" value="{if $user->phone2_code && $user->phone2}+7 ({$user->phone2_code}) {$user->phone2|phone_mask}{/if}">             
		</div>
      
    	<div class="form-group">
    		<button type="submit" class="btn btn-default w100 btn-success">Сохранить</button>
    	</div>
	</div>
</div>



<script type="text/javascript">
	$(document).ready(function(){
		$("#phone").mask("+7 (999) 999-99-99");
		$("#phone2").mask("+7 (999) 999-99-99");
	});
</script>