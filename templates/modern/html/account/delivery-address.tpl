<h1>Адрес доставки</h1>

<div class="row">
	<div class="col-xs-6">
		<div class="form-group">
			<label>Текущий адрес доставки:</label>
			<textarea class="form-control" rows="2" name="delivery_address">{$user->delivery_address}</textarea>
        </div>
   
    	<div class="form-group">
    		<button type="submit" class="btn btn-default w100 btn-success">Сохранить</button>
    	</div>
     </div>
</div>

<input type="hidden" name="mode" value="delivery-address"/>

<script type="text/javascript">
	$(document).ready(function(){
		$('textarea[name=delivery_address]').autosize();
	});
</script>