<style type="text/css">
body p {
	font-style: italic;
}
</style>
<h1>E-mail и SMS оповещения</h1>

<div class="notify-block">
<div class="row">
    <div class="col-xs-8 notify-text">
    Получать сообщения о изменении статусов заказов (E-mail)
    </div>
    <div class="col-xs-4">
        <div class="mini-onoff">
        <div data-toggle="buttons" class="btn-group">
            <label class="btn btn-default btn-xs on {if $user->mail_confirm}active{/if}">
                <input type="radio" {if $user->mail_confirm}checked=""{/if} value="1" name="mail_confirm">  Отправляем
			</label>
            <label class="btn btn-default btn-xs off {if !$user->mail_confirm}active{/if}">
                <input type="radio" {if !$user->mail_confirm}checked=""{/if} value="0" name="mail_confirm"> Не отправляем
            </label>
        </div>
    </div>
    </div>
</div>
</div>

<div class="notify-block">
<div class="row">
    <div class="col-xs-8 notify-text">
    Получать сообщения о изменении статусов заказов (SMS) (E-mail)
    </div>
    <div class="col-xs-4">
        <div class="mini-onoff">
        <div data-toggle="buttons" class="btn-group">
            <label class="btn btn-default btn-xs on {if $user->sms_confirm}active{/if}">
                <input type="radio" {if $user->sms_confirm}checked=""{/if} value="1" name="sms_confirm">  Отправляем
			</label>
            <label class="btn btn-default btn-xs off {if !$user->sms_confirm}active{/if}">
                <input type="radio" {if !$user->sms_confirm}checked=""{/if} value="0" name="sms_confirm"> Не отправляем
            </label>
        </div>
    </div>
    </div>
</div>
</div>
<p>&mdash; Для получения СМС сообщений необходимо указать корректный номер мобильного телефона в разделе</p>

<div class="row">
	<div class="col-xs-6">
    	<button type="submit" class="btn btn-success w100">Сохранить</button>
    </div>
</div>