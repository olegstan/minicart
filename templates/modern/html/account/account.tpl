{if $orders}
    <h1>Текущие заказы</h1>
    
    <div class="orderlist current-orders">
	
		{foreach $orders as $order}
		<div class="panel panel-default" data-id="{$order->id}">
			<div class="panel-heading">
				<div class="row">
					<div class="col-xs-3 order-number"><a href="{$config->root_url}{$order_module->url}{$order->url}/">Заказ №{$order->id}</a></div>
					<div class="col-xs-4 orders-status">Статус: <span class="label {if $order->status->group_name == "Новый"}label-success{elseif $order->status->group_name == "Отменен"}label-danger{else}label-primary{/if}">{$order->status->group_name}</span>
						<div class="orderlist-date">{$order->day_str}{*$order->date|date*}</div>
					</div>
					<div class="col-xs-5">
						<div class="orders-repeat">
							<a href="{$config->root_url}{$order_module->url}{$order->url}{url add=['mode'=>'repeat_order']}" class="btn btn-default btn-xs"><i class="fa fa-repeat"></i> Повторить заказ</a>
							{if $order->status->group_name != 'Отменен'} <a href="{$config->root_url}{$order_module->url}{$order->url}{url add=['mode'=>'cancel_order']}" class="btn btn-default btn-xs cancel-order">Отменить заказ</a>{/if}
						</div>
					</div>
				</div>
			</div>
			<div class="panel-body">
				<table class="table ordertable">
					<thead>
					<tr>
						<th class="ordertable-product">Товар</th>
						<th class="ordertable-priceone">Цена</th>
						<th class="ordertable-count">Количество</th>
						<th class="ordertable-priceall">Стоимость</th>
					</tr>
					</thead>
					<tbody>
						{foreach $order->purchases as $purchase}
						<tr>
							<td class="ordertable-product">
								<div class="ordertable-image">
									{if $purchase->image}
										<a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"
										   class="producthref">
											<img src="{$purchase->image->filename|resize:products:52:52}"
												 alt="{$purchase->image->name|escape}"/>
										</a>
									{/if}
								</div>

								<div class="ordertable-product-name">
									<a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"
									   class="producthref">{$purchase->product_name}</a>
									{if $purchase->variant_name}<span>({$purchase->variant_name})</span>{/if}
									{if $purchase->variant->stock == -1}
										<div class="cart-pod-zakaz">
											<span><i class="fa fa-truck"></i> Под заказ</span>
										</div>
									{/if}
								</div>
								{if $sku_value}
									<div class="artikul">Артикул: <span class="sku_value">{$sku_value}</span></div>{/if}

							<td class="ordertable-priceone">
								{$purchase->price|convert} {$main_currency->sign}
							</td>
							<td class="ordertable-count">
								{$purchase->amount} {$settings->units}
							</td>
							<td class="carttable-priceall">
								{($purchase->amount*$purchase->price)|convert} {$main_currency->sign}
							</td>
						</tr>
					{/foreach}
				</table>
			</div>
					
			<ul class="list-group">
				<li class="list-group-item">
					<div class="row">
						<div class="col-xs-3">
							<strong>Сумма заказа:</strong>
						</div>
						<div class="col-xs-9">
							<strong>{$order->total_price|convert} {$main_currency->sign}</strong>{if $order->discount>0} (Скидка {if $order->discount_type == 0}{$order->discount|string_format:"%d"}%{/if} = {($order->total_price_wo_discount-$order->total_price)|convert} {$main_currency->sign}){/if}
						</div>
					</div>
				</li>
                
		        <li class="list-group-item">
					<div class="row">
						<div class="col-xs-3">
							<strong>Способ получения: </strong>
						</div>
						<div class="col-xs-9">
							{$order->delivery_method->name}{if $order->address}&mdash; {$order->address}{/if}
						</div>
					</div>
				</li>
				
                <li class="list-group-item">
					<div class="row">
						<div class="col-xs-3">
							<strong>Способ оплаты: </strong>
						</div>
						<div class="col-xs-9">
							{$order->payment_method->name}
						</div>
					</div>
				</li>
                
				{if $order->comment}
				<li class="list-group-item">
					<div class="row">
						<div class="col-xs-3">
							<strong>Комментарий:</strong>
						</div>
						<div class="col-xs-9">
							{$order->comment}
						</div>
				</li>
				{/if}
			</ul>
		</div>
		{/foreach}
    </div>
{else}
    <h2>У Вас еще нет заказов</h2>
{/if}

<h2>Личный кабинет</h2>
<p>Используя личный кабинет Вы можете сменить <a href="account/?mode=personal-data">личные данные</a> или <a
            href="account/?mode=change-password">пароль</a>. Также доступна <a href="account/?mode=my-orders">история
        заказов</a> и управление оповещениями на <a href="account/?mode=notification-settings">почту и SMS</a></p>


		
<script type="text/javascript">
    $('.cancel-order').click(function () {
		var panel = $(this).closest('div.panel-default');
		var msg = '<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Закрыть</span></button>Ваш заказ №'+panel.data('id')+' отменен</div>';
        $.get($(this).attr('href'), function (data) {
			panel.replaceWith(msg);
			//panel.remove();
			//$('div.orderlist').append(msg);
            //location.reload();
        });
        return false;
    });
</script>