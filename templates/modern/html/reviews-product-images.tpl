<ul id="list1" class="review-images add-image">
	{if $images}
	{foreach $images as $img}
		<li><a class="delete_image" href="{$config->root_url}{$module->url}{url add=['review-images'=>1, 'mode'=>'delete-image', 'image_id'=>$img->id, 'temp_id'=>$temp_id]}"><span class="remove-review-image"><i class="fa fa-times"></i></span><img src="{$img->filename|resize_temp:62:62}"></a></li>
	{/foreach}
	{/if}
</ul>