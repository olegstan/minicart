{if $history_products|count>0}
<div class="header">Вы смотрели:</div>
<ul>
{foreach $history_products as $history_product}
	<li>
		<a class="producthref" href="{$config->root_url}{$product_module->url}{$history_product->url}{$settings->postfix_product_url}" data-placement="bottom" data-toggle="tooltip" data-html="true" data-original-title='{$history_product->name|escape}<br/>{if $history_product->currency_id}{$history_product->variant->price|convert:$history_product->currency_id}{else}{$history_product->variant->price|convert:$admin_currency->id}{/if} {$main_currency->sign}'><img src="{$history_product->image->filename|resize:'products':62:62}"/></a>
	</li>
{/foreach}
</ul>
{/if}