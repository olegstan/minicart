{* Показываем админскую шапку *}
<div class="adminpanel">
    <div class="container">
        <nav class="navbar navbar-default noradius mininavbar" role="navigation">

            <div class="row">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="col-xs-3">
                    <ul class="nav navbar-nav adminmenu">
                        <li><a href="admin/" class="to-backend allow-jump"><span>Админка</span> <i class="fa fa-level-up"></i></a></li>
                        </li>
                    </ul>
                </div>
                <div class="col-xs-9">
                    <ul class="nav navbar-nav otkliki">
                        <li data-module="" class="">
                            <a href="admin/orders-processed/" class="orderspage" data-url="orders-processed/"> <span>Заказы</span> {if $count_new_orders > 0}<span class="badge">{$count_new_orders}</span>{/if}</a>
                        </li>

                        <li data-module="" class="">
                            <a href="admin/contacts/orders-calls/" class="top-message" data-url="contacts/orders-calls/"> <span>Контакт центр</span> {if $count_callbacks > 0}<span class="badge">{$count_callbacks}</span>{/if}</a>
                        </li>

                        <li data-module="" class="">
                            <a href="admin/community/reviews-products/" class="faq" data-url="community/reviews-products/"> <span>Отзывы и вопросы</span> {if $count_new_reviews > 0}<span class="badge">{$count_new_reviews}</span>{/if}</a>
                        </li>

                    </ul>


                    <ul class="nav navbar-nav pull-right adminmenu">
                        <li class="dropdown">
                            <a data-toggle="dropdown" class="dropdown-toggle usermenu" href="#">{$user->name} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="admin/logout/" class="logout"><i class="fa fa-power-off"></i> <span>Выход</span></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</div>