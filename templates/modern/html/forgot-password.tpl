<h1>Сброс пароля</h1>

{if !$success_message}
<form class="form-signin" action="{$config->root_url}{$login_module->url}{url add=['mode'=>'forgot-password']}" method="post" id="login-form">
<div class="row">
	<div class="col-xs-5">
    	<div class="form-group">
        	<label>E-mail указанный при регистрации</label>
		<input type="email" name="email" class="form-control" value="" required/>
        </div>
        
        <div class="form-group">
		<button class="btn btn-primary w100" type="submit">Сбросить пароль</button>
        </div>
    	{*<input type="hidden" name="mode" value="forgot-password"/>*}
    </div>
</div>
</form>
{/if}

{if $error_message}
<div class="alert alert-warning">
{if $error_message == "user_not_found"}Пользователя с таким e-mail не существует
{else}{$error_message}
{/if}
</div>
{/if}

{if $success_message}
<div class="alert alert-success">
{if $success_message == "mail_sended"}Письмо отправлено на почту
{else}{$success_message}
{/if}
</div>
{/if}