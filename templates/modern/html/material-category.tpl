{breadcrumbs module='materials' type='material-category' id=$category->id}

<h1>{if $category->title}{$category->title|escape}{else}{$category->name|escape}{/if}</h1>

{include file='pagination-frontend.tpl'}

{foreach $materials as $material}
<h2><a href="{makeurl module='materials' item=$material}" class="category-materials materialhref">{if $material->title}{$material->title|escape}{else}{$material->name|escape}{/if}</a></h2>

{if $category->show_date}<p class="material-date">{$material->date|date}</p>{/if}
<div class="material-annotation">
	{if $material->image}<a href="{makeurl module='materials' item=$material}"><img src="{$material->image->filename|resize:'materials':160:160}" class="previewimage"/></a>{/if}
	{$material->description|preannotation}
	<p><a href="{makeurl module='materials' item=$material}" class="material-readmore materialhref btn btn-default">Подробнее</a></p>
</div>
<div class="material-separator"></div>
{/foreach}

{if $category->description}
	{$category->description}
{/if}

{include file='pagination-frontend.tpl'}