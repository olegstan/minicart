{breadcrumbs module='products' id=$product->id}

{$allow_edit_module = false}
{if in_array($product_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div class="product-header">
	<h1>{$product->name}{if $allow_edit_module} <span><a href="{$config->root_url}{$product_module_admin->url}{url add=['id'=>$product->id]}" class="edit btn btn-default btn-sm"><i class="fa fa-pencil"></i> Редактировать товар</a></span>{/if}</h1>
	{* список бейджей товара *}
    
		{if $product->badges}
		<div class="product-list-badges">
			{foreach $product->badges as $badge}
				<span class="{$badge->css_class}">{$badge->name}</span>
			{/foreach}
		</div>
		{/if}
</div>
<div class="product-links">

	<div id="main-rating" class="product-stars">        
			<input type="hidden" name="val" value="{$product->rating->avg_rating}">
			<input type="hidden" name="votes" value="{$product->rating->rating_count}"/>
			<input type="hidden" name="product_id" value="{$product->id}"/>
			<input type="hidden" name="vote-id" value="1"/>
   </div>
   
   <a href="" class="write-review-top dotted2" id="write-review"><i class="fa fa-pencil"></i> <span>Написать отзыв</span></a>

    <div class="buy-compare" {if in_array($product->id, $compare_items)}style="display: none;"{/if}><a href="{$config->root_url}{$compare_module->url}{url add=['action'=>'add', 'product_id'=>$product->id, 'ajax'=>1]}" class="dotted2 add_to_compare"><i class="fa fa-bars"></i> <span>Добавить к сравнению</span></a></div>
            <div class="buy-compare" {if !in_array($product->id, $compare_items)}style="display:none;"{/if}><a href="{$config->root_url}{$compare_module->url}" class="compare_items">Сравнить ({$compare_items|count})</a>&nbsp;&nbsp; <a href="{$config->root_url}{$compare_module->url}{url add=['action'=>'delete', 'product_id'=>$product->id, 'ajax'=>1]}" class="dotted2 remove_from_compare"><span>Убрать из сравнения</span></a></div>
    
	{if $user}
		<a href="#" class="dotted2" id="favorite"><i class="fa {if $product->is_favorite}fa-heart{else}fa-heart-o{/if}"></i> <span>{if $product->is_favorite}Удалить из избранного{else}Добавить в избранное{/if}</span></a>
    {else}
		<a class="dotted2" id="favorite" data-placement="bottom" data-toggle="tooltip" data-html="true" data-original-title="Авторизуйтесь, чтобы добавить товар в избранное"><i class="fa fa-heart-o"></i> <span>Добавить в избранное</span></a>
	{/if}  
            
</div>

<div class="row">
	<div class="col-xs-6">
		{if $product->images}
			{* Выводим все картинки товара с помощью fancybox *}
			<a href="{$product->image->filename|resize:'products':1600:1000}" class="main-image fancybox" rel="group">
				<img src="{$product->image->filename|resize:'products':460:460}" alt="{$product->name}" title="{$product->name}"/>
			</a>
			<ul id="more-images">
			{foreach $product->images|cut as $img}
				<li>
					<a href="{$img->filename|resize:'products':1600:1000}" class="fancybox" rel="group">
						<img src="{$img->filename|resize:'products':76:76}" title="{$product->name}" alt="{$product->name}">
					</a>
				</li>
			{/foreach}
			</ul>
		{/if}
        
        {if {banner id=1}}
        <div class="share">
        	{* Подключаем кнопки поделиться от яндекса *}
            {banner id=1}
        </div> 
        {/if}
        
        
    </div>
    
    <div class="col-xs-6">
  	<div class="row">
     <div class="col-xs-6">
    
    {* блок покупки товара *}
    <div id="buy">
    	<div class="buy-inner">
        
		{$selected_variant = $product->variant}
		
		{if $product->variants|count > 1 || ($product->variants|count == 1 && !empty($product->variant->name))}
			{* Если вариантов товара > 1 *}
                	{* Меняем статус в зависимости от статуса варианта *}
                    <div class="buy-status"> 
                        <span class="in-stock product-status" {if $product->variant->stock <= 0}style="display:none;"{/if}><i class="fa fa-check"></i> Есть в наличии</span>
                        <span class="out-of-stock product-status" {if $product->variant->stock != 0}style="display:none;"{/if}><i class="fa fa-times-circle"></i> Нет в наличии</span>
                        <span class="to-order product-status" {if $product->variant->stock >= 0}style="display:none;"{/if}><i class="fa fa-truck"></i> Под заказ</span>
                    </div>
            <div class="form-group buy-list-variants">
                <label>Доступные варианты:</label>
				
				{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ *}
                <select class="form-control" name="variant_id">
					{foreach $product->variants as $variant}
						<option value="{$variant->id}" data-stock="{$variant->stock}" data-price="{if $product->currency_id}{$variant->price|convert:$product->currency_id:false}{else}{$variant->price|convert:$admin_currency->id:false}{/if}" data-price-convert="{if $product->currency_id}{$variant->price|convert:$product->currency_id}{else}{$variant->price|convert:$admin_currency->id}{/if}" data-price-old="{if $product->currency_id}{$variant->price_old|convert:$product->currency_id:false}{else}{$variant->price_old|convert:$admin_currency->id:false}{/if}" data-price-old-convert="{if $product->currency_id}{$variant->price_old|convert:$product->currency_id}{else}{$variant->price_old|convert:$admin_currency->id}{/if}" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if} {if $variant->id == $variant_id}selected{$selected_variant = $variant}{/if} {if $variant@first}checked{/if}>{$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
					{/foreach}                    
                </select>
				{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ (END) *}
				{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ *}
				{*foreach $product->variants as $variant}
					<div class="radio">
					  <label>
						<input type="radio" name="variant_id" value="{$variant->id}" data-stock="{$variant->stock}" data-price="{if $product->currency_id}{$variant->price|convert:$product->currency_id:false}{else}{$variant->price|convert:$admin_currency->id:false}{/if}" data-price-convert="{if $product->currency_id}{$variant->price|convert:$product->currency_id}{else}{$variant->price|convert:$admin_currency->id}{/if}" data-price-old="{if $product->currency_id}{$variant->price_old|convert:$product->currency_id:false}{else}{$variant->price_old|convert:$admin_currency->id:false}{/if}" data-price-old-convert="{if $product->currency_id}{$variant->price_old|convert:$product->currency_id}{else}{$variant->price_old|convert:$admin_currency->id}{/if}" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if} {if $variant->id == $variant_id}checked{$selected_variant = $variant}{/if} {if !$variant_id && $variant@first}checked{/if}/> {$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}
					  </label>
					</div>
				{/foreach*}
				{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ (END) *}
            </div>
		{else}
			{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ *}
			<select name="variant_id" style="display: none;">
				<option value="{$product->variant->id}" data-stock="{$product->variant->stock}" data-price="{if $product->currency_id}{$product->variant->price|convert:$product->currency_id:false}{else}{$product->variant->price|convert:$admin_currency->id:false}{/if}" data-price-convert="{if $product->currency_id}{$product->variant->price|convert:$product->currency_id}{else}{$product->variant->price|convert:$admin_currency->id}{/if}" data-price-old="{if $product->currency_id}{$product->variant->price_old|convert:$product->currency_id:false}{else}{$product->variant->price_old|convert:$admin_currency->id:false}{/if}" data-price-old-convert="{if $product->currency_id}{$product->variant->price_old|convert:$product->currency_id}{else}{$product->variant->price_old|convert:$admin_currency->id}{/if}" data-sku="{$product->variant->sku}" checked>{$product->variant->name}</option>
			</select>
			{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - СЕЛЕКТ (END) *}
			{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ *}
			{*<input type="hidden" name="variant_id" value="{$product->variant->id}" data-stock="{$product->variant->stock}" data-price="{if $product->currency_id}{$product->variant->price|convert:$product->currency_id:false}{else}{$product->variant->price|convert:$admin_currency->id:false}{/if}" data-price-convert="{if $product->currency_id}{$product->variant->price|convert:$product->currency_id}{else}{$product->variant->price|convert:$admin_currency->id}{/if}" data-price-old="{if $product->currency_id}{$product->variant->price_old|convert:$product->currency_id:false}{else}{$product->variant->price_old|convert:$admin_currency->id:false}{/if}" data-price-old-convert="{if $product->currency_id}{$product->variant->price_old|convert:$product->currency_id}{else}{$product->variant->price_old|convert:$admin_currency->id}{/if}" data-sku="{$product->variant->sku}" {if $product->variant->stock == 0}class="out-of-stock"{elseif $product->variant->stock < 0}class="to-order"{/if} checked/>*}
			{* ЕСЛИ ВАРИАНТЫ НУЖНЫ - РАДИОБАТТОНЫ (END) *}
			
			{$selected_variant = $product->variant}
			{* Если 1 вариант *}           
            	
            <div class="buy-status">
				{if $product->variant->stock > 0}<span class="in-stock product-status"><i class="fa fa-check"></i> Есть в наличии</span>{/if}
				{if $product->variant->stock == 0}<span class="out-of-stock product-status"><i class="fa fa-times-circle"></i> Нет в наличии</span>{/if}
				{if $product->variant->stock < 0}<span class="to-order product-status"><i class="fa fa-truck"></i> Под заказ</span>{/if}
			</div>			
          
		{/if}
        
       	<div class="buy-oldprice" {if !$selected_variant->price_old}style="display:none;"{/if}>{*{if $product->currency_id}{$selected_variant->price_old|convert:$product->currency_id}{else}{$selected_variant->price_old|convert:$admin_currency->id}{/if}<span class="currency"> {$main_currency->sign}</span>*}</div>
        <div class="buy-price" {if !$selected_variant->price}style="display:none;"{/if}>{*{if $product->currency_id}{$selected_variant->price|convert:$product->currency_id}{else}{$selected_variant->price|convert:$admin_currency->id}{/if}<span class="currency"> {$main_currency->sign}</span>*}</div>
		
		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
		<div class="buy-price-one-pcs" style="display:none;"></div>
		{/if}
        
		{if $modificators_groups}
			<div class="product-modifier">
				{foreach $modificators_groups as $gr}
					{if $gr->id == 0 || ($gr->id > 0 && !$product->modificators_mode && in_array($gr->id, $category->modificators_groups)) || ($gr->id > 0 && $product->modificators_mode && in_array($gr->id, $product->modificators_groups))}
					
						{if $gr->name}
							<div class="product-modifier-group">
							<label class="modifier-group-header">{$gr->name}</label>
						{/if}
						
						{if $gr->type == 'checkbox'}
						<div class="modifier-checkbox">
							{foreach $gr->modificators as $m}
								{if $gr->id > 0 || ($gr->id == 0 && !$product->modificators_mode && in_array($m->id, $category->modificators)) || ($gr->id == 0 && $product->modificators_mode && in_array($m->id, $product->modificators))}
								<div class="checkbox">
									<label>
									<input type="checkbox" value="{$m->id}" name="modificators[{$gr->id}]" data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}
									</label>
									
									{if $m->multi_buy}
									<div class="modifier-counter">
									<div class="input-group">
										<span class="input-group-btn">
											<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
										</span>
										<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
										<span class="input-group-btn">
											<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
									</div>
									
									{/if}
								</div>
								{/if}
							{/foreach}
							</div>
						{/if}
						
						{if $gr->type == 'radio'}
							{foreach $gr->modificators as $m}
								<div class="radio">
									<label>
									<input type="radio" name="modificators[{$gr->id}]" value="{$m->id}" {if $m@first}checked{/if} data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}
									</label>
									
									{if $m->multi_buy}
									<div class="input-group">
										<span class="input-group-btn">
											<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
										</span>
										<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
										<span class="input-group-btn">
											<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
										</span>
									</div>
									{/if}
								</div>
							{/foreach}
						{/if}
						
						{if $gr->type == 'select'}
							<div class="modifier-select">
							<select name="modificators[{$gr->id}]" class="form-control">
								<option>Не выбрано</option>
								{foreach $gr->modificators as $m}
									<option value="{$m->id}" data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">
									{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
									</option>
								{/foreach}
							</select>
							</div>
						{/if}
						
						{if $gr->name}
							</div>
						{/if}
					{/if}
				{/foreach}
			</div>
		{/if}
        
		{if $settings->catalog_show_multi_buy}
		<div class="price-with-amount"> {* Блок покупки нескольких штук товара *}
        <div class="kolvo">Количество:</div>
            <div class="input-group">
                <span class="input-group-btn">
                    <button data-type="minus" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
                </span>
                
                <input type="text" class="form-control input-sm" placeholder="" 
                        value="1" name="amount"
                        data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">
                        
                <span class="input-group-btn">
                    <button data-type="plus" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
                </span>
            </div>
            
            <div class="buy-price-with-amount" {if !$selected_variant->price}style="display:none;"{/if}></div>
		</div>
		{else}
			<input type="hidden" value="1" name="amount" data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">
		{/if}
		
		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
		<div class="kolvo">{$settings->catalog_variable_amount_name}:</div>
            <div class="input-group">
                <span class="input-group-btn">
                    <button data-type="minus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
                </span>
                
                <input type="text" class="form-control input-sm" placeholder="" 
                        value="{$product->min_amount|string_format:'%d'}" name="var_amount"
                        data-stock="{if $selected_variant->stock == -1}999{else}{$selected_variant->stock}{/if}">
                        
                <span class="input-group-btn">
                    <button data-type="plus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
                </span>
            </div>
            
            <div class="buy-price-with-amount" {if !$selected_variant->price}style="display:none;"{/if}></div>
		</div>
		{/if}
		
		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
		<div class="begunok">
			<div id="modern"></div>
			<div class="clear-begunok"></div>
        </div>
		{/if}
        
		<a href="#" class="btn btn-success btn-lg buy-buy" {if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if}>Добавить в корзину</a>
		{if $product->currency_id}
			<a href="#" class="buy1 btn btn-primary" {*if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if*}>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden" id="buy1spinner"></i></a>
		{else}
			<a href="#" class="buy1 btn btn-primary" {*if $selected_variant->stock == 0 || $selected_variant->stock_available == 0}style="display:none;"{/if*}>Заказ в 1 клик <i class="fa fa-spin fa-spinner hidden" id="buy1spinner"></i></a>
		{/if}
		
		<div class="alert alert-warning" id="no_stock" {if $selected_variant->stock_available >= 0}style="display: none;"{/if}></div>
      
        </div>
        
        <div class="product-id">
        <div id="sku" {if !$product->variant->sku}style="display:none;"{/if}>Артикул: <span>{$product->variant->sku}</span></div>
       {if $settings->use_product_id} 
        Код товара: <span>{$product->id}</span>{/if} </div> 
        
		{if $smarty.session.admin && $smarty.session.admin == "admin"}
			{if $settings->add_field1_enabled && $settings->add_field1_show_frontend && $product->add_field1}
				<div class="product-add-field">{$settings->add_field1_name}: {$product->add_field1}</div>
			{/if}
			{if $settings->add_field2_enabled && $settings->add_field2_show_frontend && $product->add_field2}
				<div class="product-add-field">{$settings->add_field2_name}: {$product->add_field2}</div>
			{/if}
			{if $settings->add_field3_enabled && $settings->add_field3_show_frontend && $product->add_field3}
				<div class="product-add-field">{$settings->add_field3_name}: {$product->add_field3}</div>
			{/if}
		{/if}
		
		{if $smarty.session.admin && $smarty.session.admin == "admin"}
			{if $settings->add_flag1_enabled && $settings->add_flag1_show_frontend}
				<div class="product-add-flag">{if $product->add_flag1}<a href="" class="flag1-on"></a>{else}<a href="" class="flag1-off"></a>{/if}</div>
			{/if}
			{if $settings->add_flag2_enabled && $settings->add_flag2_show_frontend}
				<div class="product-add-flag">{if $product->add_flag2}<a href="" class="flag2-on"></a>{else}<a href="" class="flag2-off"></a>{/if}</div>
			{/if}
			{if $settings->add_flag3_enabled && $settings->add_flag3_show_frontend}
				<div class="product-add-flag">{if $product->add_flag3}<a href="" class="flag3-on"></a>{else}<a href="" class="flag3-off"></a>{/if}</div>
			{/if}
		{/if}
        
     </div>     
    	
  
   
  </div>
  
	<div class="col-xs-6">
        <div class="product-blocks">       
          {if {banner id=15}}
          <div class="product-blocks-item">
            <div class="product-blocks-item-heading">{bannername id=15}</div>           
                {banner id=15}          
          </div>
            {/if}
          	{if {banner id=16}}
          <div class="product-blocks-item">
            <div class="product-blocks-item-heading">{bannername id=16}</div>            
                {banner id=16}            
          </div>
          {/if}
          {if {banner id=18}}
          <div class="product-blocks buytype">
            <div class="product-blocks-item-heading">{bannername id=18}</div>            
                {banner id=18}            
          </div>
          {/if}
        </div>
        
              
        </div>  
  </div>
  

<div class="product-description-top">
	{if $product->annotation2 or $product_tags && $tags_groups}
    <div class="product-description-header">Описание</div>

      <div class="product-description-short">
    	{$product->annotation2}
      </div>

    {/if}
    {if $product_tags && $tags_groups}   
	<div class="product-properties">
		<table class="table table-striped">
			<tbody>
			{foreach $tags_groups as $tag_group}
				{if array_key_exists($tag_group->id, $product_tags) && $tag_group->show_in_frontend}
				<tr>
					<td>{$tag_group->name}{if $tag_group->help_text} <a class="property-help" data-type="qtip" data-content="{$tag_group->help_text|escape}" data-title="{$tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}</td>
					<td>
					{foreach $product_tags[$tag_group->id] as $tag}
					{$tag->name}{if $tag_group->postfix} {$tag_group->postfix}{/if}{if !$tag@last}, {/if}
					{/foreach}
					</td>
				</tr>
				{/if}
			{/foreach}
			
		</table>
	</div>	
    {/if}
</div>  
  
    </div>
  </div>
	



<!-- Nav tabs -->
<div class="products-tabs">
    <ul class="nav nav-tabs">
      {if $count_related_products > 0 || $count_analogs_products > 0}<li class="{if !$tab || $tab == "related"}active{/if}"><a href="#related" data-toggle="tab" data-url="related">Также рекомендуем посмотреть <i class="fa fa-spinner" id="related_spinner"></i></a></li>{/if}
      {if $product->body}<li {if ($count_related_products == 0 && $count_analogs_products == 0 && !$tab) || $tab == "complete-description"}class="active"{/if}><a href="#complete-description" data-toggle="tab" data-url="complete-description">Подробные характеристики</a></li>{/if}
      {if $settings->reviews_enabled}<li {if ($count_related_products == 0 && $count_analogs_products == 0 && !$product->body && !$tab) || $tab == "reviews"}class="active"{/if}><a href="#reviews" data-toggle="tab" data-url="reviews">Отзывы{if $reviews_count>0} ({$reviews_count}){/if}</a></li>{/if}
	  {*<li {if $tab == "questions"}class="active"{/if}><a href="#questions" data-toggle="tab" data-url="questions">Вопрос-ответ</a></li>*}
      {if $product->attachments}<li {if $tab == "files"}class="active"{/if}><a href="#files" data-toggle="tab" data-url="files">Файлы</a></li>{/if}
    </ul>
    
    {* Содержимое некоторых табов можно грузить на аяксе *}
    <div class="tab-content">
      {if $count_related_products > 0 || $count_analogs_products > 0}<div class="tab-pane {if !$tab || $tab == "related"}active{/if}" id="related"></div>{/if}
      {if $product->body}<div class="tab-pane{if ($count_related_products == 0 && $count_analogs_products == 0 && !$tab) || $tab == "complete-description"} active{/if}" id="complete-description">{$product->body}</div>{/if}
      {if $settings->reviews_enabled}
	  <div class="tab-pane{if ($count_related_products == 0 && $count_analogs_products == 0 && !$product->body && !$tab) || $tab == "reviews"} active{/if}" id="reviews">
      	<div class="row">
		<div class="col-xs-9">
            <div id="reviews-list">
            </div>
         </div>
         <div class="col-xs-3">
         	
         </div>
         </div>
	  </div>
	  {/if}
	  {*<div class="tab-pane {if $tab == "questions"}active{/if}" id="questions">
		{include file='questions.tpl'}
	  </div>*}
      
      <div class="tab-pane {if $tab == "files"}active{/if}" id="files">
		{if $product->attachments}
        <div class="product-files">
        	<div class="files-header">Файлы для скачивания:</div>
            <ul class="files">
                {foreach $product->attachments as $attach}
                    <li>
                        <div class="filename">
                            <div class="fileimage">
								{$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}
								{if in_array($attach->extension, $exts)}
									<img src="{$path_frontend_template}/img/fileext/{$attach->extension}.png">
								{else}
									<img src="{$path_frontend_template}/img/fileext/other.png">
								{/if}
							</div>
                            <a href="{$attach->filename|attachment:'products'}" target="_blank">
        {if $attach->name}{$attach->name}{if $attach->extension}.{$attach->extension}{/if}{else}{$attach->filename}{/if}</a>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
        {/if}
	  </div>
      
	</div>

</div>



<script type="text/javascript">

	{if $user}
	$('#favorite').click(function(){
		$.ajax({
			url: "{$config->root_url}/ajax/get_data.php",
			type: "POST",
			data: {
				object: 'products',
				mode: 'product-togglefavorite',
				pid : {$product->id},
				uid : {$user->id},
				session_id: '{$smarty.session.id}'
			},
			success: function(data){
				if (data.success)
				{
					var i = $('#favorite i');
					var s = $('#favorite span');
					i.removeClass('fa-heart').removeClass('fa-heart-o');
					if (data.data){
						i.addClass('fa-heart');
						s.html('Удалить из избранного');
					}
					else{
						i.addClass('fa-heart-o');
						s.html('Добавить в избранное');
					}
					if (data.count > 0)
					{
						$('#favorites-informer').closest('.favorite-products').show();
						$('#favorites-informer').html('Избранное ('+data.count+')');
					}
					else
						$('#favorites-informer').closest('.favorite-products').hide();
				}
			}
		});
		return false;
	});
	{/if}
	
	{* VARIABLE AMOUNT *}
	$('#buy button[data-type=plus-var-amount]').click(function(){
		var input = $('#buy input[name=var_amount]');
		var max_stock = input.data('stock');
		if (input.length > 0)
		{
			var value = parseFloat(input.val()) + {$product->step_amount};
			
			if (value > {$product->max_amount})
				value = {$product->max_amount};
					
			input.val(value);
			
			calculate_price();
			
			$("#modern").slider( "option", "value", value );
			return false;
		}
	});
	
	$('#buy button[data-type=minus-var-amount]').click(function(){
		var input = $('#buy input[name=var_amount]');
		if (input.length > 0)
		{
			var value = parseFloat(input.val()) - {$product->step_amount};
			
			if (value < {$product->min_amount})
				value = {$product->min_amount};
			
			input.val(value);
			
			calculate_price();
			
			$("#modern").slider( "option", "value", value );
			return false;
		}
	});
	
	$('#buy input[name=var_amount]').bind('change keyup input click', function(){
		var max_stock = $(this).data('stock');
	
		if (this.value.match(/[^0-9,.]/g)) {
			this.value = this.value.replace(/[^0-9,.]/g, '');
		}
		
		if (this.value < {$product->min_amount})
			this.value = {$product->min_amount};
		
		if (this.value > {$product->max_amount})
			this.value = {$product->max_amount};
			
		calculate_price();
		
		$("#modern").slider( "option", "value", this.value );
	});
	{* VARIABLE AMOUNT *}

	$('#buy .price-with-amount button[data-type=plus]').click(function(){
		var input = $('#buy input[name=amount]');
		var max_stock = input.data('stock');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) + 1;
			if (value > max_stock)
				value = max_stock;
			input.val(value);
			calculate_price();
		}
	});
	
	$('#buy .price-with-amount button[data-type=minus]').click(function(){
		var input = $('#buy input[name=amount]');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) - 1;
			if (value <=0 )
				value = 1;
			input.val(value);
			calculate_price();
		}
	});
	
	$('#buy input[name=amount]').bind('change keyup input click', function(){
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if (this.value <= 0)
			this.value = 1;
		calculate_price();
	});

	$('.product-modifier button[data-type=minus]').click(function(){
		var i = $(this).closest('.input-group').find('input');
		if (i.val() > i.data('min'))
			i.val(parseInt(i.val()) - 1);
		calculate_price();
	});
	
	$('.product-modifier button[data-type=plus]').click(function(){
		var i = $(this).closest('.input-group').find('input');
		if (i.val() < i.data('max'))
			i.val(parseInt(i.val()) + 1);
		calculate_price();
	});
	
	$('.product-modifier input[name^=modificators_count]').bind('change keyup input click', function(){
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		var min = parseInt($(this).data('min'));
		var max = parseInt($(this).data('max'));
		if (this.value < min)
			this.value = min;
		if (this.value > max)
			this.value = max;
		calculate_price();
	});

	function calculate_price(){
		var price = parseFloat($('select[name=variant_id] option:selected').attr('data-price'));
		{*ЕСЛИ ВАРИАНТЫ - РАДИОБАТТОНЫ*}{*var price = parseFloat($('input[name=variant_id]:checked').attr('data-price'));*}
		var amount = 1;
		var amount_input = $('#buy input[name=amount]');
		if (amount_input.length > 0)
			amount = amount_input.val();
			
		var var_amount = 1;
		var var_amount_input = $('#buy input[name=var_amount]');
		if (var_amount_input.length > 0)
			var_amount = var_amount_input.val();
			
		var total_price = price * var_amount;
		
		$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
			var obj = $(this);
			var obj_type = obj.data('type');
			var obj_value = parseFloat(obj.data('value'));
			var obj_multi_apply = obj.data('multi-apply');
			if (obj_multi_apply == 0 && amount > 1)	//skip processing modificator
				return true;
			var obj_multi_buy = obj.data('multi-buy');
			var obj_multi_buy_value = 1;
			if (obj_multi_buy == 1)
				obj_multi_buy_value = parseInt($('input[name=modificators_count_'+obj.val()+']').val());
			
			if (obj_type == 'plus_fix_sum')
				total_price += obj_value * obj_multi_buy_value;
			if (obj_type == 'minus_fix_sum')
				total_price -= obj_value * obj_multi_buy_value;
			if (obj_type == 'plus_percent')
				total_price += price * obj_value * obj_multi_buy_value / 100;
			if (obj_type == 'minus_percent')
				total_price -= price * obj_value * obj_multi_buy_value / 100;
		});
		
		var total_price_variant_for_multi = total_price;
		
		$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
			var obj = $(this);
			var obj_type = obj.data('type');
			var obj_value = parseFloat(obj.data('value'));
			var obj_multi_apply = obj.data('multi-apply');
			if (!(obj_multi_apply == 0 && amount > 1))	//skip processing modificator
				return true;
			var obj_multi_buy = obj.data('multi-buy');
			var obj_multi_buy_value = 1;
			if (obj_multi_buy == 1)
				obj_multi_buy_value = parseInt($('input[name=modificators_count_'+obj.val()+']').val());
			
			if (obj_type == 'plus_fix_sum')
				total_price += obj_value * obj_multi_buy_value;
			if (obj_type == 'minus_fix_sum')
				total_price -= obj_value * obj_multi_buy_value;
			if (obj_type == 'plus_percent')
				total_price += price * obj_value * obj_multi_buy_value / 100;
			if (obj_type == 'minus_percent')
				total_price -= price * obj_value * obj_multi_buy_value / 100;
		});
		
		var additional_sum = total_price - total_price_variant_for_multi;
		
		if ($('.buy-price-one-pcs').length > 0)
		{
			var total_price_one_pcs_formatted = String(price.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
			$('.buy-price-one-pcs').html(total_price_one_pcs_formatted + ' {$main_currency->sign}');
		}
		
		total_price_formatted = String(total_price.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#buy div.buy-price').html(total_price_formatted + ' {$main_currency->sign}');
		
		total_price = total_price_variant_for_multi * amount + additional_sum;
		total_price_formatted = String(total_price.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#buy div.buy-price-with-amount').html(total_price_formatted + ' {$main_currency->sign}');
	}
	
	$('.product-modifier input, .product-modifier select').change(function(){
		var checked = $(this).prop('checked');
		var d = $(this).closest('div');
		if (checked){
			d.find('input[name^=modificators_count]').prop('disabled', false);
			d.find('button[data-type=minus]').prop('disabled', false);
			d.find('button[data-type=plus]').prop('disabled', false);
		}
		else{
			d.find('input[name^=modificators_count]').prop('disabled', true);
			d.find('button[data-type=minus]').prop('disabled', true);
			d.find('button[data-type=plus]').prop('disabled', true);
		}
		
		calculate_price();
	});

	$(document).ready(function(){
	
		{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
			var $awesome2 = $("#modern").slider({
				min: {$product->min_amount},
				max: {$product->max_amount}, 
				value: {$product->min_amount}, 
				step: {$product->step_amount} 
			});
			
			$awesome2.slider("pips", { 
				rest: "label",
				step: {($product->max_amount-$product->min_amount)/4}
			}).slider("float");
			
			$awesome2.on("slide", function(e,ui) {
				$("input[name=var_amount]").val(ui.value).trigger('change');
			});
		{/if}
	
		$(".fancybox").fancybox({
			autoResize: true,
			autoCenter: true,
       		prevEffect	: 'none',
			nextEffect	: 'none',
			helpers	: {
				title	: {
					type: 'outside'
				},
				thumbs	: {
					width	: 50,
					height	: 50
				},
				overlay : {
       			   locked     : false
		        }
			}
		});
		
		{*ЕСЛИ ВАРИАНТЫ - РАДИОБАТТОНЫ *}{*$('input[name=variant_id]').change(function(){ *}
		$('select[name=variant_id]').change(function(){
			$('#no_stock').hide();
		
			var option = $(this).find('option:selected');
			{*ЕСЛИ ВАРИАНТЫ - РАДИОБАТТОНЫ *}{*var option = $('input[name=variant_id][checked]');*}
			$('#sku span').html(option.data('sku'));
			if (option.attr('data-sku').length > 0)
				$('#sku').show();
			else
				$('#sku').hide();
			$('div.buy-status span').each(function(){
				$(this).hide();
			});
			var stock = parseInt(option.data('stock'));
			if (stock > 0){
				$('div.buy-status span.in-stock').show();
				$('#buy a.buy-buy').show();
				$('#buy a.buy1').show();
			}
			if (stock == 0){
				$('div.buy-status span.out-of-stock').show();
				$('#buy a.buy-buy').hide();
				$('#buy a.buy1').hide();
			}
			if (stock < 0){
				$('div.buy-status span.to-order').show();
				$('#buy a.buy-buy').show();
				$('#buy a.buy1').show();
			}
			var price = parseFloat(option.data('price'));
			var price_old = parseFloat(option.data('price-old'));
			if (price > 0)
				$('div.buy-price').show().html(option.data('price-convert')+' <span class="currency">{$main_currency->sign}</span>');
			else
				$('div.buy-price').hide();
			if (price_old > 0)
				$('div.buy-oldprice').show().html(option.data('price-old-convert')+' <span class="currency">{$main_currency->sign}</span>');
			else
				$('div.buy-oldprice').hide();
				
			var amount_input = $('#buy input[name=amount]');
			if (amount_input.length > 0){
				var new_stock = stock;
				if (new_stock <= 0)
					new_stock = 999;
				amount_input.attr('data-stock', new_stock);
				if (parseInt(amount_input.val()) > new_stock)
					amount_input.val(new_stock);
			}
		});
		
		$('select[name=variant_id]').trigger('change');
		{*ЕСЛИ ВАРИАНТЫ - РАДИОБАТТОНЫ *}{*$('input[name=variant_id][checked]').trigger('change');*}
		
		calculate_price();
		
		$('#content').on('change', 'ul.list li.listitem select[name=variant_id]', function(){
			var li = $(this).closest('li');
			var option = $(this).find('option:selected');
			li.find('span.in-stock').hide();
			li.find('span.out-of-stock').hide();
			li.find('span.to-order').hide();
			
			var stock = parseInt(option.attr('data-stock'));
			if (stock > 0)
			{
				li.find('span.in-stock').show();
				li.find('a.list-buy').show();
				li.find('a.buy-one-click').show();
			}
			if (stock == 0)
			{
				li.find('span.out-of-stock').show();
				li.find('a.list-buy').hide();
				li.find('a.buy-one-click').hide();
			}
			if (stock < 0)
			{
				li.find('span.to-order').show();
				li.find('a.list-buy').show();
				li.find('a.buy-one-click').show();
			}
			var price = parseFloat(option.attr('data-price'));
			var price_old = parseFloat(option.attr('data-price-old'));
			if (price > 0)
				li.find('div.list-price').show().html(option.attr('data-price-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.list-price').hide();
			if (price_old > 0)
				li.find('div.list-old-price').show().html(option.attr('data-price-old-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.list-old-price').hide();
			if (price < {$settings->cart_order_min_price} || stock == 0)
				li.find('a.buy-one-click').hide();
			else
				li.find('a.buy-one-click').show();
		});
		
		$('#content').on('change', 'ul.plitka li.plitka-item select[name=variant_id]', function(){
			var li = $(this).closest('li');
			var option = $(this).find('option:selected');
			li.find('div.plitka-status span').each(function(){
				$(this).hide();
			});
			var stock = parseInt(option.attr('data-stock'));
			if (stock > 0)
			{
				li.find('span.in-stock').show();
				li.find('a.plitka-buy').show();
			}
			if (stock == 0)
			{
				li.find('span.out-of-stock').show();
				li.find('a.plitka-buy').hide();
			}
			if (stock < 0)
			{
				li.find('span.to-order').show();
				li.find('a.plitka-buy').show();
			}
			var price = parseFloat(option.attr('data-price'));
			var price_old = parseFloat(option.attr('data-price-old'));
			if (price > 0)
				li.find('div.plitka-price').show().html(option.attr('data-price-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-price').hide();
			if (price_old > 0)
				li.find('div.plitka-old-price').show().html(option.attr('data-price-old-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-old-price').hide();
		});
	
		{if $count_related_products > 0 || $count_analogs_products > 0}
		$.get("{$config->root_url}{$products_module->url}{url add=['related_products'=>$product->id]}", function(data){
			$('#related').html(data);
			$('#related_spinner').hide();
			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});
		});
		{/if}
		
		{if $settings->reviews_enabled}
		$.get("{$config->root_url}{$products_module->url}{url add=['reviews'=>$product->id, 'page'=>$page, 'sort'=>$sort]}", function(data){
			$('#reviews-list').html(data);
			
			$('#user-rating2-1, #user-rating2-2').rating({
				fx: 'full',
				image: '{$path_frontend_template}/img/stars/big.png',
				loader: '<i class="fa fa-spinner fa-spin"></i>',
				minimal: 1,
				{*url: '{$config->root_url}{$product_module->url}',*}
				preselectMode: false,
				click: function(data){
					var index = $(this).closest('.my-review').data('index');
					$('#rating-block-error-'+index).hide();
					$('#reviews-list input[name=rating]').val(data);
				}
			});
		});
		{/if}
		
		$.get("{$config->root_url}{$pages_module->url}{url add=['material_id'=>4]}", function(data){
			if (data.success)
				$('#delivery').html(data.description);
		});
		
		$('#main-rating').rating({
			fx: 'full',
			image: '{$path_frontend_template}/img/stars/stars.png',
			loader: '<i class="fa fa-spinner fa-spin"></i>',
			minimal: 1,
			url: '{$config->root_url}{$product_module->url}',
			callback: function(data){
				/*$('#user-rating input[name=val]').val(this._data.val);
				$('#user-rating').rating('update', {
					val: this._data.val
				});
				$('#user-rating2-1').rating('update', {
					val: this._data.val
				});*/
				$('#user-rating2-2').rating('update', {
					val: this._data.score
				});
				$('#reviews-list input[name=rating]').val(this._data.score);
			}
		});
		
		$('#user-rating').rating({
			fx: 'full',
			image: '{$path_frontend_template}/img/stars/stars.png',
			loader: '<i class="fa fa-spinner fa-spin"></i>',
			minimal: 1,
			url: '{$config->root_url}{$product_module->url}',
			preselectMode: true
		});
		
	});
	
	$('a.buy-buy').click(function(){
		var variant_id = $(this).closest('#buy').find('select[name=variant_id] option:selected').val();
		{*ЕСЛИ ВАРИАНТЫ РАДИОБАТТОНЫ*}{*var variant_id = $(this).closest('#buy').find('input[name=variant_id]:checked').val();*}
		var modificators = [];
		var modificators_count = [];
		var amount = 1;
		var amount_input = $('#buy input[name=amount]');
		if (amount_input.length > 0)
			amount = amount_input.val();
			
		var var_amount = 1;
		var var_amount_input = $('#buy input[name=var_amount]');
		if (var_amount_input.length > 0)
			var_amount = var_amount_input.val();
		
		$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
			if (parseInt($(this).val()) > 0)
			{
				modificators.push($(this).val());
				var ic = $(this).closest('div').find('input[name^=modificators_count]');
				if (ic.length > 0)
					modificators_count.push(ic.val());
				else
					modificators_count.push(1);
			}
		});
		
		var href = "{$config->root_url}{$cart_module->url}?action=add_variant&variant_id="+variant_id+"&modificators="+modificators.join(',')+"&modificators_count="+modificators_count.join(',')+"&amount="+amount+"&var_amount="+var_amount;
		$.get(href, function(data){
			if (data.success)
			{
				$('#cart-placeholder').html(data.data);
				if (data.additional_result != undefined && data.additional_result.message == "no_stock")
				{
					{*$('.buy-buy').hide();
					$('.buy1').hide();*}
					$('#no_stock').html('Вы можете купить товар в количестве не более ' + data.additional_result.max_stock + ' штук');
					$('#no_stock').show();
				}
				else
					if (data.additional_result != undefined && data.additional_result.added_form !=undefined)
					{
						$('#after-buy-form .modal-header span.modal-title').html(data.additional_result.modal_header);
						$('#after-buy-form .modal-body').html(data.additional_result.added_form);
						$('#after-buy-form').modal();
					}
				return false;
			}
		});
		{*var obj1 = $(this).offset();
		var obj2 = $('#cart-placeholder').offset();
		var dx = obj1.left - obj2.left;
		var dy = obj1.top - obj2.top;
		var distance = Math.sqrt(dx * dx + dy * dy);
		$('#content a.main-image img').effect("transfer", { to: $("#cart-placeholder"), className: "transfer_class" }, distance);	
		$('.transfer_class').html($('#content a.main-image').html());
		$('.transfer_class').find('img').css('height', '100%');*}
		return false;
	});
	
	{if $settings->reviews_enabled}
		$('#reviews-list').on('focusout', '.my-review input[name=name]', function(){
			var index = $(this).closest('.my-review').data('index');
			if ($(this).val().length == 0)
				$('#name-block-error-'+index).show();
			else
				$('#name-block-error-'+index).hide();
		});
		
		$('#reviews-list').on('focusout', '.my-review textarea[name=comments]', function(){
			var index = $(this).closest('.my-review').data('index');
			if ($(this).val().length == 0)
				$('#comment-block-error-'+index).show();
			else
				$('#comment-block-error-'+index).hide();
		});

		$('#reviews-list').on('click', '#add-review-1, #add-review-2', function(){
		
			var isError = false;
			var index = $(this).data('index');
			var review = $(this).closest('.my-review');
		
			if (review.find('input[name=rating]').val().length == 0){
				$('#rating-block-error-'+index).show();
				isError = true;
			}
			
			if (review.find('input[name=name]').val().length == 0){
				$('#name-block-error-'+index).show();
				isError = true;
			}
			
			if (review.find('textarea[name=comments]').val().length == 0){
				$('#comment-block-error-'+index).show();
				isError = true;
			}
			
			if (isError)
				return false;
			
			$('#rating-block-error-'+index).hide();
			$('#name-block-error-'+index).hide();
			$('#comment-block-error-'+index).hide();
		
			$.ajax({
				url: '{$config->root_url}{$product_module->url}',
				type: 'POST',
				data: {
					name: review.find('input[name=name]').val(),
					short: review.find('input[name=short]').val(),
					pluses: review.find('textarea[name=pluses]').val(),
					minuses: review.find('textarea[name=minuses]').val(),
					comments: review.find('textarea[name=comments]').val(),
					recommended: review.find('input[name=recommended]').is(':checked') ? 1 : 0,
					rating: review.find('input[name=rating]').val(),
					product_id: {$product->id},
					temp_id: review.find('input[name=temp_id]').val()
				},
				dataType: 'json',
				success: function(data){
					$.get("{$config->root_url}{$products_module->url}{url add=['reviews'=>$product->id]}", function(data){
						$('#reviews-list').html(data);
						$('#add-review-result-'+index).show();
						
						$('#user-rating2-1, #user-rating2-2').rating({
							fx: 'full',
							image: '{$path_frontend_template}/img/stars/big.png',
							loader: '<i class="fa fa-spinner fa-spin"></i>',
							minimal: 1,
							preselectMode: true,
							click: function(data){
								$('#reviews-list input[name=rating]').val(data);
							}
						});
					});
				}
			});
		});
		
		$('#reviews-list').on('click', '.usergrade-yes', function(){
			var r = $(this).closest('div.review-body');
			$.ajax({
				url: '{$config->root_url}{$product_module->url}',
				type: 'POST',
				data: {
					review_id: r.data('id'),
					helpful: 1,
					nothelpful: 0
				},
				dataType: 'json',
				success: function(data){
					if (data.success){
						r.find('.usergrade-yes').next('span:first').html(data.helpful);
						r.find('.usergrade-no').next('span:first').html(data.nothelpful);
					}
					else
						if (data.message == 'exists')
							alert('Вы уже отметили полезность этого отзыва!');
				}
			});
			return false;
		});
		
		$('#reviews-list').on('click', '.usergrade-no', function(){
			var r = $(this).closest('div.review-body');
			$.ajax({
				url: '{$config->root_url}{$product_module->url}',
				type: 'POST',
				data: {
					review_id: r.data('id'),
					helpful: 0,
					nothelpful: 1
				},
				dataType: 'json',
				success: function(data){
					if (data.success){
						r.find('.usergrade-yes').next('span:first').html(data.helpful);
						r.find('.usergrade-no').next('span:first').html(data.nothelpful);
					}
					else
						if (data.message == 'exists')
							alert('Вы уже отметили полезность этого отзыва!');
				}
			});
			return false;
		});
		
		$('#reviews-list').on('click', '.rate-sort-item a', function(){
			var url = $(this).attr('href');
			var sort_method = $(this).closest('div').data('sort');
			var mainUrl = "{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}?tab=reviews&sort="+sort_method;
			
			if (typeof history.pushState != undefined) {
				history.pushState({ mode: 'product', url: mainUrl},null,encodeURI(decodeURI(mainUrl)));
			}
			
			$.get(url, function(data){
				$('#reviews-list').html(data);
				
				$('#user-rating2-1, #user-rating2-2').rating({
					fx: 'full',
					image: '{$path_frontend_template}/img/stars/big.png',
					loader: '<i class="fa fa-spinner fa-spin"></i>',
					minimal: 1,
					{*url: '{$config->root_url}{$product_module->url}',*}
					preselectMode: true,
					click: function(data){
						var index = $(this).closest('.my-review').data('index');
						$('#rating-block-error-'+index).hide();
						$('#reviews-list input[name=rating]').val(data);
					}
				});
			});
			return false;
		});
	{/if}

	{if $settings->reviews_enabled}
		$('div.products-tabs ul.nav-tabs a').click(function(){
			var url = "{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}?tab="+$(this).data('url');
			if (typeof history.pushState != undefined) {
				history.pushState({ mode: 'product', url: url},null,encodeURI(decodeURI(url)));
			}
		});
	{/if}
	
	$('#write-review').click(function(){
		$('.products-tabs ul.nav-tabs li.active').removeClass('active');
		$('.products-tabs ul.nav-tabs a[data-url=reviews]').closest('li').addClass('active');
		
		$('.products-tabs .tab-content div.tab-pane.active').removeClass('active');
		$('#reviews').addClass('active');
		
		$('html, body').animate({
			scrollTop: $("#add-review-bottom").offset().top
		}, 0);
		
		return false;
	});
</script>