
<div class="row">
	<div class="col-xs-6">
		<div class="after-buy-image">
			{if $added_product->image}
			<img src="{$added_product->image->filename|resize:'products':230:230}">
			{/if}
		</div>
		<div class="after-buy-price">
    
		</div>
    </div>
    
    <div class="col-xs-6">
    	<div class="after-buy-prod-name">
			{$added_product->name} {if $added_variant->name}<span>{$added_variant->name}</span>{/if}
		</div>
		
		{if $settings->catalog_use_variable_amount && $added_purchase->var_amount > 1}
		<div class="cart-modifier" data-var-amount="{$added_purchase->var_amount}">
			<ul>
				<li>
					<div>{$settings->catalog_variable_amount_name} {$added_purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}</div>
				</li>
			</ul>
		</div>
		{/if}
		
		{if $added_purchase->modificators}
			<div class="cart-modifier">
				<ul>
					{foreach $added_purchase->modificators as $m}
						<li><div>{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert}%){elseif $m->type == 'minus_percent'}(-{$m->value|convert}%){/if} {if $added_purchase->modificators_count[$m@index]>1}{$added_purchase->modificators_count[$m@index]} шт.{/if}</div></li>
					{/foreach}
				</ul>
			</div>
		{/if}
    
    	<div class="after-buy-check"><i class="fa fa-check"></i> Добавлен в корзину</div>
        В вашей корзине <strong>{$cart->total_products}</strong> {$cart->total_products|plural:'товар':'товаров':'товара'}<br/> на сумму <strong>{$cart->total_price|convert} {$main_currency->sign}</strong>
        
    </div>
</div>
