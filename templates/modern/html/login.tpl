<div class="row">
	<div class="col-xs-4">
        <form id="login-form" class="form-signin" action="user/login" method="POST" novalidate>
            <fieldset>
                <div class="form-group">
                    <label>Почта или Номер мобильного</label>
                    <input id="login-form-username" type="email" class="form-control" name="username" value="" data-type="text"/>
                    <p class="help-block hidden"></p>
                </div>
                <div class="form-group">
                    <label>Пароль</label>
                    <input id="login-form-password" type="password" class="form-control required" name="password" value="" data-type="text"/>
                    <p class="help-block hidden"></p>
                </div>
                <div class="form-group">
                <button class="btn btn-primary w100" type="submit">Войти</button>
                </div>
                <div id="login-form-error" class="alert alert-danger hidden">
                </div>
            </fieldset>
        </form>
	</div>
</div>

<p><a href="{$config->root_url}{$module->url}{url add=['mode'=>'forgot-password']}">Забыли пароль?</a></p>
<p><a href="{$config->root_url}{$register_module->url}">Хочу зарегистрироваться</a></p>

<script type="text/javascript" src="{$path_frontend_template}js/views/user/login.js"></script>