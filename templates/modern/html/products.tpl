{* Шаблон списка товаров *}

{* Выводится название категории, то что в "Заголовок категории", если же оно пусто, выводится "Название пункта меню"*}

{breadcrumbs module='categories' id=$category->id}

{$allow_edit_module = false}
{if in_array($category_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<h1>{if $category->frontend_name}{$category->frontend_name}{else}{$category->name}{/if}{if $allow_edit_module} <span><a href="{$config->root_url}{$category_module_admin->url}{url add=['id'=>$category->id]}" class="edit btn btn-default btn-sm"><i class="fa fa-pencil"></i> Редактировать категорию</a></span>{/if}</h1>

{* Текстовое описание сверху *}
{if $current_page_num == 1 && !(is_array($filter_tags) && !empty($filter_tags))}
	<div class="decription-category-top">
        <div class="row">
			{if $category->images}
        	<div class="col-xs-6 category-images">
				{if $category->image}
            	<a href="{$category->image->filename|resize:'categories-gallery':1100:800}" class="main-cetegory-image fancybox" rel="group">
					<img src="{$category->image->filename|resize:'categories-gallery':335:235}" alt="{if $category->frontend_name}{$category->frontend_name}{else}{$category->name}{/if}" title="{if $category->frontend_name}{$category->frontend_name}{else}{$category->name}{/if}">
                </a>
				{/if}
				{if $category->images|count > 1}
                <ul class="category-images-additional">
					{foreach $category->images|cut as $img}
					<li>
						<a href="{$img->filename|resize:'categories-gallery':1100:800}" class="fancybox" rel="group">
							<img src="{$img->filename|resize:'categories-gallery':71:71}" alt="{if $category->frontend_name}{$category->frontend_name}{else}{$category->name}{/if}" title="{if $category->frontend_name}{$category->frontend_name}{else}{$category->name}{/if}">
						</a>
                    </li>
					{/foreach}
                </ul>
				{/if}
            </div>
			{/if}
            <div class="{if $category->images}col-xs-6{else}col-xs-12{/if}">
            	{$category->description}
            </div>
        </div>
	</div>
{/if}

{* ПОДКАТЕГОРИИ *}
{if $category && $category->subcategories && $settings->catalog_show_subcategories}
<ul id="found-categories">
	{$mh_group = 1}
	{$iteration = 0}
	{foreach $category->subcategories as $f}
		{if $f->is_visible}
		{$iteration = $iteration + 1}
		<li class="{$f->css_class}">
				<a href="{$config->root_url}{$products_module->url}{$f->url}/">
                <div class="found-categories-image">
				{if $f->image}<img src='{$f->image->filename|resize:categories:155:155}' alt='{$f->image->name}'>{/if}
                </div>
				<div class="found-categories-name" {if $mh_group}data-mh="cgroup-{$mh_group}"{/if}>{$f->name}</div>
                </a>
		</li>
		{/if}
	{/foreach}
</ul>
{/if}
{* ПОДКАТЕГОРИИ (End) *}

<div class="selected-tags">
    <div class="tags-list">
		{foreach $filter_tags_groups as $filter_tag_group}
			{if $filter_tag_group->mode == "range"}
				{assign var="filter_from_value" value=$filter_tag_group->min_value}
				{assign var="filter_to_value" value=$filter_tag_group->max_value}
				{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)}
					{if $filter_tag_group->numeric_sort}
						{assign var="filter_from_value" value=$filter_tags[$filter_tag_group->group_id]->from|convert}
						{assign var="filter_to_value" value=$filter_tags[$filter_tag_group->group_id]->to|convert}
					{else}
						{assign var="filter_from_value" value=$filter_tags[$filter_tag_group->group_id]->from}
						{assign var="filter_to_value" value=$filter_tags[$filter_tag_group->group_id]->to}
					{/if}
				{/if}
				{if $filter_from_value != $filter_tag_group->min_value || $filter_to_value != $filter_tag_group->max_value}
					<span class="tag" data-mode="range" data-name="{$filter_tag_group->name_translit}">{if $filter_tag_group->show_prefix_in_frontend_filter && $filter_tag_group->prefix}{$filter_tag_group->prefix}: {/if}{$filter_from_value}–{$filter_to_value} {$filter_tag_group->postfix} <a href="#"><i class="fa fa-times"></i></a></span>
				{/if}
			{/if}
			{if $filter_tag_group->mode == "select" || $filter_tag_group->mode == "checkbox" || $filter_tag_group->mode == "radio"}
				{$value = ""}
				{foreach $filter_tag_group->tags_all as $tag}
					{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag->id, $filter_tags[$filter_tag_group->group_id])}
						<span class="tag" data-mode="{$filter_tag_group->mode}" data-name="{$filter_tag_group->name_translit}" data-tag="{$tag->id}">{if $filter_tag_group->show_prefix_in_frontend_filter && $filter_tag_group->prefix}{$filter_tag_group->prefix}: {/if}{$tag->name}{if $filter_tag_group->postfix} {$filter_tag_group->postfix}{/if} <a href="#"><i class="fa fa-times"></i></a></span>
					{/if}
				{/foreach}
			{/if}
			{if $filter_tag_group->mode == "logical"}
				{$tag_id = 0}
				{foreach $filter_tag_group->tags_all as $tag}
				{if $tag->name == "да"}{$tag_id = $tag->id}{/if}
				{/foreach}
				{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag_id, $filter_tags[$filter_tag_group->group_id])}
					<span class="tag" data-mode="logical" data-name="{$filter_tag_group->name_translit}">{$filter_tag_group->name} <a href="#"><i class="fa fa-times"></i></a></span>
				{/if}
			{/if}
		{/foreach}
    </div>
</div>

{if $products|count > 0}
<div id="sortview">

<ul class="nav nav-pills" id="sort">                    
	<li class="disabled">Сортировать по:</li>
	
	{assign var="sort_page_num" value=1}
	{if $current_page_num == 'all'}{assign var="sort_page_num" value='all'}{/if}
	
	{foreach $sort_methods as $index=>$method}
		{if $method@first && ((!array_key_exists('sort', $params_arr) || $params_arr['sort'] == $method) && $sort == "position") && $method == "position"}
			<li><a class="btn btn-link btn-small current"><span>{$sort_methods_name[$index]}</span></a></li>
		{elseif $method@first && !array_key_exists('sort', $params_arr) && $sort == "position"}
			<li><a data-type="{$data_type}" class="btn btn-link btn-small current" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['sort_type'=>'desc','page'=>$sort_page_num]}" data-placement="bottom" data-toggle="tooltip" data-original-title="{$sort_methods_biger[$index]}"><i class="{$sort_methods_icon_asc[$index]}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
		{elseif $params_arr['sort'] == $method || $sort == $method}
			{if $sort_methods_direction[$index] == 'asc'}
				<li><a data-type="{$data_type}" class="btn btn-link btn-small current" href="{$config->root_url}{$module->url}{$current_url}{if $params_arr['sort_type'] == 'asc' || $sort_type == 'asc'}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'desc', 'page'=>$sort_page_num]}{elseif $params_arr['sort_type'] == 'desc' || $sort_type == 'desc'}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}{else}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort_type'] == 'asc' || $sort_type == 'asc'}{$sort_methods_biger[$index]}{else}{$sort_methods_lower[$index]}{/if}"><i class="{if $params_arr['sort_type'] == 'desc' || $sort_type == 'desc'}{$sort_methods_icon_desc[$index]}{else}{$sort_methods_icon_asc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
			{else}
				<li><a data-type="{$data_type}" class="btn btn-link btn-small current" href="{$config->root_url}{$module->url}{$current_url}{if $params_arr['sort_type'] == 'asc' || $sort_type == 'asc'}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'desc', 'page'=>$sort_page_num]}{elseif $params_arr['sort_type'] == 'desc' || $sort_type == 'desc'}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}{else}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>'asc','page'=>$sort_page_num]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort_type'] == 'asc' || $sort_type == 'asc'}{$sort_methods_biger[$index]}{else}{$sort_methods_lower[$index]}{/if}"><i class="{if $params_arr['sort_type'] == 'desc' || $sort_type == 'desc'}{$sort_methods_icon_asc[$index]}{else}{$sort_methods_icon_desc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
			{/if}
		{else}
			{if $sort_methods_direction[$index] == 'asc'}
				<li><a data-type="{$data_type}" class="btn btn-link btn-small" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>$sort_methods_direction[$index],'page'=>$sort_page_num]}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == $method && ($params_arr['sort_type'] == 'asc' || $sort_type == 'asc')}{$sort_methods_biger[$index]}{else}{$sort_methods_lower[$index]}{/if}"><i class="{if $params_arr['sort'] == $method && ($params_arr['sort_type'] == 'desc' || $sort_type == 'desc')}{$sort_methods_icon_desc[$index]}{else}{$sort_methods_icon_asc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
			{else}
				<li><a data-type="{$data_type}" class="btn btn-link btn-small" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['sort'=>$method, 'sort_type'=>$sort_methods_direction[$index],'page'=>$sort_page_num]}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == $method && ($params_arr['sort_type'] == 'asc' || $sort_type == 'asc')}{$sort_methods_lower[$index]}{else}{$sort_methods_biger[$index]}{/if}"><i class="{if $params_arr['sort'] == $method && ($params_arr['sort_type'] == 'desc' || $sort_type == 'desc')}{$sort_methods_icon_desc[$index]}{else}{$sort_methods_icon_asc[$index]}{/if}"></i> <span>{$sort_methods_name[$index]}</span></a></li>
			{/if}
		{/if}
	{/foreach}	

</ul>

<div id="view">	
    <div class="view-style">
    	<div class="btn-group">
			{*<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'list']}" class="btn btn-default btn-sm {if !array_key_exists('mode', $params_arr) || $params_arr['mode'] == 'list'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Списком"><i class="fa fa-bars"></i></a>
			<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'tile']}" class="btn btn-default btn-sm {if $params_arr['mode'] == 'tile'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Плиткой"><i class="fa fa-th"></i></a>*}
			<a data-type="{$data_type}" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'list']}" class="btn btn-default btn-sm {if !$mode || $mode == 'list'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Списком"><i class="fa fa-bars"></i></a>
			<a data-type="{$data_type}" href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'tile']}" class="btn btn-default btn-sm {if $mode == 'tile'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Плиткой"><i class="fa fa-th"></i></a>
		</div>
    </div>
    <div class="view-how">Отображать:</div>
</div>

</div>

<!-- Стиль списка товаров фильтры и прочее начало -->
{include file='pagination-frontend.tpl'}

{include file='products-part.tpl'}

{else}
	<p>Нет товаров</p>
{/if}

{if $current_page_num == 1 && !(is_array($filter_tags) && !empty($filter_tags))}
<div class="decription-category-bottom">
{$category->description2}
</div>
{/if}

<script type="text/javascript">
	$(document).ready(function(){
	
		$(".fancybox").fancybox({
			autoResize: true,
			autoCenter: true,
       		prevEffect	: 'none',
			nextEffect	: 'none',
			helpers	: {
				title	: {
					type: 'outside'
				},
				thumbs	: {
					width	: 50,
					height	: 50
				},
				overlay : {
       			   locked     : false
		        }
			}
		});
	
		$('ul.list').on('change', 'li.listitem select[name=variant_id]', function(){
			var li = $(this).closest('li');
			var option = $(this).find('option:selected');
			li.find('span.in-stock').hide();
			li.find('span.out-of-stock').hide();
			li.find('span.to-order').hide();
			
			var stock = parseInt(option.attr('data-stock'));
			if (stock > 0)
			{
				li.find('span.in-stock').show();
				li.find('a.list-buy').show();
				li.find('a.buy-one-click').show();
			}
			if (stock == 0)
			{
				li.find('span.out-of-stock').show();
				li.find('a.list-buy').hide();
				li.find('a.buy-one-click').hide();
			}
			if (stock < 0)
			{
				li.find('span.to-order').show();
				li.find('a.list-buy').show();
				li.find('a.buy-one-click').show();
			}
			var price = parseFloat(option.attr('data-price'));
			var price_old = parseFloat(option.attr('data-price-old'));
			if (price > 0)
				li.find('div.list-price').show().html(option.attr('data-price-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.list-price').hide();
			if (price_old > 0)
				li.find('div.list-old-price').show().html(option.attr('data-price-old-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.list-old-price').hide();
			if (price < {$settings->cart_order_min_price} || stock == 0)
				li.find('a.buy-one-click').hide();
			else
				li.find('a.buy-one-click').show();
		});
		$('ul.plitka').on('change', 'li.plitka-item select[name=variant_id]', function(){
			var li = $(this).closest('li');
			var option = $(this).find('option:selected');
			li.find('div.plitka-status span').each(function(){
				$(this).hide();
			});
			var stock = parseInt(option.attr('data-stock'));
			if (stock > 0)
			{
				li.find('span.in-stock').show();
				li.find('a.plitka-buy').show();
			}
			if (stock == 0)
			{
				li.find('span.out-of-stock').show();
				li.find('a.plitka-buy').hide();
			}
			if (stock < 0)
			{
				li.find('span.to-order').show();
				li.find('a.plitka-buy').show();
			}
			var price = parseFloat(option.attr('data-price'));
			var price_old = parseFloat(option.attr('data-price-old'));
			if (price > 0)
				li.find('div.plitka-price').show().html(option.attr('data-price-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-price').hide();
			if (price_old > 0)
				li.find('div.plitka-old-price').show().html(option.attr('data-price-old-convert')+' <span>{$main_currency->sign}</span>');
			else
				li.find('div.plitka-old-price').hide();
		});
	
		$('ul.plitka .plitka-name-block').matchHeight(1);
		$('ul.plitka .plitka-description').matchHeight(1);
	});
</script>

<!-- Стиль списка товаров фильтры и прочее  конец -->