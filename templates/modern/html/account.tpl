<div class="row">
	<div class="col-xs-3">
		<div class="list-group accountmenu">
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'account']}" class="list-group-item {if $params_arr['mode']=='account'}active{/if}"><i class="fa fa-user"></i> <span>Личный кабинет</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'my-orders']}" class="list-group-item {if $params_arr['mode']=='my-orders'}active{/if}"><i class="fa fa-list"></i> <span>Мои заказы</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'notification-settings']}" class="list-group-item {if $params_arr['mode']=='notification-settings'}active{/if}"><i class="fa fa-envelope-o"></i> <span>E-mail и SMS</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'personal-data']}" class="list-group-item {if $params_arr['mode']=='personal-data'}active{/if}"><i class="fa fa-info-circle"></i> <span>Личные данные</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'delivery-address']}" class="list-group-item {if $params_arr['mode']=='delivery-address'}active{/if}"><i class="fa fa-truck"></i> <span>Адреса доставки</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'account-details']}" class="list-group-item {if $params_arr['mode']=='account-details'}active{/if}"><i class="fa fa-credit-card"></i> <span>Реквизиты для счетов</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'change-password']}" class="list-group-item {if $params_arr['mode']=='change-password'}active{/if}"><i class="fa fa-refresh"></i> <span>Сменить пароль</span></a>
		</div>
        
        <div class="list-group accountmenu">
        	<a href="{$config->root_url}{$module->url}{url add=['mode'=>'my-favorites']}" class="list-group-item {if $params_arr['mode']=='my-favorites'}active{/if}"><i class="fa fa-heart"></i> <span>Избранные товары {if $favorites_products_count}({$favorites_products_count}){/if}</span></a>
			<a href="{$config->root_url}{$module->url}{url add=['mode'=>'my-reviews']}" class="list-group-item {if $params_arr['mode']=='my-reviews'}active{/if}"><i class="fa fa-star"></i> <span>Мои отзывы</span></a>
            <a href="{$config->root_url}{$module->url}{url add=['mode'=>'my-questions']}" class="list-group-item {if $params_arr['mode']=='my-questions'}active{/if}"><i class="fa fa-comments-o"></i> <span>Мои вопросы</span></a>
			         
		</div>
        
		{if $user && $user->discount>0}
        <div class="current-discount">
        Ваша скидка: <span>{$user->discount}%</span>
        </div>
		{/if}
        
	</div>
	<div class="col-xs-9 account-content">
		<form action="{$config->root_url}{$module->url}{url current_params=$current_params}" method="POST">
			{include file='account/'|cat:$mode|cat:'.tpl'}
		</form>
	</div>
</div>