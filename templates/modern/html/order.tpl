{if $order}

<script src="http://mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>

<script type="text/javascript">

	var goods = [];
	
	{foreach $purchases as $purchase}
	goods.push({
		id: {$purchase->product->id},
		name: "{$purchase->product->name|escape:'quotes'}",
		price: {$purchase->price},
		quantity: {$purchase->amount}
	});
	{/foreach}

	var yaParams = {
		order_id: {$order->id},
		order_price: {$order->total_price}, 
		currency: "RUR",
		exchange_rate: 1,
		goods: goods
	};
	
	var yaCounter{$settings->yandex_metric_counter} = new Ya.Metrika({
		id: {$settings->yandex_metric_counter}, 
		params: yaParams
	});
</script>

<script>
	var products = [];
	
	{foreach $purchases as $purchase}
	products.push({
		sku: "{$purchase->sku}",
		name: "{$purchase->product->name|escape:'quotes'}",
		category: "{$purchase->breadcrumbs|escape:'quotes'}",
		price: {$purchase->price},
		quantity: {$purchase->amount}
	});
	{/foreach}

	dataLayer = [{
		"transactionId": "{$order->id}",
		"transactionTotal": {$order->total_price},
		"transactionShipping": {$order->delivery_price},
		"transactionProducts": products
	}];
</script>

	<h1>Ваш заказ №{$order->id}</h1>
	
	<p>Статус заказа: {$order_status->group_name}</p>
	
	{if $order_status->group_name == "Новый"}
	  {if {banner id=21}}
      		<div class="alert alert-success">		
            	{banner id=21}				
            </div>
       {/if}
	{/if}
	
	   
    <table class="table ordertable">
        <thead>
          <tr>
            <th class="ordertable-product">Товар</th>
            <th class="ordertable-priceone">Цена</th>
            <th class="ordertable-count">Количество</th>
            <th class="ordertable-priceall">Стоимость</th>
          </tr>
        </thead>
        <tbody>
    
    
	
	
		{foreach $purchases as $purchase}
			<tr>
					<td class="ordertable-product">
						<div class="ordertable-image">
                        	{if $purchase->image}
                        		<a {if $purchase->product}href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"{/if} class="producthref">
                                <img src="{$purchase->image->filename|resize:products:52:52}" alt="{$purchase->image->name|escape}"/>
                                </a>
                        	{/if}
                        </div>
                        
                        <div class="ordertable-product-name">
							<a {if $purchase->product}href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant_id}"{/if} class="producthref">{$purchase->product_name}</a>
							{if $purchase->variant_name}<span>({$purchase->variant_name})</span>{/if}
							{if $purchase->variant->stock == -1}
								<div class="cart-pod-zakaz">
									<span><i class="fa fa-truck"></i> Под заказ</span>
								</div>
							{/if}
							{if $settings->catalog_use_variable_amount && $purchase->var_amount > 1}
								<div class="cart-modifier" data-var-amount="{$purchase->var_amount}">
									<ul>
										<li>
											<div>{$settings->catalog_variable_amount_name} {$purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}</div>
										</li>
									</ul>
								</div>
							{/if}
							{if $purchase->modificators}
								<div class="cart-modifier">
									<ul>
										{foreach $purchase->modificators as $m}
											<li><div>{$m->modificator_name} {if $m->modificator_type == 'plus_fix_sum'}(+{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'minus_fix_sum'}(-{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'plus_percent'}(+{$m->modificator_value|convert}%){elseif $m->modificator_type == 'minus_percent'}(-{$m->modificator_value|convert}%){/if} {if $m->modificator_amount>1}{$m->modificator_amount} шт.{/if}</div></li>
										{/foreach}
									</ul>
								</div>
							{/if}
						</div>
                        
                        
						
                        {if $sku_value}<div class="artikul">Артикул: <span class="sku_value">{$sku_value}</span></div>{/if}
               			 
					</td>
                    
					<td class="ordertable-priceone">
                    {$purchase->price|convert} {$main_currency->sign}
                    </td>
                    
                    <td class="ordertable-count">
                    	{$purchase->amount} {$settings->units}
                    </td>
                
                	<td class="carttable-priceall">
				{($purchase->amount*$purchase->price_for_mul+$purchase->additional_sum)|convert} {$main_currency->sign}
                    </td>
                     </tr>              

		{/foreach}
	
    
      
      </table>
	
	{if $order->discount > 0}
	<div class="subtotal">Итого: <div class="sub-price">{($order->total_price_wo_discount - $order->delivery_price * (1 - $order->separate_delivery))|convert} {$main_currency->sign}</div></div>
	<div class="subtotal notopline">Ваша скидка{if $order->discount_type == 0} {$order->discount|string_format:"%d"}%{/if}: <div class="sub-price">{($order->total_price_wo_discount-$order->total_price)|convert} {$main_currency->sign}</div></div>
	{/if}
	{if !$order->separate_delivery && $delivery_method}<div class="order-subtotal notopline">{*if $delivery_method*}{$delivery_method->name}{*/if*}: <div class="order-sub-price">{if $order->delivery_price > 0}{$order->delivery_price|convert} {$main_currency->sign}{else}-{/if}</div></div>{/if}
    
	{if $modificators}
		<legend>Дополнения:</legend>
		<div class="cart-modifier">
			<ul>
				{foreach $modificators as $m}
					<li><div>{$m->modificator_name} {if $m->modificator_type == 'plus_fix_sum'}(+{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'minus_fix_sum'}(-{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'plus_percent'}(+{$m->modificator_value|convert}%){elseif $m->modificator_type == 'minus_percent'}(-{$m->modificator_value|convert}%){/if} {if $m->modificator_amount>1}{$m->modificator_amount} шт.{/if}</div></li>
				{/foreach}
			</ul>
		</div>
	{/if}
	
	{if $payment_method && $payment_method->operation_type != "nothing"}
    <div class="order-subtotal notopline">Оплата ({$payment_method->name} ({if $payment_method->operation_type=="plus_fix_sum"}+{rtrim(rtrim($payment_method->operation_value, '0'), '.')} {$main_currency->sign}{elseif $payment_method->operation_type=="minus_fix_sum"}-{rtrim(rtrim($payment_method->operation_value, '0'), '.')} {$main_currency->sign}{elseif $payment_method->operation_type=="plus_percent"}+{rtrim(rtrim($payment_method->operation_value, '0'), '.')}%{elseif $payment_method->operation_type=="minus_percent"}-{rtrim(rtrim($payment_method->operation_value, '0'), '.')}%{/if})):<div class="order-sub-price">{$order->payment_price|convert} {$main_currency->sign}</div></div>
	{/if}
    
	<div class="order-total">Итого к оплате{if $order->delivery_price > 0} <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="{if $order->discount > 0}{if $order->separate_delivery}Со скидкой и без учета доставки{else}С учетом скидки и доставки{/if}{else}{if $order->separate_delivery}Без учета доставки{else}С учетом доставки{/if}{/if}" class="fa fa-legend"><i class="fa fa-info-circle"></i></a>{/if}:<div class="order-sub-price">{$order->total_price|convert} {$main_currency->sign}</div></div>
	{if $order->separate_delivery}<div class="order-subtotal notopline">{if $delivery_method}{$delivery_method->name}{/if} (оплачивается отдельно): <div class="order-sub-price">{if $order->delivery_price > 0}{$order->delivery_price|convert} {$main_currency->sign}{else}-{/if}</div></div>{/if}
	
	<h2>Детали заказа</h2>
	<table class="order_info table table-striped table-bordered">
		<tr>
			<td class="firsttd">
				Дата заказа
			</td>
			<td>
				{$order->date|date} в
				{$order->date|time}
			</td>
		</tr>
		{if $order->name}
		<tr>
			<td>
				Имя
			</td>
			<td>
				{$order->name|escape}
			</td>
		</tr>
		{/if}
		{if $order->email}
		<tr>
			<td>
				E-mail
			</td>
			<td>
				{$order->email|escape}
			</td>
		</tr>
		{/if}
		{if $order->phone_code || $order->phone}
		<tr>
			<td>
				Телефон
			</td>
			<td>
				+7 ({$order->phone_code}) {$order->phone|phone_mask}
			</td>
		</tr>
		{/if}
		{if $order->phone2_code || $order->phone2}
		<tr>
			<td>
				Дополнительный телефон
			</td>
			<td>
				+7 ({$order->phone2_code}) {$order->phone2|phone_mask}
			</td>
		</tr>
		{/if}
		{if $order->address}
		<tr>
			<td>
				Адрес доставки
			</td>
			<td>
				{$order->address|escape}
			</td>
		</tr>
		{/if}
		{if $order->comment}
		<tr>
			<td>
				Комментарий
			</td>
			<td>
				{$order->comment|escape|nl2br}
			</td>
		</tr>
		{/if}
	</table>
    
	<div class="selected-payment">
		<div class="selected-payment-legend"><span>Способ оплаты — {if $payment_method}{$payment_method->name}{else}не выбран{/if}</span><button type="button" class="btn btn-primary btn-sm change-payment">Выбрать другой способ оплаты</button></div>
		<div class="selected-payment-method">
			<div class="selected-payment-description">
				{if $payment_method->image}
				<div class="selected-payment-image">
					<img src="{$payment_method->image->filename|resize:'payment':138:48}">
				</div>
				{/if}
				<div class="selected-payment-text">
					
					{if !$order->allow_payment}
						{banner id=22}
					{else}
						<p>{$payment_method->description}</p>
						{if $ext_module_form && !$order->paid}{$ext_module_form}{/if}
					{/if}
				</div>
			</div>
		</div>
			 
	</div>
	
	{if $order->paid}
		<div class="alert alert-success">		
            <span class="fz18">Ваш заказ оплачен</span><br/>{if $order_payment}
			Номер транзакции: {$order_payment->invoice_id}{/if}
        </div>
	{/if}

    <p>Постоянная ссылка на заказ: <a href="{$config->root_url}{$module->url}{$order->url}/">{$config->root_url}{$module->url}{$order->url}/</a></p>
{else}
	<h1>Такого заказа не существует</h1>
{/if}