<h1>Корзина</h1>
{if {banner id=23}}
    <div class="cart-offer">
        {banner id=23}
    </div>
{/if}

<form action="{$config->root_url}{$module->url}?action=save_order" method="POST" id="cart_form">
{if $cart->total_products > 0}
<div class="cart-block cart-block-first">
    <div class="row">
        <div class="col-xs-3">
            <div class="cart-header">Мой заказ:</div>
        </div>

        <div class="col-xs-9">

            

            <table class="table carttable">
                <thead>
                <tr>
                    <th class="carttable-product">Товар</th>
                    <th class="carttable-priceone">Цена</th>
                    <th class="carttable-count">Количество</th>
                    <th class="carttable-priceall">Стоимость</th>
                    <th class="remove"></th>
                </tr>
                </thead>
                <tbody>
                {foreach $cart->purchases as $purchase}
                    <tr>
                        <td class="carttable-product">
                            <div class="carttable-image">
                                {if $purchase->product->image}<a
                                    href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}"
                                    class="producthref"><img alt=""
                                                             src="{$purchase->product->image->filename|resize:'products':52:52}">
                                    </a>{/if}
                            </div>
                            <div class="carttable-product-name">
                                <a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}"
                                   class="producthref">{$purchase->product->name}</a>
                                {if $purchase->variant->name}<span>({$purchase->variant->name})</span>{/if}
                                {if $purchase->variant->stock == -1}
                                    <div class="cart-pod-zakaz">
                                        <span><i class="fa fa-truck"></i> Под заказ</span>
                                    </div>
                                {/if}
								
								{if $settings->catalog_use_variable_amount && $purchase->var_amount > 1}
								<div class="cart-modifier" data-var-amount="{$purchase->var_amount}">
									<ul>
										<li>
											<div>{$settings->catalog_variable_amount_name} {$purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}</div>
										</li>
									</ul>
								</div>
								{/if}
                                
								{if $purchase->modificators}
                                <div class="cart-modifier">
                                	<ul>
										{foreach $purchase->modificators as $m}
											<li><div>{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert}%){elseif $m->type == 'minus_percent'}(-{$m->value|convert}%){/if} {if $purchase->modificators_count[$m@index]>1}{$purchase->modificators_count[$m@index]} шт.{/if}</div></li>
										{/foreach}
                                    </ul>
                                </div>
                                {/if}
                                
                            </div>
                        </td>
                        <td class="carttable-priceone" data-price-for-one-pcs="{if $purchase->product->currency_id}{$purchase->variant->price_for_mul|convert:$purchase->product->currency_id}{else}{$purchase->variant->price_for_mul|convert:$admin_currency->id}{/if}" data-price-additional="{if $purchase->product->currency_id}{$purchase->variant->additional_sum|convert:$purchase->product->currency_id}{else}{$purchase->variant->additional_sum|convert:$admin_currency->id}{/if}">{if $purchase->product->currency_id}{$purchase->variant->price_for_one_pcs|convert:$purchase->product->currency_id}{else}{$purchase->variant->price_for_one_pcs|convert:$admin_currency->id}{/if} {$main_currency->sign}</td>
                        <td class="carttable-count">
                            <div class="input-group">
							<span class="input-group-btn">
								<button data-type="minus" class="btn btn-default btn-sm" type="button"><i
                                            class="fa fa-minus"></i></button>
							</span>
                                <input type="text" class="form-control input-sm" placeholder=""
                                       value="{$purchase->amount}" name="amounts[{$purchase@index}{*$purchase->variant->id*}]"
                                       data-id="{$purchase@index}{*$purchase->variant->id*}"
                                       data-stock="{if $purchase->variant->stock == -1}999{else}{$purchase->variant->stock}{/if}">
                            <span class="input-group-btn">
								<button data-type="plus" class="btn btn-default btn-sm" type="button"><i
                                            class="fa fa-plus"></i></button>
							</span>

                            </div>
                        </td>
                        <td class="carttable-priceall">{if $purchase->product->currency_id}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$purchase->product->currency_id}{else}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$admin_currency->id}{/if} {$main_currency->sign}</td>
                        <td class="carttable-remove"><a
                                    href="{$config->root_url}{$cart_module->url}{url add=['variant_id' => $purchase@index, 'action' => 'remove_variant']}"
                                    title="Удалить товар из корзины"
                                    class="incart"><i class="fa fa-times"></i></a></td>
                    </tr>
                {/foreach}

            </table>

            <div class="subtotal">Итого:
                <div class="sub-price"><span
                            id="cart_total_price">{$cart->total_price|convert}</span> {$main_currency->sign}</div>
            </div>

            <div class="subtotal notopline" {if !$user || !$user->discount}style="display:none;"{/if}>Ваша скидка <span
                        id="user_discount" data-value="{if $user}{$user->discount}{else}0{/if}">{$user->discount}</span><span>%</span>:
                <div class="sub-price"><span
                            id="cart_total_discount">{($cart->total_price*$user->discount/100)|convert}</span> {$main_currency->sign}
                </div>
            </div>

            <div class="subtotal" {if !$user || !$user->discount}style="display:none;"{/if}>Итого с учетом скидки:
                <div class="sub-price"><span
                            id="cart_total_price_w_discount">{($cart->total_price - $cart->total_price*$user->discount/100)|convert}</span> {$main_currency->sign}
                </div>
            </div>
        </div>
    </div>
</div>

	

{if $modificators_groups}
	
	<div class="cart-block">
    <div class="row">
        <div class="col-xs-3">
			<div class="cart-header">Дополнительно:</div>
		</div>
		
		<div class="col-xs-9">
			
	<div class="product-modifier">
		{foreach $modificators_groups as $gr}
			{if $gr->name}
				<div class="product-modifier-group">
				<label class="modifier-group-header">{$gr->name}</label>
			{/if}
			
			{if $gr->type == 'checkbox'}
			<div class="modifier-checkbox">
				{foreach $gr->modificators as $m}
					<div class="checkbox">
						<label>
						<input type="checkbox" value="{$m->id}" name="modificators[{$gr->id}]" data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">
						<div class="mz-right-all">	
							<div class="mz-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators-orders':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators-orders':40:40}" /></a>{/if}</div>
							<div class="mz-right">
								<div class="mz-right-header">{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %)
							{/if} </div>
							
							
							
						{if $m->description}
                        <div class="mz-description">
                        {$m->description}
						</div>
                        {/if}
							
								
								
							</div>
						</label>
						
						{if $m->multi_buy}
						<div class="modifier-counter">
						<div class="input-group">
							<span class="input-group-btn">
								<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
							</span>
							<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
							<span class="input-group-btn">
								<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
							</span>
						</div>
						</div>
						
						{/if}
					</div>
				</div>
				{/foreach}
				</div>
			{/if}
			
			{if $gr->type == 'radio'}
				{foreach $gr->modificators as $m}
					<div class="radio">
						<label>
						<input type="radio" name="modificators[{$gr->id}]" value="{$m->id}" {if $m@first}checked{/if} data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">
							<div class="mz-right-all">	
							
							<div class="mz-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators-orders':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators-orders':40:40}" /></a>{/if}</div>
							
								
							<div class="mz-right">
								<div class="mz-right-header">
								{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
								</div>	
								
							
							{if $m->description}
								<div class="mz-description">
									{$m->description}	
								</div>
							{/if}
							
							
						</label>
						
								{if $m->multi_buy}
						<div class="input-group">
							<span class="input-group-btn">
								<button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
							</span>
							<input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
							<span class="input-group-btn">
								<button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
							</span>
						</div>
						{/if}
					
					</div>
						</div>
						
					</div>
				{/foreach}
			{/if}
			
			{if $gr->type == 'select'}
				<div class="modifier-select">
				<select name="modificators[{$gr->id}]" class="form-control">
					<option>Не выбрано</option>
					{foreach $gr->modificators as $m}
						<option value="{$m->id}" data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">
						{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
						</option>
					{/foreach}
				</select>
				</div>
			{/if}
			
			{if $gr->name}
				</div>
			{/if}
		{/foreach}
	</div>
		</div>
		</div>
		
	<div class="subtotal">Итого:
		<div class="sub-price"><span id="cart_total_price_w_modificators">{$cart->total_price|convert}</span> {$main_currency->sign}</div>
    </div>
		
		</div>
		
{/if}

<div id="not-enough-block" class="warning-blue" {if $cart->total_price >= $settings->cart_order_min_price}style="display: none;"{/if}> Минимальная сумма заказа <strong>{$settings->cart_order_min_price|convert} {$main_currency->sign}</strong></div>

<div id="cart-block" {if $cart->total_price < $settings->cart_order_min_price}style="display: none;"{/if}>
<div class="cart-block">
    <div class="row">
        <div class="col-xs-3">
            <div class="cart-header">Выберите способ получения:</div>
        </div>

        <div class="col-xs-9">

            <div id="delivery-section">
                <div class="delivery-select-header"></div>
                <div class="delivery-select">
                    {foreach $deliveries as $delivery}
                        <div class="radio {if $delivery@first}active{/if}">
                            <label>
                                <input type="radio" name="delivery_id" value="{$delivery->id}"
                                       data-price="{if $delivery->delivery_type == 'paid'}{$delivery->price}{else}0{/if}"
                                       data-free="{if $delivery->delivery_type == 'paid'}{if $delivery->free_from == ''}9999999999{else}{$delivery->free_from}{/if}{else}0{/if}"
                                       data-separate="{if $delivery->delivery_type == 'separately'}1{else}0{/if}"
                                       data-pickup="{$delivery->is_pickup}"
                                       data-value="{if $delivery->delivery_type == 'free' || $delivery->delivery_type == 'separately'}0{elseif ($delivery->price > 0 && $delivery->free_from == 0) || ($delivery->free_from > 0 && $delivery->free_from > $cart->total_price)}{$delivery->price}{else}0{/if}"
                                       {if $delivery@first}checked{/if}/>

                                <div class="delivery-header">{$delivery->name} <span
                                            class="delivery-price">({if $delivery->delivery_type == "separately"}Оплачивается отдельно{elseif ($delivery->price > 0 && $delivery->free_from == 0) || ($delivery->free_from > 0 && $delivery->free_from > $cart->total_price)}{$delivery->price|convert} {$main_currency->sign}{else}Бесплатно{/if}
                                        )</span></div>
                                <div class="delivery-text">{$delivery->description}</div>
                            </label>
                        </div>
                        <div class="radio-separator"></div>
                    {/foreach}
                </div>
            </div>
        </div>
    </div>
</div>

<div class="cart-block">
    <div class="row">
        <div class="col-xs-3">
            <div class="cart-header">Выберите способ оплаты:</div>
        </div>

        <div class="col-xs-9">

            <div id="payment-section">
                <div class="payment-select">
                    {if $payment_methods}
                        {foreach $payment_methods as $payment}
                            <div class="payment-method {if $payment@first}active{/if}">
                                <div class="radio" data-id="{$payment->id}" data-type="{$payment->operation_type}" data-value="{$payment->operation_value}">
                                    <label>
                                        <input type="radio" name="payment_method_id" value="{$payment->id}"
                                               {if $payment@first}checked{/if}/>

                                        <div class="payment-image">{if $payment->image}<img
                                                src="{$payment->image->filename|resize:'payment':138:48}" />{/if}</div>
                                        <div class="payment-header">{$payment->name} {if $payment->operation_type == 'plus_fix_sum'}(+{rtrim(rtrim($payment->operation_value, '0'), '.')} {$main_currency->sign}){elseif $payment->operation_type == 'minus_fix_sum'}(-{rtrim(rtrim($payment->operation_value, '0'), '.')} {$main_currency->sign}){elseif $payment->operation_type == 'plus_percent'}(+{rtrim(rtrim($payment->operation_value, '0'), '.')}%){elseif $payment->operation_type == 'minus_percent'}(-{rtrim(rtrim($payment->operation_value, '0'), '.')}%){/if}</div>
                                        {*
                                        <div class="delivery-text">{$payment->description}</div>
                                        *}
                                    </label>
                                </div>
                            </div>
                        {/foreach}
                    {/if}
                    {* Здесь на AJAX подгрузятся возможные варианты оплаты *}
                </div>
            </div>

            {*<div class="delivery-subtotal">Итого c доставкой:
                <div class="sub-price"><span
                            id="total_price">{($cart->total_price - $cart->total_price*$user->discount/100)|convert}</span> {$main_currency->sign}
                </div>
            </div>*}

			<div class="delivery-subtotal">Итого к оплате:
                <div class="sub-price"><span
                            id="total_price_with_payment"></span> {$main_currency->sign}
                </div>
            </div>
        </div>
    </div>
</div>


<div class="cart-block">
    <div class="row">
        <div class="col-xs-3">
            <div class="cart-header">Информация о получателе:</div>
        </div>

        <div class="col-xs-9">
            <div class="userdata">
                <div class="form-group userdata-nomargin">
                    <label>ФИО получателя {if $settings->cart_fio_required}<span>*</span>{/if}</label>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="cart-ok"><i class="fa fa-check" style="display:none;"></i></div>
                            <input type="text" class="form-control {if $settings->cart_fio_required}required{/if}"
                                   name="name" value="" data-type="text" tabindex="1"
                                   data-value-default="{$user->name}"/>
                            {if $settings->cart_fio_required}
                                <div class="help-block-error" style="display:none;">Это поле обязательно для
                                    заполнения
                                </div>
                            {/if}
                        </div>
                        {if !$user}
                            <div class="col-xs-6 hint">
                                <p class="help-block">Уже покупали у нас? Авторизуйтесь и мы поможем вам быстрее
                                    оформить заказ.<br/><i class="fa fa-sign-in"></i> <a href="login/"
                                                                                         class="userdata-link enter-link">Войти
                                        на сайт</a></p>
                            </div>
                        {else}
                            <div class="col-xs-6 hint">
                                <p class="help-block">С возвращением, {$user->name}! Мы заполнили за Вас часть
                                    информации, которую Вы можете изменить по желанию</p>
                            </div>
                        {/if}
                    </div>
                </div>


                <div class="form-group">
                    <label>Мобильный телефон для связи {if $settings->cart_phone_required}<span>*</span>{/if}</label>


                    <div class="row">
                        <div class="col-xs-6">
                            <div class="cart-ok"><i class="fa fa-check" style="display:none;"></i></div>
                            <input type="tel" id="phone"
                                   class="form-control {if $settings->cart_phone_required}required{/if}" name="phone"
                                   value="" data-type="phone" tabindex="2"
                                   data-value-default="{if $user->phone_code && $user->phone}+7 ({$user->phone_code}) {$user->phone|phone_mask}{/if}"/>

                            <div class="help-block-error" id="cart-phone-required" style="display:none;">Пожалуйста
                                укажите номер телефона
                            </div>
                            <div class="add-phone">
                                <a href="#" id="add-additional-phone">Добавить еще один номер телефона</a>
                            </div>
                        </div>
                        <div class="col-xs-6 hint">
                            <p class="help-block">На этот номер вы будете получать уведомления об изменениях статуса
                                заказа</p>

                            <a href="#" id="notify-order-on" class="userdata-link on">Вкл.</a> / <a href="#"
                                                                                                    id="notify-order-off"
                                                                                                    class="userdata-link">Выкл.</a>

                            {* Если нажали Выкл. то показываем в подсказке текст "Вы не будете получать уведомления об изменениях статуса заказа"*}

                        </div>
                    </div>
                </div>

                <div class="form-group" id="additional-phone" style="display:none;">
                    <label>Дополнительный телефон</label>

                    <div class="row">
                        <div class="col-xs-6">
                            <input type="tel" id="phone2" class="form-control" name="phone2" value="" data-type="phone"
                                   data-value-default="{if $user->phone2_code && $user->phone2}+7 ({$user->phone2_code}) {$user->phone2|phone_mask}{/if}"/>
                        </div>
                    </div>
                </div>

                <div class="form-group userdata-nomargin">
                    <label>Электронный адрес (e-mail) {if $settings->cart_email_required}<span>*</span>{/if}</label>

                    {* Обязательная проверка на правильность ввода адреса *}
                    <div class="row">
                        <div class="col-xs-6">
                            <div class="cart-ok"><i class="fa fa-check" style="display:none;"></i></div>
                            <input type="email" class="form-control {if $settings->cart_email_required}required{/if}"
                                   name="email" data-type="email" tabindex="3" data-value-default="{$user->email}">
                            {if $settings->cart_email_required}
                                <div class="help-block-error" style="display:none;">Укажите e-mail в формате:
                                    ivanov@gmail.com
                                </div>
                            {/if}
                        </div>
                        <div class="col-xs-6 hint">
                            <p class="help-block">На этот адрес вы будете получать уведомления о статусе заказа и
                                рассылки с акциями</p>

                            <a href="#" id="notify-news-on" class="userdata-link on">Вкл.</a> / <a href="#"
                                                                                                   id="notify-news-off"
                                                                                                   class="userdata-link">Выкл.</a>

                            {* Если нажали Выкл. то показываем в подсказке текст "Вы не будете получать уведомления о статусе заказа и рассылки с акциями" *}

                        </div>
                    </div>
                </div>

                <div class="form-group" {if $deliveries.0->is_pickup}style="display:none"{/if}>
                    <label>Адрес получателя {if $settings->cart_address_required}<span>*</span>{/if}</label>

                    <div class="row">
                        <div class="col-xs-6">
                            <div class="cart-ok"><i class="fa fa-check" style="display:none;"></i></div>
                            <textarea class="form-control {if $settings->cart_address_required}required{/if}" rows="1"
                                      name="address" data-type="text" tabindex="5"
                                      data-value-default="{$user->delivery_address}"></textarea>
                            {if $settings->cart_address_required}
                                <div class="help-block-error" style="display:none;">Укажите адрес получателя</div>
                            {/if}
                        </div>
                        <div class="col-xs-6 hint">
                            <p class="help-block">Например: Москва, ул. Тверская дом 1, корпус 1, кв. 51</p>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Комментарии и пожелания</label>

                    <div class="row">
                        <div class="col-xs-6">
                            <textarea class="form-control" rows="2" name="comment" data-type="text"
                                      tabindex="6"></textarea>
                        </div>
                        <div class="col-xs-6 hint">
                            <p class="help-block">Например: этаж, подъезд, код домофона, удобное время доставки<br/>
                                {* Если нажали Выкл. то показываем в подсказке текст "Вы не будете получать уведомления об изменениях статуса заказа" *}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="cart-block cart-block-last">
    <div class="row">
        <div class="col-xs-3 hint">
            <p class="help-block">Нажмите кнопку "Оформить заказ" и менеджер контакт-центра свяжется с Вами и все уточнит.</p>
        </div>
        <div class="col-xs-9">
            <button type="button" class="btn btn-success btn-lg w100" id="save_order" tabindex="7">Оформить заказ <i class="fa fa-spin fa-spinner hidden" id="save_order_spinner"></i></button>
        </div>

    </div>
</div>
</div>

	{else}
		Корзина пуста
	{/if}

	
	<input type="hidden" name="notify_order" id="notify-order" value="1"/>
	<input type="hidden" name="notify_news" id="notify-news" value="1"/>
	<input type="hidden" name="order_modificators" value=""/>
	<input type="hidden" name="order_modificators_count" value=""/>
</form>
	
<script type="text/javascript">
	$(document).ready(function(){
		{if !$payment_methods}$('#delivery-section .delivery-select input:checked').trigger('click');{/if}
		
		{*var formsave1=new autosaveform({
			formid: 'cart_form',
			includefields: ['text', 'textarea', 'email', 'radio'],
			pause: 1000 //<--no comma following last option!
		});
		$('#cart_form').sayt();*}
		
		$('form#cart_form input[data-value-default], form#cart_form textarea[data-value-default]').each(function(){
			if ($(this).val().length == 0)
				$(this).val($(this).attr('data-value-default'));
		});
		
		$("#phone").mask("+7 (999) 999-99-99", { autoclear: false });
		$("#phone2").mask("+7 (999) 999-99-99", { autoclear: false });
		
		check_user_info_data(true, false);
		update_price();
		
		$('textarea[name=comment],textarea[name=address]').autosize();
		
		$(".fancybox").fancybox({
			autoResize: true,
			autoCenter: true,
       		prevEffect	: 'none',
			nextEffect	: 'none',
			helpers	: {
				title	: {
					type: 'outside'
				},
				thumbs	: {
					width	: 50,
					height	: 50
				},
				overlay : {
       			   locked     : false
		        }
			}
		});
	});
	
	$("#phone_code,#phone2_code,#phone,#phone2").keydown(function(event) {
	        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
	            (event.keyCode == 65 && event.ctrlKey === true) ||
	            (event.keyCode >= 35 && event.keyCode <= 39)) {
        	         return;
	        }
        	else {
	            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                	event.preventDefault();
        	    }  
	        }
    	});
	$("#phone_code").keyup(function(){
		if ($(this).val().length == 3)
			$("#phone").focus();
	});
	$("#phone2_code").keyup(function(){
		if ($(this).val().length == 3)
			$("#phone2").focus();
	});
	
	$('#notify-order-on').click(function(){
		$('#notify-order-on').addClass('on');
		$('#notify-order-off').removeClass('off');
		$('#notify-order').val(1);
		return false;
	});
	
	$('#notify-order-off').click(function(){
		$('#notify-order-on').removeClass('on');
		$('#notify-order-off').addClass('off');
		$('#notify-order').val(0);
		return false;
	});
	
	$('#notify-news-on').click(function(){
		$('#notify-news-on').addClass('on');
		$('#notify-news-off').removeClass('off');
		$('#notify-news').val(1);
		return false;
	});
	
	$('#notify-news-off').click(function(){
		$('#notify-news-on').removeClass('on');
		$('#notify-news-off').addClass('off');
		$('#notify-news').val(0);
		return false;
	});

	$('#content a.incart').click(function(){
		var href = $(this).attr('href');
		var tr = $(this).closest('tr');
		var a_href = tr.find('td.carttable-product div.carttable-product-name a');
		$.get(href, function(data){
			if (data.success)
			{
				if ($('table.carttable tr td.carttable-product').length == 1)
				{
					//$('table.carttable').closest('div').html('Корзина пуста');
					$('table.carttable').closest('form').html('Корзина пуста');
				}
				else
				{
					tr.html('<td class="carttable-info" colspan="5">Товар '+a_href[0].outerHTML+' был удален из корзины.</td>');
					update_price();
				}
				update_cart_info();
			}
		});
		return false;
	});
	
	$('#delivery-section .delivery-select input').click(function(){
		$('#delivery-section .radio.active').removeClass('active');
		$(this).closest('.radio').addClass('active');
		
		if ($(this).data('pickup'))
			$('.userdata textarea[name=address]')./*val('').*/closest('.form-group').hide();
		else
			$('.userdata textarea[name=address]')./*val('').*/closest('.form-group').show();
	
		var delivery_id = $('#delivery-section .delivery-select input:checked').val();
		$.get('{$config->root_url}{$module->url}?action=get_payment_methods&delivery_id='+delivery_id, function(data){
			var new_items = $('<div class="payment_select"></div>');
			for(var index in data.data)
			{
				var d = data.data[index];
				if (d.operation_type == 'plus_fix_sum')
					d.name += ' (+' + d.operation_value.replace(/0+$/,'').replace(/\.+$/,'') + ' {$main_currency->sign})';
				if (d.operation_type == 'minus_fix_sum')
					d.name += ' (-' + d.operation_value.replace(/0+$/,'').replace(/\.+$/,'') + ' {$main_currency->sign})';
				if (d.operation_type == 'plus_percent')
					d.name += ' (+' + d.operation_value.replace(/0+$/,'').replace(/\.+$/,'') + '%)';
				if (d.operation_type == 'minus_percent')
					d.name += ' (-' + d.operation_value.replace(/0+$/,'').replace(/\.+$/,'') + '%)';
				var obj = $('<div class="payment-method '+(index==0?"active":"")+'" data-id="'+d.id+'"><div class="radio" data-type="'+d.operation_type+'" data-value="'+d.operation_value+'"><label><input type="radio" name="payment_method_id" '+(index==0?"checked":"")+' value="'+d.id+'"/><div class="payment-image">'+(d.image.length>0?'<img src="'+d.image+'" />':'')+'</div><div class="payment-header">'+d.name+'</div></label></div></div>');
				new_items.append(obj);
			}
		
			$('#payment-section .payment-method.active').removeClass('active');
		
			$('#payment-section .payment-select').quicksand( new_items.find('div.payment-method'), { adjustHeight: 'dynamic', duration: 500 }, function(){
				$('#payment-section .payment-select input:first').prop('checked', true);
				$('#payment-section .payment-method:first').addClass('active');
				/*update_price();*/
			});
			
			update_price();
		});
	});
	
	$('#payment-section').on('click', '.payment-method input', function(){
		$('#payment-section .payment-method.active').removeClass('active');
		$(this).closest('.payment-method').addClass('active');
		
		update_price();
	});
	
	$('table.carttable td.carttable-count input').bind('change keyup input click', function(){
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		if (this.value <= 0)
			this.value = 1;
		set_amount($(this).attr('data-id'), this.value);
		update_price();
	});
	
	$('table.carttable td.carttable-count button[data-type=plus]').click(function(){
		var input = $(this).closest('td').find('input');
		var max_stock = input.data('stock');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) + 1;
			if (value > max_stock)
				value = max_stock;
			input.val(value);
			set_amount(input.attr('data-id'), value);
			update_price();
		}
	});
	
	$('table.carttable td.carttable-count button[data-type=minus]').click(function(){
		var input = $(this).closest('td').find('input');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) - 1;
			if (value <=0 )
				value = 1;
			input.val(value);
			set_amount(input.attr('data-id'), value);
			update_price();
		}
	});
	
	function check_user_info_data(show_checks, show_errors){
		var result = true;
		if (show_checks == undefined)
			show_checks = false;
		if (show_errors == undefined)
			show_errors = false;
	
		$('.userdata input.required:visible, .userdata textarea.required:visible').each(function(){
			var type = $(this).attr('data-type');
			if (type.length == 0)
				type = "text";
			var show_error = false;
			switch(type){
				case "text":
					$(this).closest('div.row').find('input.required, textarea.required').each(function(){
						if ($(this).val().length == 0)
						{
							show_error = true;
							result = false;
						}
					});
					if (show_error && show_errors)
					{
						$(this).closest('div.row').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
					}
					else
						if (!show_error && show_checks)
						{
							$(this).closest('div.row').find('.help-block-error').hide();
							$(this).closest('div.form-group').find('.fa-check').show();
						}
					break;
				case "phone":
					$(this).closest('div.row').find('input.required, textarea.required').each(function(){
						if ($(this).mask().length == 0)
						{
							show_error = true;
							result = false;
						}
					});
					if (show_error && show_errors)
					{
						$('#cart-phone-required').show();
						$(this).closest('div.row').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
					}
					else
						if (!show_error && show_checks)
						{
							$(this).closest('div.row').find('.help-block-error').hide();
							$(this).closest('div.form-group').find('.fa-check').show();
						}
					break;
				case "number":
					$(this).closest('div.row').find('input.required, textarea.required').each(function(){
						if ($(this).val().length == 0 || ($(this).attr('maxlength')>0 && $(this).attr('maxlength')>$(this).val().length))
						{
							show_error = true;
							result = false;
						}
					});
					
					if (show_error && show_errors)
					{
						$(this).closest('div.row').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
					}
					else
						if (!show_error && show_checks)
						{
							$(this).closest('div.row').find('.help-block-error').hide();
							$(this).closest('div.form-group').find('.fa-check').show();
						}
					break;
				case "email":
					$(this).closest('div.row').find('input.required, textarea.required').each(function(){
						if (!isValidEmailAddress($(this).val()))
						{
							show_error = true;
							result = false;
						}
					});
					if (show_error && show_errors)
					{
						$(this).closest('div.row').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
					}
					else
						if (!show_error && show_checks)
						{
							$(this).closest('div.row').find('.help-block-error').hide();
							$(this).closest('div.form-group').find('.fa-check').show();
						}
					break;
			}
		});
		
		return result;
	}
	
	$('#save_order').click(function(){
		var result = check_user_info_data(true, true);
	
		if (result){
			$('#save_order').addClass('disabled');
			$('#save_order_spinner').removeClass('hidden');
			
			var modificators = [];
			var modificators_count = [];
			$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
				if (parseInt($(this).val()) > 0)
				{
					modificators.push($(this).val());
					var ic = $(this).closest('div').find('input[name^=modificators_count]');
					if (ic.length > 0)
						modificators_count.push(ic.val());
					else
						modificators_count.push(1);
				}
			});
			
			$('input[name=order_modificators]').val(modificators.join(','));
			$('input[name=order_modificators_count]').val(modificators_count.join(','));
			
			$(this).closest('form').submit();
		}
		else
			return false;
	});
	
	$('.userdata input.required:visible, .userdata textarea.required:visible').focusout(function(){
		var type = $(this).attr('data-type');
		if (type.length == 0)
			type = "text";
		var show_error = false;
		switch(type){
			case "text":
				$(this).closest('div.row').find('input.required, textarea.required').each(function(){
					if ($(this).val().length == 0)
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.row').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.row').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "phone":
				$(this).closest('div.row').find('input.required, textarea.required').each(function(){
					if ($(this).mask().length == 0)
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.row').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.row').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "number":
				$(this).closest('div.row').find('input.required, textarea.required').each(function(){
					if ($(this).val().length == 0 || ($(this).attr('maxlength')>0 && $(this).attr('maxlength')>$(this).val().length))
						show_error = true;
				});
				
				if (show_error)
				{
					$(this).closest('div.row').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.row').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "email":
				$(this).closest('div.row').find('input.required, textarea.required').each(function(){
					if (!isValidEmailAddress($(this).val()))
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.row').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.row').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
		}
	});
	
	$('#add-additional-phone').click(function(){
		$('#additional-phone').slideToggle(400);
		return false;
	});
	
	var amount_ajax_context;
	function set_amount(variant_id, amount){
		{*$('table.carttable td.carttable-count input').addClass('disabled');
		$('table.carttable td.carttable-count button[data-type=plus]').addClass('disabled');
		$('table.carttable td.carttable-count button[data-type=minus]').addClass('disabled');*}
		
		if (amount_ajax_context != null)
			amount_ajax_context.abort();
		
		amount_ajax_context = $.get('{$config->root_url}{$module->url}?action=set_amount&variant_id='+variant_id+'&amount='+amount, function(data){
			if (data.success)
			{
				$('#cart-placeholder').html(data.data);
				{*$('table.carttable td.carttable-count input').removeClass('disabled');
				$('table.carttable td.carttable-count button[data-type=plus]').removeClass('disabled');
				$('table.carttable td.carttable-count button[data-type=minus]').removeClass('disabled');*}
			}
			amount_ajax_context = null;
		});
	}
	
	function update_price(){
		var input = $('.delivery-select input:checked');
		var total_cart_price = 0;
		var discount = parseFloat($('#user_discount').attr('data-value'));
		
		$('table.carttable tbody tr').each(function(){
			var td_count = $(this).find('td.carttable-count input');
			var td_price = $(this).find('td.carttable-priceone').attr('data-price-for-one-pcs');
			var td_price_additional = $(this).find('td.carttable-priceone').attr('data-price-additional');
			
			if (td_count.length > 0 && td_price.length > 0)
			{
				var input_count = parseFloat(td_count.val().length > 0 ? td_count.val() : 1);
				var input_price = parseFloat(td_price.replace(/ /g,''));
				var input_price_additional = parseFloat(td_price_additional.replace(/ /g,''));
				var priceall = input_count * input_price + input_price_additional;
				priceall = Math.round(priceall);
				var priceall_str = priceall.toString();
				priceall_str = priceall_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				var td_priceall = $(this).find('td.carttable-priceall');
				td_priceall.html(priceall_str + ' {$main_currency->sign}');
				total_cart_price += priceall;
			}
		});
		
		var total_cart_price_w_discount = total_cart_price - total_cart_price * discount / 100;
		
		total_cart_price = Math.round(total_cart_price);
		var cart_total_price_str = total_cart_price.toString();
		cart_total_price_str = cart_total_price_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_price').html(cart_total_price_str);
		
		total_cart_price_w_discount = Math.round(total_cart_price_w_discount);
		var cart_total_price_w_discount_str = total_cart_price_w_discount.toString();
		cart_total_price_w_discount_str = cart_total_price_w_discount_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_price_w_discount').html(cart_total_price_w_discount_str);
		
		var cart_total_discount = Math.round(total_cart_price - total_cart_price_w_discount);
		var cart_total_discount_str = cart_total_discount.toString();
		cart_total_discount_str = cart_total_discount_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_discount').html(cart_total_discount_str);
		
		if (total_cart_price >= {$settings->cart_order_min_price}){
			$('#not-enough-block').hide();
			$('#cart-block').show();
		}
		else{
			$('#not-enough-block').show();
			$('#cart-block').hide();
		}
		
		var total_price_w_modificators = total_cart_price_w_discount;
		
		$('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
			var obj = $(this);
			var obj_type = obj.data('type');
			var obj_value = parseFloat(obj.data('value'));
			var obj_multi_buy = obj.data('multi-buy');
			var obj_multi_buy_value = 1;
			if (obj_multi_buy == 1)
				obj_multi_buy_value = parseInt($('input[name=modificators_count_'+obj.val()+']').val());
			
			if (obj_type == 'plus_fix_sum')
				total_price_w_modificators += obj_value * obj_multi_buy_value;
			if (obj_type == 'minus_fix_sum')
				total_price_w_modificators -= obj_value * obj_multi_buy_value;
			if (obj_type == 'plus_percent')
				total_price_w_modificators += total_cart_price_w_discount * obj_value * obj_multi_buy_value / 100;
			if (obj_type == 'minus_percent')
				total_price_w_modificators -= total_cart_price_w_discount * obj_value * obj_multi_buy_value / 100;
		});
		
		total_price_w_modificators = Math.round(total_price_w_modificators);
		var cart_total_price_w_modificators_str = total_price_w_modificators.toString();
		cart_total_price_w_modificators_str = cart_total_price_w_modificators_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_price_w_modificators').html(cart_total_price_w_modificators_str);
		
		$('#delivery-section input').each(function(){
			var data_free = Math.round(parseFloat($(this).attr('data-free')));
			var data_price = Math.round(parseFloat($(this).attr('data-price')));
			var data_separate = parseInt($(this).attr('data-separate'));
			if (data_separate == 1)
			{
				$(this).closest('div.radio').find('span.delivery-price').html('(Оплачивается отдельно)');
				$(this).attr('data-value', 0);
			}
			else
				if ((data_free == 0 && data_price > 0) || (total_price_w_modificators < data_free && data_free > 0))
				{
					var data_price_str = data_price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
					$(this).closest('div.radio').find('span.delivery-price').html('(' + data_price_str + ' {$main_currency->sign})');
					$(this).attr('data-value', data_price);
				}
				else
				{
					$(this).closest('div.radio').find('span.delivery-price').html('(Бесплатно)');
					$(this).attr('data-value', 0);
				}
		});
		
		var delivery_price = parseFloat(input.attr('data-value'));
		var total_price = total_price_w_modificators + delivery_price;
		total_price = Math.round(total_price);
		
		var total_price_with_payment = total_price;
		var payment_type = $('#payment-section .payment-method.active div.radio').attr('data-type');
		var payment_val = parseFloat($('#payment-section .payment-method.active div.radio').attr('data-value'));
		if (payment_type == "plus_fix_sum"){
			total_price_with_payment += payment_val;
		}
		if (payment_type == "minus_fix_sum"){
			total_price_with_payment -= payment_val;
		}
		if (payment_type == "plus_percent"){
			total_price_with_payment *= 100/(100-payment_val);
		}
		if (payment_type == "minus_percent"){
			total_price_with_payment *= 100/(100+payment_val);
		}
		total_price_with_payment = Math.round(total_price_with_payment);
		
		var total_price_with_payment_str = total_price_with_payment.toString();
		total_price_with_payment_str = total_price_with_payment_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#total_price_with_payment').html(total_price_with_payment_str);
	}
	
	$('.product-modifier input, .product-modifier select').change(function(){
		var checked = $(this).prop('checked');
		var d = $(this).closest('div');
		if (checked){
			d.find('input[name^=modificators_count]').prop('disabled', false);
			d.find('button[data-type=minus]').prop('disabled', false);
			d.find('button[data-type=plus]').prop('disabled', false);
		}
		else{
			d.find('input[name^=modificators_count]').prop('disabled', true);
			d.find('button[data-type=minus]').prop('disabled', true);
			d.find('button[data-type=plus]').prop('disabled', true);
		}
		
		update_price();
	});
	
	$('.product-modifier button[data-type=minus]').click(function(){
		var i = $(this).closest('.input-group').find('input');
		if (i.val() > i.data('min'))
			i.val(parseInt(i.val()) - 1);
		update_price();
	});
	
	$('.product-modifier button[data-type=plus]').click(function(){
		var i = $(this).closest('.input-group').find('input');
		if (i.val() < i.data('max'))
			i.val(parseInt(i.val()) + 1);
		update_price();
	});
	
	$('.product-modifier input[name^=modificators_count]').bind('change keyup input click', function(){
		if (this.value.match(/[^0-9]/g)) {
			this.value = this.value.replace(/[^0-9]/g, '');
		}
		var min = parseInt($(this).data('min'));
		var max = parseInt($(this).data('max'));
		if (this.value < min)
			this.value = min;
		if (this.value > max)
			this.value = max;
		update_price();
	});

	{literal}
	function isValidEmailAddress(emailAddress) {
		var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
		return pattern.test(emailAddress);
    }
	{/literal}
</script>