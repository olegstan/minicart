
<div class="question-header">
	<div class="rate-sort">Всего 67 вопросов и ответов {Соотвественно 61 вопрос и ответ} </div>    
	<a class="btn btn-primary write-question"><i class="fa fa-pencil"></i> Задать вопрос</a>
</div>

<div class="faq-list">
<div class="question">
	<div class="question-header">
    	<div class="question-username">Николай</div>
        <div class="question-date">02 июня, 12:31</div>
        <div class="question-complain">Пожаловаться</div>
    </div>
    
    <div class="question-text">
    	Еще никто не брал? А то есть вопрос, можно ли смотреть online сериалы и фильмы на WebOs, нет ли проблем с кэшем как на моделях LA***. А то думаю что брать серию LB*** 2014 года или взять LA6** 2013 года.
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    </div>    
</div>


<div class="answer ">
	<div class="question-header">
    	<div class="question-username">Анатолий</div>
        <div class="question-date">21 мая, 20:21</div>
        <a class="question-complain" href="#">Пожаловаться</a>
    </div>
    
    <div class="question-text">
    	Если смотреть через приложения смарта, то особых проблем не наблюдал. Если через встроенный браузер, то на всех тв проблема с кэшем.
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    	<div class="answer-help-me"><a href="#">Ответ помог мне</a></div>
        <div class="help-counter">Ответ помог 7 пользователям</div>        
    </div>    
</div>

<div class="q-separator"></div>

<div class="question ">
	<div class="question-header">
    	<div class="question-username">Ivan</div>
        <div class="question-date">2 августа, 15:05</div>
        <a class="question-complain" href="#">Пожаловаться</a>
    </div>
    
    <div class="question-text">
    	Пожалуйста ответьте в чём заключается разница между моделями SAMSUNG UE48H5000AK и SAMSUNG UE48H5270AU? А то стоят примерно одинаково и все характеристики одинаковы, в чём же тогда разница?
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    	<div class="answer-help-me"><a href="#">Ответ помог мне</a></div>
        <div class="help-counter">Ответ помог 10 пользователям</div>        
    </div>    
</div>

<div class="q-separator"></div>

<div class="question">
	<div class="question-header">
    	<div class="question-username">Иван</div>
        <div class="question-date">21 апреля, 11:18</div>
        <a class="question-complain" href="#">Пожаловаться</a>
    </div>
    
    <div class="question-text">
    	В комплекте есть 3д очки и какие активные или пассивные?
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    	<div class="answer-help-me"><a href="#">Ответ помог мне</a></div>
        <div class="help-counter">Ответ помог 10 пользователям</div>        
    </div>    
</div>

<div class="answer answer-solved">
	<div class="question-header">
    	<div class="question-username this-answer"><i class="fa fa-check-circle"></i> Ответ <span>NexTron</span> решил вопрос</div>
        <div class="question-date">21 апреля, 11:34</div>
        <a class="question-complain" href="#">Пожаловаться</a>
    </div>
    
    <div class="question-text">
    	В комплекте 2 пары (двое) активных затворных очков SSG-5100GB.
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    	<div class="answer-help-me"><a href="#">Ответ помог мне</a></div>
        <div class="help-counter">Ответ помог 7 пользователям</div>        
    </div>    
</div>

<div class="answer">
	<div class="question-header">
    	<div class="question-username">Владимир</div>
        <div class="question-date">05 мая, 20:57</div>
        <div class="answer-admin">Ответ Администратора</div>
        <a class="question-complain" href="#">Пожаловаться</a>
    </div>
    
    <div class="question-text">
    	я хочу приобрести данную модель и хочу заказать сразу фиксированный кронштейн, поэтому по поводу разъемов задней панели сказать не могу.
    </div>
    
    <div class="question-answer">
    	<div class="your-answer"><a href="#">Ответить</a></div>
    	<div class="answer-help-me"><a href="#">Ответ помог мне</a></div>
        <div class="help-counter">Ответ помог 7 пользователям</div>        
    </div>    
</div>
</div>
<div class="q-separator"></div>
<div class="my-question">
	<div class="my-question-header">Мой вопрос о {$product->name}</div>
    
    <div class="form-group">
        <label>Ваше имя</label>
        <input type="text" class="form-control" placeholder="" name="name" value="{if $user}{$user->name}{/if}" autocomplete="off">
		<div id="name-block-error-{$index}" class="help-block-error" style="display:none;">Пожалуйста введите Ваше имя</div>
    </div>
    
        <div class="form-group">
        <label>Ваш вопрос</label>
        <textarea class="form-control" rows="2" name="comments"></textarea>
		<div id="comment-block-error-{$index}" class="help-block-error" style="display:none;">Пожалуйста введите вопрос</div>
        
        <div class="my-question-add-images">
            <span class="btn btn-file btn-default btn-sm">
                <i class="fa fa-plus"></i> <span>Добавить изображения</span>
                <input id="uploadImageField-{$index}" type="file" multiple name="uploaded-images">
				<i class="fa fa-spin fa-spinner hidden" id="upload-anim-{$index}"></i>
            </span>
			
			<ul id="img-list-{$index}" class="hidden"></ul>
			
			<div class="object-images">
				{include file='reviews-product-images.tpl'}
			</div>
        </div>
        </div>
        
        <div class="my-question-send">
            <button type="button" class="btn btn-success" id="add-question-{$index}" data-index="{$index}">Задать вопрос</button>
        </div>     
   </div>   




