<div class="my-review" data-index="{$index}">
   	<div class="my-review-header">Мой отзыв о {$product->name}</div>
                            
   	<div class="myrate">
      	<label>Оцените товар</label>
        <div id="user-rating2-{$index}" class="rating">
			{if $product->rate_by_user}
			<input type="hidden" name="val" value="{$product->rate_by_user->rating}">
			{/if}
			{*<input type="hidden" name="product_id" value="{$product->id}"/>*}
			<input type="hidden" name="vote-id" value="3"/>
		</div>
		<div id="rating-block-error-{$index}" class="help-block-error" style="display:none;">Пожалуйста оцените товар</div>
    </div>   
           
	<div class="form-group {if !$settings->reviews_short_visible}hidden{/if}">
        <label>Отзыв в 2-ух словах:</label>
        <input type="text" class="form-control" placeholder="" name="short" value="" autocomplete="off" maxlength="45">
    </div>
            
    <div class="form-group {if !$settings->reviews_pluses_visible}hidden{/if}">
        <label>Достоинства</label>
        <textarea class="form-control" rows="2" name="pluses"></textarea>
    </div>
            
    <div class="form-group {if !$settings->reviews_minuses_visible}hidden{/if}">
        <label>Недостатки</label>
        <textarea class="form-control" rows="2" name="minuses"></textarea>
    </div>
            
    <div class="form-group">
        <label>Комментарий</label>
        <textarea class="form-control" rows="2" name="comments"></textarea>
		<div id="comment-block-error-{$index}" class="help-block-error" style="display:none;">Пожалуйста введите коментарий</div>
        
		{if $settings->reviews_images_visible}
        <div class="review-add-images">
            <span class="btn btn-file btn-default btn-sm">
                <i class="fa fa-plus"></i> <span>Добавить изображения</span>
                <input id="uploadImageField-{$index}" type="file" multiple name="uploaded-images">
				<i class="fa fa-spin fa-spinner hidden" id="upload-anim-{$index}"></i>
            </span>
			
			<ul id="img-list-{$index}" class="hidden"></ul>
			
			<div class="object-images">
				{include file='reviews-product-images.tpl'}
			</div>
        </div>
        {/if}
    </div>
    
    
            
    <div class="myrecommend {if !$settings->reviews_recommended_visible}hidden{/if}">
        <div class="checkbox">
            <label>
                <input type="checkbox" value="1" name="recommended">
                Да - я рекомендую этот товар 
            </label>
        </div>
    </div>
    
    <div class="form-group">
        <label>Ваше имя</label>
        <input type="text" class="form-control" placeholder="" name="name" value="{if $user}{$user->name}{/if}" autocomplete="off">
		<div id="name-block-error-{$index}" class="help-block-error" style="display:none;">Пожалуйста введите Ваше имя</div>
    </div>

	<input type="hidden" name="rating" value="{if $product->rate_by_user}{$product->rate_by_user->rating}{/if}"/>
	<input type="hidden" name="temp_id" value="{$temp_id}"/>
	
    <div class="my-review-send">
       	<button type="button" class="btn btn-success" id="add-review-{$index}" data-index="{$index}">Отправить отзыв</button>
    </div>
</div>

<script type="text/javascript">
	$('.my-review textarea').autosize();
</script>