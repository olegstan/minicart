{assign var="product_additional_class" value=""}
{if $product->badges}
{foreach $product->badges as $badge}
{if $badge->css_class_product}
	{if !empty($product_additional_class)}{assign var="product_additional_class" value=$product_additional_class|cat:' '}{/if}
	{assign var="product_additional_class" value=$product_additional_class|cat:"favorite-"}
	{assign var="product_additional_class" value=$product_additional_class|cat:($badge->css_class_product)}
{/if}
{/foreach}
{/if}
<li class="favorite-item{if $product_additional_class} {$product_additional_class}{/if}">
        <div class="favorite-left">
		<a href="{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}" class="favorite-image producthref">

		{if $product->image}<img src='{$product->image->filename|resize:products:100:100}' alt='{$product->name}'>{/if}
		</a>
    	</div>
	
	<div class="favorite-center">
    <div class="row">
    	<div class="col-xs-8">
	<div class="favorite-name-block" {if $mh_group}data-mh="ngroup-{$mh_group}"{/if}>
        <a href="{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}" class="favorite-name producthref">{$product->name}</a>
        {if $product->rating->rating_count > 0}
            <div class="rating-micro micro-rate{$product->rating->avg_rating*10}" title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}">
            </div>
        {/if}
    </div>
    	</div>
        
        <div class="col-xs-4">
        <a href="#" class="delete-from-favorite" data-id="{$product->id}" title="Удалить из избранного"><i class="fa fa-times"></i> <span>Удалить</span></a>
        </div>
    </div>
    
    
    
	<div class="favorite-description" {if $mh_group}data-mh="dgroup-{$mh_group}"{/if}>
        {if $product->badges}
		<ol class="favorite-list-badges">
		{foreach $product->badges as $badge}
			<li>
            	<span class="{$badge->css_class}">{$badge->name}</span>
            </li>
		{/foreach}
		</ol>
		{/if}
		{$product->annotation}
		
		{$show_properties = false}
		{if $product->tags && $tags_groups}
			{foreach $tags_groups as $tag_group}
				{if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}
					{$show_properties = true}
				{/if}
			{/foreach}
		{/if}
		
		{if $product->tags && $tags_groups && $show_properties}
			<div class="favorite-properties">
				<table class="table table-condensed">
					<tbody>
					{foreach $tags_groups as $tag_group}
						{if array_key_exists($tag_group->id, $product->tags_groups) && $tag_group->show_in_product_list && !empty($product->tags_groups[$tag_group->id])}
						<tr>
							<td>{$tag_group->name}{if $tag_group->help_text} <a class="property-help" data-type="qtip" data-content="{$tag_group->help_text|escape}" data-title="{$tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}</td>
							<td>
							{foreach $product->tags_groups[$tag_group->id] as $tag}
							{$tag->name}{if $tag_group->postfix} {$tag_group->postfix}{/if}{if !$tag@last}, {/if}
							{/foreach}
							</td>
						</tr>
						{/if}
					{/foreach}
					
				</table>
			</div>
		{/if}
        
        {*Артикул первого варианта товара*}
        {if $product->variant->sku}
            {*<div class="product-sku">Артикул: {$product->variant->sku}</div>*}
        {/if}
        
        {*Код товара*}
        {if $product->id}
            {*<div class="product-code">Код товара: {$product->id}</div>*}
        {/if}
        
	</div>
    
    </div>
    
    
    <div class="favorite-right">
    
	<div class="favorite-statusbar">

		<div class="favorite-status">
			<span class="in-stock" {if $product->variant->stock <= 0}style="display:none;"{/if}><i class="fa fa-check"></i> Есть в наличии</span>
			<span class="out-of-stock" {if $product->variant->stock != 0}style="display:none;"{/if}><i class="fa fa-times-circle"></i> Нет в наличии</span>
			<span class="to-order" {if $product->variant->stock >= 0}style="display:none;"{/if}><i class="fa fa-truck"></i> Под заказ</span>
		</div>
	</div>
	
	<div class="favorite-buy-block">
	
	{if $product->variants|count > 1}
        <div class="form-group favorite-variants">
        	<select class="form-control input-sm" name="variant_id">
				{foreach $product->variants as $variant}
					<option value="{$variant->id}" data-stock="{$variant->stock}" data-price="{$variant->price}" data-price-convert="{if $product->currency_id}{$variant->price|convert:$product->currency_id}{else}{$variant->price|convert:$admin_currency->id}{/if}" data-price-old="{$variant->price_old}" data-price-old-convert="{if $product->currency_id}{$variant->price_old|convert:$product->currency_id}{else}{$variant->price_old|convert:$admin_currency->id}{/if}" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if}>{$variant->name}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
				{/foreach}
            </select>
        </div>
	{else}
		<select name="variant_id" style="display: none;">
			<option value="{$product->variant->id}">{$product->variant->name}</option>
		</select>
	{/if}
	
		<div class="favorite-old-price" {if !$product->variant->price_old}style="display:none;"{/if}>{if $product->currency_id}{$product->variant->price_old|convert:$product->currency_id}{else}{$product->variant->price_old|convert:$admin_currency->id}{/if} <span>{$main_currency->sign}</span></div>
		<div class="favorite-price" {if !$product->variant->price}style="display:none;"{/if}>{if $product->currency_id}{$product->variant->price|convert:$product->currency_id}{else}{$product->variant->price|convert:$admin_currency->id}{/if} <span>{$main_currency->sign}</span></div>
		<a href="#" class="btn btn-success btn-sm favorite-buy w100" {if $product->variant->stock == 0}style="display:none;"{/if}>Купить</a>
	</div>
    
    </div>
    
	
</li>