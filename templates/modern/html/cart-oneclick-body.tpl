<table class="table carttable">
<thead>
<tr>
	<th class="carttable-product">Товар</th>
	<th class="carttable-priceone">Цена</th>
	<th class="carttable-count">Количество</th>
	<th class="carttable-priceall">Стоимость</th>
	<th class="remove"></th>
</tr>
</thead>
<tbody>
{foreach $cart->purchases as $purchase}
	<tr>
		<td class="carttable-product">
			<div class="carttable-image">
				{if $purchase->product->image}<a
					href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}"
					class="producthref"><img alt=""
											 src="{$purchase->product->image->filename|resize:'products':52:52}">
					</a>{/if}
			</div>
			<div class="carttable-product-name">
				<a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}"
				   class="producthref">{$purchase->product->name}</a>
				{if $purchase->variant->name}<span>({$purchase->variant->name})</span>{/if}
				{if $purchase->variant->stock == -1}
					<div class="cart-pod-zakaz">
						<span><i class="fa fa-truck"></i> Под заказ</span>
					</div>
				{/if}
				
				{if $settings->catalog_use_variable_amount && $purchase->var_amount > 1}
				<div class="cart-modifier" data-var-amount="{$purchase->var_amount}">
					<ul>
						<li>
							<div>{$settings->catalog_variable_amount_name} {$purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}</div>
						</li>
					</ul>
				</div>
				{/if}
				
				{if $purchase->modificators}
				<div class="cart-modifier">
					<ul>
						{foreach $purchase->modificators as $m}
							<li><div>{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert}%){elseif $m->type == 'minus_percent'}(-{$m->value|convert}%){/if} {if $purchase->modificators_count[$m@index]>1}{$purchase->modificators_count[$m@index]} шт.{/if}</div></li>
						{/foreach}
					</ul>
				</div>
				{/if}
				
			</div>
		</td>
		<td class="carttable-priceone" data-price-for-one-pcs="{if $purchase->product->currency_id}{$purchase->variant->price_for_mul|convert:$purchase->product->currency_id}{else}{$purchase->variant->price_for_mul|convert:$admin_currency->id}{/if}" data-price-additional="{if $purchase->product->currency_id}{$purchase->variant->additional_sum|convert:$purchase->product->currency_id}{else}{$purchase->variant->additional_sum|convert:$admin_currency->id}{/if}">{if $purchase->product->currency_id}{$purchase->variant->price_for_one_pcs|convert:$purchase->product->currency_id}{else}{$purchase->variant->price_for_one_pcs|convert:$admin_currency->id}{/if} {$main_currency->sign}</td>
		<td class="carttable-count">
			<div class="input-group">
			<span class="input-group-btn">
				<button data-type="minus" class="btn btn-default btn-sm" type="button"><i
							class="fa fa-minus"></i></button>
			</span>
				<input type="text" class="form-control input-sm" placeholder=""
					   value="{$purchase->amount}" name="amounts[{$purchase@index}{*$purchase->variant->id*}]"
					   data-id="{$purchase@index}{*$purchase->variant->id*}"
					   data-stock="{if $purchase->variant->stock == -1}999{else}{$purchase->variant->stock}{/if}">
			<span class="input-group-btn">
				<button data-type="plus" class="btn btn-default btn-sm" type="button"><i
							class="fa fa-plus"></i></button>
			</span>

			</div>
		</td>
		<td class="carttable-priceall">{if $purchase->product->currency_id}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$purchase->product->currency_id}{else}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$admin_currency->id}{/if} {$main_currency->sign}</td>
		<td class="carttable-remove"><a
					href="{$config->root_url}{$cart_module->url}{url add=['variant_id'=> $purchase@index, 'action' => 'remove_variant']}"
					title="Удалить товар из корзины"
					class="incart"><i class="fa fa-times"></i></a></td>
	</tr>
{/foreach}

</table>

<div class="subtotal">Итого:
	<div class="sub-price"><span id="cart_total_price">{$cart->total_price|convert}</span> {$main_currency->sign}</div>
</div>

<div class="subtotal notopline" {if !$user || !$user->discount}style="display:none;"{/if}>Ваша скидка <span id="user_discount" data-value="{if $user}{$user->discount}{else}0{/if}">{$user->discount}</span><span>%</span>:
	<div class="sub-price"><span id="cart_total_discount">{($cart->total_price*$user->discount/100)|convert}</span> {$main_currency->sign}</div>
</div>

<div class="subtotal" {if !$user || !$user->discount}style="display:none;"{/if}>Итого с учетом скидки:
	<div class="sub-price"><span id="cart_total_price_w_discount">{($cart->total_price - $cart->total_price*$user->discount/100)|convert}</span> {$main_currency->sign}</div>
</div>

<script type="text/javascript">
	$('table.carttable a.incart').click(function(){
		var href = $(this).attr('href');
		var tr = $(this).closest('tr');
		var a_href = tr.find('td.carttable-product div.carttable-product-name a');
		$.get(href, function(data){
			if (data.success)
			{
				if ($('table.carttable tr td.carttable-product').length == 1)
				{
					$('#buy1click').modal('hide');
				}
				else
				{
					tr.html('<td class="carttable-info" colspan="5">Товар '+a_href[0].outerHTML+' был удален из корзины.</td>');
					update_price();
				}
				update_cart_info();
			}
		});
		return false;
	});
	
	$('table.carttable td.carttable-count button[data-type=plus]').click(function(){
		var input = $(this).closest('td').find('input');
		var max_stock = input.data('stock');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) + 1;
			if (value > max_stock)
				value = max_stock;
			input.val(value);
			set_amount(input.attr('data-id'), value);
			update_price();
		}
	});
	
	$('table.carttable td.carttable-count button[data-type=minus]').click(function(){
		var input = $(this).closest('td').find('input');
		if (input.length > 0)
		{
			var value = parseInt(input.val()) - 1;
			if (value <=0 )
				value = 1;
			input.val(value);
			set_amount(input.attr('data-id'), value);
			update_price();
		}
	});
	
	var amount_ajax_context;
	function set_amount(variant_id, amount){		
		if (amount_ajax_context != null)
			amount_ajax_context.abort();
		
		amount_ajax_context = $.get('{$config->root_url}{$cart_module->url}?action=set_amount&variant_id='+variant_id+'&amount='+amount, function(data){
			if (data.success)
			{
				$('#cart-placeholder').html(data.data);
			}
			amount_ajax_context = null;
		});
	}
	
	function update_price(){
		var input = $('.delivery-select input:checked');
		var total_cart_price = 0;
		var discount = parseFloat($('#user_discount').attr('data-value'));
		
		$('table.carttable tbody tr').each(function(){
			var td_count = $(this).find('td.carttable-count input');
			var td_price = $(this).find('td.carttable-priceone').attr('data-price-for-one-pcs');
			var td_price_additional = $(this).find('td.carttable-priceone').attr('data-price-additional');
			
			if (td_count.length > 0 && td_price.length > 0)
			{
				var input_count = parseFloat(td_count.val().length > 0 ? td_count.val() : 1);
				var input_price = parseFloat(td_price.replace(/ /g,''));
				var input_price_additional = parseFloat(td_price_additional.replace(/ /g,''));
				var priceall = input_count * input_price + input_price_additional;
				priceall = Math.round(priceall);
				var priceall_str = priceall.toString();
				priceall_str = priceall_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				var td_priceall = $(this).find('td.carttable-priceall');
				td_priceall.html(priceall_str + ' {$main_currency->sign}');
				total_cart_price += priceall;
			}
		});
		
		var total_cart_price_w_discount = total_cart_price - total_cart_price * discount / 100;
		
		total_cart_price = Math.round(total_cart_price);
		var cart_total_price_str = total_cart_price.toString();
		cart_total_price_str = cart_total_price_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_price').html(cart_total_price_str);
		
		total_cart_price_w_discount = Math.round(total_cart_price_w_discount);
		var cart_total_price_w_discount_str = total_cart_price_w_discount.toString();
		cart_total_price_w_discount_str = cart_total_price_w_discount_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_price_w_discount').html(cart_total_price_w_discount_str);
		
		var cart_total_discount = Math.round(total_cart_price - total_cart_price_w_discount);
		var cart_total_discount_str = cart_total_discount.toString();
		cart_total_discount_str = cart_total_discount_str.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
		$('#cart_total_discount').html(cart_total_discount_str);
		
		if (total_cart_price >= {$settings->cart_order_min_price}){
			$('#not-enough-block').hide();
			$('#make-order-one-click').show();
			$('#buy1click .buy1click-user-info').show();
		}
		else{
			$('#not-enough-block').show();
			$('#make-order-one-click').hide();
			$('#buy1click .buy1click-user-info').hide();
		}
	}
</script>