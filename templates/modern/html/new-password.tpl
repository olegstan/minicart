<h1>Новый пароль</h1>

{if $user_node && !$success_message}
<form class="form-signin" action="{$config->root_url}{$login_module->url}{url add=['mode'=>'new-password']}" method="post" id="new-password-form">
<div class="row">
	<div class="col-xs-5">

	<div class="form-group">
		<label>Логин</label>
		<input type="text" class="form-control disabled" value="{$user_node->email}" disabled/>
	</div>

    	<div class="form-group">
        	<label>Введите новый пароль</label>
		<input type="password" name="password1" class="form-control required"/>
        </div>
        
        <div class="form-group">
        	<label>Введите новый пароль еще раз</label>
		<input type="password" name="password2" class="form-control required"/>
            	<p class="help-block-error" style="display:none;">Пароли не совпадают</p>
        </div>
        
        <div class="form-group">
		<button class="btn btn-success w100" type="submit">Сохранить</button>
        </div>

    	<input type="hidden" name="reset_code" value="{$reset_code}"/>
    </div>
            
</div>
</form>
{/if}

{if $error_message}
<div class="alert alert-warning">
{if $error_message == "empty_reset_code"}Отсутствует код восстановления
{elseif $error_message == "invalid_reset_code"}Код восстановления неверный
{else}{$error_message}
{/if}
</div>
{/if}

{if $success_message}
<div class="alert alert-success">
{if $success_message == "password_changed"}Пароль успешно изменен!
{else}{$success_message}
{/if}
</div>
{/if}

<script type="text/javascript">
	function check_form(){
		var input1 =  $('form#new-password-form input[name=password1]');
		var input2 =  $('form#new-password-form input[name=password2]');
		$('form#new-password-form p.help-block-error').hide();
		if (input1.val().length == 0){
			input1.focus();
			return false;
		}
		if (input2.val().length == 0){
			input2.focus();
			return false;
		}
		if (input1.val() != input2.val()){
			$('form#new-password-form p.help-block-error').show();
			input1.focus();
			return false;
		}
		return true;
	}

	$('form#new-password-form').submit(function(){
		if (!check_form())
			return false;
	});
</script>