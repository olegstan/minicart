<h1>Отзывы о магазине</h1>

<div class="review-shop-all"></div>

<div class="row">	
	<div class="col-xs-8">
    
    <div class="rsy">
    	<div class="rsy-avatar"><img src="" width="40" height="40"></div>
        <div class="rsy-text">
        	<textarea class="form-control" rows="1" placeholder="Ваш отзыв..."></textarea>
        </div>
        
        <div class="rsy-thumb">
        	<div class="btn-group" data-toggle="buttons">
              <label class="btn btn-default rsy-up">
                <input type="radio" name="options" id="" autocomplete="off" checked><i class="fa fa-thumbs-up"></i></i></label>
              <label class="btn btn-default rsy-down">
                <input type="radio" name="options" id="" autocomplete="off"><i class="fa fa-thumbs-down"></i></label>
</div>
        </div>
        
        <div class="rsy-send">
        <button type="submit" class="btn btn-default">Отправить</button>
        </div>
    </div>
    
    </div>
    
   <div class="col-xs-4">   
   	Общий рейтинг на основе 150 оценок покупателей

    <div class="review-shop-stars">
    	<div class="review-shop-circle-center">
    	<div class="review-shop-circle">
            97% <i class="fa fa-thumbs-o-up"></i> 
        </div>
        </div>
        <div class="review-shop-ritle">
            Общий итог: 97% пользователей довольны!
        </div>
        
        <div class="review-shop-stat">
				<div class="rating-value">
                	<a href="#" class="review-shop-view-plus">1544 положительных отзыва</a>
                </div>            	
                <div class="rating-value-count">
                	<a href="#" class="review-shop-view-minus">147 отрицательных отзывов</a>
                </div>
        </div>
    </div>
   </div>
</div>