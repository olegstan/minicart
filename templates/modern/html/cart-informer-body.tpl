{if $cart->total_products > 0}
	<ul class="dropdown-menu dropdown-cart">
	{foreach $cart->purchases as $purchase}
		<li class="in-cart-item">
			<div class="cart-image">
				{if $purchase->product->image}<a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}" class="producthref"><img alt="" src="{$purchase->product->image->filename|resize:'products':52:52}"></a>{/if}
			</div>
			<div class="cart-product-name">
				<a href="{$config->root_url}{$product_module->url}{$purchase->product->url}{$settings->postfix_product_url}?variant={$purchase->variant->id}" class="producthref">{$purchase->product->name}</a>
                <span class="cart-amount">{$purchase->amount} шт.</span>
				{if $purchase->variant->name}<span>({$purchase->variant->name})</span>{/if}
                {if $purchase->variant->stock == -1}
					<div class="cart-pod-zakaz">
						<span><i class="fa fa-truck"></i> Под заказ</span>
					</div>
				{/if}
                
				{if $settings->catalog_use_variable_amount && $purchase->var_amount > 1}
				<div class="cart-modifier" data-var-amount="{$purchase->var_amount}">
					<ul>
						<li>
							<div>{$settings->catalog_variable_amount_name} {$purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}</div>
						</li>
					</ul>
				</div>
				{/if}
                
				{if $purchase->modificators}
                                <div class="cart-modifier">
                                	<ul>
										{foreach $purchase->modificators as $m}
											<li><div>{$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert}%){elseif $m->type == 'minus_percent'}(-{$m->value|convert}%){/if} {if $purchase->modificators_count[$m@index]>1}{$purchase->modificators_count[$m@index]} шт.{/if}</div></li>
										{/foreach}
                                    </ul>
                                </div>
                {/if}
                
			</div>
			<div class="cart-priceblock">
            <div class="cart-product-price">{if $purchase->product->currency_id}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$purchase->product->currency_id}{else}{($purchase->variant->price_for_mul*$purchase->amount+$purchase->variant->additional_sum)|convert:$admin_currency->id}{/if} <span class="currency">{$main_currency->sign}</span></div>
            </div>
			<div class="cart-delete">
				<a href="{$config->root_url}{$cart_module->url}?variant_id={$purchase@index}&action=remove_variant" class="incart"><i class="fa fa-times"></i></a>
			</div>
		</li>
	{/foreach}
		<li class="divider"></li>
        
        
        
        <div class="cart-intotal">
        	<a href="#" class="cart-buy1click">Заказать в 1 клик</a>
            <div class="cart-intotal-right">
            Итого: <span>{$cart->total_price|convert} <span class="currency">{$main_currency->sign}</span></span><br/>
            {if $user->discount > 0}
            Итого со скидкой {$user->discount}%: <span>{($cart->total_price - $cart->total_price*$user->discount/100)|convert} <span class="currency">{$main_currency->sign}</span></span>
            {/if}
            </div>
        </div>
        <li class="divider"></li>
		<li>
			<a href="{$config->root_url}{$cart_module->url}" class="btn btn-success checkout" id="make-order">Оформить заказ <i class="fa fa-angle-right"></i></a>
		</li>
	</ul>
{/if}
