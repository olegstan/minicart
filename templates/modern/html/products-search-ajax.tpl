{if $products || $categories}
	<div id="new_menu" class="dd">
		<ol class="addlist">
			{if $products}
				<li class="object-header"><span class="apname">Товары</span></li>
				{foreach $products as $product}
					<li data-id="{$product->id}" data-object="product" data-href="{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}">
						<div class="apimage">{if $product->image}<img src='{$product->image->filename|resize:products:30:30}' alt='{$product->image->name}'>{/if}</div>
						<span class="apname">
                        {if $product->variant}
							<span class="apprice">
								{if $product->currency_id}
									{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
									{($product->variant->price*$product->min_amount)|convert:$product->currency_id}
									{else}
									{$product->variant->price|convert:$product->currency_id}
									{/if}
								{else}
									{if $settings->catalog_use_variable_amount && $product->use_variable_amount}
									{($product->variant->price*$product->min_amount)|convert:$admin_currency->id}
									{else}
									{$product->variant->price|convert:$admin_currency->id}
									{/if}
								{/if}
								{$main_currency->sign}
							</span>
						{/if}
                        <div class="app-productname">{$product->name}</div>
						{if ($settings->search_ajax_show_sku && $product->variant->sku) || $settings->search_ajax_show_product_id}
                        <div class="sky-id">
                        	{if $settings->search_ajax_show_sku && $product->variant->sku}<div class="apsky">{$product->variant->sku}</div>{/if}
                            {if $settings->search_ajax_show_product_id}<div class="apid">{$product->id}</div>{/if}
                        </div>
						{/if}
                        </span>
					</li>
				{/foreach}
			{/if}
			{if $categories}
				<li class="object-header"><span class="apname">Категории</span></li>
				{foreach $categories as $category}
					<li data-id="{$category->id}" data-object="category" data-href="{$config->root_url}{$products_module->url}{$category->url}/">
						<div class="apimage">{if $category->image}<img src='{$category->image->filename|resize:categories:30:30}' alt='{$category->image->name}'>{/if}</div>
						<span class="apname">{$category->name}</span>
					</li>
				{/foreach}
			{/if}
		</ol>
	</div>
{else}
	<div class="noresults">Ничего не найдено...</div>
{/if}