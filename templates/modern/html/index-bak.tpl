<!DOCTYPE html>
{*
	Общий вид страницы
	Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>
	
	<base href="{$config->root_url}/"/>
	<title>{$meta_title|escape}</title>
	
	{* Метатеги *}
	<meta charset="utf-8">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="description" content="{$meta_description|escape}" />
	<meta name="keywords" content="{$meta_keywords|escape}" />
        <link rel="shortcut icon" href="{$path_frontend_template}/img/favicon.ico">

   	{* Подключаем jquery последнюю версию из библиотек Code.jquery.com, Яндекса или localhost *}    
	{*<script src="http://code.jquery.com/jquery-latest.min.js"></script>*}
	{*<script src="http://code.jquery.com/jquery-2.0.0.min.js"></script>*}
    {*<script src="http://yandex.st/jquery/2.0.3/jquery.min.js"></script>*}
    <script src="{$path_frontend_template}/js/jquery.min.js"></script>
	<script src="{$path_frontend_template}/js/jquery-ui-1.10.4.custom.min.js"></script>
    
    {* Подключаем бустрап 3 *}
    <script src="{$path_frontend_template}/js/bootstrap.min.js"></script>
    
    {* Подключаем бустрап 3*}
    <link href="{$path_frontend_template}/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>
    
    {* Подключаем Font Awesome *}
    <link rel="stylesheet" href="{$path_frontend_template}/css/font-awesome.min.css">

    {* Подключаем Fancybox *}
	<script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
	<link href="{$config->root_url}/public/assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
	<script src="{$config->root_url}/public/assets/fancybox/jquery.fancybox.pack.js"></script>

	<link rel="stylesheet" href="{$config->root_url}/public/assets/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
	<script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.fancybox-buttons.js"></script>
	<link rel="stylesheet" href="{$config->root_url}/public/assets/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
	<script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.fancybox-thumbs.js"></script>
	{* /Подключаем Fancybox *}

	{* Подключаем ajax-заливку файлов *}
	<script src="{$path_frontend_template}/js/jquery.damnUploader.min.js"></script>
    
    {* Подключаем css *}    
    <link href="{$path_frontend_template}/css/theme.css" rel="stylesheet"/>
    <link href="{$path_frontend_template}/css/data.css" rel="stylesheet"/>

    {* Подключаем jquery.quicksand *}
    {*<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"> </script>*}
    <script src="{$path_frontend_template}/js/jquery.quicksand.js"></script>
    
	{* Подключаем jQuery Masked Input *}
	<script src="{$path_frontend_template}/js/jquery.maskedinput.min.js" type="text/javascript"></script>

	{* Подключаем AutoSave для корзины*}
	{*<script src="{$path_frontend_template}/js/autosaveform.js" type="text/javascript"></script>*}
	<script src="{$path_frontend_template}/js/jquery-cookie.js" type="text/javascript"></script>
	<script src="{$path_frontend_template}/js/sayt.min.jquery.js" type="text/javascript"></script>
    
	{* Подключаем qTip для тултипов *}
	<link type="text/css" rel="stylesheet" href="{$path_frontend_template}/css/jquery.qtip.min.css" />
	<script src="{$path_frontend_template}/js/jquery.qtip.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="{$path_frontend_template}/js/imagesloaded.pkg.min.js"></script>
	
	{* Подключаем ion.rangeSlider *}
	<script src="{$path_frontend_template}/js/rangeSlider.js" type="text/javascript"></script>
    
	{if $settings->catalog_count_opened_level > 0}
		{$nested_level = ""}
		{section name=nested start=0 loop=$settings->catalog_count_opened_level step=1}
			{$nested_level = $nested_level|cat:"> ol > li:not(.collapsed) "}
		{/section}
		{*assign var="nested_level" value="> ol > li:not(.collapsed)"*}
	{/if}
	
	{* Подключаем Javascript функции для обработки действий пользователя и загрузки контента *}
	<script type="text/javascript">
		{* Системные функции *}
		{include file='../js/system_functions.js' nocache}
		{* Функции загрузки контента *}           
		{include file='../js/content_functions.js' nocache}
		{* Функции обработки действий пользователя *}
		{include file='../js/actions_functions.js' nocache}
	</script>
    
    {* Подключаем кнопку Вверх *}
	
	{* Подключаем авторесайз для textarea *}
	<script src="{$path_frontend_template}/js/jquery.autosize.min.js"></script>
	
	{* Подключаем улучшалку отображения для старых браузеров *}
	 <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	{* Подключаем компонент для выставления рейтинга *}
	<script src="{$path_frontend_template}/js/jquery.rating-2.0.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('body').append ('<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>');
			$(window).scroll(function () {
				if ($(this).scrollTop() > 0)
					$('#scroller').fadeIn();
				else
					$('#scroller').fadeOut();
			});
			$('#scroller').click(function () {
				$('body,html').animate( { scrollTop: 0 }, 400 );
				return false;
			});
		});
	</script>


<body data-spy="scroll" data-target=".subnav" data-offset="50" class="{$body_category_css} {$body_product_css}">

<div id="wrap">  

{if $smarty.session.admin && $smarty.session.admin == "admin"}
<nav class="navbar navbar-default noradius mininavbar" role="navigation">
	<div class="container">         
		<ul class="nav navbar-nav">
			<li><a href="{$config->root_url}/admin/"><i class="fa fa-level-up"></i> <span>Административная панель</span></a></li>
		</ul>
		<ul class="nav navbar-nav  pull-right">
			<li><a href="/admin/logout/" class="logout"><i class="fa fa-power-off"></i> <span>Выход</span></a></li>
		</ul>
	</div>
</nav>
{/if}
	<!-- Begin page content -->
    <div class="topline">
    	<div class="container nopadding">
        	<div class="row topline-links">
            	<div class="col-xs-4">
                
                
               
                <ul class="login-gerister">
					{if !$user}
				  <li class="login-gerister-inline"><a href="{$config->root_url}{$login_module->url}">Вход в личный кабинет</a></li>
                  <li class="login-gerister-inline"> | <a href="{$config->root_url}{$register_module->url}">Регистрация</a></li>
				  {else}
                  {* Этот блок показываем когда клиент залогинился *}
                  <li class="login-gerister-inline">
                        <a href="{$config->root_url}{$account_module->url}">Личный кабинет</a>
                  </li>
                   |
				  <li class="login-gerister-inline">
					<i class="fa fa-power-off"></i>	<a href="{$config->root_url}{$login_module->url}{url add=['logout'=>1]}">Выход</a>
				  </li>
				  {/if}
               </ul>
				
                </div>
                
                <div class="col-xs-3">
                    <div class="compare-body" {if !$compare_href_show}style="display:none;"{/if}>
                        <i class="fa fa-bars"></i> <a href="{$compare_href}" id="make-compare">Сравнить {$compare_items|count} {$compare_items|count|plural:'товар':'товаров':'товара'}</a>
                    </div>
                </div>
                <div class="col-xs-5 taright">
					<i class="fa fa-phone"></i> <a href="#" {*data-toggle="modal" data-target="#contact-center"*} id="make-callback">Закажите обратный звонок</a>
                </div>
            </div>
        </div>
    </div>
    
	<div class="container">
    <header>
    	<div class="row">
    		<div class="col-xs-3">
            	<a href="/" class="logo"></a>
            </div>
            <div class="col-xs-6">
            	{* Поиск *}
                <div id="search">
                <form role="search">
                		<button id="searchCancelButton" class="searchclear" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
					
						<input id="searchField" type="text"  class="form-control" placeholder="{$settings->search_placeholder}" autocomplete="off" value="{$keyword}">	
						
                        <button id="searchButton" type="button" class="btn btn-default">Найти</button>
                </form>	
				<div id="refreshpart" class="addcontainer" style="display:none;"></div>
                
				{if $settings->search_help_text1 || $settings->search_help_text2}
                <div class="search-helper">
					Например, {if $settings->search_help_text1}<span id="help_msg1">{$settings->search_help_text1}</span>{/if} {if $settings->search_help_text1 && $settings->search_help_text2}или{/if} {if $settings->search_help_text2}<span id="help_msg2">{$settings->search_help_text2}</span>{/if}
				</div>
				{/if}
            </div>

            </div>
            <div class="col-xs-3">
            {if {banner id=2}}
            	<div class="phone">				
            	{banner id=2}				
                </div>
            {/if}
            </div>
		</div>
    </header>
      
      <nav class="navbar navbar-default" role="navigation" id="topmenu">

		{menu id=1}
    
       <ul class="nav navbar-nav navbar-right">
                   <li class="dropdown cart-dropdown" id="cart-placeholder">
					{include file='cart-informer.tpl'}
                  </li>
               </ul>
    
	</nav>
    
    {* Объявление на сайте *}
    
{if {banner id=4}}
    <div class="panel panel-default topnews">
  		<div class="panel-body">
    	{banner id=4}
    	</div>
	</div>
{/if}

	{if $show_success_registration}
	<div class="alert alert-success"><strong>Поздравляем!</strong> Теперь, используя <a href="/account/">личный кабинет</a>, вы можете <a href="/account/?mode=personal-data">сменить личные данные</a> или <a href="/account/?mode=change-password">пароль</a>. Также Вам доступна <a href="/account/?mode=my-orders">история заказов</a> и <a href="/account/?mode=notification-settings">управление оповещениями на почту и SMS</a>.</div>
    {/if}

	<div class="row" id="main-container">
    	<div class="col-xs-3" id="main-left-container" {if $module->module=='CartController' || $module->module=='CompareController' || $module->module=='OrderController' || $module->module=='AccountController'}style='display:none;'{/if}>
        	{* Список категорий для результатов поиска начало*}
            <div id="searchcategories" {if !$finded_categories}style="display:none;"{/if}>
				{if $finded_categories}
				<div class="searchcats">Категории:</div>
				<ol>
					{*<li>
						<div>
							<span></span>
							<a class="menu61 {if !$category_id}active{/if}" href="{$config->root_url}{$products_module->url}{url add=['format'=>'search', 'keyword'=>$keyword]}">Все</a>
						</div>
					</li>*}
				{foreach $finded_categories as $categories_position}
				{foreach $categories_position as $cat}
					<li>
						<div>
							<span>{$cat.count}</span>
							<a class="menu61 {if $cat.id == $category_id}active{/if}" href="{$cat.url}">{$cat.name}</a>
						</div>
					</li>
				{/foreach}
				{/foreach}
				</ol>
				{/if}
            	
            </div>
            {* Список категорий для результатов поиска конец *}
            
            <div id="categories" class="panel panel-default">
            	<div class="panel-heading">Каталог товаров</div>
                <div id="collapseCategory">
                <div class="panel-body">
            {* Дерево строится на аяксе *}
				<ol>
					{function name=categories_tree level=0}
						{foreach $items as $item}
							<li class="{if !$item.subcategories}collapsed{/if} {if $item.folder}havechild{/if}" data-id="{$item.id}">
							{if $item.folder}
								<i class="fa fa-minus" style="{if $item.subcategories}display: block;{else}display: none;{/if}"></i>
								<i class="fa fa-plus" style="{if $item.subcategories}display: none;{else}display: block;{/if}"></i>
								<i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
							{/if}
								<div>
									<span>{$item.products_count}</span>
									<a href="{$config->root_url}{$products_module->url}{$item.url}" class="menu{$item.id} {if $item.id == $category_id}active{/if}">{$item.title}</a>
								</div>
								{if $item.subcategories}
									<ol style="display: block;">
									{categories_tree items=$item.subcategories level=$level+1}
									</ol>
								{/if}
							</li>
						{/foreach}
					{/function}
					{categories_tree items=$categories_frontend}
				
				
					{*foreach $categories_frontend as $c}
					<li class="collapsed {if $c.folder}havechild{/if}" data-id="{$c.id}">
					{if $c.folder}
						<i class="fa fa-minus" style="display: none;"></i>
						<i class="fa fa-plus" style="display: block;"></i>
						<i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
					{/if}
					<div>
						<span>{$c.products_count}</span>
						<a href="{$config->root_url}{$products_module->url}{$c.url}" class="menu{$c.id}">{$c.title}</a>
					</div>
					</li>
					{/foreach*}
				</ol>
            </div>
            </div>
            </div>
            {* Дерево категорий конец *}
            
            {include file='filter.tpl'}

			{menu id=7}
			
        
			{$brands_all_count = 0}			
			{foreach $brands_all as $b}{if $b->products_count > 0}{$brands_all_count = $brands_all_count + 1}{/if}{/foreach}
		  <div id="brands" class="panel panel-default" {if $brands_all_count == 0}style="display:none;"{/if}>
			<div class="panel-heading">Бренды
				{if $brands_popular|count > 0}
                <div class="btn-group brandlinks">
                    <a href="#" id="pop-brands" class="btn btn-xs current"><span>Популярные</span></a>
					<a href="#" id="all-brands" class="btn btn-xs"><span>Все</span></a>
				</div>
				{/if}
			</div>
			{$brands_popular_count = 0}
			{foreach $brands_popular as $b}{if $b->products_count > 0}{$brands_popular_count = $brands_popular_count + 1}{/if}{/foreach}
			{if $brands_popular_count > 0}
			<div class="panel-body" id="pop-brands-list">
                            <ul>
                            {foreach $brands_popular as $b}
							{if $b->products_count > 0}
            			<li data-id="{$b->id}">
            				<a href="{$config->root_url}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}" {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a> <span>{$b->products_count}</span>
            			</li>
							{/if}
                            {/foreach}
                            </ul>
			</div>
			{/if}
                        <div class="panel-body" id="all-brands-list" {if $brands_popular_count > 0}style="display:none;"{/if}>
                            <ul>
                            {foreach $brands_all as $b}
							{if $b->products_count > 0}
            			<li data-id="{$b->id}">
            				<a href="{$config->root_url}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}" {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a> <span>{$b->products_count}</span>
            			</li>
							{/if}
                            {/foreach}
                            </ul>
			</div>
            </div>
            
            <div id="viewedproducts">
            {* Блок генерируется через ajax *}
			{include file='viewed-products.tpl'}
            </div>
            
             {* Баннер группы Вконтакте *}
            {if {banner id=6}}
                <div class="left-module">
                	{banner id=6}
                </div>
        	{/if}
            
		{if $news}
        <div id="news">
        	<div class="head"><a href="{makeurl module='materials' item=$news_category}">Новости</a></div>
            <ul>
				{foreach $news as $news_item}
				<li>
					<div class="date">{$news_item->date|date}</div>
					<a href="{makeurl module='materials' item=$news_item}" class="news-header">{if $news_item->title}{$news_item->title|escape}{else}{$news_item->name|escape}{/if}</a>
					<div class="news-description">
						{if $news_item->image}
                        <a href="{makeurl module='materials' item=$news_item}" class="news-header" alt="{if $news_item->title}{$news_item->title|escape}{else}{$news_item->name|escape}{/if}">
                        <img src="{$news_item->image->filename|resize:'materials':201:150}" alt="{if $news_item->title}{$news_item->title|escape}{else}{$news_item->name|escape}{/if}"/>
                        </a>
                        {/if}
						{$news_item->description|preannotation}
						<p><a href="{makeurl module='materials' item=$news_item}" class="readmore">Подробнее</a></p>
					</div>
				</li>
				{/foreach}
				<a href="{makeurl module='materials' item=$news_category}" class="allnews">Все новости ({$news_count})</a>
			</ul>
        </div>
		{/if}
        
        {* Местро под дополнительный модуль под Новостями *}
        {if {banner id=7}}
                <div class="left-module">
                	{banner id=7}
                </div>
        	{/if}
            
        </div>
        <div class="{if $module->module=='CartController' || $module->module=='CompareController' || $module->module=='AccountController'}col-xs-12{else}col-xs-9{/if}" id="main-right-container">
        	{*Выводим контент*}
        	<div id="content">
            	{$content}
            </div> 
        </div>
     
    </div>



      </div>
      
    </div>
    
    
    <div id="footer">
      <div class="container">
        {banner id=3}
      </div>
    </div>


{* Подключаем модальные окна *}
{include file='modals.tpl'}

<script>
	$(document).ready(function(){
		$('a[data-toggle="tooltip"]').tooltip({
			container: 'body'
		});

		{*$('a[data-toggle="popover"]').popover({
			html: true,
			trigger: 'hover'
		});*}
		
		$('a[data-type="qtip"]').qtip({
			content: {
				attr: 'data-content',
				title: function(event, api) {
					return $(this).attr('data-title');
				},
				button: true
			},
			style: {
				classes: 'qtip-light qtip-rounded'
			},
			show: {
				delay: 0{*,
				event: 'hover'*}
			}{*,
			hide: {
				event: 'unfocus'
			}*}
		});
		
		{*history_products_ajax();

		{if $category_path}
			var path = "{$category_path}";
			path = path.split(',');
			print_lazy_result($('#categories'),0,undefined,undefined, path, {$category_id});
		{else}
			{if $category_id}
				print_lazy_result($('#categories'),0,undefined,undefined, [], {$category_id});
			{else}
				print_lazy_result($('#categories'),0,undefined,undefined,[],0);
			{/if}
		{/if*}
	});
	
	
	var initialPopup = true;
	
	window.addEventListener('popstate', function(event) {
		if (event.state != undefined && event.state.mode != undefined)
		{
			if (event.state.mode == 'brand'){
				brand_request_ajax(event.state.params, false);
			}
			if (event.state.mode == 'products'){
				$('#main-left-container').show();
				if ($('#main-right-container').hasClass('col-xs-12'))
				{
					$('#main-right-container').removeClass('col-xs-12');
					$('#main-right-container').addClass('col-xs-9');
				}
			
				var category_path = event.state.category_path;
				var selected_category_id = event.state.selected_category_id;
				
				{* Свернем другие категории *}
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					if (icon_minus.length > 0)
						icon_minus.trigger('click');
				});
				
				if (category_path.length > 0)
				{
					var path = category_path.split(',');
					
					while (path.length > 0){
						var id = path[0];
						var li = $('#categories li[data-id='+id+']');
						var icon_plus = li.find('i.fa-plus:first');
						if (icon_plus.length > 0)
							icon_plus.trigger('click');
						path.shift();
					}
				}
				
				if (selected_category_id.length > 0)
				{
					var li = $('#categories li[data-id='+selected_category_id+']');
					var icon_plus = li.find('i.fa-plus');
					if (icon_plus.length > 0)
						icon_plus.trigger('click');
					$('#categories li[data-id='+selected_category_id+'] a:first').addClass('active');
				}
				
				products_request_ajax(event.state.params, false);
			}
			if (event.state.mode == 'product'){
				$('#main-left-container').show();
				if ($('#main-right-container').hasClass('col-xs-12'))
				{
					$('#main-right-container').removeClass('col-xs-12');
					$('#main-right-container').addClass('col-xs-9');
				}
			
				var category_path = event.state.category_path;
				var selected_category_id = event.state.selected_category_id;
				
				{* Свернем другие категории *}
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					if (icon_minus.length > 0)
						icon_minus.trigger('click');
				});
				
				if (category_path.length > 0)
				{
					var path = category_path.split(',');
					
					while (path.length > 0){
						var id = path[0];
						var li = $('#categories li[data-id='+id+']');
						var icon_plus = li.find('i.fa-plus:first');
						if (icon_plus.length > 0)
							icon_plus.trigger('click');
						path.shift();
					}
				}
				
				if (selected_category_id.length > 0)
				{
					var li = $('#categories li[data-id='+selected_category_id+']');
					var icon_plus = li.find('i.fa-plus');
					if (icon_plus.length > 0)
						icon_plus.trigger('click');
					$('#categories li[data-id='+selected_category_id+'] a:first').addClass('active');
				}
				
				product_request_ajax(event.state.url, false);
			}
			if (event.state.mode == 'material'){
				$('#main-left-container').show();
				if ($('#main-right-container').hasClass('col-xs-12'))
				{
					$('#main-right-container').removeClass('col-xs-12');
					$('#main-right-container').addClass('col-xs-9');
				}
			
				{* Свернем другие категории *}
				$('#categories a.active').removeClass('active');
				$('#categories li:not(.collapsed) {$nested_level}').each(function(){
					var icon_minus = $(this).find('i.fa-minus');
					if (icon_minus.length > 0)
						icon_minus.trigger('click');
				});
				
				$('#collapseCategory').addClass('in');
				
				material_request_ajax(event.state.url, false);
			}
			if (event.state.mode == 'cart'){
				cart_request_ajax();
			}
			if (event.state.mode == 'compare'){
				compare_request_ajax();
			}
		}
		{*else 
		if (initialPopup)
		{
			initialPopup = false;
			
			{if $module->module == "ProductsController"}
			if (typeof history.pushState != undefined)
				history.replaceState({ mode: 'products', params: [{ key: 'url', value: '{$config->root_url}{$module->url}{$current_url}'},{foreach $params_arr as $key=>$val}{ key: '{$key}', value: '{$val}'}{if !$val@last},{/if}{/foreach}], selected_category_id: '{$category->id}', category_path: "{$category_path}"},null,encodeURI(decodeURI(document.location.href)));
			{elseif $module->module == "ProductController"}
			if (typeof history.pushState != undefined)
				history.replaceState({ mode: 'product', url: encodeURI(decodeURI(document.location.href)), category_path: "{$category_path}", selected_category_id: '{$category->id}'},null,encodeURI(decodeURI(document.location.href)));
			{elseif $module->module == "PagesController" || $module->module == "MainController"}
			if (typeof history.pushState != undefined)
				history.replaceState({ mode: 'material', url: encodeURI(decodeURI(document.location.href))},null,encodeURI(decodeURI(document.location.href)));
			{/if}
		}
		else
			document.location.href = location.origin;*}
	});
	
	{if $module->module == "ProductsController"}
	if (typeof history.pushState != undefined)
		history.replaceState({ mode: 'products', params: [{ key: 'url', value: '{$config->root_url}{$module->url}{$current_url}'},{foreach $params_arr as $key=>$val}{ key: '{$key}', value: '{$val}'}{if !$val@last},{/if}{/foreach}], selected_category_id: '{$category->id}', category_path: "{$category_path}"},null,encodeURI(decodeURI(document.location.href)));
	{elseif $module->module == "ProductController"}
	if (typeof history.pushState != undefined)
		history.replaceState({ mode: 'product', url: encodeURI(decodeURI(document.location.href)), category_path: "{$category_path}", selected_category_id: '{$category->id}'},null,encodeURI(decodeURI(document.location.href)));
	{elseif $module->module == "PagesController" || $module->module == "MainController"}
	if (typeof history.pushState != undefined)
		history.replaceState({ mode: 'material', url: encodeURI(decodeURI(document.location.href))},null,encodeURI(decodeURI(document.location.href)));
	{/if}
</script>

{* Выводим коды счетчиков и различной аналитики *}
{$settings->counters_codes}
</body>
</html>