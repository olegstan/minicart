<div class="row topblocks">
    <div class="col-xs-3">
    {if {banner id=19} or {banner id=24} or {banner id=25} }
    <div class="flex-vertical">

    
			{if {banner id=19}}
                <div class="left-main {bannercss id=19}">
				{if {bannerlink id=19}}
					<a href="{bannerlink id=19}" target="{bannerlinkwindow id=19}" title="{bannername id=19}">
				{/if}
                    <div class="left-main-header">
						<span>{bannername id=19}</span>
                    </div>
                    <div class="left-main-body">
                    {banner id=19}
                    </div>
                {if {bannerlink id=19}}
					</a>
				{/if}
                </div>
             
            {/if}
            
            {if {banner id=24}}
                <div class="left-main {bannercss id=24}">
				{if {bannerlink id=24}}
					<a href="{bannerlink id=24}" target="{bannerlinkwindow id=24}" title="{bannername id=24}">
				{/if}
                    <div class="left-main-header">
						<span>{bannername id=24}</span>
                    </div>
                    <div class="left-main-body">
                    {banner id=24}
                    </div>
				{if {bannerlink id=24}}
					</a>
				{/if}
                </div> 
            {/if}
            
            
      		{if {banner id=25}}
                <div class="left-main {bannercss id=25}">
				{if {bannerlink id=25}}
					<a href="{bannerlink id=25}" target="{bannerlinkwindow id=25}" title="{bannername id=25}">
				{/if}
                    <div class="left-main-header">
						<span>{bannername id=25}</span>
                    </div>
                    <div class="left-main-body">
                    {banner id=25}
                    </div>
				{if {bannerlink id=25}}
					</a>
				{/if}
                </div> 
            {/if}
            
        
    
    </div>
    {/if}
    </div>
     


    
    <div class="col-xs-9">
        {* Слайдшоу - начало *}
            {if $settings->slideshow_enabled && $slides|count>0}
            <div class="swiper-container main-slideshow">
                    <div class="swiper-wrapper">
                        {foreach $slides as $s}
                            {if $s->image->filename}
                            <div class="swiper-slide">
                                {*if $s->url}<a href="{$s->url}">{/if*}
                                <img src="{$s->image->filename|resize:'slideshow'}"/>
                                {*if $s->url}</a>{/if*}
                            </div>
                            {/if}
                        {/foreach}
                    </div>
                    <!-- Пагинация -->
                    <div class="swiper-pagination"></div>
                    <!-- Стрелки навигации -->
                    <div class="swiper-button-next swiper-button-black"></div>
                    <div class="swiper-button-prev swiper-button-black"></div>
                </div>
            {/if}
{* Слайдшоу - конец *}
    </div>
</div>



{* Группы категорий - начало *}
        {*foreach $groups_categories_on_main as $g}
        <div class="block-class">
            <legend>{$g->name}</legend>
            <ul class="category-list">
                {foreach $g->related_categories as $category}
                <li class="{if $category->css_class} {$category->css_class}{/if}">
                    <a href="{$config->root_url}{$products_module->url}{$category->url}/">
                        <div class="category-list-image">
                            {if $category->image}<img src='{$category->image->filename|resize:categories:155:155}'
                                                      alt='{$category->image->name}'>{/if}
                        </div>
                        <div class="category-list-name"><span>{$category->name}</span>{$category->products_count}</div>
                    </a>
                </li>
                {/foreach}
            </ul>
        </div>
        {/foreach*}
        {* Группы категорий - конец *}


        {* Группы товаров - начало *}
        {foreach $groups_on_main as $g}
        <div class="block-class">
            <legend>{$g->name}</legend>
            <p>
                {$show_mode = true}
                {if empty($show_mode)}{$show_mode = $settings->default_show_mode}{/if}

                {if $show_mode == 'list'}
            <ul class="list">
                {foreach $g->related_products as $product}
                    {include file='product-list.tpl'}
                {/foreach}
            </ul>
            {/if}

            {if $show_mode == 'tile'}
            <ul class="plitka">
                {$mh_group = 1}
                {foreach $g->related_products as $product}
                    {include file='product-tile.tpl' mh_group=$mh_group}
                {if $product@iteration is div by 4}
                <div class="plitka-separator"></div>
                {$mh_group = $mh_group + 1}{/if}
                {/foreach}
            </ul>
            {/if}
            </p>
        </div>
        {/foreach}
        {* Группы товаров - конец *}
        
<div class="row">
    <div class="col-xs-3">
        {material_category id=$settings->news_category_id count=$settings->news_show_count header='Новости' footer='Все новости'}
        {material_category id=3 count=5 header='Статьи' footer='Все статьи'}
    </div>


    <div class="col-xs-9">

        {* Материал главной страницы - начало *}
        {if $main_menu_item->object_type == "material"}
		
		{$allow_edit_module = false}
		{*if in_array($material_module_admin->module, $global_group_permissions)}{$allow_edit_module = true}{/if*}


        <h1>
            {if $material->title}{$material->title|escape}{else}{$material->name}{/if}{if $allow_edit_module} <span><a href="{$config->root_url}{$material_module_admin->url}{url add=['id'=>$material->id]}" class="edit btn btn-default btn-sm"><i class="fa fa-pencil"></i> Редактировать материал</a></span>{/if}
        </h1>
        {if $material->parent_id > 0 && $category->show_date}<p class="material-date">{$material->date|date}</p>{/if}
        <div class="material-full">
            {$material->description|annotation}
        </div>

        {if $images_gallery}
        {include file='material-gallery.tpl'}
        {/if}
		
		{if $attachments}
		<div class="product-files">
			<div class="files-header">Файлы для скачивания:</div>
			<ul class="files">
				{foreach $attachments as $attach}
					<li>
						<div class="filename">
							<div class="fileimage">
								{$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}
								{if in_array($attach->extension, $exts)}
									<img src="{$path_frontend_template}/img/fileext/{$attach->extension}.png">
								{else}
									<img src="{$path_frontend_template}/img/fileext/other.png">
								{/if}
							</div>
							<a href="{*$attach->filename|attachment:'materials'*}" target="_blank">
		{if $attach->name}{$attach->name}{if $attach->extension}.{$attach->extension}{/if}{else}{$attach->filename}{/if}</a>
						</div>
					</li>
				{/foreach}
			</ul>
		</div>
		{/if}

        {/if}

        {* Материал главной страницы - конец *}


    </div>
</div>

        {$brands_all_count = 0}
        {foreach $brands_all as $b}
            {if $b->products_count > 0}
                {$brands_all_count = $brands_all_count + 1}
            {/if}
        {/foreach}

        <div id="brands" {if $brands_all_count == 0}style="display:none;"{/if}>
            <div class="head">Бренды
                {if $brands_popular|count > 0}
                    <div class="btn-group brandlinks">
                        <a href="#" id="pop-brands" class="btn btn-xs current"><span>Популярные</span></a>
                        <a href="#" id="all-brands" class="btn btn-xs"><span>Все</span></a>
                    </div>
                {/if}
            </div>
            {$brands_popular_count = 0}
            {foreach $brands_popular as $b}{if $b->products_count > 0}{$brands_popular_count = $brands_popular_count + 1}{/if}{/foreach}
            {if $brands_popular_count > 0}
                <div class="panel-body" id="pop-brands-list">
                    <ul>
                        {foreach $brands_popular as $b}
                            {if $b->products_count > 0}
                                <li data-id="{$b->id}">
                                    <a href="{$config->root_url}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}"
                                       {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                                    <span>{$b->products_count}</span>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            {/if}
            <div class="panel-body" id="all-brands-list" {if $brands_popular_count > 0}style="display:none;"{/if}>
                <ul>
                    {foreach $brands_all as $b}
                        {if $b->products_count > 0}
                            <li data-id="{$b->id}">
                                <a href="{$config->root_url}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}"
                                   {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                                <span>{$b->products_count}</span>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>


<script type="text/javascript">
    $(document).ready(function () {
        $('ul.list').on('change', 'li.listitem select[name=variant_id]', function () {
            var li = $(this).closest('li');
            var option = $(this).find('option:selected');
            li.find('span.in-stock').hide();
            li.find('span.out-of-stock').hide();
            li.find('span.to-order').hide();

            var stock = parseInt(option.attr('data-stock'));
            if (stock > 0) {
                li.find('span.in-stock').show();
                li.find('a.list-buy').show();
                li.find('a.buy-one-click').show();
            }
            if (stock == 0) {
                li.find('span.out-of-stock').show();
                li.find('a.list-buy').hide();
                li.find('a.buy-one-click').hide();
            }
            if (stock < 0) {
                li.find('span.to-order').show();
                li.find('a.list-buy').show();
                li.find('a.buy-one-click').show();
            }
            var price = parseFloat(option.attr('data-price'));
            var price_old = parseFloat(option.attr('data-price-old'));
            if (price > 0)
                li.find('div.list-price').show().html(option.attr('data-price-convert') + ' <span>{$main_currency->sign}</span>');
            else
                li.find('div.list-price').hide();
            if (price_old > 0)
                li.find('div.list-old-price').show().html(option.attr('data-price-old-convert') + ' <span>{$main_currency->sign}</span>');
            else
                li.find('div.list-old-price').hide();
            if (price < {$settings->cart_order_min_price} || stock == 0)
                li.find('a.buy-one-click').hide();
            else
                li.find('a.buy-one-click').show();
        });
        $('ul.plitka').on('change', 'li.plitka-item select[name=variant_id]', function () {
            var li = $(this).closest('li');
            var option = $(this).find('option:selected');
            li.find('div.plitka-status span').each(function () {
                $(this).hide();
            });
            var stock = parseInt(option.attr('data-stock'));
            if (stock > 0) {
                li.find('span.in-stock').show();
                li.find('a.plitka-buy').show();
            }
            if (stock == 0) {
                li.find('span.out-of-stock').show();
                li.find('a.plitka-buy').hide();
            }
            if (stock < 0) {
                li.find('span.to-order').show();
                li.find('a.plitka-buy').show();
            }
            var price = parseFloat(option.attr('data-price'));
            var price_old = parseFloat(option.attr('data-price-old'));
            if (price > 0)
                li.find('div.plitka-price').show().html(option.attr('data-price-convert') + ' <span>{$main_currency->sign}</span>');
            else
                li.find('div.plitka-price').hide();
            if (price_old > 0)
                li.find('div.plitka-old-price').show().html(option.attr('data-price-old-convert') + ' <span>{$main_currency->sign}</span>');
            else
                li.find('div.plitka-old-price').hide();
        });

        $('ul.plitka .plitka-name-block').matchHeight(1);
        $('ul.plitka .plitka-description').matchHeight(1);

        $('ul.category-list .category-list-image').matchHeight(1);
        $('ul.category-list .category-list-name').matchHeight(1);

        $(".fancybox").fancybox({
            autoResize: true,
            autoCenter: true,
            prevEffect: 'none',
            nextEffect: 'none',
            helpers: {
                title: {
                    type: 'outside'
                },
                thumbs: {
                    width: 50,
                    height: 50
                },
                overlay: {
                    locked: false
                }
            }
        });
    });
</script>