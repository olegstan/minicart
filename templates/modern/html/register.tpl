<h1>Регистрация</h1>
<div id="register-form-success" class="alert alert-success hidden"><strong>Поздравляем!</strong> Теперь вы можете войти в <a href="{$config->root_url}{$account_module->url}">личный кабинет</a> используя свой логин и пароль.</div>
<div id="register-form-error" class="alert alert-warning hidden"></div>
<form id="register-form" action="user/register" method="POST" novalidate>
	<div class="tab-pane active">
		<div class="row">
			<div class="col-xs-4">
				<div class="form-group">
					<label>Ваше Имя *</label>
					<div class="input-ok"><i class="fa fa-check" style="display:none;"></i></div>
					<input id="register-form-name" type="text" class="form-control" name="name" />
					<p class="help-block-error hidden">Это поле обязательно для заполнения</p>
				</div>
				<div class="form-group">
					<label>Мобильный телефон для связи</label>
					<div class="input-ok"><i class="fa fa-check" style="display:none;"></i></div>
					<input id="register-form-phone" type="tel" class="form-control" name="phone"/>
					<p class="help-block-error hidden">Пожалуйста укажите номер телефона</p>
				</div>
				<div class="form-group">
					<label>Электронный адрес (e-mail) *</label>
					<div class="input-ok"><i class="fa fa-check" style="display:none;"></i></div>
					<input id="register-form-email" type="email" class="form-control" name="email" data-type="email" value="{$user_register->email}"/>
					<p class="help-block-error hidden">Это поле обязательно для заполнения</p>
				</div>  	
				<div class="form-group">
					<label>Пароль *</label>
					<div class="input-ok"><i class="fa fa-check" style="display:none;"></i></div>
					<input id="register-form-password" type="password" class="form-control" name="password" />
					<p class="help-block-error hidden">Это поле обязательно для заполнения</p>
				</div>
				{*if $settings->google_recaptcha_enabled}
                    <div class="form-group">
                        <div class="g-recaptcha" data-sitekey="{$settings->google_recaptcha_key}"></div>
                    </div>
				{/if*}
				<button type="submit" class="btn btn-lg btn-success w100">Зарегистрироваться</button>
			</div>
        </div>
    </div>
</form>

<script type="text/javascript" src="{$path_frontend_template}js/views/user/register.js"></script>

<script type="text/javascript">
	/*$(document).ready(function(){

	});
	
	$('#person input.required').focusout(function(){
		var type = $(this).attr('data-type');
		if (type.length == 0)
			type = "text";
		var show_error = false;
		switch(type){
			case "text":
				$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
					if ($(this).val().length == 0)
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.form-group').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.form-group').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "phone":
				$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
					if ($(this).mask().length == 0)
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.form-group').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.form-group').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "phone_simple":
				$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
					if ($(this).mask().length == 0)
						show_error = true;
				});
				if (show_error)
					$(this).closest('div.form-group').find('.fa-check').hide();
				else
					$(this).closest('div.form-group').find('.fa-check').show();
				break;
			case "number":
				$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
					if ($(this).val().length == 0 || ($(this).attr('maxlength')>0 && $(this).attr('maxlength')>$(this).val().length))
						show_error = true;
				});
				
				if (show_error)
				{
					$(this).closest('div.form-group').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.form-group').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
			case "email":
				$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
					if (!isValidEmailAddress($(this).val()))
						show_error = true;
				});
				if (show_error)
				{
					$(this).closest('div.form-group').find('.help-block-error').show();
					$(this).closest('div.form-group').find('.fa-check').hide();
				}
				else
				{
					$(this).closest('div.form-group').find('.help-block-error').hide();
					$(this).closest('div.form-group').find('.fa-check').show();
				}
				break;
		}
	});
	
	$('#register_form').submit(function(){
		var result = true;
	
		$('#person input.required').each(function(){
			var type = $(this).attr('data-type');
			if (type.length == 0)
				type = "text";
			var show_error = false;
			switch(type){
				case "text":
					$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
						if ($(this).val().length == 0)
							show_error = true;
					});
					if (show_error)
					{
						$(this).closest('div.form-group').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
						$(this).focus();
						result = false;
						return false;
					}
					break;
				case "phone":
					$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
						if ($(this).mask().length == 0)
							show_error = true;
					});
					if (show_error)
					{
						$(this).closest('div.form-group').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
						$(this).focus();
						result = false;
						return false;
					}
					break;
				case "phone_simple":
					$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
						if ($(this).mask().length == 0)
							show_error = true;
					});
					if (show_error)
					{
						$('#register-phone-required').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
						$(this).focus();
						result = false;
						return false;
					}
					break;
				case "number":
					$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
						if ($(this).val().length == 0 || ($(this).attr('maxlength')>0 && $(this).attr('maxlength')>$(this).val().length))
							show_error = true;
					});
					
					if (show_error)
					{
						$(this).closest('div.form-group').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
						$(this).focus();
						result = false;
						return false;
					}
					break;
				case "email":
					$(this).closest('div.form-group').find('input.required, textarea.required').each(function(){
						if (!isValidEmailAddress($(this).val()))
							show_error = true;
					});
					if (show_error)
					{
						$(this).closest('div.row').find('.help-block-error').show();
						$(this).closest('div.form-group').find('.fa-check').hide();
						$(this).focus();
						result = false;
						return false;
					}
					break;
			}
		});
	
		return result;
	});*/
</script>