{* Шаблон списка сопутствующих товаров и аналогов *}

{if $related_products|count > 0}

	<div id="sortview">
	<div class="inproduct-header">Сопутствующие товары</div>

	<div id="view">	
		<div class="view-style">
			<div class="btn-group">
				<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'list']}" class="btn btn-default btn-sm {if !$mode || $mode == 'list'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Списком"><i class="fa fa-bars"></i></a>
				<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'tile']}" class="btn btn-default btn-sm {if $mode == 'tile'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Плиткой"><i class="fa fa-th"></i></a>
			</div>
		</div>
		<div class="view-how">Отображать:</div>
	</div>

	</div>

	<!-- Стиль списка товаров фильтры и прочее начало -->
	{if !$mode || $mode == 'list'}
	<ul class="list">
		{foreach $related_products as $product}
			{include file='product-list.tpl'}
		{/foreach}
	</ul>
	{/if}

	{if $mode == 'tile'}
	<ul class="plitka">
		{$mh_group = 1}
		{foreach $related_products as $product}
			{include file='product-tile.tpl' mh_group=$mh_group}
			{if $product@iteration is div by 4}<div class="plitka-separator"></div>{$mh_group = $mh_group + 1}{/if}
		{/foreach}
	</ul>
	{/if}
	<!-- Стиль списка товаров фильтры и прочее  конец -->
{/if}

{if $analogs_products|count > 0}

	<div id="sortview">
	<div class="inproduct-header">Аналоги</div>
	<div id="view">	
		<div class="view-style">
			<div class="btn-group">
				<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'list']}" class="btn btn-default btn-sm {if !$mode || $mode == 'list'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Списком"><i class="fa fa-bars"></i></a>
				<a href="{$config->root_url}{$module->url}{$current_url}{url current_params=$current_params add=['mode'=>'tile']}" class="btn btn-default btn-sm {if $mode == 'tile'}active{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="Плиткой"><i class="fa fa-th"></i></a>
			</div>
		</div>
		<div class="view-how">Отображать:</div>
	</div>

	</div>

	<!-- Стиль списка товаров фильтры и прочее начало -->
	{if !$mode || $mode == 'list'}
	<ul class="list">
		{foreach $analogs_products as $product}
			{include file='product-list.tpl'}
		{/foreach}
	</ul>
	{/if}

	{if $mode == 'tile'}
	<ul class="plitka">
		{$mh_group = 1}
		{foreach $analogs_products as $product}
			{include file='product-tile.tpl' mh_group=$mh_group}
			{if $product@iteration is div by 4}<div class="plitka-separator"></div>{$mh_group = $mh_group + 1}{/if}
		{/foreach}
	</ul>
	{/if}
	<!-- Стиль списка товаров фильтры и прочее  конец -->
{/if}

<script type="text/javascript">
	$('#view a').click(function(){
		var href = $(this).attr('href');
		
		$('a[data-toggle="tooltip"]').each(function(){
			$(this).tooltip('hide');
		});

		$.get(href, function(data){
			$('#related').html(data);
			$('a[data-toggle="tooltip"]').tooltip({
				container: 'body'
			});
		});
		return false;
	});
	
	$(document).ready(function(){
		$('ul.plitka .plitka-name-block').matchHeight(1);
		$('ul.plitka .plitka-description').matchHeight(1);
	});
</script>