{if $reviews}
<div class="rate-header">
	<div class="rate-sort">

    <div class="rate-sort-item">Сортировать:</div><div class="rate-sort-item {if $params_arr['sort'] == "rank" || !array_key_exists('sort', $params_arr)}active{/if}" data-sort="rank">    
    	<a href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'rank']}" data-placement="bottom" data-toggle="tooltip" data-original-title="Сначала самые актуальные">по актуальности</a>
    </div><div class="rate-sort-item {if $params_arr['sort'] == "date"}active{/if}" data-sort="date">
    	<i class="fa fa-sort-amount-desc"></i> <a href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'date']}" data-placement="bottom" data-toggle="tooltip" data-original-title="Сначала новые">по дате</a>
    </div><div class="rate-sort-item {if $params_arr['sort'] == "rating"}active{/if}" data-sort="rating">
    	<i class="fa fa-sort-amount-desc"></i> <a href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'rating']}" data-placement="bottom" data-toggle="tooltip" data-original-title="Сначала полезные">по оценке</a>        
    </div>

    {* Всего {$reviews_count} {$reviews_count|plural:'отзыв':'отзывов':'отзыва'} *}
    </div>    
	<a class="btn btn-primary write-review" id="write-review2"><i class="fa fa-pencil"></i> Написать отзыв</a>
	{*<a class="btn btn-primary write-review hidden" id="hide-write-review"><i class="fa fa-pencil"></i> Скрыть форму написания отзыва</a>*}
</div>

{*<div id="add-review-top" class="hidden">
{include file='reviews-product-new-review.tpl' index=1}
</div>*}

<div class="alert alert-success" id="add-review-result-1" style="display:none;">Спасибо! Ваш отзыв опубликован.</div>

{* include file='pagination-frontend.tpl' *}

{foreach $reviews as $review}
	<div class="review-body" data-id="{$review->id}">
    
		<div class="review-title">
			<div class="review-autor">{$review->name}</div>
            <div class="review-date">{$review->day_str}</div>
            
            <div class="review-id">id: {$review->id}</div>
            
            <a href="#" class="review-complain">Пожаловаться</a>
            
			{if $smarty.session.admin && $smarty.session.admin == "admin"}<div class="review-id">Ранг: {$review->rank}</div>{/if}
        </div>
			
           
		
		<div class="review-body-text">
			<div class="review-topline">
				<div class="raiting rate{$review->avg_rating*10}" title="Рейтинг товара {$review->rating|string_format:"%.1f"}"></div>{if $review->short && $settings->reviews_short_visible} &mdash; {$review->short}{/if}
                </div>
			
			{if $review->pluses && $settings->reviews_pluses_visible}
			<div class="review-verdict">
				<div class="review-verdict-type">
					<div class="review-verdict-header"><i class="fa fa-plus-circle"  title="Достоинства"></i></div>
					<div class="review-verdict-text">
						{$review->pluses}
					</div>
				</div>
			</div>
			{/if}
			
			{if $review->minuses && $settings->reviews_minuses_visible}
				<div class="review-verdict">
					<div class="review-verdict-type">
						<div class="review-verdict-header"><i class="fa fa-minus-circle" title="Недостатки"></i></div>
						<div class="review-verdict-text">
							{$review->minuses}
						</div>
					</div>
				</div>
			{/if}
			
			{if $review->comments}
            <div class="review-verdict">
				<div class="review-verdict-type">
						<div class="review-verdict-header review-comment"><i class="fa fa-comment-o"  title="Комментарий"></i></div>
						<div class="review-verdict-text">
					 		{$review->comments} 
                            
                            {* Максимум 5 фото *}
							{if $review->images && $settings->reviews_images_visible}
								<ul class="review-images">
								{foreach $review->images as $img}
									<li><a href="{$img->filename|resize:'reviews':1000:1000}" class="fancybox" rel="group_review{$review->id}"><img src="{$img->filename|resize:'reviews':62:62}"></a></li>
								{/foreach}
								</ul>
							{/if}
                        </div>
                        
					</div>
				</div>
			{/if}
			
			{if $review->recommended && $settings->reviews_recommended_visible}
			<div class="irecommend"><i class="fa fa-check"></i>Да - я рекомендую этот товар</div>
			{/if}
			
			<div class="usergrade">Отзыв полезен? <span class="usergrade-yes">Да</span> <span class="usergrade-numbers">{$review->helpful}</span> / <span class="usergrade-no">Нет</span> <span class="usergrade-numbers">{$review->nothelpful}</span> </div>
        </div>
	</div>
{/foreach}

{include file='pagination-frontend.tpl'}
{else}
<div class="non-reviews">Пока не было отзывов. Станьте первым!</div>
{/if}

<div class="alert alert-success" id="add-review-result-2" style="display:none;">{if $settings->reviews_premoderate}Спасибо! Ваш отзыв появится на сайте через некоторое время.{else}Спасибо! Ваш отзыв опубликован.{/if}</div>

<div id="add-review-bottom">
{include file='reviews-product-new-review.tpl' index=2}
</div>

<script type="text/javascript">

	var imgList1 = $('#img-list-1');
	var imgList2 = $('#img-list-2');
	var fileInput1 = $('#uploadImageField-1');
	var fileInput2 = $('#uploadImageField-2');
	
	$(document).ready(function(){
		fileInput1.damnUploader({
			url: '{$config->root_url}{$module->url}{url add=['review-images'=>1, 'mode'=>'upload-images']}',
			fieldName:  'uploaded-images',
			object_id: '{$temp_id}',
			limit: 5,
			onSelect: function(file) {
				$('#upload-anim-1').removeClass('hidden');
				var addedItem = addFileToQueue1(file);
				fileInput1.damnUploader('startUploadItem', addedItem.queueId);
				return false;
			},
			onAllComplete: function(){
				$('#upload-anim-1').addClass('hidden');
				
				fileInput1.damnUploader('cancelAll');
				imgList1.html('');
			
				href = '{$config->root_url}{$module->url}{url add=['review-images'=>1, 'mode'=>'get-images', 'temp_id'=>$temp_id]}';
				$.get(href, function(data) {
					if (data.success)
					{
						$('div.my-review div.object-images').html(data.data);
					}
				});
				return false;
			},
		});
		fileInput2.damnUploader({
			url: '{$config->root_url}{$module->url}{url add=['review-images'=>1, 'mode'=>'upload-images']}',
			fieldName:  'uploaded-images',
			object_id: '{$temp_id}',
			limit: 5,
			onSelect: function(file) {
				$('#upload-anim-2').removeClass('hidden');
				var addedItem = addFileToQueue2(file);
				fileInput2.damnUploader('startUploadItem', addedItem.queueId);
				return false;
			},
			onAllComplete: function(){
				$('#upload-anim-2').addClass('hidden');
			
				fileInput2.damnUploader('cancelAll');
				imgList2.html('');
			
				href = '{$config->root_url}{$module->url}{url add=['review-images'=>1, 'mode'=>'get-images', 'temp_id'=>$temp_id]}';
				$.get(href, function(data) {
					if (data.success)
					{
						$('div.my-review div.object-images').html(data.data);
					}
				});
				return false;
			},
		});
		
		$(".fancybox").fancybox({
        		prevEffect	: 'none',
				nextEffect	: 'none',
				helpers	: {
					title	: {
						type: 'outside'
					},
					thumbs	: {
						width	: 50,
						height	: 50
					},
					overlay : {
         			   locked     : false
			        }
				}
		});
	});
	
	function updateProgress(bar, value) {
		bar.css('width', value+'%');
    }
	
	function addFileToQueue1(file) {

        // Создаем элемент li и помещаем в него название, миниатюру и progress bar
        var li = $('<li/>').appendTo(imgList1);
        var title = $('<div/>').text(file.name+' ').appendTo(li);
        var cancelButton = $('<a/>').attr({
            href: '#cancel',
            title: 'отменить'
        }).text('X').appendTo(title);

        // Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
        // обозначающий, что переданный параметр на самом деле лишь имитация настоящего File)
        if(!file.fake)
		{
            // Отсеиваем не картинки
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {
                return true;
            }

            // Добавляем картинку и прогрессбар в текущий элемент списка
            var img = $('<img/>').appendTo(li);
			
			var pBar = $('<div class="progress progress-striped active"></div>').appendTo(li);
			var ppBar = $('<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>').appendTo(pBar);

            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах (только в браузерах, поддерживающих FileReader)
            if($.support.fileReading) {
                var reader = new FileReader();
                reader.onload = (function(aImg) {
                    return function(e) {
                        aImg.attr('src', e.target.result);
                        aImg.attr('width', 150);
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }

        // Создаем объект загрузки
        var uploadItem = {
            file: file,
			queueId: 0,
            onProgress: function(percents) {
                updateProgress(ppBar, percents);
            },
			onComplete: function(successfully, data, errorCode) {
				pBar.removeClass('active');
			},
        };

        // помещаем его в очередь
        var queueId = fileInput1.damnUploader('addItem', uploadItem);
		uploadItem.queueId = queueId;

        // обработчик нажатия ссылки "отмена"
        cancelButton.click(function() {
            fileInput1.damnUploader('cancel', queueId);
            li.remove();
            return false;
        });

        return uploadItem;
    }
	
	function addFileToQueue2(file) {

        // Создаем элемент li и помещаем в него название, миниатюру и progress bar
        var li = $('<li/>').appendTo(imgList2);
        var title = $('<div/>').text(file.name+' ').appendTo(li);
        var cancelButton = $('<a/>').attr({
            href: '#cancel',
            title: 'отменить'
        }).text('X').appendTo(title);

        // Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
        // обозначающий, что переданный параметр на самом деле лишь имитация настоящего File)
        if(!file.fake)
		{
            // Отсеиваем не картинки
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {
                return true;
            }

            // Добавляем картинку и прогрессбар в текущий элемент списка
            var img = $('<img/>').appendTo(li);
			
			var pBar = $('<div class="progress progress-striped active"></div>').appendTo(li);
			var ppBar = $('<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>').appendTo(pBar);

            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах (только в браузерах, поддерживающих FileReader)
            if($.support.fileReading) {
                var reader = new FileReader();
                reader.onload = (function(aImg) {
                    return function(e) {
                        aImg.attr('src', e.target.result);
                        aImg.attr('width', 150);
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }

        // Создаем объект загрузки
        var uploadItem = {
            file: file,
			queueId: 0,
            onProgress: function(percents) {
                updateProgress(ppBar, percents);
            },
			onComplete: function(successfully, data, errorCode) {
				pBar.removeClass('active');
			},
        };

        // помещаем его в очередь
        var queueId = fileInput2.damnUploader('addItem', uploadItem);
		uploadItem.queueId = queueId;

        // обработчик нажатия ссылки "отмена"
        cancelButton.click(function() {
            fileInput2.damnUploader('cancel', queueId);
            li.remove();
            return false;
        });

        return uploadItem;
    }
	
	$('div.object-images').on("click", "a.delete_image", function(){
		var item = $(this).closest('li');
		var href = $(this).attr('href');
		$.get(href, function(data) {
			if (data)
			{
				item.fadeOut('fast').remove();
			}
		});
		return false;
	});

	/*$('#write-review').click(function(){
		$(this).addClass('hidden');
		$('#hide-write-review').removeClass('hidden');
		$('#add-review-top').removeClass('hidden');
		return false;
	});
	
	$('#hide-write-review').click(function(){
		$(this).addClass('hidden');
		$('#write-review').removeClass('hidden');
		$('#add-review-top').addClass('hidden');
		return false;
	});*/
	$('#write-review2').click(function(){
		$('html, body').animate({
			scrollTop: $("#add-review-bottom").offset().top
		}, 0);
	});
</script>