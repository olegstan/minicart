{* Заказ звонка *}

<!-- Modal -->
<div class="modal" id="contact-center" tabindex="-1" role="dialog" aria-labelledby="callmemodal" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Заказ звонка</h4>
      </div>
      <div class="modal-body">
		<div id="callback-part">
			<form id="callback-form" method="post" action="{$config->root_url}{$main_module->url}">
			<div class="form-group">
				<label>Телефон *</label>
				<input type="tel" name="phone_number" class="form-control" required autocomplete="off"/>
                <div class="help-block-error" id="contact-center-phone-required" style="display:none;">Пожалуйста укажите номер телефона</div>       
            </div>
            <div class="form-group">
				<label>Ваше имя</label>
				<input type="text" name="user_name" id="user_name" class="form-control" autocomplete="off"/>				
			</div>
			 <div class="form-group">
				<label>Удобное время для звонка</label>
				<input type="text" name="call_time" id="" class="form-control" autocomplete="off"/>				
			</div>            
			<div class="form-group">
			<label>Сообщение</label>
			<textarea class="form-control" rows="2" name="message"></textarea>
			<p class="help-block">Менеджер перезвонит Вам в удобное для Вас время</p>
			</div>
			<input type="hidden" name="variant_id" value="0"/>
			<input type="hidden" name="oneclickbuy" value="1"/>
			</form>
		</div>
		<div id="callback-success-part" style="display: none;">
			<div class="alert alert-success">
				<strong>Ваш заказ звонка принят!</strong><br/>Менеджер свяжется с Вами в удобное для Вас время.
			</div>
		</div>
		<div id="callback-error-part" style="display: none;">
			<div class="alert alert-danger">
				<strong>Произошла ошибка при заказе звонка!</strong>
			</div>
		</div>
      </div>
	<div class="modal-footer">
		<a href="#" class="btn btn-success" id="send-callback">Заказать</a>
		<button id="callback-close" type="button" class="btn btn-default" data-dismiss="modal" style="display: none;">Закрыть</button>
	</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{* Заказ в 1 клик *}
<!-- Modal -->
<div class="modal" id="buy1click" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Заказ в 1 клик</h4>
      </div>
      <div class="modal-body">
		<form id="order-one-click-form" method="post" action="{$config->root_url}{$cart_module->url}">
		<div class="cart-body">
		</div>
		<div id="not-enough-block" class="warning-blue"> Минимальная сумма заказа <strong>{$settings->cart_order_min_price} {$main_currency->sign}</strong></div>
		<div class="buy1click-user-info">
            <div class="form-group">
				<div class="row">
					<div class="col-xs-2 buy1click-phone-block">
						<label>Телефон</label>
					</div>
					<div class="col-xs-4 buy1click-enterphone">
						<input type="tel" name="phone_number" class="form-control buy1click-phone required value="{if $user->phone_code && $user->phone}+7 ({$user->phone_code}) {$user->phone|phone_mask}{/if}"/>
						<div class="help-block-error" id="buy1click-phone-required" style="display:none;">Пожалуйста, укажите номер телефона</div>        
					</div>
					<div class="col-xs-6">
					<p class="help-block">Менеджер перезвонит Вам, узнает все детали и сам оформит заказ на ваше имя.</p>
					</div>
				</div>
				
				{if $settings->cart_one_click_show_comment}
				<div class="form-group">
					<div class="row">
						<div class="col-xs-2 buy1click-phone-block">
							<label>Комментарий</label>
						</div>
						<div class="col-xs-4 buy1click-enterphone">
						   <textarea class="form-control" rows="1" name="comment" data-type="text"></textarea>       
						</div>
						<div class="col-xs-6">
						<p class="help-block">Например, удобное время для звонка или пожелания к заказу</p>
						</div>
					</div>
				</div>
				{/if}
			</div>
			<input type="hidden" name="variant_id" value="0"/>
			<input type="hidden" name="oneclickbuy" value="1"/>
		</div>
		</form>
      </div>
	<div class="modal-footer">
		<a href="#" class="btn btn-success btn-lg w100" id="make-order-one-click">Оформить заказ <i class="fa fa-spin fa-spinner hidden" id="save_order_1click_spinner"></i></a>
	</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal" id="complain-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md modal-complain">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Укажите причину жалобы</h4>
      </div>
      <div class="modal-body">
		<div id="claim-part">
			<div class="form-group">
			  <div class="radio">
				  <label>
					<input type="radio" name="claim_type" id="optionsRadios1" value="profanity" checked>
					Ненормативная лексика
				  </label>
				</div>
				<div class="radio">
				  <label>
					<input type="radio" name="claim_type" id="optionsRadios2" value="spam">
					Рассылка рекламной информации (спам)
				  </label>              
				</div>
				<div class="radio">
				  <label>
					<input type="radio" name="claim_type" id="optionsRadios3" value="foul">
					Нарушение правил сайта
				  </label>              
				</div>
				<div class="radio">
				  <label>
					<input type="radio" name="claim_type" id="optionsRadios4" value="other">
					Другая причина (укажите ниже)
				  </label>              
				</div>
			</div>
			<div class="form-group">
				<textarea class="form-control" rows="2" name="claim_text" disabled></textarea>
			</div>
        </div>
      
		<div id="claim-success-part" style="display: none;">
			<div class="alert alert-success">
				<strong>Ваш жалоба принята!</strong>
			</div>
		</div>
		
		<div id="claim-error-part" style="display: none;">
			<div class="alert alert-danger">
				<strong>Вы уже пожаловались на этот отзыв.</strong>
			</div>
		</div>
	  </div>
	<div class="modal-footer">
		<input type="hidden" name="claim_review_id" value=""/>
		<a href="#" class="btn btn-success w100" id="make-claim">Отправить жалобу</a>
		<button id="claim-close" type="button" class="btn btn-default" data-dismiss="modal" style="display: none;">Закрыть</button>
	</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{* Форма после добавления товара в корзину *}
<!-- Modal -->
<div class="modal fade" id="after-buy-form" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
      	<span class="modal-title">Товар добавлен в козину</span>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel"></h4>
      </div>
      <div class="modal-body">
      </div>
	<div class="modal-footer">
    	<div class="row">
			<div class="col-xs-6">
            	<button id="after-buy-form-close-continue" type="button" class="btn btn-default w100" data-dismiss="modal">Продолжить покупки</button>
    		</div>            
            <div class="col-xs-6">
                <a href="cart/" class="btn btn-success w100" id="after-buy-form-make-order">Оформить заказ</a>            
            </div>
          </div>  
		
		
	</div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
	$(function(){
		$("#buy1click input[name=phone_number], #contact-center input[name=phone_number]").mask("+7 (999) 999-99-99");
		
		$('textarea[name=comment]').autosize();
	});
	
	$('#make-order-one-click').click(function(){
		if ($('#buy1click input[name=phone_number]').mask().length == 0)
		{
			$('#buy1click input[name=phone_number]').focus();
			$('#buy1click-phone-required').show();
			return false;
		}
		$('#make-order-one-click').addClass('disabled');
		$('#save_order_1click_spinner').removeClass('hidden');
		$('#order-one-click-form').submit();
		return false;
	});
	
	$('#order-one-click-form').submit(function(){
		if ($('#buy1click input[name=phone_number]').mask().length == 0)
		{
			$('#buy1click input[name=phone_number]').focus();
			$('#buy1click-phone-required').show();
			return false;
		}
	});
	
	$('#buy1click input[name=phone_number]').keypress(function(){
		$('#buy1click-phone-required').hide();
	});
	
	$('#send-callback').click(function(){
		if ($('#contact-center input[name=phone_number]').mask().length == 0)
		{
			$('#contact-center input[name=phone_number]').focus();
			$('#contact-center-phone-required').show();
			return false;
		}
		$('#callback-form').submit();
		return false;
	});
	
	$('#callback-form').submit(function(){
		if ($('#contact-center input[name=phone_number]').mask().length == 0)
		{
			$('#contact-center input[name=phone_number]').focus();
			$('#contact-center-phone-required').show();
			return false;
		}
		
		var phone_field = encodeURIComponent($('#contact-center input[name=phone_number]').val());
		var user_name = encodeURIComponent($('#contact-center input[name=user_name]').val());
		var call_time_field = encodeURIComponent($('#contact-center input[name=call_time]').val());
		var message_field = encodeURIComponent($('#contact-center textarea[name=message]').val());
		
		var url = "{$config->root_url}{$main_module->url}{url add=['callback'=>1]}&phone_number="+phone_field+"&call_time=" + call_time_field + "&message=" + message_field + "&user_name=" + user_name;
		$.ajax({
			type: 'GET',
			url: url,
			success: function(data) {
				$('#callback-part').hide();
				$('#send-callback').hide();
				$('#callback-close').show();
				if (data)
					$('#callback-success-part').show();
				else
					$('#callback-error-part').show();
			}
		});
		return false;
	});
	
	$('#complain-form .modal-body input[type=radio]').change(function(){
		var v = $(this).val();
		if (v == 'other')
			$('#complain-form .modal-body textarea').prop('disabled', false);
		else
			$('#complain-form .modal-body textarea').prop('disabled', true);
	});
	
	$('#make-claim').click(function(){
		if ($('#complain-form .modal-body input[type=radio]:checked').val() != 'other')
			$('#complain-form .modal-body textarea').val('');
		$.ajax({
			url: '{$config->root_url}{$product_module->url}',
			type: 'POST',
			data: {
				review_id: $('#complain-form input[name=claim_review_id]').val(),
				claim_type: $('#complain-form .modal-body input[type=radio]:checked').val(),
				claim_text: $('#complain-form .modal-body textarea').val()
			},
			dataType: 'json',
			success: function(data){
				$('#claim-part').hide();
				$('#make-claim').hide();
				$('#claim-close').show();
				if (data.success)
					$('#claim-success-part').show();
				else
					$('#claim-error-part').show();
			}
		});
		return false;
	});
</script>