<!DOCTYPE html>
{*
	Общий вид страницы
	Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>
    <base href="{$core->root}/"/>
    <title>{$core->title}</title>

    {* Метатеги *}
    <meta name="description" content="{$core->description}"/>
    <meta name="keywords" content="{$core->meta_keywords}"/>
    <link rel="shortcut icon" href="{$path_frontend_template}/img/favicon.ico">
       
    
    {* Подключаем jquery последнюю версию из библиотек Code.jquery.com, Яндекса или localhost *}
    {*<script src="http://yandex.st/jquery/2.0.3/jquery.min.js"></script>*}
    <script src="{$front_vendor}jquery/dist/jquery.min.js"></script>
	{*<script src="{$path_frontend_template}/js/jquery-ui-1.10.4.custom.min.js"></script>*}
	<script src="{$front_vendor}jquery-ui/jquery-ui.min.js"></script>

	{*Если используется покупка с переменным количеством то подключаем слайдер для карточки товара*}
	{if $settings->catalog_use_variable_amount}
	<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/flick/jquery-ui.min.css">
	<script src="{$front_vendor}jquery-ui-slider-pips/dist/jquery-ui-slider-pips.min.js"></script>
	{/if}

    {* Подключаем бустрап 3 *}
    <script src="{$front_vendor}bootstrap/dist/js/bootstrap.min.js"></script>

    {* Подключаем бустрап 3*}
    <link href="{$front_vendor}bootstrap/dist/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>

    {* Подключаем Font Awesome *}
    <link rel="stylesheet" href="{$front_vendor}components-font-awesome/css/font-awesome.min.css">

    {* Подключаем Fancybox *}
    <script type="text/javascript" src="{$front_vendor}fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <link href="{$front_vendor}fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$front_vendor}fancybox/source/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" href="{$front_vendor}fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="{$front_vendor}fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <link rel="stylesheet" href="{$front_vendor}fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen"/>
    <script type="text/javascript" src="{$front_vendor}fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
    {* /Подключаем Fancybox *}

    {* Подключаем ajax-заливку файлов *}
    <script src="{$path_frontend_template}/js/jquery.damnUploader.min.js"></script>

    {* Подключаем css *}
    <link href="{$path_frontend_template}/css/theme.css" rel="stylesheet"/>

    {* Подключаем jquery.quicksand *}
    {*<script src="http://code.jquery.com/jquery-migrate-1.0.0.js"> </script>*}
    <script src="{$front_vendor}/jquery.maskedinput/jquery.quicksand.js"></script>

    {* Подключаем jQuery Masked Input *}
    <script src="{$front_vendor}jquery.maskedinput/dist/jquery.maskedinput.min.js" type="text/javascript"></script>

    {* Подключаем AutoSave для корзины*}
    {*<script src="{$path_frontend_template}/js/autosaveform.js" type="text/javascript"></script>*}
    <script src="{$front_vendor}jquery.cookie/jquery.cookie.js" type="text/javascript"></script>
    <script src="{$path_frontend_template}/js/sayt.min.jquery.js" type="text/javascript"></script>

    {* Подключаем qTip для тултипов *}
    <link type="text/css" rel="stylesheet" href="{$front_vendor}/css/jquery.qtip.min.css"/>
    <script src="{$front_vendor}/js/jquery.qtip.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="{$front_vendor}imagesloaded/imagesloaded.pkgd.min.js"></script>

    {* Подключаем ion.rangeSlider *}
    <script src="{$path_frontend_template}/js/rangeSlider.js" type="text/javascript"></script>

    {if $settings->catalog_count_opened_level > 0}
        {$nested_level = ""}
        {section name=nested start=0 loop=$settings->catalog_count_opened_level step=1}
            {$nested_level = $nested_level|cat:"> ol > li:not(.collapsed) "}
        {/section}
        {*assign var="nested_level" value="> ol > li:not(.collapsed)"*}
    {/if}

    {* Подключаем Javascript функции для обработки действий пользователя и загрузки контента *}
    <script type="text/javascript">
        {* Системные функции *}
        {*include file='../js/system_functions.js' nocache*}
        {* Функции загрузки контента *}
        {*include file='../js/content_functions.js' nocache*}
        {* Функции обработки действий пользователя *}
        {*include file='../js/actions_functions.js' nocache*}
    </script>

    {* Подключаем авторесайз для textarea *}
    <script src="{$front_vendor}jquery-autosize/dist/autosize.min.js"></script>

    {* Подключаем улучшалку отображения для старых браузеров *}
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    {* Подключаем компонент для выставления рейтинга *}
    <script src="{$path_frontend_template}/js/jquery.rating-2.0.js"></script>
	
	{* Подключаем компонент для выравнивания высоты у плиточных списков *}
	<script src="{$front_vendor}matchHeight/js/jquery.matchHeight-min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('body').append('<div id="scroller" class="b-top" style="display: none;"><span class="b-top-but">наверх</span></div>');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 0)
                    $('#scroller').fadeIn();
                else
                    $('#scroller').fadeOut();
            });
            $('#scroller').click(function () {
                $('body,html').animate({ scrollTop: 0 }, 400);
                return false;
            });
        });
    </script>
    
    
    {* Подключаем JS *}
    <script src="{$front_vendor}swiper/dist/js/swiper.min.js"></script>
    
    {* Подключаем Swiper's CSS *}
    <link rel="stylesheet" href="{$path_frontend_template}/css/swiper.css">
    
	{* Подключаем Google reCAPTCHA *}
	{if $settings->google_recaptcha_enabled}
	<script src='https://www.google.com/recaptcha/api.js'></script>
	{/if}
</head>

<body data-spy="scroll" data-target=".subnav" data-offset="50" class="{$body_category_css} {$body_product_css}">

{*<script src="http://ulogin.ru/js/ulogin.js"></script>
<div id="uLogin" data-ulogin="display=panel;fields=first_name,last_name;optional=email,bdate,sex,photo,photo_big,city,country,phone;providers=vkontakte,odnoklassniki,mailru,facebook;hidden=other;redirect_uri=http%3A%2F%2Fminicart.su%2Flogin%2F"></div>*}

{if $user}
<script> dataLayer = [{ '&uid': '{$user->id}' }] </script>
{/if}

{if $smarty.session.admin && $smarty.session.admin == "admin"}
    {include file='../parts/admin-header.tpl'}
{/if}


<div id="wrap">
<!-- Begin page content -->

<div class="topline">
    <div class="container nopadding">
        <div class="row topline-links">                     
            <div class="col-xs-9">
             <nav class="navbar navbar-default" role="navigation" id="topmenu">
    			{*menu id=1 menu_type='horizontal'*}
			</nav>  
            </div>

           

            <div class="col-xs-3">            
                <ul class="nav navbar-nav navbar-right">
        			<li class="dropdown cart-dropdown" id="cart-placeholder">
            			{*include file='cart-informer.tpl'*}
        			</li>
    			</ul>       
            </div>       
    </div>
</div>
</div>

<div class="container">
<header>
    <div class="row">
        <div class="col-xs-3">
            <a href="/" class="logo"></a>
        </div>
        <div class="col-xs-6">
        
        <div class="row toplinks">
        <div class="col-xs-6">
        
        <ul>
                    {if !$user}
                        <li class="login-register-inline"><a href="{*$config->root_url*}{$login_module->url}">Войти с паролем</a></li>
                        <li class="login-register-inline"> | <a href="{*$config->root_url*}{$register_module->url}">Регистрация</a>
                        </li>
                    {else}
                        {* Этот блок показываем когда клиент залогинился *}
                        <li class="login-register-inline">
                            <a href="{*$config->root_url*}{$account_module->url}">Личный кабинет</a>
                        </li>
                        |
                        <li class="login-register-inline">
                            <i class="fa fa-power-off"></i> <a
                                    href="{*$config->root_url*}{$login_module->url}{*url add=['logout'=>1]*}">Выход</a>
                        </li>
                    {/if}            
                     
                    
                    
                </ul>
        	</div>
            
            <div class="col-xs-6 taright">
                <div class="compare-body" {if !$compare_href_show}style="display:none;"{/if}>
                    <i class="fa fa-bars"></i> <a href="{$compare_href}" id="make-compare">Сравнить ({$compare_items|count})</a>
                </div>
           
                <div class="favorite-products" {if !$user || $favorites_products_count == 0}style="display: none;"{/if}>
					<i class="fa fa-heart"></i> <a href="{*$config->root_url*}{$account_module->url}{*url add=['mode'=>'my-favorites']*}" id="favorites-informer">Избранное ({$favorites_products_count})</a>
                </div>
            </div>
            
            
        	</div>
            <div id="search">
                <form role="search">
                    <button id="searchCancelButton" class="searchclear" type="button serchremove" {if !$keyword} style="display:none;"{/if}><i class="fa fa-times"></i></button>
                    <input id="searchField" type="text" class="form-control" placeholder="{$settings->search_placeholder}" autocomplete="off" value="{$keyword}">
                    <button id="searchButton" type="button" class="btn btn-default">Найти</button>
                </form>
                <div id="refreshpart" class="addcontainer" style="display:none;"></div>
                
                {* Для шаблона Modern скрываем поисковые подсазки, поскольу нужноместо для Личного кабинета *}
                {*
                {if $settings->search_help_text1 || $settings->search_help_text2}
                    <div class="search-helper">
                        Например, {if $settings->search_help_text1}<span id="help_msg1">{$settings->search_help_text1}</span>{/if} {if $settings->search_help_text1 && $settings->search_help_text2}или{/if} {if $settings->search_help_text2} <span id="help_msg2">{$settings->search_help_text2}</span>{/if}
                    </div>
                {/if}
                *}
            </div>
        </div>
        <div class="col-xs-3">
            {*if {banner id=2}}
                <div class="phone">
                    <div class="number">{banner id=2}</div>
                    <span>
                    {if {banner id=12}}
                    {banner id=12}
                    {/if} | <i class="fa fa-phone"></i> <a href="#" id="make-callback">Перезвонить?</a></span>
                </div>
            {/if*}
        </div>
    </div>
</header>

{* Горизонтальное меню каталога товаров начало *}

<nav class="navbar white-navbar" role="navigation">
	<ul class="nav navbar-nav white-menu" id="categories-menu">
	{function name=categories_tree_menu level=0}
			{foreach $items as $item}
				{if $item->is_visible}
					{if $item->subcategories}
						<li class="menu-item dropdown level{$level} {if $level>0}dropdown-submenu{/if} {if $item->id == $category_id}active{/if}">
							<a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}{if $level == 0}<b class="caret"></b>{/if}</a>
							<ul class="dropdown-menu dropdown{$level} {$item->css_class}">
							{categories_tree_menu items=$item->subcategories level=$level+1}
							</ul>
						</li>
					{else}
						<li class="menu-item level{$level} {if $item->id == $category_id}active{/if}"><a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}</a></li>
					{/if}
				{/if}
			{/foreach}
	{/function}
	{categories_tree_menu items=$categories_frontend_all level=0}
	</ul>
</nav>

{* Горизонтальное меню каталога товаров конец *}


{********************* Мегаменю *************************}
{*<nav class="navbar navbar-default navbar-static horizontal-navbar">*}
<nav class="navbar navbar-default navbar-static horizontal-navbar" role="navigation">
<div class="collapse navbar-collapse js-navbar-collapse">
<ul class="nav navbar-nav" id="categories-menu-columns">
	{foreach $categories_frontend_all as $cat_level1}
		{if $cat_level1->is_visible}
		<li class="dropdown dropdown-large {if $cat_level1->id == $category_id}active{/if}">
			{if empty($cat_level1->subcategories)}
				<a class="menu{$cat_level1->id} menu-level0" href="{*$config->root_url*}{$products_module->url}{$cat_level1->url}/" data-type="href"><span>{$cat_level1->name}</span></a>
			{else}
			
				<a href="{*$config->root_url*}{$products_module->url}{$cat_level1->url}/" class="dropdown-toggle menu{$cat_level1->id} menu-level0" data-type="href"><span>{$cat_level1->name}</span> <b class="caret"></b></a>
			
				{* ЕСЛИ ПЕРВЫЙ УРОВЕНЬ МЕНЮ ПРОСТОЙ *}
				{if $cat_level1->menu_level1_type == 1}
					<ul class="dropdown-menu dropdown0 {$cat_level1->css_class} simple-menu" id="categories-menu">
					{function name=categories_tree_menu level=0}
							{foreach $items as $item}
								{if $item->is_visible}
									{if $item->subcategories}
										<li class="menu-item dropdown level{$level} {if $level>0}dropdown-submenu{/if} {if $item->id == $category_id}active{/if}">
											<a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}" >{$item->name}{if $level == 0}{/if}</a>
											<ul class="dropdown-menu dropdown{$level} {$item->css_class}">
											{categories_tree_menu items=$item->subcategories level=$level+1}
											</ul>
										</li>
									{else}
										<li class="menu-item level{$level} {if $item->id == $category_id}active{/if}"><a href="{*$config->root_url*}{$products_module->url}{$item->url}/" data-type="href" class="menulevel{$level}">{$item->name}</a></li>
									{/if}
								{/if}
							{/foreach}
					{/function}
					{categories_tree_menu items=$cat_level1->subcategories level=0}
					</ul>
				{/if}
				
				{if $cat_level1->menu_level1_type == 3}
					<ul class="dropdown-menu dropdown-menu-large row">
						{$levels = []}
						{section name=levels start=1 loop=$cat_level1->menu_level1_columns+1 step=1}
							{$levels[] = $smarty.section.levels.index}
						{/section}
						{foreach $levels as $level}
						
							<li class="col-xs-3">
								<ul>
									{foreach $cat_level1->subcategories as $cat_level2}
										{if $cat_level2->is_visible && $cat_level2->menu_level2_column == $level}
											{if empty($cat_level2->subcategories)}
												<li {if $cat_level2->id == $category_id}class="active"{/if}><a class="menu{$cat_level2->id} menu-level1" href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" data-type="href" {if $cat_level2->id == $category->id}class="active"{/if}>{$cat_level2->name}</a></li>
											{else}
												<li class="have-childs {if $cat_level2->id == $category_id}active{/if}">
												<a href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" class="menu{$cat_level2->id} menu-level1 {if $cat_level2->id == $category->id}active{/if}" data-type="href">{$cat_level2->name}</a>
												<ul>
												{foreach $cat_level2->subcategories as $cat_level3}
													{if $cat_level3->is_visible}
													<li {if $cat_level3->id == $category_id}class="active"{/if}><a href="{*$config->root_url*}{$products_module->url}{$cat_level3->url}/" class="menu{$cat_level3->id} menu-level2 {if $cat_level3->id == $category->id}active{/if}" data-type="href">{$cat_level3->name}</a></li>
													{/if}
												{/foreach}
												</ul>
												</li>
												{******* РАЗДЕЛИТЕЛЬ *******}
												{*<li class="divider"></li>*}
											{/if}
										{/if}
									{/foreach}
								</ul>
							</li>                        
						{/foreach}
					</ul>
				{/if}
				
				{* ЕСЛИ ПЕРВЫЙ УРОВЕНЬ МЕГАМЕНЮ *}
				{if $cat_level1->menu_level1_type == 2}
					{* мегаменю *}
					
					{* $cat_level1->menu_level1_columns - На сколько колонок выпадает меню *}
					
					{* На какую ширину выпадает меню *}
					{if $cat_level1->menu_level1_width == 1}
						{* На ширину колонок *}
					{elseif $cat_level1->menu_level1_width == 2}
						{* На всю ширину *}
					{/if}
					
					{*Куда ориентировано меню*}
					{if $cat_level1->menu_level1_align == 1}						
					{elseif $cat_level1->menu_level1_align == 2}						
					{/if}
					
					{*Использовать рекламный баннер для этого пункта меню*}
					{if $category->menu_level1_use_banner}
						{* Да, использовать *}
						{* $category->menu_level1_banner_id - код баннера *}
					{/if}
					
					<ul class="dropdown-menu dropdown-menu-large menu-columns menu-columns-{$cat_level1->menu_level1_columns} menu-width-{$cat_level1->menu_level1_width} {if $category->menu_level1_use_banner}have-banner{/if} {if $cat_level1->menu_level1_align == 1} menu-align-right {elseif $cat_level1->menu_level1_align == 2} menu-align-left {/if}">
                    
                    {if $cat_level1->menu_level1_width == 2}
                    	<div str-row class="margin--7">
                    {/if}
						{$levels = []}
						{section name=levels start=1 loop=$cat_level1->menu_level1_columns+1 step=1}
							{$levels[] = $smarty.section.levels.index}
						{/section}
						{foreach $levels as $level}
							{$column_width = 1}
							{if $level == 1}{$column_width = $cat_level1->menu_level1_column1_width}{/if}
							{if $level == 2}{$column_width = $cat_level1->menu_level1_column2_width}{/if}
							{if $level == 3}{$column_width = $cat_level1->menu_level1_column3_width}{/if}
							{if $level == 4}{$column_width = $cat_level1->menu_level1_column4_width}{/if}
							{if $level == 5}{$column_width = $cat_level1->menu_level1_column5_width}{/if}
							{if $level == 6}{$column_width = $cat_level1->menu_level1_column6_width}{/if}
							<li
                            {if $cat_level1->menu_level1_width == 2}
                            str-xs str-xs-flex="{$column_width}" class="menu-column-width"
                            {else}
                            class="menu-column-width columns-width-standart"
                            {/if}>
								<ul>
									{foreach $cat_level1->subcategories as $cat_level2}
										{if $cat_level2->is_visible && $cat_level2->menu_level2_column == $level}
											{if empty($cat_level2->subcategories)}
												<li {if $cat_level2->id == $category_id}class="active"{/if}><a class="menu{$cat_level2->id} menu-level1" href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" data-type="href" {if $cat_level2->id == $category->id}class="active"{/if}>{$cat_level2->name}</a></li>
											{else}
												<li class="have-childs {if $cat_level2->id == $category_id}active{/if}">
												<a href="{*$config->root_url*}{$products_module->url}{$cat_level2->url}/" class="menu{$cat_level2->id} menu-level1 {if $cat_level2->id == $category->id}active{/if}" data-type="href">{$cat_level2->name}</a>
												<ul class="submenu-columns-{$cat_level2->menu_level2_columns}">
												{foreach $cat_level2->subcategories as $cat_level3}
													{if $cat_level3->is_visible}
													<li {if $cat_level3->id == $category_id}class="active"{/if}><a href="{*$config->root_url*}{$products_module->url}{$cat_level3->url}/" class="menu{$cat_level3->id} menu-level2 {if $cat_level3->id == $category->id}active{/if}" data-type="href">{$cat_level3->name}</a></li>
													{/if}
												{/foreach}
												</ul>
												</li>
												{******* РАЗДЕЛИТЕЛЬ *******}
												{*<li class="divider"></li>*}
											{/if}
										{/if}
									{/foreach}
								</ul>
							</li>                        
						{/foreach}
						
						{if $cat_level1->menu_level1_use_banner}
                        	<div class="wide-menu-block">
							{*banner id=$cat_level1->menu_level1_banner_id*}
                            </div>
						{/if}
                        
                     {if $cat_level1->menu_level1_width == 2}
                    	</div>
                    {/if}
					</ul>
				{/if}
			{/if}
		</li>
		{/if}
	{/foreach}   
</ul>


{* Бренды начало *}

    <ul class="nav navbar-nav brandsmenu" id="categories-menu-columns">
    <li class="dropdown dropdown-large menubrands">
		<a href="#" class="dropdown-toggle menu-level0 menubrands-link" data-type="href"><span>Все бренды</span> <b class="caret"></b></a>        
        

        <div class="dropdown-menu dropdown-menu-large menu-columns menu-width-2" style="display: none;">
			{$brands_all_count = 0}
			{foreach $brands_all as $b}
                {$b->products_count|var_dump}
                {if $b->products_count > 0}
                    {$brands_all_count = $brands_all_count + 1}
                {/if}
            {/foreach}
			<ul>
				{foreach $brands_all as $b}
					{if $b->products_count > 0}
						<li data-id="{$b->id}">
							<a href="{*$config->root_url*}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}" data-type="href"
							   {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
							<span>{$b->products_count}</span>
						</li>
					{/if}
				{/foreach}
			</ul>
		</div>
     </li>
     </ul>

    {* Бренды конец *}

</div>
</nav>

{********************* Мегаменю КОНЕЦ] *************************}

{* Объявление на сайте *}

{*if {banner id=4}}
    <div class="panel panel-default topnews">
        <div class="panel-body">
            {banner id=4}
        </div>
    </div>
{/if*}

{if $show_success_registration}
    <div class="alert alert-success">
        <strong>Поздравляем!</strong> Теперь, используя <a href="/account/">личный кабинет</a>, вы можете <a href="/account/?mode=personal-data">сменить личные данные</a> или <a href="/account/?mode=change-password">пароль</a>. Также Вам доступна <a href="/account/?mode=my-orders">история заказов</a> и <a href="/account/?mode=notification-settings">управление оповещениями на почту и SMS</a>.
    </div>
{/if}

<div class="row" id="main-container">

	{$show_filter = false}
	{$show_categories = false}
	{if $filter_tags_groups}{$show_filter = true}{/if}
	{if !empty($current_categories_frontend)}{$show_categories = true}{/if}

    <div class="col-xs-3" id="main-left-container"
		{* ЕСЛИ ТРЕБУЕТСЯ СКРЫВАТЬ ЛЕВУЮ КОЛОНКУ КОГДА НЕТ КАТЕГОРИЙ И ФИЛЬТРА ТО ДОБАВИТЬ В УСЛОВИЕ СЛЕДУЮЩЕЕ *}
		{*|| ($module->module == "ProductsController" && !$show_filter && !$show_categories)*}
		
         {if $module->module=='CartController' || $module->module=='CompareController' || $module->module=='OrderController' || $module->module=='AccountController' || $module->module=='ProductController' || $module->module=="MainController" || $module->module=="LoginController" || $module->module=="RegisterController" || $module->module == "ReviewsController" || $module->module=="SitemapViewController"}style='display:none;'{/if}>
        {* Список категорий для результатов поиска начало*}
        <div id="searchcategories" {if !$finded_categories}style="display:none;"{/if}>
            {if $finded_categories}
                <div class="searchcats">Категории:</div>
                <ol>
                    {*<li>
                        <div>
                            <span></span>
                            <a class="menu61 {if !$category_id}active{/if}" href="{*$config->root_url*}{$products_module->url}{*url add=['format'=>'search', 'keyword'=>$keyword]*}">Все</a>
                        </div>
                    </li>*}
                    {foreach $finded_categories as $categories_position}
                        {foreach $categories_position as $cat}
                            <li>
                                <div>
                                    <span>{$cat.count}</span>
                                    <a class="menu61 {if $cat.id == $category_id}active{/if}"
                                       href="{$cat.url}">{$cat.name}</a>
                                </div>
                            </li>
                        {/foreach}
                    {/foreach}
                </ol>
            {/if}

        </div>
        {* Список категорий для результатов поиска конец *}
		
		{* Список подкатегорий *}
		{if $show_categories}
		<div id="categories">
            <div class="categories-heading">{$category_header->name}</div>
            <div class="categories-body">
                    {* Дерево строится на аяксе *}
					{$olclass = ''}
					{foreach $current_categories_frontend[0].subcategories as $subcategory1}
						{if $subcategory1.folder}
							{$olclass = 'class="havechilds"'}
						{/if}
					{/foreach}
                    <ol {$olclass}>
                        {function name=categories_tree level=1}
                            {foreach $items as $item}
                                <li class="{if !$item.subcategories}collapsed{/if} {if $item.folder}havechild{/if}"
                                    data-id="{$item.id}"
									data-level="{$level}">
                                    {if $item.folder}
                                        <i class="fa fa-minus"
                                           style="{if $item.subcategories}display: block;{else}display: none;{/if}"></i>
                                        <i class="fa fa-plus"
                                           style="{if $item.subcategories}display: none;{else}display: block;{/if}"></i>
                                        <i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
                                    {/if}
                                    <div>
                                        <span>{$item.products_count}</span>
                                        <a href="{*$config->root_url*}{$products_module->url}{$item.url}"
                                           class="menu{$item.id} menu-level{$level} {if $item.id == $category_id}active{/if}">{$item.title}</a>
                                    </div>
                                    {if $item.subcategories}
                                        <ol style="display: block;">
                                            {categories_tree items=$item.subcategories level=$level+1}
                                        </ol>
                                    {/if}
                                </li>
                            {/foreach}
                        {/function}
                        {categories_tree items=$current_categories_frontend[0].subcategories}
                    </ol>
             </div>
        </div>
		{/if}
		{* Список подкатегорий конец *}

        {*include file='filter.tpl'*}

		{if $module->module=='PagesController'}
			
                <div class="materials-menu">
                    <div class="materials-menu-header">
                        {*menuname id=1*}
                    </div>
						{*menu id=1 menu_type='vertical'*}
                </div>
		{/if}

        {* Местро под дополнительный модуль под Новостями *}
        {*if {banner id=7}}
            <div class="left-module">
                {banner id=7}
            </div>
        {/if*}

    </div>
	
	{* ЕСЛИ ТРЕБУЕТСЯ СКРЫВАТЬ ЛЕВУЮ КОЛОНКУ КОГДА НЕТ КАТЕГОРИЙ И ФИЛЬТРА ТО ДОБАВИТЬ В УСЛОВИЕ СЛЕДУЮЩЕЕ *}
	{*|| ($module->module == "ProductsController" && !$show_filter && !$show_categories)*}
	
    <div class="col-xs-12" id="main-right-container">
        {*Выводим контент*}
        <div id="content">
            {$content}
        </div>
    </div>

</div>

	{if $module->module == "MainContoller"}
		{$brands_all_count = 0}
        {foreach $brands_all as $b}{if $b->products_count > 0}{$brands_all_count = $brands_all_count + 1}{/if}{/foreach}
        <div id="brands" {if $brands_all_count == 0}style="display:none;"{/if}>
             <div class="head">Бренды
                {if $brands_popular|count > 0}
                    <div class="btn-group brandlinks">
                        <a href="#" id="pop-brands" class="btn btn-xs current"><span>Популярные</span></a>
                        <a href="#" id="all-brands" class="btn btn-xs"><span>Все</span></a>
                    </div>
                {/if}
            </div>
            {$brands_popular_count = 0}
            {foreach $brands_popular as $b}{if $b->products_count > 0}{$brands_popular_count = $brands_popular_count + 1}{/if}{/foreach}
            {if $brands_popular_count > 0}
                <div class="panel-body" id="pop-brands-list">
                    <ul>
                        {foreach $brands_popular as $b}
                            {if $b->products_count > 0}
                                <li data-id="{$b->id}">
                                    <a href="{*$config->root_url*}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}"
                                       {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                                    <span>{$b->products_count}</span>
                                </li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            {/if}
            <div class="panel-body" id="all-brands-list" {if $brands_popular_count > 0}style="display:none;"{/if}>
                <ul>
                    {foreach $brands_all as $b}
                        {if $b->products_count > 0}
                            <li data-id="{$b->id}">
                                <a href="{*$config->root_url*}{$brand_module->url}{$b->url}{$settings->postfix_brand_url}"
                                   {if $brand->id == $b->id}class="active"{/if}>{$b->name}</a>
                                <span>{$b->products_count}</span>
                            </li>
                        {/if}
                    {/foreach}
                </ul>
            </div>
        </div>
	{/if}

<div id="viewedproducts">
            {* Блок генерируется через ajax *}
            {*include file='viewed-products.tpl'*}
</div>

<div class="needhelp">
	<div class="needhelp-header">Нужна помощь?</div>
	<div class="needhelp-text">Звоните {if {banner id=11}}{banner id=11}{/if}</div>
	<div class="needhelp-add">{if {banner id=13}}{banner id=13}{/if}</div>
</div>

</div>
</div>

<div class="company">
    <div class="company-inner">
            <div str-row>
                <div str-xs>
                    <div class="company-menu">
                        <div class="company-menu-header">
                            {menuname id=16}
                        </div>
                            {menu id=16 menu_type='vertical'}
                    </div>
                </div>
                <div str-xs>
                    <div class="company-menu">
                        <div class="company-menu-header">
                            {menuname id=9}
                        </div>
                        {menu id=9 menu_type='vertical'}
                    </div>
                </div>
                {if {banner id=9}}
                <div str-xs>
                    <div class="company-menu">
                        <div class="company-menu-header">
                            {bannername id=9}
                        </div>
                        {banner id=9}
                    </div>
                </div>
                {/if}
                {if {banner id=6}}
                <div str-xs>
                    <div class="company-menu">
                        <div class="company-menu-header">
                            {bannername id=6}
                        </div>
                        {banner id=6}
                    </div>
                </div>
                {/if}

            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <a href="/" class="footer-logo"></a>
            </div>
            <div class="col-xs-6">
            {if {banner id=3}}
                <div class="footer-copyright">
                    {banner id=3}
                </div>
            {/if}
            </div>
            <div class="col-xs-3">
            {if {banner id=14}}
                <div class="footer-cms">
                    {banner id=14}
                </div>
            {/if}
            </div>
            </div>
        </div>
    </div>
</div>

{* Подключаем модальные окна *}
{include file='modals.tpl'}

<script>
	{*
	$(window).scroll(function() {
		if ($(this).scrollTop() > 1 && $('#emptycart').length == 0){  
			$('#topline').addClass("sticky");
		}
		else{
			$('#topline').removeClass("sticky");
		}
	});
	*}

    $(document).ready(function () {
        $('a[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });

        {*$('a[data-toggle="popover"]').popover({
            html: true,
            trigger: 'hover'
        });*}

        $('a[data-type="qtip"]').qtip({
            content: {
                attr: 'data-content',
                title: function (event, api) {
                    return $(this).attr('data-title');
                },
                button: true
            },
            style: {
                classes: 'qtip-light qtip-rounded'
            },
            show: {
                delay: 0{*,
				event: 'hover'*}
            }{*,
			hide: {
				event: 'unfocus'
			}*}
        });

        {*history_products_ajax();

        {if $category_path}
            var path = "{$category_path}";
            path = path.split(',');
            print_lazy_result($('#categories'),0,undefined,undefined, path, {$category_id});
        {else}
            {if $category_id}
                print_lazy_result($('#categories'),0,undefined,undefined, [], {$category_id});
            {else}
                print_lazy_result($('#categories'),0,undefined,undefined,[],0);
            {/if}
        {/if*}
    });


    var initialPopup = true;

    window.addEventListener('popstate', function (event) {
        if (event.state != undefined && event.state.mode != undefined) {
            if (event.state.mode == 'brand') {
                brand_request_ajax(event.state.params, false);
            }
            if (event.state.mode == 'products') {
				var href_str = "";
				var params_str = "";
				
				for(var index in event.state.params)
				{
					if (event.state.params[index].key == "url")
						href_str = event.state.params[index].value;
					else
					{
						if (params_str.length == 0)
							params_str = "?";
						else
							params_str += "&";
						params_str += event.state.params[index].key + "=" + event.state.params[index].value;
					}
				}
				
				if (params_str.length > 0)
					href_str = href_str + params_str;
				
				location.href = href_str;
				return false;
			
                {*$('#main-left-container').show();
                if ($('#main-right-container').hasClass('col-xs-12')) {
                    $('#main-right-container').removeClass('col-xs-12');
                    $('#main-right-container').addClass('col-xs-9');
                }

                var category_path = event.state.category_path;
                var selected_category_id = event.state.selected_category_id;

                $('#categories a.active').removeClass('active');
                $('#categories li:not(.collapsed) {$nested_level}').each(function () {
                    var icon_minus = $(this).find('i.fa-minus');
                    if (icon_minus.length > 0)
                        icon_minus.trigger('click');
                });

                if (category_path.length > 0) {
                    var path = category_path.split(',');

                    while (path.length > 0) {
                        var id = path[0];
                        var li = $('#categories li[data-id=' + id + ']');
                        var icon_plus = li.find('i.fa-plus:first');
                        if (icon_plus.length > 0)
                            icon_plus.trigger('click');
                        path.shift();
                    }
                }

                if (selected_category_id.length > 0) {
                    var li = $('#categories li[data-id=' + selected_category_id + ']');
                    var icon_plus = li.find('i.fa-plus');
                    if (icon_plus.length > 0)
                        icon_plus.trigger('click');
                    $('#categories li[data-id=' + selected_category_id + '] a:first').addClass('active');
                }

                products_request_ajax(event.state.params, false);*}
            }
            if (event.state.mode == 'product') {
                $('#main-left-container').hide();
                if ($('#main-right-container').hasClass('col-xs-9')) {
                    $('#main-right-container').removeClass('col-xs-9');
                    $('#main-right-container').addClass('col-xs-12');
                }

                var category_path = event.state.category_path;
                var selected_category_id = event.state.selected_category_id;

                {* Свернем другие категории *}
                $('#categories a.active').removeClass('active');
                $('#categories li:not(.collapsed) {$nested_level}').each(function () {
                    var icon_minus = $(this).find('i.fa-minus');
                    if (icon_minus.length > 0)
                        icon_minus.trigger('click');
                });

                if (category_path.length > 0) {
                    var path = category_path.split(',');
                    while (path.length > 0) {
                        var id = path[0];
                        var li = $('#categories li[data-id=' + id + ']');
                        var icon_plus = li.find('i.fa-plus:first');
                        if (icon_plus.length > 0)
                            icon_plus.trigger('click');
                        path.shift();
                    }
                }

                if (selected_category_id.length > 0) {
                    var li = $('#categories li[data-id=' + selected_category_id + ']');
                    var icon_plus = li.find('i.fa-plus');
                    if (icon_plus.length > 0)
                        icon_plus.trigger('click');
                    $('#categories li[data-id=' + selected_category_id + '] a:first').addClass('active');
                }

                product_request_ajax(event.state.url, false);
            }
            if (event.state.mode == 'material') {
                $('#main-left-container').show();
                if ($('#main-right-container').hasClass('col-xs-12')) {
                    $('#main-right-container').removeClass('col-xs-12');
                    $('#main-right-container').addClass('col-xs-9');
                }

                {* Свернем другие категории *}
                $('#categories a.active').removeClass('active');
                $('#categories li:not(.collapsed) {$nested_level}').each(function () {
                    var icon_minus = $(this).find('i.fa-minus');
                    if (icon_minus.length > 0)
                        icon_minus.trigger('click');
                });

                $('#collapseCategory').addClass('in');

                material_request_ajax(event.state.url, false);
            }
            if (event.state.mode == 'cart') {
                cart_request_ajax(false);
            }
            if (event.state.mode == 'compare') {
                compare_request_ajax(false);
            }
			if (event.state.mode == 'order'){
				location.href = event.state.url;
			}
			if (event.state.mode == 'account'){
				location.href = event.state.url;
			}
        }
        else
        if (initialPopup)
        {
            initialPopup = false;

            {if $module->module == "ProductsController"}
            if (typeof history.pushState != undefined)
                history.replaceState({ mode: 'products', params: [{ key: 'url', value: '{*$config->root_url*}{$module->url}{$current_url}'},{foreach $params_arr as $key=>$val}{ key: '{$key}', value: '{$val}'}{if !$val@last},{/if}{/foreach}], selected_category_id: '{$category->id}', category_path: "{$category_path}"},null,encodeURI(decodeURI(document.location.href)));
            {elseif $module->module == "ProductController"}
            if (typeof history.pushState != undefined)
                history.replaceState({ mode: 'product', url: encodeURI(decodeURI(document.location.href)), category_path: "{$category_path}", selected_category_id: '{$category->id}'},null,encodeURI(decodeURI(document.location.href)));
            {elseif $module->module == "PagesController" || $module->module == "MainController"}
            if (typeof history.pushState != undefined)
                history.replaceState({ mode: 'material', url: encodeURI(decodeURI(document.location.href))},null,encodeURI(decodeURI(document.location.href)));
            {elseif $module->module == "OrderController"}
			if (typeof history.pushState != undefined)
				history.replaceState({ mode: 'order', url: encodeURI(decodeURI(document.location.href))}, null, encodeURI(decodeURI(document.location.href)));
			{elseif $module->module == "BrandController"}
			if (typeof history.pushState != undefined)
				history.replaceState({ mode: 'brand', url: encodeURI(decodeURI(document.location.href))}, null, encodeURI(decodeURI(document.location.href)));
			{/if}
        }
        else
            document.location.href = location.origin;
    });

    {if $module->module == "ProductsController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'products', params: [
            { key: 'url', value: '{*$config->root_url*}{$module->url}{$current_url}'},
                {foreach $params_arr as $key=>$val}{ key: '{$key}', value: '{$val}'}{if !$val@last},
            {/if}{/foreach}
        ], selected_category_id: '{$category->id}', category_path: "{$category_path}"}, null, encodeURI(decodeURI(document.location.href)));
    {elseif $module->module == "ProductController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'product', url: encodeURI(decodeURI(document.location.href)), category_path: "{$category_path}", selected_category_id: '{$category->id}'}, null, encodeURI(decodeURI(document.location.href)));
    {elseif $module->module == "PagesController" || $module->module == "MainController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'material', url: encodeURI(decodeURI(document.location.href))}, null, encodeURI(decodeURI(document.location.href)));
    {elseif $module->module == "OrderController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'order', url: encodeURI(decodeURI(document.location.href))}, null, encodeURI(decodeURI(document.location.href)));
	{elseif $module->module == "BrandController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'brand', params: [
            { key: 'url', value: '{*$config->root_url*}{$module->url}{$current_url}'},
                {foreach $params_arr as $key=>$val}{ key: '{$key}', value: '{$val}'}{if !$val@last},
            {/if}{/foreach}
        ]}, null, encodeURI(decodeURI(document.location.href)));
	{elseif $module->module == "AccountController"}
    if (typeof history.pushState != undefined)
        history.replaceState({ mode: 'account', url: encodeURI(decodeURI(document.location.href))}, null, encodeURI(decodeURI(document.location.href)));
    {/if}
</script>


    <!-- Инициализируем слайдшоу Swiper -->
	{if $settings->slideshow_enabled}
    <script>
    var swiper = new Swiper('.swiper-container', {
        pagination: '.swiper-pagination',
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        spaceBetween: 30,
		speed: {if $settings->slideshow_animation_speed}{$settings->slideshow_animation_speed}{else}800{/if},
        centeredSlides: true,
        autoplay: {if $settings->slideshow_change_speed}{$settings->slideshow_change_speed}{else}4200{/if},
        autoplayDisableOnInteraction: false,
		loop:true
    });
    </script>
	{/if}

{* Выводим коды счетчиков и различной аналитики *}
{$settings->counters_codes}
</body>
</html>