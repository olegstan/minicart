<h1>Карта сайта</h1>


{*Вывод иерархии категорий*}

<p>Каталог товаров</p>
<ul class="sitemap">
{function name=category_list level=0}
	{foreach from=$categories item=category}
		{if $category->is_visible}
			<li>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}<a href="{$config->root_url}{$products_module->url}{$category->url}/">{$category->name|escape}</a>
			{if $category->subcategories}
				<ul class="sitemap">
				{category_list categories=$category->subcategories level=$level+1}
				</ul>
			{/if}
			</li>
		{/if}
	{/foreach}
{/function}
{category_list categories=$categories_tree}
</ul>


{*Вывод материалов*}

{foreach from=$menus item=m}
	{if !$m->is_static}
	<p>{$m->name}</p>
	<ul class="sitemap">
		{foreach $m->items as $subitem}
			<li><a href="{if $subitem->is_main}/{else}{makeurl module='materials' item=$subitem}{/if}">{$subitem->name}</a></li>
		{/foreach}
	</ul>
	{/if}
{/foreach}