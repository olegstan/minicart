{if $cart->total_products > 0}
	<a href="#" class="dropdown-toggle" id="cart" data-toggle="dropdown">В корзине <span class="cart-counter">{$cart->total_products}</span> {$cart->total_products|plural:'товар':'товаров':'товара'}
    {*<br/>на сумму {$cart->total_price|convert} {$main_currency->sign}*}</a>
{else}
	<div id="emptycart">Корзина пуста</div>
{/if}