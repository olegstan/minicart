{if $products|count > 0}
<table class="table compare-head">
	<thead>
		<tr>
			<th class="compare-fistblock">
            <div class="btn-group compare-params">
            	<h3>Сравнение {$products|count} товаров</h3>
				<a href="#" id="show-all" class="btn btn-link {if $products|count == 1}active{/if}"><span>Все параметры</span></a>
				{if $products|count > 1}<a href="#" id="show-different" class="btn btn-link active"><span>Различающиеся</span></a>{/if}
            </div>
			</th>
			{foreach $products as $product}
				<th class="compare-product">
                	<div class="compare-product-image">
					{if $product->image}<a href="{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}"><img src="{$product->image->filename|resize:'products':120:120}"/></a>{/if}
                    </div>
					<a href="{$config->root_url}{$product_module->url}{$product->url}{$settings->postfix_product_url}"  class="compare-product-name">{$product->name}</a>
					<a href="{$config->root_url}{$module->url}{url add=['action'=>'delete', 'product_id'=>$product->id, 'ajax'=>1, 'id'=>$ids]}" class="compare-delete"><i class="fa fa-times"></i> <span>Удалить</span></a>
				</th>
			{/foreach}
            
          </tr>
        </thead>
 </table>
 
 <table id="table-parameters" class="table table-hover compare-body">
	<tbody>
		{foreach $product_tags_groups_ids as $group_id}
			{$group = $tags_groups[$group_id]}
			
			{$is_different = false}
			{$old_tags_str = ""}
			
			{foreach $products as $product}
				{$tags = $product->tags[$group_id]}
				{$tags_str = ""}
				{foreach $tags as $tag}
					{if !empty($tags_str)}{$tags_str = $tags_str|cat:', '}{/if}
					{*if $group->name == "Цена"}
						{$tags_str = $tags_str|cat:($tag->name|convert)}
					{else*}
						{$tags_str = $tags_str|cat:$tag->name}
					{*/if*}
					{if $group->postfix}
						{$tags_str = $tags_str|cat:" "}
						{if $group->name == "Цена"}
							{$tags_str = $tags_str|cat:$main_currency->sign}
						{else}
							{$tags_str = $tags_str|cat:$group->postfix}
						{/if}
					{/if}
				{/foreach}
				{if empty($tags_str)}{$tags_str = "-"}{/if}
				{if empty($old_tags_str)}
					{$old_tags_str = $tags_str}
				{elseif $old_tags_str != $tags_str}
					{$is_different = true}
				{/if}
			{/foreach}
			
			{if $group->name == "Наличие"}{$is_different = true}{/if}
			
			<tr class="all-row{if $is_different} different-row{/if}" {if !$is_different && $products|count > 1}style="display:none;"{/if}>
				<td class="compare-property-name">{if $group->name == "Наличие"}Статус{else}{$group->name}{/if}{if $group->help_text} <a class="property-help" data-type="qtip" data-content="{$group->help_text|escape}" data-title="{$group->name}"><i class="fa fa-question-circle"></i></a>{/if}</td>
				{foreach $products as $product}
					{$tags = $product->tags[$group_id]}
					{$tags_str = ""}
					{if $group->name == "Наличие"}
						{foreach $product->variants as $variant}
							{if $variant->name}
								{$tags_str = $tags_str|cat:$variant->name}
								{$tags_str = $tags_str|cat:" - "}
							{/if}
							{if $variant->stock < 0}
								{$tags_str = $tags_str|cat:"<span class=\"to-order product-status\"><i class=\"fa fa-truck\"></i>"}
								{$tags_str = $tags_str|cat:" Под заказ"}
								{$tags_str = $tags_str|cat:"</span>"}
							{/if}
							{if $variant->stock == 0}
								{$tags_str = $tags_str|cat:"<span class=\"out-of-stock product-status\"><i class=\"fa fa-times-circle\"></i>"}
								{$tags_str = $tags_str|cat:" Нет в наличии"}
								{$tags_str = $tags_str|cat:"</span>"}
							{/if}
							{if $variant->stock > 0}
								{$tags_str = $tags_str|cat:"<span class=\"in-stock product-status\"><i class=\"fa fa-check\"></i>"}
								{$tags_str = $tags_str|cat:" В наличии"}
								{$tags_str = $tags_str|cat:"</span>"}
							{/if}
							{if !$variant@last}{$tags_str = $tags_str|cat:"<hr/>"}{/if}
						{/foreach}
					{else}
						{foreach $tags as $tag}
							{if !empty($tags_str)}{$tags_str = $tags_str|cat:', '}{/if}
							{if $group->name == "Цена"}
								{$tags_str = $tags_str|cat:($tag->name|convert)}
							{else}
								{$tags_str = $tags_str|cat:$tag->name}
							{/if}
							{if $group->postfix}
								{$tags_str = $tags_str|cat:" "}
								{if $group->name == "Цена"}
									{$tags_str = $tags_str|cat:$main_currency->sign}
								{else}
									{$tags_str = $tags_str|cat:$group->postfix}
								{/if}
							{/if}
						{/foreach}
						{if empty($tags_str)}{$tags_str = "-"}{/if}
					{/if}
					<td class="compare-property-value">{if $product->brand && $group->name == "Бренд"}<a href="{$config->root_url}{$brand_module->url}{$product->brand->url}{$settings->postfix_brand_url}">{$tags_str}</a>{else}{$tags_str}{/if}</td>
				{/foreach}
			</tr>
			
			{if $group->name == "Бренд"}
				<tr class="all-row different-row">
					<td class="compare-property-name">Рейтинг</td>
					{foreach $products as $product}
						<td class="compare-property-value">
							{if $product->rating->rating_count > 0}
								<div class="raiting rate{$product->rating->avg_rating*10}" title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}">
								</div>
							{/if}
						</td>
					{/foreach}
				</tr>
			{/if}
		{/foreach}
   	
</table>
{else}
<h3>Нет товаров для сравнения</h3>
{/if}

<script type="text/javascript">
	$('.compare-delete').click(function(){
		var href = $(this).attr('href');
		$.get(href, function(data){
			if (data.success)
			{
				location.href = data.compare_href;
			}
		});
		return false;
	});
	
	$('#show-different').click(function(){
		$('#table-parameters tr.all-row').hide();
		$('#table-parameters tr.different-row').show();
		$('#show-all').removeClass('active');
		$('#show-different').addClass('active');
		return false;
	});
	
	$('#show-all').click(function(){
		$('#table-parameters tr.different-row').hide();
		$('#table-parameters tr.all-row').show();
		$('#show-all').addClass('active');
		$('#show-different').removeClass('active');
		return false;
	});
</script>