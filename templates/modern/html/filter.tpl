<div id="filter" class="panel panel-default" {if !$filter_tags_groups}style="display:none;"{/if} data-url="{$config->root_url}{$module->url}{$current_url}">
{*
<div class="filter-counter">
	<a href="#">Показать 1083 товара</a>
    <div class="filter-arrow"></div>        
    <!-- Товары не найдены -->
</div>
*}
	<div class="panel-heading">Фильтр <i class="fa fa-spinner fa-spin fa-large" id="filter_spinner" style="display: none;"></i>
	<a href="#" id="clear-filter" class="btn btn-xs"><span>Сбросить</span></a>
	</div>
	<div class="panel-body">
	<div class="selected-values"></div>
	
	{foreach $filter_tags_groups as $filter_tag_group}
		{*if $filter_tag_group->mode == "range" && $filter_tag_group->min_value_available && $filter_tag_group->max_value_available && $filter_tag_group->min_value_available != $filter_tag_group->max_value_available*}
		{*if $filter_tag_group->mode == "range" && $filter_tag_group->min_value_available && $filter_tag_group->max_value_available*}
		{if $filter_tag_group->mode == "range"}
			<div class="form-group">
				<i class="fa fa-caret-right" {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<i class="fa fa-caret-down" {if (!is_array($filter_tags) || !array_key_exists($filter_tag_group->group_id, $filter_tags)) && !$filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<label data-toggle="collapse" class="labelcollapse" data-parent="#filter" href="#collapse{$filter_tag_group->group_id}" class="labelcollapse"><span>{$filter_tag_group->name}</span></label>
				
				<div id="collapse{$filter_tag_group->group_id}" class="collapse {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}in{/if}">
					<div class="range">
						<div class="rleft">
							<div class="input-group">					
								<span class="input-group-addon">от</span>
								<input type="text" class="form-control input-sm rangeSlider" id="slider{$filter_tag_group->group_id}_min" data-control="from" data-object="slider{$filter_tag_group->group_id}" {if !isset($filter_tag_group->min_value_available) || !isset($filter_tag_group->max_value_available) || (empty($filter_tag_group->min_value_available) && empty($filter_tag_group->max_value_available))}disabled{/if}/>
								<span class="input-group-addon">до</span>
							</div>
						</div>
						<div class="rright">
							<div class="input-group">
								<input type="text" class="form-control input-sm rangeSlider" id="slider{$filter_tag_group->group_id}_max" data-control="to" data-object="slider{$filter_tag_group->group_id}" {if !isset($filter_tag_group->min_value_available) || !isset($filter_tag_group->max_value_available) || (empty($filter_tag_group->min_value_available) && empty($filter_tag_group->max_value_available))}disabled{/if}/>
								<span class="input-group-addon">{$filter_tag_group->postfix}</span>
							</div>
						</div>
					</div>
					
					{assign var="filter_from_value" value=$filter_tag_group->min_value}
					{assign var="filter_to_value" value=$filter_tag_group->max_value}
					
					{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)}
						{assign var="filter_from_value" value=$filter_tags[$filter_tag_group->group_id]->from}
						{assign var="filter_to_value" value=$filter_tags[$filter_tag_group->group_id]->to}
					{/if}
					
					<input type="text" name="{$filter_tag_group->name_translit}" value="{$filter_from_value};{$filter_to_value}" data-control="rangeSlider" data-type="double" data-prettify="true" data-hideminmax="true" data-hidefromto="true" data-min="{$filter_tag_group->min_value}" data-max="{$filter_tag_group->max_value}" data-from="{$filter_from_value}" data-to="{$filter_to_value}" data-datafrom="{$filter_tag_group->min_value_available}" data-datato="{$filter_tag_group->max_value_available}" data-hasgrid="true" data-step="{if $filter_tag_group->diapason_step}{$filter_tag_group->diapason_step}{else}1{/if}" data-id="slider{$filter_tag_group->group_id}" {if !isset($filter_tag_group->min_value_available) || !isset($filter_tag_group->max_value_available) || (empty($filter_tag_group->min_value_available) && empty($filter_tag_group->max_value_available))}data-disable="true"{/if} id="slider{$filter_tag_group->group_id}"/>
				</div>
			</div>
		{/if}
		{if $filter_tag_group->mode == "select"}
			<div class="form-group select-filter">
				<label>{$filter_tag_group->name}</label>
				{if $filter_tag_group->help_text}<a class="property-help" data-type="qtip" data-content="{$filter_tag_group->help_text|escape_string}" data-title="{$filter_tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}
				<select class="form-control input-sm" name="{$filter_tag_group->name_translit}">
					<option value="0">Все</option>
					{foreach $filter_tag_group->tags_all as $tag}
					<option value="{$tag->id}" {if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag->id, $filter_tags[$filter_tag_group->group_id])}selected{/if} {if !in_array($tag->id, $filter_tag_group->tags_available)}class="unavailable"{/if}>{$tag->name}</option>
					{/foreach}
				</select>		  
			</div>
		{/if}
		{if $filter_tag_group->mode == "checkbox"}
			<div class="form-group checkboxes">
				<i class="fa fa-caret-right" {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<i class="fa fa-caret-down" {if (!is_array($filter_tags) || !array_key_exists($filter_tag_group->group_id, $filter_tags)) && !$filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<label data-toggle="collapse" class="labelcollapse" data-parent="#filter" href="#collapse{$filter_tag_group->group_id}" class="labelcollapse"><span>{$filter_tag_group->name}</span></label>
				{if $filter_tag_group->help_text}<a class="property-help" data-type="qtip" data-content="{$filter_tag_group->help_text|escape_string}" data-title="{$filter_tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}
				
				<div id="collapse{$filter_tag_group->group_id}" class="collapse {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}in{/if}">
					<div class="items-collapse">
						{$show_mode = "popular"}
						{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)}
							{foreach $filter_tags[$filter_tag_group->group_id] as $ftag}
								{if !in_array($ftag, $filter_tag_group->tags_popular_arr)}{$show_mode = "all"}{/if}
							{/foreach}
						{/if}
						
						{foreach $filter_tag_group->tags_all as $tag}
							{$class = ""}
							{if $show_mode == "popular" && !in_array($tag->id, $filter_tag_group->tags_popular_arr) && $filter_tag_group->tags_popular|count > 0 && ((is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && !in_array($tag->id, $filter_tags[$filter_tag_group->group_id])) || (is_array($filter_tags) && !array_key_exists($filter_tag_group->group_id, $filter_tags)) || !is_array($filter_tags))}{$class = "hidden"}{/if}
							<div class="checkbox {$class} {if !in_array($tag->id, $filter_tag_group->tags_available)}unavailable{/if}">
								<label>
									<input type="checkbox" name="{$filter_tag_group->name_translit}" value="{$tag->id}" {if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag->id, $filter_tags[$filter_tag_group->group_id])}checked{/if}/> {$tag->name}
								</label>
							</div>
						{/foreach}
					</div>
					{if $filter_tag_group->tags_popular|count > 0 && $filter_tag_group->tags_all|count != $filter_tag_group->tags_popular|count && $show_mode == "popular"}
						<a class="expand-more-tags2">Показать все</a>
					{/if}
				</div>
			</div>
		{/if}
		{if $filter_tag_group->mode == "radio"}
			<div class="form-group radio-filter">
				<i class="fa fa-caret-right" {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<i class="fa fa-caret-down" {if (!is_array($filter_tags) || !array_key_exists($filter_tag_group->group_id, $filter_tags)) && !$filter_tag_group->default_expand}style="display:none;"{/if}></i>
				<label data-toggle="collapse" class="labelcollapse" data-parent="#filter" href="#collapse{$filter_tag_group->group_id}"><span>{$filter_tag_group->name}</span></label>
				
				{if $filter_tag_group->help_text}<a class="property-help" data-type="qtip" data-content="{$filter_tag_group->help_text|escape_string}" data-title="{$filter_tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}
				
				<div id="collapse{$filter_tag_group->group_id}" class="collapse {if (is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)) || $filter_tag_group->default_expand}in{/if}">
					<div class="items-collapse">
						{$show_mode = "popular"}
						{if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags)}
							{foreach $filter_tags[$filter_tag_group->group_id] as $ftag}
								{if !in_array($ftag, $filter_tag_group->tags_popular_arr)}{$show_mode = "all"}{/if}
							{/foreach}
						{/if}
					
						{assign var="exists_selected" value=false}
						{foreach $filter_tag_group->tags_all as $tag}
							{$class = ""}
							{if $show_mode == "popular" && !in_array($tag->id, $filter_tag_group->tags_popular_arr) && $filter_tag_group->tags_popular|count > 0 && ((is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && !in_array($tag->id, $filter_tags[$filter_tag_group->group_id])) || (is_array($filter_tags) && !array_key_exists($filter_tag_group->group_id, $filter_tags)) || !is_array($filter_tags))}{$class = "hidden"}{/if}
							{*if $show_mode == "popular" && !in_array($tag->id, $filter_tag_group->tags_popular_arr) && !in_array($tag->id, $filter_tags[$filter_tag_group->group_id])}{$class = "hidden"}{/if*}
							<div class="radio {$class} {if !in_array($tag->id, $filter_tag_group->tags_available)}unavailable{/if}">
								<label>
									<input type="radio" name="{$filter_tag_group->name_translit}" value="{$tag->id}" {if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag->id, $filter_tags[$filter_tag_group->group_id])}checked{assign var="exists_selected" value=true}{/if}/> {$tag->name}
								</label>
							</div>
						{/foreach}
						
						<div class="radio">
							<label>
								<input type="radio" name="{$filter_tag_group->name_translit}" value="0" {if !$exists_selected}checked{/if}/> неважно
							</label>
						</div>
					</div>
					
					{if $filter_tag_group->tags_popular|count > 0 && $filter_tag_group->tags_all|count != $filter_tag_group->tags_popular|count && $show_mode == "popular"}
						<a class="expand-more-tags2">еще...</a>
					{/if}
				</div>
					
			</div>
		{/if}
		{if $filter_tag_group->mode == "logical"}
			{$tag_id = 0}
			{foreach $filter_tag_group->tags_all as $tag}
				{if $tag->name == "да"}{$tag_id = $tag->id}{/if}
			{/foreach}
			<div class="checkbox logicbox {if !in_array($tag_id, $filter_tag_group->tags_available)}unavailable{/if}">
				<label>
					<input type="checkbox" name="{$filter_tag_group->name_translit}" value="{$tag_id}" {if is_array($filter_tags) && array_key_exists($filter_tag_group->group_id, $filter_tags) && in_array($tag_id, $filter_tags[$filter_tag_group->group_id])}checked{/if}/> {$filter_tag_group->name}
				</label>
				{if $filter_tag_group->help_text}<a class="property-help" data-type="qtip" data-content="{$filter_tag_group->help_text|escape_string}" data-title="{$filter_tag_group->name}"><i class="fa fa-question-circle"></i></a>{/if}
			</div>
		{/if}
	{/foreach}
	
	</div>
	<div class="panel-footer">
		<a href="#" class="btn btn-default btn-sm w100 {if is_array($filter_tags) && $filter_tags|count>0 && $products_count == 0}disabled{/if}" id="apply_products_filter">{if is_array($filter_tags) && $filter_tags|count>0 && $products_count == 0}Товары не найдены{else}Показать{if is_array($filter_tags) && $filter_tags|count>0} {$products_count} {$products_count|plural:'товар':'товаров':'товара'}{/if}{/if}</a>
	</div>
</div>

<script type="text/javascript">
	$('.expand-more-tags').click(function(){
		$(this).prev('div.items-collapse.popular-tags').hide();
		$(this).next('div.items-collapse.all-tags').show();
		$(this).hide();
		return false;
	});
	$('.expand-more-tags2').click(function(){
		$(this).prev('div.items-collapse').find('div.hidden').toggleClass('hidden');
		$(this).hide();
		return false;
	});
</script>