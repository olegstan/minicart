<?php
//получить доступные PDO драйвера сервера
//print_r(PDO::getAvailableDrivers());
//настройки подключения ДБ

return [

    /**
     * настройка базы данных
     */

//    'type' => 'mysql', // тип базы данных
//    'host' => 'localhost', // сервер базы
//    'database' => 'mini', // имя базы данных
//    'user' => 'root', // пользователь базы данных
//    'password' => '', // пароль от базы данных
//    'charset' => 'utf8', // кодировка
//    'prefix' => 'mc_', // префикс таблиц
//
    'type' => 'mysql', // тип базы данных
    'host' => '193.169.179.70', // сервер базы
    'database' => 'minicart2', // имя базы данных
    'user' => 'minicart2', // пользователь базы данных
    'password' => '8I5n0O9c', // пароль от базы данных
    'charset' => 'utf8', // кодировка
    'prefix' => 'mc_', // префикс таблиц


    /**
     * шаблон
     */

    'template' => 'test',

    /**
     * системные настройки
     */

    'protocol' => 'http', //протокол сервера



    /**
     * хеширование  паролей
     *
     * ! изменение данной настройки может повлечь
     * необратимые изменения
     */

    'hash_cost' => '10', // цена алгоритмического расхода
    'hash_salt' => 'a25jlfAsdaag42Asdasdaf', // соль хеширования

    /**
     * изображение по умолчанию
     */

    'default_image' => '/files/originals/default/none.png',

    /**
     * окончание урла товара
     *
     *  '/', '.htm', '.html'
     *
     */

    'product_url_end' => '.html',

    'material_url_end' => '.html',

    /**
     * механизм хранения сессий
     *
     * 'files', 'mysql', 'redis'
     */

    'session_handler' => 'files',


    /**
     * настройка для отображения цены
     *
     * если true то 100
     *
     * если false то 100.00
     */

    'without_nulls' => true,

    'defug' => true,

    'log' => true

];