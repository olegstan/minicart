<?php

require_once('api/core/Minicart.php');
require('addons/YMimport/init.php');

require('parser.php');

/**
 * Yandex Market import page for simpla
 */

class YMImportControllerAdmin extends Minicart {
	private $param_url, $options;

	public function set_params($url = null, $options = null)
	{
		$this->param_url = $url;
		$this->options = $options;
	}

	/** An IP address[es] to bind to for outgoing connections
	 * @var string[]|null
	 */
	static public $bindto = array(
			'89.208.157.166',
			'89.208.157.168',
			'193.169.178.20',
				'89.208.157.176',
				'89.208.157.177',
				'89.208.157.178',
				'89.208.157.162',
				'89.208.157.163',
			);
//	static public $bindto = null;

	/** Network surfer
	 * @var NetfSurf
	 */
	static public $Netfsurf;

	/** MySQL
	 * @var MySQL
	 */
	protected $M;

	function __construct() {
		$this->M = oo_mysql_object();
		}

	function fetch() {
		try { 
			return $this->_fetch(); 
		}
		catch (ECaptcha $e) {
			try {
				// Parse out the image URL
				$page = stream_get_contents($e->page);
				$captcha = Str::extract($page, '<img src="', '"');
				$hidden_key = Str::extract($page, 'value="', '"');

				// Load the image
				$Q = self::$Netfsurf->Request($captcha);
				$captcha_image = stream_get_contents($Q->f);
				} 
			catch (EBase $e) {}
			
			return $this->mode_captcha($hidden_key, $captcha_image);
		}
	}

	function _fetch(){

		/* ====== Display the form */
		if (count($_POST) == 0)
			return $this->mode_display_form();

		/* ====== Create the surfer */
		// Pick an IP
		$ipid = null;
		if (is_array(self::$bindto) && count(self::$bindto))
			$ipid = array_rand(self::$bindto);
		// Init the surfer
		self::$Netfsurf = new NetfSurf("addons/YMimport/tmp/YMarket-{$ipid}.cookies");
		self::$Netfsurf->set_timeout(10);
		if (!is_null($ipid))
			self::$Netfsurf->set_interface(self::$bindto[$ipid]);

		if (isset($_POST['captcha'])) {
			try {
				$Q = self::$Netfsurf->Request('http://market.yandex.ru/captcha/check-captcha.xml', array(
					'key' => $_POST['captcha_key'],
					'response' => $_POST['captcha'],
					'retpath' => 'http://market.yandex.ru/',
					));
				} catch (ENet $e) {}
			return $this->mode_display_form();
			}

		/* ====== Prepare URLs for importing */

		if (isset($_POST['urls'])) {
			$_SESSION['YMimport'] = array('list' => array(), 'product' => array());
			if (!isset($_SESSION['YMimport-dupecheck']))
				$_SESSION['YMimport-dupecheck'] = array();

			/* === Create import lists */
			foreach (explode("\n",$_POST['urls']) as $url) {
				$url = trim($url);
				if (strlen($url) == 0)
					continue;
				/* What is the URL pointing to? */
				$mode = null;
				$invalid = array();
				if (YMParser_List::test_url($url))
					$mode = 'list';
					elseif (YMParser_Product::test_url($url))
					$mode='product';
					else
					$invalid[] = $url;
				/* Add to list */
				if (!is_null($mode))
					$_SESSION['YMimport'][$mode][] = $url;
			}

			/* === Errors? */
			if (count($invalid) > 0)
				return $this->mode_invalid_urls($invalid);

			/* === Process import lists */
			$exceptions = array();
			$_SESSION['YMimport']['choose'] = array();
			foreach ($_SESSION['YMimport']['list'] as $url) {
				try {
					$YMp = new YMParser_List($url);
					$YMp->Parse();
					foreach ($YMp->products as $product) {
						$product->id = count($_SESSION['YMimport']['choose']);
						$_SESSION['YMimport']['choose'][] = $product;
					}
				}catch (ECaptcha $e) { throw $e; }
				 catch (EBase $e) { $exceptions[] = $e; $list=null; continue; }
			}

			/* === Errors ? */
			if (count($exceptions) > 0)
				return $this->mode_exceptions(0,0,$exceptions);
			/* === Choose */
			if (count($_SESSION['YMimport']['choose']))
				return $this->mode_choose($_SESSION['YMimport']['choose']);
		}

		/* ====== Choose page process */
			elseif (isset($_POST['choose'])) {
			foreach (array_keys($_SESSION['YMimport']['choose']) as $id)
				if (!in_array($id, $_POST['choose']))
				 unset($_SESSION['YMimport']['choose'][$id]);
			}
			//print_r(array_keys($_SESSION['YMimport']['choose']));exit();

		/* ====== Process the URLs */
		$successful = 0;
		$expected = 0;
		$exceptions = array();
		$this->M->connect();

		@set_time_limit(60*60);
		@ignore_user_abort(1);
		foreach ($_SESSION['YMimport']['choose'] as $product) {
			$expected++;
			$product = get_object_vars($product); // it comes broken: fetch vars
			$successful += $this->_import($product['url'], $exceptions, $product['spec_line']);
		}

		foreach ($_SESSION['YMimport']['product'] as $url) {
			$expected++;
			$successful += $this->_import($url, $exceptions);
		}

		$_SESSION['YMimport'] = null;

		/* Finish */
		if (count($exceptions) == 0) {
			return $this->mode_finished($successful);
			unset($_SESSION['YMimport-dupecheck']);
		}
		else
			return $this->mode_exceptions($successful, $expected, $exceptions);
	}

	protected function _import($url, &$exceptions, $spec_replace = null) {
		try {
			sleep(mt_rand(1,5));
			$YMi = new YMParser_Product($url);
			$YMi->Parse();

			$_SESSION['YMimport-dupecheck'][] = $url;

			}
			catch (ECaptcha $e) { throw $e; }
			catch (EBase $e) { $exceptions[] = $e; return 0; }
		/* ====== Collect data for insertion */
		$data = array(
			'product_id' => null,
			'url' => $this->furl->generate_url($YMi->model),
			//'url' => strtolower(preg_replace('~[^a-z0-9_-]+~iSu', '_', self::translit($YMi->model))).'.html', // URL of the current product
			'cat' => $_POST['category_id'], // its category id
			'model' => trim($YMi->model), // Model string
			'brand' => $YMi->brand,
			'descr' => is_null($spec_replace)? $YMi->spec_short : $spec_replace,//'', // Short description text
			'body_short' => $YMi->spec_short,
			'body' => $YMi->spec_long,//'', // Long description
			'price' => is_null($YMi->price)? 0 : $YMi->price,
			'order_num' => 0, // sorting
			'img' => '', // BIG image filename in 'files/products'
			'thumb' => '', // SMALL image filename in 'files/products'
			'enabled' => 0, // Is it enabled by default? 0|1

			'specs_short' => '',
			'specs_long' => ''
			);

		/* ====== Add to database */

		try {
		
			$brand_id = 0;
			if (!empty($data['brand']))
			{
				$tmp_brand = $this->brands->get_brand($data['brand']);
				if (!$tmp_brand)
				{
					$new_brand = new StdClass;
					$new_brand->name = $data['brand'];
					$new_brand->meta_title = $new_brand->name;
					$new_brand->meta_keywords = $new_brand->name;
					$new_brand->meta_description = $new_brand->name;
					$brand_id = $this->brands->add_brand($new_brand);
				}
				else
					$brand_id = $tmp_brand->id;
			}
		
		
			/* === Add product */
			
			$new_product = new StdClass;
			$new_product->brand_id = $brand_id;
			$new_product->url = $data['url'];
			$new_product->name = $data['model'];
			$new_product->annotation = $data['descr'];
			$new_product->annotation2 = $data['body_short'];
			$new_product->body = $data['body'];
			$new_product->meta_title = $new_product->name;
			
			if (!empty($new_product->annotation))
			{
				$str_name = "";
				if (!empty($new_product->name))
					$str_name = mb_strtolower(strip_tags(html_entity_decode($new_product->name)), 'utf-8');
				$str = mb_strtolower(strip_tags(html_entity_decode($new_product->annotation)), 'utf-8');
				$str = preg_replace("/[^a-zа-я0-9\s]/u", " ", $str);
				$str = preg_replace("/\s+/u", "  ", $str);				//заменим пробелы на двойные пробелы, чтоб следующая регулярка работала, иначе между словами будет общий пробел и условие не пройдет
				$str = preg_replace("/(\s[^\s]{1,3}\s)+/u", " ", $str);	//remove words with length<=3
				$str = preg_replace("/(\s\s)+/u", " ", $str);			//remove double spaces
				$str = trim($str, 'p ');
				$str = preg_replace("/\s+/u", ", ", $str);
				$str = empty($str_name)?$str:(empty($str)?$str_name:$str_name.", ".$str);
				$str = mb_substr($str, 0, 200, 'utf-8');
				$new_product->meta_keywords = $str;
			}
			
			if (!empty($new_product->annotation))
			{
				$str = preg_replace("/[^a-zA-Zа-яА-Я0-9\s]/u", " ", strip_tags(html_entity_decode($new_product->annotation)));
				$str = preg_replace("/\s\s+/u", " ", $str);
				$str = trim($str, 'p ');
				$str_name = "";
				if (!empty($new_product->name))
					$str_name = strip_tags(html_entity_decode($new_product->name));
				$str = empty($str_name)?$str:(empty($str)?$str_name:$str_name.", ".$str);
				$str = mb_substr($str, 0, 200, 'utf-8');
				$new_product->meta_description = $str;
			}
			
			/*$new_product->meta_keywords = $new_product->name;
			$new_product->meta_description = $new_product->name;*/
			$new_product->source = 'market';
			
			$product_id = $this->products->add_product($new_product);
			$data['product_id'] = $product_id;
			
			/* === Add category link */
			
			$this->categories->add_product_category($data['product_id'], $data['cat']);
			
			$new_variant = new StdClass;
			$new_variant->product_id = $product_id;
			$new_variant->price = $data['price'];
			
			$variant_id = $this->variants->add_variant($new_variant);
			}
			catch (ECaptcha $e) { throw $e; }
			catch (EBase $e) { $exceptions[] = $e; return 0; }
		/* === Product ID in sorting */
		$this->db->query("UPDATE __products SET position=id WHERE id=?", $data['product_id']);
		/* === Images */

		foreach ($YMi->imgs as $img_id => $img) {
			$this->image->add_internet_image('products', $product_id, $img->big);
		}
		return 1;
		}

	function mode_display_form() {
		$this->design->assign('mode', 'form');
		$this->design->assign('POST_URLS', isset($_POST['urls'])?$_POST['urls']:'');
		$this->design->assign('Categories', $this->categories->get_categories_tree() );
		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_invalid_urls($invalid) {
		$this->design->assign('mode', 'invalid-urls');
		$this->design->assign('invalid', $invalid);
		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_choose($products) {
		$this->design->assign('mode', 'choose');
		$this->design->assign('category_id', $_POST['category_id']);
		$this->design->assign('products', $products);
		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_finished($successful) {
		$this->design->assign('mode', 'finished');

		$this->design->assign('successful', $successful);

		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_auth_failed() {
		$this->design->assign('mode', 'exceptions');

		$this->design->assign('successful', 0);
		$this->design->assign('expected', 0);

		$this->design->assign('exceptions', 'Не удалось произвести авторизацию в Маркете!');
		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_exceptions($successful, $expected, $exceptions) {
		$this->design->assign('mode', 'exceptions');

		$this->design->assign('successful', $successful);
		$this->design->assign('expected', $expected);

		foreach ($exceptions as $e) {
			$e->class = get_class($e);
			$e->context = DEBUG::sdump($e->context);
			}
		$this->design->assign('exceptions', $exceptions);

		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	function mode_captcha($hidden_key, $captcha_image) {
		$this->design->assign('mode', 'captcha');
		$this->design->assign('captcha_key', $hidden_key);
		$this->design->assign('captcha_img', base64_encode($captcha_image));
		$this->design->assign('POST_URLS', isset($_POST['urls'])?$_POST['urls']:'');
		return $this->design->fetch($this->design->getTemplateDir('admin').'yandex-market-import.tpl');
		}

	static function translit($str, $reverse = false) {
		$ru = array(
					'ё','ж','ч','ш','щ','ъ','ы','ь','э','ю','я',
					'Ё','Ж','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
					'а','б','в','г','д','е','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц',
					'А','Б','В','Г','Д','Е','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц',
					);
		$tr = array(
					'jo','zh','ch','sh','sch',/*'\'\''*/'','y',/*'\''*/'','eh','ju','ja',
					'Jo','Zh','Ch','Sh','Sch',/*'\'\''*/'','Y',/*'\''*/'','Eh','Ju','Ja',

					'a','b','v','g','d','e','z','i','j','k','l','m','n','o','p','r','s','t','u','f','h','c',
					'A','B','V','G','D','E','Z','I','J','K','L','M','N','O','P','R','S','T','U','F','H','C',
					);

		if (!$reverse)
			return str_replace($ru,$tr, $str);
			else
			return str_replace($tr,$ru, $str);
		}
	}


class ECaptcha extends EBase {

	public $page;

	function __construct($page) {
		parent::__construct(0, 'CaptchaRequest', 'Yandex wants a captcha');
		$this->page = $page;
		}
	}

/** Holds a product ()dsfsdf
 */
class YMproduct {

	/** URL pointing to the product
	 * @var string
	 */
	public $url;

	/** Model name
	 * @var string
	 */
	public $model;

	/** Its specifications, plaintext string
	 * @var string
	 */
	public $spec_line;

	function __construct($url, $model, $spec) {
		if (strncmp($url, './', 2) === 0)
			$url = substr($url, 1); // we still do need one slash
		$this->url = "http://market.yandex.ru$url";
		$this->model = $model;
		$this->spec_line = $spec;
		}
	}

/** Holds images for a product
 */
class YMimg {
	/** Big image URL
	 * @var string
	 */
	public $big;

	/** Thumbnail image URL
	 * @var string
	 */
	public $thumb;

	function __construct($big, $thumb) {
		$this->big = $big;
		$this->thumb = $thumb;
		}

	protected function _download($from ,$into) {
		/* Download */
		$N = YMImportControllerAdmin::$Netfsurf;
		$result = $N->Request($from);
		$img = stream_get_contents($result->f);
		fclose($result->f);
		/* Save */
		if (!file_put_contents($into, $img))
			throw new EFile(EFile::Open($into, 'image', 'w', compact('to')));
		}

	/** Download this image: both big & thumb
	 * @param string    $big    Save filename for the big one
	 * @param string    $thumb  Save filename for the small one
	 * @throws ENet(0, 'DOWNLOAD') when failed to download the image
	 * @throws EFile('Open') when failed to save the image
	 */
	function Download($big, $thumb) {
		if (!is_null($big))
			$this->_download($this->big, $big);
		if (!is_null($thumb))
			$this->_download($this->thumb, $thumb);
		}
	}

/** Parses a list of products on one page
 */
class YMParser_List {

	/** Product page URL
	 * @var string
	 */
	protected $url;

	static public function test_url($url) {
		return preg_match('~^http[s]?://market\.yandex\.(ru|ua)/guru\.xml\?~iS', $url);
		}

	/** Parse a page
	 * @param string    $url    URL at Yandex Market
	 * @throws EData(0, 'URL') when the supplied URL is not pointing to Yandex Market
	 */
	function __construct($url) {
		/* URL okay? */
		if (!self::test_url($url))
			throw new EData(0, 'URL', 'The supplied URL does not have product list!', compact('url'));
		$this->url = $url;
		}

	/** Found products
	 * @var YMproduct[]
	 */
	public $products = array();

	/** Parses the products list
	 * @throws ENet     Connection error
	 * @throws EParse   Tag parse error
	 * @throws EData    Combine error: parsing matching specs failed
	 */
	function Parse() {
		/* Load the page */				
		$N = YMImportControllerAdmin::$Netfsurf;
		$result = $N->Request($this->url);
		$f = $result->f;
		if (strpos($result->url, 'captcha') !== FALSE)
			throw new ECaptcha($result->f);
		
		$s = stream_get_contents($f);		
		$XML2 = str_get_html($s);
		
		$get = array();
		
		$div_page = $XML2->find('div[class="page__b-offers__guru"]',0);
		if (isset($div_page))
		{
			foreach($div_page->find('div[class="b-offers b-offers_type_guru"]') as $prod)
			{				
				$arr = array();
				$div_desc = $prod->find('div[class="b-offers__desc"]',0);
				
				if($div_desc)
				{
					$a = $div_desc->find('a',0);
					if ($a)
					{
						$arr['url'] = trim($a->href);
						$arr['model'] = trim($a->innertext);
						
						$p = $div_desc->find('p',0);
						
						if ($p)
						{
							$arr['spec'] = trim($p->innertext);
						
							$get[] = $arr;
						}
					}
				}
			}
		}
		/* Create objects */
		foreach ($get as $product)
			$this->products[] = new YMproduct($product['url'], $product['model'], $product['spec']);
	}
}

/** Parses one product
 */
class YMParser_Product {
	/** Product page URL
	 * @var string
	 */
	protected $url;

	static public function test_url($url) {
		return preg_match('~^http[s]?://market\.yandex\.ru/model\.xml\?~iS', $url);
		}

	/** Parse a page
	 * @param string    $url    URL at Yandex Market
	 * @throws EData(0, 'URL') when the supplied URL is not pointing to Yandex Market
	 */
	function __construct($url) {
		/* URL okay? */
		if (!self::test_url($url))
			throw new EData(0, 'URL', 'The supplied URL does not point to a product!', compact('url'));
		$this->url = $url;
		}

	/** Model name
	 * @var string
	 */
	public $model;
	
	/** Brand name
	 * @var string
	 */
	public $brand;

	/** Price
	 * NULL when no price available
	 * @var float|null
	 */
	public $price;

	/** Array of parsed SHORT properties:
	 * array( label => value )
	 * @var string[]
	 */
	public $specs_short = array();

	/** Array of parsed LONG properties:
	 * array( label => value )
	 * @var string[]
	 */
	public $specs_long = array();

	/** Array of parsed images
	 * The first one is the biggest
	 * @var YMimg[]
	 */
	public $imgs = array();

	/** Parse the product page
	 * @throws ENet     Connection error
	 * @throws EParse   Tag parse error
	 * @throws EData    Combine error: parsing matching specs failed
	 */
	function Parse() {
		exec_parse($this->url,$other,$imgs);
				
		$this->model = $other['model'][0];
		$this->brand = $other['brand'];
		$this->price = $other['price'];
		$this->spec_short = $other['spec_short'];
		$this->spec_long = $other['spec_long'];

		foreach ($imgs as $img)
			$this->imgs[] = new YMimg($img['href'], $img['thumb']);
		}
	}
?>
