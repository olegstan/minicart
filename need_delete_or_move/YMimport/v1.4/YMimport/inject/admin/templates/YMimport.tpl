<div id="page"><div id="content"><div id="cont_border"><div id="cont">

{literal}
<style>
	.exception { background: #FDD; margin-bottom: 20px; }
	.exception .title { font-weight: bold; }
	.exception .context { font-family: monospace; border-left: 3px solid #090; }
	.exception .backtrace { font-family: monospace; border-left: 3px solid #009; }
</style>
{/literal}


{if $mode=='form'}
		<h1 align=center>Импорт товаров из <font color=red>Я</font>ндекс.Маркета</h1>

		<form method=POST>
		<table class="" width="100%">
				<col width="20%"> <col width="70%">

			<tr><td class="model"> <label for="category">Целевая категория:</td>
				<td class="m_t">
					<select name="category_id" class="select2">
						{defun name=categories_select Categories=$Categories level=0}
							{foreach from=$Categories item=category}
								{if $CurrentCategory != $category->category_id}
									<option value='{$category->category_id}' {if $category->category_id == $Item->category_id}selected{/if} category_name='{$category->single_name}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name}</option>
									{fun name=categories_select Categories=$category->subcategories level=$level+1}
								{/if}
							{/foreach}
						{/defun}
						</select>
					</td>
				</tr>

			<tr>
				<td class="model"><label for="links">Ссылки на товары:</td>
				<td class="m_t">
					<textarea name="urls" rows=5 style="width:100%">{$POST_URLS}</textarea>
					<font color=gray">
						<ol>
							<li>Вы можете указать ссылки на конкретные товары (/model.xml): краткое описание будет представлено в виде таблицы.</li>
							<li>Вы можете указать ссылки на страницы со списком товаров (/guru.xml): Вам будет предложен выбор
							какие именно товары со страницы импортировать, а краткое описание будет взято строкой.</li>
							</ol>
						</font>
					</td>
				</tr>

			<tr><td colspan=2>
				<input type="submit" value="Импорт!" class="submit">
				</td></tr>

			</table>
		</form>

{elseif $mode=='invalid-urls'}

<h2>Ошибка!</h2>
<p>Некоторые ссылки не были распознаны как указывающие на товары Яндекс.Маркета:
<ul>
	{foreach from=$invalid item=url}
		<li>{$url}</li>
		{/foreach}
</ul>

<p>Вернитесь назад, и исправьте эти ссылки!

{elseif $mode=='choose'}

<h2>Выбор товаров для импорта</h2>

<p>Вы указали ссылки на страницы, содержащие несколько товаров.
<p><b>Выберите</b> из них те, которые нужно импортировать:</p>

<form method="POST">
	<input type="hidden" name="category_id" value="{$category_id}">
	{foreach from=$products item=product}
		<br><label><input type="checkbox" name="choose[]" CHECKED value="{$product->id}">{$product->model}</label>
		{/foreach}
	<br><input type="submit" value="Импорт!" class="submit">
	</form>

{elseif $mode=='finished'}

<h2>Успех!</h2>
<p><b>Все  <b>{$successful} товаров</b> успешно импортированы!

{elseif $mode=='exceptions'}

<h2>Возникли ошибки</h2>
<p>Успешно импортировано <b>{$successful} товаров из {$expected}</b></p>
<br><hr><br>
<p>Возникшие ошибки:</p>

{foreach from=$exceptions item=e}
	<div class="exception">
		<div class="title">{$e->class} : {$e->message}</div>
		<pre class="context">{$e->context}</pre>
		<pre class="backtrace">{$e->backtrace}</pre>
		</div>
	{/foreach}

{elseif $mode=='captcha'}

<h2>Возникли ошибки</h2>
<form method="POST">
	<input type="hidden" name="captcha_key" value="{$captcha_key}" />
	<textarea name="urls" style="display: none;">{$POST_URLS}</textarea>

	Яндекс просит ввести число с картинки (CAPTCHA).
	<br><img src="data:image/jpeg;base64,{$captcha_img}" />
	<br><input type="text" name="captcha" value="">
	<br><input type="submit" value="Ok">
	<br>
	<br>После ввода, пожалуйста, повторите свой запрос.
	</form>

{/if}






</div></div></div></div>