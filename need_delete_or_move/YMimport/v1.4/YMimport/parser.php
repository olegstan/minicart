<?php
	include('simple_html_dom.php');

	function exec_parse($url, &$other, &$imgs)
	{
	###############
	$ch = curl_init();
	
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	//curl_setopt($cr, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)');
	curl_setopt($ch, CURLOPT_TIMEOUT,30); 
	//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
	curl_setopt($ch, CURLOPT_COOKIE, "yandex_gid=213");

	$html_curl = curl_exec($ch); 
	curl_close($ch); 
	
	$html = str_get_html($html_curl);
	
	#################

		//Инициализируем библиотеку парсинга
		//$html = file_get_html($url);//str_get_html($f);
		
		//Получаем модель
		$other['model'][0] = $html->find('table[class="l-page l-page_layout_72-20"]',0)->find('h1[class="b-page-title b-page-title_type_model"]',0)->innertext;
		if (strpos($other['model'][0],'<')!=0)
			$other['model'][0] = substr($other['model'][0],0,strpos($other['model'][0],'<'));
			
		//Получаем бренд
		$tmp_brand = $html->find('#mdVendor a span',0)->innertext;
		if (isset($tmp_brand) && !empty($tmp_brand))
			$other['brand'] = trim($tmp_brand);
		else
			$other['brand'] = '';
		
		//Получаем цену
		$other['price'] = str_replace(chr(194).chr(160), '', $html->find('table[class="l-page l-page_layout_72-20"]',1)->find('span[class="b-prices__num"]',0)->innertext);
		
		//Краткое описание
		$other['spec_short'] = $html->find('ul[class="b-vlist b-vlist_type_mdash b-vlist_type_friendly"]',0)->outertext;
		
		//Подробное описание
		$spec_page = "http://market.yandex.ru".$html->find('p[class="b-model-friendly__title"]',0)->find('a',0)->href;		
		$html2 = file_get_html($spec_page);		
		$other['spec_long'] = $html2->find('table[class="b-properties"]',0)->outertext;
		
		//Изображения
			//Основное
			$imgs[0]['href'] = /*$other['img']['big']['href'] = */@$html->find('table[class="l-page l-page_layout_72-20"]',1)->find('span[class="b-model-pictures__big"]',0)->find('a',0)->href;
		
			$imgs[0]['thumb'] = /*$other['img']['big']['thumb'] = */@$html->find('table[class="l-page l-page_layout_72-20"]',1)->find('span[class="b-model-pictures__big"]',0)->find('img',0)->src;
						                                            
			if (!isset($imgs[0]['href']) || empty($imgs[0]['href']))
				$imgs[0]['href'] = $imgs[0]['thumb'];
			
			$ind = 1;
			//Дополнительные
			foreach($html->find('span[class="b-model-pictures__small"]') as $img_small) 
			{
				$imgs[$ind]['href'] = /*$other['img']['small']['href'][] = */@$img_small->find('a',0)->href;
				$imgs[$ind]['thumb'] = /*$other['img']['small']['thumb'][] = */@$img_small->find('img',0)->src;
				$ind++;
			}
		//Изображения считаны
	}
?>
