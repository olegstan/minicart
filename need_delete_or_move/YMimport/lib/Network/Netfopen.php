<?php
/** Simple interface for opening remote URLs and handling data streams
 *
 * @Name		class Netfopen
 * @Package		Network
 * @Date		16.12.2008
 * @Version		1.0
 * @Depends		Web, ENet
 * @Provides	Netfopen
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Netfopen
	{
	/** Current URL, with GET params
	 * @var string
	 */
	public $url;

	/** Context array
	 * @var array
	 */
	protected $ctx = array();

	static private $ONCE = false;

	/** Prepares a request
	 * @param string	$URL	URL to connect to. May contain GET arguments
	 * @param array		$GET	Array of GET arguments to urlencode: array( name => value )
	 * @param array		$POST	Array of POST data: array( name => value )
	 * @param array		$HEADS	Array of additional headers: array( name => value ). `User-Agent` is already set, but you can overwrite
	 */
	function __construct($URL, $GET = null, $POST = null, $HEADS = null)
		{
		if (!self::$ONCE)
			{
			if (class_exists('Logger', false))
				Logger::HError_Ignore(basename(__FILE__), 2);
			self::$ONCE = true;
			}

		/* Prepare */
		$this->url = $URL;
		/* Browser simulation */
		$this->ctx['http']['ignore_errors'] = true; // Since PHP 5.2.10
		$this->ctx['http']['user_agent'] = 'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30)';
		$this->ctx['http']['header'] = array(
			'Accept-Language: en-us,en;q=0.5',
			'Accept-Encoding: gzip,deflate',
			'Accept-Charset: windows-1251,utf-8;q=0.7,*;q=0.7',
			'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			);
		/* GET */
		if (!is_null($GET))
			{
			$GET = Web::urlenc($GET);
			$this->url .= (strpos($this->url, '?')? '&' : '?').$GET;
			}
		/* POST */
		if (!is_null($POST))
			{
			$this->ctx['http']['content'] = Web::urlenc($POST);
			$this->ctx['http']['method'] = 'POST';
			$this->ctx['http']['header'][] = 'Content-Type: application/x-www-form-urlencoded';
			}
			else
			$this->ctx['http']['method'] = 'GET';
		/* Headers */
		if (!is_null($HEADS))
			foreach ($HEADS as $n => $v)
				$this->ctx['http']['header'][] = $n.': '.$v;
		$this->ctx['http']['header'] = implode("\r\n",$this->ctx['http']['header']);
		}

	/** Use an HTTP(S) proxy
	 * @param mixed $proxy_str	"ip:port", or `NULL` to clear the value
	 */
	function setopt_proxy($proxy_str)
		{
		if (!is_null($proxy_str))
			$this->ctx['http']['proxy'] = $proxy_str;
			elseif (isset($this->ctx['http']['proxy']))
			unset($this->ctx['http']['proxy']);
		}

	/** Set max redirects count
	 * @param mixed $n	int to enable, 1 to disable auto redirect handling, `null` to use the default
	 */
	function setopt_max_redirects($n)
		{
		if (!is_null($n))
			$this->ctx['http']['max_redirects'] = $n;
			elseif (isset($this->ctx['http']['max_redirects']))
			unset($this->ctx['http']['max_redirects']);
		}

	/** Sometimes it can be useful to bind to a specific local address
	 * @param string	$host	IP/host to use. '0' to use the current
	 * @param int		$port	Port number to use. `0` to use any
	 */
	function bindto($host, $port)
		{ $this->ctx['socket']['bindto'] = $host.':'.$port; }

	/** The reply headers are stored here.
	 * When a connection error occurs - it is empty array().
	 * Otherwise - it's an array(
	 * 		'code' => 200,
	 * 		'http' => '1.0',
	 * 		'set-cookie' => array( "name=value; path=/; expires=Fri, 18 Jul 2036 13:20:00 GMT; domain=.mail.ru", ... ),
	 * 		'content-type' => 'text/html', // Spaces are trimmed, !!! header-names lowercased
	 * 		...
	 * 		)
	 * @var array
	 */
	public $reply = array();

	/** Copy of the descriptor, returned by Request()
	 * @var resource
	 */
	public $f = false;

	/** Make the prepared request. After it finished - you can access the $this->reply property
	 * @param float $timeout	Read timeout in seconds
	 * @return resource A stream you can read. Also stored in $this->f
	 * @throws ENet(0, 'Netfopen', 'connect') on connection failure
	 * @throws ENet(1, 'Netfopen', 'HTTP') on HTTP error code received (see $this->reply['code'])
	 */
	function Request($timeout = 3.0)
		{
		/* prepare */
		$this->ctx['http']['timeout'] = $timeout;
		$context = stream_context_create($this->ctx);
		$this->reply = array();
		$this->f = false;
		/* request */
		$this->f = @fopen($this->url, 'r', FALSE, $context);
		/* results */
		if (!isset($http_response_header))
			throw new ENet(0,'Netfopen','connect','Connection failed', array('URL' => $this->url) );
			else
			foreach ($http_response_header as $i => $header)
				if ($i == 0)
					{
					$header = explode(' ', $header, 3);
					$this->reply['code'] = (int)$header[1];
					$this->reply['http'] = substr($header[0], strpos($header[0],'/')+1);
					}
					elseif (count($header = explode(':', $header, 2)) == 2)
					{
					$name = strtolower(trim($header[0]));
					$value = trim($header[1]);
					switch ($name) {
						case 'set-cookie':
							if (!isset($this->reply['set-cookie']))
								$this->reply['set-cookie'] = array();
							$this->reply['set-cookie'][] = $value;
							break;
						default:
							$this->reply[  $name  ] = $value;
						}
					}
		if (!$this->f)
			throw new ENet(1,'Netfopen','HTTP','HTTP '.$this->reply['code'].' error received', array('URL' => $this->url) );
		/* finish */
		return $this->f;
		}
	}

?>