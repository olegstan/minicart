<?php
/** Fucking cURL replacement, based on Network/Netfopen.
 * This handles redirections, referrers, cookies, and can store cookies to a file.
 * Alas, no SOCKS supported yet.
 *
 * @Name		class NetfSurf
 * @Package		Network
 * @Date		Apr 15, 2009
 * @Version		1.2
 * @Depends		Netfopen, URLworks, ENet
 * @Provides	NetfSurf
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class NetfSurf
	{
	protected $cookie_jar;

	/** Creates a new, independent web surfer
	 * @param string $cookie_jar	When specified, cookies can be stored in some file
	 * @throws EFile(Perm) when cookie file exists, but not readable
	 */
	function __construct($cookie_jar = null)
		{
		$this->cookie_jar = $cookie_jar;
		if (file_exists($cookie_jar))
			{
			$this->cookies = unserialize(file_get_contents($cookie_jar));
			if ($this->cookies === FALSE)
				{
				$this->cookies = array();
				throw new EFile(EFile::Perm($cookie_jar, 'CookieJar','r'));
				}
			}
		}

	protected $headers = array();

	/** Add a static header: this will be automatically added to every request.
	 * @param string	$name	Name of the header. Case-sensitive!
	 * @param string	$value	Value of the header. New values overwrite older ones. `null` deletes a header.
	 */
	function set_header($name, $value)
		{
		if (is_null($value))
			{
			if (isset($this->headers[$name]))
				unset($this->headers[$name]);
			}
		$this->headers[$name] = $value;
		}

	protected $bindto = null;

	/** Allows using a custom network interface available on host machine.
	 * @param string	$host	Specify the host. '0' will use any host.
	 * @param int		$port	A specific port to use while connecting. '0' uses any available port.
	 */
	function set_interface($host, $port = 0)
		{ $this->bindto = array( $host, $port ); }

	protected $timeout = 5.0;

	/** Sets timeout for requests: connection, and I/O
	 * @param float $timeout	Timeout in seconds
	 * @return float The previous timeout
	 */
	function set_timeout($timeout)
		{
		$ret = $this->timeout;
		$this->timeout = $timeout;
		return $ret;
		}

	protected $raise_http_errors = false;

	/** Whether to raise exceptions on HTTP errors.
	 * This is false by default
	 * @param bool $enable	Enable this option?
	 */
	function raise_http_errors($enable) {
		$this->raise_http_errors = $enable;
	}

	protected $retry_count = array(0, 3);

	/* Netfsurf will try to repeat requests on error.
	 * (0,3) is the default, and disables retries
	 * @param	int	$times		The number of times to try to reconnect
	 * @param	int	$interval	Seconds to sleep between retries. When skipped - then the previous value is used instead
	 */
	function set_retry($times, $interval = null) {
		if (is_null($interval))
			$interval = $this->retry_count($interval);
		$this->retry_count = array($times, $interval);
	}

	/** Array of cookies
	 * @var array ( name => array( value, live_until, path, is_secure ))
	 */
	protected $cookies = array();

	/** Results of the last query
	 * @var Netfresult
	 */
	protected $result;

	protected $referer = null;

	/** Makes a request
	 * @param string	$URL	URL to connect to. May contain GET arguments
	 * @param array		$GET	Array of GET arguments to urlencode: array( name => value )
	 * @param array		$POST	Array of POST data: array( name => value )
	 * @param array		$HEADS	Array of additional headers: array( name => value ). `User-Agent` is already set, but you can overwrite.
	 * @return Netfresult
	 * @throws ENet(0, 'Netfopen', 'connect') on connection failure
	 * @throws ENet(1, 'Netfopen', 'HTTP') on HTTP error codes (only when raise_http_errors(true) )
	 */
	function Request($URL, $GET = null, $POST = null, $HEADS = null) {
		/* Wrapper to support retries */
		$ret = null;
		$exception = null;
		for ($try = -1; $try < $this->retry_count[0]; $try++) {
			try { $ret = $this->__Request($URL, $GET, $POST, $HEADS); }
				catch (ENet $e) {
					$exception = $e;
					sleep($this->retry_count[1]);
					//TODO: Notify the logger
					continue;
				}
			return $ret;
		}
		throw $exception;
	}

	function __Request($URL, $GET = null, $POST = null, $HEADS = null)
		{
		/* Prepare result */
		$this->result = new Netfresult;
		/* Prepare cookies */
		//TODO: discard timed-out cookies
		//TODO: use `path`
		//TODO: use `secure`
		$cookies = array();
		foreach ($this->cookies as $name => $cookie)
			$cookies[] = sprintf('%s=%s', self::_cookie_encode($name), self::_cookie_encode($cookie['value']));
		/* Prepare headers */
		$HDs = array();
		if (!is_null($this->referer))
			$HDs['Referer'] = $this->referer; // referer
		$HDs += $this->headers; // static headers
		if (is_array($HEADS)) $HDs += $HEADS; // custom headers
		if (count($cookies))
			{
			if (isset($HDs['Cookie']))
				$HDs['Cookie'] .= '; '.implode('; ', $cookies);
				else
				$HDs += array('Cookie' => implode('; ', $cookies)); // cookies
			}
		/* Prepare request */
		$netfopen = new Netfopen($URL, $GET, $POST, $HDs);
		$netfopen->setopt_max_redirects(1); // disable auto redirections: else we're unable to handle cookies!
		$this->result->url = $netfopen->url;
		if ($this->bindto)
			$netfopen->bindto($this->bindto[0], $this->bindto[1]);
		/* Do request */
		try { $this->result->f = $netfopen->Request($this->timeout); }
			catch (ENet $e)
			{
			if (!is_resource($this->result->f))
				$this->result->f = null;
			if ($e->operation == 'HTTP') // will not throw HTTP error
				{
				if ($this->raise_http_errors && !in_array($netfopen->reply['code'],array(301,302,303,307))) // TODO: 4xx & 5xx, not only 302,303
					throw $e; // Raise an error is enabled
				}
				else // connection error: throws
				{
				$this->result->code = null;
				throw $e;
				}
			}
		/* Prepare results */
		$this->result->headers = $netfopen->reply; unset($this->result->headers['code'],$this->result->headers['http']);
		$this->result->code = $netfopen->reply['code'];
		$this->result->http_version = $netfopen->reply['http'];
		/* Save cookies */
		if (isset($this->result->headers['set-cookie']))
			{
			foreach ($this->result->headers['set-cookie'] as $cookiestr)
				{
				$cook = self::parse_cookie_str($cookiestr);
				if ($cook === FALSE)
					{
					trigger_error('Netfsurf: Failed to parse cookie string '.var_export($cookiestr,1).'!', E_USER_NOTICE);
					continue;
					}
				$cook_name = $cook['name']; unset($cook['name']);
				if (is_null($cook['value']))
					{
					if (isset($this->cookies[$cook_name]))
						unset($this->cookies[$cook_name]);
					}
					else
					$this->cookies[$cook_name] = $cook;
				}
			unset($this->result->headers['set-cookie']);
			if ($this->cookie_jar)
				file_put_contents($this->cookie_jar, serialize($this->cookies));
			}
		/* result cookies */
		foreach ($this->cookies as $cook_name => $cook)
			$this->result->cookies[$cook_name] = $cook['value'];
		/* Redirection? */
		if (in_array($this->result->code, array(301,302,303,307)))
			{
			if (is_resource($this->result->f))
				fclose($this->result->f);
			/* presave */
			$this->result->redirects[] = $this->result->url;
			$redir_sav = $this->result->redirects;
			/* relative location? */
			$location = $this->result->headers["location"];
			if (strpos($location, '://') === FALSE)
				{
				$URLw = new URLworks($this->result->url);
				$location = $URLw->URI($location);
				}
			/* go */
			$exception_sav = null;
			try { $this->Request($location, null, null, $HEADS); }
				catch (ENet $e) { $exception_sav = $e; }
			/* finish */
			$this->result->redirects = array_merge($redir_sav, $this->result->redirects);
			if (!is_null($exception_sav))
				throw $e;
			}
		/* Return the results */
		$this->referer = $this->result->url;
		return $this->result;
		}

	/** Parse a cookie responce string
	 * @param string $str	mrcu=67E949E517EF4C54115FC23943C2; path=/; expires=Fri, 18 Jul 2036 13:20:00 GMT; domain=.mail.ru; secure
	 * @return array (
	 * 		'name'		=>	string,	// name of the cookie
	 * 		'value'		=>	string | NULL, // value of the cookie
	 * 		'path'		=>	string | NULL,
	 * 		'expires'	=>	int | NULL, // UNIT timestamp
	 * 		'domain'	=>	string | NULL
	 * 		'secure'	=> bool, // is secure: true/false
	 * 		)
	 * @return false on parse failed
	 */
	static function parse_cookie_str($str)
		{
		if (preg_match_all('~\s*([^=]+=?(?:[^;]+)?);?\s*~', $str, $m) == 0)
			return FALSE;
		$cook = array(
			'name'		=> null,
			'value'		=> null,
			'path'		=> null,
			'expires'	=> null,
			'domain'	=> null,
			'secure'	=> false,
			);
		foreach ($m[1] as $i => $piece)
			{
			$piece = explode('=', $piece, 2);
			if ($i == 0)
				{
				$cook['name'] = self::_cookie_decode($piece[0]);
				if (strlen($piece[1])!=0)
					$cook['value'] = self::_cookie_decode($piece[1]);
				}
				else
				{
				$piece[0] = strtolower($piece[0]);
				if ($piece[0] != 'secure')
					$cook[$piece[0]] = $piece[1];
					else
					$cook['secure'] = true;
				}
			}
		if (!is_null($cook['expires']))
			$cook['expires'] = strtotime($cook['expires']);
		return $cook;
		}

	static function _cookie_encode($str) { return str_replace(array("\t",' ','=',';'),array('%09','%20','%3D','%3B'),$str); }
	static function _cookie_decode($str) { return rawurldecode($str); }
	}

class Netfresult
	{
	/** The current URL (either a requested one, or a redirection result)
	 * @var string
	 */
	public $url;

	/** Redirections array: an URL where a redirection was met is stored here
	 * Empty array when got a page without any redirections
	 * @var array
	 */
	public $redirects = array();

	/** Remote host reply code
	 * IS NULL when a connection error has occured (and an exception is thrown in Request())
	 * @var int
	 */
	public $code = null;

	/** Stream descriptor with data from the remote server
	 * IS NULL when an HTTP error has occured
	 * @var resource
	 */
	public $f = null;

	/** HTTP version at the remote host
	 * @var string
	 */
	public $http_version = '1.1';

	/** Reply headers
	 * @var array ('content-type' => 'text/html', ...)
	 */
	public $headers = array();

	/** Array of cookies for the host that are still actual
	 * @var array ( name => value , ... )
	 */
	public $cookies = array();
	}

?>