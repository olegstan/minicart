<?php
/** A pack of funcs & classes useful for buiulding sites & scripts
 *
 * @Name		class EWeb
 * @Date		12.11.2008
 * @Version		1.0
 * @Provides	EWeb, EWeb404, EWebHeadersSent, EWebNoData, EWebData
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

// TODO; make it more nice!
// TODO: make use of additional fields
// TODO: check everything!

/** Mostly internal website error */
class EWeb extends EBase {
	/** The page where an error has occured */
	public $URL;

	const E404 = 0;
	const EHEADERS_SENT = 1;
	const EWEBNODATA = 2;
	const EWEBDATA = 3;

	/* Yu can define your own errors exceeding the value of this constant */
	const EUSER = 1000;

	/** A 'web error': occurs at an URL with a user message. Is a mostly internal one
	 * @param int 		$code		One of EWeb:: constants
	 * @param string	$message	Exception message
	 * @param string	$URL		URL of the page. If null, then is copied from $_SERVER['REQUEST_URI']
	 * @param array 	$context	compact()-snapshot of meaningful context variables. Any additional data can go here
	 */
	function __construct($code, $message, $URL = null, $context = null)
		{
		if (is_null($URL))
			$URL = $_SERVER['REQUEST_URI'];
		$this->URL = $URL;
		parent::__construct($code, get_class($this), $message, $context);
		}
	}

/** Navigation error */
class EWeb404 extends EWeb {
	/** The page title that was not found */
	public $title;

	/** A 'Page not found' error
	 * @param string	$title		Title of the page
	 * @param string	$URL		URL of the page. if null, then is copied from $_SERVER['REQUEST_URI']
	 * @param mixed		$context	compact()-snapshot of meaningful context variables
	 */
	function __construct($title = '', $URL = null, $context = null)
		{
		$this->title = $title;
		parent::__construct(EWeb::E404, sprintf("Page `%s` not found.", $title), $URL, $context);
		}
	}

/** "Headers sent" error */
class EWebHeadersSent extends EWeb {
	public $headers_sent_place = array('file' => '', 'line' => 0);

	/** Error when headers were sent, but should not
	 * @param mixed		$context	compact()-snapshot of meaningful context variables
	 */
	function __construct($context = null)
		{
		headers_sent($this->headers_sent_place['file'], $this->headers_sent_place['line']);
		parent::__construct(EWeb::EHEADERS_SENT, 'Headers were already sent', null, $context);
		}
	}

/** Missing arguments error */
class EWebNoData extends EWeb {
	/** Missing arguments error
	 * @param array $get_args	Expected GET arguments
	 * @param array $post_args	Expected POST arguments
	 * @param array $files_args	Expected FILES
	 * @param array $context	compact()-snapshot of meaningful context variables
	 */
	function __construct($get_args = null, $post_args = null, $files_args = null, array $context = null) {
		$missing = array( 'GET' => array(), 'POST' => array(), 'FILES' => array() );
		foreach (array('get' => '_GET', 'post' => '_POST','files' => '_FILES') as $check => $V)
			foreach ($$check as $varname)
				if (!isset($$V[ $varname ]))
					$missing[  strtoupper($check)  ][] = $varname;
		foreach ($missing as $n => &$v)
			if (count($v) == 0) unset($missing[$n]);
				else $missing[$n] = sprintf('%s(%s)', $n, implode(', ', $v) );

		parent::__construct(EWeb::EWEBNODATA, 'Not enough request data provided! Missing: '.implode('; ', $missing), null, $context);
		}
	}

/** Errors that can be debugged using the input data, which is encapsulated in the error class. */
class EWebData extends EWeb {
	/** The input data key and its value */
	public $key,$value;

	/** HTTP headers */
	public $headers = array();
	/** The $_GET array */
	public $get = array();
	/** The $_POST array */
	public $post = array();
	/** user session */
	public $session = array();
	/** uploaded files */
	public $files = array();

	/** Errors that can be debugged using the input data, which is encapsulated in the error class.
	 * @param string	$key		Name of the input data key
	 * @param mixed		$value		Its value which has an error
	 * @param string	$message	Error message
	 * @param mixed		$context	compact()-snapshot of meaningful context variables
	 */
	function __construct($key, $value, $message, $context = null) {
		parent::__construct(EWeb::EWEBDATA, $message, null, $context);
		$this->get = $_GET;
		$this->post = $_POST;
		$this->headers = Web::getHeaders();
		$this->session = $_SESSION;
		$this->files = $_FILES;
		}
	}
?>