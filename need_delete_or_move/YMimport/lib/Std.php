<?php
/** 
 * Std class
 * 
 * 
 * @Name		class Std
 * @Date		Jan 10, 2010
 * @Version		1.0
 * @Provides	Std
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Std {
	/** Determines whether a variable is not set, and initializes it with a default
	 * @param mixed $var		The variable to check.
	 * @param mixed $default	The default value to set if missing
	 * @return bool TRUE when is set, false when not set
	 * @example
	 * 		if (varset($a[0][0], 0)) $a[0][0]++;
	 */
	static function varset(&$var, $default) {
		if (!isset($var)) { $var = $default; return false; }
		return true;
		}

	/** Returns a variable reference. If it's not set - initialize it with the $default
	 * @param mixed $var		The variable to check.
	 * @param mixed $default	The default value to set if missing
	 * @return &mixed
	 * @example
	 *		$CACHE = &varsetref($GLOBALS['$C5$'][$CLASS]['$CACHE'][$name], array());
	 */
	static function &varsetref(&$var, $default) {
		if (!isset($var)) $var = $default;
		return $var;
		}
	}
?>