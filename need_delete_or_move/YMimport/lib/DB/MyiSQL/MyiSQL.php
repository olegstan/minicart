<?php
/**
 * A class that handles MySQL-Improved features as i like them to be =)
 * - Delayed connections
 * - Automatically frees all the resources
 * - Allows to handle several result sets
 * - Advanced Injection-Filtering
 * - Lightweight query constructor
 * - Automatic table-prefix insertion
 *
 *
 * @Name		class MyiSQL
 * @Date		Feb 22, 2010
 * @Version		1.0
 * @Depends		MyiSQLd, MyiSQL_Result, EMyiSQL, MyiSQL_Mkq, MyiSQL_Stmt
 * @Provides	MyiSQL
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

Autoload::init_modules('myisql', null, array(
	'emyisql'=>'DB/MyiSQL/EMyiSQL.php',
	'emyisql_mkq'=>'DB/MyiSQL/EMyiSQL.php',
	'emyisql_sys'=>'DB/MyiSQL/EMyiSQL.php',
	'emyisql_connect'=>'DB/MyiSQL/EMyiSQL.php',
	'emyisql_query'=>'DB/MyiSQL/EMyiSQL.php',
	'emyisql_stmt'=>'DB/MyiSQL/EMyiSQL.php',
	));

class MyiSQL extends mysqli {
	/** Connection data copy
	 * @var MyiSQLd
	 */
	protected $imd;

	/** Store connexion info, and DO NOT connect currently
	 * @param MyiSQLd $connexion	MyiSQLd object: connection data
	 */
	function __construct(MyiSQLd $imd) {
		$this->imd = clone $imd;
		$this->imd->l = $this;
		}

	function __destruct() {
		$this->disconnect();
		}

	/** Get the connection data object. Do not modify it!
	 * @return MyiSQLd
	 */
	function get_imd() {
		return $this->imd;
		}

	function __clone() {
		$this->imd = clone $this->imd;
		if ($this->connected) {
			$this->connected = false;
			$this->connect();
			}
		}

	/** Is connected?
	 * @var bool
	 */
	private $connected = false;

	/** Establish a [p]connection to MySQL server with the data provided to the constructor
	 * @param bool $persistent	TRUE to make a persistent connection (@since PHP 5.3.0)
	 * @return MyiSQL
	 * @throws EMyiSQL(setopt)
	 * @throws EMyiSQL_Connect(conn)
	 * @throws EMyiSQL_Sys(charset)
	 */
	function connect($persistent = false) {
		if ($this->connected) return $this;
		// Prepare
		$host = ($persistent && is_null($this->imd->socket)? 'p:' : '').$this->imd->host;
		parent::init();
		// Options
		foreach ($this->imd->options as $opt => $val)
			if (!parent::options( $opt, $val ))
				throw new EMyiSQL($this->imd, 'setopt', compact('opt'));
		// SSL settings
		if (!is_null($this->imd->ssl_sets)) {
			parent::ssl_set(/*TODO: SSL settings*/);
			}
		// Connect
		if (!parent::real_connect(
				$host, $this->imd->login, $this->imd->pass,
				$this->imd->database, $this->imd->port,
				$this->imd->socket,
				$this->imd->flags
				))
			throw new EMyiSQL_Connect($this->imd, 'conn');
		$this->connected = true;
		// Charset
		if (!is_null($this->imd->charset))
			if (!parent::set_charset($this->imd->charset))
				throw new EMyiSQL_Sys($this->imd, 'charset', array('charset' => $this->imd->charset));
		// Init handler
		if (!is_null($this->imd->init))
			call_user_func($this->imd->init, $this->imd, $this);
		// Finish
		return $this;
		}

	/** Disconnect from the MySQL server
	 * @throws EMyiSQL(disc)
	 */
	function disconnect() {
		if (!$this->connected)
			return;
		if (!parent::close())
			throw new EMyiSQL($this->imd, 'disc');
		$this->connected = false;
		}

	/** Check the connection, and reconnect if possible
	 * @throws EMyiSQL_Connect(reconn)
	 * @return bool True
	 */
	function ping() {
		if (parent::ping())
			return TRUE;
		// Connection failure
		$this->connected = false;
		//TODO: reconnection is not always enabled
		}

	/** Selects the default database
	 * @throws EMyiSQL_Sys(select,noconn)
	 */
	function select_db($name) {
		if (!$this->connected)
			throw new EMyiSQL_Sys($this->imd, 'noconn');
		if (!parent::select_db($name))
			throw new EMyiSQL_Sys($this->imd, 'select', compact('name'));
		}

	/** Add the table prefix
	 */
	protected function _tbl_prefix(&$query) {
		if (is_null($this->imd->table_prefix))
			return;
		if (!$this->connected)
			throw new EMyiSQL_Sys($this->imd, 'noconn');
		if (is_null($query))
			$query = $this->query;
		if (!is_object($query) && strpos($query, '`^') !== FALSE)
			$query = str_replace('`^', '`'.$this->imd->table_prefix, $query);
		}

	/** The last query
	 * @var string|MyiSQL_Mkq
	 */
	public $query = null;

	/** Prepare a new Mkq query and bind data to it (optinal)
	 * @return MyiSQL_Mkq
	 * @throws EMyiSQL_Mkq(no_key,no_prop,wr_type)
	 */
	function make($query, &$data = null) {
		$this->_tbl_prefix($query);
		return new MyiSQL_Mkq($this, $query, $data);
		}

	/** Quick version of $M->query($M->make($query, $data)) with data *copying*
	 * Call query(null), multi_query(null) or prepare(null) afterwise!
	 * @return MyiSQL
	 * @throws EMyiSQL_Mkq(no_key,no_prop,wr_type)
	 */
	function qmake($query, $data) {
		$this->_tbl_prefix($query);
		$this->query = new MyiSQL_Mkq($this, $query, $data);
		return $this;
		}

	/** Execute a query to the database.
	 * For SELECT,SHOW,DESCRIBE,EXPLAIN it returns a result set.`null` otherwise.
	 * @param string|MyiSQL_Mkq	$query	The query
	 * @param bool				$use	`USE_RESULT` is unbuffered: the result is transferred while you're reading. Useful for huge ones.
	 * @return MyiSQL_Result
	 * @throws EMyiSQL_Query(query)
	 */
	function query($query, $use = false) {
		$this->_tbl_prefix($query);
		// Query
		if (!parent::real_query($query))
			throw new EMyiSQL_Query($this->imd, 'query', $query);
		// Does it have a result set?
		if ($this->field_count == 0)
			return null;
		// Yes, it has!
		return new MyiSQL_Result($this, $use);
		}

	private $_mquery_id = 0, $_mquery_more = true;

	/** Execute multiple MySQL queries ';'-sep.
	 * Use multi_query_next() to get them
	 * @param string|MyiSQL_Mkq	$query	The query
	 * @throws EMyiSQL_Query(mquery)
	 */
	function multi_query($query) {
		$this->_tbl_prefix($query);
		// Query
		if (!parent::multi_query($query))
			throw new EMyiSQL_Query($this->imd, 'mquery', "Multi-Query #0");
		$this->_mquery_id = 0;
		$this->_mquery_more = true;
		}

	/** Get a multi_query result set
	 * @param MyiSQL_Result		$result		Output result variable, which is null for queries without results
	 * @param bool				$use		`USE_RESULT` is unbuffered
	 * @return bool Whether there's anything to read now
	 * @throws EMyiSQL_Query(mquery)
	 */
	function multi_query_next(&$result, $use = false) {
		// Were there anything else?
		if (!$this->_mquery_more)
			return false;
		// Any result set?
		$result = ($this->field_count==0)? null : new MyiSQL_Result($this, $use);
		// Any more?
		if (!parent::more_results()) {
			$this->_mquery_more = false;
			return !is_null($result);
			}
		// Move to the next
		$this->_mquery_id++;
		if (!parent::next_result())
			throw new EMyiSQL_Query($this->imd, 'mquery', "Multi-Query #$this->_mquery_id");
		return $this->_mquery_more;
		}

	/** Create a new prepared statement.
	 * You can use Mkq's '?'-placeholders even if you don't provide that object
	 * @param string|MyiSQL_Mkq	$query			The query
	 * @param string			$query_name		System name of the query. Is used in Exceptions instead of full query text
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(stmt)
	 */
	function prepare($query, $query_name = null) {
		$this->_tbl_prefix($query);
		// Create
		return new MyiSQL_Stmt($this,$query, $query_name);
		}
	}
?>