<?php
/**
 * Collection of classes used to implement active records
 *
 * @Name		class MyiSQL
 * @Date		May 3, 2010
 * @Version		1.0
 * @Depends		MyiSQL_StmtPack
 * @Provides	ARecord, IARecord, AProudRecord
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */


/** An active record abstraction: a class that's linked with a database
 */
abstract class ARecord {
	/** Associated StmtPack
	 * @var MyiSQL_StmtPack
	 */
	private $_MPack;

	/** Statements that were prepared for this instance and bound to it.
	 * Each statement can be bound only once
	 * @var MyiSQL_Stmt[]
	 */
	private $_stmts = array();

	/** The last used statement
	 * If used to detect the last_insert_id()
	 * @var MyiSQL_Stmt|null
	 */
	private $_stmt_last = null;

	/** The last used statement with results
	 * @var MyiSQL_Stmt|null
	 */
	private $_stmt_res = null;

	/** Bind this object with a StmtPack
	 */
	function __construct(MyiSQL_StmtPack $MPack) {
		$this->_MPack = $MPack;
		}

	final private function __clone() {} // Cloning does not unbind args so fuck it!

	/** Get an StmtPack assigned to the object
	 * @var MyiSQL_StmtPack
	 */
	protected function _db_get_pack() {
		return $this->_MPack;
		}

	/** Store a query in StmtPack
	 * @param string	$name	Name of the query. It'll be prefixed with 'Simpla/<className>'
	 * @param string	$query
	 * @param string[]	$result_names
	 * @return MyiSQL_StmtPack
	 */
	protected function _db_store($name, $query, $result_names = null) {
		return $this->_MPack->store($name, $query, $result_names);
		}

	/** Prepare a statement for me
	 * @param string	$name			Name of the statement to prepare
	 * @param bool		$bind_stmt		Bind placeholders to me. Set FALSE if you'd like to bind to another object
	 * @param bool		$bind_result	Bind results to me. Set FALSE if you'd like to bind to another object
	 * @param bool		&$reuse			Whether a new Stmt was bound to this instance or an old one was reused
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	protected function _db_prepare($name, $bind_stmt, $bind_result, &$reuse = null) {
		/* === Prepare the query */
		$reuse = isset($this->_stmts[$name]);
		if ($reuse)
			// This query was not previously prepared for this instance
			$stmt = $this->_stmts[$name];
			else {
			// Prepare a new one
			$stmt = $this->_MPack->prepare_unpacked($name);
			if (is_null($stmt)) // No statement with such name was found
				trigger_error("ARecord '".get_class($this)."' requested query '$name' but none found in StmtPack '{$this->_MPack}'", E_USER_ERROR);
			// Bind it
			if ($bind_stmt)
				$stmt->bind_stmt($this);
			if ($bind_result)
				$stmt->bind_result_object($this);
			// Finish
			$this->_stmts[$name] = $stmt;
			}
		/* === Handle stmt */
		$this->_stmt_last = $stmt;
		/* === Handle results */
		if ($bind_result) {
			// Reset the prev query and bind a new one
			if (!is_null($this->_stmt_res))
				$this->_stmt_res->reset();
			$this->_stmt_res = $stmt;
			// Link this one
			if (!$reuse)
				$stmt->bind_result_object($this);
			}
		return $stmt;
		}

	function __destruct() {
		foreach ($this->_stmts as $name => $stmt)
			$this->_MPack->repack($name, $stmt);
		}


	/** Fetch one result of the last query with $bind_result=true
	 * This uses only the last query with results: no insert/... will spoil it!
	 * @param bool	$reset	Reset immediately: set to TRUE if you need only one result
	 * @return bool Whether there's anything else to fetch
	 * @throws EMyiSQL_Stmt(*)
	 */
	function db_next($reset = false) {
		if (is_null($this->_stmt_res))
			return false;
		$more = $this->_stmt_res->fetch();
		if (!$more || $reset) {
			$this->_stmt_res->reset();
			$this->_stmt_res = null;
			}
		return $more;
		}
	
	/** Get the number of rows affected by the last query
	 * @return int
	 */
 	function db_affected() {
		return $this->_stmt_last->affected_rows;
		}

	/** Get the number of rows from the last query with $bind_result=true
	 * @return int
	 */
	function db_count() {
		return $this->_stmt_res->num_rows;
		}

	/** Get the last query's Insert Id
	 * @return int
	 */
	function db_insert_id() {
		return $this->_stmt_last->insert_id;
		}

	/** Test everything: prepare all queries, bind them, check for errors
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function db_test() {
		foreach ($this->_MPack->get_names() as $name)
			$stmt = $this->_db_prepare($name, true, true);
		}
	}






/** Common interface for all ActiveRecords
 * They define standard methods to work with the database
 */
interface IARecord {}
interface IARecord_Modifyable extends IARecord {
	/** Changes fields' values so they conform certain standards.
	 * Execute this function in insert/update/replace calls.
	 * It MUST call $stmt->exec()
	 * @access protected
	 * @return int Affected rows
	 */
	function _modify_refinery(MyiSQL_Stmt $stmt);
	}

interface IARecord_Selectable extends IARecord {
	/** Changes fields' values so they conform certain standards.
	 * Execute this function in select calls.
	 * It MUST call $stmt->exec()
	 * @access protected
	 * @return int Resulting rows
	 */
	function _fetch_refinery(MyiSQL_Stmt $stmt);

	/** Select all rows from the table that match a criteria.
	 * Use db_next() and db_rows() to navigate thorough the results
	 * @param string|string[]|null	$where		An array of property names to use in the WHERE clause
	 * @return int The number of matching rows
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function select($where = null);
	}
interface IARecord_Insertable extends IARecord_Modifyable {
	/** Insert this object
	 * @return int The number of affected rows
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function insert();
	}
interface IARecord_Replaceable extends IARecord_Modifyable {
	/** Replace this object
	 * @return int The number of affected rows
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function replace();
	}
interface IARecord_Updateable extends IARecord_Modifyable {
	/** Update a matching rows with data from this object
	 * @param string|string[]|null	$where	An array of property names to use in the WHERE clause
	 * @return int The number of affected rows
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function update($where = null);
	}
interface IARecord_Deleteable extends IARecord {
	/** Delete the matching rows
	 * @param string|string[]|null	$where	An array of property names to use in the WHERE clause
	 * @return int The number of affected rows
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function delete($where = null);
	}
interface IARecord_Limitable extends IARecord {
	//public $limit_count = 2000000000;
	//public $limit_offset = 0;
	
	/** Constrain the number of rows
	 * @param int	$count	The number of rows to return (2^31 by default)
	 * @param int	$offset	Offset of the first row
	 */
	function limit($count = 2000000000, $offset = 0);
	}






/** A Proud active record: it needs no external resources and restricts itself :)
 */
abstract class AProudRecord extends ARecord implements IARecord {
	/** Global StmtPack for all classes
	 * @var MyiSQL_StmtPack
	 */
	private static $_MPack = array();

	/** Array of StmtPacks for every className
	 * @var MyiSQL_StmtPack[]
	 */
	private static $_MPackClones = array();

	/** Bind a StmtPack that will be used for all instances of this class and its descendants
	 * Each instance will get a namespace prefix of <ClassName>
	 * @param MyiSQL_StmtPack	$MPack		The pack to use
	 */
	public static function StmtPack(MyiSQL_StmtPack $MPack) {
		self::$_MPack = $MPack;
		}

	/** Choose StmtPack to use for this instance. It's called only once for every registered class
	 * You can override it to choose another StmtPack. Don't clone it!
	 * @param string	$className	The calling class name
	 * @return MyiSQL_StmtPack
	 */
	protected function _choose_stmtpack($className) {
		if (is_null(self::$_MPack))
			trigger_error("AProudRecord '$className' was not bound to any StmtPack with '$className::StmtPack()'", E_USER_ERROR);
		return self::$_MPack;
		}


	/** ClassName of this instance
	 * @var string
	 */
	private $_myClass;

	/** Create the ARecord.
	 * It calls the _db_init() method that initializes local queries
	 */
	function __construct() {
		$this->_myClass = get_class($this);
		// Clone the StmtPack
		$MPackClone = &self::$_MPackClones[$this->_myClass];
		$mk_clone = !isset($MPackClone);
		if ($mk_clone) {
			$MPackClone = clone $this->_choose_stmtpack($this->_myClass);
			$this->_db_init_restrict($MPackClone, $this->_myClass);
			}
		// Allow this class to use it
		parent::__construct($MPackClone);
		// Initialize
		if ($mk_clone)
			$this->_db_init();
		}

	/** Restrict StmtPack of this class with a namespace.
	 * The function is called only once when the class is declared.
	 * You can override it or make an empty function.
	 * @param MyiSQL_StmtPack	$MPack		Restriction target
	 * @param string			$className	Name of this instance
	 */
	protected function _db_init_restrict(MyiSQL_StmtPack $MPack, $className) {
		$MPack->restrict($className);
		}

	/** This function should _db_store() queries that this class uses.
	 * It's called only once when the class is declared.
	 * The registered queries are available for all instances.
	 */
	abstract protected function _db_init();
	}
?>