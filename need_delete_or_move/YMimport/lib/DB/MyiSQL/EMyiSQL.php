<?php
/**
 * MyiSQL exceptions
 *
 *
 * @Name		class EMyiSQL
 * @Date		Feb 22, 2010
 * @Version		1.0
 * @Depends		EBase
 * @Provides	!EMyiSQL, !EMyiSQL_Mkq, !EMyiSQL_Sys, !EMyiSQL_Connect, !EMyiSQL_Query, !EMyiSQL_Stmt
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */



/** General MyiSQL error.
 * Represents major errors that occur in a not-connected state (during startup)
 */
class EMyiSQL extends EBasePreset {
	/** MySQLd object which describes the error
	 * @var MyiSQLd
	 */
	public $imd;

	protected $_list = array(
			'setopt'	=> array(0, 'MyiSQL', 'Failed to set connection options', 1),
			'disc'		=> array(1, 'MyiSQL', 'Failed to disconnect', 1),
			);

	function __construct($imd, $nick, $context = null) {
		parent::__construct($nick, $context);
		$this->imd = $imd;
		}

	function __dump() {
		return parent::__dump() + array('MyiSQLd' => (string)$this->imd);
		}
	}


/** Prepare query errors
 */
class EMyiSQL_Mkq extends EMyiSQL {
	/** The buggy query
	 * @var string
	 */
	public $query;

	/** The data key which has an error
	 * @var string
	 */
	public $ph;

	/** The exact key which has an error
	 * @var string
	 */
	public $key;

	protected $_list = array(
		'no_key'		=> array(0, 'MyiSQL:Mkq', 'Key not found in array', 3),
		'no_prop'		=> array(1, 'MyiSQL:Mkq', 'Prop not found in object', 3),
		'wr_type'		=> array(2, 'MyiSQL:Mkq', 'Wrong processing type specified', 3),
		'no_blob'		=> array(3, 'MyiSQL:Mkq', 'Failed to find a BLOB by name', 2),
		'serial-^table'	=> array(4, 'MyiSQL:Mkq', 'The query contains table prefix which can\'t be serialized', 2),
		);

	function __construct($m, $nick, $query, $keys, $key, $context = null) {
		parent::__construct($m->get_imd(), $nick, $context);
		$this->query = $query;
		$this->name .= ":Mkq";
		$this->ph = is_null($keys) ? null : implode(',', $keys);
		$this->key = $key;
		}

	function __dump() {
		return parent::__dump() + $this->_dump_props(array('ph', 'key', 'query'));
		}
	}


/** MyiSQL feature/method error
 */
class EMyiSQL_Sys extends EMyiSQL {
	/** MySQL error number
	 * @var int
	 */
	public $mysql_errno;

	/** MySQL error message
	 * @var string
	 */
	public $mysql_error;

	/** The SQL state
	 * @var string[5]
	 */
	public $sqlstate;

	protected $_list = array(
		'charset'	=> array(0, 'MyiSQL:charset',	'Failed to set the client\'s charset', 1),
		'select'	=> array(1, 'MyiSQL:select-db',	'Failed to select a database', 1),
		'noconn'	=> array(2, 'MyiSQL:not-conn',	'The object is not connected.', 2),
		);

	function __construct($imd, $nick, $context = null) {
		parent::__construct($imd, $nick, $context);
		$this->mysql_errno = @$this->imd->l->errno;
		$this->mysql_error = @$this->imd->l->error;
		$this->sqlstate = @$this->imd->l->sqlstate;
		}

	function __dump() {
		return parent::__dump() + $this->_dump_props(array('mysql_errno', 'mysql_error', 'sqlstate'));
		}
	}



/** MyiSQL connection error
 */
class EMyiSQL_Connect extends EMyiSQL_Sys {

	protected $_list = array(
		'conn'		=> array(0, 'MyiSQL:connect', 'Connection failed', 1),
		'reconn'	=> array(1, 'MyiSQL:connect', 'Reconnection attempt failed', 1),
		);

	function __construct($imd, $nick) {
		parent::__construct($imd, $nick, null);
		$this->mysql_errno = $this->imd->l->connect_errno;
		$this->mysql_error = $this->imd->l->connect_error;
		}
	}



/** MyiSQL query error
 */
class EMyiSQL_Query extends EMyiSQL_Sys {
	/** The buggy query
	 * @var string
	 */
	public $query;

	protected $_list = array(
		'query'		=> array(0, 'MyiSQL:query', 'MySQL query error', 1),
		'mquery'	=> array(1, 'MyiSQL:query', 'MySQL multi-query error', 1),
		);

	function __construct($imd, $nick, $query, $context = null) {
		parent::__construct($imd, $nick, $context);
		$this->query = (string)$query;
		}

	function __dump() {
		return parent::__dump() + $this->_dump_props(array('query'));
		}
	}

/** MyiSQL Prepared Statement error
 */
class EMyiSQL_Stmt extends EMyiSQL_Query {

	protected $_list = array(
		'stmt'			=> array(0, 'MyiSQL:stmt',				'MySQL prepared statement error', 1),
		'bind_param'	=> array(1, 'MyiSQL:stmt:param-bind',	'Failed to bind Stmt params', 2),
		'send_blob'		=> array(2, 'MyiSQL:stmt:param-send',	'Failed to send BLOB data', 3),
		'store_result'	=> array(3, 'MyiSQL:stmt:store',		'Failed to store the results', 1),
		'bind_result'	=> array(4, 'MyiSQL:stmt:bind_result',	'Fields count does not match the query\'s field_count', 2),
		'fetch'			=> array(5, 'MyiSQL:stmt:fetch',		'Failed to fetch the results', 1),
		'notbound'		=> array(6, 'MyiSQL:stmt:fetch',		'The result was not bound to anything', 1),
		);

	function __construct(MyiSQL $m, $nick, $query, $context = null) {
		EMyiSQL_Sys::__construct($m->get_imd(), $nick, $context); //WOW!
		$this->query = (string)$query;
		}
	}
?>