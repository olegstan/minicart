<?php
/** A class that handles mysql features as i like them to be =)
 * - Delayed connexions
 * - Automatically frees all the resources
 * - Allows to handle several result sets
 * - Advanced Injection-Filtering
 * - Lightweight query constructor
 * - Automatic table-prefix insertion
 *
 * @Name		class MySQL
 * @Date		25.10.2008
 * @Depends		MySQLd, Str, EBase, EParse
 * @Provides	MySQL
 * @Version		3.1
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

//TODO: Show warnings
//TODO: Rewrite exceptions

//TODO: mkq should be a separate class! Implement Blob-Data in mkq

/** MySQL connexion
 */
class MySQL {
	/** Connexion data copy
	 * @var MySQLd
	 */
	protected $mysqld;

	/** Store connexion info, and DO NOT connect currently
	 * @param MySQL $connexion	MySQLd object: connection data
	 */
	function __construct(MySQLd $mysqld) {
		$this->mysqld = clone $mysqld;
		}

	public function __clone() {
		$this->mysqld = clone $this->mysqld;
		$this->m = null;
		if ($this->connected) {
			$this->connected = false;
			$this->connect();
			}
		}

	/** MySQL connexion. Is null when not connected (don't use it!)
	 * @var resource
	 */
	private $m = null;

	/** Is connected?
	 * @var bool
	 */
	private $connected = false;

	/** Establish a [p]connexion to the MySQL server, and selects a database
	 * When already connected - new connexion is not established
	 * @param bool $persistent	TRUE to make a persistent connexion, FALSE otherwise
	 * @throws EMySQL(0,'Connect') on connection error
	 * @throws EMySQL(<code>,'Query') on database selection or initial query error
	 * @return MySQL
	 */
	function connect($persistent = false) {
		if ($this->connected) return $this;
		/* Connect */
		$this->m = $persistent?
				@mysql_pconnect($this->mysqld->host, $this->mysqld->login, $this->mysqld->password) :
				@mysql_connect($this->mysqld->host, $this->mysqld->login, $this->mysqld->password, TRUE /* new link */);
		/* Success? */
		if (!$this->m)
			throw new EMySQL(mysql_error());
			else
			$this->connected = true;
		/* Select DB */
		if (!is_null($this->mysqld->database))
			$this->select_db();
		/* Queries */
		if (count($this->mysqld->onconnect_queries))
			foreach ($this->mysqld->onconnect_queries as $query)
				$this->query($query);
		/* Callback */
		if (!is_null($this->mysqld->onconnect_handler))
			call_user_func($handler,   $this->mysqld, $this);
		/* Finish */
		return $this;
		}

	/** Disconnect from the MySQL server
	 */
	function disconnect() {
		mysql_close($this->m);
		$this->m = null;
		$this->connected = false;
		}

	/** Is the object connected?
	 * @return bool
	 */
	function connected() {return $this->connected;}

	/** Get a MySQL connection resource. Is NULL when not connected.
	 * @return resource
	 */
	function getLink() { return $this->m; }

	/** Selects a database
	 * @param string $dbname	The database to select. When not specified - the default is used (from MySQLd)
	 * @throws EMySQL(<code>, 'Query')
	 * @return TRUE
	 */
	function select_db($dbname = null) {
		$this->query('USE `'.($dbname? $dbname : $this->mysqld->database).'`;');
		return TRUE;
		}

	/** Escape a variable so it can be used in queries securely.
	 * Objects are stringified. Streams are read & ihex()ed
	 * @param mixed $v
	 * @return string
	 */
	function i($v) {
		if (is_null($v)) return 'NULL';
		if (is_string($v) || is_object($v)) return '"' . mysql_real_escape_string($v, $this->m) . '"';
		if (is_bool($v)) return (int)$v;
		if (is_resource($v)) return $this->ihex(stream_get_contents($v));
		return (string)$v;
		}

	/** Format a variable as a string BLOB: X'...hex...'
	 * @param mixed $v
	 * @return mixed
	 */
	function ihex($v) {
		$ret = "X'";
		for ($i=0, $L=strlen($v); $i<$L; $i++)
			$ret .= Str::lpad(dechex(ord($v[$i])),2,'0');
		$ret .= "'";
		return $ret;
		}

	/** Last query string
	 * @param string
	 */
	public $query = null;

	/** Make a MySQL query.
	 * @param string	$query		The query to execute
	 * @param array		$data		When specified - array( key => value, ...}
	 * 								Its keys can be used in query as "{key}": securely replaced with values.
	 * 								{key->property} can be used to access object's properties
	 * 								{%Xkey} can be used to perform special encoding (also valid for properties):
	 * 									{%Xkey}	hex, {%skey} as is, {%dkey}	int, {%fkey} float, {%bkey}	bool: 1|0
	 * 									{%,key} inserts a safe ','-separated array of X
	 * @param bool		$unbuffered	Make an unbuffered MySQL query.
	 * 								The result set can be retrieved immediately, one by one, without waiting for it to be ready.
	 * 								You'll not be able to make other queries until this one is finished
	 * @throws EMySQL(<code>, 'Query') on query error
	 * @throws EParse(0, 'mkq') on makeQuery error when using templates
	 * @return MySQLresult	To handle the results
	 * You can use table prefixes: `^tablename` is auto-replaced with a chosen table prefix `[prefix]tablename` BEFORE $data insertion
	 */
	function query($query, array $data = null, $unbuffered = false) {
		if (!$this->connected)
			throw new EMySQL('Not connected');
		/* Replace table prefix */
		if (!is_null($this->mysqld->table_prefix))
			$query = str_replace('`^', '`'.$this->mysqld->table_prefix, $query);
		/* mkq */
		if (!is_null($data))
			$query = $this->mkq($query, $data);
		$this->query = $query;
		/* query */
		$r = $unbuffered? mysql_unbuffered_query($query, $this->m) : mysql_query($query, $this->m);
		if ($r === FALSE)
			throw new EMySQL($this, $query);
		/* finish */
		return new MySQLresult($this, $r);
		}

	/** Execute multiple MySQL queries.
	 * A result object for each is returned.
	 * @param string    $delim      Queries delimiter
	 * @param string    $queries    Queries delimited by $delim
	 * @param array     $data       Data for mkq
	 * @param bool      $unbuffered Unbuffered queries?
	 * @return MySQLresult[]
	 * @throws EMySQL(<code>, 'Query') on query error
	 * @throws EParse(0, 'mkq') on makeQuery error when using templates
	 * @return MySQLresult	To handle the results
	 */
	function multi_query($delim, $queries, array $data = null, $unbuffered = false) {
		$queries = explode($delim, $queries);
		$Rs = array();
		foreach ($queries as $q)
			$Rs[] = is_null($data)? $this->query($q) : $this->query($q, $data, $unbuffered);
		return $Rs;
		}

	/**
	 * @return int LAST_INSERT_ID()
	 */
	function ID() {return mysql_insert_id($this->m);}

	/** Replaces "{key}" in query $q with values from array $arr, escaped for secure use
	 * {key->property} can be used to access object's properties
	 * Special encodings (also valid for properties):
	 * 	{%Xkey}	hex, {%skey} as is, {%dkey}	int, {%fkey} float, {%bkey}	bool: 1|0
	 * 	{%,key} inserts a safe ','-separated array of X
	 * @param string $q
	 * @param array $arr
	 * @return string
	 */
	public function mkq($q, $arr) {
		$query = '';
		$last = 0;
		while(($pos = strpos($q, '{', $last))) {
			$query .= substr($q, $last, $pos - $last);
			/* locate new */
			$last = strpos($q, '}', $pos + 1);
			$key = substr($q, $pos + 1, $last++ - $pos - 1);
			if ($key[0] == '%') {
				$insert_mode = $key[1];
				$key = substr($key,2);
				} else
				$insert_mode = '';
			/* insert */
			$prop = null;
			if (strpos($key, '->') !== FALSE)
				list($key,$prop) = explode('->', $key, 2);

			if (!array_key_exists($key, $arr))
				throw new EParse(0, 'mkq', 'Missing key in mkq() $data', array('key' => $key, 'provided_keys' => array_keys($arr)));
			$val = $arr[$key];
			if (!is_null($prop)) {
				if (!property_exists($val, $prop))
					throw new EParse(0, 'mkq', 'Missing property in mkq() $data', array('key' => $key, 'prop' => $prop, 'provided_keys' => array_keys($arr)));
				$val = $val->$prop;
				}

			switch ($insert_mode) {
				case 'X': $val = $this->ihex($val); break; // hex
				case 's': break; // as is
				case 'd': $val = (int)$val; break; // integer
				case 'f': $val = (float)$val; break; // float
				case 'b': $val = $val? 1 : 0; break; // bool: 1,0
				case ',': foreach($val as &$v) $v = $this->i($v); $val = implode(",", $val); break; // ','-separated array
				default:
					$val = $this->i($val);
					break;
				}
			$query .= $val;
			}
		$query .= substr($q, $last);
		return $query;
		}
	}

//TODO: Deeper error reporting
class MySQLresult {
	/** Parent object
	 * @var MySQL
	 */
	private $m;
	/** MySQL result: TRUE for no-result, resource otherwise
	 * @var resource
	 */
	private $r;

	function __construct(MySQL $mysql, $result) {
		$this->m = $mysql;
		$this->r = $result;
		}

	/**
	 * @return int the number of rows from a result set.
	 */
	function N() {return mysql_num_rows($this->r);}

	/**
	 * @return int number of affected rows in previous MySQL operation.
	 */
	function A() {return mysql_affected_rows($this->m->getLink());}

	/**
	 * @return int LAST_INSERT_ID()
	 */
	function ID() {return $this->m->ID();}

	/** Returns information on every field in the result set
	 * @return array Array(object,...). @see mysql_fetch_field()
	 * @throws EMySQL(<code>, 'Query') on fetch data error
	 */
	function fields() {
		if (!(  $n = mysql_num_fields($this->r)  ))
			throw new EMySQL($this->m, 'mysql_num_fields()');
		$ret = array();
		for ($i=0; $i<$n; $i++)
			if (!(  $ret[$i] = mysql_fetch_field($this->r, $i)  ))
				throw new EMySQL($this->m, 'mysql_fetch_field()');
		return $ret;
		}

	/** Fetch a row from the result set [and strip slashes]
	 * @param array	$row	Output array
	 * @param int	$type	MYSQL_NUM, MYSQL_ASSOC, MYSQL_BOTH
	 * @param bool	$strip	TRUE to strip the slashes
	 * @return TRUE/FALSE whether there was any data
	 */
	function fetch(&$row, $type = MYSQL_ASSOC, $strip = false) {
		$row = mysql_fetch_array($this->r, $type);
		if (!$row) return FALSE;
		if ($strip)
			$row=array_map('stripslashes',$row);
		return TRUE;
		}

	/** Fetch an associative array.
	 * @return TRUE/FALSE whether there was any data ($row=FALSE also)
	 */
	function fetch_a(&$row) {
		return FALSE!==( $row = mysql_fetch_assoc($this->r) );
		}
	/** Fetch an object
	 * @param string    $className  Class to instantiate
	 * @param mixed[]   $args       Arguments for the constructor
	 * @return TRUE/FALSE whether there was any data ($row=FALSE also)
	 */
	function fetch_o(&$row, $className = null, array $args = null) {
		if (is_null($className))
			$row = mysql_fetch_object($this->r);
			elseif (is_null($args))
			$row = mysql_fetch_object($this->r, $className);
			else
			$row = mysql_fetch_object($this->r, $className, $args);
		return FALSE!==($row);
		}
	/** Fetch an indexed row.
	 * @return TRUE/FALSE whether there was any data ($row=FALSE also)
	 */
	function fetch_r(&$row) {
		return FALSE!==( $row = mysql_fetch_row($this->r) );
		}
	/** Fetch all rows as an array of associative arrays.
	 * @return mixed[][]
	 */
	function fetchall_a() {
		$ret = array();
		while ($this->fetch_a($row)) $ret[] = $row;
		return $ret;
		}
	/** Fetch all rows as an array of indexed arrays.
	 * @return mixed[][]
	 */
	function fetchall_r() {
		$ret = array(); $row = array();
		while ($this->fetch_r($row)) $ret[] = $row;
		return $ret;
		}
	/** Fetch all columns as a one-dimensional array
	 * @param mixed $col	When specified, this is the name|index of a column to take. When null - the whole array is fetched (assoc)
	 * @param mixed $icol	When specified, this is the name|index of a indexing column. E.g. row_id
	 * @param mixed $rmicol	Remove $icol array key to prevent duplicate data
	 * @return array
	 */
	function fetchall_one($col = 0, $icol = null, $rmicol = false) {
		/* How to fetch */
		if (is_null($icol)) {
			$how = is_int($col)? MYSQL_NUM : MYSQL_ASSOC;
			} else {
			if (is_null($col))
				$how = is_string($icol)? MYSQL_ASSOC : MYSQL_NUM;
				elseif (is_int($col) && is_int($icol))
				$how = MYSQL_NUM;
				elseif (is_string($col) && is_string($icol))
				$how = MYSQL_ASSOC;
				else
				$how = MYSQL_BOTH;
			}
		/* Perform */
		$ret = array();
		while ($this->fetch($row, $how)) {
			if (is_null($col)) {
				$add = $row;
				if ($rmicol)
					unset($add[$icol]);
			} else
				$add = $row[$col];
			if (is_null($icol))
				$ret[] = $add;
				else
				$ret[  $row[$icol]  ] = $add;
			}
		return $ret;
		}

	/** Fetch one column from the result
	 * @param mixed $col	When specified, this is the name|index of a column to take
	 * @return scalar | FALSE on none
	 * @example while (FALSE !== $col = $R->One()) print $col;
	 */
	function One($col = 0) {
		$how = is_int($col)? MYSQL_NUM : MYSQL_ASSOC;
		if (!$this->fetch($row, $how)) return FALSE;
		return $row[$col];
		}

	function __destruct() {
		if (is_resource($this->r))
			mysql_free_result($this->r);
		}
	}

//TODO: error classes: NOTCONNECTED, CONNECT, QUERY, RESULTS-API and different arguments
//TODO: substitude file:line to point to the call place
class EMySQL extends EBase {
	function __construct($mysql, $query = '') {
		if (is_string($mysql))
			parent::__construct(0, 'Connect', $mysql);
			else {
			$link = $mysql->getLink();
			parent::__construct(mysql_errno($link), 'Query', mysql_error($link), array('query' => $query));
			}
		}
	}
?>