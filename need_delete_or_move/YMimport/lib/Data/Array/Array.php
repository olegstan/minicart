<?php
/** Extended arrays functionality
 *
 * @Date		24.11.2008
 * @Version		1.0
 * @Depends		MultiIterator, EData
 * @Provides	Arr
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Arr {
	/** recursively initializes MISSING keys of $arr with defaults from $default */
	static function defaults(&$arr, array $default) {
		//TODO: CHECK!
		foreach ($default as $key => $value) {
			if (!isset($arr[$key])) $arr[$key] = $value;
			if (is_array($value))
				self::defaults( $arr[$key], $value );
			}
		}

	/** Searches an item in an multidimensional array
	 * @return array Keys-path to the value: array( key1, key2, ... )
	 */
	static function search_r($needle, array $haystack, $strict = FALSE) {
		foreach ($haystack as $k => $v)
			if (is_array($v) && ($ret = self::search_r($needle, $v)) !== FALSE)
				return array_merge(array($k), $ret);
				elseif ($strict?($v === $needle) : ($v == $needle))
				return array($k);
		return FALSE;
		}

	/** If an array has equal keys on every inclusion level, you can swap these levels.
	 * @param array	$array	The array to reorder
	 * @param int[]	$map	Level ids map. E.g. array( 0 => 3, 1 => 2, 2 => 1, 3 => 0 ) will reverse it
	 * @throws EData('Arr')	when $map length does not match $array depth
	 */
	static function relevel(array $array, array $map) { //TODO: test how it handles missing keys. Do not create them
		/* === Analyze the levels */
		$LVLs = self::getlevels($array);
		$depth = count($LVLs);
		if ($depth != count($map))
			throw new EData(0, 'Arr', 'Arr::relevel failure: $map length does not match the $array depth', array(
				'array_depth' => $depth,
				'map_length' => count($map),
				));
		/* === Create the iterator */
		// Count the limits
		foreach ($LVLs as $lvl => $keys)
			$maxs[$lvl] = count($keys)-1;
		// Create
		$I = new MultiIterator($depth, 0, $maxs);
		/* ======[ Iterate ]====== */
		$ret = array();
		do {
			// Map the value
			$src = &$array; $dst = &$ret;
			$src_keys = array(); $dst_keys = array();
			foreach ($I->pos as $src_lvl => $src_key_id) {
				// Navigate the source
				$src_key = $LVLs[$src_lvl][$src_key_id];
				$src = &$src[$src_key];
				// Navigate the destination
				$dst_lvl = $map[$src_lvl];
				$dst_key_id = $I->pos[$dst_lvl];
				$dst_key = $LVLs[$dst_lvl][$dst_key_id];
				$dst = &$dst[$dst_key];
				}
			// Copy
			if (isset($src))
				$dst = $src;
			} while ($I->inc());
		// Finish
		return $ret;
		}

	/** Get the list of array keys on each level of the array
	 * @param array	$array	The array to analyze
	 * @return string[][]
	 */
	static function getlevels($array, $_lvl=0, &$_keys = array()) {
		if (!isset($_keys[$_lvl]))
			$_keys[$_lvl] = array();
		foreach ($array as $k => $v) {
			if (!in_array($k, $_keys[$_lvl]))
				$_keys[$_lvl][] = $k;
			if (is_array($v))
				self::getlevels($v, $_lvl+1, $_keys);
			}
		return $_keys;
		}
	}
?>