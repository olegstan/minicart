<?php
/** Common string additions collection
 *
 * @Name		class Str
 * @Date		08.12.2009
 * @Version		1.0
 * @Provides	Str
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class str {
	/** @return string|bool A substr from between $from..$till, or FALSE on not found. */
	/** Extract a substring between two anchors
	 * @param string	$s		Haystack
	 * @param string	$from	Leading anchor
	 * @param string	$till	Trailing anchor
	 * @param int		$offset	Haystack offset to start searching from
	 * @return string|bool	A substring, or FALSE on any anchor not found
	 */
	static function extract($s,$from,$till, $offset=0) {
		if (FALSE === $pos=strpos($s,$from, $offset)) return FALSE;
		$end=$pos+strlen($from);
		if (FALSE === $pos2=strpos($s,$till,$end)) return FALSE;
		return substr($s, $end, $pos2 - $end);
		}

	/** String left padding */
	static function lpad($input,$length,$padder=' ') {return str_pad($input,$length,$padder,STR_PAD_LEFT);}
	/** String right padding */
	static function rpad($input,$length,$padder=' ') {return str_pad($input,$length,$padder,STR_PAD_RIGHT);}
	/** String both padding */
	static function pad($input,$length,$padder=' ') {return str_pad($input,$length,$padder,STR_PAD_BOTH);}

	/** Choose data size suffix
	 * @param int	$sz	Data size in bytes, been modified
	 * @return int Data size suffix id: array( 0 => bytes, 1 => KBytes, ..., 8 => 'YBytes' )
	 */
	static function bytesz_suffix_id(&$sz) {
		$rem = 0; // remainder
		$i=0; // the current suffix
		while ($sz >= 1024 && $i++ < 8) {
			$rem = (($sz & 0x3ff) + $rem) / 1024;
			$sz = $sz >> 10; # /1024
			}
		$sz += $rem;
		return $i;
		}

	/** Data size units suffixes, english
	 * @var string[]
	 */
	static protected $_szsi = ' KMGTPEZY';

	/** Print fancy data size
	 * @param int	$size		File size in bytes
	 * @param int	$decimals	Round decimals
	 * @return string
	 */
	static function bytesz($size, $decimals=3) {
		$id = self::bytesz_suffix_id($size);
		return round($size, $decimals).' '.self::$_szsi[$id].'ib';
		}
	}

?>
