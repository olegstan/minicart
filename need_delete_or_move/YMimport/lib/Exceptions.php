<?php
/** Exceptions collection
 *
 * @Name		Exceptions
 * @Date		19.11.2008
 * @Version		1.1
 * @Depends		EBase
 * @Provides	EFile, EDir, EParse, EData, EAction, ENet, EInterface
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** Files operation Exception
 * Common arguments:
 * 		string	$fname		Filename
 * 		string	$ftitle		File title. Example: "log" -> "LogFile"
 * 		array	$context	compact()-snapshot of meaningful context variables
 */
class EFile extends EBase {
	protected static $REASON = array(
		0	=> array('GENERAL',		'General failure with %s-file "%s".'),
		1	=> array('NRESOURCE',	'The supplied argument is not a file resource.'),

		10	=> array('EXISTS',		'%s-file "%s" exists, while it should not.'),
		11	=> array('NEXISTS',		'%s-file "%s" does not exist, while it should.'),
		12	=> array('PERM',		'Insufficient permissions for %s-file "%s". Need `%s`.'),

		20	=> array('STAT',		'%s failed for %s-file "%s".'),
		21	=> array('OPEN',		'Failed to fopen("%s","%s") %s-file. Check permissions!'),
		22	=> array('TOUCH',		'Failed to touch("%s") %s-file.'),
		23	=> array('TMPFILE',		'Failed to create a temporary %s-file.'),
		24	=> array('DELETE',		'Failed to delete %s-file.'),
		);

	/** General EFile constructor
	 * @param array $EFILE	Resulting array from one of the EFile:: static methods
	 */
	function __construct(array $EFILE) {
		$R = ($this instanceof EDir)? EDir::$REASON : EFile::$REASON;
		if (!isset($R[$EFILE[0]]))
			trigger_error(sprintf('EFile triggered at `%s:%s` with INVALID EFile code (%d)!!!', $this->file, $this->line, $EFILE[0]),E_USER_ERROR);
		parent::__construct($EFILE[0],$R[$EFILE[0]][0],vsprintf($R[$EFILE[0]][1],$EFILE[1]),$EFILE[2]);
		}

	/** General file error */
	static function General($fname, $ftitle, $context = null)	{return array(0, array($ftitle, $fname), $context);}
	/** Not a resource, but expected to be */
	static function NResource($context = null)					{return array(1,array(),$context);}
	/** File exists, while should not */
	static function Exists($fname, $ftitle, $context = null)			{return array(10, array($ftitle, $fname), $context);}
	/** File does not exist, while should */
	static function NExists($fname, $ftitle, $context = null)			{return array(11, array($ftitle, $fname), $context);}
	/** Insufficient permissions to access file. $need_perm="read" */
	static function Perm($fname, $ftitle, $need_perm, $context = null)	{return array(12, array($ftitle, $fname, $need_perm), $context);}
	/** Filestat failed: mtime(), filesize(), etc. $func="mtime" */
	static function Stat($fname, $ftitle, $func, $context = null)	{return array(20, array($func, $ftitle, $fname), $context);}
	/** Failed to fopen()|file_get_contents() file. $mode="r"|"w"|.... */
	static function Open($fname, $ftitle, $mode, $context = null)	{return array(21, array($fname, $mode, $ftitle), $context);}
	/** Failed to touch() a file */
	static function Touch($fname, $ftitle, $context = null)			{return array(22, array($fname, $ftitle), $context);}
	/** Failed to tmpfile() file. */
	static function TmpFile($ftitle, $context = null)				{return array(23, array($ftitle), $context);}
	/** Failed to unlink() file. */
	static function Delete($ftitle, $context = null)				{return array(24, array($ftitle), $context);}
	}

/** Directory operation exceptions */
class EDir extends EFile
	{
	protected static $REASON = array(
		0	=> array('GENERAL',		'General failure with %s-dir "%s".'),
		1	=> array('NRESOURCE',	'The supplied argument is not a directory resource.'),

		10	=> array('EXISTS',		'%s-dir "%s" exists, while it should not.'),
		11	=> array('NEXISTS',		'%s-dir "%s" does not exist, while it should.'),
		12	=> array('PERM',		'Insufficient permissions for %s-dir "%s". Need `%s`.'),

		20	=> array('STAT',		'%s failed for %s-dir "%s".'),
		21	=> array('OPEN',		'Failed to opendir("%s") %s-dir. Check permissions!'),
		22	=> array('TOUCH',		'Failed to touch %s-dir.'),
		23	=> null,
		24	=> array('DELETE',		'Failed to delete %s-dir.'),
		25	=> array('CREATE',		'Failed to create %s-dir'),

		26	=> array('DEFINED',		'Path to %s-dir "%s" was already defined.'),
		27	=> array('NDEFINED',	'Path to %s-dir "%s" was not previously defined.'),
		);

	/** General EDir constructor
	 * @param array $EDIR	Resulting array from one of the EDir:: static methods
	 */
	function __construct(array $EDIR)
		{parent::__construct($EDIR);}

	/** Failed to opendir()|scandir() */
	static function Open($dname, $dtitle, $context = null)			{return array(21, array($dname, $dtitle), $context);}
	/** Failed to mkdir() */
	static function Create($dname, $dtitle, $context = null)		{return array(25, array($dname, $dtitle), $context);}
	/** Failed to rmdir() */
	static function Delete($dtitle, $context = null)				{return array(24, array($dtitle), $context);}
	/** Path to the directory was already defined */
	static function Defined($dtitle, $context = null)				{return array(26, array($dtitle), $context);}
	/** Path to the directory was not previously defined */
	static function NDefined($dtitle, $context = null)				{return array(27, array($dtitle), $context);}
	}



/** Parse error */
class EParse extends EBase
	{function __construct($code, $name, $message, $context = null){ parent::__construct($code,$name,$message,$context); }}

/** Wrond data provided */
class EData extends EBase
	{function __construct($code, $name, $message, $context = null){ parent::__construct($code,$name,$message,$context); }}

/** Wrong action */
class EAction extends EBase
	{function __construct($code, $name, $message, $context = null){ parent::__construct($code,$name,$message,$context); }}

/** Network errors */
class ENet extends EBase
	{
	/** Failed operation: usually 'HTTP', 'connect', 'read', 'write', ... */
	public $operation;

	function __construct($code, $name, $operation, $message, $context = null) {
		$this->operation = $operation;
		parent::__construct($code, $name, sprintf('`%s` failed: %s', $operation, $message), $context);
		}
	}

/** The requested interface is not defined (for classes that works with interfaces that implement functionality) */
class EInterface extends EBase
	{function __construct($code, $name, $iface, $context = null){ parent::__construct($code,$name,'Interface '.$iface.' is not defined',$context); }}
?>