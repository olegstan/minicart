<?php
/** Linear XML parsing tool: provides methods for tags searching and manipulation
 * WARNING: Memory-intensive! Takes about 1Mb of memory for 1000 tags parsed!
 *
 * @Name		class ParseXML
 * @Date		26.11.2008
 * @Version		2.0
 * @Depends		XMLtag, XMLtagMatch, EParse
 * @Provides	pXMLtag, ParseXML
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

//TODO: the `__dump()` method that returns an array

/** A Parsed-XMLtag with enhanched properties related to source context position */
class pXMLtag extends XMLtag
	{
	/** Replacement string for the tag. Set a string to replace the tag in the output with a new string.
	 * @var mixed
	 */
	public $replace = null;

	/** You can delete a tagpair. Works well in tandem with $replace: the deleted segment will be replaced with a string
	 * Set 1 to delete it with a closing tag
	 * Set 2 to delete it with a closing tag, and the contents within
	 * @var unknown_type
	 */
	public $delete = null;

	/** Position of the tag in some context, provided by the parser
	 * [0] => Position of the opening brace, [1] => Position of the closing brace
	 * @var array
	 */
	public $pos = array(0,0);

	/** ID of the tag in parsing context
	 * @var int
	 */
	public $id;

	/** Constructs a new parsed XMLtag.
	 * @param string	$tagstr		@see `class XMLtag`
	 * @param int		$pos		Position of the tag's opening brace in some context
	 * @throws EParse(0,'XMLtag') on XMLtag construction $tagstring parsing failure
	 */
	function __construct($tagstr, $id, $pos)
		{
		parent::__construct($tagstr);
		$this->id = $id;
		$this->pos = array( $pos, $pos + strlen($tagstr) + 1 );
		}

	function __toString()
		{
		if (!is_null($this->replace))
			return $this->replace;
		return parent::__toString();
		}
	}

class ParseXML
	{
	/** The source string
	 * @var src
	 */
	protected $src = '';

	/** Array of `PrsXMLtag`s
	 * @var array
	 */
	protected $tags = array();

	/** Number of tags in $tags array
	 * @var int
	 */
	protected $tagsN = 0;

	/** Charset array( from, to) for iconv */
	protected $charset = null;

	/** Parses tags from the input source.
	 * Each tag receives a $tag->id - global ordinal tag id
	 * @param string	$src		Source string to parse tags from
	 * @param string	$onlytags	Specify a ','|' '-separated list of tagNames to parse. Others are ignored: this will speed-up the parse, and take less memory
	 * 								Setting this to TRUE you enable the "extended mode": It handles HTML comments and XML "<![CDATA[" properly. NEVER do it until you really need it!
	 * @throws EParse(0,'XMLtag') on XMLtag construction $tagstring parsing failure (bad system error)
	 */
	function __construct($src, $onlytags = null)
		{
		if (is_resource($src)) $src = stream_get_contents($src); // Backward compatibility
		$this->src = $src;
		$this->_construct_action('pXMLtag', $onlytags);
		}

	/** A constructor subroutine.
	 * Need it because derived classes use different tag ClassNames
	 */
	protected function _construct_action($tagclass, $onlytags)
		{
		/* Choose the right PREG */
		if (is_string($onlytags))
			$PREG = sprintf(XMLtag::PREG_uTAGS, strtolower(strtr($onlytags, ' ,', '||')));
			else
			$PREG = is_null($onlytags)? XMLtag::PREG_TAGS : XMLtag::PREG_xTAGS;
		/* Match & add */
		if (preg_match_all($PREG, $this->src, $matches, PREG_OFFSET_CAPTURE))
		foreach ($matches[1] as $match)
			if (is_array($match)) // Comments in enhanched mode produce an empty string here
				$this->tags[] = new $tagclass($match[0], $this->tagsN++, $match[1]-1);
		}

	/** Set a charset to automatically translate all data that comes from the class
	 * @param string	$from	The original source's charset
	 * @param string	$to		The charset of output data you wish to see
	 */
	function Charset($from, $to = 'windows-1251')
		{$this->charset = array($from,$to.'//IGNORE');}

	/** Match the tags collection to select these you want
	 * @param string $tagstr	@see `class XMLtagMatch`
	 * @param string $operator	@see `class XMLtagMatch`
	 * @return array Array of `class XMLtag`s that matched. Already replaced tags are NEVER returned again.
	 * @throws EParse(0,'XMLtag') on XMLtagMatch $tagstr parsing failure
	 * @throws EData(0,'XMLtagMatcher') on wrong XMLtagMatch $operator
	 * Note: You can edit the tag attributes. @see `class pXMLtag::replace` && `class XMLtag::Attr()`
	 * @example foreach ($XML->Match('div', '=') as $tag) $tag->replace = '['.$tag->name.']';
	 */
	function Match($tagstr, $operator = '=')
		{
		/* All tags */
		if ($tagstr == '.') return $this->tags;
		/* Exact tags */
		$match = new XMLtagMatch($tagstr, $operator);
		$ret = array();
		foreach ($this->tags as $tag) /* @var $tag pXMLtag */
			if (is_null($tag->replace) && $match->Match($tag))
				$ret[] = $tag;
		return $ret;
		}

	/** Return all tags in the class
	 * @return array Array of pXMLtag
	 */
	function GetAll()
		{return $this->tags;}

	/** Return the number of tags in the class
	 * @return int
	 */
	function CountAll()
		{return $this->tagsN;}


	protected function _iconv($str)
		{
		if (is_null($this->charset)) return $str;
		return iconv($this->charset[0],$this->charset[1], $str);
		}

	/** Returns a string with all tag-modifications applied
	 * With no params - returns the whole string.
	 * Optional parameters allow you to control
	 * @param int	$start		Tag id: The resulting string starts from the specified tag's contents
	 * @param int	$end		Tag id: The resulting string ends at the beginning of the specified tag
	 * @param bool	$with_tags	TRUE to include tags in the results, FALSE not to include
	 * @return string
	 * ParseXMLtree uses this intensively
	 */
	function toString($start = null, $end = null, $with_tags = true)
		{
		if (is_null($start))
			{ $start = 0; $last = 0; }
			else $last = $this->tags[$start++]->pos[1]+1;
		if (is_null($end))
			{ $Ni = $this->tagsN; $end = strlen($this->src); }
			else $end = $this->tags[$Ni = $end]->pos[0];
		/* Work */
		$ret = '';
		for ($i=$start; $i<$Ni; $i++)
		if (		$with_tags == false
			  ||	!is_null($this->tags[$i]->replace)
			  ||	!is_null($this->tags[$i]->delete)
			  ||	$this->tags[$i]->isModified()
			) // Unmodified tags are taken out as a part of a string using $last
			{
			$tag = $this->tags[$i]; /* @var $tag pXMLtag */
			$ret .= substr($this->src, $last, $tag->pos[0] - $last);
			/* === DELETE */
			if (!is_null($tag->delete))
				{
				if ($tag->delete == 2)
					{ /* Aggressive delete */
					/* Find its pair */
					$pair = null; $balance = 1;
					if (isset($tag->pair))
						$pair = $tag->pair;
						elseif ($tag->closing === FALSE)
						for ($j=($i+1); $j<$Ni; $j++)
							if ($this->tags[$j]->name == $tag->name && ($balance += $this->tags[$j]->closing?-1:+1) == 0)
								{ $pair = $this->tags[$j]; break; }
					/* Delete */
					if (!is_null($pair))
						{
						if (!is_null($tag->replace)) $ret .= $tag->replace;
						$last = $pair->pos[1] + 1;
						$i = $pair->id /* +1 (continue makes this)*/;
						continue;
						}
						/* Else - simple delete */
					}
				/* Simple delete */
				if ($tag->delete == 1)
					{ $last = $tag->pos[1] + 1; continue; }
				}
			/* === REPLACE */
			if (!is_null($tag->replace))
				$ret .= $tag->replace;
				elseif ($with_tags)
				$ret .= '<'.$tag.'>';
			$last = $tag->pos[1] + 1;
			}
		/* Finish */
		$ret .= substr($this->src, $last, $end-$last);
		return $this->_iconv($ret);
		}

	function __toString() {
		return $this->toString();
		}
	}

?>