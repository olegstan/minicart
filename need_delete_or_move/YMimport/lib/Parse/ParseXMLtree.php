<?php
/** Tree XML parsing tool: provides methods for data search & fetch
 *
 * @Name		class ParseXMLtree
 * @Date		27.11.2008
 * @Version		2.0
 * @Depends		pXMLtag, XMLtagMatch, SimpleCacher
 * @Provides	tXMLtag, ParseXMLtree
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

//TODO: the `__dump()` method that returns an array

/** A Treed-XMLtag with enhanched properties related to source context position, and tree navigation */
class tXMLtag extends pXMLtag
	{
	function __construct($tagstr, $id, $pos) {return parent::__construct($tagstr, $id, $pos);}

	/** Static reference to a tag that is paired with this one: is set on __construct()
	 * That is,
	 * 	if ($closing===TRUE) $pair is a closer '/tag'
	 * 	if ($closing===FALSE) $pair is an opener 'tag'
	 * 	if ($closing===NULL) then the tag is single, and $pair is also NULL
	 * @var tXMLtag
	 */
	public $pair = NULL;

	/** Static level of the tag in the global tree: is set on __construct()
	 * For instance, in "<div><a><img></a></div>" the <img> tag has a level of 2
	 * @var unknown_type
	 */
	public $level = 0;

	/** Dynamic id of the tag within the whole matched set: is set on Match()
	 * That is, when Match() returns - each tag receives its id within the whole current matching set
	 * @see Below
	 * @var int
	 */
	public $tid = 0;

	/** Dynamic id of a matching group the tag belongs to
	 * @see Below
	 * @var int
	 */
	public $gid = 0;

	/** Dynamic id of a tag within a group in the matching set: is set on Match()
	 * That is, when Match() works on the previous matching set, each tag produces a group of inner tags.
	 * 		Each inner tag, that match the condition, receives $gid equals to its ordinal num in the group.
	 * 		Initially, all gids are equal to tids in the first match generation
	 * @example
	 * 		<div>	-> <img>	tid = 0, gtid = 0 \
	 * 				-> <a>		tid = 1, gtid = 1 | Group 0
	 * 				-> </a>		tid = 2, gtid = 2 /
	 * 		<div>	-> <div>	tid = 3, gtid = 0 \ Group 1
	 * 				-> <table>	tid = 4, gtid = 1 /
	 * @var int
	 */
	public $gtid = 0;
	}

class ParseXMLtree extends ParseXML
	{
	/** Array of root tags, such as <html>
	 * @var array Array of tXMLtag
	 */
	protected $root = array();

	/** The current matching set in groups, initially set equal to root
	 * @var array Array of [ array of tXMLtag]
	 */
	protected $match = array();

	/** $match, stored under a name using Save(), and retrieved using Load()
	 * @var array Array( 'name' =>  Array of tXMLtag )
	 */
	private $storedmatches = array();

	/** Cachers for XMLtagMatch && filters callbacks
	 * @var SimpleCacher
	 */
	static private $cache_match,$cache_filter;

	/** Parses all tags from the source, and builds a tree.
	 * NOTE: ONLY implicitly closed </tags> produce a sub-tree! Such as <li> do not produce it, and stay plain like <br />
	 * @param string $src
	 * @param string $onlytags	@see `class ParseXML`
	 * @throws EParse(0,'XMLtag') on XMLtag construction $tagstring parsing failure (bad system error)
	 * BENCHMARK: P4 3Ghz
	 * 		Document size :  '77.448 Kb'
	 * 		Total tags :  2173
	 * 		TIMER `PARSING`: 0.2241 sec
	 */
	function __construct($src, $onlytags = null)
		{
		/* Cachers */
		if (is_null(self::$cache_filter))
			self::$cache_filter = new SimpleCacher(50);
		if (is_null(self::$cache_match))
			self::$cache_match = new SimpleCacher(20);
		/* Debug && load */
		$this->DEBUG = false;
		if ($this->DEBUG) DEBUG::profile('total', true);
		if ($this->DEBUG) DEBUG::profile('parse', true);
		if (is_resource($src)) $src = stream_get_contents($src);
		$this->src = $src;
		$this->_construct_action('tXMLtag', $onlytags);
		if ($this->DEBUG) DEBUG::profile('parse', false);

		if ($this->DEBUG) DEBUG::profile('build tree', true);
		/* In order to parse even broken HTML - we'll assume that all tags are not closed and stay plain like <img>
		 * When we find an implicitly closed "/tag" - we search for its opener "tag", and pair them
		 */
		/* === Analyze closings */
		foreach ($this->tags as $i => $tag) /* @var $tag tXMLtag */
			if ($tag->closing)
				{
				/* @var $pair tXMLtag */
				$pair = null; // Pair not found
				/* Search backwards to find an opener */
				for ($j = ($i-1); $j >= 0; $j--)
					if ($this->tags[$j]->closing)
						{
						if (!is_null($this->tags[$j]->pair)) // Jump over a subset in order to prevent searching tags not really needed
							$j = $this->tags[$j]->pair->id;
						}
					elseif (is_null($this->tags[$j]->pair) && $this->tags[$j]->name == $tag->name)
						{ $pair = $this->tags[$j]; break; }
				if ($pair)
					{
					/* Bind */
					$tag->pair = $pair;
					$pair->pair = $tag;
					}
				}

		/* === Levels, root, and singleness for the others */
		$level = 0;
		foreach ($this->tags as $i => $tag) /* @var $tag tXMLtag */
			{
			$tag->level = $level;
			if ($level == 0) $this->root[] = $tag;
			if (is_null($tag->pair))
				$tag->closing = NULL;
				elseif ($tag->closing) $level--;
				else $level++;
			}
		if ($this->DEBUG) DEBUG::profile('build tree', false);
		/* === Initial match */
		$this->Reset();
		if ($this->DEBUG) DEBUG::profile('total', false);
		}

	function __destruct()
		{
		/* PHP seems to fail in freeing memory allocated for objects stored in other projects' properties,
		 * and this causes a really bad meory leak. I have to free the memory manually
		 * ( http://bugs.php.net/bug.php?id=46790 )
		 * Note, that a destructor for `class tXMLtag` fails to perform the task
		 */
		for ($i=0; $i<$this->tagsN; $i++)
			{
			if ($this->tags[$i]->pair) unset($this->tags[$i]->pair);
			unset($this->tags[$i]); //TODO: really need it?
			}
		}

	/** Numerate tags in the matching set: $tid,$gid,$gtid */
	protected function _NumerateMatch()
		{
		$tid = 0;
		foreach ($this->match as $gid => $group)
			if (count($group) == 0) $this->match[$gid] = array(NULL);
			else
			foreach ($group as $gtid => $tag) /* @var $tag tXMLtag */
				if (is_null($tag)) $tid++;
				else
				{
				$tag->tid = $tid++;
				$tag->gid = $gid;
				$tag->gtid = $gtid;
				}
		}

	/** Selects tags using rules applied to the current matching set, and produces a new matching set
	 * Initially, the matching set is equal to the root tags of a document.
	 * @return ParseXMLtree
	 * @throws EData(0|1|2,'ParseXMLtree') on argument syntax error
	 *
	 * @param string args...
	 * 	The function accepts variable number of arguments, where each is a matching rule with the following syntax:
	 * 	[motion][N][modifier] [$operator] [$tagstr]
	 * 	Spaces are optional
	 * 		`motion`: (Default: ">")
	 * 				- How to spawn new groups
	 * 				">"	- Dive N levels in each subtree of the current matching set.
	 * 					That is, 3 matched tags provide 3 new groups of inner tags, to which the rules are applied.
	 * 					Tags without subtrees do not create a new group.
	 * 				"|"	- Apply rules to the current matching set, without modifying it
	 *		`N`: (Default: 1)
	 * 				Specifies the number of levels for [motion], when [motion] is ">". >=0
	 * 		`modifier`: (Default: none)
	 * 				- Change behavior of [motion] operator.
	 * 				">" - Makes ">" to dive ">=N" levels deep: takes all tags with matching levels.
	 * 						(this looks like ">[5]>" in the result, when diving >=5 levels)
	 * 				"<"	- Makes ">" to dive "<=N" (and >=0) levels deep: takes all tags with matching levels
	 * 						(this looks like ">[5]<" in the result, when diving <=5 levels)
	 * 				"?"	- Is applied not only to `motion`, but to the whole Match() process:
	 * 						When any of the stages fails to produce a group:
	 * 							1. `motion`	A tag has no inner tags
	 * 							2. `tagstr` There's no matching tags in a group
	 * 						it's group is stored as `array( 0 => NULL)`, which is saved by ALL subsequent Match() & Filter() calls
	 * 						until is fetched using Get*() functions.
	 * 						To apply the "?" modifier to `motion` only - skip the `tagstr` part
	 * 						To apply the "?" modifier to `tagstr` only - specify "|" as the current `motion`
	 * 						So, when a group becomes empty - it's replaced with array(NULL)
	 * 				"&"	- Enables advanced grouping on motion: this places inner tags in the same group the parent tag occupied.
	 * 						Also implies the "?" modifier: when a group becomes empty - it's NULL now
	 * 		`$operator`: (Default: "=")
	 * 				@see `class XMLtagMatch`
	 * 		`$tagstr`: (Default: ".")
	 * 				@see `class XMLtagMatch`
	 * @example:
	 * <html>
	 * 	<body>
	 * 		<div>
	 * 			<a style="float: left">
	 * ...
	 * $XML->Match('body', '>2 * a style="left"') will match the <a> tag
	 */
	function Match(/* args... */)
		{
		//IDEA: Maybe, should replace the "NULL" concept with a special NULL-tag? e.g. id=-1
		static $PREG = null;
		if (is_null($PREG))
			$PREG = sprintf('~^'.  ('(?:([>|])?(\d*)?([><?&])?)')  .  ('\s?'.'(%s)?')  .  ('\s?'.'(.*)')  .'$~S'
				, XMLtagMatch::PREG_OPERATOR_CHARS);

		if (count($this->match) == 0 || func_num_args() == 0) return $this;
		foreach (func_get_args() as $func_arg)
			{
			/* ====== Prepare the argument ====== */
			if (!preg_match($PREG,$func_arg,$m))
				throw new EData(0,'ParseXMLtree','Unsupported match format',compact('func_arg'));
			list(,$motion,$motionN,$modifier,$operator,$tagstr) = $m;
			foreach (array('motion' => '>', 'motionN' => '1', 'modifier' => null, 'operator' => '=', 'tagstr' => null) as $v => $def)
				if ($$v === '') $$v = $def;
			if ($this->DEBUG) DEBUG::dump('Match(arg)', compact('func_arg','motion','motionN','modifier','operator','tagstr'));

			/* ====== Check the argument ====== */
			if ($motion == '|' && ($motionN != '1' || !is_null($modifier)))
				throw new EData(1,'ParseXMLtree','Don\'t use neither `N` nor `modifier` while `motion`="|"',compact('func_arg'));

			/* ====== Apply the motion [using a modifier] ====== */
			if ($motion == '>')
				{
				if ($this->DEBUG) DEBUG::profile('motion', true);
				/* === Make a level-matcher from the modifier & N */
				switch ($modifier) {
					case '?':
					case '&':
					case null:	$lvls = array( $motionN, $motionN ); break;
					case '>':	$lvls = array( $motionN, null ); break;
					case '<':	$lvls = array( 1, $motionN ); break;
					}
				if ($this->DEBUG) DEBUG::dump('Motion: levels += ', compact('lvls'));

				/* === Perform */
				$newmatch = array();
				foreach ($this->match as $group)
					{
					$newgroup = array();
					foreach ($group as $tag) /* @var $tag tXMLtag */
						/* Preserve old NULLs, and case modifier='?' and the tag can't spawn a subset */
						if (	is_null($tag) || (  ($modifier == '?' || $modifier == '&')
								&& ($tag->closing !== FALSE || ($tag->id+1) == $tag->pair->id) // Not an opener, or has no inner tags
							)) $newmatch[] = array(NULL);
						/* The tag CAN spawn a subset */
						elseif ($tag->closing === FALSE)
						{
						if ($modifier != '&') $newgroup = array();
						/* Walk thru tags that are inside */
						for ($i = $tag->id+1, $itill=$tag->pair->id; $i<$itill; $i++)
							{
							$dlvl = ($this->tags[$i]->level - $tag->level); // Delta-level
							if ( $dlvl >= $lvls[0] && (is_null($lvls[1]) || $dlvl <= $lvls[1]))
								$newgroup[] = $this->tags[$i]; // The tag has matched
								//IDEA: we can jump over subsets using $tag->pair: great speed improve!
							}
						/* Add a group */
						if ($modifier != '&')
							{
							if (count($newgroup) != 0)
								$newmatch[] = $newgroup;
								elseif ($modifier == '?')
								$newmatch[] = array(NULL);
							}
						}
					if ($modifier == '&')
						$newmatch[] = $newgroup;
					}
				$this->match = $newmatch;
				if ($this->DEBUG) DEBUG::profile('motion', false);
				}

			/* ====== Match ====== */
			if (!is_null($tagstr))
				{
				if (is_null( $matcher = &self::$cache_match->get_or_add($operator.$tagstr) ))
					$matcher = new XMLtagMatch($tagstr, $operator);
				if ($this->DEBUG) DEBUG::profile('match', true);
				$newmatch = array();
				foreach ($this->match as $group)
					{
					$newgroup = array();
					foreach ($group as $tag) /* @var $tag tXMLtag */
						if (is_null($tag) || $matcher->Match($tag))
							$newgroup[] = $tag;
					if (count($newgroup) != 0)
						$newmatch[] = $newgroup;
						elseif ($modifier == '?' || $modifier == '&')
						$newmatch[] = array(NULL);
					}
				$this->match = $newmatch;
				if ($this->DEBUG) DEBUG::profile('match', false);
				}
			}
		/* Finish */
		$this->_NumerateMatch();
		return $this;
		}

	/** Filter the current matching set using eval() conditions
	 * @param string $eval	PHPcode that receives $tag variable, and returns TRUE/FALSE whether to save it in the currrent matching set
	 * All the tags save their properties. NULL elements are preserved. Empty groups are removed
	 * FEATURE: You can explicitly set $tag=NULL here, and return TRUE
	 * FEATURE: You can use $tag->replace & $tag->delete here. @see `class pXMLtag`
	 * @return ParseXMLtree
	 */
	function Filter($eval)
		{
		/* Cache */
		if (is_null( $F = &self::$cache_match->get_or_add($eval) ))
			$F = create_function('&$tag', $eval);
		/* Exec */
		$mmod = false; // Matches modified
		foreach ($this->match as $gid => &$group)
			{
			$gmod = false; // Group modified
			foreach ($group as $i => $tag) /* @var $tag tXMLtag */
				if (!is_null($tag) && !$F($tag))
					{ unset($group[$i]); $gmod=true; }
			if (count($group) == 0 && $mmod=true)
				unset($this->match[$gid]);
				elseif ($gmod)
				$group = array_values($group);
			}
		if ($mmod)
			$this->match = array_values($this->match);
		/* Finish */
		return $this;
		}

	/** Walks thru the matching groups, and splits each on match to two different ones
	 * That is, every group is searched for a match, and separated in two
	 * @param string $tagstr
	 * @param string $operator	@see `class XMLtagMatch`
	 * @return ParseXMLtree
	 * @throws EParse(0,'XMLtag') on XMLtagMatch $tagstr parsing failure
	 * @throws EData(0,'XMLtagMatcher') on wrong XMLtagMatch $operator
	 */
	function Split($tagstr, $operator = '=')
		{
		$matcher = new XMLtagMatch($tagstr, $operator);
		$newmatch = array();
		foreach ($this->match as $group)
			{
			$newgroup = array();
			foreach ($group as $tag) /* @var $tag tXMLtag */
				if (is_null($tag) || !$matcher->Match($tag))
					$newgroup[] = $tag;
					elseif (count($newgroup) != 0)
					{
					$newmatch[] = $newgroup;
					$newgroup = array();
					}
			if (count($newgroup))
				$newmatch[] = $newgroup;
			}
		$this->match = $newmatch;
		$this->_NumerateMatch();
		return $this;
		}

	/** Reset the current matching set back to the original: root of the tree
	 * Tags' $tid,$gid,$gtid are initialized to a single gid=0, and numerated
	 * @return ParseXMLtree
	 */
	function Reset()
		{
		foreach ($this->root as $i => $tag) /* @var $tag tXMLtag */
			{
			$tag->gid = 0;
			$tag->tid = $tag->gtid = $i;
			}
		$this->match = array( 0 => $this->root);
		return $this;
		}

	/** Save the current matching set under a name. Later you can return to the parsing node using Load()
	 * @param string $name	Optional stored set name
	 * @return ParseXMLtree
	 */
	function Save($name)
		{
		$this->storedmatches[$name] = $this->match;
		return $this;
		}

	/** Restore previously stored matches set, including $tid,$gid,$gtid, and NULLs
	 * @param mixed $name	The same name passed to Save()
	 * @return ParseXMLtree
	 * @throws EData(0,'ParseXMLtree') when name of a stored matching set was not found
	 */
	function Load($name)
		{
		if (!isset($this->storedmatches[$name]))
			throw new EData(0,'ParseXMLtree', 'There\'s no stored matches with the provided name', compact('name'));
		$this->match = $this->storedmatches[$name];
		$this->_NumerateMatch();
		return $this;
		}

	/** Fetches the contents of tags in the matching set. Ignores grouping. Allows to use `class TextProc`
	 * @param array		&$out		Storage. A tag's contents takes one cell. NULLs preserved.
	 * 								Single/Closing are $html?stored as is:ignored
	 * @param bool		$html	TRUE to include tags in the output
	 * @param bool		$group	TRUE to concat results from group items, FALSE to store them as separate array elements
	 * @param array		$TXTp	You can use advanced text processing to modify the output. @see `class TextProc::Apply()`
	 * @return ParseXMLtree
	 * @throws EData(0|1|2, 'TextProc') when the TextProc script has an incorrect format
	 */
	function GetContents(&$out, $html = true, array $TXTp = null)
		{return $this->_GetContents($out,$html,null,null,$TXTp);}

	/** Fetches the HTML contents of tags in the matching set. Ignores grouping. Allows preg_match()
	 * @param array		&$out		@see GetContents()
	 * @param string	$preg		When specified - the PCRE regex to apply to the string, and $out[]=the array
	 * @param int		$match_id	When specified, an exact match from regex is taken, or NULL when nothing
	 * @return ParseXMLtree
	 */
	function GetHTML(&$out, $preg = null, $match_id = null)
		{return $this->_GetContents($out,true,$preg,$match_id,NULL);}

	/** Fetches the PLAINTEXT contents of tags in the matching set. Ignores grouping. Allows preg_match()
	 * @param array		&$out		@see GetContents()
	 * @param string	$preg
	 * @param int		$match_id	@see GetHTML
	 * @return ParseXMLtree
	 */
	function GetText(&$out, $preg = null, $match_id = null)
		{return $this->_GetContents($out,false,$preg,$match_id,NULL);}

	/** Collect the attribute value from the matching tags. Ignores grouping.
	 * When an atribute is missing - NULL is stored instead
	 * @param string	$attr_name	The name of the attribute to fetch
	 * @param array		&$out		Storage. A tag's attribute takes one cell. NULLs preserved.
	 * @return ParseXMLtree
	 */
	function GetAttrs($attr_name, &$out)
		{
		if (!is_array($out)) $out = array();
		foreach ($this->match as $group)
		foreach ($group as $tag) /* @var $tag tXMLtag */
			$out[] = is_null($tag)? null : $tag->Attr($attr_name);
		return $this;
		}

	/** Fetch the tags in the matching set
	 * @param array $out	Storage. A tag takes one cell. NULLs preserved.
	 * @return ParseXMLtree
	 */
	function GetTags(&$out)
		{
		if (!is_array($out)) $out = array();
		foreach ($this->match as $group)
		foreach ($group as $tag) /* @var $tag tXMLtag */
			$out[] = $tag;
		return $this;
		}

	/** Fetch the tag groups in the matching set
	 * @param array $out	Storage. A tag-group takes one cell: array($tag,...). NULLs preserved.
	 * @return ParseXMLtree
	 */
	function GetGroupedTags(&$out)
		{
		if (!is_array($out)) $out = array();
		foreach ($this->match as $group)
		foreach ($group as $tag) /* @var $tag tXMLtag */
			$out[] = $tag;
		return $this;
		}

	/** When matched several document parts - usually they should be together.
	 * This functions combines an array, where you've stored different docparts under different keys.
	 * @param array $gets	Array with EQUAL items count, under meaningful keys
	 * @return array
	 * @throws EData('Combine') when one of the arrays depletes while some other still has values
	 *
	 * @example:
	 * 		...->GetText($get['nick'])->...->GetText($get['date'])->...->GetHTML($get['post']);
	 * 		$result=$XML->Combine($get);
	 * 		Here, $result = array of array('nick' => .., 'date' => .., 'post' => .. )
	 */
	function Combine(array $gets)
		{
		/* prepare counter */
		$getsN = count($gets);
		$total = 0; foreach ($gets as $v) $total += count($v);

		/* Work */
		$ret = array();
		while ($total>0)
			{
			$item = array();
			/* Take one element from each */
			foreach ($gets as $n => &$arr)
				if (count($arr) != 0)
					$item[$n] = array_shift($arr);
					else // Error
					{
					$lenghts = array();
					foreach ($gets as $n => $arr) $lenghts[] = sprintf('%s: %d', $n, count($arr));
					throw new EData(5, 'Combine',
						sprintf('Combine failed: array $%s depleted while others still have values: {%s}',
						$n, implode(', ', $lenghts)));
					}
			/* Next */
			$total -= $getsN;
			$ret[] = $item;
			}
		/* Finish */
		return $ret;
		}

	/** Fetches the contents in non-grouping mode, with modifiers applied */
	private function _GetContents(&$out,$html=TRUE,$preg=NULL,$match_id=NULL,$TXTp=NULL)
		{
		if (!is_array($out)) $out = array();
		foreach ($this->match as $group)
		foreach ($this->_get_group_contents($group,$html) as $data)
			$out[] = $this->_data_proc($data,$preg,$match_id,$TXTp);
		return $this;
		}

	/** Fetches contents of the specified matching group $group, $html?with:without html tags */
	private function _get_group_contents($group,$html)
		{
		$out = array();
		foreach ($group as $tag) /* @var $tag tXMLtag */
			{
			if (is_null($tag)) $out[] = null;
				elseif ($tag->closing === FALSE)
				$out[] = parent::toString($tag->id, $tag->pair->id, $html);
				elseif ($html) /* Closing, Single */
				$out[] = (string)$tag;
			}
		return $out;
		}

	private function _data_proc($data, $preg=null,$match_id=null,$TXTp=null)
		{
		if (is_null($data))
			return NULL;
		if (!is_null($TXTp))
			return (string)TextProc::SINGLE()->Take($data)->Apply($TXTp);
		if (!is_null($preg))
			{
			if (!preg_match($preg,$data,$m)) return NULL;
			if (is_null($match_id)) return $m;
			return isset($m[$match_id])?$m[$match_id]:NULL;
			}
		return $data;
		}

	/** Prints the whole tags tree with system information included in hints
	 * @param int $dive	The number of levels to deepen into the tree. 0 to display only root tags
	 */
	function DumpTree($dive = 999)
		{$this->_DumpTagsArray($dive?'Root tree':'Root tags', $this->root, $dive);}

	/** Prints the current matching set tree with system information included in hints
	 * @param int $dive	The number of levels to deepen into the tree. 0 to display only matching tags
	 */
	function DumpMatch($dive = 0)
		{
		$tags = array();
		foreach ($this->match as $group) foreach ($group as $tag) $tags[] = $tag;
		$this->_DumpTagsArray($dive?'Matches tree':'Matching set', $tags, $dive);
		}

	/** Common displayer for DumpTree() & DumpMatch() */
	private function _DumpTagsArray($label, array $tags, $dive, &$dupecheck = array())
		{
		if (!is_null($label)) print "<pre> <b>$label dump:</b>";
		foreach ($tags as $tag)
			if (is_null($tag)) print "\n<u><b>--NULL</b></u>";
			else//if (!in_array($tag->id, $dupecheck)) - this spoils, i think
			{
			//$dupecheck[] = $tag->id;
			$tag->Modified();
			/* Whitespace and grouping */
			print "\n";
			print is_null($label)? '   ' : sprintf("<b>% 3d</b>", $tag->gid);
			print str_repeat("\t", $tag->level);
			/* Tag */
			printf("<span title=\"%s\">%s</span>",
				sprintf('id:%d, level:%d, pair: %s, tid: %d, gid: %d, gtid: %d, closing: `%s` (%s)',
					$tag->id, $tag->level, is_null($tag->pair)?'-':$tag->pair->id, $tag->tid, $tag->gid, $tag->gtid,
					$tag->closing?'closing':( $tag->closing===FALSE?'opening':'single' ),
					strtoupper(var_export($tag->closing,1))
					),
				(is_null($label)?'':'<b>').htmlspecialchars('<'.$tag.'>').(is_null($label)?'':'</b>')
				);
			if ($tag->closing===FALSE) // is an opening tag, and there're some dive levels
				if ($dive == 0) print " <b>&lt;...&gt;</b>";
				else
				{
				$inner = array();
				for ($j = $tag->id+1; $j<=$tag->pair->id; $j++)
					if (($tag->level+1) == $this->tags[$j]->level)
						$inner[] = $this->tags[$j];
				$this->_DumpTagsArray(null, $inner, $dive-1, $dupecheck);
				}
			}
		if (!is_null($label)) print '</pre>';
		}
	}
?>