<?php
/** XML Tag Entity, and its service environment
 *
 * @Name		class Parser2
 * @Date		25.11.2008
 * @Version		2.0
 * @Depends
 * @Provides	XMLtag, XMLtagMatch
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/**
 * An XML tag entity
 * Takes a string like "[/]img[ src="..." style='...' DISABLED ][/]", and provides object-oriented access to the tag
 * Inner constants are good at parsing tags, and BRUTALLY optimized for best performance
 */
class XMLtag
	{
	/** Parses all the tags, but fails with HTML comments. [1] is the tag body without braces */
	const PREG_TAGS = '~<([^<>]+)>~Ss';
	/** Parses all the tags, names specified by the user. sprintf(., strtolower('a|br|div')) here. '/a' also match */
	const PREG_uTAGS = '~<(/?(?:%s)(?(?!>)\s[^<>]*))>~Ss';
	/** Slower, but more accurate version for matching tags: is tolerant to HTML comments and XML CDATA thing.
	 * "<!-- -->" && "<![CDATA[ ]]>" DO NOT match. PREG returns "" in place of them.
	 * Use ONLY when PREG_TAGS returns too much trash
	 */
	const PREG_xTAGS = '~<(?(?=!\[CDATA\[)(?U:.*)(?<=\]\])|(?(?=!--)(?U:.*)(?<=[^!]--)|([^<>]+)))>~Ss';

	/** Parses the tagname, and fetches its attributes: [1] = "/" | "", [2] = tagname, [3] = tagattrs[+"/"] */
	const PREG_TAGSPLIT = '|^(/?)([^\s/]+)\s*(.*)$|Ss';
	/** Parses the tag attributes into "FLAG", "name=value", "name='value'", 'name="value"' */
	const PREG_ATTRS = '/(?(?=\S+=["\'])[^"\']+(?(?=")"[^"]+"|\'[^\']+\')|\S+)/Ss';
			// TODO: try this: |(?i:attribute_name)=(\'|")([\s\S]*?)\1|

	/** Cached tagstring (without "<>"). =NULL on modification
	 * @var string
	 */
	protected $tagstr;

	/** TagName, lowercased.
	 * @var string
	 */
	public $name;

	/** Is a string initially, and on first Attr() call - is parsed
	 * @var array Array( name => value ). Names in lowercase
	 */
	protected $attrs = null;

	/** Tag type indicator: TRUE for "/div" - "closing", FALSE for "div" - "opening", NULL for "img /" - "single"
	 * @var bool
	 */
	public $closing;

	/** Constructs the tag entity
	 * NOTE: When a tagstr like "!-- ...comments... --" comes - the results are unprediclatble
	 * @param string $tagstr Tag string: "img class="noborder" src=one.gif"
	 * @throws EParse(0,'XMLtag') on XMLtag construction $tagstring parsing failure (bad system error)
	 */
	function __construct($tagstr)
		{
		$this->tagstr = trim($tagstr);
		$n = preg_match(self::PREG_TAGSPLIT, $this->tagstr, $m);
		if ($n == 0)
			throw new EParse(0, 'XMLtag', 'Failed to split tagstring to logical parts. Syntax?', compact('tagstr'));
		$this->name = $m[2];
		$this->attrs = $m[3];
		if (substr($this->attrs,-1,1) == '/')
			{
			$this->attrs = substr($this->attrs, 0, -1);
			$this->closing = null;
			}
			else
			$this->closing = ($m[1] == '/');
		}

	/** Parses the attributes on demand */
	protected function _parse_attrs()
		{
		if (is_array($this->attrs)) return 0;
		$n = preg_match_all(self::PREG_ATTRS, $this->attrs, $m);
		$this->attrs = array();
		foreach ($m[0] as $a)
			if ($a != '/') {
			$a = explode('=',$a,2); $a[0] = strtolower($a[0]);
			if (count($a) == 1)
				$this->attrs[ $a[0] ] = TRUE;
				else
				$this->attrs[ $a[0] ] = htmlspecialchars_decode(  ($a[1][0] == '"' || $a[1][0] == '\'') ? substr($a[1],1,-1) : $a[1]  );
			}
		return $n;
		}

	/** Get/Set an attribute
	 * @param string		$name	Attribute name
	 * @param string|bool	$value	New attribute value
	 * @return string|bool The current|new attribute value. TRUE is returned for flag attributes
	 * @return null For a missing value
	 * NOTE: Flag attributes are such as ("<select ...><option .. SELECTED>")
	 * 		They return TRUE on set, NULL on not set (FALSE when were unset manually)
	 */
	function Attr($name, $value = null)
		{
		$this->_parse_attrs();
		$name = strtolower($name);
		if (is_null($value))
			return isset($this->attrs[$name])? $this->attrs[$name] : NULL;
		$this->Modified();
		return $this->attrs[$name] = htmlspecialchars($value);
		}

	/** Get all attributes as an array
	 * @return array Array( name => value, ...). Names in lowercase
	 */
	function getAttrs()
		{
		$this->_parse_attrs();
		return $this->attrs;
		}

	/** When you change $name or $closing externally, and still want the tag to __toString() properly - invoke me
	 */
	function Modified() {$this->tagstr = null;}
	function isModified() {return is_null($this->tagstr);}

	/** Get the tagstring
	 * When attributes were modified - is created anew.
	 */
	function __toString()
		{
		if (!is_null($this->tagstr)) return $this->tagstr;
		/* Attributes */
		$attrs = '';
		if (is_string($this->attrs))
			$attrs = $this->attrs;
			else
			foreach($this->attrs as $n => $v)
				if ($v) {
					if ($v === TRUE) $attrs .= $n.' ';
					elseif (strpos($v,'"') === FALSE) $attrs .= sprintf('%s="%s" ', $n, $v);
					else $attrs .= sprintf('%s=\'%s\' ', $n, $v);
					}
		/* Finish */
		return ($this->closing===TRUE?'/':'').$this->name.($attrs?' '.rtrim($attrs):'').($this->closing===NULL?' /':'');
		}

	function __dump()
		{
		$this->_parse_attrs();
		return array('name' => $this->name, 'attrs' => $this->attrs, 'closing' => $this->closing);
		}
	}






/**
 * A solution for comparing a pattern with XMLtags in collection
 * @example: ("*", "a href style='float'") matches "a href='lol' style='display: block; float: left'
 */
class XMLtagMatch extends XMLtag
	{
	/** INCOMPLETE parser for the operator */
	const PREG_OPERATOR = "(!?)([=^$*]?)(/?)";

	const PREG_OPERATOR_CHARS = "[!]?[=^$*]?[/]?";

	/** The comparison operator for attributes
	 * @var array [0] => bool Negation, [1] => "=^$*", [2] => bool Skip `closing` check
	 */
	protected $op;

	/** Creates a new Matcher
	 * @param XMLtag $tagstr	The pattern tag to compare to. Is a "XMLtag" $tagstr actually, and parsed data is reused
	 * 							Use tagname "." to skip tagname check.
	 * 							Specify a list of tagnames using "tag1|tag2|tag3" syntax to match multiple tagnames
	 * 							`closing` check:
	 * 								Lead $tagstr with '/' to match closing tags ("/div"), skip it to match opening tags only ("div").
	 * 								Trail $tagstr with '/' to match single tags ("br /")
	 * 								Lead $tagstr with '*' to skip `closing` check ("div","/div","br /")
	 * 							FLAG attributes only check existance
	 * @param string $operator	Operator to compare the ATTRIBUTES
	 * 							One of '=','^','$','*' to match the given attributes with "equals", "starts with", "ends with","contains"
	 * 							FLAG attributes ALWAYS check existance (those that do not have a value, but are set)
	 * 							Trail the operator with '/' to skip the `closing` check
	 * 							Precede the operator with '!' to negate the comparison
	 * @throws EParse(0,'XMLtag') on XMLtagMatch $tagstr parsing failure
	 * @throws EData(0,'XMLtagMatcher') on wrong XMLtagMatch $operator
	 * NOTE: "/tag", "tag /" and "tag" are different ones! `closing` field must also match!
	 */
	function __construct($tagstr, $operator = '=/')
		{
		/* Tag */
		parent::__construct($tagstr);
		if ($this->name == '.')
			$this->name = null;
			else
			$this->name = explode('|', $this->name);
		$this->_parse_attrs();
		/* Operator */
		if (is_string($operator))
			{
			if (!preg_match('~^'.self::PREG_OPERATOR.'$~S', $operator, $this->op))
				throw new EData(0,'XMLtagMatcher','Wrong operator provided', compact('operator'));
			array_shift($this->op);
			} else $this->op = $operator;
		$this->op[0] = ($this->op[0] == '!');
		$this->op[2] = ($this->op[2] == '/');
		}

	/** Match myself agains the provided tag using rules
	 * @param XMLtag $tag	The tag to compare to
	 * @return bool TRUE when match, FALSE otherwise
	 */
	function Match(XMLtag $tag)
		{
		/* For POSITIVE check: ($this->op[0] === false)
		 * 	if ANY test fails - return FALSE. Else return TRUE
		 * For NEGATIVE check: ($this->op[0] === true)
		 * 	if ALL tests fail - return TRUE. Else return FALSE
		 * So, for every test: if ($test_ok == $this->op[0]) return FALSE; else check further
		 */
		/* `closing` check, TagName */
		if (!$this->op[2]  &&  $this->op[0] == ($this->closing === $tag->closing)  ) return FALSE;
		if (!is_null($this->name) &&  $this->op[0] == in_array($tag->name, $this->name)  ) return FALSE;
		/* Attributes */
		$attrs = $tag->getAttrs();
		foreach ($this->attrs as $n => $v)
			{
			$test = false;
			if (is_bool($v)) $test = isset($attrs[$n]);
				elseif (isset($attrs[$n]))
				switch ($this->op[1]) {
					case '^': $test = strpos($attrs[$n],$v) === 0; break;
					case '*': $test = strpos($attrs[$n],$v) !== FALSE; break;
					case '$': $test = strpos($attrs[$n],$v) === (strlen($attrs[$n])-strlen($v)); break;
					case '=': $test = $attrs[$n] === $v; break;
					}
			if ($test == $this->op[0]) return FALSE;
			}
		return TRUE;
		}
	}
?>