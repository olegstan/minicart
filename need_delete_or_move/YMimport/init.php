<?php

$path = dirname(__FILE__);
require("$path/lib/_Autoload.php");
Autoload::init("$path/lib");

function oo_mysql_object() {
	$cfg = new Config;
	$MySQLd = MySQLd::host($cfg->db_server)->login($cfg->db_user, $cfg->db_password)->database($cfg->db_name)
		->query('SET NAMES utf8;');
	return new MySQL($MySQLd);
	}

function oo_myisql_object() {
	$cfg = new Config;
	$myisqld = MyiSQLd::host($cfg->dbhost)->login($cfg->dbuser, $cfg->dbpass)->database($cfg->dbname)
		->set_names('utf8');
	return new MyiSQL($myisqld);
	}
?>