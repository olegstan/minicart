<?php

function process_category($category, $level, &$new_array)
{
	$new_array[] = array('id'=>$category->id, 'text'=>str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level).$category->name, 'class'=>'level'.$level, 'level'=>str_repeat("&nbsp;&nbsp;", $level));
	if (isset($category->subcategories))
		foreach($category->subcategories as $subcategory)
			process_category($subcategory, $level+1, $new_array);
}

function process_item($item, $menu_id, $level, &$new_array, $exclude)
{
	if ($item->menu_id != $menu_id || in_array($item->id, $exclude))
		return;
	$new_array[] = array('id'=>$item->id, 'text'=>str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level).$item->name, 'class'=>'level'.$level);
	if (isset($item->subitems))
		foreach($item->subitems as $subitem)
			if ($subitem->menu_id == $menu_id && !in_array($subitem->id, $exclude))
				process_item($subitem, $menu_id, $level+1, $new_array, $exclude);
}

	session_start();

	chdir("..");
	require_once('api/core/Minicart.php');
	$minicart = new Minicart();

	// Проверка сессии для защиты от xss
	if(!$minicart->request->check_session())
		exit('Session expired');
	
	$settings = $minicart->settings;
	
	$result = array();
	
	if ($minicart->request->method() == "POST")
	{
		$object = $minicart->request->post('object', 'string');
		$mode = $minicart->request->post('mode', 'string');
	}
	if ($minicart->request->method() == "GET")
	{
		$object = $minicart->request->get('object', 'string');
		$mode = $minicart->request->get('mode', 'string');
	}
	
	if (!isset($object) || !isset($mode) || 
		empty($object) || empty($mode) || 
		!in_array($object, array('categories', 'materials', 'orders', 'products', 'filters', 'users')))
			$result = -2;
	else
	{
		switch ($object){
			case 'categories':
				switch ($mode){
					case 'select2':
						$categories = $minicart->categories->get_categories_tree();
						$result = array('success'=>true, 'data'=>array());
						foreach($categories as $c)
							process_category($c, 0, $result['data']);
						break;
					case 'get_tags':
						$tags = $minicart->tags->get_tags(array('enabled'=>1));
						$result = array('success'=>true, 'data'=>array());
						foreach($tags as $t)
							$result['data'][] = array('id'=>$t->name, 'text'=>$t->name);
						break;
					default:
						$result = -3;
				}
				break;
			case 'materials':
				switch ($mode){
					case 'categories_list':
						$tree = $minicart->materials->get_categories_tree();
						$result = array('success'=>true, 'data'=>array());
						foreach($tree as $titem)
							process_category($titem, 0, $result['data']);
						break;
					case 'select2':
						$filter = array('limit' => $minicart->settings->products_num_admin, 'sort' => 'position', 'sort_type' => 'asc');
						if ($minicart->request->post('keyword'))
							$filter['keyword'] = $minicart->request->post('keyword');
						if ($minicart->request->get('keyword'))
							$filter['keyword'] = $minicart->request->get('keyword');
						$materials = $minicart->materials->get_materials($filter);
						$result = array('success'=>true, 'data'=>array());
						foreach($materials as $m)
							$result['data'][] = array('id'=>$m->id, 'text'=>$m->name);
						break;
					case "get_menu_items":
						$menu_id = $minicart->request->post('menu_id', 'integer');
						$exclude = explode(',', $minicart->request->post('exclude', 'string'));
						$items_tree = $minicart->materials->get_menu_items_tree();
						$new_items = array(array('id'=>0, 'text'=>'Корень', 'class'=>'level0'));
						
						foreach($items_tree as $item)
							process_item($item, $menu_id, 0, $new_items, $exclude);
						
						$result = array('success' => true, 'data' => $new_items);
						break;
					default:
						$result = -1;
				}
				break;
			case 'orders':
				switch ($mode){
					case "get_purchases":
						$id = $minicart->request->post('id', 'integer');
						$purchases = $minicart->orders->get_purchases(array('order_id'=>$id));
						$result = array('success' => true, 'data' => array());
						foreach($purchases as $purchase)
						{
							$row = array();
							$row['product_name'] = $purchase->product_name;
							$row['variant_name'] = $purchase->variant_name;
							$row['amount'] = $purchase->amount;
							$row['sku'] = $purchase->sku;
							$row['price'] = $minicart->currencies->convert($purchase->price, null, false);
							$row['format_price'] = $minicart->currencies->convert($purchase->price, null, true);
							$product = $minicart->products->get_product($purchase->product_id);
							
							if ($product)
							{
								$product->images = $minicart->image->get_images('products', $product->id);
								$product->image = @reset($product->images);
								$row['image'] = empty($product->image)?"":$minicart->design->resize_modifier($product->image->filename, 'products', 40, 40);
								$row['product_id'] = $purchase->product_id;
							}
							else
							{
								$row['product_id'] = 0;
								$row['image'] = "";
							}
							
							$result['data'][] = $row;
						}
						break;
					case "get_payment_methods":
						$delivery_id = $minicart->request->post('delivery_id', 'integer');
						$result = array('success' => true, 'data' => array());
						$payment_methods = $minicart->payment->get_payment_methods(array('enabled'=>1, 'delivery_id'=>$delivery_id));
						foreach($payment_methods as $method)
							$result['data'][] =  array('id'=>$method->id, 'name'=>$method->name, 'description'=>$method->description);
						break;
					case "change_status":
						$id = $minicart->request->post('id', 'integer');
						$status_id = $minicart->request->post('status_id', 'integer');
						$minicart->orders->update_order($id, array('status_id'=>$status_id));
						$result = array('success' => true);
						break;
					default:
						$result = -1;
				}
				break;
			case 'products':
				switch ($mode){
					case "get_tag_groups":
						/*$all_groups = $minicart->tags->get_taggroups(array('enabled'=>1, 'is_auto'=>0));
						$ids = $minicart->request->post('ids');
						if (!is_array($ids))
							$ids = array();
						$result = array('success' => true, 'data' => '<option value="0" data-name="" data-mode="">Не выбрано</option>');
						foreach($all_groups as $gr)
							if (!in_array($gr->id, $ids))
								$result['data'] .= "<option value='".$gr->id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->id.")</option>";*/
						
						$ids = $minicart->request->post('ids');
						if (!is_array($ids))
							$ids = array();
							
						$added_ids = array();
						$result = array('success' => true, 'data' => '<option value="0" data-name="" data-mode="">Не выбрано</option>');
							
						$tags_sets = $minicart->tags->get_tags_sets(array('limit'=>10000));
						foreach($tags_sets as $ts)
						{
							$is_empty_group = true;
							$str = "<optgroup label='".$ts->name."'>";
							$tags_groups = $minicart->tags->get_tags_set_tags(array('set_id'=>$ts->id, 'is_auto'=>0));
							foreach($tags_groups as $gr)
								if (!in_array($gr->group_id, $ids))
								{
									$str .= "<option value='".$gr->group_id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->group_id.")</option>";
									$is_empty_group = false;
									if (!in_array($gr->group_id, $added_ids))
										$added_ids[] = $gr->group_id;
								}
							$str .= "</optgroup>";
							
							if (!$is_empty_group)
								$result['data'] .= $str;
						}
						
						$is_empty_group = true;
						$str = "<optgroup label='Остальные свойства'>";
						$all_groups = $minicart->tags->get_taggroups(array('enabled'=>1, 'is_auto'=>0));
						foreach($all_groups as $gr)
							if (!in_array($gr->id, $ids) && !in_array($gr->id, $added_ids))
							{
								$str .= "<option value='".$gr->id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->id.")</option>";
								$is_empty_group = false;
							}
						$str .= "</optgroup>";
						if (!$is_empty_group)
								$result['data'] .= $str;
						break;
					case "get_tags":
						$group_id = $minicart->request->post('group_id', 'integer');
						if (empty($group_id))
							$group_id = $minicart->request->get('group_id', 'integer');
						$keyword = $minicart->request->post('keyword', 'string');
						if (empty($keyword))
							$keyword = $minicart->request->get('keyword', 'string');
						$tags = $minicart->tags->get_tags(array('group_id'=>$group_id, 'enabled'=>1, 'keyword'=>$keyword, 'is_auto'=>0));
						$result = array('success' => true, 'data' => '');
						if (!empty($tags))
							foreach($tags as $t)
								$result['data'][] = array('id'=>$t->name, 'text'=>$t->name);
						else
							$result['data'][] = array('id'=>$keyword, 'text'=>$keyword);
						break;
					/*case "get_tags_new":
						$group_id = $minicart->request->post('group_id', 'integer');
						$keyword = $minicart->request->post('keyword', 'string');
						$tags = $minicart->tags->get_tags(array('group_id'=>$group_id, 'enabled'=>1, 'keyword'=>$keyword));
						$result = array('success' => true, 'data' => array());
						foreach($tags as $t)
							$result['data'][] = $t->name;
						break;*/
					case "get_badges":
						$keyword = $minicart->request->post('keyword', 'string');
						if (empty($keyword))
							$keyword = $minicart->request->get('keyword', 'string');
						$badges = $minicart->badges->get_badges(array('visible'=>1, 'keyword'=>$keyword));
						$result = array('success' => true, 'data' => array());
						foreach($badges as $b)
							$result['data'][] = array('id'=>$b->name, 'text'=>$b->name);
						break;
					case "get_variants":
						$id = $minicart->request->post('id', 'integer');
						if (empty($id))
							$id = $minicart->request->get('id', 'integer');
						$variants = $minicart->variants->get_variants(array('product_id'=>$id, 'visible'=>1));
						$result = array('success' => true, 'data' => array());
						
						$minicart->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_admin = 1");
						$admin_currency = $minicart->db->result();
						
						$show_modificators = false;
						$product_categories = $minicart->categories->get_product_categories($id);
						$product_category = @$product_categories[0];
						if (!empty($product_category))
							$product_category = $minicart->categories->get_category(intval($product_category->category_id));
						if (!empty($product_category))
							$show_modificators = !empty($product_category->modificators) || !empty($product_category->modificators_groups);
						
						foreach($variants as $v){
							$p = $minicart->products->get_product($v->product_id);
							$result['data'][] = array('id'=>$v->id, 'name'=>$v->name, 'sku'=>$v->sku, 'price'=>$minicart->currencies->convert($v->price,!empty($p->currency_id) ? $p->currency_id : $admin_currency->id,false), 'stock'=>$v->stock, 'format_price'=>$minicart->currencies->convert($v->price,!empty($p->currency_id) ? $p->currency_id : $admin_currency->id,true), 'show_modificators' => $show_modificators);
						}
						break;
					case 'categories_list':
						$tree = $minicart->categories->get_categories_tree();
						$result = array('success'=>true, 'data'=>array());
						foreach($tree as $titem)
							process_category($titem, 0, $result['data']);
						break;
					case "select2":
						$filter = array();
						$keyword = $minicart->request->post('keyword', 'string');
						if (empty($keyword))
							$keyword = $minicart->request->get('keyword', 'string');
						if (!empty($keyword))
							$filter['keyword'] = $keyword;
						$filter['limit'] = $minicart->settings->products_num_admin;
						$products = $minicart->products->get_products($filter);	
						$result = array('success'=>true, 'data'=>array());
						foreach($products as $p)
							$result['data'][] = array('id'=>$p->id, 'text'=>$p->name);
						break;
					case "product-addrelated":
						$filter = array(/*'visible'=>1*/);
						if ($minicart->request->post('keyword'))
							$filter['keyword'] = $minicart->request->post('keyword');
						if ($minicart->request->get('keyword'))
							$filter['keyword'] = $minicart->request->get('keyword');
						if ($minicart->request->post('exception'))
							$filter['exception'] = explode(",", $minicart->request->post('exception'));
						if ($minicart->request->get('exception'))
							$filter['exception'] = explode(",", $minicart->request->get('exception'));
						$products = $minicart->products->get_products($filter);
						foreach($products as $index=>$product)
						{
							$products[$index]->images = $minicart->image->get_images('products', $product->id);
							$products[$index]->image = reset($products[$index]->images);
							$products[$index]->variants = $minicart->variants->get_variants(array('product_id'=>$product->id, 'visible'=>1));
							$products[$index]->variant = reset($products[$index]->variants);
							
							$products[$index]->in_stock = false;
							$products[$index]->in_order = false;
							foreach($products[$index]->variants as $rv)
								if ($rv->stock > 0)
									$products[$index]->in_stock = true;
								else
									if ($rv->stock < 0)
										$products[$index]->in_order = true;
							
							$product_categories = $minicart->categories->get_product_categories($product->id);
							if ($product_categories)
							{
								$product_category = reset($product_categories);
								$category = $minicart->categories->get_category($product_category->category_id);
								$products[$index]->modificators = !empty($category->modificators) ? join(',', $category->modificators) : '';
								$products[$index]->modificators_groups = !empty($category->modificators_groups) ? join(',', $category->modificators_groups) : '';
							}
						}
						$minicart->design->assign('products', $products);
						$result = array('success' => true, 'data' => $minicart->design->fetch($minicart->design->getTemplateDir('admin').'product-addrelated.tpl'));
						break;
					case "product-togglefavorite":
						$result = array('success' => false, 'data' => '', 'count' => 0);
						$product_id = $minicart->request->post('pid', 'integer');
						$user_id = $minicart->request->post('uid', 'integer');
						if (empty($user_id) || empty($product_id))
							break;
						$exists = $minicart->products->check_favorite_product($product_id, $user_id);
						if ($exists)
							$minicart->products->delete_favorite_product($product_id, $user_id);
						else
							$minicart->products->add_favorite_product($product_id, $user_id);
						$favorites_count = $minicart->products->count_favorites_products($user_id);
						$result = array('success' => true, 'data' => !$exists, 'count' => $favorites_count);
						break;
					default:
						$result = -1;
				}
				break;
			case 'filters':
				/*$result[0] = array();
				$list = $minicart->request->post('list');
				if (!is_array($list))
					$list = array();
				$tag_groups = $minicart->tags->get_taggroups();
				foreach($tag_groups as $group)
					if (!in_array($group->id, $list))
						$result[0][] = array('id'=>$group->id, 'name'=>$group->name, 'mode'=>$group->mode, 'postfix'=>$group->postfix);*/
				$ids = $minicart->request->post('list');
				if (!is_array($ids))
					$ids = array();
					
				$added_ids = array();
				$result = array('success' => true, 'data' => '<option value="0" data-name="" data-mode="">Не выбрано</option>');
				
				$is_empty_group = true;
				$str = "<optgroup label='Автосвойства'>";
				$all_groups = $minicart->tags->get_taggroups(array('is_auto'=>1));
				foreach($all_groups as $gr)
					if (!in_array($gr->id, $ids) && !in_array($gr->id, $added_ids) && $gr->mode != "text")
					{
						$str .= "<option value='".$gr->id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->id.")</option>";
						$is_empty_group = false;
						if (!in_array($gr->id, $added_ids))
								$added_ids[] = $gr->id;
					}
				$str .= "</optgroup>";
				if (!$is_empty_group)
						$result['data'] .= $str;
					
				$tags_sets = $minicart->tags->get_tags_sets(array('limit'=>10000));
				foreach($tags_sets as $ts)
				{
					$is_empty_group = true;
					$str = "<optgroup label='".$ts->name."'>";
					$tags_groups = $minicart->tags->get_tags_set_tags(array('set_id'=>$ts->id));
					foreach($tags_groups as $gr)
						if (!in_array($gr->group_id, $ids) && !in_array($gr->group_id, $added_ids) && $gr->mode != "text")
						{
							$str .= "<option value='".$gr->group_id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->group_id.")</option>";
							$is_empty_group = false;
							if (!in_array($gr->group_id, $added_ids))
								$added_ids[] = $gr->group_id;
						}
					$str .= "</optgroup>";
					
					if (!$is_empty_group)
						$result['data'] .= $str;
				}
				
				$is_empty_group = true;
				$str = "<optgroup label='Остальные свойства'>";
				$all_groups = $minicart->tags->get_taggroups(array());
				foreach($all_groups as $gr)
					if (!in_array($gr->id, $ids) && !in_array($gr->id, $added_ids) && $gr->mode != "text")
					{
						$str .= "<option value='".$gr->id."' data-name='".$gr->name."' data-mode='".$gr->mode."' data-postfix='".$gr->postfix."'>".$gr->name.(empty($gr->postfix)?"":", ".$gr->postfix)." (".$gr->id.")</option>";
						$is_empty_group = false;
					}
				$str .= "</optgroup>";
				if (!$is_empty_group)
						$result['data'] .= $str;
				break;
			case 'users':
				switch ($mode){
					case 'reset_password':
						$id = $minicart->request->post('id', 'integer');
						$user = $minicart->users->get_user($id);
						if ($user)
						{
							$reset_url = md5($minicart->salt . $user->password . md5($user->password) . $user->id);
							$datetime = new DateTime();
							$datetime->modify('+1 day');
							$reset_date = $datetime->format('Y-m-d G:i');
							
							$minicart->users->update_user($user->id, array('reset_url'=> $reset_url, 'reset_date'=> $reset_date));
							$minicart->notify_email->email_reset_password($user->id);
							$result = array('success'=>true);
						}
						else
							$result = array('success'=>false);
						break;
				}
				break;
		}
	}

	header("Content-type: application/json; charset=UTF-8");
	header("Cache-Control: must-revalidate");
	header("Pragma: no-cache");
	header("Expires: -1");		
	$json = json_encode($result);
	print $json;
?>