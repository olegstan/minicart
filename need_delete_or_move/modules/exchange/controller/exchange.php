<?php

require_once('api/core/Minicart.php');

error_reporting(E_ERROR);

ini_set('include_path', 'modules/exchange/lib/Excel');
require_once('PEAR.php');
require_once('Spreadsheet/Excel/Writer.php');
require_once('excel_reader2.php');

class ExchangeControllerAdmin extends Minicart {

	private $param_url, $params_arr, $options;
	
	private $xls, $sheet, $excelLine, $cellFormat, $allTags, $brands_ids, $exported_products, $exported_variants;

	public function set_params($url = null, $options = null){
		$this->options = $options;
		
		$url = urldecode(trim($url, '/'));
		$delim_pos = mb_strpos($url, '?', 0, 'utf-8');
		
		if ($delim_pos === false)
		{
			$this->param_url = $url;
			$this->params_arr = array();
		}
		else
		{
			$this->param_url = trim(mb_substr($url, 0, $delim_pos, 'utf-8'), '/');
			$url = mb_substr($url, $delim_pos+1, mb_strlen($url, 'utf-8')-($delim_pos+1), 'utf-8');
			$this->params_arr = array();
			foreach(explode("&", $url) as $p)
			{
				$x = explode("=", $p);
				$this->params_arr[$x[0]] = "";
				if (count($x)>1)
					$this->params_arr[$x[0]] = $x[1];
			}
		}
	}
	
	function export_products($categories_ids, $brands_ids){
	
		$fname = "export_new_".date("His").".xls";
		$xls_filename = $this->config->root_dir . "modules/exchange/files/" . $fname;
		//$xls_filename = "/var/www/mini4/data/www/mini4.ru/modules/exchange/files/".$fname;
		$this->design->assign('xls_filename', $this->config->root_url . "/modules/exchange/files/".$fname);
		
		if (file_exists($xls_filename))
			unlink($xls_filename);
	
		$this->xls = new Spreadsheet_Excel_Writer(/*$xls_filename*/);
		$this->xls->send($fname);
		$this->xls->setVersion(8);
		$this->xls->setTempDir($this->config->root_dir . "modules/exchange/files/");
		//$this->xls->setTempDir('/var/www/mini4/data/www/mini4.ru/modules/exchange/files/');
		$this->sheet = & $this->xls->addWorksheet('export');
		$this->sheet->setInputEncoding('UTF-8');
		
		// Создание объекта форматирования
		$this->xls->setCustomColor(12, 192, 192, 192);
		$this->cellFormat = $this->xls->addFormat();
		$this->cellFormat->setFgColor(12);
		
		$this->excelLine = 0;
		
		$this->brands_ids = $brands_ids;
		
		$header = array("Уровень","Код товара","Код варианта","Артикул","Название товара (название варианта)","Краткое описание","Краткое описание 2","Полное описание","Цена","Видимость варианта","Склад","Старая цена","Бренд","Порядок","Категории");
	
		$this->allTags = $this->tags->get_taggroups(array('is_auto' => 0, 'limit' => 10000, 'sort' => 'name', 'sort_type' => 'asc'));
		foreach($this->allTags as $t)
			$header[] = $t->name;		
		//Записываем заголовок
		$this->writeArrayToExcel($header);
		
		$header = array("","","","","","","","","","","","","","","");
		foreach($this->allTags as $t)
			$header[] = $t->id;
		//Записываем id свойств
		$this->writeArrayToExcel($header);
		
		$this->exported_products = 0;
		$this->exported_variants = 0;
		
		if (empty($categories_ids)){
			$categories = $this->categories->get_categories_tree();
			foreach($categories as $category)
				$this->process_category($category, 0);
		}
		else
			foreach($categories_ids as $category_id)
				$this->process_category($this->categories->get_category($category_id), 0);
		
		$res = $this->xls->close();
		
		//$this->design->assign('exported_products', $this->exported_products);
		//$this->design->assign('exported_variants', $this->exported_variants);
	}
	
	function writeArrayToExcel($line, $is_category = false){
		foreach($line as $column=>$c)
			if ($is_category && $this->excelLine > 0)
				$this->sheet->write($this->excelLine, $column, strval($c), $this->cellFormat);
			else
				$this->sheet->write($this->excelLine, $column, strval($c));
		
		$this->excelLine++;
	}
	
	function process_category($category, $level){
	
		$line = array($level,"","","",$category->name . "(" . $category->id . ")","","","","","","","","","","");
		foreach($this->allTags as $t)
			$line[] = "";
		$this->writeArrayToExcel($line, true);
		$products_filter = array('category_id'=>$category->id, 'limit'=>100000, 'sort'=>'export_xls');
		if (!empty($this->brands_ids))
			$products_filter['brand_id'] = $this->brands_ids;
		$products = $this->products->get_products($products_filter);
		foreach($products as $product)
		{
			$cats = $this->categories->get_product_categories($product->id);
			$cat = reset($cats);
			if ($cat->category_id != $category->id)
				continue;
		
			$brand = $this->brands->get_brand(intval($product->brand_id));
			
			$product_categories = $this->categories->get_product_categories($product->id);
			$product_categories_str = "";
			foreach($product_categories as $pc)
			{
				if (!empty($product_categories_str))
					$product_categories_str .= ";";
				$product_categories_str .= $pc->category_id;
			}
			
			//$header = array("Уровень","Код товара","Код варианта","Артикул","Название товара (название варианта)","Краткое описание","Краткое описание 2","Полное описание","Цена","Видимость варианта","Склад","Старая цена","Бренд","Порядок","Категории");
			$variants = $this->variants->get_variants(array('product_id'=>$product->id));
			foreach($variants as $index=>$variant)
			{
				$is_variant = false;
				if ($index>0)
					$is_variant = true;
				$nalichie='';
				if ($variant->infinity || $variant->stock == '')
					$nalichie='∞';
				else
					$nalichie = $variant->stock;
				$line = array();
				$line[] = "";
				$line[] = $product->id;
				$line[] = $variant->id;
				$line[] = $variant->sku;
				$line[] = $is_variant ? $variant->name : $product->name;
				$line[] = $product->annotation;
				$line[] = $product->annotation2;
				$line[] = $product->body;
				$line[] = $variant->price;
				$line[] = $variant->visible;
				$line[] = $nalichie;
				$line[] = !empty($variant->price_old) ? $variant->price_old : "";
				$line[] = $brand ? $brand->name : "";
				$line[] = $index==0 ? $product->position : "";
				$line[] = $product_categories_str;
				
				$product_tags = $this->tags->get_product_tags($product->id);
				
				$product_tags_groups = array();
				foreach($product_tags as $pt)
				{
					if (!array_key_exists($pt->group_id, $product_tags_groups))
						$product_tags_groups[$pt->group_id] = array();
					$product_tags_groups[$pt->group_id][] = $pt;
				}
				
				if ($index==0)
				{
					foreach($this->allTags as $t)
					{
						if (array_key_exists($t->id, $product_tags_groups))
						{
							$tags = array();
							foreach($product_tags_groups[$t->id] as $tag)
								$tags[] = $tag->name;
							$line[] = implode("#", $tags);
						}
						else
							$line[] = "";
					}
				}
				else
					foreach($this->allTags as $t)
						$line[] = "";
				
				$this->writeArrayToExcel($line);
				
				$this->exported_variants++;
			}
			unset($variants);
			
			$this->exported_products++;
		}
		
		unset($products);
		
		if (!empty($category->subcategories))
			foreach($category->subcategories as $subcat)
				$this->process_category($subcat, $level+1);
	}
	
	function import_products(){
		$fname = $this->request->files("importFile");
		
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();
		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		// Read XLS-file
		$data->read($fname[0]["tmp_name"]);
		
		$features_ids = array();
		$proccessed_products_ids = array();
		
		$updated_products = 0;
		$updated_variants = 0;
		for ($line = 2; $line <= $data->sheets[0]['numRows']; $line++)
		{
			if (!isset($data->sheets[0]['cells'][$line]))
					continue;
			
			$arr = array();
			for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++)
			{
				if (array_key_exists($j, $data->sheets[0]['cells'][$line]))
					$arr[] = trim($data->sheets[0]['cells'][$line][$j]);
				else
					$arr[] = "";
			}
			
			if (count($arr) <= 1)
				continue;
			
			foreach($arr as &$a)
				$a = iconv('cp1251','utf-8', trim($a));
			
			if ($line == 2)
			{
				for($i=15;$i<count($arr);$i++)
				{
					$tag_group = $this->tags->get_taggroup($arr[$i]);
					if (!$tag_group)
					{
						$this->design->assign('message_error', 'Свойство "'.$arr[$i].'" не найдено в базе');
						return false;
					}
					$features_ids[] = intval($tag_group->id);
				}
			}
			else
			{
				//$header = array("Уровень","Код товара","Код варианта","Артикул","Название товара (название варианта)","Цена","Наличие","Старая цена","Бренд","Порядок","Категории");
				
				$update_price = true;
				$update_price_old = true;
				if (trim($arr[8]) == '')
					$update_price = false;
				if (trim($arr[11]) == '')
					$update_price_old = false;
				
				$level = strval($arr[0]);
				$product_id = intval($arr[1]);
				$variant_id = intval($arr[2]);
				$sku = $arr[3];
				$name = trim($arr[4]);
				$annotation = trim($arr[5]);
				$annotation2 = trim($arr[6]);
				$body = trim($arr[7]);
				$price = floatval($arr[8]);
				$visible = intval($arr[9]);
				if ($visible != 0 && $visible != 1)
					$visible = 0;
				$nalichie = strval($arr[10]);
				$price_old = floatval($arr[11]);
				$brand = trim($arr[12]);
				$position = intval($arr[13]);
				$categories = trim(strval($arr[14]));
				$product_categories = explode(";", $categories);
				
				if (empty($product_id))
					continue;

				$stock = null;
				if ($nalichie == -1)
					$stock = -1;
				if ($nalichie == 0)
					$stock = 0;
				
				//если категория то пропустим
				if (is_numeric($level))
					continue;
					
				if (!in_array($product_id, $proccessed_products_ids))
				{
					//get features values
					$features = array();
					for($i=0;$i<count($features_ids);$i++)
						if (empty($arr[$i+15]))
							$features[] = "";
						else
							$features[] = trim($arr[$i+15]);
							
					//определение бренда
					if (!empty($brand))
					{
						$b = $this->brands->get_brand($brand);
						if (!$b)
						{
							$b->name = $brand;
							$b->description = "";
							$b->image = "";
							$b->meta_title = $brand;
							$b->meta_keywords = $brand;
							$b->meta_description = $brand;
							$b->id = $this->brands->add_brand($b);
						}
					}
					
					//update products
					$query = $this->db->placehold("UPDATE __products SET name=?, brand_id=?, position=?, annotation=?, annotation2=?, body=? WHERE id=?", $name, isset($b)?$b->id:NULL, $position, $annotation, $annotation2, $body, $product_id);
					$this->db->query($query);
					
					//delete all product tags
					$product_tags = $this->tags->get_product_tags($product_id);
					foreach($product_tags as $pt)
						$this->tags->delete_product_tag($product_id, $pt->id);
					
					foreach($features as $index=>$feature)
					{
						if (empty($feature))
							continue;
							
						$feature_arr = explode("#", $feature);
						
						foreach($feature_arr as $f){
							$tags = $this->tags->get_tags(array('group_id' => $features_ids[$index], 'name' => $f, 'limit' => 1));
							if ($tags)
								$t_id = $tags[0]->id;
							else
							{
								$tag = new stdClass;
								$tag->name = $f;
								$tag->group_id = $features_ids[$index];
								$tag->enabled = 1;
								$tag->is_auto = 0;
								
								$t_id = $this->tags->add_tag($tag);
							}
							
							$this->tags->add_product_tag($product_id, $t_id);
						}
					}
					
					//update s_variants
					$this->variants->update_variant($variant_id, array('name' => $name, 'sku' => $sku, 'stock' => $stock, 'visible' => $visible));
					
					if ($update_price)
						$this->variants->update_variant($variant_id, array('price' => $price));
					
					if ($update_price_old)
						$this->variants->update_variant($variant_id, array('price_old' => $price_old));
					
					// update categories
					$query = $this->db->placehold('DELETE FROM __products_categories WHERE product_id=?', $product_id);
					$this->db->query($query);
					if(is_array($product_categories))
						foreach($product_categories as $i=>$category_id)
						{
							$this->categories->add_product_category($product_id, $category_id, $i);
						}
						
					$proccessed_products_ids[] = $product_id;
					
					$updated_products++;
					$updated_variants++;
				}
				else
				{
					$this->variants->update_variant($variant_id, array('name' => $name, 'sku' => $sku, 'price' => $price, 'price_old' => $price_old, 'stock' => $stock));
					$updated_variants++;
				}
			}
		}
		$this->design->assign('message_success', "Обновлено $updated_products товаров, $updated_variants вариантов");
	}
		
	function fetch() {
		if (!(isset($_SESSION['admin']) && $_SESSION['admin']=='admin'))
			header("Location: http://".$_SERVER['SERVER_NAME']."/admin/login/");
				
		if ($this->request->method('post')){
			$export_categories = $this->request->post('export_button_categories');
			$export_brands = $this->request->post('export_button_brands');
			$import = $this->request->post('import_button');
			
			if ($export_categories){
				$categories_ids = (array) $this->request->post('category_id');
				
				$this->export_products($categories_ids, array());
			}
			
			if ($export_brands){
				$brands_ids = (array) $this->request->post('brand_id');
				
				$this->export_products(array(), $brands_ids);
			}
			
			if ($import){
				$this->import_products();
			}
		}
		
		$this->design->assign('categories', $this->categories->get_categories_tree());
		$this->design->assign('brands', $this->brands->get_brands());

		return $this->design->fetch('modules/exchange/template/exchange.tpl');
	}
}
