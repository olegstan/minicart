{* Title *}
{$meta_title='Загрузка и выгрузка файлов Excel' scope=parent}

{if $xls_filename}

<div class="alert alert-success" role="alert">
	Файл успешно сгенерирован! <a href="{$xls_filename}">Скачать</a><br>
	Экспортировано {$exported_products} товаров, {$exported_variants} вариантов.
</div>

{/if}

{if $message_success}

<div class="alert alert-success" role="alert">
	{$message_success}
</div>

{/if}

{if $message_error}

<div class="alert alert-error" role="alert">
	{$message_error}
</div>

{/if}

<form method=post enctype="multipart/form-data">

<div role="tabpanel">

	  <!-- Nav tabs -->
	  <ul class="nav nav-tabs" role="tablist">
		<li role="presentation" class="active"><a href="#export-categories" role="tab" data-toggle="tab">Экспорт категорий</a></li>
		<li role="presentation"><a href="#export-brands" role="tab" data-toggle="tab">Экспорт брендов</a></li>
		<li role="presentation"><a href="#import" role="tab" data-toggle="tab">Импорт</a></li>
	  </ul>

	  <!-- Tab panes -->
	  <div class="tab-content">
		<div role="tabpanel" class="tab-pane active" id="export-categories">
			{function name=categories_tree level=0}
				{foreach $items as $item}
					<div class="checkbox">
						<label>
						  <input type="checkbox" value="{$item->id}" name="category_id[]"> {section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}
						</label>
					</div>
					{categories_tree items=$item->subcategories level=$level+1}
				{/foreach}
			{/function}
			{categories_tree items=$categories}
			
			<div class="form-group">
				<input type="submit" value="Экспортировать" class="btn btn-primary btn-lg" name="export_button_categories"/>
			</div>
			
			<div class="alert alert-info">
            	<h4>Примечание</h4>
				<ol>
                   	<li>Товар будет попадать в категорию только если она указана у него как первая родительская.</li>
				</ol>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="export-brands">
			{foreach $brands as $brand}
				<div class="checkbox">
					<label>
					  <input type="checkbox" value="{$brand->id}" name="brand_id[]"> {$brand->name}
					</label>
				</div>
			{/foreach}
			
			<div class="form-group">
				<input type="submit" value="Экспортировать" class="btn btn-primary btn-lg" name="export_button_brands"/>
			</div>
			
			<div class="alert alert-info">
            	<h4>Примечание</h4>
				<ol>
                   	<li>Товар будет попадать в категорию только если она указана у него как первая родительская.</li>
				</ol>
			</div>
		</div>
		<div role="tabpanel" class="tab-pane" id="import">
			<div class="form-group">
				<label for="exampleInputFile">Файл</label>
				<input type="file" id="exampleInputFile" name="importFile">
			</div>
			<input type="submit" value="Импортировать" class="btn btn-primary btn-lg" name="import_button"/>
		</div>
	  </div>

	</div>
     
</form>

<script type="text/javascript">

	$(document).ready(function () {
		$('select[name=category_id]').select2({
            placeholder: "Все категории"
        });
		$('select[name=brand_id]').select2({
            placeholder: "Все бренды"
        });
	});

	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>