{* Title *}
{$meta_title='Импорт товаров из Яндекс.Маркета' scope=parent}

{* Заголовок *}

<form method=POST>

    <div class="row yandexmarket">
		<div class="col-md-8">
		<legend>Импорт товаров из <font color=red>Я</font>ндекс.Маркета</legend>
		
		{if $mode == "default"}
			<input type=hidden name="session_id" value="{$smarty.session.id}"/>
			<input type=hidden name="mode" value="start_parse"/>
			
			<div class="form-group">
				<label>Целевая категория:</label>
						
				<select name="category_id" class="select2 form-control">							
					{function name=category_select level=0}
						{foreach from=$categories item=category}
							<option value='{$category->id}' {if $category->id == $selected_id}selected{/if} category_name='{$category->name|escape}'>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$category->name|escape}</option>
							{category_select categories=$category->subcategories selected_id=$selected_id  level=$level+1}
						{/foreach}
					{/function}
					{category_select categories=$Categories selected_id=$product_category->id}
				</select>
			</div>
                    
            <div class="form-group">
				<label>Ссылки на товары:</label>
				<textarea name="urls" rows="5" class="form-control" required></textarea>
            </div>
            <input type="submit" value="Получить список товаров" class="btn btn-primary btn-lg {if $tmp_folder_perms != "777"}disabled{/if}">
            <hr/>
			<div class="alert alert-info">
            	<h4>Инструкция по парсингу</h4>
				<ol>
                   	<li>Вы можете указать ссылки на страницы со списком товаров (/guru.xml), вот пример правильной ссылки:<br/>
                    <strong><u>http://market.yandex.ru/guru.xml</u></strong>?CMD=-RR=0,0,0,0-VIS=70-CAT_ID=6427101-EXC=1-PG=10&hid=6427100<br/><br/>
                    Именно такие ссылки могут быть импортированы с Яндекс-маркета.
                    Вам будет предложен выбор какие именно товары со страницы импортировать, а краткое описание будет взято строкой.</li>
					<li>Вы можете указать ссылки на конкретные товары (/model.xml), напрмер:
                    <strong><u>http://market.yandex.ru/model.xml</u></strong>?modelid=8485579&hid=6427100<br/><br/>краткое описание будет представлено в виде таблицы.</li>
				</ol>
			</div>
		{*#####################################################################################################################################################################*}
		{elseif $mode=='invalid_urls'}
			<h2>Ошибка!</h2>
			<p>Некоторые ссылки не были распознаны как указывающие на товары Яндекс.Маркета:</p>
			<ul>
				{foreach from=$invalid_urls item=url}
					<li>{$url}</li>
				{/foreach}
			</ul>

			<p>Вернитесь назад, и исправьте эти ссылки!</p>

			<a href="{$config->root_url}{$module->url}" class="btn btn-primary">Вернуться</a>
		{*#####################################################################################################################################################################*}
		{elseif $mode=='captcha_request'}
			<h2>Возникли ошибки</h2>
			<input type=hidden name="session_id" value="{$smarty.session.id}">
			<input type=hidden name="mode" value="captcha_input"/>
			<input type="hidden" name="captcha_hidden_key" value="{$captcha_hidden_key}" />
			<input type="hidden" name="captcha_retpath" value="{$captcha_retpath}" />

			Яндекс просит ввести число с картинки (CAPTCHA).
			<br><img src="data:image/jpeg;base64,{$captcha_image}" />
			<br><input type="text" name="captcha_text" value="">
			<br><input type="submit" value="Ok">
		{*#####################################################################################################################################################################*}
		{elseif $mode=='choose'}
			<p><b>Выберите товары</b>, которые нужно импортировать:</p>

			<input type=hidden name="session_id" value="{$smarty.session.id}">
			<input type=hidden name="mode" value="choose"/>
			<div class="twocolumns">
				{foreach from=$products item=product}
					<div class="checkbox">
						<label><input type="checkbox" name="choose[]" CHECKED value="{$product->id}">{$product->model}</label>
					</div>
				{/foreach}
			</div>
			<div class="btn-group">
				<a href="#" onclick="selectall();return false;" class="btn btn-default btn-sm">Выделить все</a>
				<a href="#" onclick="deselectall();return false;" class="btn btn-default btn-sm">Отменить все</a>
			</div>
			<hr/>
			<input type="submit" value="Импортировать товары" class="btn btn-primary btn-lg">
			
			{literal}
			<script>
				function selectall() {
					$("input:checkbox").prop("checked",true);
				};
				function deselectall() {
					$("input:checkbox").prop("checked",false);
				}
			</script>
			{/literal}
		{*#####################################################################################################################################################################*}
		{elseif $mode=='finish'}
			<p>Успешно импортировано <b>{$successful}</b> {$successful|plural:'товар':'товаров':'товара'} из <b>{$expected}</b>!</p>
			<a href="{$config->root_url}{$module->url}" class="btn btn-primary">Вернуться</a>
		{/if}
		
	</div>
<div class="col-md-4">
<legend>Настройки</legend>
    <div class="form-group">
        <label>Забирать цену с маркета</label>
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default4 on {if $settings->modules_ymimport_parse_price}active{/if}">
            <input type="radio" name="modules_ymimport_parse_price" value="1" {if $settings->modules_ymimport_parse_price}checked{/if}/>Забирать</label>
            <label class="btn btn-default4 off {if !$settings->modules_ymimport_parse_price}active{/if}">
            <input type="radio" name="modules_ymimport_parse_price" value="0" {if !$settings->modules_ymimport_parse_price}checked{/if}/>Не забирать</label>
        </div>
        <p class="help-block">Если цена не будет забираться с маркета, она будет устанавливаться по умолчанию равной "0"</p>
    </div>
    
    <div class="form-group">
        <label>Активен ли товар после парсинга</label>
        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default4 on {if $settings->modules_ymimport_product_visible}active{/if}">
            <input type="radio" name="modules_ymimport_product_visible" value="1" {if $settings->modules_ymimport_product_visible}checked{/if}/>Активен</label>
            <label class="btn btn-default4 off {if !$settings->modules_ymimport_product_visible}active{/if}">
            <input type="radio" name="modules_ymimport_product_visible" value="0" {if !$settings->modules_ymimport_product_visible}checked{/if}/>Скрыт</label>
        </div>
    </div>
    
    <div class="form-group">
        <label>Количество товара после парсинга</label>
        <input type="text" name="modules_ymimport_variant_stock" class="form-control w50" value="{if $settings->modules_ymimport_variant_stock}{$settings->modules_ymimport_variant_stock}{else}∞{/if}">
		<p class="help-block">По умолчанию "∞"</p>
    </div>
	
	{if isset($tmp_folder_perms) && $tmp_folder_perms != "777"}
	<div class="form-group">
		<div class="alert alert-warning">Не хватает прав, установите права <b>777</b> на папку <b>/modules/ymimport/controller/tmp</b></div>
    </div>
	{/if}
</div>

</div>

</form>