<?php
/** Handles objects auto-loading using provided (className => fileName) maps, or '.autoloadlist.php' files in classes' root directory
 * NOTE: Auto-Loading is not available if using PHP in CLI interactive mode due to PHP limitations.
 *
 * @Name		class Autoload
 * @Date		30.11.2009
 * @Version		2.1
 * @Provides	Autoload
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Autoload {
	/** Classes => filenames mapping
	 * Format: array( 'classname' => array(ROOT_ID, "filename") )
	 * @var array
	 */
	static $MAP = array();

	/** Array of roots
	 * Format: array( ROOT_ID => "path" )
	 * @var array
	 */
	static $ROOTS = array();

	/** Init autoloader with a directory that contains classes files hierarchy.
	 * The function can be called multiple times.
	 * If no $map provided - the target directory must include '.autoloadlist.php' file <?php return $MAP; ?>
	 * @param string	$dir	Root directory for classes hierarchy.
	 * @param string[]	$map	Classes map: array of ('classname(lowercase)' => 'filename.php' ).
	 */
	static function init($dir, $map = null) {
		/* Load mappings */
		if (is_null($map)) {
			$autoloadlist = "$dir/.autoloadlist.php";
			$map = require $autoloadlist;
			}
		/* save classes paths */
		$root_id = count(self::$ROOTS);
		self::$ROOTS[] = $dir;

		foreach ($map as $class => $file)
		{
			self::$MAP[$class] = array($root_id, $file);
			require_once $file;
		}
	}
		
	/** Add some class' modules to the autoloader: use in class' php file.
	 * @param string		$owner	Owner class name: LOWERCASE!
	 * @param string|null	$dir	Directory, relative to the parent class' folder.
	 * @param string[]		$map	Classes map: array of ('classname(lowercase)' => 'filename.php' ).
	 */
	static function init_modules($owner, $dir, $map = null) {
		if (!isset(self::$MAP[$owner]))
			trigger_error("Autoload::init_modules('$owner', '$dir') failed to resolve parent class name!", E_USER_ERROR);
		$path = self::$ROOTS[self::$MAP[$owner][0]];
		$relpath = dirname(self::$MAP[$owner][1]);
		if ($relpath != '.')
			$path .= "/$relpath";
		if (!is_null($dir))
			$path .= "/$dir";
		self::init($path, $map);
		}

	/** Registered namespace prefixes that were not loaded yet
	 * @var string[]
	 */
	static $NSs = array();

	/** When a class within a namespace is needed, `Autoload` will load the '.autoloadlist.php' file from this dir
	 * @param string	$ns		Namespace _PREFIX_! Careful!
	 * @param string	$dir	A directory/array of directories to initialize
	 */
	function init_namespace($ns, $dir) {
		self::$NSs[$ns] = $dir;
		}

	/** Lookup a classname's path
	 * @param string	$classname	The lowercase(className) to search for
	 * @return string	Path to the class file
	 * @return FALSE	Class not found
	 */
	static function class2path($classname) {
		// If a class is missing, it may be in a namespace not currently loaded
		if (!isset(self::$MAP[$classname]))
			foreach (self::$NSs as $ns => $dir)
				if (strncmp($classname, $ns, strlen($ns) ) == 0) {
					self::init($dir);
					unset(self::$NSs[$ns]);
					}
		// Try to find it now
		if (!isset(self::$MAP[$classname]))
			return FALSE;
		// Get the path
		return self::$ROOTS[  self::$MAP[$classname][0]  ].'/'.self::$MAP[$classname][1];
		}

	/** Checks whether the class exists and its file does exist
	 * @param string	$classname	The lowercase(className) to search for
	 * @return bool
	 */
	static public function class_exists($classname) {
	echo "load class_exists with classname = $classname<br>";
		$fname = self::class2path($classname);
		if ($fname === FALSE)
			return FALSE;
		return file_exists($fname);
		}
	}

function __autoload($className) {
	// TODO: namespaces autoload support
	echo "__autoload started<br>";
	$path = Autoload::class2path(strtolower($className));
	echo "path in __autoload = $path<br>";
	if ($path === FALSE) {
		$bt = debug_backtrace();
		do { $b = array_shift($bt); } while(!isset($b['file']));
		trigger_error("__autoload('$className') resolving failed at '$b[file]:$b[line]'", E_USER_ERROR);
		}
	require $path;
	}
?>