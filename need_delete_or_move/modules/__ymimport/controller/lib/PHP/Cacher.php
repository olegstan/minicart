<?php
/** 
 * A Cacher that can store any data in itself.
 * Capable of removing old entries when cache limit exceeds
 * 
 * @Name		class Cacher
 * @Date		Dec 1, 2009
 * @Version		1.0
 * @Depends		
 * @Provides	Cacher
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Cacher {

	/** Limit of items stored inside
	 * @var int
	 */
	protected $limit = null;

	/** Creates a new caching object
	 * @param int   $limit  Limit of items stored inside. NULL to create unlimited cache
	 */
	function __construct($limit = null) {
		$this->limit = $limit;
		}

	/** OnRemove handler
	 * @var callback
	 */
	protected $on_rm = null;

	/** Sets an OnRemove handler invoked when an item is removed from the cache
	 * due to exceeded limit.
	 * @param callback  $callback   function($item) to handle the removed item.
	 *                              Set NULL to unset an existing handler
	 */
	function on_remove($callback) {
		$this->on_rm = $callback;
		}

	/** OnAdd handler
	 * @var callback
	 */
	protected $on_add = null;

	/** Sets an OnAdd handler invoked when an item is added to the cache.
	 * @param callback  $callback   function($item)=item to modify the item being added
	 *                              Set NULL to unset an existing handler
	 */
	function on_add($callback) {
		$this->on_add = $callback;
		}

	/** The cache: array( key => value )
	 * @var array
	 */
	protected $cache = array();

	/** The current cache size
	 * @var int
	 */
	protected $size = 0;
	
	/** Add an item to the cache [and invoke callbacks]
	 * @param mixed $item   The item to add
	 * @return mixed $item
	 */
	protected function _cache_add($key, $item) {
		/* Handler */
		if (!is_null($this->on_add))
			$item = $this->on_add($key, $item);
		/* No action? */
		if (is_null($item))
			return null;
		/* Add */
		$this->cache[$key] = $item;
		$this->size++;
		return $item;
		}

	/** Shift an item off the cache [and invoke callbacks]
	 */
	protected function _cache_shift() {
		/* No action? */
		if ($this->size == 0)
			return;
		/* Remove */
		$item = array_shift($this->cache);
		$this->size--;
		/* Handler */
		if (is_null($this->on_rm))
			return;
		$this->on_rm($item);
		}

	/** Get an item from the cache
	 * @param string    $key    A key that uniquely identifies an item
	 * @return mixed|null The item | NULL when not found
	 */
	function get($key) {
		if (!isset($this->cache[$key]))
			return NULL;
		return $this->cache[$key];
		}

	/** Add an item to cache. This will overwrite any previous values with the same $key
	 * @param string    $key    A key that uniquely identifies an item
	 * @param mixed     $item   The item to add
	 * @return mixed $item
	 */
	function add($key, $item) {
		/* Limit? */
		if ($this->size > $this->limit)
			$this->_cache_shift();
		return $this->_cache_add($key, $item);
		}

	/** Get the item if the key exists.
	 * If not, reference to a NULL cache item is returned: you can set it
	 * NOTE: use it ONLY when you don't have 'on_add' callbacks!
	 * @param string    $key    A key that uniquely identifies an item
	 * @return &mixed
	 * @Example
	 *  if (is_null($item = &$cache->get_or_set(...)))
	 *      $item = ...;
	 */
	function &get_or_add($key) {
		/* Exists */
		if (isset($this->cache[$key]))
			return $this->cache[$key];
		/* Limit? */
		if ($this->size >= $this->limit)
			$this->_cache_shift();
		/* Add */
		$this->cache[$key] = null;
		return $this->cache[$key];
		}
	}
?>