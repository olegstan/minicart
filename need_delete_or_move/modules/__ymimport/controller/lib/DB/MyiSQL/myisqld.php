<?php
/**
 * MySQLi connection data storage. class MySQLi connects with this thingie
 *
 * @Name		class MyiSQLd
 * @Date		Feb 22, 2010
 * @Version		1.0
 * @Depends
 * @Provides	MyiSQLd
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class MyiSQLd {

	/** Bound MyiSQL object
	 * @var MyiSQL
	 */
	public $l;

	private function __construct() {}

	/** Connection host/IP: "[p:]<host|ip>"
	 * 'p:' creates a persistent connection (@since PHP 5.3.0)
	 * @var string
	 */
	public $host = null;

	/** The port number
	 * @var int
	 */
	public $port = null;

	/** Connect to host[:port]
	 * @param string	$host	Hostname || IP address
	 * @param int		$port	The optional port number to connect to
	 * @return MyiSQLd
	 */
	static function host($host, $port = null) {
		$imd = new MyiSQLd;
		$imd->host = $host;
		$imd->port = $port;
		return $imd;
		}

	/** The socket to use for connections
	 */
	public $socket = null;

	/** Connect via socket path
	 * @param string	$host	???
	 * @param string	$path	Path to MySQL socket file
	 * @return MyiSQLd
	 */
	static function socket($host, $path) {
		$imd = new MyiSQLd;
		$imd->host = $host;
		$imd->socket = $path;
		return $imd;
		}

	/** MySQL user name
	 * @var string
	 */
	public $login = null;

	/** MySQL user password. `null` attempts to connect without a password
	 * @var string
	 */
	public $pass = null;

	/** Set auth info
	 * @param string	$login	User login
	 * @param string	$pass	User password.
	 * @return MyiSQLd
	 */
	function login($login, $pass = null) {
		$this->login = $login;
		$this->pass = $pass;
		return $this;
		}

	/** Default database name
	 * @var string
	 */
	public $database = '';

	/** Table prefix. Optional.
	 * Is auto-replaced in queries with `^table` syntax
	 * @var string
	 */
	public $table_prefix = null;

	/** Set database information
	 * @param string	$database	Database name to use
	 * @param string	$prefix		Table prefix to append to table names using `^table` syntax.
	 * @return MyiSQLd
	 */
	function database($db, $prefix = null) {
		$this->database = $db;
		$this->table_prefix = $prefix;
		return $this;
		}


	function __toString() {
		$ret = "";
		if (!is_null($this->socket))
			$ret .= "$this->socket//";
		$ret .= "$this->login:$this->pass@$this->host/";
		if (!is_null($this->database))
			$ret .= $this->database;
		if (!is_null($this->table_prefix))
			$ret .= "?$this->table_prefix";
		return $ret;
		}

	/** The default client character set
	 * @var string
	 */
	public $charset = null;

	/** Set the default client character set
	 * @param string $charset	The character set. 'UTF8', 'CP1251', ...
	 * @return MyiSQLd
	 */
	function set_names($charset) {
		$this->charset = $charset;
		return $this;
		}

	/** A callback to execute on connection. Optional.
	 * Its arguments are: function(MyiSQLd $MyiSQLd, MySQLi $myisql)
	 * @var callback
	 */
	public $init = null;

	/** Set a single init function executed right after the connection is established
	 * @param callback	$function	function(MyiSQLd $imd, MySQLi $myisql)
	 * @return MyiSQLd
	 */
	function init($function) {
		$this->init = $function;
		return $this;
		}

	/** MySQLi flags
	 * @var int
	 */
	public $flags = 0;

	/** Control client-server compression.
	 * Default: off
	 * @param bool	$on	Enable?
	 * @return MyiSQLd
	 */
	function set_gzip($on) {
		if ($on)
			$this->flags |= MYSQLI_CLIENT_COMPRESS;
			else
			$this->flags ^= MYSQLI_CLIENT_COMPRESS;
		return $this;
		}

	/** SSL settings
	 * @var string[]
	 */
	public $ssl_sets = null;

	/** Control client-server SSL encryption.
	 * Default: off
	 * @param bool	$on	Enable?
	 * @return MyiSQLd
	 */
	function set_ssl($on) { //TODO: ssl_set() params!
		if ($on)
			$this->flags |= MYSQLI_CLIENT_SSL;
			else
			$this->flags ^= MYSQLI_CLIENT_SSL;
		return $this;
		}

	/** MySQLi options
	 * @var int[]
	 */
	public $options = array();

	/** Set connection timeout
	 * @param int	$sec	Connection timeout in seconds
	 * @return MyiSQLd
	 */
	function set_connect_timeout($sec) {
		$this->options[MYSQLI_OPT_CONNECT_TIMEOUT] = $sec;
		return $this;
		}
	}

?>