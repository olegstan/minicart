<?php
/**
 * MyiSQL prepared statements
 *
 * @Name		class MyiSQL
 * @Date		Feb 25, 2010
 * @Version		1.0
 * @Depends		MyiSQL
 * @Provides	MyiSQL_Mkq, MyiSQL_Stmt
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** MakeQuery class for MySQLi
 * The class allows named placeholders for MyiSQL_Stmt that are parsed ONLY on demand:
 * this means you can store your queries in this class without any notable overheads.
 * Also it has its own placeholders implementation w/out any restrictions that the Stmt ones have.
 */
class MyiSQL_Mkq implements Serializable {
	/** Parent object
	 * @var MyiSQL
	 */
	protected $m;

	/** The original query (with placeholders)
	 * @var string
	 */
	protected $query;

	/** Mkq placeholders array(array('SQL',$type,$keys))
	 * @var string[]
	 */
	protected $phmk = null;

	/** Statement placeholders array($type,$keys)
	 * @var string[]
	 */
	protected $phst = null;

	/** Bound variables for static mkq: array($type,&$var)
	 * @var &mixed[]
	 */
	protected $bmk = null;

	/** The parsed query with data inside
	 * @var string
	 */
	protected $q = null;

	/** Creates a new query
	 * The basic placeholder syntax is: {[?][type:]key/prop/key/id/...}
	 *	`key` is an array index
	 *	`prop` is the key's object property
	 *	`id` is the array key
	 *	`type` is the placeholder processing instruction. Empty = detect from `key`'s type.
	 *	'?' - Prepared Statements placeholder ('?'): is not inserted by mkq, but are stored for $this->prepare()
	 * @param MyiSQL	$m		MyiSQL object (its real_escape_string() is used)
	 * @param string	$query	A query with placeholders
	 * @param array		&$data	NULL if you just want to cache the query, array/object for immediate data insertion
	 * @throws EMyiSQL_Mkq(no_key,no_prop,wr_type)
	 */
	function __construct(MyiSQL $m, $query, &$data = null) {
		// Init
		$this->m = $m;
		$this->query = $query;
		// Finish
		if (is_array($data))
			$this->bind_mkq($data);
		}

	function bind_to_myisql(MyiSQL $m) {
		$this->m = $m;
		}

	/** Name of the query
	 * @var string
	 */
	protected $query_name;

	/** Get the query name
	 * If no name is specified - the query string is returned
	 * @return string
	 */
	function get_query_name() {
		return is_null($this->query_name)? (string)$this->query : $this->query_name;
		}

	/** Set the query name
	 * @return MyiSQL_Mkq
	 */
	function set_query_name($name) {
		$this->query_name = $name;
		return $this;
		}

	/** Clean the query cache so the changed values in bound mkq-data can appear in the query.
	 * You should invoke it ONLY when the mkq-data source changes!
	 * @return MyiSQL_Mkq
	 */
	function renew() {
		$this->q = null;
		return $this;
		}

	/** Parse placeholders in the query
	 */
	protected function _parse_ph() {
		// Prepare
		$this->phmk = $this->phst = array();
		$last = 0; $qpart = '';
		// Parse
		while ( FALSE !== $p = strpos($this->query, '{', $last) ) {
			$qpart .= substr($this->query, $last, $p-$last);
			/* === PARSE === */
			// Key
			$last = strpos($this->query, '}', $p+1);
			$key = substr($this->query, $p+1, $last++ - $p - 1);
			// Ph?
			if ($ph = $key[0] == '?')
				$key = substr($key,1);
			// Type?
			$type = null;
			if (FALSE !== $t = strpos($key, ':')) {
				$type = substr($key, 0, $t);
				$key = substr($key, $t+1);
				}
			$keys = explode('/', str_replace(array('->','[',']'),array('/','/',''), $key));
			/* === Stmt-Data === */
			// Prepared Statement?
			if ($ph) {
				$this->phst[] = array($type,$keys);
				$qpart .= '?';
				continue;
				}
			/* === Mkq-Data === */
			$this->phmk[] = array($qpart, $type,$keys);
			if (strpos($qpart, '`^') !== FALSE)
				$this->phmks_tblprefix[] = count($this->phmk)-1;
			$qpart = '';
			}
		$this->phmk[] = $qpart.=substr($this->query, $last);
		if (strpos($qpart, '`^') !== FALSE)
			$this->phmks_tblprefix[] = count($this->phmk)-1;
		}

	/** Convert the query to string with Mkq-data inserted and Stmt-data replaced with '?'
	 */
	function __toString() {
		// Cached?
		if (!is_null($this->q))
			return $this->q;
		// Parsed?
		if (is_null($this->phmk))
			$this->_parse_ph();
		// Errors?
		if (is_null($this->bmk) && count($this->phmk)>1) {
			trigger_error("The query has unbound static placeholders: string convertion impossible! Mkq: '{$this->get_query_name()}'", E_USER_ERROR);
			return '';
			}
		// Prepare
		$this->q = '';
		foreach($this->phmk as $phid=>$ph){
			if (is_string($ph)) { $this->q .= $ph; continue; }
			$this->q .= $ph[0];
			// Insert
			list($type,$v) = $this->bmk[$phid];
			// Null?
			if (is_null($v)) { $this->q .= 'NULL'; continue; }
			// Insert type
			switch ($type) {
				case 'ib':	$v=(bool)$v;
				case 'i':	$v=(int)$v; break;
				case 'd':	$v=(float)$v; break;
				case 's':	$v='"'.$this->m->real_escape_string($v).'"'; break;
				case 's=':	break;
				case 's,':
					foreach($v as &$iv)
						if (is_null($iv)) $iv='NULL';
						elseif (is_string($iv)) $iv='"'.$this->m->real_escape_string($iv).'"';
					$v=implode(',',$v);
					break;
				case 'b':
					$v = "X'".$this->_hex($v)."'";
					break;
				}
			// Insert
			$this->q .= $v;
			}
		return $this->q;
		}

	/** Get a value from $data, handle errors, detect its type
	 * @throws EMyiSQL_Mkq(no_key,no_prop)
	 * @return &mixed
	 */
	protected function &_ival(&$data, &$type, $keys) {
		/*=== FETCH THE VARIABLE ===*/
		$val = &$data;
		foreach ($keys as $key)
			if (is_object($val)) {
				if (!property_exists($val, $key))
					throw new EMyiSQL_Mkq($this->m, 'no_prop', $this->get_query_name(), $keys, $key);
				$val = &$val->$key;
				} else {
				if (!array_key_exists($key, $data))
					throw new EMyiSQL_Mkq($this->m, 'no_key', $this->get_query_name(), $keys, $key);
				$val = &$val[$key];
				}
		/*=== TYPE DETECTION ===*/
		// NULL immediate return
		if (is_null($val)) {
			if (is_null($type)) $type = 's';
			return $val;
			}
		// Detect its type
		if (is_null($type)) {
			if (is_float($val))
				$type = 'd';
				elseif (is_int($val) || is_bool($val))
				$type = 'i';
				else
				$type = 's';
			}
		// Finish
		return $val;
		}

	protected function _hex($v) {
		$r = '';
		for ($i=0,$L=strlen($v); $i<$L; $i++)
			$r .= str_pad(dechex(ord($v[$i])), 2, '0', STR_PAD_LEFT);
		return $r;
		}

	/** Bind variables to in-query placeholders (not prefixed with '?'). They will be inserted into the query on (string)cast.
	 * The function will detect data types for placeholders without `type` using the data provided.
	 * Types available for processing:
	 * 'i' integer
	 *		'ib' bool: boolean '1' or '0'
	 * 'd' double
	 * 's' string
	 *		's=' string: inserts a string without escaping
	 *		's,' string: ','-sep array (for the IN() operator). Inserted as is, but every value is escaped (when string)
	 * 'b' blob: is sent as a *hexadecimal* string (!!! laggy !!!)
	 * @param array|object	$data	The array for `key`s lookup
	 * @throws EMyiSQL_Mkq(no_key,no_prop,wr_type)
	 * @return MyiSQL_Mkq
	 */
	function bind_mkq(&$data) {
		// Parsed?
		if (is_null($this->phmk))
			$this->_parse_ph();
		// Process
		$this->q = null; // Reset the query cache
		$this->bmk = array();
		foreach ($this->phmk as $ph) {
			if (is_string($ph)) continue;
			// Prepare
			list(,$type,$keys)=$ph;
			$v = &$this->_ival($data,$type,$keys);
			if (!in_array($type, array('i','ib','d','s','s=','s,','b')))
				throw new EMyiSQL_Mkq($this->m, 'wr_type', $this->get_query_name(), $keys, null, compact('type'));
			// Store
			$this->bmk[] = array($type,&$v);
			}
		return $this;
		}

	/** BLOB names for pleasure!
	 * @var int[]
	 */
	protected $blob_names = array();

	/** Data from variables bound to BLOBs
	 * @var mixed[]
	 */
	protected $blob_data = array();

	/** Bind params to all STMT placeholders (prefixed with '?')
	 * The function will detect data types for placeholders without `type` using the data provided.
	 * Types available for processing:
	 * 'i' integer
	 * 'd' double
	 * 's' string
	 * 'b' blob: send it with `send_long_data()` or `stmt_send_blobs()`
	 * @param mysqli_stmt	$stmt	A statement you wish to insert the data into
	 * @param array|object	$data	The array/object for indexes lookup
	 * @return mixed
	 * @throws EMyiSQL_Mkq(no_key,no_prop,wr_type)
	 * @throws EMyiSQL_Stmt(bind_param)
	 */
	function bind_stmt(MyiSQL_Stmt $stmt, &$data) {
		// Parsed?
		if (is_null($this->phst))
			$this->_parse_ph();
		// Process
		$this->blob_names = array();
		$this->blob_data = array();
		$args=array('');
		foreach ($this->phst as $phid => $ph) {
			// Prepare
			list($type,$keys)=$ph;
			$v = &$this->_ival($data,$type,$keys);
			// Check insert type
			if (!in_array($type, array('i','d','s','b')))
				throw new EMyiSQL_Mkq($this->m, 'wr_type', $this->get_query_name(), $keys, null, compact('type'));
			// Blob?
			if ($type == 'b') {
				$this->blob_names[implode('/',$keys)] = $phid;
				$this->blob_data[$phid] = &$v;
				unset($v);
				$v = &$this->_null; // pass NULL to `bind_param`
				}
			// Add to args
			$args[0] .= $type;
			$args[] = &$v;
			}
		// Bind
		if (strlen($args[0]) > 0) // there can be no params actually
			if (!call_user_func_array(array($stmt, 'bind_param'),$args))
				throw new EMyiSQL_Stmt($this->m, 'bind_param', $stmt->get_query_name(), array('types' => $args[0]));
		}

	/** get_stmt_ph_names() cache
	 * @var string[]|null
	 */
	protected $_stmt_ph_names = null;

	/** Get the list of Statement placeholder names.
	 * Hierarchical names are returned '/'-imploded
	 * @return string[]
	 */
	function get_stmt_ph_names() {
		if (is_null($this->_stmt_ph_names)) {
			// Parsed?
			if (is_null($this->phst))
				$this->_parse_ph();
			// Build the list
			$this->_stmt_ph_names = array();
			foreach ($this->phst as $ph)
				$this->_stmt_ph_names[] =  implode('/', $ph[1]);
			}
		return $this->_stmt_ph_names;
		}

	/** Get BLOB field id to send data to it
	 * @param string $name	BLOB's name like 'key/prop/id'
	 * @return int
	 * @throws EMyiSQL_Mkq(no_blob)
	 */
	function stmt_blob_id($name) {
		if (!isset($this->blob_names[$name]))
			throw new EMyiSQL_Mkq($this->m, 'no_blob', $this->get_query_name(), null, null, array('blob_name' => $name, 'blobs' => $this->blob_names));
		return $this->blob_names[$name];
		}

	/** Send all BLOBS from the bound variables.
	 * @see MyiSQL_Stmt::send_bound_blobs
	 */
	function stmt_send_blobs(MyiSQL_Stmt $stmt, $bytes = 4096) {
		foreach ($this->blob_data as $blob_id => $data)
			if (is_null($data))
				continue;
			elseif (is_string($data)) {
				$L = strlen($data);
				// Short string
				if ($L<=$bytes) {
					$stmt->send_long_data($blob_id, $data);
					continue;
					}
				// Long one
				for ($i=0; $i<$L; $i+=$bytes)
					$stmt->send_long_data($blob_id, substr($data, $i, $bytes ));
			} elseif (is_resource($data)) {
				while (!feof($data))
					$stmt->send_long_data($blob_id, fread($data, $bytes));
			}
		}

	function __dump() {
		return array('query' => $this->query, 'q' => $this->q);
		}

	/* Serializable */
	/** Array of $this->phmk fields that have the `^ table prefix insertion syntax
	 * @var int[]
	 */
	protected $phmks_tblprefix = array();

	/** Serialize the object parse results for later reuse
	 * @throws EMyiSQL_Mkq(serial-^table)
	 */
	function serialize() {
		// Parsed?
		if (is_null($this->phmk))
			$this->_parse_ph();
		// Table prefix?
		if (count($this->phmks_tblprefix) != 0)
			throw new EMyiSQL_Mkq($this->m, 'serial-^table', $this->get_query_name(), null, null);
		// Serialize
		return serialize(array(
			0 => $this->get_query_name(),
			1 => $this->phmk,
			2 => $this->phst
			));
		}
	function unserialize($data) {
		$arr = unserialize($data);
		$this->query = $arr[0];
		$this->phmk = $arr[1];
		$this->phst = $arr[2];
		}
	}







/** MyiSQL prepared statement
 * Typical usage:
 * $q = $M->prepare();
 * 			|| $q->bind_stmt(array|object);
 * 			|| $q->bind_param('sss', $var1, $var2, $var3), $var1=... $var2=... $var3=... ,
 * 					|| $q->exec()->bind_result();
 * 							while( $q->fetch() ) {}
 * 					|| $q->bind_result_names();
 * 					   $q->exec()->bind_result_{array,assoc,object}($row, ...);
 * 							while( $q->fetch() ) {  ... || $Data[] = $q->copy_bound_result($row);  }
 * 					$q->reset();
 * 	$q->close()
 */
class MyiSQL_Stmt extends mysqli_stmt {
	/** Parent object
	 * @var MyiSQL
	 */
	protected $m;

	/** The original query
	 * @var MyiSQL_Mkq
	 */
	protected $mkq;

	/** Creates a prepared statement.
	 * Note that in `^table_name` the prefix is inserted in MyiSQL, not here!
	 * @param MyiSQL				$m			The connection
	 * @param string|MyiSQL_Mkq		$query		The query. Can be a Mkq object
	 * @param string				$query_name	System name of the query. Is used in Exceptions instead of full query text
	 * @throws EMyiSQL_Stmt(stmt)
	 */
	function __construct(MyiSQL $m, $query, $query_name = null) {
		if (is_object($query) && $query instanceof MyiSQL_Mkq) {
			$this->mkq = $query;
			$this->mkq->bind_to_myisql($m);
			} else
			$this->mkq = new MyiSQL_Mkq($m, $query);
		$this->mkq->set_query_name($query_name);
		parent::__construct($m, (string)$this->mkq);
		$this->m = $m;
		// Errors?
		if ($this->errno != 0)
			throw new EMyiSQL_Stmt($this->m, 'stmt', $this->get_query_name());
		}

	/** Get the query name
	 * If no name is specified - the query string is returned
	 * @return string
	 */
	function get_query_name() {
		return $this->mkq->get_query_name();
		}

	/** Get the inline Mkq object
	 * @return MyiSQL_Mkq
	 */
	function get_mkq() {
		return $this->mkq;
		}

	function __destruct() {
		parent::close();
		}

	/** Bind all params simultaneously into Mkq '?'-prefixed placeholders
	 * @param array|object	$params	The params array/object. Its keys/props are used for `key` lookup, and its values are taken
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(bind_param,send_blob)
	 */
	function bind_stmt(&$params) {
		$this->mkq->bind_stmt($this, $params);
		return $this;
		}

	/** Get BLOB field id to send data to it
	 * @param string $name	BLOB's name like 'key/prop/id'
	 * @return int
	 * @throws EMyiSQL_Mkq(no_blob)
	 */
	function get_blob_id($name) {
		return $this->mkq->stmt_blob_id($name);
		}

	/** Send data in blocks to a field marked with 'b' type. The field is identified by ID.
	 * @param int				$blob_id	Blob id
	 * @param stirng|resource	$data		Either a string of data, or a resource to read the data from
	 * @param int				$bytes		Size of the data chunk for reading from a resource
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(send_blob)
	 */
	function send_long_data($blob_id, $data) {
		if (!parent::send_long_data($blob_id, $data))
			throw new EMyiSQL_Stmt($this->m, 'send_blob', $this->get_query_name(), array('blob_id' => $blob_id));
		return $this;
		}

	/** Send all BLOB fields from the bound variables.
	 * Strings are copied in $bytes chunks, resources are read by $bytes chunks and sent to MySQL
	 * @param int	$bytes		Size of a data chunk for the transfer
	 * @return MyiSQL_Stmt
	 */
	function send_blobs($bytes = 4096) {
		$this->mkq->stmt_send_blobs($this, $bytes);
		return $this;
		}

	/** Execute the query: execute() & store_result() if needed.
	 * You should call bind_result() next!
	 * @param array|object	$params	Lazy params for $this->bind_stmt(). Handy when a query is used just once
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(stmt,store)
	 */
	function exec($params = null, $blob_bytes = 4096) {
		// Lazy params?
		if (!is_null($params)) {
			$this->mkq->bind_stmt($this, $params);
			$this->send_blobs($blob_bytes);
			}
		// Execute
		if (!parent::execute())
			throw new EMyiSQL_Stmt($this->m, 'stmt', $this->get_query_name());
		// Store result
		if ($this->field_count && !parent::store_result())
			throw new EMyiSQL_Stmt($this->m, 'store_result', $this->get_query_name());
		// Delayed bind_result() execution: ONLY after store_result() or MySQLi will use LOTS of memory!
		if (!is_null($this->_bind_result_args)) {
			call_user_func_array(array('parent','bind_result'), $this->_bind_result_args);
			$this->_bind_result_args = null;
			}
		// Finish
		if ($this->field_count)
			$this->is_running = true;
		return $this;
		}

	/** Detect whether the statement is being executed
	 * If TRUE, you must reset() it in order to reuse it.
	 * @readonly
	 * @var bool
	 */
	public $is_running = false;

	/** Stop the statement execution: result sets, errors, blobs
	 * It does not clear the binding
	 */
	function reset() {
		$this->is_running = false;
		parent::reset();
		}

	/** Whether the copied data is an object
	 * @var bool
	 */
	protected $copy_is_obj = true;

	/** List of result field names
	 * The second variable is used for copying indexed arrays while the original names stays intact
	 * @var string[]|int[]|null
	 */
	protected $copy_names_orig = null;
	protected $copy_names = null;

	/** Specify the result field names.
	 * These are later used in bind_result_{assoc|object}
	 * @param string[] $fields	Field names
	 * @return MyiSQL_Stmt
	 */
	function bind_result_names(array $fields) {
		$this->copy_names_orig = $fields;
		return $this;
		}

	/** Get the array of bound result names
	 * @return string[]|null
	 */
	function bound_result_names() {
		return $this->copy_names;
		}

	/** Arguments for parent::bind_result() delayed execution (after execute()).
	 * This is needed because bind_result() changes bound variable values!
	 * @var mixed[]|null
	 */
	private $_bind_result_args = null;

	/** Bind any array of pointers as a result row
	 * @throws EMyiSQL_Stmt(bind_result)
	 */
	protected function _bind_result_any($is_obj, &$data, array $rename = null) {
		// Names
		$this->copy_names = $this->copy_names_orig;
		if (!is_null($rename))
			$this->copy_names = $rename;
		if (!$this->copy_names && $this->field_count==0) {
			$this->_bind_result_args = null;
			return;
			}
		if (!is_array($this->copy_names) || count($this->copy_names) != $this->field_count)
			throw new EMyiSQL_Stmt($this->m, 'bind_result', $this->get_query_name(), $this->copy_names);
		// Save
		$this->copy_is_obj = $is_obj;
		// Bind
		$args = array();
		if ($is_obj)
			foreach ($this->copy_names as $prop)
				$args[] = &$data->$prop;
			else
			foreach ($this->copy_names as $key)
				$args[] = &$data[$key];
		// No delay when is running
		if ($this->is_running)
			call_user_func_array(array('parent','bind_result'), $args);
			else // Delayed execution in exec()
			$this->_bind_result_args = $args;
		}

	/** Bind the result row to an assoc array.
	 * @param array		$array	Output row array
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(bind_result)
	 */
	function bind_result_array(array &$array) {
		$this->_bind_result_any(false,  $array);
		return $this;
		}

	/** Bind the result row to an indexed array.
	 * @param array		$array	Output row array
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(bind_result)
	 */
	function bind_result_row(array &$array) {
		$this->_bind_result_any(false,  $array, range(0,  $this->field_count-1));
		return $this;
		}

	/** Bind the result row to an object.
	 * It's `clone`d upon copy_bound_result()
	 * @param object	$object	Output row object
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Stmt(bind_result)
	 */
	function bind_result_object(&$object) {
		$this->_bind_result_any(true, $object);
		return $this;
		}

	/** Fetch a result row into the bound result variables.
	 * The result is freed upon end of data.
	 * @return bool Whether there are more more rows
	 * @throws EMyiSQL_Stmt(fetch,notbound)
	 */
	function fetch() {
		$more = parent::fetch();
		if ($more === FALSE) {
			$this->is_running = false;
			throw new EMyiSQL_Stmt($this->m, ($this->errno==0)?  'notbound'  :  'fetch' ,  $this->get_query_name());
			}
		if (!($more = !is_null($more))) {
			$this->is_running = false;
			parent::free_result();
			}
		return $more;
		}

	/** Dereference the bound result array/object's contents
	 * Use this if you form an array of results: i.e. you use the row outside the reading cycle.
	 * NOTE: Ugly. Bad performance. Avoid it!
	 * @param object|array	$row	The bound array/object
	 * @return array|object	The Copy
	 */
	function copy_bound_result($row) {
		if ($this->copy_is_obj) {
			$ret = clone $row;
			foreach ($this->copy_names as $prop) {
				unset($ret->$prop); // dereference
				$ret->$prop = $row->$prop;
				}
			} else {
			$ret = $row;
			foreach ($this->copy_names as $key) {
				unset($ret[$key]); // dereference
				$ret[$key] = $row[$key];
				}
			}
		return $ret;
		}

	function __toString(){
		return (string)$this->mkq;
		}

	function __dump(){
		return array('mkq' => $this->mkq->__dump(), 'result_names' => $this->copy_names_orig);
		}
	}
?>