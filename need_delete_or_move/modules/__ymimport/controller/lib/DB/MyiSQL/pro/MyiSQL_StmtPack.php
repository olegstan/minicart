<?php
/**
 * Simple pack of MyiSQL_Stmt prepared statements with named access.
 * Because a statement can't be cloned, this class tracks unused prepared statements and reuses them:
 * it's safe to bind a query to each existing object (assuming you won't forget to unbind it :)
 *
 * @Date		May 1, 2010
 * @Version		1.0
 * @Depends		MyiSQL_Stmt
 * @Provides	MyiSQL_StmtPack
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** A collection of delay-prepared statements
 */
class MyiSQL_StmtPack {
	/** The DB connection
	 * @var MyiSQL
	 * @readonly
	 */
	public $M;

	/** Name of the pack (for exceptions)
	 * @var string
	 */
	protected $name;

	/** An array of prepared statements
	 * @var _MyiSQL_OneStmtPacked[]
	 */
	protected $stmts = array();

	/** Mandatory name prefix for queries
	 * Is used for query restriction
	 * @var string|null
	 */
	protected $namespace = null;

	/** Create a collection of prepared statements
	 * @param MyiSQL	$M			MySQL connection, connected
	 * @param string	$name		Query pack name (for exceptions)
	 */
	function __construct(MyiSQL $M, $name) {
		$this->M = $M;
		$this->name = $name;
		}

	/** Restrict this object's queries namespace with a prefix.
	 * If the current object is already restricted, another '/$namespace' is appended.
	 * @param string|null	$namespace	The query name prefix to use. It actually becomes "$namespace/$name"
	 * @return MyiSQL_StmtPack
	 */
	function restrict($namespace) {
		if (is_null($namespace) || is_null($this->namespace))
			$this->namespace = $namespace;
			else
			$this->namespace = "{$this->namespace}/$namespace";
		return $this;
		}

	/** Remove this object's namespace prefix.
	 * To do it temporarily, just `clone` me.
	 * @return MyiSQL_StmtPack
	 */
	function unrestrict() {
		return $this->restrict(null);
		}

	/** Add a namespace prefix to a query name
	 * @param string
	 * @return string
	 */
	function _qname($name) {
		return is_null($this->namespace)? $name : "{$this->namespace}/$name";
		}

	/** Store a query string in the pack
	 * @param string	$name			Name of the query: will be used for later query addressing
	 * @param string	$query			The query string
	 * @param string[]	$result_names	If the query provides a result set, you may want to name its fields
	 * @return MyiSQL_StmtPack
	 */
	function store($name, $query, $result_names = null) {
		$name = $this->_qname($name);
		$this->stmts[$name] = new _MyiSQL_OneStmtPacked($query, $result_names);
		return $this;
		}

	/** Get names of all queries in the current namespace.
	 * You can prepare each of them.
	 * If used for debugging purposes
	 * @return string[]
	 */
	function get_names() {
		if (is_null($this->namespace))
			$nslen = 0;
			else
			$nslen = strlen($this->namespace) + 1;

		$names = array();
		foreach (array_keys($this->stmts) as $full_name)
			$names[] = substr($full_name, $nslen);
		return $names;
		}

	/** Get an independent prepared query instance.
	 * When the query finishes, StmtPack can happen to reuse it for another prepare() request
	 * @param string	$name	Name of the query that was previously store()d
	 * @return MyiSQL_Stmt|null
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function prepare($name) {
		$name = $this->_qname($name);
		if (!isset($this->stmts[$name]))
			return null;
		return $this->stmts[$name]->prepare($this->M, "$this->name/$name", false);
		}

	/** Get an independent prepared query instance.
	 * The query comes 'unpacked': removed from the StmtPack's reuse list
	 * @param string	$name	Name of the query that was previously store()d
	 * @return MyiSQL_Stmt|null
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function prepare_unpacked($name) {
		$name = $this->_qname($name);
		if (!isset($this->stmts[$name]))
			return null;
		return $this->stmts[$name]->prepare($this->M, "$this->name/$name", true);
		}

	/** Repack a previously unpack()ed query.
	 * The query forgets its context when this function gets it.
	 * @param string		$name	Named of that query
	 * @param MyiSQL_Stmt	$stmt	A prepared statement got with prepare_unpacked()
	 * @return MyiSQL_StmtPack
	 */
	function repack($name, MyiSQL_Stmt $stmt) {
		$name = $this->_qname($name);
		$this->stmts[$name]->repack($stmt);
		return $this;
		}

	function __dump() {
		$stmt_stat = array();
		foreach ($this->stmts as $name => $packed)
			$stmt_stat[$name] = count($packed);
		return array(
			'pack_name' => $this->name,
			'can_reuse' => $stmt_stat,
			'namespace' => $this->namespace,
			);
		}

	/** name::namespace
	 */
	function __toString(){
		return "$this->name::$this->namespace";
		}
	}






/** A pack of one statement
 * @private
 */
class _MyiSQL_OneStmtPacked implements Countable {
	/** The query string: either a raw string or a Mkq object
	 * @var string|MyiSQL_Mkq
	 */
	protected $query;

	/** Arguments for the future call to bind_result_names()
	 * @var null|string[]
	 */
	protected $result_names;

	/** An array of prepared statements.
	 * @var MyiSQL_Stmt
	 */
	protected $stmts = array();

	/** Each statement gets a unique id. This is the last one.
	 * @var int
	 */
	protected $stmt_id = 0;

	function __construct($query, $result_names = null) {
		$this->query = $query;
		$this->result_names = $result_names;
		}

	/** Prepare a query or get one of the prepared ones
	 * @param MyiSQL	$M			MySQL connection
	 * @param string	$query_name	The name of this query (for exceptions)
	 * @param bool		$unpack		If TRUE, the function will return an unpacked query (that won't be reused until repack()ed manually)
	 * @return MyiSQL_Stmt
	 * @throws EMyiSQL_Mkq(*), EMyiSQL_Stmt(*)
	 */
	function prepare(MyiSQL $M, $query_name, $unpack) {
		// Convert to Mkq
		if (is_string($this->query))
			$this->query = $M->make($this->query);
		// Find a Stmt that's not in the running state
		$stmt = null;
		$stmt_id = null;
		foreach ($this->stmts as $id => $packed)
			if (!$packed->is_running) {
				$stmt = $packed;
				$stmt_id = $id;
				break;
				}
		// If not Stmt was found - create a new one
		if (is_null($stmt)) {
			// Prepare
			$stmt = $M->prepare($this->query, $query_name);
			if (!is_null($this->result_names))
				$stmt->bind_result_names($this->result_names);
			// Store
			$stmt_id = $this->stmt_id++;
			$stmt->stmtpack_id = $stmt_id;
			$this->stmts[$stmt_id] = $stmt;
			}
		// Gotcha!
		if ($unpack) // remove the query from the reuse list
			unset($this->stmts[$stmt_id]);
		return $stmt;
		}

	/** Repack an unpacked query: this will return it to the reuse list
	 */
	function repack(MyiSQL_Stmt $stmt) {
		$stmt->reset();
		$this->stmts[$stmt->stmtpack_id] = $stmt;
		}

	/* I: Countable */
	function count() {
		return count($this->stmts);
		}
	}
?>