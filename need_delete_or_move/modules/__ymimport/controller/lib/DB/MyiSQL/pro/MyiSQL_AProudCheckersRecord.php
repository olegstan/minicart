<?php
/** 
 * A Proud active record that uses checker placeholders to enable/disable 'WHERE' conditions
 * It generates the '!'-checkers for all placeholders in its queries so you can easily prepare something like
 * 		WHERE
 * 			({?!id} AND `id`={?i:id}) OR
 * 			... OR
 * 			({?!all}	AND TRUE)
 * and this block will be executed only if 'id' was provided in 'where': $record->where(array('id'));
 * This '!id' is actually a property of $this which is turned to '1' only if you store them in `$where` array.
 * You can manually control them with _db_checker().
 *
 * DO NOT USE '!'-checkers with functions like 'IF()': they're not optimized!
 * DO NOT ORDER BY placeholders! This results in 'using FileSort'
 * 
 * @Date		May 8, 2010
 * @Version		1.0
 * @Depends		AProudRecord
 * @Provides	AProudCheckersRecord
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

abstract class AProudCheckersRecord extends AProudRecord {
	/** Array of '!'-checker names (with '!') got from prepared statements' placeholder names
	 * @var string[][][]
	 */
	static private $_db_checkers = array();

	/** @inheritDoc
	 * Also this function turns on (=1) '!'-checkers listed in $where, turn off (=0) all the others.
	 * @param string|string[]|null	$where	Array of checker names (without '!') to turn on
	 * @return MyiSQL_Stmt|string
	 */
	protected function _db_prepare($name, $bind_stmt, $bind_result, $where = null) {
		/* === Prepare. bind_stmt() is called manually later */
		$reuse = false;
		$stmt = parent::_db_prepare($name, false, $bind_result, $reuse);

		/* === Checkers */
		$checkers = &self::$_db_checkers[get_class($this)];
		if ($bind_stmt) {
			/* === Initialize Checkers */
			if (!$reuse) {
				// Create the checkers cache if it's missing
				if (!isset($checkers)) {
					$checkers = array();
					foreach ($stmt->get_mkq()->get_stmt_ph_names() as $prop)
						if ($prop[0] == '!') {
							$this->$prop = 0;
							if (!in_array($prop, $checkers))
								$checkers[] = $prop;
							}
					}
				}
			/* === Set valid checkers states */
			// Turn off all checkers for this query
			foreach ($checkers as $prop)
				$this->$prop = null;
			// Turn on the specified checkers
			if (!is_null($where))
				foreach ((array)$where as $prop) {
					$prop = "!$prop";
					$this->$prop = 1;
					}

			/* === Bind placeholders */
			if (!$reuse)
				$stmt->bind_stmt($this);
			}

		return $stmt;
		}
	}
?>