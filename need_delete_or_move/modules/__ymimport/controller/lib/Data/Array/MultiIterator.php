<?php
/** 
 * Iterator for multiple positions
 * 
 * 
 * @Name		class MultiIterator
 * @Date		Apr 2, 2010
 * @Version		1.0
 * @Depends		
 * @Provides	MultiIterator
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class MultiIterator {
	/** The number of positions
	 * @var int
	 */
	protected $N;

	/** The positions' values
	 * @var int[]
	 * @readonly
	 */
	public $pos;

	/** Positions' minimal values
	 * @var int[]
	 */
	protected $min;

	/** Positions' maximal values
	 * @var int[]
	 */
	protected $max;

	/** Create a new iterator with $N positions, each going from $min to $max
	 * @param int			$N		The number of positions to iterate
	 * @param int|int[]		$min	The minimum value of a position
	 * @param int|int[]		$max	The maximum value of a position
	 */
	function __construct($N, $min, $max) {
		$this->N = $N;
		// Min
		if (is_array($min))
			$this->min = $min;
			else
			$this->min = array_fill(0,$N, $min);
		// Max
		if (is_array($max))
			$this->max = $max;
			else
			$this->max = array_fill(0,$N, $max);
		// Positions
		$this->pos = array();
		for ($i=0; $i<$this->N; $i++)
			$this->pos[$i] = $this->min[$i];
		}

	/** Increment the iterator
	 * @return bool Whether the iteration succeeded
	 */
	function inc() {
		for ($i=($this->N-1); $i>=0; $i--)
			if ($this->pos[$i] == $this->max[$i])
				$this->pos[$i] = $this->min[$i];
				else {// increment it
				$this->pos[$i]++;
				return true;
				}
		return false;
		}

	/** Increment the iterator, reverse order
	 * @return bool Whether the iteration succeeded
	 */
	function rinc() {
		for ($i=0; $i<$this->N; $i++)
			if ($this->pos[$i] == $this->max[$i])
				$this->pos[$i] = $this->min[$i];
				else {// increment it
				$this->pos[$i]++;
				return true;
				}
		return false;
		}
	
	function __dump() { return $this->pos; }
	function __toString() { return implode(',', $this->pos); }
	}
?>