<?php
/** A pack of funcs & classes useful for buiulding sites & scripts
 *
 * @Name		class Web
 * @Date		12.11.2008
 * @Version		1.0
 * @Depends		EBase, EWeb
 * @Provides	Web, WebUplFile
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** Web handling & data manipulation */
class Web {

	/** Remove the magic quotes, if enabled
	 */
	static public function demagic_quotes_gpc() {
		if (get_magic_quotes_gpc()==1) {
			$in = array(&$_GET, &$_POST, &$_COOKIE);
			while (list($k,$v) = each($in))
				foreach ($v as $key => $val) {
					if (!is_array($val)) {
						$in[$k][$key] = stripslashes($val); continue;
						}
					$in[] = &$in[$k][$key];
					}
			unset($in);
			}
		set_magic_quotes_runtime(0);
		}

	/** Provides HTTP authorisation for the user
	 * @param callback	$cb_check	function($login,$pass) that returns TRUE/FALSE whether the pair is ok
	 * @param string	$realm		Welcome string displayed to the user
	 * @return bool TRUE on success, FALSE on failure: must die() with an error page in case of that
	 * @example: Web::httpAuth("auth", 'Please login') or die('Fucked off brutally');
	 */
	static public function httpAuth($callback, $realm = 'Restricted access') {
		if (	!isset($_SERVER['PHP_AUTH_USER'],$_SERVER['PHP_AUTH_PW'])
			 || !$callback($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']))
			 {
			 header('HTTP/1.1 401 Unauthorized');
			 header('WWW-Authenticate: Basic realm="'.$realm.'"');
			 return FALSE;
			 }
		return TRUE;
		}

	/** Redirects the user using a header, or a script depending on headers_sent()
	 * @param string $URL	Redirection target
	 * @return string ''
	 * @example die(Web::redirect('/index.php'));
	 */
	static public function redirect($URL) {
		if (headers_sent())
			print '<script>location="'.$URL.'";</script>';
			else
			header('Location: '.$URL, true, 303); // Code 303!
		return '';
		}

	/** Handle caching for the page
	 * @param mixed $cache_time TRUE to cache forever, FALSE to disable, else - int seconds
	 * @throws
	 */
	static public function page_cache($cache_time) {
		if ($cache_time === TRUE)
			{
			header("Expires: ".gmdate("D, d M Y H:i:s", time()+86400*365)." GMT");
			header("Cache-Control: max-age="+86400*365);
			return;
			}
		die('Unimplemented: '.__FILE__.':'.__LINE__);
		}

	/** Get headers in an array like array(0 => array('GET', '/path', '1.1'), 'Content-Type' => 'text/html', ...)
	 * Cookies are an array
	 * @return array
	 */
	static public function getHeaders() {
		$headers = array(
			0 => array($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'], $_SERVER['SERVER_PROTOCOL']),
			'Cookie' => $_COOKIE
			);
		foreach ($_SERVER as $h => $v)
			if ($h != 'HTTP_COOKIE' && strncmp($h, 'HTTP_', 5) == 0)
				$headers[
					strtr(ucwords(strtolower(  strtr(substr($h,5),'_',' ')   )),' ','-')
					] = $v;
		return $headers;
		}

	/** Cached array of GET params
	 * @var array ( keyname, ... )
	 */
	static private $mkuri_getk = null;

	/** Cached encoded keys
	 * @var array ( name => string, ... )
	 */
	static private $mkuri_kcache = array();

	/** A caching arguments generator
	 * It takes the current arguments array, and applies the action:
	 * @param int $do	= 0 : Kill mode: kill arguments, others are saved
	 * 					= 1 : Save mode: save arguments, others are deleted
	 * @param string args..	List of $_GET varnames to apply the $do to
	 * @return string The generated arguments list, startting from '?' and with trailing '&'
	 * The results are cached on 1st invoke: it's CPU-safe to reinvoke.
	 * @example GET: "a=1&b=2&c=3&d=4"
	 * 		Web::mkuri(0,'a','b'); // "c=3&d=4&"
	 * 		Web::mkuri(1,'a','b'); // "a=1&b=2&"
	 * 		Web::mkuri(0) = "kill nothing" // "a=1&b=2&c=3&d=4&"
	 */
	static public function mkuri($do /* , args... */) {
		/* func args */
		$A = func_get_args();
		unset($A[0]);
		/* GET keys */
		if (is_null(self::$mkuri_getk))
			self::$mkuri_getk = array_keys($_GET);
		/* Apply filter */
		if ($do == 1)
			$keys = array_intersect($A, self::$mkuri_getk);
			else
			$keys = array_diff(self::$mkuri_getk, $A);
		/* Finish */
		$ret = '';
		foreach ($keys as $k)
			{
			if (!isset(self::$mkuri_kcache[$k]))
				self::$mkuri_kcache[$k] = $k.'='.urlencode($_GET[$k]).'&';
			$ret .= self::$mkuri_kcache[$k];
			}
		return $ret;
		}

	/** Recursively URL-encodes the specified array of arguments
	 * @param array $args	array('module' => 'news', 'page' => 1)
	 * @return string arguments in "a=b&c=d&" form
	 * NOTE: When it meets a stream resource - it reads its contents with stream_get_contents()
	 */
	static public function urlenc(array $args, $_pre = '', $_post = '') {
		$ret = '';
		foreach ($args as $k => $v)
			if (is_array($v))
				$ret .= self::urlenc($v, $_pre.$k.$_post.'[', ']');
				else
				{
				if (is_resource($v))
					$v = stream_get_contents($v);
				$ret .= $_pre.$k.$_post .'='. urlencode($v) .'&';
				}
		return $ret;
		}
	}





/** Uploaded files handling
 */
class WebUplFile {
	/** Create an upload class from the $_FILES array entry
	 * @param array	$upl	Array of 'name' , 'type', 'size', 'tmpname', 'error'
	 */
	function __construct(array &$upl) {
		if (isset($upl)) {
			$this->name = $upl['name'];
			$this->type = $upl['type'];
			$this->size = $upl['size'];
			$this->tmp_name = $upl['tmp_name'];
			$this->error = $upl['error'];
			// Extension
			if (FALSE !== $p = strrpos($this->name, '.', -2))
				$this->ext = substr($this->name, $p+1);
			$this->ok = $this->error == UPLOAD_ERR_OK;
			foreach (array('name','type','tmp_name') as $p)
				if ($this->$p === '')
					$this->$p = null;
			} else {
			$this->error = UPLOAD_ERR_NO_FILE;
			$this->ok = false;
			}
		}

	/** The original name of the file on the client machine.
	 * @var string|null
	 */
	public $name = null;

	/** The original filename's extension, if any
	 * @var string|null
	 */
	public $ext = null;

	/** The mime type of the file, if the browser provided this information.
	 * @var string|null
	 */
	public $type = null;

	/** The size, in bytes, of the uploaded file.
	 * @var int
	 */
	public $size = 0;

	/** The temporary filename on the server
	 * @var string|null
	 */
	public $tmp_name = null;

	/** The error code associated with this file upload. UPLOAD_ERR_*
	 * @var int
	 */
	public $error;

	/** Whether the upload is ok
	 * @var bool
	 */
	public $ok;
	}
?>