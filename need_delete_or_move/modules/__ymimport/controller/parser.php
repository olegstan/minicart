<?php
	require_once('simple_html_dom.php');

	function exec_parse($url, &$other, &$imgs)
	{
		$html = str_get_html($url);
	
	#################
		//Получаем модель
		//$other['model'][0] = $html->find('table[class="l-page l-page_layout_72-20"]',0)->find('h1[class="b-page-title b-page-title_type_model"]',0)->innertext;
		$other['model'][0] = $html->find('h1.b-page-title__title',0)->innertext;
		if (strpos($other['model'][0],'<')!=0)
			$other['model'][0] = substr($other['model'][0],0,strpos($other['model'][0],'<'));
			
		//Получаем бренд
		/*$tmp_brand = $html->find('#mdVendor a span',0)->innertext;
		if (isset($tmp_brand) && !empty($tmp_brand))
			$other['brand'] = trim($tmp_brand);
		else
			$other['brand'] = '';*/
		$tmp_brands = $html->find('div.b-breadcrumbs',0)->find('span[itemprop=title]');//->last_child()->innertext;
		$tmp_brand = end($tmp_brands)->innertext;
		if (isset($tmp_brand) && !empty($tmp_brand))
			$other['brand'] = trim($tmp_brand);
		else
			$other['brand'] = '';
		
		//Получаем цену
		//$other['price'] = str_replace(chr(194).chr(160), '', $html->find('table[class="l-page l-page_layout_72-20"]',1)->find('span[class="b-prices__num"]',0)->innertext);
		$other['price'] = str_replace(chr(194).chr(160), '', $html->find('span.price__int', 0)->innertext);
		
		//Краткое описание
		$other['spec_short'] = $html->find('ul[class="b-vlist b-vlist_type_mdash b-vlist_type_friendly"]',0)->outertext;
		
		//Подробное описание
		$spec_page = "http://market.yandex.ru".$html->find('p[class="b-model-friendly__title"]',0)->find('a',0)->href;		
		//$html2 = file_get_html($spec_page);		
		//$other['spec_long'] = $html2->find('table[class="b-properties"]',0)->outertext;
		$other['spec_long'] = 'temporary test';
		
		//Изображения
			//Основное
			$imgs[0]['href'] = @$html->find('table#model-pictures',0)->find('span[class="b-model-pictures__big"]',0)->find('a',0)->href;
			
			$imgs[0]['thumb'] = @$html->find('table#model-pictures',0)->find('span[class="b-model-pictures__big"]',0)->find('img',0)->src;
			
			if (!isset($imgs[0]['href']) || empty($imgs[0]['href']))
				$imgs[0]['href'] = $imgs[0]['thumb'];
			
			$ind = 1;
			//Дополнительные
			foreach($html->find('span[class="b-model-pictures__small"]') as $img_small) 
			{
				$imgs[$ind]['href'] = /*$other['img']['small']['href'][] = */@$img_small->find('a',0)->href;
				$imgs[$ind]['thumb'] = /*$other['img']['small']['thumb'][] = */@$img_small->find('img',0)->src;
				$ind++;
			}
		//Изображения считаны
	}
?>
