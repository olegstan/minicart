{* Title *}
{$meta_title='Отправка СМС' scope=parent}

{* Заголовок *}

<form method=POST>

		<div class="controlgroup">		
			<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
		</div>


    <div class="row">
		<div class="col-md-8">
		
			<legend>Настройки</legend>
			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<label>E-mail на SMSMini.ru</label>
						<input type="email" name="sms_login" class="form-control" placeholder="" value="{$settings->sms_login}"/>
					</div>
					
					<div class="form-group">
						<label>Ключ</label>
						<input type="password" name="sms_key" class="form-control" placeholder="" value="{$settings->sms_key}"/>
					</div>
					
					{if $sms_send_names}
					<div class="form-group">
						<label>Отправлять смс от имени</label>
						<select class="form-control" name="sms_sender_name" {if $sms_send_names|count == 1}disabled{/if}>
							{foreach from=$sms_send_names item=n}
								<option value="{$n.name}" {if $n.name == $settings->sms_sender_name}selected{/if}>{$n.name}</option>
							{/foreach}
						</select>
					</div>
					{/if}
					
					<div class="form-group">
						<label>Телефон администратора</label>
						<input type="text" id="phone" class="form-control" name="sms_admin_phone1" value="{if $settings->sms_admin_phone_code && $settings->sms_admin_phone}+7 ({$settings->sms_admin_phone_code}) {$settings->sms_admin_phone|phone_mask}{/if}"/>
					</div>
					
					<div class="form-group">
						<label>Дополнительный телефон администратора</label>
						<input type="text" id="phone2" class="form-control" name="sms_admin_phone2" value="{if $settings->sms_admin_phone2_code && $settings->sms_admin_phone2}+7 ({$settings->sms_admin_phone2_code}) {$settings->sms_admin_phone2|phone_mask}{/if}"/>
					</div>
				</div>
			</div>

			
			 <legend>Шаблоны и поля</legend>
			<table class="table table-striped sms-table">
				<thead>
					<tr>
						<th>Событие</th>
						<th>Кому идет смс</th>
						<th class="sms-message">Сообщение</th>
						<th class="sms-status">Статус</th>
					</tr>
				</thead>
				<tbody>
					{foreach $templates as $template}
						<tr>
							<td>{$template->action}</td>
							<td>{$template->target}</td>
							<td><textarea class="form-control" rows="2" name="value[{$template->id}]">{$template->value}</textarea></td>
							<td>
								<div class="notify-onoff sms-left">
									<div data-toggle="buttons" class="btn-group">
										<label class="btn btn-default4 btn-xs on {if $template->enabled}active{/if}">
											<input type="radio" value="1" name="enabled[{$template->id}]" {if $template->enabled}checked{/if}><i class="fa fa-check"></i>
										</label>
										<label class="btn btn-default4 btn-xs off {if !$template->enabled}active{/if}">
											<input type="radio" value="0" name="enabled[{$template->id}]" {if !$template->enabled}checked{/if}><i class="fa fa-times"></i>
										</label>
									</div>
								</div>
							</td>
						</tr>
					{/foreach}
				</tbody>
			</table>

			{*<div class="add-temlate-sms">
				<button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Добавить шаблон</button>
			</div>*}
         
		</div>
        <div class="col-md-4">

			{*<div class="list-group" id="settings-switch">
  				<a href="{$config->root_url}{$module->url}{url add=['mode'=>'settings']}" class="list-group-item {if $mode == 'settings'}active{/if}">Настройки</a>
                <a href="{$config->root_url}{$module->url}{url add=['mode'=>'templates']}" class="list-group-item {if $mode == 'templates'}active{/if}">Шаблоны и поля</a>
                <a href="{$config->root_url}{$module->url}{url add=['mode'=>'send']}" class="list-group-item {if $mode == 'send'}active{/if}">Отправка сообщения</a>
  			</div>*}
            
            
            <div class="smsinfo">
              <legend>Информация</legend>
                <p>Баланс: {if $sms_balance}<strong>{$sms_balance|convert}</strong> <span class="b-rub">Р</span>{/if} <button type="button" class="btn btn-success btn-xs sms-balance-add">Пополнить</button></p>
                <p>Стоимость смс: 0.89 <span class="b-rub">Р</span></p>
                <p class="smsminiinfo">Пополнить баланс и изменить настройки вы можете на сайте <a href="http://smsmini.ru/" target="_blank">SMSMini.ru</a></p>
            </div>
			
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Переменные</h3></div>
				<div class="panel-body">
					<table class="table table-condensed noboldth">
					<thead>
					  <tr>
						<th>Переменная</th>
						<th>Занчение</th>
					  </tr>
					</thead>
					<tbody>
						<tr>
							<td><code>$sms_site_url$</code></td>
							<td>URL магазина</td>
						</tr>
						<tr>
							<td><code>$sms_shop_name$</code></td>
							<td>Название магазина</td>
						</tr>
						<tr>
							<td><code>$sms_id$</code></td>
							<td>Номер заказа</td>
						</tr>
						<tr>
							<td><code>$sms_name$</code></td>
							<td>Имя пользователя</td>
						</tr>
						<tr>
							<td><code>$sms_mail$</code></td>
							<td>Почта пользователя</td>
						</tr>
						<tr>
							<td><code>$sms_phone$</code></td>
							<td>Телефон пользователя</td>
						</tr>
						<tr>
							<td><code>$sms_delivery_method$</code></td>
							<td>Способ доставки</td>
						</tr>
						<tr>
							<td><code>$sms_address$</code></td>
							<td>Адрес доставки</td>
						</tr>
						<tr>
							<td><code>$sms_order_link$</code></td>
							<td>Ссылка на заказ</td>
						</tr>
						<tr>
							<td><code>$sms_order_sum$</code></td>
							<td>Сумма заказа</td>
						</tr>
						<tr>
							<td><code>$sms_order_discount$</code></td>
							<td>Скидка, %</td>
						</tr>
						<tr>
							<td><code>$sms_order_discount_sum$</code></td>
							<td>Скидка, {$main_currency->sign}</td>
						</tr>
						<tr>
							<td><code>$sms_order_status$</code></td>
							<td>Статус заказа</td>
						</tr>
						<tr>
							<td><code>$sms_order_date$</code></td>
							<td>Дата заказа</td>
						</tr>
						<tr>
							<td><code>$sms_order_time$</code></td>
							<td>Время заказа</td>
						</tr>
						<tr>
							<td><code>$sms_products$</code></td>
							<td>Заказанные товары</td>
						</tr>
						<tr>
							<td><code>$sms_order_comment$</code></td>
							<td>Комментарий к заказу</td>
						</tr>
						<tr>
							<td><code>$sms_call_id$</code></td>
							<td>Номер заявки на заказ звонка</td>
						</tr>
						<tr>
							<td><code>$sms_call_time$</code></td>
							<td>Когда удобно позвонить (Заказ звонка)</td>
						</tr>
						<tr>
							<td><code>$sms_call_message$</code></td>
							<td>Сообщение (Заказ звонка)</td>
						</tr>
					</tbody>
				  </table>
				</div>
			</div>

        </div>
        
	</div>

</form>

<script type="text/javascript">
	$(function(){
		$("#phone").mask("+7 (999) 999-99-99");
		$("#phone2").mask("+7 (999) 999-99-99");
		$('textarea[name^=value]').autosize();
	});
	
	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>