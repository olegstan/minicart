<legend>Настройки</legend>
<div class="row">
	<div class="col-md-6">
		<div class="form-group">
			<label>E-mail на SMSMini.ru</label>
			<input type="email" name="sms_login" class="form-control" placeholder="" value="{$settings->sms_login}"/>
		</div>
		
		<div class="form-group">
			<label>Ключ</label>
			<input type="password" name="sms_key" class="form-control" placeholder="" value="{$settings->sms_key}"/>
		</div>
		
		{if $sms_send_names}
		<div class="form-group">
			<label>Отправлять смс от имени</label>
			<select class="form-control" name="sms_sender_name" {if $sms_send_names|count == 1}disabled{/if}>
				{foreach from=$sms_send_names item=n}
					<option value="{$n.name}" {if $n.name == $settings->sms_sender_name}selected{/if}>{$n.name}</option>
				{/foreach}
			</select>
		</div>
		{/if}
		
		<div class="form-group">
			<label>Телефон администратора</label>
			<input type="text" id="phone" class="form-control" name="sms_admin_phone1" value="{if $settings->sms_admin_phone_code && $settings->sms_admin_phone}+7 ({$settings->sms_admin_phone_code}) {$settings->sms_admin_phone|phone_mask}{/if}"/>
		</div>
		
		<div class="form-group">
			<label>Дополнительный телефон администратора</label>
			<input type="text" id="phone2" class="form-control" name="sms_admin_phone2" value="{if $settings->sms_admin_phone2_code && $settings->sms_admin_phone2}+7 ({$settings->sms_admin_phone2_code}) {$settings->sms_admin_phone2|phone_mask}{/if}"/>
		</div>
	</div>
</div>

<input type="hidden" name="mode" value="settings"/>

<script type="text/javascript">
	$(function(){
		$("#phone").mask("+7 (999) 999-99-99");
		$("#phone2").mask("+7 (999) 999-99-99");
	});
</script>