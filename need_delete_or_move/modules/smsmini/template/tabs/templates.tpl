 <legend>Шаблоны и поля</legend>
<table class="table table-striped sms-table">
	<thead>
		<tr>
			<th>Событие</th>
			<th>Кому идет смс</th>
			<th class="sms-message">Сообщение</th>
			<th class="sms-status">Статус</th>
		</tr>
	</thead>
	<tbody>
		{foreach $templates as $template}
			<tr>
				<td>{$template->action}</td>
				<td>{$template->target}</td>
				<td><textarea class="form-control" rows="2" name="value[{$template->id}]">{$template->value}</textarea></td>
				<td>
					<div class="notify-onoff sms-left">
						<div data-toggle="buttons" class="btn-group">
							<label class="btn btn-default4 btn-xs on {if $template->enabled}active{/if}">
								<input type="radio" value="1" name="enabled[{$template->id}]" {if $template->enabled}checked{/if}><i class="fa fa-check"></i>
							</label>
							<label class="btn btn-default4 btn-xs off {if !$template->enabled}active{/if}">
								<input type="radio" value="0" name="enabled[{$template->id}]" {if !$template->enabled}checked{/if}><i class="fa fa-times"></i>
							</label>
						</div>
					</div>
				</td>
			</tr>
		{/foreach}
	</tbody>
</table>

{*<div class="add-temlate-sms">
   	<button type="button" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Добавить шаблон</button>
</div>*}

<input type="hidden" name="mode" value="templates"/>

<script type="text/javascript">
	$('textarea[name^=value]').autosize();
</script>