<?php

require_once('api/core/Minicart.php');

class SMSMiniControllerAdmin extends Minicart {

	private $param_url, $params_arr, $options;

	public function set_params($url = null, $options = null){
		$this->options = $options;
		
		$url = urldecode(trim($url, '/'));
		$delim_pos = mb_strpos($url, '?', 0, 'utf-8');
		
		if ($delim_pos === false)
		{
			$this->param_url = $url;
			$this->params_arr = array();
		}
		else
		{
			$this->param_url = trim(mb_substr($url, 0, $delim_pos, 'utf-8'), '/');
			$url = mb_substr($url, $delim_pos+1, mb_strlen($url, 'utf-8')-($delim_pos+1), 'utf-8');
			$this->params_arr = array();
			foreach(explode("&", $url) as $p)
			{
				$x = explode("=", $p);
				$this->params_arr[$x[0]] = "";
				if (count($x)>1)
					$this->params_arr[$x[0]] = $x[1];
			}
		}
	}

	function fetch() {
		if (!(isset($_SESSION['admin']) && $_SESSION['admin']=='admin'))
			header("Location: http://".$_SERVER['SERVER_NAME']."/admin/login/");
			
		$mode = 'settings';
		
		if ($this->request->method('post')){
			$this->settings->sms_login = $this->request->post('sms_login');
			$this->settings->sms_key = $this->request->post('sms_key');
			if ($this->request->post('sms_sender_name'))
				$this->settings->sms_sender_name = $this->request->post('sms_sender_name');
			
			$sms_admin_phone1 = $this->request->post('sms_admin_phone1');
			$match_res = preg_match("/^[^\(]+\(([^\)]+)\).(.+)$/", $sms_admin_phone1, $matches);
			
			$sms_admin_phone2 = $this->request->post('sms_admin_phone2');
			$match2_res = preg_match("/^[^\(]+\(([^\)]+)\).(.+)$/", $sms_admin_phone2, $matches2);
			
			$this->settings->sms_admin_phone_code = '';
			$this->settings->sms_admin_phone = '';
			$this->settings->sms_admin_phone2_code = '';
			$this->settings->sms_admin_phone2 = '';
			
			if ($match_res && count($matches) == 3)
			{
				$this->settings->sms_admin_phone_code = $matches[1];
				$this->settings->sms_admin_phone = str_replace("-","",$matches[2]);
			}
			
			if ($match2_res && count($matches2) == 3)
			{
				$this->settings->sms_admin_phone2_code = $matches2[1];
				$this->settings->sms_admin_phone2 = str_replace("-","",$matches2[2]);
			}
			
			$value = (array) $this->request->post('value');
			$enabled = (array) $this->request->post('enabled');
			$ids = array_keys($value);
			foreach($ids as $id){
				$this->db->query("UPDATE __sms_templates SET value=?, enabled=? WHERE id=?", $value[$id], $enabled[$id], $id);
			}
		}
		
		if ($this->settings->sms_login && $this->settings->sms_key){
			$result = file_get_contents("http://smsmini.ru/service/request.php?username=".$this->settings->sms_login."&password=".$this->settings->sms_key."&action=get_balance");
			if ($result !== false){
				$result = json_decode($result, true);
				$this->design->assign('sms_balance', $result['balance']);
			}
			
			$result = file_get_contents("http://smsmini.ru/service/request.php?username=".$this->settings->sms_login."&password=".$this->settings->sms_key."&action=get_signs");
			if ($result !== false){
				$result = json_decode($result, true);
				$this->design->assign('sms_send_names', $result['signs']);
			}
		}
		
		$this->db->query("SELECT * FROM __sms_templates");
		$templates = $this->db->results();
		$this->design->assign('templates', $templates);
		
		return $this->design->fetch('modules/smsmini/template/smsmini.tpl');
	}
}