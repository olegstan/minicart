<?php
/** 
 * A class that behaves like an array
 * 
 * @Date		May 2, 2010
 * @Version		1.0
 * @Depends		
 * @Provides	stdArray
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

// NOTE: if you just want to export an array without reloading this methods - just implement an IteratorAggregate interface

abstract class stdArray implements Iterator,ArrayAccess,Countable {
	/** Reference to an array that's actually iterated
	 * @var mixed[]
	 */
	private $array = null; 

	/** Initialize the iterator with a class-local array
	 */
	final protected function iterator_init(array &$iterate_me) {
		$this->array = &$iterate_me;
		}
	
	/* I: Iterator: foreach($this) */
	function rewind() { reset($this->array); }
	function key() { return key($this->array); }
	function next() { next($this->array); }
	function current() { return current($this->array); }
	function valid() { return current($this->array)!==FALSE; }
	/* I: ArrayAccess: $this[] */
	function offsetExists($key) { return array_key_exists($key, $this->array); }
	function offsetGet($key) { return array_key_exists($key, $this->array) ? $this->array[$key] : null; }
	function offsetSet($key, $value) { $this->array[$key] = $value; }
	function offsetUnset($key) { unset($this->array[$key]); }
	/* I: Countable: count($this) */
	function count()		{ return count($this->array); }
	}
?>