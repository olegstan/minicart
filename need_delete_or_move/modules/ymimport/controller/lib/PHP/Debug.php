<?php
/** Nice backtracing+debugging package.
 * Including this file will enable `class Logger` to dump backtraces and contexts
 *
 * @Date		24.11.2008
 * @Version		1.0
 * @Provides	DEBUG
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** Nice methods to display, catch, measure and of course - debug! :)
 */
class DEBUG {
	/** The default style for sdump(),dump()
	 * @var array
	 */
	static protected $_sdumpstyle = array(
		'whitespace' => '%s',
		'html' => '%s',
		'keyword' => '<b>%s</b>',
		'identifier' => '%s',
		'string' => '<font color="#800000">%s</font>',
		'int' => '<font color="#0000FF">%s</font>',
		'float' => '<font color="#0000FF">%f</font>',
		);

	/** Get a nice, styled var_export
	 * @param mixed		$var	The variable to dump
	 * @param array     $style	Styling array. If null, self::$_sdumpstyle is used
	 * @return string HTML-styled var_export
	 */
	static public function sdump($var, array $style = null) {
		if (is_null($style))
			$style = self::$_sdumpstyle;
		$ret = '';
		foreach (token_get_all('<? '.var_export($var,1).' ?>') as $t) {
			if (is_string($t)) $t = array( -1, $t);
			switch ($t[0]) {
				case T_OPEN_TAG:
				case T_CLOSE_TAG:
					$fmt = ''; break; // no opening tags, please!
				case T_WHITESPACE:
					$fmt = $style['whitespace']; break;
				case T_INLINE_HTML:
					$fmt = $style['html']; break;
				case T_STRING:
					$fmt = (in_array(strtolower($t[1]), array('true','false','null')))? $style['keyword'] : $style['identifier']; break;
				case T_CONSTANT_ENCAPSED_STRING:
					$fmt = $style['string']; break;
				case T_LNUMBER:
					$fmt = $style['int']; break;
				case T_DNUMBER:
					$fmt = $style['float']; break;
				case -1:
				default:
					$fmt = $style['keyword']; break;
				}
			$ret .= sprintf($fmt, htmlspecialchars($t[1]));
			}
		return $ret;
		}

	/** Get a nice, styled var_export. FAST :)
	 * @param mixed		$var	The variable to dump
	 * @param string	$title  The title to display
	 * @return string HTML-styled var_export
	 */
	static public function dump($var, $title='') {
		print "<pre class='debug-dump'>";
		print "<b>$title</b> = ";
		print self::sdump($var);
		print "</pre>";
		}

	static protected function EDump_TXT(EBase $e, $descr = '') {
		printf("%s\n", $descr);
		$eclass = get_class($e);
		if ($e instanceof EBasePreset) $eclass = "$eclass/$e->nickname";
		printf("EXCEPTION %s(%d,%s) at %s:%d\n%s\n",
					$eclass, $e->code, $e->name,
					$e->file, $e->line,
					$e->message
					);
		if (!is_null($e->context))
			printf("Context: %s\n", str_replace("\n", "\n\t", var_export($e->context, 1)));
		printf("Dump: %s\n", str_replace("\n", "\n\t", var_export($e->__dump(), 1)));
		printf("Backtrace: \n\t%s\n", str_replace("\n", "\n\t", (string)$e->backtrace));

		if (!is_null($e->prev)) {
			print "\nPre-";
			self::EDump_Plain($e->prev);
			}
		}

	static protected function EDump_HTML(EBase $e, $descr = '', $styles) {
		//=== STYLES
		static $style_printed = false;
		if ($styles && !$style_printed) {
			print <<<STYLE
		<style>
			.exception { background: #FDD; padding: 5px; border: 3px inset #F00; }
			.exception .title { font-weight: bold; margin-left: 50px; font-size: 1.2em; text-decoration: underline;  }
			.exception .title .eclass {}
			.exception .title .eclass .ecode { color: #009; }
			.exception .title .eclass .ename { color: #900; }
			.exception .title .at { font-size: 0.8em; font-weight: normal; }
			.exception .title .descr {}
			.exception .message {  }
			.exception .context { font-family: monospace; border-left: 3px solid #090; padding-left: 5px; }
			.exception .backtrace { font-family: monospace; border-left: 3px solid #009; padding-left: 5px; }
		</style>
STYLE;
			$style_printed = true;
			}
		//=== PREPARE
		$eclass = get_class($e);
		if ($e instanceof EBasePreset) $eclass = "$eclass/$e->nickname";
		$ctx = (is_null($e->context) ? '' : self::sdump($e->context)) . "\n" .self::sdump($e->__dump());
		//=== DISPLAY
		print <<<EXCEPTION
		<div class="exception">
			<div class="title">
					<span class="eclass">
						$eclass(<span class="ecode">{$e->code}</span>, <span class="ename">{$e->name}</span>)
						</span>
					<span class="at">$e->file:$e->line</span>
					<span class="descr">{$descr}</span></div>
			<div class="message">{$e->message}</div>
			<pre class="context">{$ctx}</pre>
			<pre class="backtrace">{$e->backtrace}</pre>
EXCEPTION;
		if (!is_null($e->prev))
			self::EDump($e->prev);
		print '</div>';
		}

	/** Get a nice exception dump. It's HTML in browser and nice-formatted in CLI.
	 * @param EBase		$e		The exception to display
	 * @param string	$descr	Optional exception description
	 * @param bool		$styles	Print CSS styles when in browser
	 */
	static public function EDump(EBase $e, $descr = '', $styles = true) {
		if (php_sapi_name() == 'cli')
			self::EDump_TXT($e, $descr);
			else
			self::EDump_HTML($e,$descr,$styles);
		}

	/** First call creates a timer
	 * All subsequent calls outputs the number of seconds elapsed from the first call
	 * @param string $name	Unique name of the timer. When skipped - all timers are displayed
	 */
	static public function timer($name) {
		static $timers = array();
		$tm = microtime(true);
		if (!isset($timers[$name]))
			$timers[$name] = $tm;
			else
			printf(
				(php_sapi_name() == 'cli')? "TIMER `%s`: %.4f s\n" : "<b>TIMER `<u>%s</u>`</b>: %.4f s\n"
				, $name, $tm - $timers[$name]);
		}

	static protected $profiler = array();
	/** A little profiler
	 * Measures the code evaluation time between measure('..', true) and measure('..', false).
	 * Groups the collected info.
	 * @param string	$name	An optional name, unique within a file
	 * @param bool		$start	TRUE to start a measure-interval, FALSE to stop it
	 */
	static public function profile($name, $start) {
		/* Prepare */
		$bt = debug_backtrace();
		$bt = Backtrace::parse_btrace_step($bt[1]);
		$bt = new BTraceCALL($bt);
		$tm = microtime(true);

		if (!isset(self::$profiler[  $file=$bt->file  ][$name]))
			self::$profiler[$file][$name] = array(
				'lines' => array(null, null),
				'cur' => $tm,
				'time' => 0,
				'N' => 0,
				'call' => $bt->call,
				);
		$prof = &self::$profiler[$file][$name];
		/* Refine */
		$prof['lines'][  (int)$start  ] = $bt->line;
		if ($start)
			$prof['N']++;
			else
			$prof['time'] += $tm - $prof['cur'];
		$prof['cur'] = $tm;
		}

	/** Prints all collected profiling information */
	static public function profile_print() {
		if (count(self::$profiler) == 0) return;
		print "\n<table class=\"profiler\">";
		print "\n\t<CAPTION>Profiling data</CAPTION>";
		print "\n\t<THEAD><tr><th>File</th><th>Name</th><th>Lines</th><th>Call</th><th>N</th><th>Time</th></tr></THEAD>";
		print "\n\t<TBODY>";
		foreach (self::$profiler as $file => $profiles) {
			printf("\n\t<tr><th rowspan=%d>%s</th>", count($profiles), $file);
			$n = 0;
			foreach ($profiles as $name => $data) {
				if (($n++) != 0)
					print "\n\t\t</tr>\n\t\t<tr>";
				printf("<td>`%s`</td><td>%d-%d</td><td>%s</td><td>%d</td><td>%.5f</td>",
					$name, $data['lines'][0], $data['lines'][1], $data['call'], $data['N'], $data['time']);
				}
			print "\n\t\t</tr>";
			}
		print "\n\t</TBODY>";
		print "\n</table>";
		}
	}
?>