<?php
/**
 * Object-oriented backtrace handler
 *
 * @Name		class Backtrace
 * @Date		Dec 1, 2009
 * @Version		2.0
 * @Provides	Backtrace, BTraceCALL
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class Backtrace {
	/** Array of backtrace lines as objects
	 * @var BTraceStep[]
	 */
	protected $bt = array();

	/** The last backtrace step. Useful to get it fast
	 * @var BTraceCALL
	 */
	public $first = null;

	/** The last backtrace step. Useful to get it fast
	 * @var BTraceCALL
	 */
	public $last = null;

	/** Dump a backtrace from the call place.
	 * @param int   $backstep   Shift this number of items back.
	 * @param array $bt         Specify debug_backtrace() result if you ever need it
	 */
	function __construct($backstep = 0, array $bt = null) {
		/* Get the backtrace */
		if (is_null($bt))
			$bt = debug_backtrace();
		//DEBUG::dump($bt, 'bt');
		if ($backstep != 0)
			$bt = array_slice($bt, $backstep+1);
		/* Parse & store */
		foreach ($bt as $i => $step) {
			/* Sometimes when object->__call() backtraced, there's no [file] && [line]. Copy it from the next btitem */
			if (!isset($step['file'])) {
				$step['file'] = $bt[$i+1]['file'];
				$step['line'] = $bt[$i+1]['line'];
				}
			/* Parse & add */
			$bts = self::parse_btrace_step($step);
			if (is_null($this->last))
				$this->last = new BTraceCALL($bts);
			array_unshift($this->bt, $bts);
			}
		$this->first = new BTraceCALL($bts);
		}

	/** Parses one backtrace step array
	 * @param array     $bts    Backtrace step array
	 * @return BTraceStep
	 */
	static public function parse_btrace_step(array $bts) {
		//DEBUG::dump($bts, 'bts');
		/* === Eval && create_function */
		if (preg_match('/(.*)\((\d+)\)\s*:\s*(.*)$/', $bts['file'], $m)) // 1 => path, 2 => line, 3 => "runtime-created function|eval()'d code"
			return new BTraceInline($m[1], $m[2], $bts['line']);
		/* === Include, Require */
		if (in_array(  strtolower($bts['function'])  , array('include', 'require', 'include_once', 'require_once')))
			return new BTraceInclude($bts['file'], $bts['line'], isset($bts['args'])? $bts['args'][0] : null);
		/* __call(), __callStatic() function */
		if (in_array(  strtolower($bts['function']),  array('__call','__callStatic')  )) {
			$bts['function'] = sprintf('%s(%s)', $bts['function'], $bts['args'][0]);
			$bts['args'] = $bts['args'][1];
			}
		/* eval() */
		if (strtolower($bts['function']) == 'eval' && !isset($bts['args']))
			$bts['args'] = array('...');
		if (isset($bts['class'])) {
			/* === Static call */
			if ($bts['type'] == '::')
				return new BTraceStatic($bts['file'], $bts['line'], $bts['class'], $bts['function'], $bts['args']);
			/* === Method call */
			return new BTraceMethod($bts['file'], $bts['line'], isset($bts['object'])?$bts['object']:$bts['class'], $bts['function'], $bts['args']);
			}
		/* === Function call */
		return new BTraceFunction($bts['file'], $bts['line'], $bts['function'], $bts['args']);
		}

	/** Return $id of a backtrace item.
	 * Values <0 and >count() are automatically adjusted
	 * @param int $id   Id of backtrace step
	 * @return int
	 */
	function _btid($id) {
		if ($id<0) $id=0;
		if ($id>=count($this->bt))
			$id = count($this->bt)-1;
		return $id;
		}

	/** Get one trace step.
	 * @param int $id   Id of backtrace step to get.
	 * @return BTraceStep
	 */
	function get($id) {
		return $this->bt[  $this->_btid($id)  ];
		}

	/** Get the whole trace
	 * @return BTraceStep[]
	 */
	function get_trace() {
		return $this->bt;
		}

	/** Get one trace step in a simple form
	 * @param int	$id	Id of backtrace step to get.
	 * @return BTraceCALL
	 */
	function get_call($id) {
		return new BTraceCALL($this->bt[  $this->_btid($id)  ]);
		}

	/** Get one trace step in a simple form
	 * @param int	$id	Id of backtrace step to get (from the end)
	 * @return BTraceCALL
	 */
	function get_rcall($id) {
		return new BTraceCALL($this->bt[  $this->_btid(count($this->bt)-$id-1)  ]);
		}

	/** Get the whole trace in a simple form
	 * @return BTraceCALL[]
	 */
	function get_call_trace() {
		$ret = array();
		foreach ($this->bt as $bts)
			$ret[] = new BTraceCALL($bts);
		return $ret;
		}

	function __toString() {
		$ret = '';
		foreach ($this->bt as $bts) {
			$fl = sprintf('%s:%d', $bts->file, $bts->line);
			$ret .= "$fl\t$bts\n";
			}
		return $ret;
		}
	}

/** Backtrace call
 * A simplified object that contains only the major fields.
 * Is useful when only simple printing matters
 */
class BTraceCALL extends BTraceStep {
	/** String description of the functional call
	 * [object(::|->)]function(arg,...)
	 * @var string
	 */
	public $call;

	/** Create a simplified backtrace step description
	 * @param BTraceStep $bts   The original object
	 */
	function __construct(BTraceStep $bts) {
		$this->file = $bts->file;
		$this->line = $bts->line;
		$this->call = (string)$bts;
		$this->type = self::CALL;
		}

	function __toString() {
		return $this->call;
		}
	}

/** Backtrace step
 */
abstract class BTraceStep {
	const CALL       = 0;
	const F_INCLUDE  = 1;
	const F_INLINE   = 2;
	const F_FUNCTION = 3;
	const F_STATIC   = 4;
	const F_METHOD   = 5;

	/** The type of this line: one of self::F_* constants
	 * @var int
	 */
	public $type;

	/** The current file name
	 * @var string
	 */
	public $file;

	/** The current line number
	 * @var string
	 */
	public $line;

	function __construct($file,$line) {
		$this->file = $this->_shortpath($file);
		$this->line = $line;
		}

	/** Shorten the path so it becomes relative to the executed script
	 * @param string $file	Path to the file that should be shortened
	 * @return string
	 */
	protected function _shortpath($file) {
		// Absolute paths
		$self = explode('/',realpath(dirname($_SERVER['PHP_SELF'])));
		$path = explode('/',realpath($file));

		// Try to find the common prefix
		for ($i=0,$Ns=count($self),$Np=count($path); $i<$Ns && $i<$Np; $i++)
			if ($self[$i] != $path[$i])
				break;
				else {
				unset($self[$i]);
				unset($path[$i]);
				}

		// Simple case: path's already relative to $self
		if (count($self) == 0)
			return implode('/',$path);

		// Hard case: relative path will be too long
		if (count($self) > 2)
			return $file;

		// Nice case: relatice path is rather short
		//TODO: make & test it
		return $file;
		}

	abstract function __toString();
	}

class BTraceInclude extends BTraceStep {
	/** The included file path
	 * @var string
	 */
	public $include;

	function __construct($file, $line, $include) {
		parent::__construct($file,$line);
		$this->include = is_null($include)? '...' : $this->_shortpath($include);
		$this->type = self::F_INCLUDE;
		}

	function __toString() {
		return sprintf('include(%s)', $this->include);
		}
	}

/** Backtrace step: eval()'d code && runtime-created functions
 */
class BTraceInline extends BTraceStep {
	/** The current line in that code
	 * @var int
	 */
	public $code_line;

	function __construct($file,$line,$code_line) {
		parent::__construct($file, $line);
		$this->code_line = $code_line;
		$this->type = self::F_INLINE;
		}

	function __toString() {
		return sprintf('(inline:%d)', $this->code_line);
		}
	}

/** A pure function call
 */
class BTraceFunction extends BTraceStep {
	/** Function name
	 * @var string
	 */
	public $function;

	/** Function arguments. Shortened a bit :)
	 * @var string[]
	 */
	public $args = array();

	function __construct($file, $line, $function, array $args) {
		parent::__construct($file, $line);
		$this->function = $function;
		/* Shorten the arguments */
		foreach ($args as $arg)
			if (is_string($arg))
				$this->args[] = var_export(substr($arg,0,50),1);
				elseif (is_array($arg))
				$this->args[] = sprintf('array[%s]', count($arg));
				elseif (is_object($arg))
				$this->args[] = sprintf('object(%s)', get_class($arg));
				elseif (is_resource($arg))
				$this->args[] = sprintf('resource(%d:%s)', (int)$arg, get_resource_type($arg));
				else
				$this->args[] = (string)$arg;
		$this->type = self::F_FUNCTION;
		}

	function __toString() {
		return sprintf('%s(%s)', $this->function, implode(', ', $this->args));
		}
	}

/** A call to a class' static method
 */
class BTraceStatic extends BTraceFunction {
	/** Classname whose method was called statically
	 * @var string
	 */
	public $class_name;

	function __construct($file, $line, $class_name, $function, array $args) {
		parent::__construct($file, $line, $function, $args);
		$this->class_name = $class_name;
		$this->type = self::F_STATIC;
		}

	function __toString() {
		return sprintf('%s::%s', $this->class_name, parent::__toString());
		}
	}

/** A call to an object's method
 */
class BTraceMethod extends BTraceStatic {
	/** That object's dump (provided if it has "__dump()" method)
	 * @var array|null
	 */
	public $object_dump = null;

	function __construct($file, $line, $object, $method, array $args) {
		$class = is_object($object)?get_class($object):$object;
		parent::__construct($file, $line, $class, $method, $args);
		if (is_object($object) && method_exists($object, "__dump"))
			$this->object_dump = $object->__dump();
		$this->type = self::F_METHOD;
		}

	function __toString() {
		return sprintf('%s->%s(%s)', $this->class_name, $this->function, implode(', ', $this->args));
		}
	}
?>