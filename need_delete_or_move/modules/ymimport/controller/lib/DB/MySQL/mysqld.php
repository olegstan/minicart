<?php
/**
 * MySQL connection data storage. class MySQL connects with this thingie
 *
 * @Name		class MySQLd
 * @Date		Nov 18, 2009
 * @Version		3.1
 * @Depends
 * @Provides	MySQLd
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class MySQLd {

	private function __construct() {}

	/** Connection host string.
	 * Format: <host|ip>[:port]
	 * Also can be a path to the local socket: ':/path/to/socket'
	 * @var string
	 */
	public $host;

	/** Login for connection
	 * @var string
	 */
	public $login;

	/** Password for connection. Optional.
	 * @var string
	 */
	public $password = '';

	/** Database name to use. Optional.
	 * @var string
	 */
	public $database = null;

	/** Table prefix. Optional.
	 * Is auto-replaced in queries with `^table` syntax
	 * @var string
	 */
	public $table_prefix = null;

	/** Queries to execute on connection. Optional.
	 * @var array
	 */
	public $onconnect_queries = array();

	/** A callback to execute on connection. Optional.
	 * Its arguments are: function(MySQLd $mysqld, MySQL $client)
	 * @var callback
	 */
	public $onconnect_handler = null;

	/** Create a new connection data via TCP
	 * @param string	$host	Hostname, IP address
	 * @param int		$port	Port number to connect. If not specified - the default is used.
	 * @return MySQLd
	 */
	static public function host($host, $port = null) {
		$mysql = new MySQLd;
		$mysql->host = $host.(is_null($port)?'':":$port");
		return $mysql;
		}

	/** Create a new connection data via socket file
	 * @param string	$path	Path to MySQL socket file
	 * @return MySQLd
	 */
	static public function socket($path) {
		$mysql = new MySQLd;
		$mysql->host = ":$path";
		return $mysql;
		}

	/** Refine connection data with login information
	 * @param string	$login	User login
	 * @param string	$pass	User password.
	 * @return MySQLd
	 */
	function login($login, $pass = '') {
		$this->login = $login;
		$this->password = $pass;
		return $this;
		}

	/** Refine connection data with database information
	 * @param string	$database	Database name to use
	 * @param string	$prefix		Table prefix to append to table names using `^table` syntax.
	 * @return MySQLd
	 */
	function database($db, $prefix = null) {
		$this->database = $db;
		$this->table_prefix = $prefix;
		return $this;
		}

	/** Add an OnConnect query.
	 * Can be called multiple times.
	 * @param string    $query
	 * @return MySQLd
	 */
	function query($query) {
		$this->onconnect_queries[] = $query;
		return $this;
		}
	}
?>