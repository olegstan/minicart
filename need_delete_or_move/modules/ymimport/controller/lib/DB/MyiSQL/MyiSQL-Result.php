<?php
/**
 * MyiSQL non-prepared query result.
 * I rarely use it so it was moved away to save some parsing time :)
 *
 *
 * @Name		class MyiSQL_Result
 * @Date		Feb 22, 2010
 * @Version		1.0
 * @Depends		MyiSQL, EMyiSQL
 * @Provides	MyiSQL_Result
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

/** MyiSQL Result Set
 */
class MyiSQL_Result extends MySQLi_result {

	/** Parent MyiSQL object
	 * @var MyiSQL
	 */
	protected $m;

	/** Whether the result is USEd (or STOREd)?
	 * When USEd, no data seeking functions should be available!
	 * @var bool
	 */
	protected $used;

	function __construct(MyiSQL $m, $used) {
		parent::__construct($m, $used? MYSQLI_USE_RESULT : MYSQLI_STORE_RESULT);
		$this->m = $m;
		$this->used = $used;
		}

	function __destruct() {
		parent::free();
		}

	/** Fetch a row array
	 * @param mixed[]	$row	Output array
	 * @param int		$type	MYSQLI_ASSOC, MYSQLI_NUM, MYSQLI_BOTH
	 * @return bool TRUE/FALSE no more rows
	 */
	function fetch_array(/*&$row, $type = MYSQLI_ASSOC*/$result_type = NULL) {
		//return !is_null(  $row = parent::fetch_array($type)  );
		return parent::fetch_array($result_type);
		}

	/** Fetch an indexed row array
	 * @param mixed[]	$row	Output array
	 * @return bool TRUE/FALSE no more rows
	 */
	function fetch_r(&$row) {
		return !is_null(  $row = parent::fetch_row()  );
		}

	/** Fetch an associative row array
	 * @param mixed[]	$row	Output array
	 * @return bool TRUE/FALSE no more rows
	 */
	function fetch_a(&$row) {
		return !is_null(  $row = parent::fetch_assoc()  );
		}

	/** Fetch an object
	 * @param stdClass	$obj	Output object
	 * @param string	$class	Name of the class to instantiate
	 * @param mixed[]	$args	Additional arguments for the object's constructor
	 * @return bool TRUE/FALSE no more rows
	 */
	function fetch_o(&$obj, $class = null, array $args = null) {
		if (is_null($class))
			return !is_null( $obj = parent::fetch_object() );
			elseif (is_null($args))
			return !is_null( $obj = parent::fetch_object($class) );
			else
			return !is_null( $obj = parent::fetch_object($class, $args) );
		}

	/** Fetch the first row and take one column only.
	 * @param int|string	$col	id/name of the column to take
	 * @return mixed
	 */
	function one($col = 0) {
		$ret = parent::fetch_array( is_int($col)? MYSQLI_NUM : MYSQLI_ASSOC );
		return is_null($ret) ? null : $ret[$col];
		}

	/** Fetch all the rows, indexed
	 * @return mixed[][]
	 */
	function fetch_all_r() {
		$ret = array();
		while (!is_null(  $row = parent::fetch_row()  ))
			$ret[] = $row;
		return $ret;
		}

	/** Fetch all the rows, associative
	 * @return mixed[][]
	 */
	function fetch_all_a() {
		$ret = array();
		while (!is_null(  $row = parent::fetch_assoc()  ))
			$ret[] = $row;
		return $ret;
		}

	/** Fetch all, but make $icol the KEY for the outer array: return array( $icol => $row ).
	 * Row indexation depends on $icol,$col types.
	 * @param int|string	The indexing column: either id or name
	 * @param int|string	The single column to take as the value. When null, the whole row is taken.
	 * @return array
	 */
	function fetch_all_one($icol, $col = null) {
		// How to fetch
		if (is_null($col) || gettype($icol)==gettype($col))
			$how = is_int($icol) ? MYSQLI_NUM : MYSQLI_ASSOC;
			else
			$how = MYSQLI_BOTH;
		// Fetch!
		$ret = array();
		while (!is_null($row = parent::fetch_array($how))) {
			$key = $row[$icol];
			if (is_null($col)) {
				unset($row[$icol]);
				$ret[$key] = $row;
				} else
				$ret[$key] = $row[$col];
			}
		return $ret;
		}
	}
?>