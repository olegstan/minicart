<?php // WARNING: GENERATED FILE, NO NOT EDIT!
/* Autoloadlist file */
return array(
'autoload'=>'_Autoload.php',
'std'=>'Std.php',
'debug'=>'PHP/Debug.php',
'arr'=>'Data/Array/Array.php',
'multiiterator'=>'Logic/MultiIterator.php',

'ebase'=>'EBase.php',

'efile'=>'Exceptions.php',
'edir'=>'Exceptions.php',
'eparse'=>'Exceptions.php',
'edata'=>'Exceptions.php',
'eaction'=>'Exceptions.php',
'enet'=>'Exceptions.php',
'einterface'=>'Exceptions.php',

'ethrow'=>'EBase.php',
'ebasepreset'=>'EBase.php',
'backtrace'=>'PHP/Backtrace.php',
'btracecall'=>'PHP/Backtrace.php',
'json'=>'Web/AJAX/JSON.php',
'eweb'=>'Web/WebException.php',
'eweb404'=>'Web/WebException.php',
'ewebheaderssent'=>'Web/WebException.php',
'ewebnodata'=>'Web/WebException.php',
'ewebdata'=>'Web/WebException.php',
'mysql'=>'DB/MySQL/MySQL.php',
'mysqld'=>'DB/MySQL/mysqld.php',
'str'=>'Data/String/Str.php',
'myisql'=>'DB/MyiSQL/MyiSQL.php',
'myisqld'=>'DB/MyiSQL/myisqld.php',
'myisql_result'=>'DB/MyiSQL/MyiSQL-Result.php',
'myisql_mkq'=>'DB/MyiSQL/MyiSQL-Stmt.php',
'myisql_stmt'=>'DB/MyiSQL/MyiSQL-Stmt.php',
'myisql_stmtpack'=>'DB/MyiSQL/pro/MyiSQL_StmtPack.php',
'arecord'=>'DB/MyiSQL/pro/MyiSQL_ARecord.php',
'iarecord'=>'DB/MyiSQL/pro/MyiSQL_ARecord.php',
'aproudrecord'=>'DB/MyiSQL/pro/MyiSQL_ARecord.php',
'aproudcheckersrecord'=>'DB/MyiSQL/pro/MyiSQL_AProudCheckersRecord.php',
'netfopen'=>'Network/Netfopen.php',
'web'=>'Web/Web.php',
'webuplfile'=>'Web/Web.php',
'netfsurf'=>'Network/Netfsurf.php',
'urlworks'=>'Parse/URLworks.php',
'parsexmlreader'=>'Parse/ParseXMLReader.php',

'xmltag'=>'Parse/XMLtag.php',
'xmltagmatch'=>'Parse/XMLtag.php',

'pxmltag'=>'Parse/ParseXML.php',
'parsexml'=>'Parse/ParseXML.php',

'txmltag'=>'Parse/ParseXMLtree.php',
'parsexmltree'=>'Parse/ParseXMLtree.php',

'simplecacher'=>'Logic/SimpleCacher.php',
);
?>