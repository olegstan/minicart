<?php
/** Helps to send data in JSON.
 * Communicates with the o_O Tync's dAjax jQuery plugin
 *
 * @Name		class JSON
 * @Date		02.02.2009
 * @Version		1.0
 * @Depends		EWebHeadersSent
 * @Provides	JSON
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class JSON {
	/** Construct a new JSON printer, which will act when destructed.
	 * The object will send all the headers
	 */
	function __construct() {
		}

	/** Reports an error thru 509 HTTP code, and its status. Data can still be sent, but XHR will be unable to read it.
	 * @param string $str Error string to report using HTTP status string.
	 * @throws EWebHeadersSent When the headers were sent, and was unsuccessful to set new ones.
	 * @return string The same error message
	 */
	function error($str) {
		if (headers_sent()) throw new EWebHeadersSent();
		header('HTTP/1.0 509 '.$str, true, 509);
		print 'Error: '.$str;
		return $str;
		}

	/** Same as error(), but die()s correctly
	 * @param string $str
	 * @see error
	 * @see __destruct Notes
	 */
	function fatal($str) {
		$this->error($str);
		$this->__destruct();
		die();
		}

	static function Send($data) {
		/* Convert */
		$JSON = json_encode($data);
		/* Headers */
		if (headers_sent())
			throw new EWebHeadersSent();
		header('Content-Type: text/html; charset: UTF-8');
		/* Send */
		print $JSON;
		}

	/** Outputs the JSON headers and data.
	 * Converts non-unicode string properties to UTF-8.
	 * NOTE: on die() the destructor will raise strange exceptions. Make `$JSON = null;` to avoid
	 * @throws EWebHeadersSent When the headers were sent, and was unsuccessful to set new ones.
	 */
	function __destruct() {
		self::Send($this);
		}
	}
?>