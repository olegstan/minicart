<?php
/** 
 * Exceptions base
 * 
 * @Name		class EBase & co
 * @Date		Nov 30, 2009
 * @Version		1.5
 * @Depends		Backtrace
 * @Provides	EBase, EThrow, EBasePreset
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

class EBase extends Exception {
	public $message;
	public $code;
	public $file;
	public $line;

	/** Name of the error context. Is used for lazy exception-distinguishing
	 * @var string
	 */
	public $name;

	/** Snapshot of variables context.
	 * Array( 'varname' => value, ... )
	 * @var array|null
	 */
	public $context = array();

	/** Wrapped Exception, if any
	 * @var EBase
	 */
	public $prev = null;

	/** The backtrace object
	 * Is NULL for instances of EThrow
	 * @var Backtrace|NULL
	 */
	public $backtrace = null;

	/** Creates a new exception
	 * @param int 		$code		Custom error code
	 * @param string	$name		Name of the error context
	 * @param string	$message	Exception message
	 * @param array 	$context	compact()-snapshot of meaningful context variables. Any additional data can go here
	 */
	function __construct($code, $name, $message, $context = null) {
		/* Construct */
		parent::__construct($message, $code);
		$this->code = $code;
		$this->name = $name;
		$this->message = $message;
		$this->context = $context;
		/* Backtrace */
		if (!$this instanceof EThrow) {
			$bt = $this->getTrace();
			array_unshift($bt, array( // add the throw place
					'file' => $this->file, 'line' => $this->line,
					'function' => 'throw',
					'args' => array(  get_class($this)  ),
					));
			$this->backtrace = new Backtrace(0, $bt);
			$call = $this->get_call();
			$this->file = $call->file;
			$this->line = $call->line;
			}
		}

	/** Typical backtrace offset for a relevant function call to display
	 * Override it when your exception is risen from the deeper levels
	 * @var int
	 */
	public $_btback = 0;

	const BTBACK_INC = 1; // increment
	const BTBACK_DEC = -1; // decrement
	const BTBACK_SET = 0; // set

	/** Adjust the _btback value and return the same exception
	 * @param int	$value	The adjustment
	 * @param int	$how	self::BTBACK_* - how to adjust: increment, decrement, set
	 * @return EBase
	 */
	function btback_set($value, $how = self::BTBACK_SET) {
		switch ($how) {
			case self::BTBACK_INC:
			case self::BTBACK_DEC:
				$this->_btback += $how * $value;
				break;
			case self::BTBACK_SET:
				$this->_btback = $value;
				break;
			}
		return $this;
		}

	/** Get the function call which raised this exception.
	 * This is more useful: it contains not the exception 'throw' line, but the calling function's line
	 * @return BTraceCALL
	 */
	final function get_call() {
		if (is_null($this->backtrace))
			return new BTraceCALL(new BTraceFunction($this->file, $this->line, '???', array()));
		return $this->backtrace->get_rcall($this->_btback);
		}

	/** WARNING: This exception message exposes inner data (file,line,function)!
	 */
	function __toString() {
		return sprintf("Exception `%s(%d, %s)` at %s: %s (thrown %s:%s)",
				get_class($this), $this->code, $this->name,
				(string)$this->get_call(),
				$this->message,
				$this->file, $this->line
				);
		}

	function __dump() {
		return array();
		}

	/** Dump exception properties as an array.
	 * It should be used in the __dump() method
	 * @param	string[]	$props	The list of exception properties to dump
	 * @return mixed[]
	 */
	function _dump_props(array $props) {
		$ret = array();
		foreach ($props as $prop)
			if (property_exists($this, $prop))
				$ret[$prop] = $this->$prop;
		return $ret;
		}
	}

/** Wrapper for system errors to display them to the user and hide system details.
 */
class EThrow extends EBase {
	/** Wraps $old exception, using a new $message.
	 * NOTE:
	 * @param EBase		$prev	The wrapped exception
	 * @param string	$msg	The new message, user-friendly
	 */
	function __construct(EBase $prev, $msg) {
		parent::__construct($prev->code, $prev->name, $msg);
		$this->prev = $prev;
		$this->file = $prev->file;
		$this->line = $prev->line;
		}
	}

/** Exceptions preset: its properties are predefined in a list
 */
abstract class EBasePreset extends EBase {
	/** List of exception presets defined
	 * @var string[]
	 */
	protected $_list = array(
		'nick'		=> array(0, 'name',	'message'),
		);

	/** List mapping to properties
	 * @var string[]
	 */
	protected $_listmap = array( 0 => 'code', 1 => 'name', 2 => 'message', 3 => '_btback');

	/** Init an exception by its nickname
	 * @param string	$nick	Exception nickname from $_list
	 */
	protected function _byNick($nick) {
		if (!isset($this->_list[$nick]))
			trigger_error(sprintf('%s::_byNick(%s): failed to find by nickname', get_class($this), $nick), E_USER_WARNING);
		foreach ($this->_listmap as $id => $prop)
			if (array_key_exists($id, $this->_list[$nick])) {
				$this->$prop = $this->_list[$nick][$id];
				if ($prop == '_btback') {
					$call = $this->get_call();
					$this->file = $call->file;
					$this->line = $call->line;
					}
				}
		$this->nickname = $nick;
		}

	/** Exception nickname
	 * @var string
	 */
	public $nickname;

	function __construct($nick, $context = null) {
		parent::__construct(0, null, null, $context);
		$this->_byNick($nick);
		}
	}
?>