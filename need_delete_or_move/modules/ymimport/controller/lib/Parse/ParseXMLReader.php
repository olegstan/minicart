<?php
/** 
 * Higher level XMLReader wrapper that reads objects and handles attrs/values automatically
 * 
 * @Name		class ParseXMLReader
 * @Date		Mar 5, 2010
 * @Version		1.0
 * @Depends		
 * @Provides	ParseXMLReader
 * @Author		o_O Tync, ICQ# 1227-700, email/JID: ootync@gmail.com
 * 
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

//TODO: exceptions
//TODO: XML namespace?
//TODO: localName? WTF?
//TODO: ENTITY, END_ENTITY, ENTITY_REF
//TODO: PI, DOC, DOC_TYPE, DOC_FRAGMENT, NOTATION, XML_DECLARATION

class ParseXMLReader {
	/** XMLReader object
	 * @var XMLReader
	 */
	protected $xml;

	/** Construct the reader
	 */
	protected function __construct($method, $source, $encoding = null, $options = 0) {
		$this->xml = new XMLReader();
		$this->xml->$method($source, $encoding, $options); // TODO: errors control
		}

	/** Construct an XML Reader from XML string
	 * @param string	$xml	XML string to parse
	 * @param string	$enc	XML encoding
	 * @param int		$opt	Options: ORed LIBXML_* consts
	 * @return ParseXMLReader
	 */
	static function from_string($xml, $enc = null, $opt = 0) {
		return new ParseXMLReader("xml", $xml, $enc, $opt);
		}

	/** Construct an XML Reader from XML file
	 * @param string	$file	XML filename to parse
	 * @param string	$enc	XML encoding
	 * @param int		$opt	Options: ORed LIBXML_* consts
	 * @return ParseXMLReader
	 */
	static function from_file($file, $enc = null, $opt = 0) {
		return new ParseXMLReader("open", $file, $enc, $opt);
		}

	/** Construct an XML Reader from XML stream resource
	 * @param string	$file	XML stream to parse
	 * @param string	$enc	XML encoding
	 * @param int		$opt	Options: ORed LIBXML_* consts
	 * @return ParseXMLReader
	 */
	static function from_resource($res, $enc = null, $opt = 0) {
		return new ParseXMLReader("xml", stream_get_contents($res), $enc, $opt);
		}

	function __destruct() {
		$this->xml->close();
		}

	/** Set $path for tags?
	 * @const
	 */
	const O_PATH = 1;

	/** Ignore empty (whitespace)?
	 * @const
	 */
	const O_IGN_WS = 2;

	/** Trim text whitespace?
	 * @const
	 */
	const O_TRIM = 4;

	/** Decode HTML entities?
	 * @const
	 */
	const O_DECODE = 8;

	/** Ignore comments?
	 * @const
	 */
	const O_IGN_COMMENT = 16;

	/** Parse options
	 * @var int
	 */
	protected $opts = 0;

	/** Set parser options
	 * @param int	$options	The options to set: ORed self::O_* constants
	 */
	function set_opts($options) {
		$this->opts |= $options;
		}

	/** XMLpath array to the current tag
	 * @var string[]
	 */
	protected $_patha = array();

	/** XMLpath array to the current tag
	 * @var string
	 */
	protected $path = '';

	protected function _path_push($node) {
		array_push($this->_patha, $node);
		$this->path = implode('/', $this->_patha);
		}

	protected function _path_pop() {
		array_pop($this->_patha);
		$this->path = implode('/', $this->_patha);
		}

	/** Whether the next node is already read
	 * @var bool
	 */
	protected $no_next = false;

	/** Get the next tag instance
	 * @param &ParseXMLnode	$tag	Output tag instance
	 * @return bool Whether there's anything to read
	 */
	function read(&$tag) {
		do {
			$tag = null;
			// Read
			$read = !$this->no_next;
			$this->no_next = false;
			if ($read && !$this->xml->read())
				return false;
			// Get the tag
			switch ($this->xml->nodeType) {
				case XMLReader::ELEMENT:
					$tag = new ParseXMLnode($this->xml);
					// Path
					if ($this->opts & self::O_PATH) {
						$tag->path = $this->path;
						if (!$this->xml->isEmptyElement)
							$this->_path_push($tag->name);
						}
					// Get the value
					if (!$this->xml->isEmptyElement) { // Any value?
						$tag->value = '';
						while ($this->xml->read())
							switch ($this->xml->nodeType) {
								case XMLReader::SIGNIFICANT_WHITESPACE:
									if (!($this->opts & self::O_IGN_WS))
										$tag->value .= $this->xml->value;
										break;
								case XMLReader::TEXT:
								case XMLReader::CDATA:
									$tag->value .= ($this->opts & self::O_TRIM)? trim($this->xml->value) : $this->xml->value;
									break;
								default:
									break 2;
								}
						$this->no_next = true; // Prevent skipping a tag
						}
					// Decode special chars
					if ($this->opts & self::O_DECODE)
						$tag->decode();
					break;
				case XMLReader::END_ELEMENT:
					$tag = new ParseXMLnode($this->xml);
					$tag->depth++;
					// Path
					if ($this->opts & self::O_PATH) {
						$tag->path = $this->path;
						$this->_path_pop();
						}
					// Decode special chars
					if ($this->opts & self::O_DECODE)
						$tag->decode();
					break;
				case XMLReader::COMMENT:
					if ($this->opts & self::O_IGN_COMMENT)
						continue 2;
					// Create
					$tag = new ParseXMLnode($this->xml);
					$tag->value = $this->xml->value;
					// Path
					if ($this->opts & self::O_PATH)
						$tag->path = $this->path;
					break;
				case XMLReader::WHITESPACE: // whitespace between tags
				case XMLReader::SIGNIFICANT_WHITESPACE:
					break;
				//TODO: implement all entities!
				default:
					trigger_error('Unsupported node type: '.$this->xml->name.':'.$this->xml->nodeType, E_USER_ERROR);
				}
			} while (is_null($tag));
		return true;
		}
	}

abstract class _ParseXMLobject {
	/** Qualified name
	 * @var string
	 */
	public $name;

	/** Local name
	 * @var string
	 */
	public $localName;

	/** Depth of the object
	 * @var int
	 */
	public $depth;

	/** Text value, its contents or CDATA
	 * @var string|null
	 */
	public $value = null;

	/** The URI of the namespace associated with the node
	 * @var string
	 */
	public $ns_uri;

	/** The prefix of the namespace associated with the node
	 * @var string
	 */
	public $ns_prefix;

	/** The xml:lang scope which the node resides
	 * @var string
	 */
	public $lang;

	function __construct(XMLReader $xml) {
		$this->name = $xml->name;
		$this->localName = $xml->localName;
		$this->depth = $xml->depth;

		$this->ns_uri = $xml->namespaceURI;
		$this->ns_prefix = $xml->prefix;
		$this->lang = $xml->xmlLang;
		}

	function __dump() {
		return (array)$this;
		}

	/** Decode HTML special chars
	 */
	function decode() {
		if (strpos($this->value, '&') !== FALSE)
			$this->value = html_entity_decode($this->value);
		}
	}

class ParseXMLnode extends _ParseXMLobject {
	/** Start element: <tag>
	 * @const
	 */
	const ELEMENT_START = 0;

	/** Empty element: <tag />
	 * @const
	 */
	const ELEMENT_EMPTY = 1;

	/** End element: </tag>
	 * @const
	 */
	const ELEMENT_END = 2;

	/** Comment node
	 * @const
	 */
	const COMMENT = 3;

	//TODO: implement all entities!

	/** XMLpath to the current tag (includes the parent, excludes the current).
	 * Works only when O_PATH is set.
	 * For END tags, contains the current one.
	 * @var string
	 */
	public $path = null;

	/** Type of the node: self::*
	 * @var int
	 */
	public $type;

	/** Attributes of the tag
	 * @var ParseXMLattr[]
	 */
	public $attrs = array();

	function __construct(XMLReader $xml) {
		parent::__construct($xml);
		// Node type
		switch ($xml->nodeType) {
			case XMLReader::ELEMENT:
				$this->type = ($xml->isEmptyElement)? self::ELEMENT_EMPTY : self::ELEMENT_START;
				break;
			case XMLReader::END_ELEMENT:
				$this->type = self::ELEMENT_END;
				break;
			case XMLReader::COMMENT:
				$this->type = self::COMMENT;
				break;
			//TODO: implement all entities!
			}
		// Attributes
		if ($xml->hasAttributes)
			while($xml->moveToNextAttribute())
				$this->attrs[] = new ParseXMLattr($xml);
		$xml->moveToElement(); // Restore the pointer to the node from the last attribute
		}

	/** Get the tag's XML name
	 * 'tag', '/tag', 'tag/', '#comment', 
	 * @return string
	 */
	function xmlname() {
		switch ($this->type) {
			case self::ELEMENT_START:
			case self::COMMENT:
				return "$this->name";
			case self::ELEMENT_EMPTY:
				return "$this->name/";
			case self::ELEMENT_END:
				return "/$this->name";
			//TODO: implement all entities!
			}
		}

	function __toString() {
		$ret = '<';
		if ($this->type == self::ELEMENT_END)
			$ret .= '/';
		$ret .= $this->name;
		foreach ($this->attrs as $attr)
			$ret .= " $attr->name=\"$attr->value\"";
		if ($this->type == self::ELEMENT_EMPTY)
			$ret .= " /";
		$ret .= ">";
		if ($this->value)
			$ret .= $this->value;
		//TODO: implement all entities!
		return $ret;
		}

	/** Decode HTML special chars
	 */
	function decode() {
		parent::decode();
		foreach ($this->attrs as $attr)
			$attr->decode();
		}
	}

class ParseXMLattr extends _ParseXMLobject {
	/** Indicates if attribute is defaulted from DTD
	 * @var bool
	 */
	public $isDefault;

	function __construct(XMLReader $xml) {
		parent::__construct($xml);
		$this->value = $xml->value;
		$this->isDefault = $xml->isDefault;
		}

	function __toString() {
		return $this->value;
		}
	}
?>