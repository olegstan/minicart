<?php
/** Utilities for handling URIs and URLs that came from the outside
 *
 * @Name		class URLworks
 * @Date		25.11.2008
 * @Version		1.0
 * @Depends
 * @Provides	URLworks
 * @Author		o_O Tync, ICQ# 1227-700
 *
 * @License		You're allowed to copy, distribute, and modify this code while the name of the original author is mentioned :)
 *
 * Enjoy!
 */

//TODO: the `__dump()` method that returns an array

/** URL functions */
class URLworks
	{
	/**
	 * Central part of a PREG expression that parses URLS.
	 * Use '~^...$~S' to check an URL, and '~...~S' to parse them out
	 * Is NOT much slower that parse_url(), but able to handle partial URLs :)
	 */
	const PREG_URL = '(?i:(?:(https|http|ftp)://)?(?:([\da-z_-]{1,32})(?::([\da-z_-]{1,32}))?@)?((?:[\da-z_-]{1,128}\.)+[a-z]{2,5}|(?:\d{1,3}\.){3}\d{1,3})(/[a-z0-9.,_@%&?+=\~/-]*)?(#[^ \'\"&<>]*)?)';

	/** Checks whether an URL is in valid RFC format
	 * @return bool	FALSE when not ok
	 * @return bool	TRUE when OK and HAS protocol part
	 * @return int	1 when OK and HAS NO protocol part
	 */
	static function check($URL)
		{
		$r = preg_match('~^'.self::PREG_URL.'$~S', trim($URL), $m);
		if (!$r) return FALSE;
		if ($m[1] === '') return 1;
		return TRUE;
		}

	/** Parse an URL and return its parts
	 * @return array Array( 0 => whole, [1 => protocol], [2 => user, [3 => pass,]] 4 => Host|IP, 5 => URI, [6 => Hash] ).
	 * @return FALSE on invalid URL
	 */
	static function parse($URL)
		{
		$r = preg_match('~^'.self::PREG_URL.'$~S', trim($URL), $m);
		if (!$r) return FALSE;
		if ($m[5] == '') $m[5] = '/';
		return $m;
		}

	/** Turn all slashes '\' to backslashes '//', and remove multiple slashes occurences '/////' except for protocol delimiter
	 * @return string renewed URL
	 */
	static function slashes($URI) {return substr($URI=trim($URI),0, 8).preg_replace('|[\\\/]+|S', '/', substr($URI,8));}

	/** When working with relative URLs and their relations - a new URLworks class must be created
	 * @param string $URL	Specifies the base URL from which all further calculations are made. Must be FULL
	 */
	function __construct($URL)
		{
		$this->URL = $this->slashes($URL);
		if (strpos($this->URL, '/', 10) === FALSE)
			$this->URL .= '/';
		}

	/** Original URL */
	private $URL = null;
	/** Cached items. See below */
	private $URLp = null; /* parsed */
	private $URLb = null; /* broken */

	/** Convert a relative URI into an URL ("/path/to", "?argument", "#place")
	 * @param string	$uri		URI to convert. If URL given - is returned as is, with slashes turned [and dots killed]
	 * @param bool		$killdots	Whether to expand '.' and '..'. Usually this is not needed
	 * @return string URL, slashes '/'
	 * @return FALSE on URI incorrect
	 */
	function URI($URI, $killdots=false)
		{
		$URI=$this->slashes($URI);
		/* Prepare */
		if (strpos($URI, '://') === FALSE)
			{
			if (is_null($this->URLp)) $this->_mkURLp();
			/* URLialize */
			if (isset($this->URLp[  $c=$URI[0]  ])) /* args addition | root straight path */
				return $this->URLp[$c].$URI;
			/* Else - relative path: "./path/to/../file" */
			$URI = $this->URLp['/'].'/'.$URI;
			}
		/* Kill dots */
		if (!$killdots || strpos($URI,'..') === FALSE) return $URI;
		$path = array(); $match = array();
		preg_match('|^([^:]+://[^/]+/)([^?#]*)(.*)$|S', $URI, $match); /* [1] => proto://host/ , [2] => path, [3] => other */
		unset($match[0]);
		$match[2] = explode('/', $match[2]);
		while (!is_null($dir = array_shift($match[2])))
			if ($dir === '.') continue;
			elseif ($dir === '..' && count($path)) array_pop($path);
			else $path[] = $dir;
		$match[2] = implode('/', $path);
		return implode($match);
		}

	/** Returns the hierarchical difference between the two URLs: compares domains and paths. Handles '..' and '/'
	 * @param string $URL	New URL to compare with the original constructor's
	 * NOTE: Protocol is not compared
	 * NOTE: Directories MUST have a trailing slash
	 * @return array( 0 => seld:DIFF_*, [1 => levels] ). @see below
	 */
	function cmp($URL)
		{
		/* prepare */
		if (is_null($this->URLb)) $this->_MkPurl($this->URL);
		$base = $this->URLb; $new = $this->_MkPurl($URL);
		/* perform */
		//TODO: REWRITE!
		if ($base[0] != $new[0]) /* another 2nd level domain */
			return array( 0 => self::DIFF_HOST, 1 => 0 );
		$ret = array();
		for ($I=1;$I<=2;$I++) /* domain, path */
			{
			if ($base[$I] == $new[$I]) { $ret[$I-1] = 0; continue; } /* exact match */
			for ($i=0,$NB=count($base[$I]),$NN=count($new[$I]),$N = max($NN,$NB), $ret[$I-1]=$NB;$i<$N;$i++,$ret[$I-1]--) /* $d is the difference. GO while equal */
				if ($i >= $NB) /* a SUB one, indeed */
					{$ret[$I-1] = -($NN - $NB); break;} /* delta-length */
					elseif (!isset($new[$I][$i])) break;
					elseif ($new[$I][$i] != $base[$I][$i]) {$ret[$I-1]--; break;}
			if ($ret[$I-1] == 0) $ret[$I-1] = FALSE; /* same level, another type */
			}
		if ($ret[1] != 0) $ret[1] = -$ret[1];
		/* remake old to new */
		if ($ret[0] === FALSE || $ret[0] != 0)
			$ret = array(0 => self::DIFF_DOMAIN, 1 => (int)$ret[0]);
			/* else domains do not differ */
			elseif ($ret[1] === FALSE || $ret[1] != 0)
			$ret = array(0 => self::DIFF_PATH, 1 => (int)$ret[1]);
			/* else no difference */
			else $ret = array(0 => self::DIFF_NONE, 1 => 0);
		return $ret;
		}

	/** They are exactly the same */
	const DIFF_NONE = 0;
	/** 1|2 level domain differs */
	const DIFF_HOST = 1;
	/** Domains of level 3+ differ. [1] Represents the relation:
	 * -N		is a SUB-domain of level abs(N)
	 * +N		is a SUP-domain of level abs(N)
	 * 0		is a domain of the same level, but is different
	 */
	const DIFF_DOMAIN = 2;
	/** Paths differ. [1] Represents the relation:
	 * -N		is a SUB-folder
	 * +N		is a SUP-folder
	 * 0		is a folder of the same level, but is different
	 */
	const DIFF_PATH = 3;

	/** Convert $this->URL to array(...) */
	private function _mkURLp()
		{
		$this->URLp = array( /* parts of URL up to last character occurence */
			'/' => substr($this->URL, 0, strrpos($this->URL,'/')),
			'?' => ($p=strpos($this->URL,'?')) ? substr($this->URL, 0,$p) : $this->URL,
			'#' => ($p=strpos($this->URL,'#')) ? substr($this->URL, 0,$p) : $this->URL,
			);
		}

	/** Convert "http://page.tync.narod.ru/files/images" -> array( 'ru.narod', array('tync','page'), array('files', 'images') */
	private function _MkPurl($URL)
		{
		$ret = array();
		$purl = parse_url($this->slashes($URL));
		if (!$purl) return FALSE;
		/* match domains: [1] -> 3+ lvl, [2] - 2 lvl, [3] -> 1 lvl */
		$match = array();
		preg_match('/^(?:(.*)\.)?([a-z0-9_-]+)\.([a-z0-9_-]+)$/iS',$purl['host'], $match);
		$ret[0] = array($match[3],$match[2]);
		$ret[1] = $match[1]? array_reverse(explode('.', $match[1])) : array();
		/* match path */
		$path = explode('/', $purl['path']); array_pop($path); /* skip filename | trail slash */
		$ret[3] = array();
		while (!is_null($p = array_shift($path)))
			if ($p === '.' || $p === '') continue;
			elseif ($p === '..' && count($ret[3])) array_pop($ret[3]);
			else $ret[3][] = $p;
		return $ret;
		}
	}
?>