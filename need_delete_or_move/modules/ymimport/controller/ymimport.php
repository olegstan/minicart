<?php

require_once('api/core/Minicart.php');
require('init.php');
require('parser.php');

set_time_limit(0);

/**
 * Yandex Market import page for simpla
 */

class YMImportControllerAdmin extends Minicart {

	private $param_url, $params_arr, $options;

	public function set_params($url = null, $options = null){
		$this->options = $options;
		
		$url = urldecode(trim($url, '/'));
		$delim_pos = mb_strpos($url, '?', 0, 'utf-8');
		
		if ($delim_pos === false)
		{
			$this->param_url = $url;
			$this->params_arr = array();
		}
		else
		{
			$this->param_url = trim(mb_substr($url, 0, $delim_pos, 'utf-8'), '/');
			$url = mb_substr($url, $delim_pos+1, mb_strlen($url, 'utf-8')-($delim_pos+1), 'utf-8');
			$this->params_arr = array();
			foreach(explode("&", $url) as $p)
			{
				$x = explode("=", $p);
				$this->params_arr[$x[0]] = "";
				if (count($x)>1)
					$this->params_arr[$x[0]] = $x[1];
			}
		}
	}

	/** An IP address[es] to bind to for outgoing connections
	 * @var string[]|null
	 */
	public $bindto = array(
			/*'193.169.178.36',
			'193.169.179.58',
			'193.169.179.56',
			'193.169.179.57',
			'193.169.179.55',
			'193.169.179.54',
			'193.169.179.53',
			'193.169.179.52',
			'193.169.178.247',
			'193.169.178.246',
			'193.169.178.207',
			'193.169.178.206'*/
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147',
			'148.251.179.147'
			);
	
	/** Network surfer
	 * @var NetfSurf
	 */
	public $Netfsurf;
	
	/** MySQL
	 * @var MySQL
	 */
	protected $M;

	function __construct() {
		$this->M = oo_mysql_object();
		
		/* ====== Create the surfer */
		// Pick an IP
		$ipid = null;
		if (is_array($this->bindto) && count($this->bindto))
			$ipid = array_rand($this->bindto);
		// Init the surfer
		$this->Netfsurf = new NetfSurf("modules/ymimport/controller/tmp/YMarket-{$ipid}.cookies");
		$this->Netfsurf->set_timeout(10);
		if (!is_null($ipid))
			$this->Netfsurf->set_interface($this->bindto[$ipid]);
			
		$Q = $this->Netfsurf->Request('https://passport.yandex.ru/passport?mode=auth&amp;from=market&amp;retpath=http://market.yandex.ru/?ncrnd=9302', null, array(
			'login' => 'popov.puser',
			'passwd' => '01091986',
			'twoweeks' => 'no',
			'timestamp' => ''
		));
	}
	
	function test_list_url($url) {
		return preg_match('~^http[s]?://market\.yandex\.(ru|ua)/guru\.xml\?~iS', $url);
	}
	
	function test_model_url($url) {
		return preg_match('~^http[s]?://market\.yandex\.ru/model\.xml\?~iS', $url);
	}
	
	function parse_list($page) {
		$XML2 = str_get_html($page);

		$products = array();
		$get = array();
		$div_page = $XML2->find('div[class="page__b-offers__guru"]',1);
		if (isset($div_page))
		{
			foreach($div_page->find('div[class="b-offers b-offers_type_guru"]') as $prod)
			{				
				$arr = array();
				$div_desc = $prod->find('div[class="b-offers__desc"]',0);
				
				if($div_desc)
				{
					$a = $div_desc->find('a',0);
					if ($a)
					{
						$arr['url'] = trim($a->href);
						$arr['model'] = trim($a->innertext);
						
						$p = $div_desc->find('p',0);
						
						if ($p)
						{
							$arr['spec'] = trim($p->innertext);
						
							$get[] = $arr;
						}
					}
				}
			}
		}
		/* Create objects */
		foreach ($get as $g){
			$product = new stdClass;
			
			$product->url = $g['url'];
			$product->model = $g['model'];
			$product->spec = $g['spec'];
			
			if (strncmp($product->url, './', 2) === 0)
				$product->url = substr($product->url, 1);
			$product->url = "http://market.yandex.ru" . $product->url;
			
			$products[] = $product;
		}
		$XML2->clear();
		return $products;
	}
	
	function parse_model($page, $d = null) {
		$html = str_get_html($page);
		
		//die($page);
		
		$product = new stdClass;
		//############## Получаем модель
		$product->model = str_replace(chr(194).chr(160), ' ', $html->find('div.product-title h1',0)->innertext);
		if (strpos($product->model,'<')!=0)
			$product->model = substr($product->model, 0, strpos($product->model, '<'));
		//############## Получаем бренд
		//$tmp_brands = $html->find('div.b-breadcrumbs',0)->find('span[itemprop=title]');//->last_child()->innertext;
		$tmp_brand = $html->find('ul.breadcrumbs2 li.breadcrumbs2__item:last span[itemprop=title]', 0)->innertext;
		//$tmp_brand = end($tmp_brands)->innertext;
		if (isset($tmp_brand) && !empty($tmp_brand))
			$product->brand = trim($tmp_brand);
		else
			$product->brand = '';
		//############## Получаем цену
		$product->price = str_replace(chr(194).chr(160), '', $html->find('span.price__int', 0)->innertext);
		//############## Краткое описание
		$product->spec_short = $html->find('ul[class="b-vlist b-vlist_type_mdash b-vlist_type_friendly"]',0)->outertext;
		//############## Подробное описание
		$spec_page = "http://market.yandex.ru".$html->find('p[class="b-model-friendly__title"]',0)->find('a',0)->href;
		$product->spec_long = '<error>';
		for($try_step = 0 ; $try_step < 3 ; $try_step++){
			try{
				$result = $this->Netfsurf->Request($spec_page);
				if (strpos($result->url, 'captcha') === FALSE){
					$content2 = stream_get_contents($result->f);
					$html2 = str_get_html($content2);
					$product->spec_long = $html2->find('table[class="b-properties"]',0)->outertext;
					break;
				}
				continue;
			}
			catch(ENet $e){
				return 2;
			}
		}
		
		//############## Изображения
		$product->imgs = array();
		$product->imgs[0] = new stdClass;
		$product->imgs[0]->big = @$html->find('table#model-pictures',0)->find('span[class="b-model-pictures__big"]',0)->find('a',0)->href;
		$product->imgs[0]->thumb = @$html->find('table#model-pictures',0)->find('span[class="b-model-pictures__big"]',0)->find('img',0)->src;
		
		if (!empty($product->imgs[0]->big))
			$product->imgs[0]->big = "http:" . $product->imgs[0]->big;
		if (!empty($product->imgs[0]->thumb))
			$product->imgs[0]->thumb = "http:" . $product->imgs[0]->thumb;
		
		if (!isset($product->imgs[0]->big) || empty($product->imgs[0]->big))
			$product->imgs[0]->big = $product->imgs[0]->thumb;
		
		$ind = 1;
		foreach($html->find('span[class="b-model-pictures__small"]') as $img_small) 
		{
			$product->imgs[$ind] = new stdClass;
			$product->imgs[$ind]->big = @$img_small->find('a',0)->href;
			$product->imgs[$ind]->thumb = @$img_small->find('img',0)->src;
			
			if (!empty($product->imgs[$ind]->big))
				$product->imgs[$ind]->big = "http:" . $product->imgs[$ind]->big;
			if (!empty($product->imgs[$ind]->thumb))
				$product->imgs[$ind]->thumb = "http:" . $product->imgs[$ind]->thumb;
			
			$ind++;
		}
		
		$html->clear();
		return $product;
	}
	
	function insert_product_to_db($product, $spec_replace){
		/* ====== Collect data for insertion */
		$data = array(
			'product_id' => null,
			'url' => $this->furl->generate_url($product->model), // URL of the current product
			'cat' => $_SESSION['YMimport']['category_id'], // its category id
			'model' => trim($product->model), // Model string
			'brand' => $product->brand,
			'descr' => is_null($spec_replace)? $product->spec_short : $spec_replace,//'', // Short description text
			'body_short' => $product->spec_short,
			'body' => $product->spec_long,//'', // Long description
			'price' => $this->settings->modules_ymimport_parse_price ? (is_null($product->price)? 0 : $product->price) : 0,
			'order_num' => 0, // sorting
			'img' => '', // BIG image filename in 'files/products'
			'thumb' => '', // SMALL image filename in 'files/products'
			'enabled' => $this->settings->modules_ymimport_product_visible, // Is it enabled by default? 0|1

			'specs_short' => '',
			'specs_long' => ''
			);

		/* ====== Add to database */

		$brand_id = 0;
		if (!empty($data['brand']))
		{
			$tmp_brand = $this->brands->get_brand($data['brand']);
			if (!$tmp_brand)
			{
				$new_brand = new StdClass;
				$new_brand->name = $data['brand'];
				$new_brand->meta_title = $new_brand->name;
				$new_brand->meta_keywords = $new_brand->name;
				$new_brand->meta_description = $new_brand->name;
				$brand_id = $this->brands->add_brand($new_brand);
			}
			else
				$brand_id = $tmp_brand->id;
		}
	
	
		/* === Add product */
		
		$new_product = new StdClass;
		$new_product->brand_id = $brand_id;
		$new_product->url = $data['url'];
		$new_product->name = $data['model'];
		$new_product->annotation = empty($data['descr']) ? '' : $data['descr'];
		$new_product->annotation2 = empty($data['body_short']) ? '' : $data['body_short'];
		$new_product->body = empty($data['body']) ? '' : $data['body'];
		$new_product->meta_title = $new_product->name;
		$new_product->visible = $data['enabled'];
		
		if (!empty($new_product->annotation)){
			$str_name = "";
			if (!empty($new_product->name))
				$str_name = mb_strtolower(strip_tags(html_entity_decode($new_product->name)), 'utf-8');
			$str = mb_strtolower(strip_tags(html_entity_decode($new_product->annotation)), 'utf-8');
			$str = preg_replace("/[^a-zа-я0-9\s]/u", " ", $str);
			$str = preg_replace("/\s+/u", "  ", $str);				//заменим пробелы на двойные пробелы, чтоб следующая регулярка работала, иначе между словами будет общий пробел и условие не пройдет
			$str = preg_replace("/(\s[^\s]{1,3}\s)+/u", " ", $str);	//remove words with length<=3
			$str = preg_replace("/(\s\s)+/u", " ", $str);			//remove double spaces
			$str = trim($str, 'p ');
			$str = preg_replace("/\s+/u", ", ", $str);
			$str = empty($str_name)?$str:(empty($str)?$str_name:$str_name.", ".$str);
			$str_arr = explode(', ', $str);
			$str = '';//mb_substr($str, 0, 200, 'utf-8');
			for($i = 0; $i < count($str_arr); $i++){
				if ((mb_strlen($str, 'utf-8') + 2 + mb_strlen($str_arr[$i], 'utf-8')) > 200)
					break;
				if (!empty($str))
					$str .= ', ';
				$str .= $str_arr[$i];
			}
			$new_product->meta_keywords = $str;
		}
		
		if (!empty($new_product->annotation)){
			$str = preg_replace("/[^a-zA-Zа-яА-Я0-9\s]/u", " ", strip_tags(html_entity_decode($new_product->annotation)));
			$str = preg_replace("/\s\s+/u", " ", $str);
			$str = trim($str, 'p ');
			$str_name = "";
			if (!empty($new_product->name))
				$str_name = strip_tags(html_entity_decode($new_product->name));
			$str = empty($str_name)?$str:(empty($str)?$str_name:$str_name.", ".$str);
			$str_arr = explode(' ', $str);
			$str = '';//mb_substr($str, 0, 200, 'utf-8');
			for($i = 0; $i < count($str_arr); $i++){
				if ((mb_strlen($str, 'utf-8') + 1 + mb_strlen($str_arr[$i], 'utf-8')) > 200)
					break;
				if (!empty($str))
					$str .= ' ';
				$str .= $str_arr[$i];
			}
			$new_product->meta_description = $str;
		}
		
		$new_product->source = 'market';
		
		$product_id = $this->products->add_product($new_product);
		$data['product_id'] = $product_id;
		
		/* === Add category link */
		
		$this->categories->add_product_category($data['product_id'], $data['cat']);
		
		$new_variant = new StdClass;
		$new_variant->product_id = $product_id;
		$new_variant->price = $data['price'];
		if ($this->settings->modules_ymimport_variant_stock != "∞")
			$new_variant->stock = $this->settings->modules_ymimport_variant_stock;
		
		$variant_id = $this->variants->add_variant($new_variant);
		
		/* === Product ID in sorting */
		$this->db->query("UPDATE __products SET position=id WHERE id=?", $data['product_id']);
		/* === Images */

		foreach ($product->imgs as $img_id => $img) {
			$this->image->add_internet_image('products', $product_id, $img->big);
		}
		
	}
	
	function load_page($url){
		$return_status = 0;		// 0 - ok,  1 - captcha
	
		try{
			$result = $this->Netfsurf->Request($url);
		}
		catch(ENet $e){
			return 2;
		}
		$f = $result->f;
		$content = stream_get_contents($f);
				
		if (strpos($result->url, 'captcha') !== FALSE){
			$captcha_src = Str::extract($content, '<img src="', '"');
			$captcha_hidden_key = Str::extract($content, 'value="', '"');
			$captcha_retpath = Str::extract($content, 'name="retpath" value="', '"');

			// Load the image
			$Q = $this->Netfsurf->Request($captcha_src);
			$captcha_image = stream_get_contents($Q->f);
			
			$this->design->assign('captcha_image', base64_encode($captcha_image));
			$this->design->assign('captcha_hidden_key', $captcha_hidden_key);
			$this->design->assign('captcha_retpath', $captcha_retpath);
			
			$return_status = 1;
		}
		
		return array('status' => $return_status, 'content' => $content);
	}
	
	function import($url, $spec_replace = NULL, $content = NULL) {
		sleep(mt_rand(1,5));
		
		if (isset($content))
			$product = $this->parse_model($content, 1);
		else{
			$page = $this->load_page($url);
			if ($page['status'] != 0){
				return $page['status'];
				break;
			}
			$product = $this->parse_model($page['content']);
		}
		
		$_SESSION['YMimport-dupecheck'][] = $url;

		$this->insert_product_to_db($product, $spec_replace);
			
		return 0;
	}
	
	function import_from_choose_list($arr, $content = NULL, $last_index = NULL){
		foreach ($arr as $index=>$product) {
		
			if (isset($content) && isset($last_index) && $index < $last_index)
				continue;
		
			if (isset($content) && isset($last_index) && $index == $last_index)
				$import_result = $this->import($product->url, $product->spec, $content);
			else
				$import_result = $this->import($product->url, $product->spec);
			if ($import_result == 1){
				$_SESSION['YMimport']['import_from'] = 'choose';
				$_SESSION['YMimport']['import_from_index'] = $index;
				return 1;
			}
			$_SESSION['YMimport']['expected']++;
			if ($import_result == 2){
				$_SESSION['YMimport']['exceptions']++;
				continue;
			}
			$_SESSION['YMimport']['successful']++;
		}
		return 0;
	}
	
	function import_from_model_list($arr, $content = NULL, $last_index = NULL){
		foreach ($arr as $index=>$url) {
		
			if (isset($content) && isset($last_index) && $index < $last_index)
				continue;
		
			if (isset($content) && isset($last_index) && $index == $last_index)
				$import_result = $this->import($url, NULL, $content);
			else
				$import_result = $this->import($url);
			if ($import_result == 1){
				$_SESSION['YMimport']['import_from'] = 'model';
				$_SESSION['YMimport']['import_from_index'] = $index;
				return 1;
			}
			$_SESSION['YMimport']['expected']++;
			if ($import_result == 2){
				$_SESSION['YMimport']['exceptions']++;
				continue;
			}
			$_SESSION['YMimport']['successful']++;
		}
		return 0;
	}
	
	function fetch() {
		if (!(isset($_SESSION['admin']) && $_SESSION['admin']=='admin'))
			header("Location: http://".$_SERVER['SERVER_NAME']."/admin/login/");
			
		$mode = '';
		$invalid_urls = array();
		if ($this->request->method('post')){
			$mode = $this->request->post('mode', 'string');
			
			switch($mode){
				//###########################################################################################################
				case "start_parse":
					// Параметры парсинга
					$this->settings->modules_ymimport_parse_price = intval($_POST['modules_ymimport_parse_price']);
					$this->settings->modules_ymimport_product_visible = intval($_POST['modules_ymimport_product_visible']);
					$this->settings->modules_ymimport_variant_stock = $_POST['modules_ymimport_variant_stock'];
										
					$_SESSION['YMimport'] = array(
						'list' => array(), 
						'product' => array(), 
						'category_id' => $this->request->post('category_id', 'integer')
					);
					
					if (!isset($_SESSION['YMimport-dupecheck']))
						$_SESSION['YMimport-dupecheck'] = array();
						
					// Создадим список ссылок для импорта
					$urls = $this->request->post('urls');
					foreach (explode("\n", $urls) as $url) {
						$url = trim($url);
						if (strlen($url) == 0)
							continue;
						/* Определим что за тип ссылки */
						$url_mode = null;
						if ($this->test_list_url($url))
							$url_mode = 'list';
						elseif ($this->test_model_url($url))
							$url_mode='model';
						else
							$invalid_urls[] = $url;
							
						// Добавим в список для импорта
						if (!is_null($url_mode))
							$_SESSION['YMimport'][$url_mode][] = $url;
					}
					
					if (count($invalid_urls) > 0){
						$mode = "invalid_urls";
						break;
					}
					
					// Пропарсим ссылки со списками товаров, чтобы получить ссылки на конкретные товары
					$_SESSION['YMimport']['exceptions'] = array();
					$_SESSION['YMimport']['choose'] = array();
					foreach ($_SESSION['YMimport']['list'] as $url) {
						$page = $this->load_page($url);
						
						/*echo "<pre>";
						print_r($page);
						echo "</pre>";
						die();*/
						
						if ($page['status'] == 1){
							$mode = "captcha_request";
							break;
						}
						if ($page['status'] == 2){
							$_SESSION['YMimport']['exceptions']++;
							continue;
						}
						
						$products = $this->parse_list($page['content']);
						foreach($products as $product){
							$product->id = count($_SESSION['YMimport']['choose']);
							$_SESSION['YMimport']['choose'][] = $product;
						}
					}
					
					if ($mode == "captcha_request")
						break;
					
					if (count($_SESSION['YMimport']['choose'])){
						$mode = "choose";
						break;
					}
					
					//Иначе парсим урлы конкретных товаров
					$_SESSION['YMimport']['successful'] = 0;
					$_SESSION['YMimport']['expected'] = 0;
					$_SESSION['YMimport']['exceptions'] = array();
					
					$this->M->connect();
					
					//Импорт товаров на которые была прямая ссылка
					if (!empty($_SESSION['YMimport']['model'])){
						$import_result = $this->import_from_model_list( $_SESSION['YMimport']['model'] );
						if ($import_result == 1){
							$mode = "captcha_request";
							break;
						}
					}
					
					$mode = "finish";
						
					break;
					
				//###########################################################################################################
				case "choose":
					$choose = $this->request->post('choose');
					foreach (array_keys($_SESSION['YMimport']['choose']) as $id)
						if (!in_array($id, $choose))
							unset($_SESSION['YMimport']['choose'][$id]);
					
					$_SESSION['YMimport']['successful'] = 0;
					$_SESSION['YMimport']['expected'] = 0;
					$_SESSION['YMimport']['exceptions'] = array();
					
					$this->M->connect();
					
					//Импорт товаров из выбранного списка
					if (!empty($_SESSION['YMimport']['choose'])){
						$import_result = $this->import_from_choose_list( $_SESSION['YMimport']['choose'] );
						if ($import_result == 1){
							$mode = "captcha_request";
							break;
						}
					}
					
					//Импорт товаров на которые была прямая ссылка
					if (!empty($_SESSION['YMimport']['model'])){
						$import_result = $this->import_from_model_list( $_SESSION['YMimport']['model'] );
						if ($import_result == 1){
							$mode = "captcha_request";
							break;
						}
					}
					
					$mode = "finish";
					
					break;
				//###########################################################################################################
				case "captcha_input":
					$captcha_hidden_key = $this->request->post('captcha_hidden_key');
					$captcha_retpath = $this->request->post('captcha_retpath');
					$captcha_text = $this->request->post('captcha_text');
					$Q = $this->Netfsurf->Request('http://market.yandex.ru/checkcaptcha', array(
						'key' => $captcha_hidden_key,
						'rep' => $captcha_text,
						'retpath' => $captcha_retpath,
					));
					$content = stream_get_contents($Q->f);
					
					$import_from = @$_SESSION['YMimport']['import_from'];
					$import_from_index = @$_SESSION['YMimport']['import_from_index'];
					
					unset($_SESSION['YMimport']['import_from']);
					unset($_SESSION['YMimport']['import_from_index']);
					
					if (!empty($import_from) && $import_from == "choose"){
						$this->M->connect();
					
						//Импорт товаров из выбранного списка
						if (!empty($_SESSION['YMimport']['choose'])){
							$import_result = $this->import_from_choose_list($_SESSION['YMimport']['choose'], $content, $import_from_index);
							if ($import_result == 1){
								$mode = "captcha_request";
								break;
							}
						}
						
						//Импорт товаров на которые была прямая ссылка
						if (!empty($_SESSION['YMimport']['model'])){
							$import_result = $this->import_from_model_list($_SESSION['YMimport']['model']);
							if ($import_result == 1){
								$mode = "captcha_request";
								break;
							}
						}
						
						$mode = "finish";
					}
					elseif (!empty($import_from) && $import_from == "model"){
						$this->M->connect();
					
						//Импорт товаров на которые была прямая ссылка
						$import_result = $this->import_from_model_list($_SESSION['YMimport']['model'], $content, $import_from_index);
						if ($import_result == 1){
							$mode = "captcha_request";
							break;
						}
						
						$mode = "finish";
					}
					else{
						$products = $this->parse_list($content);
						foreach($products as $product){
							$product->id = count($_SESSION['YMimport']['choose']);
							$_SESSION['YMimport']['choose'][] = $product;
						}
						$mode = "choose";
					}
					break;
			}
		}
		
		if (empty($mode))
			$mode = "default";
		
		$this->design->assign('mode', $mode);
		
		switch ($mode){
			case "default":
				$this->design->assign('Categories', $this->categories->get_categories_tree() );
				$this->design->assign('tmp_folder_perms', substr(sprintf('%o', fileperms('modules/ymimport/controller/tmp')), -3));
				break;
			case "invalid_urls":
				$this->design->assign('invalid_urls', $invalid_urls);
				break;
			case "choose":
				$this->design->assign('category_id', $_SESSION['YMimport']['category_id']);
				$this->design->assign('products', $_SESSION['YMimport']['choose']);
				break;
			case "finish":
				$this->design->assign('successful', $_SESSION['YMimport']['successful']);
				$this->design->assign('expected', $_SESSION['YMimport']['expected']);
				unset($_SESSION['YMimport']);
				break;
		}
		
		return $this->design->fetch('modules/ymimport/template/yandex-market-import.tpl');
	}
}