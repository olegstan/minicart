<?php

require_once('api/core/Minicart.php');

class CartControllerAdmin extends Minicart {
	private $param_url, $options;

	public function set_params($url = null, $options = null)
	{
		$this->param_url = $url;
		$this->options = $options;
	}

	function fetch() {
		if (!(isset($_SESSION['admin']) && $_SESSION['admin']=='admin'))
			header("Location: http://".$_SERVER['SERVER_NAME']."/admin/login/");

		if ($this->request->method('post'))
		{
			$this->settings->cart_fio_required = $this->request->post('cart_fio_required');
			$this->settings->cart_phone_required = $this->request->post('cart_phone_required');
			$this->settings->cart_email_required = $this->request->post('cart_email_required');
			$this->settings->cart_address_required = $this->request->post('cart_address_required');
			$this->settings->cart_right_message = $this->request->post('cart_right_message');
			$this->settings->cart_order_min_price = $this->request->post('cart_order_min_price');
			$this->settings->cart_fio_required = $this->request->post('cart_fio_required');

			$this->design->assign('message_success', 'saved');
		}
			
		return $this->design->fetch($this->design->getTemplateDir('admin').'cart.tpl');

		return false;
	}
}
?>