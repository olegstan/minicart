<?PHP

/**
 * Minicart CMS
 *
 * Этот класс использует шаблоны cart.tpl
 *
 */
 
require_once('controllers/GlobalController.php');

class CartController extends GlobalController
{
	private $param_url, $params_arr, $options;

	public function set_params($url = null, $options = null)
	{
		$this->param_url = urldecode(trim($url, '/'));
		$this->options = $options;
		
		$this->params_arr = array();
		foreach(explode("&", $this->param_url) as $p)
		{
			$x = explode("=", $p);
			$this->params_arr[$x[0]] = "";
			if (count($x)>1)
				$this->params_arr[$x[0]] = $x[1];
		}
	}

	function fetch()
	{
		$ajax = false;
		foreach($this->params_arr as $p=>$v)
		{
			switch ($p)
			{
				case "ajax":
					$ajax = true;
					unset($this->params_arr[$p]);
					break;
			}
		}
		
		if ($ajax)
		{
			$result = $this->design->fetch($this->design->getTemplateDir('frontend').'cart.tpl');
			header("Content-type: application/json; charset=UTF-8");
			header("Cache-Control: must-revalidate");
			header("Pragma: no-cache");
			header("Expires: -1");
			print json_encode($result);
			die();
		}
	
		if($this->page)
		{
			$this->design->assign('meta_title', $this->page->meta_title);
			$this->design->assign('meta_keywords', $this->page->meta_keywords);
			$this->design->assign('meta_description', $this->page->meta_description);
		}

		return $this->design->fetch($this->design->getTemplateDir('frontend').'cart.tpl');
	}
}