{* Title *}
{$meta_title='Корзина' scope=parent}

<form method=POST>

<div class="controlgroup">
	<a href="#" class="save">Сохранить</a>
	<a href="#" class="saveclose">Сохранить и закрыть</a>
	<a href="{$config->root_url}/modules/" class="cancel">Отменить</a>
	<div class="sep">
		<a href="#" class="help">Помощь</a>
	</div>
</div>


 <div class="row">
	<div class="col-md-8">
	
	     <legend>Корзина</legend>	

	     <div class="form-group">
             <label><strong>Какие поля обязательны при оформлении заказа:</strong></label>   
             </div>
             
              <div class="row form-group">
				<div class="col-md-4">  
                  ФИО получателя:   
            	</div>
                <div class="col-md-8"> 
                	<div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $settings->cart_fio_required}active{/if}">
                        <input type="radio" name="cart_fio_required" id="option1" value="1" {if $settings->cart_fio_required}checked{/if}/>  Обязательно для заполнения
                      </label>
                      <label class="btn btn-default4 off {if !$settings->cart_fio_required}active{/if}">
                        <input type="radio" name="cart_fio_required" id="option2" value="0" {if !$settings->cart_fio_required}checked{/if}/> Не обязательно
                      </label>
                    </div>
                </div>
         	 </div> 
              
             
             <div class="row form-group">
				<div class="col-md-4">  
                  Мобильный телефон:   
            	</div>
                <div class="col-md-8"> 
                	<div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $settings->cart_phone_required}active{/if}">
                        <input type="radio" name="cart_phone_required" id="option1" value="1" {if $settings->cart_fio_required}checked{/if}/>  Обязательно для заполнения
                      </label>
                      <label class="btn btn-default4 off {if !$settings->cart_phone_required}active{/if}">
                        <input type="radio" name="cart_phone_required" id="option2" value="0" {if !$settings->cart_fio_required}checked{/if}/> Не обязательно
                      </label>
                    </div>
                </div>
         	 </div> 
             
             
             <div class="row form-group">
				<div class="col-md-4">  
                  E-mail:   
            	</div>
                <div class="col-md-8"> 
                	<div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $settings->cart_email_required}active{/if}">
                        <input type="radio" name="cart_email_required" id="option1" value="1" {if $settings->cart_email_required}checked{/if}/>  Обязательно для заполнения
                      </label>
                      <label class="btn btn-default4 off {if !$settings->cart_email_required}active{/if}">
                        <input type="radio" name="cart_email_required" id="option2" value="0" {if !$settings->cart_email_required}checked{/if}/> Не обязательно
                      </label>
                    </div>
                </div>
         	 </div> 
             
             
             <div class="row form-group">
				<div class="col-md-4">  
                  Адрес получателя:   
            	</div>
                <div class="col-md-8"> 
                	<div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $settings->cart_address_required}active{/if}">
                        <input type="radio" name="cart_address_required" id="option1" value="1" {if $settings->cart_address_required}checked{/if}/>  Обязательно для заполнения
                      </label>
                      <label class="btn btn-default4 off {if !$settings->cart_address_required}active{/if}">
                        <input type="radio" name="cart_address_required" id="option2" value="0" {if !$settings->cart_address_required}checked{/if}/> Не обязательно
                      </label>
                    </div>
                    <p class="help-block">Если Ваш магазин использует самовывоз, то при выборе самовывоза поле "Адрес получателя" будет скрыто.</p>
                </div>
         	 </div>
             
        
        
        
        
        <hr/>
        
         <label>Сообщение справа</label> 
         <textarea class="form-control" rows="3" name="cart_right_message">{$settings->cart_right_message}</textarea>
		 <p class="help-block">Сообщение будет показывать в корзине справой стороны, можно разместить какую либо информацию о ньюансах доставки или рекламный баннер.</p>
         
			
		<div class="col-md-4">
			<legend>Настройка заказов</legend>
			<label>Минимальная сумма заказа</label>
			<div class="input-group">
				<input type="text" class="form-control" placeholder="" name="cart_order_min_price" value="{if $settings->cart_order_min_price}{$settings->cart_order_min_price}{else}0{/if}"/>
				<span class="input-group-addon"><i class="fa fa-rub"></i></span>
			</div> 
			<p class="help-block">Пользователю будет выведено собщение в корзине о минимальной сумме заказа.<br/> По умолчанию "0".</p>  
		</div>
	
		<input type=hidden name="session_id" value="{$smarty.session.id}">
	</div>
</div>
</form>

<script type="text/javascript">

	$(document).ready(function(){

		{if $message_success}
			info_box($('#notice'), 'Настройки сохранены!', '');
		{/if}

		{if $message_error}
			error_box($('#notice'), 'Ошибка при сохранении!', '')
		{/if}
	});

	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>