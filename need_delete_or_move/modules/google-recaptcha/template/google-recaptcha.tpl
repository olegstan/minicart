{* Title *}
{$meta_title='Каптча от Google reCAPTCHA' scope=parent}

<form method=POST>

<div class="controlgroup">		
	<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
</div>

<div class="row">
		<div class="col-md-8">		
			<legend>Настройки</legend>
            
            <div class="form-group">
					<label>Включить Google reCAPTCHA <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="На странице регистрации будет включена защита от спам-регистраций роботов"><i class="fa fa-info-circle"></i></a></label>
					<div class="btn-group noinline" data-toggle="buttons">
						<label class="btn btn-default4 on {if $settings->google_recaptcha_enabled}active{/if}">
							<input type="radio" name="google_recaptcha_enabled" value="1" {if $settings->google_recaptcha_enabled}checked{/if}>Да
						</label>
						<label class="btn btn-default4 off {if !$settings->google_recaptcha_enabled}active{/if}">
							<input type="radio" name="google_recaptcha_enabled" value="0" {if !$settings->google_recaptcha_enabled}active{/if}>Нет
						</label>
					</div>
				 </div>
            
            <div class="form-group">
            	<label>Ключ</label>
                <input type="text" class="form-control" name="google_recaptcha_key" value="{$settings->google_recaptcha_key}">
            </div>
            
            <div class="form-group">
            	<label>Секретный ключ</label>
                <input type="text" class="form-control" name="google_recaptcha_secret_key" value="{$settings->google_recaptcha_secret_key}">
            </div>
            
        </div> 
        
        <div class="col-md-4">		
			<legend>Инструкция</legend>
            
			<ol>
            	<li>Вам необходимо завести аккаунт в Google (если еще нет)</li>
            	<li>Перейти на сайт <a href="https://www.google.com/recaptcha" target="_blank">https://www.google.com/recaptcha</a></li>
                <li>В разделе "Регистрация сайта" введите название и домен(ы)</li>
                <li>Введите полученные ключи в настройки</li>                
            </ol>
            
        </div> 
        
</div>
     
</form>

<script type="text/javascript">
	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>