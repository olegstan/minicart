{* Title *}
{$meta_title='Яндекс.Деньги' scope=parent}

<form method=POST>

<div class="controlgroup">		
	<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
</div>

<div class="row">
		<div class="col-md-6">		
			<legend>Настройки</legend>
            <div class="row">
                <div class="col-md-8">	
                    <div class="form-group">
                        <label>Идентификатор Магазина (shopId)</label>
                        <input type="text" id="" class="form-control" name="ykassa_shop_id" value="{$settings->ykassa_shop_id}"/>
                     </div>
					 
					 <div class="form-group">
                        <label>Номер витрины Контрагента (scid)</label>
                        <input type="text" id="" class="form-control" name="ykassa_shop_scid" value="{$settings->ykassa_shop_scid}"/>
                     </div>
                     
                     <div class="form-group">
                        <label>Пароль магазина (shopPassword)</label>
                        <input type="text" id="" class="form-control" name="ykassa_shop_password" value="{$settings->ykassa_shop_password}"/>
                     </div>  
            	 </div>
             </div> 
             
            <label>Какой счет исползуется?  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="'Реальный счет' - используется для реальных платежей; 'Демо счет' - используется для демо-платежей и тестирования оплаты"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
				<label class="btn btn-default {if $settings->ykassa_account_type == "real"}active{/if}">
					<input type="radio" name="ykassa_account_type" value="real" {if $settings->ykassa_account_type == "real"}checked{/if}>Реальный счет
				</label>
				<label class="btn btn-default {if $settings->ykassa_account_type == "demo" || $settings->ykassa_account_type == ""}active{/if}">
					<input type="radio" name="ykassa_account_type" value="demo" {if $settings->ykassa_account_type == "demo" || $settings->ykassa_account_type == ""}checked{/if}>Демо счет
				</label>
			</div>
              
             
              <hr/>
            	
             
                    
        </div> 
        <div class="col-md-6">
        <legend>Инструкция по подключению Яндекс-кассы</legend>
				<ol>
                   	<li><p>Заполнить заявку на подключение <a href="https://kassa.yandex.ru/">https://kassa.yandex.ru/</a> - Яндекс предложит вам создать новый аккаунт на ваше юридическое лицо - создавайте, ведь ваш существующий аккаунт создан на физ. лицо, а чтобы принимать деньги нужен отдельный</p></li>
					<li><p>
						Для HTTP протокола укажите следующие адреса:</p>
						<p><strong>checkURL</strong> - {$config->secure_root_url}/order/check/</p>
						<p><strong>avisoURL</strong> - {$config->secure_root_url}/order/aviso/</p>
						<p>Установите флажок <strong>"Использовать страницы успеха и ошибки с динамическими адресами"</p></strong>
					</li>
                    <li><p>Модуль Яндекс-касса предоставляет следующие возможности оплаты:</p>
                    <ul>
						<li>Оплата из кошелька в Яндекс.Деньгах</li>
                        <li>Оплата с произвольной банковской карты</li>
                        <li>Платеж со счета мобильного телефона</li>
                        <li>Оплата наличными через кассы и терминалы</li>
                        <li>Оплата из кошелька в системе WebMoney</li>
                        <li>Оплата через Сбербанк: оплата по SMS или Сбербанк Онлайн</li>
                        <li>Оплата через мобильный терминал (mPOS)</li>
                        <li>Оплата через Альфа-Клик</li>
                        <li>Оплата через MasterPass</li>
                        <li>Оплата через Промсвязьбанк</li>
                    </ul>
					</li>
				</ol>
			
        
        </div>       
</div>
     
</form>

<script type="text/javascript">
	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>