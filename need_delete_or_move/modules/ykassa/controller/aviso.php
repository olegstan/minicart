<?php

require_once('api/core/Minicart.php');

class YandexKassaAvisoOrder extends Minicart {

	private $param_url, $params_arr, $options;

	public function set_params($url = null, $options = null){
		$this->options = $options;
		
		$url = urldecode(trim($url, '/'));
		$delim_pos = mb_strpos($url, '?', 0, 'utf-8');
		
		if ($delim_pos === false)
		{
			$this->param_url = $url;
			$this->params_arr = array();
		}
		else
		{
			$this->param_url = trim(mb_substr($url, 0, $delim_pos, 'utf-8'), '/');
			$url = mb_substr($url, $delim_pos+1, mb_strlen($url, 'utf-8')-($delim_pos+1), 'utf-8');
			$this->params_arr = array();
			foreach(explode("&", $url) as $p)
			{
				$x = explode("=", $p);
				$this->params_arr[$x[0]] = "";
				if (count($x)>1)
					$this->params_arr[$x[0]] = $x[1];
			}
		}
	}
		
	function fetch() {
		if ($this->request->method() != 'POST'){
			
			echo "no data";
			die();
		}
		
		$hash = md5($this->request->post('action') . ';' . $this->request->post('orderSumAmount') . ';' . $this->request->post('orderSumCurrencyPaycash') . ';' . $this->request->post('orderSumBankPaycash') . ';' . $this->settings->ykassa_shop_id . ';' . $this->request->post('invoiceId') . ';' . $this->request->post('customerNumber') . ';' . $this->settings->ykassa_shop_password);
		
		if (strtolower($hash) != strtolower($this->request->post('md5')))
			$code = 1;
		else
			$code = 0;
			
		$order = $this->orders->get_order_by_url(strval($this->request->post('customerNumber')));
		if (!$order)
			$code = 100;
		
		if ($code == 0){
			$this->orders->update_order($order->id, array('paid' => 1));
			
			$yp = new stdClass;
			$yp->order_id = $order->id;
			$yp->invoice_id = $this->request->post('invoiceId');
			$yp->sum = $this->request->post('orderSumAmount');
			$yp->payment_type = $this->request->post('paymentType');
			//���� � ����� �������
			$yp->payment_datetime = $this->request->post('paymentDatetime');
			$yp->payment_datetime = mb_substr($yp->payment_datetime, 0, 19, 'utf-8') . "Z";
			$dt = DateTime::createFromFormat('Y-m-d?G:i:s?', $yp->payment_datetime);
			$yp->payment_datetime = $dt->format('Y-m-d G:i:s');
			$this->ypayments->add_payment($yp);
			
			//���������� ������ ������������ � ������ �� ������ ������
			$this->notify_email->email_order_paid($order->id);
			$this->notify_email->email_order_paid_admin($order->id);
		}
		
		print '<?xml version="1.0" encoding="UTF-8"?>';
		if ($code == 100)
			print '<paymentAvisoResponse performedDatetime="'. $this->request->post('requestDatetime') .'" code="'.$code.'"'. ' invoiceId="'. $this->request->post('invoiceId') .'" shopId="'. $this->settings->ykassa_shop_id .'" message="����� �� ������"/>';
		else
			print '<paymentAvisoResponse performedDatetime="'. $this->request->post('requestDatetime') .'" code="'.$code.'"'. ' invoiceId="'. $this->request->post('invoiceId') .'" shopId="'. $this->settings->ykassa_shop_id .'"/>';
			
		die();
	}
}
