<?php

require_once('api/core/Minicart.php');

class YKassa_payment extends Minicart {

	public function payment_form($order_id)
	{
		$order = $this->orders->get_order(intval($order_id));
		
		$order_module =  $this->furl->get_module_by_name('OrderController');
		
		$payment_method = $this->payment->get_payment_method($order->payment_method_id);
		if ($payment_method)
		{
			$payment_method->images = $this->image->get_images('payment', $payment_method->id);
			$payment_method->image = reset($payment_method->images);
		}
		
		$this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
		$main_currency = $this->db->result();
		
		$form = '
			<div class="payment">
			<!-- Значения всех полей условны и приведены исключительно для примера --> 
				';
		if ($this->settings->ykassa_account_type == "real")
			$form .= '<form action="https://money.yandex.ru/eshop.xml" method="post">';
		else
			$form .= '<form action="https://demomoney.yandex.ru/eshop.xml" method="post">';
		
		$form .= '
					<!-- Обязательные поля --> 
					<input name="shopId" value="'.$this->settings->ykassa_shop_id.'" type="hidden"/> 
					<input name="scid" value="'.$this->settings->ykassa_shop_scid.'" type="hidden"/> 
					<input name="sum" value="'.round($order->total_price).'" type="hidden"> 
					<input name="customerNumber" value="'.$order->url.'" type="hidden"/> 
					  
					<!-- Необязательные поля --> 
					<!--<input name="paymentType" value="AC" type="hidden"/>-->
					<input name="shopSuccessURL" value="'.$this->config->root_url.$order_module->url.$order->url.'/" type="hidden"/>
					<input name="shopFailURL" value="'.$this->config->root_url.$order_module->url.$order->url.'/" type="hidden"/>
					<input name="orderNumber" value="'.$order->id.'" type="hidden"/> 
					<input name="cps_phone" value="'.$order->phone.'" type="hidden"/> 
					<input name="cps_email" value="'.$order->email.'" type="hidden"/> 
					
					<input type="hidden" name="paymentType" value="AC" checked="">
					<input type="submit" name="submit-button" value="Оплатить '.$this->currencies->convert($order->total_price).' '.$main_currency->sign_simple.'" class="btn btn-success">
				</form>
			</div>
		';
		
		/*$form = '<div class="payment">
<h1>Пополнение баланса</h1>
    <form method="POST" action="https://money.yandex.ru/quickpay/confirm.xml"> 
        <input type="hidden" name="receiver" value="'.$this->settings->ykassa_shop_id.'"> 
        <input type="hidden" name="formcomment" value="'.$this->settings->site_name.'"> 
        <input type="hidden" name="short-dest" value="Заказ №'.$order->id.'"> 
        <input type="hidden" name="label" value="'.$order->id.':::'.$order->url.'"> 
        <input type="hidden" name="quickpay-form" value="shop">
        <input type="hidden" name="targets" value="Оплата заказа"> 		
		<div class="form-group payment-summ">
        <div class="input-group">
        	<span class="input-group-addon">Сумма</span>
       		<input type="text" name="sum" value="'.$order->total_price.'" data-type="number" class="form-control" readonly>
            <span class="input-group-addon"><span class="b-rub">Р</span></span>
               
        </div>
        </div>
        <input type="hidden" name="comment" value="" >
        <input type="hidden" name="need-fio" value="false"> 
        <input type="hidden" name="need-email" value="false" > 
        <input type="hidden" name="need-phone" value="false"> 
        <input type="hidden" name="need-address" value="false"> 
        <div class="form-group payment-list">
			<div class="payment-method active">
				<div class="radio" data-id="7">
					<label>
						<input type="radio" name="paymentType" value="AC" checked="">
						<div class="payment-image"><img src="templates/default/img/cards.png"></div>
						<div class="payment-header">Банковской картой</div>
					</label>
				</div>
			</div>
        </div>
        
        <input type="submit" name="submit-button" value="Оплатить заказ" class="btn btn-success">
    </form>
</div>';*/
		
		return $form;
	}

}