<?php

require_once('api/core/Minicart.php');

class YandexKassaControllerAdmin extends Minicart {

	private $param_url, $params_arr, $options;

	public function set_params($url = null, $options = null){
		$this->options = $options;
		
		$url = urldecode(trim($url, '/'));
		$delim_pos = mb_strpos($url, '?', 0, 'utf-8');
		
		if ($delim_pos === false)
		{
			$this->param_url = $url;
			$this->params_arr = array();
		}
		else
		{
			$this->param_url = trim(mb_substr($url, 0, $delim_pos, 'utf-8'), '/');
			$url = mb_substr($url, $delim_pos+1, mb_strlen($url, 'utf-8')-($delim_pos+1), 'utf-8');
			$this->params_arr = array();
			foreach(explode("&", $url) as $p)
			{
				$x = explode("=", $p);
				$this->params_arr[$x[0]] = "";
				if (count($x)>1)
					$this->params_arr[$x[0]] = $x[1];
			}
		}
	}
		
	function fetch() {
		if (!(isset($_SESSION['admin']) && $_SESSION['admin']=='admin'))
			header("Location: http://".$_SERVER['SERVER_NAME']."/admin/login/");
		
		if ($this->request->method('post')){
			$this->settings->ykassa_shop_id = $this->request->post('ykassa_shop_id');
			$this->settings->ykassa_shop_scid = $this->request->post('ykassa_shop_scid');
			$this->settings->ykassa_shop_password = $this->request->post('ykassa_shop_password');
			$this->settings->ykassa_account_type = $this->request->post('ykassa_account_type');
		}

		return $this->design->fetch('modules/ykassa/template/yandex-kassa.tpl');
	}
}
