{* Title *}
{$meta_title='Яндекс.SMTP' scope=parent}

<form method=POST>

<div class="controlgroup">		
	<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
</div>

<div class="row">
		<div class="col-md-8">
			<legend>Настройки</legend>
            <div class="row">
                <div class="col-md-8">
                	<div class="form-group">
					<label>Включить отравку писем через smtp.yandex.ru  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если включить отправку все письма будут отправлятся по smtp-протоколу через сервера Яндекса (по умолчанию письма отправляются через phpmail()). Работает только с серверами Яндекса."><i class="fa fa-info-circle"></i></a></label>
					<div class="btn-group noinline" data-toggle="buttons">
						<label class="btn btn-default4 on {if $settings->yandexsmtp_enabled}active{/if}">
							<input type="radio" name="yandexsmtp_enabled" value="1" {if $settings->yandexsmtp_enabled}checked{/if}>Да
						</label>
						<label class="btn btn-default4 off {if !$settings->yandexsmtp_enabled}active{/if}">
							<input type="radio" name="yandexsmtp_enabled" value="0" {if !$settings->yandexsmtp_enabled}checked{/if}>Нет
						</label>
					</div>
				 </div>
                    <div class="form-group">
                        <label>Логин</label>
                        <input type="text" id="" class="form-control" name="yandexsmtp_login" value="{$settings->yandexsmtp_login}"/>
                     </div>
                     
                     <div class="form-group">
                        <label>Пароль</label>
                        <input type="password" id="" class="form-control" name="yandexsmtp_password" value="{$settings->yandexsmtp_password}"/>
                     </div>  
            	 </div>
            </div> 
        </div>
        
                
        <div class="col-md-4">
        <legend>Инструкция по подключению</legend>
        <p>Для подключения необходимо:</p>
        <ol>
        	<li>Завести почтовый ящик на <a href="http://mail.yandex.ru/" target="_blank">http://mail.yandex.ru</a> или использовать уже существующий</li>
            <li>Ввести логин и пароль от этого ящика</li>
         
        </ol>
        
        <p>Мы рекомендуем использовать сервис "Почта на домене" от Яндекса, настроить можно тут -<a href="http://pdd.yandex.ru" target="_blank">http://pdd.yandex.ru</a></p>
        </div>
</div>
     
</form>

<script type="text/javascript">
	$("form a.save").click(function(){
		$(this).closest('form').submit();
		return false;
	});
</script>