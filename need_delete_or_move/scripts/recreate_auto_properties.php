<?php

//включаем отображение только ошибок
error_reporting(E_ALL);
//Время выполнения скрипта неограничено
set_time_limit(0);
//Ставим часовой пояс
date_default_timezone_set('Europe/Moscow');

session_start();
chdir("/var/www/solum/data/www/solum.ru/");
require_once('api/core/Minicart.php');
$minicart = new Minicart();
$settings = $minicart->settings;

$minicart->tags->recreate_auto_properties();

?>