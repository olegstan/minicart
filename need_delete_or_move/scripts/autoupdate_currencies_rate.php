<?php

//включаем отображение только ошибок
error_reporting(E_ALL);
//Время выполнения скрипта неограничено
set_time_limit(0);
//Ставим часовой пояс
date_default_timezone_set('Europe/Moscow');

session_start();
chdir("..");
require_once('api/core/Minicart.php');
$minicart = new Minicart();
$settings = $minicart->settings;

$currencies = $minicart->currencies->get_currencies();
foreach($currencies as $c)
	if ($c->auto_update && $c->code)
	{
		$new_rate_to = $minicart->currencies->update_currency_rate($c->code);
		$minicart->currencies->update_currency($c->id, array('rate_to'=>$new_rate_to));
	}

?>