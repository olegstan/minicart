DELETE FROM mc_brands;
DELETE FROM mc_callbacks;
DELETE FROM mc_categories;
DELETE FROM mc_categories_search;
DELETE FROM mc_images WHERE module_id=2;
DELETE FROM mc_orders;
DELETE FROM mc_purchases;
DELETE FROM mc_products;
DELETE FROM mc_products_categories;
DELETE FROM mc_products_search;
DELETE FROM mc_ratings;
DELETE FROM mc_related_products;
DELETE FROM mc_search_history;
DELETE FROM mc_tags;
DELETE FROM mc_tags_alternative;
DELETE FROM mc_tags_categories;
DELETE FROM mc_tags_products;
DELETE FROM mc_tags_groups WHERE is_auto=0;
DELETE FROM mc_tags_sets;
DELETE FROM mc_tags_sets_tags;
DELETE FROM mc_variants;
DELETE FROM mc_attachments;
DELETE FROM mc_ymoney_payments;