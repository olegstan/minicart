<?php

//�������� ����������� ������ ������
error_reporting(E_ALL);
//����� ���������� ������� ������������
set_time_limit(0);
//������ ������� ����
date_default_timezone_set('Europe/Moscow');

function trimPhone($phone)
{
	$phone = preg_replace("/[\D]/","",$phone);
	if (mb_strlen($phone, 'utf-8') == 10)
		$phone = "7".$phone;
	if ($phone[0] == "8")
		$phone[0] = "7";
	return $phone;
}

$con_s = mysql_connect("localhost","lifeknife","7u5YMwG9") or die('not connect to source sql');
$db_s = mysql_select_db("lifeknife", $con_s) or die('not select source database');
mysql_query("SET NAMES utf8",$con_s);
//$path_to_images = "http://opini.ru/files/originals/";
$path_to_images = "/var/www/mini4/data/www/mini4.ru/files/originals/";

session_start();
chdir("..");
require_once('api/core/Minicart.php');
$minicart = new Minicart();
$settings = $minicart->settings;

#############################################
## MAPPING ARRAYS
##   key - source_id, value - destination_id
#############################################
$brands_map = array();
$categories_map = array();
$products_map = array();
$features_map = array();
$orders_map = array();
#############################################
## END MAPPING
#############################################

#############################################
## BRANDS
$query = "SELECT * FROM brands ORDER BY brand_id";
$res = mysql_query($query, $con_s);
while ($brand_row = mysql_fetch_object($res))
{
	$tmp_brand = new StdClass;
	$tmp_brand->name = $brand_row->name;
	$tmp_brand->meta_title = $brand_row->meta_title;
	$tmp_brand->meta_keywords = $brand_row->meta_keywords;
	$tmp_brand->meta_description = $brand_row->meta_description;
	$tmp_brand->description = $brand_row->description;
	$brands_map[$brand_row->brand_id] = $minicart->brands->add_brand($tmp_brand);
}


#############################################
## CATEGORIES
$query = "SELECT * FROM categories ORDER BY parent";
$res = mysql_query($query, $con_s);
while ($category_row = mysql_fetch_object($res))
{
	$tmp_category = new StdClass;
	$tmp_category->parent_id = $category_row->parent;
	$tmp_category->name = $category_row->name;
	$tmp_category->meta_title = $category_row->meta_title;
	$tmp_category->meta_keywords = $category_row->meta_keywords;
	$tmp_category->meta_description = $category_row->meta_description;
	$tmp_category->description = $category_row->description;
	$tmp_category->position = $category_row->order_num;
	$tmp_category->visible = $category_row->enabled;
	$categories_map[$category_row->category_id] = $minicart->categories->add_category($tmp_category);
	
	//Copy category image
	if (!empty($category_row->image) && file_exists('/var/www/lifeknife/data/www/life-knife.ru/files/categories/'.$category_row->image))
		$minicart->image->add_internet_image('categories', $categories_map[$category_row->category_id], '/var/www/lifeknife/data/www/life-knife.ru/files/categories/'.$category_row->image);
}

$minicart->db->query("SELECT * FROM __categories ORDER BY id");
$categories = $minicart->db->results();
foreach($categories as $c)
	if ($c->parent_id > 0)
	{
		$new_parent_id = 0;
		if (array_key_exists($c->parent_id, $categories_map))
			$new_parent_id = $categories_map[$c->parent_id];
		$minicart->categories->update_category($c->id, array('parent_id'=>$new_parent_id));
	}

#############################################
## PRODUCTS
$query = "SELECT * FROM products ORDER BY product_id";
$res = mysql_query($query, $con_s);
while ($product_row = mysql_fetch_object($res))
{
	$tmp_product = new StdClass;
	if ($product_row->brand_id > 0 && array_key_exists($product_row->brand_id, $brands_map))
		$tmp_product->brand_id = $brands_map[$product_row->brand_id];
	$tmp_product->name = $product_row->model;
	$tmp_product->annotation = $product_row->description;
	$tmp_product->body = $product_row->body;
	$tmp_product->visible = $product_row->enabled;
	$tmp_product->position = $product_row->order_num;
	$tmp_product->meta_title = $product_row->meta_title;
	$tmp_product->meta_keywords = $product_row->meta_keywords;
	$tmp_product->meta_description = $product_row->meta_description;
	$products_map[$product_row->product_id] = $minicart->products->add_product($tmp_product);
	
	//Copy product image
	if (!empty($product_row->large_image) && file_exists('/var/www/lifeknife/data/www/life-knife.ru/files/products/'.$product_row->large_image))
		$minicart->image->add_internet_image('products', $products_map[$product_row->product_id], '/var/www/lifeknife/data/www/life-knife.ru/files/products/'.$product_row->large_image);
	
	//Copy other product images
	$query = "SELECT * FROM products_fotos WHERE product_id=".$product_row->product_id;
	$res2 = mysql_query($query, $con_s);
	while ($image_row = mysql_fetch_object($res2))
	{
		$minicart->image->add_internet_image('products', $products_map[$product_row->product_id], '/var/www/lifeknife/data/www/life-knife.ru/files/products/'.$image_row->filename);
	}
	
	if (array_key_exists($product_row->category_id, $categories_map))
		$minicart->categories->add_product_category($products_map[$product_row->product_id], $categories_map[$product_row->category_id], 0);
}

#############################################
## PRODUCTS_CATEGORIES
$query = "SELECT * FROM products_categories ORDER BY product_id";
$res = mysql_query($query, $con_s);
$position = 1;
while ($prodcat_row = mysql_fetch_object($res))
{
	if (!array_key_exists($prodcat_row->product_id, $products_map) || !array_key_exists($prodcat_row->category_id, $categories_map))
		continue;
	$minicart->categories->add_product_category($products_map[$prodcat_row->product_id], $categories_map[$prodcat_row->category_id], $position);
	$position++;
}

#############################################
## VARIANTS
$query = "SELECT * FROM products_variants ORDER BY product_id, variant_id";
$res = mysql_query($query, $con_s);
while ($variant_row = mysql_fetch_object($res))
{
	if (!array_key_exists($variant_row->product_id, $products_map))
		continue;
	$tmp_variant = new StdClass;
	$tmp_variant->product_id = $products_map[$variant_row->product_id];
	$tmp_variant->sku = $variant_row->sku;
	$tmp_variant->name = $variant_row->name;
	$tmp_variant->price = $variant_row->price;
	if (isset($variant_row->stock))
		$tmp_variant->stock = $variant_row->stock;
	$tmp_variant->position = $variant_row->position;
	$variants_map[$variant_row->variant_id] = $minicart->variants->add_variant($tmp_variant);
}

#############################################
## ORDERS
$query = "SELECT * FROM orders ORDER BY order_id";
$res = mysql_query($query, $con_s);
while ($order_row = mysql_fetch_object($res))
{
	$tmp_order = new StdClass;
	$tmp_order->url = $order_row->code;
	if ($order_row->status == 0)
		$tmp_order->status_id = 1;
	if ($order_row->status == 1)
		$tmp_order->status_id = 2;
	if ($order_row->status == 2)
		$tmp_order->status_id = 9;
	$tmp_order->closed = $order_row->written_off;
	$tmp_order->date = $order_row->date;
	$tmp_order->name = $order_row->name;
	$tmp_order->email = $order_row->email;
	
	$phone = trimPhone($order_row->phone);
	if (mb_strlen($phone, 'utf-8') == 11)
	{
		$tmp_order->phone_code = mb_substr($phone, 1, 3, 'utf-8');
		$tmp_order->phone = mb_substr($phone, 4, 7, 'utf-8');
	}
	$tmp_order->address = $order_row->address;
	$tmp_order->comment = $order_row->comment;
	$tmp_order->note = '';
	if (!empty($order_row->delivery_method_id))
		$tmp_order->delivery_id = $order_row->delivery_method_id;
	$tmp_order->delivery_price = $order_row->delivery_price;
	if (!empty($order_row->payment_method_id))
		$tmp_order->payment_method_id = $order_row->payment_method_id;
	$tmp_order->paid = $order_row->payment_status;
	$tmp_order->payment_date = $order_row->payment_date;
	$tmp_order->ip = $order_row->ip;
	
	$orders_map[$order_row->order_id] = $minicart->orders->add_order($tmp_order);
}

#############################################
## ORDERS PRODUCTS
$query = "SELECT * FROM orders_products ORDER BY order_id, product_id, variant_id";
$res = mysql_query($query, $con_s);
while ($op_row = mysql_fetch_object($res))
{
	if (!array_key_exists($op_row->order_id, $orders_map))
		continue;
	
	$tmp_purchase = new stdClass;
	$tmp_purchase->order_id = $orders_map[$op_row->order_id];
	if (array_key_exists($op_row->product_id, $products_map))
		$tmp_purchase->product_id = $products_map[$op_row->product_id];
	if (array_key_exists($op_row->variant_id, $variants_map))
		$tmp_purchase->variant_id = $variants_map[$op_row->variant_id];
	$tmp_purchase->product_name = $op_row->product_name;
	$tmp_purchase->variant_name = $op_row->variant_name;
	$tmp_purchase->price = $op_row->price;
	$tmp_purchase->price_for_mul = $op_row->price;
	$tmp_purchase->amount = $op_row->quantity;
	$tmp_purchase->sku = '';
	if (!empty($tmp_purchase->variant_id))
	{
		$tmp_variant = $minicart->variants->get_variant($tmp_purchase->variant_id);
		if (!empty($tmp_variant))
			$tmp_purchase->sku = $tmp_variant->sku;
	}
	
	$minicart->orders->add_purchase($tmp_purchase);
}

#############################################
## FEATURES	- ��������� properties -> mc_tags_groups
$query = "SELECT * FROM properties ORDER BY property_id";
$res = mysql_query($query, $con_s);
while ($feature_row = mysql_fetch_object($res))
{
	$tmp_feature = new StdClass;
	$tmp_feature->name = $feature_row->name;
	$features_map[$feature_row->property_id] = $minicart->tags->add_taggroup($tmp_feature);
}

#############################################
## OPTIONS	-	��������� s_options -> mc_tags & mc_tags_products
$query = "SELECT * FROM properties_values ORDER BY product_id";
$res = mysql_query($query, $con_s);
while ($option_row = mysql_fetch_object($res))
{
	if (!array_key_exists($option_row->product_id, $products_map) || !array_key_exists($option_row->property_id, $features_map))
		continue;
	
	if ($tag = $minicart->tags->get_tags(array('group_id'=>$features_map[$option_row->property_id],'name'=>$option_row->value)))
		$tag_id = intval(reset($tag)->id);
	else
	{
		$tag = new StdClass;
		$tag->group_id = $features_map[$option_row->property_id];
		$tag->name = $option_row->value;
		$tag->enabled = 1;
		$tag->id = $minicart->tags->add_tag($tag);
		$tag_id = $tag->id;
	}
	$minicart->tags->add_product_tag($products_map[$option_row->product_id], $tag_id);
}

mysql_close($con_s);


echo "END";

?>