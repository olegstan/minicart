<?php

#########################################
#### IMPORT SETTINGS
#########################################
$settings_copy_properties = true;
$settings_copy_attachments = true;
$settings_copy_orders = true;

function trimPhone($phone)
{
	$phone = preg_replace("/[\D]/","",$phone);
	if (mb_strlen($phone, 'utf-8') == 10)
		$phone = "7".$phone;
	if ($phone[0] == "8")
		$phone[0] = "7";
	return $phone;
}

//включаем отображение только ошибок
error_reporting(E_ALL);
//Время выполнения скрипта неограничено
set_time_limit(0);
//Ставим часовой пояс
date_default_timezone_set('Europe/Moscow');

$con_s = mysql_connect("localhost","opini","opini") or die('not connect to source sql');
$db_s = mysql_select_db("opini", $con_s) or die('not select source database');
mysql_query("SET NAMES utf8",$con_s);
//$path_to_images = "http://opini.ru/files/originals/";
$path_to_images = "/var/www/opini/data/www/opini.ru/files/originals/";

session_start();
chdir("..");
require_once('api/core/Minicart.php');
$minicart = new Minicart();
$settings = $minicart->settings;

#############################################
## MAPPING ARRAYS
##   key - source_id, value - destination_id
#############################################
$brands_map = array();
$categories_map = array();
$products_map = array();
$features_map = array();
$orders_map = array();
#############################################
## END MAPPING
#############################################

#############################################
## BRANDS
$query = "SELECT * FROM s_brands ORDER BY id";
$res = mysql_query($query, $con_s);
while ($brand_row = mysql_fetch_object($res))
{
	$tmp_brand = new StdClass;
	$tmp_brand->name = $brand_row->name;
	$tmp_brand->meta_title = $brand_row->meta_title;
	$tmp_brand->meta_keywords = $brand_row->meta_keywords;
	$tmp_brand->meta_description = $brand_row->meta_description;
	$tmp_brand->description = $brand_row->description;
	$brands_map[$brand_row->id] = $minicart->brands->add_brand($tmp_brand);
}

#############################################
## CATEGORIES
$query = "SELECT * FROM s_categories ORDER BY parent_id";
$res = mysql_query($query, $con_s);
while ($category_row = mysql_fetch_object($res))
{
	$tmp_category = new StdClass;
	$tmp_category->parent_id = $category_row->parent_id;//($category_row->parent_id>0) ? (array_key_exists($category_row->id, $categories_map) ? $categories_map[$category_row->id] : 0) : 0;
	$tmp_category->name = $category_row->name;
	$tmp_category->meta_title = $category_row->meta_title;
	$tmp_category->meta_keywords = $category_row->meta_keywords;
	$tmp_category->meta_description = $category_row->meta_description;
	$tmp_category->description = $category_row->description;
	$tmp_category->position = $category_row->position;
	$tmp_category->visible = $category_row->visible;
	$categories_map[$category_row->id] = $minicart->categories->add_category($tmp_category);
}

$minicart->db->query("SELECT * FROM __categories ORDER BY id");
$categories = $minicart->db->results();
foreach($categories as $c)
	if ($c->parent_id > 0)
	{
		$new_parent_id = 0;
		if (array_key_exists($c->parent_id, $categories_map))
			$new_parent_id = $categories_map[$c->parent_id];
		$minicart->categories->update_category($c->id, array('parent_id'=>$new_parent_id));
	}

#############################################
## PRODUCTS
$query = "SELECT * FROM s_products ORDER BY id";
$res = mysql_query($query, $con_s);
while ($product_row = mysql_fetch_object($res))
{
	$tmp_product = new StdClass;
	if ($product_row->brand_id > 0 && array_key_exists($product_row->brand_id, $brands_map))
		$tmp_product->brand_id = $brands_map[$product_row->brand_id];
	$tmp_product->name = $product_row->name;
	$tmp_product->annotation = $product_row->annotation;
	$tmp_product->body = $product_row->body;
	$tmp_product->visible = $product_row->visible;
	$tmp_product->position = $product_row->position;
	$tmp_product->meta_title = $product_row->meta_title;
	$tmp_product->meta_keywords = $product_row->meta_keywords;
	$tmp_product->meta_description = $product_row->meta_description;
	$products_map[$product_row->id] = $minicart->products->add_product($tmp_product);
}

#############################################
## VARIANTS
$query = "SELECT * FROM s_variants ORDER BY product_id, id";
$res = mysql_query($query, $con_s);
while ($variant_row = mysql_fetch_object($res))
{
	if (!array_key_exists($variant_row->product_id, $products_map))
		continue;
	$tmp_variant = new StdClass;
	$tmp_variant->product_id = $products_map[$variant_row->product_id];
	$tmp_variant->sku = $variant_row->sku;
	$tmp_variant->name = $variant_row->name;
	$tmp_variant->price = $variant_row->price;
	if (isset($variant_row->compare_price))
		$tmp_variant->price_old = $variant_row->compare_price;
	if (isset($variant_row->stock))
		$tmp_variant->stock = $variant_row->stock;
	$tmp_variant->position = $variant_row->position;
	$variants_map[$variant_row->id] = $minicart->variants->add_variant($tmp_variant);
}

#############################################
## PRODUCTS_CATEGORIES
$query = "SELECT * FROM s_products_categories ORDER BY product_id, position";
$res = mysql_query($query, $con_s);
while ($prodcat_row = mysql_fetch_object($res))
{
	if (!array_key_exists($prodcat_row->product_id, $products_map) || !array_key_exists($prodcat_row->category_id, $categories_map))
		continue;
	$minicart->categories->add_product_category($products_map[$prodcat_row->product_id], $categories_map[$prodcat_row->category_id], $prodcat_row->position);
}

#############################################
## IMAGES
$query = "SELECT * FROM s_images ORDER BY product_id, position";
$res = mysql_query($query, $con_s);
while ($image_row = mysql_fetch_object($res))
{
	if (!array_key_exists($image_row->product_id, $products_map))
		continue;
	$image_url = $path_to_images . $image_row->filename;
	
	$product = $minicart->products->get_product($products_map[$image_row->product_id]);
	
	$minicart->image->add_internet_image('products', $product->id, $minicart->furl->generate_url($product->name), $image_url);
}

if ($settings_copy_attachments)
{
	#############################################
	## FILES
	$query = "SELECT * FROM s_files ORDER BY product_id, position";
	$res = mysql_query($query, $con_s);
	while ($file_row = mysql_fetch_object($res))
	{
		if (!array_key_exists($file_row->product_id, $products_map))
			continue;
		$file_url = $path_to_images . $file_row->filename;
		
		$product = $minicart->products->get_product($products_map[$file_row->product_id]);
		
		$minicart->attachments->add_internet_attachment_with_username('products', $product->id, $minicart->furl->generate_url($product->name), $file_url, $file_row->name);
	}
}

if ($settings_copy_properties)
{
	#############################################
	## FEATURES	- ПЕРЕНОСИМ s_featues -> mc_tags_groups
	$query = "SELECT * FROM s_features WHERE in_filter=1 ORDER BY id";
	$res = mysql_query($query, $con_s);
	while ($feature_row = mysql_fetch_object($res))
	{
		$tmp_feature = new StdClass;
		$tmp_feature->name = $feature_row->name;
		$features_map[$feature_row->id] = $minicart->tags->add_taggroup($tmp_feature);
	}


	#############################################
	## OPTIONS	-	Переносим s_options -> mc_tags & mc_tags_products
	$query = "SELECT * FROM s_options ORDER BY product_id";
	$res = mysql_query($query, $con_s);
	while ($option_row = mysql_fetch_object($res))
	{
		if (!array_key_exists($option_row->product_id, $products_map) || !array_key_exists($option_row->feature_id, $features_map))
			continue;
		
		if ($tag = $minicart->tags->get_tags(array('group_id'=>$features_map[$option_row->feature_id],'name'=>$option_row->value)))
			$tag_id = intval(reset($tag)->id);
		else
		{
			$tag = new StdClass;
			$tag->group_id = $features_map[$option_row->feature_id];
			$tag->name = $option_row->value;
			$tag->enabled = 1;
			$tag->id = $minicart->tags->add_tag($tag);
			$tag_id = $tag->id;
		}
		$minicart->tags->add_product_tag($products_map[$option_row->product_id], $tag_id);
	}

	#############################################
	## CATEGORIES_TAGS	-	Переносим s_categories_features -> s_tags_sets & s_tags_sets_tags
	$categories_features = array(); //key - category_id, value = array of feature
	$query = "SELECT * FROM s_categories_features ORDER BY category_id, feature_id";
	$res = mysql_query($query, $con_s);
	while ($link_row = mysql_fetch_object($res))
	{
		if (!array_key_exists($link_row->category_id, $categories_map) || !array_key_exists($link_row->feature_id, $features_map))
			continue;
		if (!array_key_exists($categories_map[$link_row->category_id], $categories_features))
			$categories_features[$categories_map[$link_row->category_id]] = array();
		$categories_features[$categories_map[$link_row->category_id]][] = $features_map[$link_row->feature_id];
	}

	foreach($categories_features as $category_id=>$features_array)
	{
		$tmp_cat = $minicart->categories->get_category($category_id);
		if (!$tmp_cat)
			continue;
		$tag_set = new StdClass;
		$tag_set->name = "Фильтр для " . $tmp_cat->name;
		$tag_set->visible = 1;
		$tag_set->id = $minicart->tags->add_tags_set($tag_set);
		
		$minicart->categories->update_category($category_id, array('set_id' => $tag_set->id));
		foreach($features_array as $position=>$feature_id)
			$minicart->tags->add_tags_set_tag(array('set_id'=>$tag_set->id, 'tag_id'=>$feature_id, 'in_filter'=>1, 'position'=>$position, 'default_expand'=>0));
	}
}

if ($settings_copy_orders)
{
		#############################################
	## ORDERS
	$query = "SELECT * FROM s_orders ORDER BY id";
	$res = mysql_query($query, $con_s);
	while ($order_row = mysql_fetch_object($res))
	{
		$tmp_order = new StdClass;
		$tmp_order->url = $order_row->url;
		if ($order_row->status == 0)
			$tmp_order->status_id = 1;
		if ($order_row->status == 1)
			$tmp_order->status_id = 2;
		if ($order_row->status == 2)
			$tmp_order->status_id = 9;
		if ($order_row->status == 3)
			$tmp_order->status_id = 10;
		$tmp_order->closed = $order_row->closed;
		$tmp_order->date = $order_row->date;
		$tmp_order->name = $order_row->name;
		$tmp_order->email = $order_row->email;
		
		$phone = trimPhone($order_row->phone);
		if (mb_strlen($phone, 'utf-8') == 11)
		{
			$tmp_order->phone_code = mb_substr($phone, 1, 3, 'utf-8');
			$tmp_order->phone = mb_substr($phone, 4, 7, 'utf-8');
		}
		$tmp_order->address = $order_row->address;
		$tmp_order->comment = $order_row->comment;
		$tmp_order->note = '';
		if (!empty($order_row->delivery_id))
			$tmp_order->delivery_id = $order_row->delivery_id;
		$tmp_order->delivery_price = $order_row->delivery_price;
		if (!empty($order_row->payment_method_id))
			$tmp_order->payment_method_id = $order_row->payment_method_id;
		$tmp_order->paid = $order_row->paid;
		$tmp_order->payment_date = $order_row->payment_date;
		$tmp_order->ip = $order_row->ip;
		
		$orders_map[$order_row->id] = $minicart->orders->add_order($tmp_order);
	}

	#############################################
	## ORDERS PRODUCTS
	$query = "SELECT * FROM s_purchases ORDER BY order_id, product_id, variant_id";
	$res = mysql_query($query, $con_s);
	while ($op_row = mysql_fetch_object($res))
	{
		if (!array_key_exists($op_row->order_id, $orders_map))
			continue;
		
		$tmp_purchase = new stdClass;
		$tmp_purchase->order_id = $orders_map[$op_row->order_id];
		if (array_key_exists($op_row->product_id, $products_map))
			$tmp_purchase->product_id = $products_map[$op_row->product_id];
		if (array_key_exists($op_row->variant_id, $variants_map))
			$tmp_purchase->variant_id = $variants_map[$op_row->variant_id];
		$tmp_purchase->product_name = $op_row->product_name;
		$tmp_purchase->variant_name = $op_row->variant_name;
		$tmp_purchase->price = $op_row->price;
		$tmp_purchase->price_for_mul = $op_row->price;
		$tmp_purchase->amount = $op_row->amount;
		$tmp_purchase->sku = $op_row->sku;
		/*if (!empty($tmp_purchase->variant_id))
		{
			$tmp_variant = $minicart->variants->get_variant($tmp_purchase->variant_id);
			if (!empty($tmp_variant))
				$tmp_purchase->sku = $tmp_variant->sku;
		}*/
		
		$minicart->orders->add_purchase($tmp_purchase);
	}
}

mysql_close($con_s);


echo "END";

?>