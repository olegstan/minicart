<?php
namespace core;

use Iterator;
use app\models\image\Image;
use app\models\product\Product;

class Collection implements Iterator
{
    private $position = 0;
    private $items = [];
    private $map = [];
    private $tree = [];

    public function __construct()
    {
        $this->position = 0;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function current()
    {
        return $this->items[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        ++$this->position;
    }

    public function valid()
    {
        return isset($this->items[$this->position]);
    }

    public function appendToArray($element, $key = null)
    {
        if(isset($key)){
            $this->items[$key] = $element;
        }else{
            $this->items[] = $element;
        }
    }

    public function appendOne($element)
    {
        $this->items = $element;
    }

    public function getId()
    {
        if (is_array($this->items)) {
            $result_ids = [];
            foreach ($this->items as $k => $v) {
                $result_ids[] = $v->id;
            }
            $this->result_ids = $result_ids;
            return $this->result_ids;
        } else {
            return;
        }
    }

    public function getField($field)
    {
        if (is_array($this->items)) {
            $fields = [];
            foreach ($this->items as $k => $v) {
                $fields[] = $v->$field;
            }
            $this->result_fields = $fields;
            return $this->result_fields;
        } else {
            return;
        }
    }

    /**
     * @return array|null|Image|Product
     */

    public function getResult()
    {
        if(!empty($this->items)){
            return $this->items;
        }else{
            return null;
        }
    }

    public function toMap()
    {
        foreach($this->items as $item){
            $this->map[$item->id] = $item;
        }
        return $this;
    }

    public function toTree($field)
    {
        $this->toMap();

        foreach($this->items as $item){
            if($item->$field == 0){
                $this->tree[] = $item;
            }else{
                $this->map[$item->$field]->nodes[] = $item;
            }
        }
        return $this;
    }

    public function toArray()
    {
        $array = [];
        foreach($this->items as $k => $item){
            $array[$k] = (array) $item;
        }
        return $array;
    }

    public function toJson()
    {

    }

    public function getTree()
    {
        return $this->tree;
    }

    public function getMap()
    {
        return $this->map;
    }
}