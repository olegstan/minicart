<?php
namespace core;

use core\Core;
use app\RouteAccess;
use app\controllers\FilesController;
use app\controllers\ErrorController;
use core\helper\Response;
use \Exception;

class FrontController
{
    public $response;

    private $app_start = 0;
    private $app_end = 0;
    private $app_seconds;
    private $app_bytes;

    public function getCore()
    {
        return Core::getCore();
    }

    public function init()
    {
        $core = $this->getCore();

        //проверяем роут
        $core->request->routes = (new RouteAccess())->check();

        if($core->request->is_files){

            $action = $core->request->routes[1];
            $file_controller = new FilesController();

            if(method_exists($file_controller, $action)){
                die($file_controller->$action());
            }else{
                die((new ErrorController())->{'404'}('В контроллере не существует данного метода ' . $file_controller->$action()));
            }
        }

        if(!$core->request->is_ajax){
            $start_time_script = microtime(true);
        }

        // контроллер и действие по умолчанию в ядре

        //routing

        $i = 0;
        //установка локали

        $set_locale_res = setlocale(LC_ALL, 'ru_RU.UTF8');

        if($set_locale_res == false){

//            php_locale_collate = ru_RU;
//            php_locale_ctype = ru_RU;
//            php_locale_monetary = ru_RU;
//            php_locale_numeric = ru_RU;
//            php_locale_time = ru_RU;
            //echo 1;
            //echo '<!-- локаль не установлена -->';
        }else{

        }

        if(!empty($core->request->routes[$i])){

            $core->controller_name = $core->request->routes[$i];
            $core->controller = $core->controller_name;
            $core->controller_name = ucfirst($core->controller_name);

            // смотрим первую часть урла
            // добавляем префиксы
            // подцепляем файл с классом контроллера
            // для ajax свои контроллеры

            if ($core->request->is_admin) {
                $core->controller_name = $core->controller_name . 'Controller';
                $controller_path = MINICART . '/app/system/controllers/';
                $controller_namespace = 'app\\system\\controllers\\';
            } else {
                $core->controller_name = $core->controller_name . 'Controller';
                $controller_path = MINICART . '/app/controllers/';
                $controller_namespace = 'app\\controllers\\';
            }

            if($core->request->is_ajax){
                $controller_path .= 'ajax/';
                $controller_namespace .= 'ajax\\';
            }

            $controller = $controller_namespace . $core->controller_name;
        }else{
            $core->controller = 'index';
            $core->controller_name = 'IndexController';
            if($core->request->is_admin){
                $controller_path = MINICART . '/app/system/controllers/';
                $controller = 'app\\system\\controllers\\' . $core->controller_name;
            }else{
                $controller_path = MINICART . '/app/controllers/';
                $controller = 'app\\controllers\\' . $core->controller_name;
            }
        }

        $controller_path .= $core->controller_name . '.php';

        if(file_exists($controller_path)){
            $controller = new $controller;
        }else{
            return $this->response((new ErrorController())->{'404'}('Не существует контроллер' . $controller_path));
        }
        $i++;


        // получаем имя экшена
        if(!empty($core->request->routes[$i]))
        {
            $core->action = $core->request->routes[$i];
            $core->action_uri = $core->request->routes[$i];
            //режем слова по '-'
            $action_words = explode('-', $core->action);

            //проверяем сколько слов
            if(count($action_words) > 1){
                $core->action = '';
                foreach($action_words as $k => $action_word){
                    if($k == 0){
                        $core->action .= strtolower($action_word);
                    }else{
                        $core->action .= ucfirst($action_word);
                    }
                }
            }
            $i++;
        }else{
            $core->action = 'index';
            $core->action_uri = 'index';
        }

        //проверяем есть ли третий параметр
        //есть идея переработать для передачи нескольких параметров
        //но может это и не нужно


        if(!empty($core->request->routes[$i]))
        {
            $core->request_id = $core->request->routes[$i];
        }

        $action = $core->action;
        if(method_exists($controller, $action)){
            // вызываем действие контроллера
            if(isset($core->request_id)){
                return $this->response($controller->$action($core->request_id));
            }else{
                return $this->response($controller->$action());
            }
        }else{
            return $this->response((new ErrorController())->{'404'}('В контроллере не существует ' . $core->action . ' метода'));
        }
    }

    public function response(Response $response)
    {
        if(!$this->getCore()->request->is_ajax){
            $this->app_end = microtime(true);
            $this->app_seconds = number_format(($this->app_start - $this->app_end), 5);
            $this->app_bytes = memory_get_usage(true);
        }

        switch($response->type){
            case 'json':
                echo $response->data;
                break;
            case 'html':
                echo $response->data;
                echo '<!--' . $this->app_seconds . ' sec -->';
                echo '<!--' . $this->app_bytes . ' bytes -->';
                foreach($this->getCore()->debug->getQueries() as $query){
                    echo '<!--' . $query . ' bytes -->';
                }
                foreach($this->getCore()->debug->getErrors() as $error){
                    echo '<!--' . $error . ' bytes -->';
                }
                break;
            case 'xml':

                break;
            case 'image':

                break;
        }
        return true;
    }
}