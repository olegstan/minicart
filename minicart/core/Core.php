<?php
namespace core;

use core\Auth;
use core\helper\Cart;
use core\helper\Debug;
use core\helper\Flash;
use core\AssetBundle;
use core\helper\Redirect;
use core\helper\Request;
use core\Config;
use app\RouteAccess;

class Core
{
    public $tpl_path = 'test';
    public $tpl_admin_path = 'system';

    public $routes;

    public $view;
    public $asset;
    //data to layout
    public $data;

    public $layout;
    public $default_layout;

    public $title = '';
    public $meta_keywords = '';
    public $meta_description = '';

    public $remote_ip;
    public $request_uri;
    public $protocol;
    public $host;
    public $root;

    public $method;
    public $referer;

    public $session;
    public $cookie;

    /* singleton */
    static public $core;
    public $auth;
    public $flash;
    public $query;
    public $redis;

    /**
     * @var Request
     */
    public $request;
    /**
     * @var Redirect
     */
    public $redirect;

    public $config;

    /**
     * @var Settings
     */
    public $settings;

    /**
     * @var Debug
     */
    public $debug;

    public function __construct()
    {

    }

    public function init()
    {
        $this->config = Config::get();

        $this->auth = new Auth();

        $this->flash = new Flash();

        $this->asset = new AssetBundle();

        $this->request = new Request();

        $this->redirect = new Redirect();

        $this->settings = (new Settings());

        $this->debug = new Debug();

        //$this->cart = new Cart();
        //$this->protocol = $_SERVER['SERVER_PORT'] == 443 ? 'https' : 'http';
        $this->protocol = $this->config['protocol'];
        $this->remote_ip =& $_SERVER['REMOTE_ADDR']?:($_SERVER['HTTP_X_FORWARDED_FOR']?:$_SERVER['HTTP_CLIENT_IP']);
        $this->host =& $_SERVER['HTTP_HOST'];
        $this->request_uri =& $_SERVER['REQUEST_URI'];


        $this->root = $this->protocol . '://' . $this->host;
        //$this->ip =

        $this->method =& $_SERVER['REQUEST_METHOD'];
        $this->referer =& $_SERVER['HTTP_REFERER'];
        $this->session =& $_SESSION;
        $this->cookie =& $_COOKIE;

        return $this;
    }

    /**
     * @return $this
     */

    public static function getCore()
    {
        if (!static::$core) {
            static::$core = (new static())->init();
            static::$core->settings->init();
        }
        return static::$core;
    }

    private function __clone()
    {

    }

    private function __wakeup()
    {

    }
}