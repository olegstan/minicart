<?php
namespace core\helper;

class Translit
{
    public static function make($string, $space = '', $dot = '.')
    {
        $converter = [
            'а' => 'a',   'б' => 'b',   'в' => 'v',
            'г' => 'g',   'д' => 'd',   'е' => 'e',
            'ё' => 'e',   'ж' => 'zh',  'з' => 'z',
            'и' => 'i',   'й' => 'y',   'к' => 'k',
            'л' => 'l',   'м' => 'm',   'н' => 'n',
            'о' => 'o',   'п' => 'p',   'р' => 'r',
            'с' => 's',   'т' => 't',   'у' => 'u',
            'ф' => 'f',   'х' => 'h',   'ц' => 'ts',
            'ч' => 'ch',  'ш' => 'sh',  'щ' => 'shch',
            'ь' => '',  'ы' => 'i',   'ъ' => '',
            'э' => 'e',   'ю' => 'yu',  'я' => 'ya',

            'А' => 'A',   'Б' => 'B',   'В' => 'V',
            'Г' => 'G',   'Д' => 'D',   'Е' => 'E',
            'Ё' => 'E',   'Ж' => 'Zh',  'З' => 'Z',
            'И' => 'I',   'Й' => 'Y',   'К' => 'K',
            'Л' => 'L',   'М' => 'M',   'Н' => 'N',
            'О' => 'O',   'П' => 'P',   'Р' => 'R',
            'С' => 'S',   'Т' => 'T',   'У' => 'U',
            'Ф' => 'F',   'Х' => 'H',   'Ц' => 'Ts',
            'Ч' => 'Ch',  'Ш' => 'Sh',  'Щ' => 'Shch',
            'Ь' => '\'',  'Ы' => 'I',   'Ъ' => '',
            'Э' => 'E',   'Ю' => 'Yu',  'Я' => 'Ya',

            //cpecial chars
            ' ' => $space,
            '.' => $dot,
            '?' => '', '!' => '', ',' => '', ';' => '',
            '"' => '', '\'' => '', '{' => '', '}' => '',
            '(' => '', ')' => '', ':' => '', '*' => '',
            '[' => '', ']' => '', '%' => '', '$' => '',
            '#' => '', '№' => '', '@' => '', '=' => '',
            '^' => '', '-' => '', '+' => '', '/' => '',
            '\\' => '', '|' => '',
        ];
        $string = preg_replace('/\-\-+/', '-', $string);
        return trim(mb_strtolower(strtr($string, $converter)), 'utf-8');
    }
}
//
//function str2url($str) {
//    // переводим в транслит
//    $str = rus2translit($str);
//    // в нижний регистр
//    $str = strtolower($str);
//    // заменям все ненужное нам на "-"
//    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
//    // удаляем начальные и конечные '-'
//    $str = trim($str, "-");
//    return $str;
//}
