<?php
namespace core\helper;

use app\models\image\Image;
use core\helper\Header;

class Response
{
    public $type = 'html';
    public $data = '';

    public function __construct($type, $data, $status = 200)
    {
        $this->type = $type;
        $this->data = $data;
        $this->status = $status;
    }

    public static function json($a_array = null, $status = 200)
    {
        Header::json();
        return (new self('json', json_encode($a_array), $status));
    }

    public static function html($html, $status = 200)
    {
        Header::html();
        return (new self('html', $html, $status));
    }

    public static function xml($xml, $status = 200)
    {
        return (new self('xml', $xml, $status));
    }

    public static function image(Image $image, $status = 200)
    {
        return (new self('image', $image, $status));
    }
}
