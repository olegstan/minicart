<?php
namespace core\helper;

class File
{
    public static function getFiles($path)
    {
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry !== '.' && $entry !== '..') {
                    $files[] = $entry;
                }
            }
            closedir($handle);
        }
        return $files;
    }

    /**
     * @param $path
     */

    public static function read($path)
    {
        $handle = fopen($path, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                $lines[] = $line;
            }
            fclose($handle);

            return $lines;
        } else {
            return;
        }
    }

    public static function get($path)
    {

    }

    public static function save($file_name, $path)
    {

    }

    public static function saveAsString($string, $file_name, $path)
    {
        file_put_contents($path . $file_name, $string);
    }

    public static function remove()
    {

    }

    public static function download($path)
    {
        if (file_exists($path)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($path));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($path));
            readfile($path);
            die();
        }
    }

    public static function upload()
    {

    }
}