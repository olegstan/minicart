<?php
namespace core\helper;

use core\Core;

class Pagination
{
    public function getCore()
    {
        return Core::getCore();
    }

    public function by($per_page = 20)
    {
        $needed_page = $this->getCore()->request->request('page');

        if($needed_page){

        }else{
            return ['limit' => 0, 'offset' => $per_page];
        }
    }
}