<?php
namespace core\helper;

class Request
{
    public $is_ajax;
    public $is_cli;
    public $is_files = false;

    public $routes = [];
    public $routes_count = 0;

    /**
     * Конструктор, чистка слешей
     */
    public function __construct()
    {
        $this->getRoutes();
        $this->isAjax();
        $this->isCli();
        $this->isAdmin();
        $this->isFiles();
    }

    /**
    * Определение request-метода обращения к странице (GET, POST)
    * Если задан аргумент функции (название метода, в любом регистре), возвращает true или false
    * Если аргумент не задан, возвращает имя метода
    * Пример:
    *
    *    if($minicart->request->method('post'))
    *        print 'Request method is POST';
    *
    */
    public function method($method = null)
    {
        if(!empty($method))
            return strtolower($_SERVER['REQUEST_METHOD']) == strtolower($method);
        return $_SERVER['REQUEST_METHOD'];
    }

    public function has($name)
    {
        if(isset($_REQUEST[$name])){
            return true;
        }else{
            return false;
        }
    }

    public function input($name = null, $type = null, $method = 'request')
    {
        $array = [];
        switch($method){
            case 'get':
                $array = $_GET;
                break;
            case 'post':
                $array = $_POST;
                break;
            case 'request':
                $array = $_REQUEST;
                break;
        }

        $val = null;
        if(isset($array[$name]))
            $val = $array[$name];

        if(!empty($type) && is_array($val))
            $val = reset($val);

        if($type == 'string')
            return strval(preg_replace('/[^\p{L}\p{Nd}\d\s_\-\.\%\s\+\!]/ui', '', $val));

        if($type == 'integer')
            return intval($val);

        if($type == 'boolean')
            return !empty($val);

        return $val;
    }

    /**
    * Возвращает переменную отфильтрованную по заданному типу, если во втором параметре указан тип фильтра
    * Второй параметр $type может иметь такие значения: integer, string, boolean
    * Если $type не задан, возвращает переменную в чистом виде
    */
    public function get($name = null, $type = null)
    {
        return $this->input($name, $type, 'get');
    }

    public function post($name = null, $type = null)
    {
        return $this->input($name, $type, 'post');
    }

    public function request($name = null, $type = null)
    {
        return $this->input($name, $type, 'request');
    }

    public function file($name)
    {
        if (empty($_FILES[$name]))
            return null;

        return $_FILES[$name];
    }

    /**
    * Возвращает переменную $_FILES
    * Обычно переменные $_FILES являются двухмерными массивами, поэтому можно указать второй параметр,
    * например, чтобы получить имя загруженного файла: $filename = request->files('myfile', 'name');
    */
    public function files($name)
    {
        if (empty($_FILES[$name]))
            return null;
        $files_post = $_FILES[$name];
        $files_array = array();

        if('array' !== gettype($files_post['name'])) {
            $files_array[] = $files_post;
        }else{
            $keys = array_keys($files_post['name']);
            foreach ($keys as $key) {
                foreach ($files_post as $res => $item) {
                    $files_array[$key][$res] = $item[$key];
                }
            }
        }

        return $files_array;
    }

    private function isAjax()
    {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $this->is_ajax = true;
        }
    }

    /**
     * если консоль то is_cli = true
     *
     */

    private function isCli()
    {
        if(php_sapi_name() == 'cli'){
            $this->is_cli = true;
        }
    }

    private function isAdmin()
    {
        if($this->routes[0] == 'admin'){
            $this->is_admin = true;
            array_shift($this->routes);
        }
    }

    private function isFiles()
    {
        if($this->routes[0] == 'files'){
            $this->is_files = true;
        }
    }

    public function getRoutes()
    {
        $parts = explode('?', $_SERVER['REQUEST_URI']);

        $route_parts = explode('/', $parts[0]);

        foreach ($route_parts as $part) {
            if($part){
                $this->routes[] = $part;
            }
            $this->routes_count++;
        }
    }
}