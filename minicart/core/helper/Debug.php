<?php

namespace core\helper;

class Debug
{
    private $queries = [];
    private $errors = [];

    public function getQueries()
    {
        return $this->queries;
    }

    public function addQuery($query)
    {
        $this->queries[] = $query;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function addError($error)
    {
        $this->errors[] = $error;
    }
}