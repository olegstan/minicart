<?php

/**
 * Minicart CMS
 * 
 * 
 */
 
require_once('api/core/Minicart.php');

class Compare extends Minicart
{

    /*
    *
    * Функция возвращает корзину
    *
    */
    public function get_compare()
    {
        /*$compare = new stdClass();
        $compare->purchases = array();
        $compare->total_price = 0;
        $compare->total_products = 0;

        // Берем из сессии список variant_id=>amount
        if(!empty($_SESSION['user_compare']))
        {
            $compare_items = $_SESSION['user_compare'];

            $products = array();
            foreach($this->products->get_products(array('id'=>$compare_items)) as $p)
            {
                $p->images = $this->image->get_images('products', $p->id);
                $p->image = @reset($p->images);
                $products[$p->id]=$p;

                $compare->purchases[] = $p;
            }
        }

        return $compare;*/

        if (!array_key_exists('user_compare', $_SESSION))
            return array();
        return array_keys($_SESSION['user_compare']);
    }

    /*
    *
    * Добавление товара в сравнение
    *
    */
    public function add_item($product_id)
    {
        if(isset($_SESSION['user_compare'][$product_id]))
              return;

        // Выберем товар из базы, заодно убедившись в его существовании
        $product = $this->products->get_product($product_id);

        // Если товар существует, добавим его в сравнение
        if(!empty($product))
            $_SESSION['user_compare'][$product_id] = 1;
    }


    /*
    *
    * Удаление товара из сравнения
    *
    */
    public function remove_item($product_id)
    {
        unset($_SESSION['user_compare'][$product_id]);
    }

    /*
    *
    * Очистка сравнения
    *
    */
    public function empty_compare()
    {
        unset($_SESSION['user_compare']);
    }
 
}