<?php
namespace core\helper;

class Hash
{
    public static $options;

    public static function password($str)
    {
        $path = ABS . '/config/config.php';
        if(file_exists($path)){
            $config = require($path);
        }else{
            die(__FILE__ . ' ' . __LINE__ . 'Не найден файл с настройками: ' . $path);
        }

        self::$options = [
            'cost' => $config['hash_cost'],
            'salt' => $config['hash_salt']
        ];

        return password_hash($str, PASSWORD_BCRYPT, self::$options);
    }

    public static function verify($str, $hash)
    {
        return password_verify($str, $hash);
    }

    public function random($length)
    {

    }

}