<?php
namespace core\helper;

class Cart
{


    /*
    *
    * Функция возвращает корзину
    *
    */
    public function get_cart()
    {
        $cart = new stdClass();
        $cart->purchases = array();
        $cart->total_price = 0;
        $cart->total_products = 0;

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_admin = 1");
        $admin_currency = $this->db->result();

        // Берем из сессии список variant_id=>amount
        if(!empty($_SESSION['user_cart']))
        {
            $cart_items = $_SESSION['user_cart'];

            foreach($cart_items as $cart_item)
            {
                $purchase = new stdClass();
                $purchase->variant = $this->variants->get_variant($cart_item['variant_id']);
                if (!empty($purchase->variant))
                {
                    $purchase->product = $this->products->get_product($purchase->variant->product_id);
                    if (!empty($purchase->product)){
                        $purchase->product->images = $this->image->get_images('products', $purchase->product->id);
                        $purchase->product->image = @reset($purchase->product->images);
                    }
                }
                $purchase->amount = $cart_item['amount'];
                $purchase->var_amount = $cart_item['var_amount'];
                $purchase->modificators = [];
                $purchase->modificators_count = [];

                if (!empty($cart_item['modificators']))
                    foreach($cart_item['modificators'] as $idx=>$m_id){
                        $m = $this->modificators->get_modificator(intval($m_id));
                        if ($m){
                            $purchase->modificators[] = $m;
                            if (array_key_exists($idx, $cart_item['modificators_count']))
                                $purchase->modificators_count[] = intval($cart_item['modificators_count'][$idx]);
                            else
                                $purchase->modificators_count[] = 1;
                        }
                    }

                //изменим цену варианта в соответствии с модификаторами
                $tmp_price = $purchase->variant->price;

                if ($this->settings->catalog_use_variable_amount && $purchase->product->use_variable_amount)
                {
                    $tmp_price *= $purchase->var_amount;
                    $purchase->variant->price *= $purchase->var_amount;
                }

                foreach($purchase->modificators as $idx=>$m){
                    $v = $m->value;
                    $count = $purchase->modificators_count[$idx];
                    if ($m->multi_apply == 0 && $purchase->amount > 1)
                        continue;
                        //$v *= $purchase->amount;
                    if ($m->type == 'plus_fix_sum')
                        $purchase->variant->price += $v * $count;
                    if ($m->type == 'minus_fix_sum')
                        $purchase->variant->price -= $v * $count;
                    if ($m->type == 'plus_percent')
                        $purchase->variant->price += $tmp_price * $v * $count / 100;
                    if ($m->type == 'minus_percent')
                        $purchase->variant->price -= $tmp_price * $v * $count / 100;
                }

                $purchase->variant->price_for_mul = $purchase->variant->price;
                $purchase->variant->price_for_one_pcs = $purchase->variant->price_for_mul;

                foreach($purchase->modificators as $idx=>$m){
                    $v = $m->value;
                    $count = $purchase->modificators_count[$idx];
                    if (!($m->multi_apply == 0 && $purchase->amount > 1))
                        continue;
                        //$v *= $purchase->amount;
                    if ($m->type == 'plus_fix_sum')
                        $purchase->variant->price_for_one_pcs += $v * $count;
                    if ($m->type == 'minus_fix_sum')
                        $purchase->variant->price_for_one_pcs -= $v * $count;
                    if ($m->type == 'plus_percent')
                        $purchase->variant->price_for_one_pcs += $tmp_price * $v * $count / 100;
                    if ($m->type == 'minus_percent')
                        $purchase->variant->price_for_one_pcs -= $tmp_price * $v * $count / 100;
                }

                $purchase->variant->additional_sum = $purchase->variant->price_for_one_pcs - $purchase->variant->price_for_mul;

                $cart->purchases[] = $purchase;
                //$cart->total_price += floatval($this->currencies->convert($purchase->variant->price, $purchase->product->currency_id ? $purchase->product->currency_id : $admin_currency->id, false))*$purchase->amount;
                $cart->total_price += floatval($this->currencies->convert($purchase->variant->price_for_mul, $purchase->product->currency_id ? $purchase->product->currency_id : $admin_currency->id, false))*$purchase->amount + floatval($this->currencies->convert($purchase->variant->additional_sum, $purchase->product->currency_id ? $purchase->product->currency_id : $admin_currency->id, false));

                $cart->total_products += $purchase->amount;
            }
        }

        return $cart;
    }

    /*
    *
    * Добавление варианта товара в корзину
    *
    */
    public function add_item($variant_id, $amount = 1, $var_amount = 1, $modificators = array(), $modificators_count = array())
    {
        $amount = max(1, $amount);

        $finded_index = -1;
        if (isset($_SESSION['user_cart'])){
            foreach($_SESSION['user_cart'] as $idx=>$cart_item){
                if ($cart_item['variant_id'] == $variant_id && $cart_item['var_amount'] == $var_amount && $cart_item['modificators'] == $modificators && $cart_item['modificators_count'] == $modificators_count){
                    $finded_index = $idx;
                    $amount = max(1, $amount + $cart_item['amount']);
                }
            }
        }

        // Выберем товар из базы, заодно убедившись в его существовании
        $variant = $this->variants->get_variant($variant_id);

        if (!empty($variant))
        {
            if (!($variant->stock > 0 && $variant->stock < $amount)){
                if ($finded_index >= 0){
                    $_SESSION['user_cart'][$finded_index]['amount'] = intval($amount);
                }
                else{
                    $item = array('variant_id' => $variant_id,
                                'amount' => intval($amount),
                                'var_amount' => floatval($var_amount),
                                'modificators' => $modificators,
                                'modificators_count' => $modificators_count);
                    $_SESSION['user_cart'][] = $item;
                    $finded_index = count($_SESSION['user_cart'])-1;
                }

                return ($variant->stock > 0 ? $variant->stock : 999) - ($finded_index > 0 ? $_SESSION['user_cart'][$finded_index]['amount'] : 0);
            }
        }

        return 0;
    }

    /*
    *
    * Обновление количества товара
    *
    */
    public function update_item($variant_id, $amount = 1)
    {
        $amount = max(1, $amount);

        // Выберем товар из базы, заодно убедившись в его существовании
        if (isset($_SESSION['user_cart'][$variant_id])){
            $v_id = $_SESSION['user_cart'][$variant_id]['variant_id'];
            $variant = $this->variants->get_variant($v_id);
        }

        // Если товар существует, добавим его в корзину
        if(!empty($variant))
            $_SESSION['user_cart'][$variant_id]['amount'] = intval($amount);
    }


    /*
    *
    * Удаление товара из корзины
    *
    */
    public function remove_item($variant_id)
    {
        unset($_SESSION['user_cart'][$variant_id]);
    }

    /*
    *
    * Очистка корзины
    *
    */
    public function empty_cart()
    {
        unset($_SESSION['user_cart']);
    }
 
}