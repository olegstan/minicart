<?php

/**
 * Minicart CMS
 *
 */

require_once('api/core/Minicart.php');
 
class EmailNotify extends Minicart
{
    function email_standard($to, $subject, $message, $from = '', $reply_to = '')
    {
        $headers = "MIME-Version: 1.0\n" ;
        $headers .= "Content-type: text/html; charset=utf-8; \r\n";
        $headers .= "From: $from\r\n";
        if(!empty($reply_to))
            $headers .= "reply-to: $reply_to\r\n";
        $subject = "=?utf-8?B?".base64_encode($subject)."?=";

        @mail($to, $subject, $message, $headers);
    }

    function email_yandex_smtp($to, $subject, $message, $from = '', $reply_to = '')
    {
        require_once "SendMailSmtpClass.php"; // подключаем класс
  
        $mailSMTP = new SendMailSmtpClass($this->settings->yandexsmtp_login, $this->settings->yandexsmtp_password, 'ssl://smtp.yandex.ru', $this->settings->company_name, 465);
        // $mailSMTP = new SendMailSmtpClass('логин', 'пароль', 'хост', 'имя отправителя');

        // заголовок письма
        $headers= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=utf-8\r\n"; // кодировка письма
        $headers .= "From: $from\r\n"; // от кого письмо
        $result =  $mailSMTP->send($to, $subject, $message, $headers); // отправляем письмо
    }

    function email($to, $subject, $message, $from = '', $reply_to = '')
    {
        if ($this->settings->yandexsmtp_enabled)
            $this->email_yandex_smtp($to, $subject, $message, $from, $reply_to);
        else
            $this->email_standard($to, $subject, $message, $from, $reply_to);
    }

    function sms($to, $message)
    {
        if (!$this->settings->sms_login ||
            !$this->settings->sms_key ||
            !$this->settings->sms_sender_name ||
            empty($to) ||
            empty($message))
                return false;

        $url = "http://smsmini.ru/service/send.php?username=".$this->settings->sms_login."&password=".$this->settings->sms_key."&from=".$this->settings->sms_sender_name."&to=".rawurlencode($to)."&message=".rawurlencode($message);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 200);
        $data = curl_exec($ch);
        $curl_errno = curl_errno($ch);
        $curl_error = curl_error($ch);
        curl_close($ch);

        //exec("wget " . $url);

        /*$fp = fopen($url,'rt');
        if ($fp)
        {
            $r = fgets($fp);
            fclose($fp);
            return true;
        }*/
        return false;
    }

    function get_template($url)
    {
        $this->db->query("SELECT * FROM __email_templates WHERE url=?", $url);
        $template = $this->db->result();
        return $template;
    }

    function get_sms_template($code)
    {
        $this->db->query("SELECT * FROM __sms_templates WHERE code=?", $code);
        $template = $this->db->result();
        return $template;
    }

    function make_purchases_list_table($purchases)
    {
        if ($purchases)
        {
            foreach($purchases as $index=>$purchase)
            {
                $purchases[$index]->product = $this->products->get_product($purchase->product_id);
                $purchases[$index]->variants = $this->variants->get_variants(array('product_id'=>$purchase->product_id));
                $purchases[$index]->variant = $this->variants->get_variant($purchase->variant_id);
                $purchases[$index]->images = $this->image->get_images('products', $purchase->product_id);
                $purchases[$index]->image = reset($purchases[$index]->images);
                $purchases[$index]->modificators = $this->orders->get_purchases_modificators(array('purchase_id' => $purchase->id));
            }
            $purchases_total = 0;
            $purchases_count = 0;
            foreach($purchases as $purchase)
            {
                $purchases_total += $purchase->price*$purchase->amount;
                $purchases_count += $purchase->amount;
            }
        }

        $product_module = $this->furl->get_module_by_name('ProductController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_admin = 1");
        $admin_currency = $this->db->result();

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $str = "<table style=\"border-collapse: collapse;\" cellpadding=\"6\" cellspacing=\"0\" border=\"1\"><tbody>";
        foreach($purchases as $purchase)
        {
            $modificators_str = "";
            foreach($purchase->modificators as $m)
            {
                if (!empty($modificators_str))
                    $modificators_str .= ", ";
                $modificators_str .= $m->modificator_name;

                if ($m->modificator_amount>1)
                    $modificators_str .= " ".$m->modificator_amount." шт.";
            }

            $str .= "<tr>";
            $str .= "<td align=\"center\" style=\"padding:6px; width:70; padding:6px; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;\">".($purchase->image?"<img src=\"".$this->design->resize_modifier($purchase->image->filename, 'products', 60, 60)."\">":"")."</td> ";
            $str .= "<td style=\"padding:6px; width:250; padding:6px; background-color:#f0f0f0; border:1px solid #e0e0e0;font-family:arial;\"><a href=\"".($purchase->product?$this->config->root_url.$product_module->url.$purchase->product->url.$this->settings->postfix_product_url."?variant=".$purchase->variant_id:"")."\">".$purchase->product_name.($this->settings->cart_show_product_id_in_email?" (".$purchase->product->id.")":"").($purchase->variant_name?" (".$purchase->variant_name.")":"").(($this->settings->catalog_use_variable_amount && $purchase->product->use_variable_amount) ? " (".$this->settings->catalog_variable_amount_name." ".round($purchase->var_amount,1)." ".$this->settings->catalog_variable_amount_dimension.")" : "").(!empty($modificators_str)?" (".$modificators_str.")":"")."</a></td> ";
            $str .= "<td style=\"padding:6px; text-align:right; width:150; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;\">".$purchase->amount." ".$this->settings->units."</td>" ;
            $str .= "<td style=\"padding:6px; text-align:right; width:150; background-color:#ffffff; border:1px solid #e0e0e0;font-family:arial;\">".$this->currencies->convert($purchase->price*$purchase->amount)." ".$main_currency->sign."</td> ";
            $str .= "</tr>";
        }
        $str .= "</tbody></table>";

        return $str;
    }

    function make_purchases_list_table_for_sms($purchases)
    {
        $str = "";
        foreach($purchases as $purchase){
            if (!empty($str))
                $str .= ", ";
            $str .= $purchase->product_name;
            if (!empty($purchase->variant_name))
                $str .= " (".$purchase->variant_name.")";
        }

        return $str;
    }

#### SECTION :: EMAIL

    public function email_new_user($user_id)
    {
        if(!($user = $this->users->get_user(intval($user_id))) ||
            !($template = $this->get_template('registration')) ||
            empty($user->email) ||
            !$template->enabled)
            return false;

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$name$', $user->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $user->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$user->phone_code.') '.$this->design->phone_mask_modifier($user->phone), $template->message_text);
        $template->message_text = str_replace('$address$', $user->delivery_address, $template->message_text);
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $this->email($user->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_reset_password($user_id)
    {
        if(!($user = $this->users->get_user(intval($user_id))) ||
            !($template = $this->get_template('reset-password')) ||
            empty($user->email) ||
            !$template->enabled)
            return false;

        $login_module = $this->furl->get_module_by_name('LoginController');

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$name$', $user->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $user->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$user->phone_code.') '.$this->design->phone_mask_modifier($user->phone), $template->message_text);
        $template->message_text = str_replace('$address$', $user->delivery_address, $template->message_text);
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);
        $template->message_text = str_replace('$RESET_URL$', $this->config->root_url . $login_module->url . '?mode=new-password&reset_code='.$user->reset_url, $template->message_text);

        $this->email($user->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_new_order($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('new-order')) ||
            empty($order->email) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);
        $template->message_text = str_replace('$order_comment$', $order->comment, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($order->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_change_order_status($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('change-order-status')) ||
            empty($order->email) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($order->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_notification_product_available($user_id)
    {
        if(!($user = $this->users->get_user(intval($user_id))) ||
            !($template = $this->get_template('notification-product-available')) ||
            empty($user->email) ||
            !$template->enabled)
            return false;

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$name$', $user->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $user->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$user->phone_code.') '.$this->design->phone_mask_modifier($user->phone), $template->message_text);
        $template->message_text = str_replace('$address$', $user->delivery_address, $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $this->email($user->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_new_order_admin($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('new-order-admin')) ||
            /*empty($this->settings->order_email) ||*/
            !$template->enabled)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);
        $template->message_text = str_replace('$order_comment$', $order->comment, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($this->settings->order_email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_change_order_status_admin($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('change-order-status-admin')) ||
            !$template->enabled)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($this->settings->order_email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_callback($callback_id)
    {
        if(!($callback = $this->callbacks->get_callback(intval($callback_id))) ||
            !($template = $this->get_template('callback')) ||
            /*empty($this->settings->order_email) ||*/
            !$template->enabled)
            return false;

        if ($callback->user_id > 0)
            $callback->user = $this->users->get_user($callback->user_id);

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);
        $template->message_header = str_replace('$call_id$', $callback->id, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);
        $template->message_text = str_replace('$call_id$', $callback->id, $template->message_text);
        $template->message_text = str_replace('$name$', isset($callback->user) ? $callback->user->name : (empty($callback->user_name) ? "Аноним" : $callback->user_name), $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$callback->phone_code.') '.$this->design->phone_mask_modifier($callback->phone), $template->message_text);
        $template->message_text = str_replace('$call_time$', $callback->call_time, $template->message_text);
        $template->message_text = str_replace('$call_message$', $callback->message, $template->message_text);

        $this->email($this->settings->comment_email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_user_question($question_id)
    {
        if(!($template = $this->get_template('user-question')) ||
            empty($this->settings->order_email) ||
            !$template->enabled)
            return false;

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $this->email($this->settings->order_email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_cancel_order($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('cancel-order')) ||
            empty($order->email) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);
        $template->message_text = str_replace('$order_comment$', $order->comment, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($order->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_order_paid($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('order-paid')) ||
            empty($order->email) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$phone2$', '+7 ('.$order->phone2_code.') '.$this->design->phone_mask_modifier($order->phone2), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($order->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_order_paid_admin($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('order-paid-admin')) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$phone2$', '+7 ('.$order->phone2_code.') '.$this->design->phone_mask_modifier($order->phone2), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($this->settings->order_email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

    public function email_allow_order_payment($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_template('paid-accepted')) ||
            !$template->enabled)
            return false;

        if (!$order->notify_news)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в заголовке
        $template->message_header = str_replace('$shop_name$', $this->settings->site_name, $template->message_header);
        $template->message_header = str_replace('$id$', $order->id, $template->message_header);
        $template->message_header = str_replace('$site_url$', $this->config->root_url, $template->message_header);

        //Замена в тексте письма
        $template->message_text = str_replace('$shop_name$', $this->settings->site_name, $template->message_text);
        $template->message_text = str_replace('$id$', $order->id, $template->message_text);
        $template->message_text = str_replace('$name$', $order->name, $template->message_text);
        $template->message_text = str_replace('$mail$', $order->email, $template->message_text);
        $template->message_text = str_replace('$phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->message_text);
        $template->message_text = str_replace('$phone2$', '+7 ('.$order->phone2_code.') '.$this->design->phone_mask_modifier($order->phone2), $template->message_text);
        $template->message_text = str_replace('$delivery_method$', $delivery_method->name, $template->message_text);
        $template->message_text = str_replace('$address$', $order->address, $template->message_text);
        $template->message_text = str_replace('$order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->message_text);
        $template->message_text = str_replace('$order_sum$', $this->currencies->convert($order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_discount$', $order->discount.' %', $template->message_text);
        $template->message_text = str_replace('$order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price).' '.$main_currency->sign, $template->message_text);
        $template->message_text = str_replace('$order_status$', $status->group_name, $template->message_text);
        $template->message_text = str_replace('$order_date$', $this->design->date_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$order_time$', $this->design->time_modifier($order->date), $template->message_text);
        $template->message_text = str_replace('$products$', $this->make_purchases_list_table($purchases), $template->message_text);
        $template->message_text = str_replace('$site_url$', $this->config->root_url, $template->message_text);

        $delivery_price = "";
        if ($delivery_method->delivery_type == "separately")
            $delivery_price = "Оплачивается отдельно";
        else
            if (($order->delivery_price > 0 && $delivery_method->free_from == 0) || ($delivery_method->free_from > 0 && $delivery_method->free_from > $order->total_price))
                $delivery_price = $this->currencies->convert($order->delivery_price).' '.$main_currency->sign;
            else
                $delivery_price = "Бесплатно";
        $template->message_text = str_replace('$delivery_price$', $delivery_price, $template->message_text);

        $this->email($order->email, $template->message_header, $template->message_text, $this->settings->template_email);
    }

#### SECTION :: SMS

    public function sms_new_order($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_sms_template('new-order')) ||
            empty($order->phone) ||
            !$template->enabled)
            return false;

        if (!$order->notify_order)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в тексте сообщения
        $template->value = str_replace('$sms_shop_name$', $this->settings->site_name, $template->value);
        $template->value = str_replace('$sms_id$', $order->id, $template->value);
        $template->value = str_replace('$sms_name$', $order->name, $template->value);
        $template->value = str_replace('$sms_mail$', $order->email, $template->value);
        $template->value = str_replace('$sms_phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->value);
        $template->value = str_replace('$sms_delivery_method$', $delivery_method->name, $template->value);
        $template->value = str_replace('$sms_address$', $order->address, $template->value);
        $template->value = str_replace('$sms_order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->value);
        $template->value = str_replace('$sms_order_sum$', $this->currencies->convert($order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_discount$', $order->discount.' %', $template->value);
        $template->value = str_replace('$sms_order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_status$', $status->group_name, $template->value);
        $template->value = str_replace('$sms_order_date$', $this->design->date_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_order_time$', $this->design->time_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_products$', $this->make_purchases_list_table_for_sms($purchases), $template->value);
        $template->value = str_replace('$sms_site_url$', $this->config->root_url, $template->value);
        $template->value = str_replace('$sms_order_comment$', $order->comment, $template->value);

        $this->sms('7'.$order->phone_code.$order->phone, $template->value);
    }

    public function sms_new_order_admin($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_sms_template('new-order-admin')) ||
            !$template->enabled)
            return false;

        if (!$order->notify_order)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в тексте сообщения
        $template->value = str_replace('$sms_shop_name$', $this->settings->site_name, $template->value);
        $template->value = str_replace('$sms_id$', $order->id, $template->value);
        $template->value = str_replace('$sms_name$', $order->name, $template->value);
        $template->value = str_replace('$sms_mail$', $order->email, $template->value);
        $template->value = str_replace('$sms_phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->value);
        $template->value = str_replace('$sms_delivery_method$', $delivery_method->name, $template->value);
        $template->value = str_replace('$sms_address$', $order->address, $template->value);
        $template->value = str_replace('$sms_order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->value);
        $template->value = str_replace('$sms_order_sum$', $this->currencies->convert($order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_discount$', $order->discount.' %', $template->value);
        $template->value = str_replace('$sms_order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_status$', $status->group_name, $template->value);
        $template->value = str_replace('$sms_order_date$', $this->design->date_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_order_time$', $this->design->time_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_products$', $this->make_purchases_list_table_for_sms($purchases), $template->value);
        $template->value = str_replace('$sms_site_url$', $this->config->root_url, $template->value);
        $template->value = str_replace('$sms_order_comment$', $order->comment, $template->value);

        if ($this->settings->sms_admin_phone_code && $this->settings->sms_admin_phone)
            $this->sms('7'.$this->settings->sms_admin_phone_code.$this->settings->sms_admin_phone, $template->value);
        if ($this->settings->sms_admin_phone2_code && $this->settings->sms_admin_phone2)
            $this->sms('7'.$this->settings->sms_admin_phone2_code.$this->settings->sms_admin_phone2, $template->value);
    }

    public function sms_change_order_status($order_id)
    {
        if(!($order = $this->orders->get_order(intval($order_id))) ||
            !($template = $this->get_sms_template('change-order-status')) ||
            empty($order->phone) ||
            !$template->enabled)
            return false;

        if (!$order->notify_order)
            return false;

        $order_module = $this->furl->get_module_by_name('OrderController');
        $all_currencies = $this->currencies->get_currencies(array('enabled'=>1));
        $currency = reset($all_currencies);
        $status = $this->orders->get_status($order->status_id);

        $delivery_method = $this->deliveries->get_delivery($order->delivery_id);

        $this->db->query("SELECT * FROM __currencies WHERE enabled = 1 AND use_main = 1");
        $main_currency = $this->db->result();

        $purchases = $this->orders->get_purchases(array('order_id'=>$order->id));

        //Замена в тексте сообщения
        $template->value = str_replace('$sms_shop_name$', $this->settings->site_name, $template->value);
        $template->value = str_replace('$sms_id$', $order->id, $template->value);
        $template->value = str_replace('$sms_name$', $order->name, $template->value);
        $template->value = str_replace('$sms_mail$', $order->email, $template->value);
        $template->value = str_replace('$sms_phone$', '+7 ('.$order->phone_code.') '.$this->design->phone_mask_modifier($order->phone), $template->value);
        $template->value = str_replace('$sms_delivery_method$', $delivery_method->name, $template->value);
        $template->value = str_replace('$sms_address$', $order->address, $template->value);
        $template->value = str_replace('$sms_order_link$', $this->config->root_url.$order_module->url.$order->url.'/', $template->value);
        $template->value = str_replace('$sms_order_sum$', $this->currencies->convert($order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_discount$', $order->discount.' %', $template->value);
        $template->value = str_replace('$sms_order_discount_sum$', $this->currencies->convert($order->total_price_wo_discount-$order->total_price, null, false).' '.$main_currency->sign_simple, $template->value);
        $template->value = str_replace('$sms_order_status$', $status->group_name, $template->value);
        $template->value = str_replace('$sms_order_date$', $this->design->date_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_order_time$', $this->design->time_modifier($order->date), $template->value);
        $template->value = str_replace('$sms_products$', $this->make_purchases_list_table_for_sms($purchases), $template->value);
        $template->value = str_replace('$sms_site_url$', $this->config->root_url, $template->value);
        $template->value = str_replace('$sms_order_comment$', $order->comment, $template->value);

        $this->sms('7'.$order->phone_code.$order->phone, $template->value);
    }

    public function sms_callback($callback_id)
    {
        if(!($callback = $this->callbacks->get_callback(intval($callback_id))) ||
            !($template = $this->get_sms_template('callback')) ||
            /*empty($this->settings->order_email) ||*/
            !$template->enabled)
            return false;

        if ($callback->user_id > 0)
            $callback->user = $this->users->get_user($callback->user_id);

        //Замена в тексте сообщения
        $template->value = str_replace('$sms_shop_name$', $this->settings->site_name, $template->value);
        $template->value = str_replace('$sms_site_url$', $this->config->root_url, $template->value);
        $template->value = str_replace('$sms_call_id$', $callback->id, $template->value);
        $template->value = str_replace('$sms_name$', isset($callback->user) ? $callback->user->name : (empty($callback->user_name) ? "Аноним" : $callback->user_name), $template->value);
        $template->value = str_replace('$sms_phone$', '+7 ('.$callback->phone_code.') '.$this->design->phone_mask_modifier($callback->phone), $template->value);
        $template->value = str_replace('$sms_call_time$', $callback->call_time, $template->value);
        $template->value = str_replace('$sms_call_message$', $callback->message, $template->value);

        if ($this->settings->sms_admin_phone_code && $this->settings->sms_admin_phone)
            $this->sms('7'.$this->settings->sms_admin_phone_code.$this->settings->sms_admin_phone, $template->value);
        if ($this->settings->sms_admin_phone2_code && $this->settings->sms_admin_phone2)
            $this->sms('7'.$this->settings->sms_admin_phone2_code.$this->settings->sms_admin_phone2, $template->value);
    }
}