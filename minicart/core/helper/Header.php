<?php
namespace core\helper;

use app\models\image\Image;

class Header
{
    public static function json()
    {
        header("Cache-Control: no-cache, must-revalidate");
        header("Content-type: application/json; charset=UTF-8");
        header("Pragma: no-cache");
        header("Expires: -1");
    }

    public static function image(Image $image)
    {
        header('Content-type: ' . $image->content_type);
        header("Content-Length: " . filesize(ABS . $image->resized_filename));
    }

    public static function html()
    {
        header('Content-Type: text/html; charset=utf-8');
    }
}