<?php
namespace core;

class Autoloader {

    public static $loader;

    public static function init()
    {
        if(!static::$loader){
            static::$loader = new static();
        }

        return static::$loader;
    }

    public function __construct()
    {
        spl_autoload_register([$this, 'autoload']);
    }

    public function autoload($class)
    {
        //директория с ядром
        $minicart_path = 'minicart';

        $parts = explode('\\', $class);
        $count_parts = count($parts);

        $file_path = ABS . '/' . $minicart_path . '/';

        $i = 0;
        foreach($parts as $part){
            if($i !== ($count_parts - 1)){
                $file_path .= $parts[$i] . '/';
            }else{
                $file_path .= $parts[$i] . '.php';
            }
            $i++;
        }

        if(file_exists($file_path)){
            require_once($file_path);
        }else{
            //пропускать костыль автоподгрузчика
            //надо придумать другой способ

            if($class == 'Imagick'){

            }else{
                echo 'Не найден файл ' . $file_path;
            }
        }

    }

}