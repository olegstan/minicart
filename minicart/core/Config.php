<?php
namespace core;

class Config
{
    public static function get()
    {
        $path = ABS . '/config/config.php';
        if(file_exists($path)){
            return require($path);
        }else{
            die(__FILE__ . ' ' . __LINE__ . 'Не найден файл с настройками: ' . $path);
        }
    }
}