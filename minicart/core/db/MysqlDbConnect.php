<?php
namespace core\db;

use PDO;

class MysqlDbConnect
{
    private static $pdo = [];
    private static $connections = [];
    private $debug;

    public function __construct()
    {

        //$this->debug = new Debuger();
    }

    //запрещаем копирование объекта
    private function __clone()
    {

    }

    private function __wakeup()
    {

    }

    //по умолчанию 0 соединение
    public static function getPDO($connection)
    {
        $path = ABS . '/config/config.php';
        if(file_exists($path)){
            self::$connections = require($path);
        }else{
            die(__FILE__ . ' ' . __LINE__ . 'Не найден файл с соединениями: ' . $path);
        }

        if (!isset(self::$pdo[$connection])) {
            try {
                self::$pdo[$connection] = new PDO(self::$connections['type'] . ":host=" . self::$connections['host'] . ";dbname=" . self::$connections['database'] . ";charset=" . self::$connections['charset'], self::$connections['user'], self::$connections['password']);
            } catch (PDOException $e) {

                die('Connection error: ' . $e->getMessage());
            }
        }
        return self::$pdo[$connection];
    }

    public function getDebug()
    {
        return $this->debug;
    }

    public static function getConnections()
    {

    }
}
