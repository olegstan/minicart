<?php
namespace core\db;

//use core\db\MysqlDbConnect;
use app\controllers\ErrorController;
use core\Collection;
use core\Core;
use PDO;

class Query extends MysqlDbConnect
{
    /**
     * @param $s_table_name
     * @param $s_class_name
     * @param null $a_left_join
     */
    
    public function getCore()
    {
        return Core::getCore();
    }

    public function __construct($i_connection, $s_table_name, $s_class_name, $a_guarded, $a_fillable/*, $a_left_join = null*/)
    {
        $this->s_table_name = $s_table_name;
        $this->s_class_name = $s_class_name;
        $this->a_fillable = $a_fillable;
        $this->a_guarded = $a_guarded;

        /*$this->a_left_join = $a_left_join;*/

        $this->i_connection = $i_connection;
    }

    /**
     * @param $field_names
     * @param $field_values
     *
     * //работа с БД
     *
     * //три типа вывода массива
     * //fetch
     * //PDO::FETCH_NUM
     * //PDO::FETCH_ASSOC
     * //PDO::FETCH_BOTH
     *
     * //fetchAll
     * //PDO::FETCH_COLUMN
     * //PDO::FETCH_CLASS
     * //PDO::FETCH_GROUP
     * //PDO::FETCH_UNIQUE
     * //PDO::FETCH_PROPS_LATE
     *
     * //fetchColumn
     *
     * //select по ID по массиву ID или по значению
     *
     **/

    private $s_table_name;
    private $s_class_name;

    private $a_parts = [
        'select' => '',
        'distinct' => '',
        'from' => '',
        'insert' => '',
        'update' => '',
        'where' => '',
        'order' => '',
        'group' => '',
        'limit' => ''
    ];
    private $a_bind = [];
    //PDO statement
    private $stmt;

    public $a_fields = [];

    public $a_relations = [];
    public $a_joins = [];

    /**
     * @var string
     * select or insert or update or delete
     */

    public $s_mode;


    private $i_query_start = 0;
    private $i_query_end = 0;
    private $i_query_seconds;

    public function distinct()
    {
        $this->a_parts['distinct'] = 'DISTINCT ';
        return $this;
    }

    public function select($a_fields = null)
    {
        $this->a_parts['select'] = 'SELECT ';

        if(!empty($this->a_parts['distinct'])){
            $this->a_parts['select'] .= $this->a_parts['distinct'];
        }

        if(isset($a_fields)){
            $i_count = count($a_fields) - 1;

            foreach ($a_fields as $i_k => $s_field) {
                $s_comma = ($i_count != $i_k) ? ', ' : ' ';
                $this->a_parts['select'] .=  $s_field .  $s_comma . ' ';
            }
        }else{
            $this->a_parts['select'] .= ' ' . $this->s_table_name . '.* ';
        }

        $this->a_parts['from'] .= 'FROM ' . $this->s_table_name . ' ';

        $this->s_mode = 'select';
        return $this;
    }

    public function from($table)
    {
        $this->a_parts['from'] = 'FROM ' . $table . ' ';

        return $this;
    }

    /**
     * @param null $conditions
     * @param null $fields
     * @return $this
     *
     */

    public function where($conditions = null, $fields = null)
    {
        if($conditions){
            $this->a_parts['where'] = 'WHERE ';

            $this->a_parts['where'] .= $conditions;

            if ($fields !== null) {
                foreach ($fields as $k => $v) {
                    $this->a_bind += [$k => $v];
                }
            }
        }
        return $this;
    }

    public function leftJoin($table, $condition = null)
    {
        $this->a_joins[] = ' LEFT JOIN ' . $table . (isset($condition) ? ' ON ' . $condition . ' ' : ' ');

        return $this;
    }

    public function rightJoin($table, $condition = null)
    {
        $this->a_joins[] = ' RIGHT JOIN ' . $table . (isset($condition) ? ' ON ' . $condition . ' ' : ' ');

        return $this;
    }

    public function innerJoin($table, $condition = null)
    {
        $this->a_joins[] = ' INNER JOIN ' . $table . (isset($condition) ? ' ON ' . $condition . ' ' : ' ');

        return $this;
    }

    /**
     * @return $this
     */

    public function delete()
    {
        $this->a_parts['delete'] = 'DELETE FROM ' . $this->s_table_name . ' ';

        $this->s_mode = 'delete';
        return $this;
    }

    /**
     * @param $object
     * @return $this
     */
    public function insert($data, $ignore = false)
    {
        $column_names = $this->getColumnNames($this->s_table_name);

        if (is_object($data)) {
            $values = get_object_vars($data);
        }else if(is_array($data)){
            $values = $data;
        }

        //проверка есть ли такое поле в таблице
        foreach ($values as $k => $v) {
            if (in_array($k, $column_names) && !in_array($k, $this->a_guarded) && $k !== 'id') {
                $this->a_fields[$k] = $v;
            }
        }

        $this->a_parts['insert'] = 'INSERT ' . ($ignore ? 'IGNORE':'') .' INTO ' . $this->s_table_name . ' ';

        $query = [];

        if (is_array($this->a_fields)) {
            $count = count($this->a_fields);

            end($this->a_fields);
            $last_key = key($this->a_fields);
            $count_for_values = 1;
            if ($count == 1) {
                $i = 1;
                foreach ($this->a_fields as $k => $v) {
                    $query[$last_key + $i] = '(`' . $k . '`) ';
                    $query[$last_key + $i + $count + $count_for_values] = '(:' . $k . ');';
                    $this->a_bind += [':' . $k => $v];
                }
            } else {
                $i = 1;
                foreach ($this->a_fields as $k => $v) {
                    if ($i == 1) {
                        $query[$last_key + $i] = '(`' . $k . '`, ';
                        $query[$last_key + $i + $count + $count_for_values] = '(:' . $k . ', ';
                    } else if ($i == $count) {
                        $query[$last_key + $i] = '`' . $k . '`) ';
                        $query[$last_key + $i + $count + $count_for_values] = ':' . $k . ');';
                    } else {
                        $query[$last_key + $i] = '`' . $k . '`, ';
                        $query[$last_key + $i + $count + $count_for_values] = ':' . $k . ', ';
                    }
                    $this->a_bind += [':' . $k => $v];
                    $i++;
                }
            }
        }

        $query[$last_key + $count + $count_for_values] = 'VALUES ';

        ksort($query);
        foreach($query as $part){
            $this->a_parts['insert'] .= $part;
        }

        $this->s_mode = 'insert';
        return $this;
    }

    /**
     * @param $object
     * @return $this
     */

    public function update($data)
    {
        $column_names = $this->getColumnNames($this->s_table_name);

        if (is_object($data)) {
            $values = get_object_vars($data);
        }else if(is_array($data)){
            $values = $data;
        }

        //проверка есть ли такое поле в таблице
        foreach ($values as $k => $v) {
            if (in_array($k, $column_names) && !in_array($k, $this->a_guarded) && $k !== 'id') {
                $this->a_fields[$k] = $v;
            }
        }

        $this->a_parts['update'] = 'UPDATE ' . $this->s_table_name . ' ';
        $this->a_parts['update'] .= 'SET ';

        if (is_array($this->a_fields)) {

            $count = count($this->a_fields);

            if ($count == 1) {
                foreach ($this->a_fields as $k => $v) {
                    $this->a_parts['update'] .= '`' . $k . ' = :' . $k . ' ';
                    $this->a_bind += [':' . $k => $v];
                }
            } else {
                $i = 1;
                foreach ($this->a_fields as $k => $v) {
                    if ($i == 1) {
                        $this->a_parts['update'] .= '`' . $k . '` = :' . $k . ', ';
                    } else if ($i == $count) {
                        $this->a_parts['update'] .= '`' . $k . '` = :' . $k . ' ';
                    } else {
                        $this->a_parts['update'] .= '`' . $k . '` = :' . $k . ', ';
                    }
                    $this->a_bind += [':' . $k => $v];
                    $i++;
                }
            }
        }

        $this->s_mode = 'update';
        return $this;
    }

    /**
     * @param $s_sql
     * @return $this
     */
    public function sql($s_sql)
    {
        $this->a_parts[] = $s_sql;
        return $this;
    }


    /**
     * @return array
     */
    public function getTables()
    {
        $stmt = self::getPDO($this->i_connection)->prepare('SHOW TABLES');
        $stmt->execute();

        $table_names = [];
        foreach ($stmt->fetchAll() as $table) {
            $table_names[] = $table[0];
        }
        return $table_names;
    }

    /**
     * @return array
     */
    public function getColumnNames()
    {
        /**
         * написать проерку если вернулась пустой массив
         */

        $stmt = self::getPDO($this->i_connection)->prepare('SHOW COLUMNS FROM `' . $this->s_table_name . '`');
        $stmt->execute();

        foreach ($stmt->fetchAll() as $field) {
            $field_names[] = $field[0];
        }
        return $field_names;
    }

    /**
     * @return $this
     */
    public function execute()
    {
        $query = '';

        switch($this->s_mode){
            case 'select':
                $query = $this->a_parts['select'] . $this->a_parts['from'];

                foreach ($this->a_joins as $join) {
                    $query .= $join;
                }

                $query .= $this->a_parts['where'] . $this->a_parts['order'] . $this->a_parts['group'] . $this->a_parts['limit'];
                break;
            case 'insert':
                $query = $this->a_parts['insert'];
                break;
            case 'update':
                $query .= $this->a_parts['update'] . $this->a_parts['where'];
                break;
            case 'delete':
                $query .= $this->a_parts['delete'] . $this->a_parts['where'];
                break;
            case 'paginate':
                $query = $this->a_parts['select'];

                foreach ($this->a_joins as $join) {
                    $query .= $join;
                }

                $query .= $this->a_parts['where'] . $this->a_parts['order'] . $this->a_parts['group'];
                break;
            default:
                die('Ошибка');
                break;
        }

        $this->getCore()->debug->addQuery($query);
        $this->stmt = self::getPDO($this->i_connection)->prepare($query);

        //ловим ошибки
        $this->i_query_start = microtime(true);

        if (!$this->stmt->execute($this->a_bind)) {
            $error = $this->stmt->errorInfo();

            if (isset($error[1])) {
                // 1054 - Unknown column
                // 1064 - Syntax error
                // 1062 - Duplicate entry
                // 1048 - mot Null
                // 1046 - table does not exist
                // 1265 - Data truncated
                //if ($error[1] == 1062 || $error[1] ==  1048 || $error[1] ==  1265){

                echo $query  . '<br>';
                echo $error[2]  . '<br>';
                die();
                //$error_action = '500';
                //die((new ErrorController())->$error_action('Ошибка запроса к БД ' . $error[2] . '<br>' . $query));
            }
        }

        $this->i_query_end = microtime(true);
        $this->i_query_seconds = number_format(($this->i_query_end - $this->i_query_start), 5);

        $this->getCore()->debug->addQuery($this->i_query_seconds . ' seconds');

        return $this;
    }

    public function paginate($rules = null, $key = false, $per_page = 30)
    {
        $page = $this->getCore()->request->request('page') ? $this->getCore()->request->request('page') : 1;

        $limit = (($page - 1) * $per_page);
        $offset = $per_page;

        $this->a_parts['limit'] = ' LIMIT ' . $limit . ', ' . $offset;

        $this->s_mode = 'select';

        $collection = $this
            ->with($this->a_relations)
            ->execute()
            ->all($rules, $key)
            ->getResult();

        $this->a_parts['select'] = 'SELECT COUNT(*) AS count FROM ' . $this->s_table_name . ' ';

        $this->s_mode = 'paginate';

        $count = $this
            ->execute()
            ->one()
            ->getResult()
            ->count;

        $pages = ceil($count / $per_page);

        return ['items' => $collection, 'count' => $count, 'pages' => $pages];
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->a_fields;
    }

    /**
     * @param null $rules
     * @return $this
     */
    
    public function with($array)
    {
        $this->a_relations = $array;
        return $this;
    }

    public function one($rules = null)
    {
        $class = $this->s_class_name;

        $objects = $this->stmt->fetchAll(PDO::FETCH_CLASS, $class);
        $collection = new Collection();

        foreach ($objects as $key => $object) {
            $object->query_seconds = $this->i_query_seconds;
            $collection->appendOne($object->afterSelect($rules), $object->id);
            break;
        }

        foreach($this->a_relations as $relation){
            $class::$relation($collection, ['type' => 'one', 'rules' => $rules]);
        }

        $this->stmt->closeCursor();

        return $collection;
    }

    /**
     * @param null $rules
     * @param null $key
     * @return Collection
     *
     * либо по порядку, либо по указанному свойству объекта
     */

    public function all($rules = null, $key = false)
    {
        $class = $this->s_class_name;
        $objects = $this->stmt->fetchAll(PDO::FETCH_CLASS, $class);
        $collection = new Collection();

        $i = 0;
        foreach ($objects as $object) {
            $object->query_seconds = $this->i_query_seconds;
            if($key){
                $collection->appendToArray($object->afterSelect($rules), $object->$key);
            }else{
                $collection->appendToArray($object->afterSelect($rules), $i);
                $i++;
            }
        }

        foreach($this->a_relations as $relation){
            $class::$relation($collection, ['type' => 'all', 'rules' => $rules]);
        }

        $this->stmt->closeCursor();

        return $collection;
    }

    /**
     * @param $field_name
     * @param string $sort
     * @return $this
     */

    public function order($field_name, $sort = 'ASC')
    {
        if(empty($this->a_parts['order'])){
            $this->a_parts['order'] = ' ORDER BY ' . $field_name . ' ' . $sort . ' ';
        }else{
            $this->a_parts['order'] .= ', ' . $field_name . ' ' . $sort . ' ';
        }
        return $this;
    }

    /**
     * @param int $start
     * @param int $offest
     * @return $this
     */

    public function limit($start = 0, $offest = 1)
    {
        $this->a_parts['limit'] = ' LIMIT ' . $start . ', ' . $offest;
        return $this;
    }

    /**
     * @param $field_name
     * @return $this
     */

    public function group($field_name)
    {
        $this->a_parts['group'] = ' GROUP BY ' . $field_name;
        return $this;
    }

    /**
     * @return mixed
     */

    public function getlastInsertId()
    {
        $last_inserted_id = self::getPDO($this->i_connection)->lastInsertId();
        return $last_inserted_id;
    }

    //rowCount() количество задетых строк

    /**
     *
     */

    public function dump()
    {
        $dump_dir = ABS . '/tmp/schema/'; // директория, куда будем сохранять резервную копию БД

        foreach($this->getTables() as $table){
            $fp = fopen( $dump_dir."/".$table[0].".sql", "a" );



        }


        /*while( $table = mysql_fetch_row($res) )
        {
            $fp = fopen( $dump_dir."/".$table[0].".sql", "a" );
            if ( $fp )
            {
                $query = "TRUNCATE TABLE `".$table[0]."`;\n";
                fwrite ($fp, $query);
                $rows = 'SELECT * FROM `'.$table[0].'`';
                $r = mysql_query($rows) or die("Ошибка при выполнении запроса: ".mysql_error());
                while( $row = mysql_fetch_row($r) )
                {
                    $query = "";
                    foreach ( $row as $field )
                    {
                        if ( is_null($field) )
                            $field = "NULL";
                        else
                            $field = "'".mysql_escape_string( $field )."'";
                        if ( $query == "" )
                            $query = $field;
                        else
                            $query = $query.', '.$field;
                    }
                    $query = "INSERT INTO `".$table[0]."` VALUES (".$query.");\n";
                    fwrite ($fp, $query);
                }
                fclose ($fp);
            }
        }*/
    }

}