<?php
namespace core;

use app\models\user\User;
use core\helper\Hash;
use core\Core;

class Auth
{
    private $session;
    private $cookie;

    public function __construct()
    {
        $this->sessionStart();
    }

    public function getCore()
    {
        return Core::getCore();
    }

    public function sessionStart()
    {
        //PHP_SESSION_DISABLED - 0 if sessions are disabled.
        //PHP_SESSION_NONE - 1 if sessions are enabled, but none exists.
        //PHP_SESSION_ACTIVE - 2 if sessions are enabled, and one exists.

        switch(session_status()){
            case PHP_SESSION_DISABLED:
                //0 if sessions are disabled
                die('Механизм сессий выключен на сервере');
                break;
            case PHP_SESSION_NONE:
                //1 if sessions are enabled, but none exists
                $config = Config::get();
                switch($config['session_handler']){
                    case 'files':
                        //проверяем существует ли сессия
                        $session_path = ABS . '/tmp/sessions';

                        if (is_writable($session_path) && is_readable($session_path)) {
                            session_save_path($session_path);
                        }else{
                            die('Директория с сессиями недоступна для записи или для чтения');
                        }
                        break;
                    case 'mysql':

                        break;
                    case 'redis':
                        session_set_save_handler('redis');
                        session_save_path('tcp://localhost:6379');
                        break;
                }

                session_name('minicart');
                session_start();
                $this->session =& $_SESSION;
                $this->cookie =& $_COOKIE;
                $this->is_auth = $this->isAuth();
                break;
            case PHP_SESSION_ACTIVE:
                //2 if sessions are enabled, and one exists
                break;
            default:
                die('Механизм сессий не работает');
        }
    }

    //функция проверки авторизованности
    public function isAuth()
    {
        //проверяем сессию
        if(isset($this->session['auth']) && $this->session['auth'] == 1){
            $this->loginById($this->session['id']);
            return true;
        //проверяем куку
        }else if(isset($this->cookie['auth'])){
            //режем куку
            $data = explode(':', $this->cookie['auth']);

            //получаем логин
            $id = $data[0];
            //получаем hash
            $cookie_hash = $data[1];

            //получаем пользователя
            $user = (new User())
                ->query()
                ->select()
                ->where('id = :id', [':id' => $id])
                ->limit()
                ->execute()
                ->one()
                ->getResult();

            //получем hash пользователя
            $password_hash = $user->password;
            $cookie_hash_user = sha1($user->auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $user->last_login_dt . ':' . $password_hash);

            //проверям есть ли такой пользователь и совпадаeт ли hash
            if($user && $cookie_hash == $cookie_hash_user){
                //логиним пользователя по куке
                $this->loginById($id);
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    public function login(User $user)
    {
        //текущая дата
        $current_time = date('Y-m-d H:i:s');

        //проверка нашёлся ли пользователь
        if($user){
            if($user->isAdmin()){
                $this->session['admin'] = 1;
            }
            $this->session['auth'] = 1;
            $this->session['id'] = $user->id;

            //устанавливаем куку
            $auth_key = uniqid();
            setcookie('auth', $user->id . ':' . sha1($auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $current_time . ':' . $user->password), time() + 60*60*60*24, '/');

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $user->last_login_dt = $current_time;
            $user->auth_key = $auth_key;
            $user->update();

            //записываем текущего пользователя
            $this->user = $user;
            return true;
        }else{
            $this->session['auth'] = 0;
            return false;
        }
    }

    public function loginById($id)
    {
        //текущая дата
        $current_time = date('Y-m-d H:i:s');

        //ищем пользователя по логину
        $user = (new User())
                    ->query()
                    ->select()
                    ->where('id = :id', [':id' => $id])
                    ->limit()
                    ->execute()
                    ->one()
                    ->getResult();

        //проверка нашёлся ли пользователь
        if($user){
            if($user->isAdmin()){
                $this->session['admin'] = 1;
            }
            $this->session['auth'] = 1;
            $this->session['id'] = $user->id;

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $auth_key = uniqid();
            setcookie('auth', $user->id . ':' . sha1($auth_key . ':' . $_SERVER['REMOTE_ADDR'] . ':' . $current_time . ':' . $user->password), time() + 60*60*24, '/');

            //записываем данные в пользователя для дальнейшей авторизации по куке
            $user->last_login_dt = $current_time;
            $user->auth_key = $auth_key;
            $user->update();

            //$this->user = $user;
            return true;
        }else{
            $this->session['auth'] = 0;
            return false;
        }
    }

    public function logout()
    {
        setcookie('auth', null, 0, '/');
        session_destroy();
    }

    public function getUserField($field)
    {
        /*if(static::isAuth()){
            $result = (new User())->findByUsername($_SESSION['username'])->$field;
            return $result;
        }else{
            return false;
        }*/
    }
}