<?php
namespace core;

use app\models\material\MaterialMenu;
use core\helper\Redirect;
use app\models\Banner;
use app\models\category\Category;

abstract class Controller
{
    public function __construct()
    {
        $this->redirect = new Redirect();
        $this->design = new Design();
    }

    /**
     * @return Core
     */

    public function getCore()
    {
        return Core::getCore();
    }

    public function index()
    {

    }

    public function view($id)
    {

    }

    public function delete($id)
    {

    }

    public function insert()
    {

    }

    public function update($id)
    {

    }

    abstract public function render($path_to_tpl, $layout);
}