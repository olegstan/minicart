<?php
namespace app\models\menu;

use app\layer\LayerModel;

class MenuModule extends LayerModel
{
    protected $table = 'mc_menu_modules';
}