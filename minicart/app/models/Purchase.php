<?php
namespace app\models;

use app\layer\LayerModel;

class Purchase extends LayerModel
{
    public $table = 'mc_purchases';
}