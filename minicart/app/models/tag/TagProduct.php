<?php
namespace app\models\tag;

use app\layer\LayerModel;

class TagProduct extends LayerModel
{
    protected $table = 'mc_tags_products';
}