<?php
namespace app\models\tag;

use app\layer\LayerModel;

class TagSet extends LayerModel
{
    protected $table = 'mc_tags_sets';
}