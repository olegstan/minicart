<?php
namespace app\models\image;

use app\layer\LayerModel;

class ImageModule extends LayerModel
{
    protected $table = 'mc_images_modules';
}