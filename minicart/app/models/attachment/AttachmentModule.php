<?php
namespace app\models\attachment;

use app\layer\LayerModel;

class AttachmentModule extends LayerModel
{
    protected $table = 'mc_attachments_modules';
}