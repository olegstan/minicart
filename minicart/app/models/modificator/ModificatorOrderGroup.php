<?php
namespace app\models\modificator;

use app\layer\LayerModel;

class ModificatorOrderGroup extends LayerModel
{
    protected $table = 'mc_modificators_orders_groups';
}