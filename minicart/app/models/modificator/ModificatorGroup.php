<?php
namespace app\models\modificator;

use app\layer\LayerModel;

class ModificatorGroup extends LayerModel
{
    protected $table = 'mc_modificators_groups';
}