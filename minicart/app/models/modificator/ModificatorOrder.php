<?php
namespace app\models\modificator;

use app\layer\LayerModel;

class ModificatorOrder extends LayerModel
{
    protected $table = 'mc_modificators_orders';
}