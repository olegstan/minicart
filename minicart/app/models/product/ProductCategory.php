<?php
namespace app\models\product;

use app\layer\LayerModel;

class ProductCategory extends LayerModel
{
    protected $table = 'mc_products_categories';
}