<?php
namespace app\models\product;

use app\layer\LayerModel;

class ProductRelated extends LayerModel
{
    protected $table = 'mc_products_related';
}