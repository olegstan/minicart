<?php
namespace app\models\user;

use app\layer\LayerModel;

class UserGroup extends LayerModel
{
    protected $table = 'mc_access_groups';

    public $group_name;

    public function afterSelect()
    {
        $this->group_name = $this->group_name ? $this->group_name : $this->name;

        return $this;
    }
}
