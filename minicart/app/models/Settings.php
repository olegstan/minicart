<?php
namespace app\models;

use app\layer\LayerModel;

class Settings extends LayerModel
{
    protected $table = 'mc_settings';
}