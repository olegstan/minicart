<?
namespace app\asset;

use app\layer\LayerAssetBundle;

class BodyAssetBundle extends LayerAssetBundle
{
    public $headerCSS = [

    ];

    public $footerJS = [

    ];
}