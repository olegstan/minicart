<?php
namespace app\controllers;

use app\layer\LayerController;
use app\models\group\GroupCategoryMain;
use app\models\group\GroupProductMain;
use app\models\material\MaterialCategory;
use app\models\Slideshow;
use app\models\Brand;
use app\models\material\Material;
use app\models\material\MaterialMenuItem;
use core\helper\Response;

class IndexController extends LayerController
{
    function index()
    {
        $this->getCore()->asset->addHeaderCSS('/libraries/swiper/dist/css/swiper.min.css');
        $this->getCore()->asset->addFooterJS('/libraries/swiper/dist/js/swiper.min.js');
        $this->getCore()->asset->addFooterJS('/libraries/matchHeight/jquery.matchHeight-min.js');

        $main_menu_item = (new MaterialMenuItem())
            ->query()
            ->with(['materials'])
            ->select()
            ->where('is_main = 1 AND object_type = :object_type',
                [
                    ':object_type' => 'material'
                ]
            )
            ->limit()
            ->execute()
            ->one()
            ->getResult();

        if ($main_menu_item->material) {
            $this->design->assign('main_menu_item', $main_menu_item);

            $this->getCore()->title = $main_menu_item->material->meta_title;
            $this->getCore()->meta_keywords = $main_menu_item->material->meta_keywords;
            $this->getCore()->meta_description = $main_menu_item->material->meta_description;
        }

        $groups_products_main = (new GroupProductMain())
            ->query()
            ->with(['products'])
            ->select()
            ->where('is_visible = :is_visible', [':is_visible' => 1])
            ->order('position')
            ->execute()
            ->all(null, 'id');

        $this->design->assign('groups_products_main', $groups_products_main);

        $groups_categories_main = (new GroupCategoryMain())->getRelatedCategories();
        $this->design->assign('groups_categories_main', $groups_categories_main);

        $slides = (new Slideshow())->getSlides();
        $this->design->assign('slides', $slides);

        $brands_popular = (new Brand())
            ->query()
            ->select()
            ->where('is_visible = :is_visible AND is_popular = :is_popular', ['is_visible' => 1, 'is_popular' => 1])
            ->order('name')
            ->execute()
            ->all(null, 'id')
            ->getResult();

        $brands_popular_count = 0;
        foreach($brands_popular as $b){
            $brands_popular_count += $b->count;
        }

        $brands_all = (new Brand())
            ->query()
            ->select()
            ->where('is_visible = :is_visible', ['is_visible' => 1])
            ->order('name')
            ->execute()
            ->all(null, 'id')
            ->getResult();

        $brands_all_count = 0;
        foreach($brands_all as $b){
            $brands_all_count += $b->count;
        }


        $this->design->assign('brands_popular', $brands_popular);
        $this->design->assign('brands_all', $brands_all);

        $this->design->assign('brands_popular_count', $brands_popular_count);
        $this->design->assign('brands_all_count', $brands_all_count);

        $news_category = (new MaterialCategory())->getMaterials('id = 1');
        $articles_category = (new MaterialCategory())->getMaterials('id = 3');

        $this->design->assign('news_category', $news_category);
        $this->design->assign('articles_category', $articles_category);

        /**************************************************/

        return Response::html($this->render(TPL . '/' . $this->getCore()->tpl_path .'/html/main.tpl'));
    }

    public function typography()
    {
        return Response::html($this->render(TPL . '/' . $this->getCore()->tpl_path .'/html/typography.tpl'));
    }

    public function callback()
    {
        /*$user_id = isset($this->user) ? $this->user->id : 0;
        $phone = array_key_exists('phone_number', $this->params_arr) ? $this->params_arr['phone_number'] : '';
        $match_res = preg_match("/^[^\(]+\(([^\)]+)\).(.+)$/", $phone, $matches);
        $user_name = array_key_exists('user_name', $this->params_arr) ? $this->params_arr['user_name'] : '';
        $call_time = array_key_exists('call_time', $this->params_arr) ? $this->params_arr['call_time'] : '';
        $message = array_key_exists('message', $this->params_arr) ? $this->params_arr['message'] : '';
        $result = false;

        if ($match_res && count($matches) == 3) {
            $callback = new StdClass;
            $callback->user_id = $user_id;
            $callback->user_name = $user_name;
            $callback->phone_code = $matches[1];
            $callback->phone = str_replace("-", "", $matches[2]);
            $callback->call_time = $call_time;
            $callback->message = $message;
            $callback->ip = $_SERVER['REMOTE_ADDR'];

            $callback->id = $this->callbacks->add_callback($callback);

            //Отправляем письмо админку, т.к. у пользователя мы не знаем почту
            $this->notify_email->email_callback($callback->id);
            //Отправляем смс админу
            $this->notify_email->sms_callback($callback->id);

            //$this->db->query("INSERT INTO __callbacks(user_id, phone_code, phone, call_time, message) VALUES(?,?,?,?,?)", $user_id, $matches[1], str_replace("-","",$matches[2]), $call_time, $message);
            $result = true;
        }

        return Response::json($result);*/
    }
}