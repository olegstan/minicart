<?php
namespace app\layer;

use core\Model;
use core\db\Database;

class LayerModel extends Model
{
    static $db;

    public static function getDB(){
        if(self::$db == null){
            self::$db = new Database();
        }
        return self::$db;
    }
}