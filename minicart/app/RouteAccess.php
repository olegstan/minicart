<?php
namespace app;

use app\controllers\ErrorController;
use core\Core;

class RouteAccess
{
    //header('Access-Control-Allow-Origin: *');
    //header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    //header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token'); // allow certain headers

    public function __construct()
    {
        $this->routes = $this->getCore()->request->routes;

        $this->rewriteRoute();


    }

    public function getCore()
    {
        return Core::getCore();
    }

    public function routes()
    {
        /*return [
            'GET' => [
                'main' => [
                    'index' => [
                        'name' => 'index'
                    ],
                    'services' => [
                        'name' => 'services'
                    ],
                    'contacts' => [
                        'name' => 'contacts'
                    ],
                    'typography' => [
                        'name' => 'typography'
                    ],
                    'test' => [
                        'name' => 'test'
                    ],
                    'test1' => [
                        'name' => 'test1'
                    ]
                ],
                'blog' => [
                    'index' => [
                        'name' => 'index'
                    ],
                    'create' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'view' => [
                        'name' => 'view',
                        'required' => [
                            'param' => true
                        ]
                    ],
                    'edit' => [
                        'name' => 'view',
                        'required' => [
                            'auth' => true,
                            'param' => true
                        ]
                    ]
                ],
                'user' => [
                    //'index' => 'index',
                    'login' => [
                        'name' => 'login'
                    ],
                    'register' => [
                        'name' => 'register'
                    ],
                    'logout' => [
                        'name' => 'logout',
                        'required' => [
                            'auth' => true
                        ],
                    ],
                    'change-password' => [
                        'name' => 'change-password',
                        'required' => [
                            'auth' => true
                        ],
                    ],
                    'forget-password' => [
                        'name' => 'forget-password',
                        'required' => [
                            'auth' => true
                        ],
                    ]
                ],
                'auth' => [
                    'vk' => [
                        'name' => 'vk'
                    ]
                ],
                'config' => [
                    'index' => [
                        'name' => 'index',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'sitemap' => [
                        'name' => 'sitemap',
                        'required' => [
                            'auth' => true
                        ]
                    ]
                ],
                'portfolio' => [
                    'view' => [
                        'name' => 'view',
                        'required' => [
                            'param' => true
                        ]
                    ]
                ],
                'ajax' => [
                    'template' => [
                        'name' => 'template',
                        'required' => [
                            'ajax' => true
                        ]
                    ],
                    'test' => [
                        'name' => 'test',
                        'required' => [
                            'ajax' => true
                        ]
                    ]
                ]
            ],
            'POST' => [
                'ajax' => [
                    'form-feedback' => [
                        'name' => 'form-feedback',
                        'required' => [
                            'refer' => 'https://uiweb.ru/'
                        ]
                    ],
                    'form-backcall' => [
                        'name' => 'form-backcall',
                        'required' => [
                            'refer' => 'https://uiweb.ru/main/contacts'
                        ]
                    ],
                    'form-login' => [
                        'name' => 'form-login'
                    ],
                    'form-register' => [
                        'name' => 'form-register'
                    ],
                    'form-change-password' => [
                        'name' => 'form-change-password'
                    ]
                ],
                'blog' => [
                    'create' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true
                        ]
                    ],
                    'edit' => [
                        'name' => 'create',
                        'required' => [
                            'auth' => true,
                            'param' => true
                        ]
                    ]
                ]
            ],
            'PUT',
            'PATCH',
            'TRACE',
            'DELETE',
            'HEAD',
            'OPTIONS'
        ];*/

        return [
            'REQUEST' => [
                'GET' => [
                    'product' => [
                        'view' => [
                            'name' => 'view'
                            /*'required' => [
                                //'param' => true
                            ]*/
                        ]
                    ]
                ],
                'POST' => [

                ],
                'PUT',
                'PATCH',
                'TRACE',
                'DELETE',
                'HEAD',
                'OPTIONS'
            ],
            'CLI' => [

            ]
        ];
    }

    public function rewriteRoute()
    {

        if($this->getCore()->request->is_admin){

            if($this->getCore()->request_uri == '/admin'){
                $this->getCore()->redirect->redirect('/admin/', 301);
            }

            switch($this->routes[0]){
                case 'login':
                    $this->routes[0] = 'user';
                    $this->routes[1] = 'login';
                    break;

            }
        }else{
            switch($this->routes[0]){
                case 'pages':
                    //если у нас нет слеша то routes_count будет равен 3
                    /**
                     * действие для вывода новости
                     */
                    $url_end = $this->getCore()->config['material_url_end'];
                    if(isset($this->routes[2])){
                        switch($url_end){
                            case '.html':
                            case '.htm':
                                $this->routes[2] = preg_replace('/\\' . $url_end . '$/', '', $this->routes[2]);
                                break;
                        }
                        $this->routes[1] = 'material';
                        //$this->routes[2] = $this->routes[2];
                        //$this->routes[0] = $this->routes[0];
                    /**
                     * действие для вывода категории новостей
                     */
                    }else{
                        /**
                         *проверяем что мы получили страницу или категорию
                         */
                        $is_material = preg_match('/\\' . $url_end . '$/', $this->routes[1]);

                        if($is_material){
                            switch($url_end){
                                case '.html':
                                case '.htm':
                                    $this->routes[2] = preg_replace('/\\' . $url_end . '$/', '', $this->routes[1]);
                                    break;

                            }
                            $this->routes[1] = 'material';
                        }else{
                            if($this->getCore()->request->routes_count === 3){
                                $this->getCore()->redirect->redirect('/' . implode('/', $this->routes) . '/', 301);
                            }
                            $this->routes[2] = $this->routes[1];
                            $this->routes[1] = 'category';
                        }
                    }
                    break;
                case 'product':
                    $this->routes[2] = $this->routes[1];
                    $url_end = $this->getCore()->config['product_url_end'];
                    switch($url_end){
                        case '.html':
                        case '.htm':
                            $this->routes[2] = preg_replace('/\\' . $url_end . '$/', '', $this->routes[2]);
                            break;
                    }
                    $this->routes[1] = 'product';
                    $this->routes[0] = 'catalog';
                    break;
                case 'catalog':
                    //если у нас нет слеша то routes_count будет равен 3
                    if($this->getCore()->request->routes_count === 3){
                        $this->getCore()->redirect->redirect('/' . implode('/', $this->routes) . '/', 301);
                    }
                    $this->routes[2] = $this->routes[1];
                    $this->routes[1] = 'catalog';
                    $this->routes[0] = 'catalog';
                    break;
                case 'brand':
                    //если у нас нет слеша то routes_count будет равен 3
                    if($this->getCore()->request->routes_count === 3){
                        $this->getCore()->redirect->redirect('/' . implode('/', $this->routes) . '/', 301);
                    }
                    $this->routes[2] = $this->routes[1];
                    $this->routes[1] = 'brand';
                    $this->routes[0] = 'catalog';
                    break;
            }
        }
    }

    public function check()
    {
        return $this->routes;

        //return [1 => 'product', 'view', 'asusmemopadfhd10me302c32gb'];

        /*$routes = $this->routes();

        if(isset($routes[$this->getCore()->method][$this->getCore()->controller][$this->getCore()->action_uri])){
            $current_action = $routes[$this->getCore()->method][$this->getCore()->controller][$this->getCore()->action_uri];

            //проверка на ajax и обязательность
            if(isset($current_action['required']['ajax'])){
                if(isset($_SERVER['HTTP_DETECT_AJAX']) || isset($_SERVER['HTTP_X_REQUESTED_WITH'])){

                }else{
                    $error = 403;
                    die((new ErrorController())->$error('Роут требует установки ajax хедера'));
                }
            }

            //проверка авторизации пользователя
            if(isset($current_action['required']['auth'])){
                if($this->getCore()->auth->is_auth){

                }else{
                    $error = '401';
                    die((new ErrorController())->$error('Попытка входа неавторизованного пользователя на станицу требующую авторизации'));
                }
            }


            if(isset($current_action['required']['refer'])){
                if($this->getCore()->referer == $current_action['required']['refer']){

                }else{
                    $error = '403';
                    die((new ErrorController())->$error('Попытка обратиться с неправильной страницы'));
                }
            }

            //проверка должны ли передаваться параметры в вызов иетода
            if(isset($current_action['required']['param'])){
                //количество роутов с парамметром
                $count_default_routes = $this->getCore()->language == $this->getCore()->default_language ? 4 : 5;
            }else{
                //количество роутов без парамметром
                $count_default_routes = $this->getCore()->language == $this->getCore()->default_language ? 3 : 4;

            }

            if(count($this->getCore()->routes) > $count_default_routes){
                $error = '404';
                die((new ErrorController())->$error('Допустимое количество роутов ' . $count_default_routes . ', a сейчас их ' . count($this->getCore()->routes)));
            }

        }else{
            $error = '404';
            die((new ErrorController())->$error('Ошибка путей, не найден роут'));
        }*/
    }

    /*
     rest api

    +-----------+---------------------------+---------+------------------+
    |   Verb    |           Path            | Action  |    Route Name    |
    +-----------+---------------------------+---------+------------------+
    | GET       | /resource                 | index   | resource.index   |
    | GET       | /resource/create          | create  | resource.create  |
    | POST      | /resource                 | store   | resource.store   |
    | GET       | /resource/{resource}      | show    | resource.show    |
    | GET       | /resource/{resource}/edit | edit    | resource.edit    |
    | PUT/PATCH | /resource/{resource}      | update  | resource.update  |
    | DELETE    | /resource/{resource}      | destroy | resource.destroy |
    +-----------+---------------------------+---------+------------------+



     */
}