var module = angular.module('app.minicart', ['ui.tree'])
    .config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]).controller("ProductController", ["$scope", "$http", "$location", function ProductController($scope, $http, $location) {
        var self = this;

        $scope.options = {
            dropped: function (e) {
                var position = e.dest.index;

                angular.forEach(e.dest.nodesScope.$modelValue, function(v, k) {
                    self.updateImagePosition(v.id, k);
                });
            }
        };

        self.changeTab = function(tab){
            $scope.tab = tab;
            self.setLocation();
        };

        self.setLocation = function () {
            $location.search({tab:$scope.tab});
        };

        self.getImages = function () {
            $scope.product_id = $('#product').data('object-id');
            $http({
                url: '/admin/product/product-images',
                method: "GET",
                params: {
                    product_id: $scope.product_id
                }
            }).success(
                function(response) {
                    if(response.result === 'success' && response.items){
                        $scope.images = response.items;
                    }
                },
                function(response) {

                }
            );
        };

        self.updateImagePosition = function(image_id, position) {
            $http({
                url: '/admin/image/update-image-position',
                method: "GET",
                params: {
                    image_id: image_id,
                    position: position
                }
            }).success(
                function(response) {
                    //$scope.items = self.tree(response);
                    //$scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        //self.selectImage = function (e) {
        //    console.log($('#upload').val());
        //    //console.log(e);
        //    //angular.element('#upload').trigger('click');
        //};

        $scope.uploadImage = function(files){
            angular.forEach(files, function(v, k){
                var position = $scope.images.length > 0 ? $scope.images.length : 0;
                var fd = new FormData();
                fd.append('file', v);
                fd.append('object_id', $scope.object_id);
                fd.append('module_id', $scope.module_id);
                fd.append('type', 'products');
                fd.append('width', 90);
                fd.append('height', 90);
                fd.append('position', position);

                $http.post('/admin/image/upload-image', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined}
                })
                .success(function(response){
                    if(response.result === 'success'){
                        $scope.images.push(response.image);
                    }
                })
                .error(function(){

                });
            });
        };

        /**
         * on drag and drop
         */
         $scope.dragEnterLeave = function(e){
             e.stopPropagation();
             e.preventDefault();
         };

        $scope.uploadImageByZone = function(e){
            e.preventDefault();
            e.stopPropagation();
            $scope.uploadImage(e.dataTransfer.files);
        };

        /**
         */

        self.uploadImageByUrl = function () {
            var position = $scope.images.length > 0 ? $scope.images.length : 0;
            var url_input = angular.element(document.getElementById('image-upload-url'));

            var fd = new FormData();
            fd.append('url', url_input.val());
            fd.append('object_id', $scope.object_id);
            fd.append('module_id', $scope.module_id);
            fd.append('type', 'products');
            fd.append('width', 90);
            fd.append('height', 90);
            fd.append('position', position);

            $http.post('/admin/image/upload-image-by-url', fd, {
                transformRequest: angular.identity,
                headers: {'Content-Type': undefined}
            })
                .success(function(response){
                    if(response.result === 'success'){
                        url_input.val('');
                        $scope.images.push(response.image);
                    }
                })
                .error(function(){

                });
        };

        self.clearUrlInput = function () {
            var url_input = angular.element(document.getElementById('image-upload-url'));
            url_input.val('');
        };

        self.deleteProductImage = function (scope, image_id) {
            $http({
                url: '/admin/image/delete-image',
                method: "GET",
                params: {
                    image_id: image_id,
                    type: 'products'
                }
            }).success(
                function(response) {
                    scope.removeNode();
                },
                function(response) {

                }
            );
        };

        self.deleteProduct = function () {

        };

        self.addVariant = function() {
            $http({
                url: '/admin/template/get-template',
                method: "GET",
                params: {
                    key: self.randomString(8, true),
                    path: '/system/html/product/variant.tpl'
                }
            }).success(
                function(response) {
                    if(response.result === 'success'){
                        $('#variants-list').append(response.template);
                    }else if(response.result === 'error'){

                    }
                    //console.log(response);
                },
                function(response) {

                }
            );
        };

        self.randomString = function randomString(length, is_unique) {
            var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'.split('');

            if (! length) {
                length = Math.floor(Math.random() * chars.length);
            }

            var str = '';
            for (var i = 0; i < length; i++) {
                str += chars[Math.floor(Math.random() * chars.length)];
            }
            if(is_unique === true){
                return str + (new Date()).getTime();
            }else{
                return str;
            }
        }


        $scope.removeVariant = function (elem) {
            $(elem).parent().parent().parent().remove();
        };

        $scope.removeTagGroup = function (elem) {
            $(elem).parent().parent().remove();
        };

        $scope.toggleVisibleVariant = function (elem) {
            var is_visible_input = $(elem).parent().parent().find('[data-field="is_visible"]');
            if($(elem).hasClass('light-on')){
                is_visible_input.val(0);
                $(elem).removeClass('light-on').addClass('light-off');
            }else{
                is_visible_input.val(1);
                $(elem).removeClass('light-off').addClass('light-on');
            }
        };

        self.addCategory = function(){
            $http({
                url: '/admin/template/get-template',
                method: "GET",
                params: {
                    path: '/system/html/product/category.tpl',
                    'variables[]': [
                        'all_categories'
                    ]
                }
            }).success(
                function(response) {
                    if(response.result === 'success'){
                        $('#categories-list').append(response.template);
                        $('.categories-dropdown-js').dropdown({duration: 0});
                    }else if(response.result === 'error'){

                    }
                },
                function(response) {

                }
            );
        };

        self.removeCategory = function(elem) {
            $(elem).parent().parent().parent().remove();
        };

        $scope.images = [];

        $('#brand_id').dropdown({duration: 0});
        $('.categories-dropdown-js').dropdown({duration: 0});

        $('.tags-dropdown-js').dropdown({
            duration: 0,
            allowAdditions: true
        });

        $scope.tab = $location.search().tab !== undefined ? $location.search().tab : 'main';
        $scope.object_id = $('[data-object-id]').data('object-id');
        $scope.module_id = 2;

        self.getImages();
    }]);

//$(function(){
//
//    $('#tablist li a').click(function(){
//        core.get_param['tab'] = $(this).attr('data-url');
//
//        location.search = core.path + '?' + $.param(core.get_param);
//
//        //location.href = '123';
//        //
//        //var currentUrl = window.location.href;
//        //var parsedUrl = $.url(currentUrl);
//        //var params = parsedUrl.param();
//        //params["tab"] = $(this).attr('data-url');
//        //
//        //var newUrl = "?" + $.param(params);
//
//
//        //var href = location;
//        //console.log(href);
//        //if (href.indexOf('?')==-1)
//        //    href += "?";
//        //else
//        //    href += "&";
//        //href += 'tab=' + $(this).attr('data-url');
//        //if (typeof history.pushState != undefined) {
//        //    history.pushState(null,null,encodeURI(decodeURI(href)));
//        //}
//    });
//
//    var imgList = $('#img-list');
//    var dropBox = $('#img-container');
//    var fileInput = $('#uploadImageField');
//
////    $(document).ready(function(){
////        fileInput.damnUploader({
////            url: '{$config->root_url}{$module->url}',
////            fieldName:  'uploaded-images',
////            object_id: '{if $object->id}{$object->id}{else}{$temp_id}{/if}',
////            dropBox: dropBox,
////            limit: {if $limit_images}{$limit_images}{else}10{/if},
////            onSelect: function(file) {
////                $('#upload-anim').removeClass('hidden');
////                var addedItem = addFileToQueue(file);
////                fileInput.damnUploader('startUploadItem', addedItem.queueId);
////                return false;
////            },
////            onAllComplete: function(){
////                $('#upload-anim').addClass('hidden');
////
////                fileInput.damnUploader('cancelAll');
////                imgList.html('');
////
////                {if $object->id}
////                    href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_images', 'object'=> $images_object_name, 'ajax'=>1]}';
////                    {else}
////                    href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_images', 'object'=> $images_object_name, 'ajax'=>1]}';
////                    {/if}
////                        $.get(href, function(data) {
////                            if (data.success)
////                            {
////                                $('div.main-object-images').html(data.data);
////                            }
////                        });
////                        return false;
////                    },
////                });
////
////                // Обработка событий drag and drop при перетаскивании файлов на элемент dropBox
////                dropBox.bind({
////                    dragenter: function() {
////                        $(this).addClass('highlighted');
////                        return false;
////                    },
////                    dragover: function() {
////                        return false;
////                    },
////                    dragleave: function() {
////                        $(this).removeClass('highlighted');
////                        return false;
////                    }
////                });
////
////                $("#upload-all").click(function() {
////                    fileInput.damnUploader('startUpload');
////                    return false;
////                });
////
////                $("ul#list1").dragsort({
////                    dragSelector: "div",
////                    placeHolderTemplate: "<li class='placeHolder'><div></div></li>",
////                });
////
////                $(".fancybox").fancybox({
////                    prevEffect    : 'none',
////                    nextEffect    : 'none',
////                    helpers    : {
////                        title    : {
////                            type: 'outside'
////                        },
////                        thumbs    : {
////                            width    : 50,
////                            height    : 50
////                        },
////                        overlay : {
////                            locked     : false
////                        }
////                    }
////                });
////            });
////
////            function updateProgress(bar, value) {
////                bar.css('width', value+'%');
////            }
////
////            function addFileToQueue(file) {
////
////                // Создаем элемент li и помещаем в него название, миниатюру и progress bar
////                var li = $('<li/>').appendTo(imgList);
////                var title = $('<div/>').text(file.name+' ').appendTo(li);
////                var cancelButton = $('<a/>').attr({
////                    href: '#cancel',
////                    title: 'отменить'
////                }).text('X').appendTo(title);
////
////                // Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
////                // обозначающий, что переданный параметр на самом деле лишь имитация настоящего File)
////                if(!file.fake)
////                {
////                    // Отсеиваем не картинки
////                    var imageType = /image.*/;
////                    if (!file.type.match(imageType)) {
////                        return true;
////                    }
////
////                    // Добавляем картинку и прогрессбар в текущий элемент списка
////                    var img = $('<img/>').appendTo(li);
////
////                    var pBar = $('<div class="progress progress-striped active"></div>').appendTo(li);
////                    var ppBar = $('<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>').appendTo(pBar);
////
////                    // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
////                    // инфу обо всех файлах (только в браузерах, поддерживающих FileReader)
////                    if($.support.fileReading) {
////                        var reader = new FileReader();
////                        reader.onload = (function(aImg) {
////                            return function(e) {
////                                aImg.attr('src', e.target.result);
////                                aImg.attr('width', 150);
////                            };
////                        })(img);
////                        reader.readAsDataURL(file);
////                    }
////                }
////
////                // Создаем объект загрузки
////                var uploadItem = {
////                    file: file,
////                    queueId: 0,
////                    onProgress: function(percents) {
////                        updateProgress(ppBar, percents);
////                    },
////                    onComplete: function(successfully, data, errorCode) {
////                        pBar.removeClass('active');
////                    },
////                };
////
////                // помещаем его в очередь
////                var queueId = fileInput.damnUploader('addItem', uploadItem);
////                uploadItem.queueId = queueId;
////
////                // обработчик нажатия ссылки "отмена"
////                cancelButton.click(function() {
////                    fileInput.damnUploader('cancel', queueId);
////                    li.remove();
////                    return false;
////                });
////
////                return uploadItem;
////            }
////
////            $('div.main-object-images').on("click", "a.delete_image", function(){
////                var item = $(this).closest('li');
////                var href = $(this).attr('href');
////                $.get(href, function(data) {
////                    if (data.success)
////                    {
////                        item.fadeOut('fast').remove();
////                    }
////                });
////                return false;
////            });
////
////            $('#image-upload-internet').click(function(){
////                $('#internet-upload-box').show();
////                return false;
////            });
////
////            $('#image-upload-btn-cancel').click(function(){
////                $('#internet-upload-box').hide();
////                return false;
////            });
////
////            $('#image-upload-btn').click(function(){
////                var image_url = $('#image-upload-url').val();
////                if (image_url.length == 0)
////                    return false;
////                {if $object->id}
////                    var href = '{$config->root_url}{$module->url}?id={$object->id}&mode=upload_internet_image&object={$images_object_name}&ajax=1&image_url='+encodebase64(image_url);
////                    {else}
////                    var href = '{$config->root_url}{$module->url}?id={$temp_id}&mode=upload_internet_image&object={$images_object_name}&ajax=1&image_url='+encodebase64(image_url);
////                    {/if}
////                        $.get(href, function(data) {
////                            if (data.success)
////                            {
////                                {if $object->id}
////                                    var href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_images', 'object'=>$images_object_name, 'ajax'=>1]}';
////                                    {else}
////                                    var href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_images', 'object'=>$images_object_name, 'ajax'=>1]}';
////                                    {/if}
////                                        $.get(href, function(data) {
////                                            if (data.success)
////                                            {
////                                                $('div.main-object-images').html(data.data);
////                                            }
////                                        });
////                                    }
////                                });
////                                $('#image-upload-url').val('');
////                                $('#internet-upload-box').hide();
////                                return false;
////                            });
////
////// /
//////$('#show-analogs-group').click(function(){
//////    $('#div-analogs-group').removeClass('hidden');
//////    return false;
//////});
//////
//////$('#select-analogs-group').change(function(){
//////    $('input[name=change_analogs_group]').val(1);
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$('#create-analogs-group').click(function(){
//////    $('input[name=create_analogs_group]').val(1);
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$('input[name=use_variable_amount]').change(function(){
//////    if ($(this).val() == 1)
//////        $('#variable_amount_panel').show();
//////    else
//////        $('#variable_amount_panel').hide();
//////});
//////
//////$('input[name=modificators_mode]').change(function(){
//////    if ($(this).val() == 1)
//////    {
//////        $('#modificators div.checkbox').removeClass('disabled');
//////        $('#modificators div.checkbox input').attr('disabled', false);
//////    }
//////    else
//////    {
//////        $('#modificators div.checkbox').addClass('disabled');
//////        $('#modificators div.checkbox input').attr('disabled', true);
//////    }
//////});
//////
//////$('#tablist li a').click(function(){
//////    var href = "{$config->root_url}{$module->url}{url current_params=$current_params}";
//////    if (href.indexOf('?')==-1)
//////        href += "?";
//////    else
//////        href += "&";
//////    href += 'tab=' + $(this).attr('data-url');
//////    if (typeof history.pushState != undefined) {
//////        history.pushState(null,null,encodeURI(decodeURI(href)));
//////    }
//////});
//////
//////var search_ajax_context;
//////
////////запрос товаров на ajax'e
//////function products_request_ajax(params) {
//////
//////    if (params == undefined || params.length == 0) {
//////        $('#refreshpart').html('');
//////        return false;
//////    }
//////
//////    $('#products-spinner').show();
//////    var url = "{$config->root_url}/ajax/get_data.php?object=products&mode=product-addrelated";
//////    if (params != undefined && params.length > 0) {
//////        var index2 = 0;
//////        for (var index in params) {
//////            if (params[index].key != "url") {
//////                url += "&";
//////                url += params[index].key + "=" + params[index].value;
//////                index2 += 1;
//////            }
//////        }
//////    }
//////    var exception_ids = "{$product->id}";
//////    $('#related input[type=hidden]').each(function () {
//////        if (exception_ids.length > 0)
//////            exception_ids += ',';
//////        exception_ids += $(this).val();
//////    });
//////    var full_url = url + "&exception=" + exception_ids;
//////
//////    if (search_ajax_context != null)
//////        search_ajax_context.abort();
//////
//////    $('#refreshpart').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');
//////
//////    search_ajax_context = $.ajax({
//////        type: 'GET',
//////        url: full_url,
//////        success: function (data) {
//////            if (data.success) {
//////                $('#refreshpart').html(data.data);
//////                $('#refreshpart li:first').addClass('selected');
//////                $('#products-spinner').hide();
//////            }
//////            search_ajax_context = null;
//////        }
//////    });
//////    return false;
//////}
//////
//////{* нажатие Enter в поле поиска *}
//////$('#searchField').on('keydown', function (e) {
//////    if (e.keyCode == 13) {
//////        var curli = $('#refreshpart li.selected');
//////        curli.click();
//////        return false;
//////    }
//////});
//////
//////{* поиск keyword *}
//////$('#searchField').on('keyup', function (e) {
//////    var params = [];
//////
//////    //down or up
//////    if (e.keyCode == 40 || e.keyCode == 38) {
//////        var curli = $('#refreshpart li.selected');
//////
//////        if (e.keyCode == 40) {
//////            var nextli = curli.next();
//////            if (nextli.length == 0)
//////                nextli = $('#refreshpart li:first');
//////        }
//////        if (e.keyCode == 38) {
//////            var nextli = curli.prev();
//////            if (nextli.length == 0)
//////                nextli = $('#refreshpart li:last');
//////        }
//////
//////        curli.removeClass('selected');
//////        nextli.addClass('selected');
//////    }
//////    else {
//////        if ($(this).val().length > 0) {
//////            $('#refreshpart').show();
//////            $('#searchCancelButton').show();
//////            params.push({
//////                'key': 'keyword',
//////                'value': $(this).val()
//////            });
//////        }
//////        else {
//////            $('#refreshpart').hide();
//////            $('#searchCancelButton').hide();
//////        }
//////        if ($(this).val().length >= {$settings->search_min_lenght} || $(this).val().length == 0)
//////            products_request_ajax(params);
//////    }
//////    return false;
//////});
//////
//////$('#refreshpart').on('mouseenter', 'li', function () {
//////    $('#refreshpart li.selected').removeClass('selected');
//////    $(this).addClass('selected');
//////});
//////
//////// сброс keyword
//////$('#searchCancelButton').click(function () {
//////    $('#searchField').val('');
//////    products_request_ajax();
//////    $('#refreshpart').hide();
//////    $('#searchCancelButton').hide();
//////    return false;
//////});
//////
//////$('#related ol').on("click", ".fa-times", function () {
//////    $(this).closest('li').fadeOut('slow').remove();
//////    return false;
//////});
//////
//////$('#refreshpart').on("click", "li[data-id]", function () {
//////    var product_id = $(this).attr('data-id');
//////    var image_src = "";
//////    var image = $(this).find('div.apimage img');
//////    if (image.length > 0)
//////        image_src = image.attr('src');
//////    var product_name = $(this).find('span.apname').clone().children().remove().end().text();
//////
//////    //$('#related ol').append('<li class="dd-item dd3-item relatedproducts" data-id="'+product_id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="'+image_src+'"/></div><div class="relatedname"><input type="hidden" name="related_ids[]" value="'+product_id+'"/><a href="
//////    {$config->root_url}{$module->url}?id='+product_id+'" target="_blank">'+product_name+'</a></div></div></li>');
//////
//////    $('#no-related-products').hide();
//////
//////    $('#related ol').append('<li class="dd-item dd3-item relatedproducts ' + ($(this).hasClass('lampoff') ? 'lampoff' : '') + '" data-id="' + product_id + '"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="statusrelated">' + $(this).find('div.apstatus').html() + '</div><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="' + image_src + '"/></div><div class="relatedname"><input type="hidden" name="related_ids[]" value="' + product_id + '"/><a href="{$config->root_url}{$module->url}?id=' + product_id + '" target="_blank">' + product_name + '</a><div class="related-sky">' + $(this).find('span.apsky').html() + '</div></div></div></li>');
//////
//////    $(this).fadeOut('slow').remove();
//////    $('#searchField').val('');
//////    $('#refreshpart').html('');
//////    $('#refreshpart').hide();
//////    $('#searchCancelButton').hide();
//////
//////    $('#related').nestable({
//////        maxDepth: 1
//////    });
//////});
//////
//////{***** АНАЛОГИ *****}
//////
//////var search_ajax_context_analogs;
//////
////////запрос товаров на ajax'e
//////function products_request_analogs_ajax(params) {
//////
//////    if (params == undefined || params.length == 0) {
//////        $('#refreshpart_analogs').html('');
//////        return false;
//////    }
//////
//////    $('#products-spinner').show();
//////    {*var url = "{$config->root_url}{$products_module->url}";*}
//////    var url = "{$config->root_url}/ajax/get_data.php?object=products&mode=product-addrelated";
//////    if (params != undefined && params.length > 0) {
//////        var index2 = 0;
//////        for (var index in params) {
//////            if (params[index].key != "url") {
//////                url += "&";
//////                url += params[index].key + "=" + params[index].value;
//////                index2 += 1;
//////            }
//////        }
//////    }
//////    var exception_ids = "{$product->id}";
//////    $('#analogs input[type=hidden]').each(function () {
//////        if (exception_ids.length > 0)
//////            exception_ids += ',';
//////        exception_ids += $(this).val();
//////    });
//////    var full_url = url + "&exception=" + exception_ids;
//////
//////    if (search_ajax_context_analogs != null)
//////        search_ajax_context_analogs.abort();
//////
//////    $('#refreshpart_analogs').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');
//////
//////    search_ajax_context_analogs = $.ajax({
//////        type: 'GET',
//////        url: full_url,
//////        success: function (data) {
//////            if (data.success) {
//////                $('#refreshpart_analogs').html(data.data);
//////                $('#refreshpart_analogs li:first').addClass('selected');
//////                $('#products-spinner').hide();
//////            }
//////            search_ajax_context_analogs = null;
//////        }
//////    });
//////    return false;
//////}
//////
//////{* нажатие Enter в поле поиска *}
//////$('#searchField_analogs').on('keydown', function (e) {
//////    if (e.keyCode == 13) {
//////        var curli = $('#refreshpart_analogs li.selected');
//////        curli.click();
//////        return false;
//////    }
//////});
//////
//////{* поиск keyword *}
//////$('#searchField_analogs').on('keyup', function (e) {
//////    var params = [];
//////
//////    //down or up
//////    if (e.keyCode == 40 || e.keyCode == 38) {
//////        var curli = $('#refreshpart_analogs li.selected');
//////
//////        if (e.keyCode == 40) {
//////            var nextli = curli.next();
//////            if (nextli.length == 0)
//////                nextli = $('#refreshpart_analogs li:first');
//////        }
//////        if (e.keyCode == 38) {
//////            var nextli = curli.prev();
//////            if (nextli.length == 0)
//////                nextli = $('#refreshpart_analogs li:last');
//////        }
//////
//////        curli.removeClass('selected');
//////        nextli.addClass('selected');
//////    }
//////    else {
//////        if ($(this).val().length > 0) {
//////            $('#refreshpart_analogs').show();
//////            $('#searchCancelButton_analogs').show();
//////            params.push({
//////                'key': 'keyword',
//////                'value': $(this).val()
//////            });
//////        }
//////        else {
//////            $('#refreshpart_analogs').hide();
//////            $('#searchCancelButton_analogs').hide();
//////        }
//////        if ($(this).val().length >= {$settings->search_min_lenght} || $(this).val().length == 0)
//////            products_request_analogs_ajax(params);
//////    }
//////    return false;
//////});
//////
//////$('#refreshpart_analogs').on('mouseenter', 'li', function () {
//////    $('#refreshpart_analogs li.selected').removeClass('selected');
//////    $(this).addClass('selected');
//////});
//////
//////// сброс keyword
//////$('#searchCancelButton_analogs').click(function () {
//////    $('#searchField_analogs').val('');
//////    products_request_analogs_ajax();
//////    $('#refreshpart_analogs').hide();
//////    $('#searchCancelButton_analogs').hide();
//////    return false;
//////});
//////
//////$('#analogs ol').on("click", ".fa-times", function () {
//////    $(this).closest('li').fadeOut('slow').remove();
//////    return false;
//////});
//////
//////$('#refreshpart_analogs').on("click", "li[data-id]", function () {
//////    var product_id = $(this).attr('data-id');
//////    var image_src = "";
//////    var image = $(this).find('div.apimage img');
//////    if (image.length > 0)
//////        image_src = image.attr('src');
//////    var product_name = $(this).find('span.apname').clone().children().remove().end().text();
//////
//////    $('#no-analogs-products').hide();
//////
//////    $('#analogs ol').append('<li class="dd-item dd3-item relatedproducts ' + ($(this).hasClass('lampoff') ? 'lampoff' : '') + '" data-id="' + product_id + '"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="statusrelated">' + $(this).find('div.apstatus').html() + '</div><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="' + image_src + '"/></div><div class="relatedname"><input type="hidden" name="analogs_ids[]" value="' + product_id + '"/><a href="{$config->root_url}{$module->url}?id=' + product_id + '" target="_blank">' + product_name + '</a><div class="related-sky">' + $(this).find('span.apsky').html() + '</div></div></div></li>');
//////
//////    $(this).fadeOut('slow').remove();
//////    $('#searchField_analogs').val('');
//////    $('#refreshpart_analogs').html('');
//////    $('#refreshpart_analogs').hide();
//////    $('#searchCancelButton_analogs').hide();
//////
//////    $('#analogs').nestable({
//////        maxDepth: 1
//////    });
//////});
//////
//////
//////// infinity
//////$("input[name*=variant][name*=stock]").focus(function () {
//////    if ($(this).val() == '∞')
//////        $(this).val('');
//////    return false;
//////});
//////
//////$("input[name*=variant][name*=stock]").blur(function () {
//////    if ($(this).val() == '')
//////        $(this).val('∞');
//////});
//////
//////$(document).ready(function () {
//////
//////    {if $message_success}
//////        info_box($('#notice'), 'Изменения сохранены!', '');
//////        {/if}
//////
//////            {if $message_error}
//////                error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название товара не может быть пустым{/if}')
//////                {/if}
//////
//////                    $('#datepicker').datepicker({
//////                        format: "dd.mm.yyyy",
//////                        todayBtn: true,
//////                        language: "ru",
//////                        autoclose: true,
//////                        todayHighlight: true
//////                    });
//////
//////                    $("#taggroups tr[data-group] input[type=hidden]").each(function () {
//////                            $(this).select2({
//////                                tags: true,
//////                                separator: '#%#',
//////                                tokenSeparators: ['#%#'],
//////                                createSearchChoice: function (term, data) {
//////                                    if ($(data).filter(function () {
//////                                            return this.text.localeCompare(term) === 0;
//////                                        }).length === 0) {
//////                                        return {
//////                                            id: term,
//////                                            text: term
//////                                        };
//////                                    }
//////                                },
//////                                multiple: true,
//////                                ajax: {
//////                                    url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id=" + $(this).attr('data-group-id'),
//////                            {*url: "{$config->root_url}{$module->url}?id={$product->id}&mode=get_tags&ajax=1&group_id="+$(this).attr('data-group-id'),*}
//////                            dataType: "json",
//////                                data: function (term, page) {
//////                                return {
//////                                    keyword: term
//////                                };
//////                            },
//////                            results: function (data, page) {
//////                                return {
//////                                    results: data.data
//////                                };
//////                            }
//////                        },
//////                        initSelection: function (element, callback) {
//////                    var values = element.val().split('#%#');
//////                    var data = [];
//////
//////                    for (var i = 0; i < values.length; i++)
//////                        data.push({
//////                            id: values[i],
//////                            text: values[i]
//////                        });
//////
//////                    callback(data);
//////                }
//////                });
//////            });
//////
//////            $("#autotags").select2({
//////                tags: true,
//////                separator: '#%#',
//////                tokenSeparators: ["#%#"],
//////                multiple: true,
//////                initSelection: function (element, callback) {
//////                    var values = element.val().split('#%#');
//////                    var data = [];
//////
//////                    for (var i = 0; i < values.length; i++)
//////                        data.push({
//////                            id: values[i],
//////                            text: values[i],
//////                            locked: true
//////                        });
//////
//////                    callback(data);
//////                }
//////            });
//////
//////            $("#badges").select2({
//////                tags: true,
//////                separator: '#%#',
//////                tokenSeparators: ["#%#"],
//////                createSearchChoice: function (term, data) {
//////                    {*if ($(data).filter(function () {
//////                            return this.text.localeCompare(term) === 0;
//////                        }).length === 0) {
//////                        return {
//////                            id: term,
//////                            text: term
//////                        };
//////                    }*}
//////                },
//////                multiple: true,
//////                ajax: {
//////                    url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_badges",
//////            {*url: "{$config->root_url}{$module->url}?id={$product->id}&mode=get_badges&ajax=1",*}
//////            dataType: "json",
//////                data: function (term, page) {
//////            return {
//////                keyword: term
//////            };
//////        },
//////            results: function (data, page) {
//////                return {
//////                    results: data.data
//////                };
//////            }
//////        },
//////        initSelection: function (element, callback) {
//////            var values = element.val().split('#%#');
//////            var data = [];
//////
//////            for (var i = 0; i < values.length; i++)
//////                data.push({
//////                    id: values[i],
//////                    text: values[i]
//////                });
//////
//////            callback(data);
//////        }
//////    });
//////
////    $('#categories-list select[name="categories_ids[]"]').each(function () {
////
////            //$(this).select2({
////            //    minimumInputLength: 1,
////            //    query: function(options){
////            //        var term = options.term;
////            //        $.getJSON( "{$config->root_url}{$module->url}search_category/"+term, function( data, status, xhr ) {
////            //            options.callback({
////            //                results: data.data
////            //            });
////            //        });
////            //    },
////            //    initSelection: function(element, callback){
////            //        var data = {
////            //            id: element.val(),
////            //            text: element.attr('data-text')
////            //        };
////            //        callback(data);
////            //    }
////            //});
////
////            $(this).select2({
////                placeholder: "Выберите категорию",
////                allowClear: true
////            });
////    });
//////
//////    {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX)*}
//////    $('#brandselect').select2({
//////        placeholder: "Выберите бренд",
//////        allowClear: true
//////    });
//////
//////    $('#variants-list').nestable({
//////        maxDepth: 1
//////    });
//////
//////    $('#related').nestable({
//////        maxDepth: 1
//////    });
//////
//////    $('#analogs').nestable({
//////        maxDepth: 1
//////    });
//////});
//////
//////$('#seo-toggle').on('shown.bs.tab', function (e) {
//////    $('textarea[name=meta_keywords], textarea[name=meta_description]').autosize();
//////});
//////
//////$('#recreate-seo').click(function(){
//////    $(this).closest('form').find('input[name=recreate_seo]').val(1);
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$("form#product a.save").click(function () {
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$("form#product a.saveclose").click(function () {
//////    $(this).closest('form').find('input[name=close_after_save]').val(1);
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$("form#product a.saveadd").click(function () {
//////    $(this).closest('form').find('input[name=add_after_save]').val(1);
//////    $(this).closest('form').submit();
//////    return false;
//////});
//////
//////$('form#product').submit(function () {
//////    var ps = '';
//////    $("ul#list1 li").each(function () {
//////        if (ps.length > 0)
//////            ps += ",";
//////        ps += $(this).attr('data-id');
//////    });
//////
//////    if (ps.length > 0)
//////        $('form#product input[name=images_position]').val(ps);
//////
//////    var as = '';
//////    $("#files li").each(function () {
//////        if (as.length > 0)
//////            as += ",";
//////        as += $(this).attr('data-id');
//////    });
//////
//////    if (as.length > 0)
//////        $('form#product input[name=attachments_position]').val(as);
//////});
//////
//////$('#add_new_tag_group').click(function () {
//////    var ids = [];
//////    $('#taggroups tr[data-group]').each(function () {
//////        ids[ids.length] = $(this).attr('data-group');
//////    });
//////
//////    $.ajax({
//////        type: 'POST',
//////        url: '{$config->root_url}/ajax/get_data.php',
//////        data: {
//////            'object': 'products',
//////            'mode': 'get_tag_groups',
//////            'ids': ids,
//////            'session_id': '{$smarty.session.id}'
//////        },
//////    {*url: '{$config->root_url}{$module->url}?id={$product->id}&mode=get_tag_groups&ajax=1',
//////        data: {
//////        'ids': ids,
//////            'session_id': '{$smarty.session.id}'
//////    },*}
//////    success: function (data) {
//////        if (data.success) {
//////            $("#new_group_id").html(data.data);
//////            $("#new_tag_group").show();
//////        }
//////    },
//////    dataType: 'json'
//////});
//////});
//////
//////$('#new_group_id').change(function () {
//////    var group_id = $('#new_group_id option:selected').val();
//////    var group_name = $('#new_group_id option:selected').attr('data-name');
//////    var group_postfix = $('#new_group_id option:selected').attr('data-postfix');
//////    var group_mode = $('#new_group_id option:selected').attr('data-mode');
//////
//////    if (group_id == undefined || group_name == undefined || group_mode == undefined || group_id == 0 || group_name.length == 0 || group_mode.length == 0) {
//////        $("#new_tag_group").hide();
//////        return false;
//////    }
//////
//////    switch(group_mode){
//////        case "logical":
//////            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><div class="btn-group" data-toggle="buttons"><label class="btn btn-sm btn-default4 on"><input type="radio" name="product_tags[' + group_id + ']" value="да">  Да</label><label class="btn btn-sm btn-default4 off active"><input type="radio" name="product_tags[' + group_id + ']" value="нет" checked> Нет</label></div></td></tr>');
//////            break;
//////        case "text":
//////            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><textarea class="form-control" name="product_tags[' + group_id + ']" data-group-id="' + group_id + '"></textarea></td></tr>');
//////            break;
//////        default:
//////            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><input type="hidden" name="product_tags[' + group_id + ']" data-group-id="' + group_id + '" value=""></td></tr>');
//////            break;
//////    }
//////
//////    $('#taggroups tr[data-group=' + group_id + '] a[data-toggle=tooltip]').tooltip();
//////
//////    if (group_mode != "logical" && group_mode != "text")
//////        $('#taggroups tr[data-group=' + group_id + '] input[type=hidden]').select2({
//////            tags: true,
//////            separator: '#%#',
//////            tokenSeparators: ["#%#"],
//////            createSearchChoice: function (term, data) {
//////                if ($(data).filter(function () {
//////                        return this.text.localeCompare(term) === 0;
//////                    }).length === 0) {
//////                    return {
//////                        id: term,
//////                        text: term
//////                    }
//////                }
//////            },
//////            multiple: true,
//////            ajax: {
//////                url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id=" + group_id,
//////                dataType: "json",
//////                data: function (term, page) {
//////                    return {
//////                        keyword: term
//////                    };
//////                },
//////                results: function (data, page) {
//////                    return {
//////                        results: data.data
//////                    };
//////                }
//////            }
//////        });
//////
//////    $("#new_tag_group").hide();
//////    return false;
//////});
//////
//////$('#taggroups').on('click', '.delete-property', function () {
//////    $(this).closest('tr').remove();
//////    return false;
//////});
//////
//////{*$('#cancel_tag_group').click(function(){
//////    $("#new_tag_group").hide();
//////});
//////
//////    $('#add_tag_group').click(function(){
//////        var group_id = $('#new_group_id option:selected').val();
//////        var group_name = $('#new_group_id option:selected').attr('data-name');
//////
//////        if (group_id == undefined || group_name == undefined)
//////        {
//////            $("#new_tag_group").hide();
//////            return false;
//////        }
//////
//////        $('#taggroups tbody').append('<tr data-group="'+group_id+'"><td class="col-md-3">'+group_name+'</td><td class="col-md-9"><input type="hidden" name="product_tags['+group_id+']" data-group-id="'+group_id+'" value=""></td></tr>');
//////        $('#taggroups tr[data-group='+group_id+'] a[data-toggle=tooltip]').tooltip();
//////
//////        $('#taggroups tr[data-group='+group_id+'] input[type=hidden]').select2({
//////            tags: true,
//////            separator: '#%#',
//////            tokenSeparators: ["#%#"],
//////            createSearchChoice: function(term, data) {
//////                if ($(data).filter(function() {
//////                        return this.text.localeCompare(term) === 0;
//////                    }).length === 0) {
//////                    return {
//////                        id:term,
//////                        text: term
//////                    }
//////                }
//////            },
//////            multiple: true,
//////            ajax: {
//////                url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id="+group_id,
//////                dataType: "json",
//////                data: function(term, page) {
//////                    return {
//////                        keyword: term
//////                    };
//////                },
//////                results: function(data, page) {
//////                    return {
//////                        results: data.data
//////                    };
//////                }
//////            }
//////        });
//////
//////        $("#new_tag_group").hide();
//////        return false;
//////    });*}
//////
//////$('#add-category').click(function () {
//////    var li = $('<li></li>');
//////    var s = $('#new_parent_category_proto').clone();
//////    s.removeAttr('id');
//////    //s.removeAttr('style');
//////    s.show();
//////    s.attr('name', 'categories_ids[]');
//////    li.append(s);
//////    li.append('<div class="categories-right"><a href="#" class="delete-parent_category btn btn-sm btn-link"><i class="fa fa-times"></i></a><div>');
//////    $('#categories-list').append(li);
//////    {* ВЕРСИЯ С CHOOSEN
//////        /*s.chosen({
//////         allow_single_deselect: true,
//////         no_results_text: 'Ничего не найдено!'
//////         });*/*}
//////
//////    {* ВЕРСИЯ С SELECT2 (AJAX)
//////        s.select2({
//////            minimumInputLength: 0,
//////            query: function(options){
//////                var term = options.term;
//////                $.getJSON( "{$config->root_url}{$module->url}search_category/"+term, function( data, status, xhr ) {
//////                    options.callback({
//////                        results: data.data
//////                    });
//////                });
//////            }
//////        });*}
//////
//////    {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX)*}
//////    s.select2({
//////        placeholder: "Выберите категорию",
//////        allowClear: true
//////    });
//////
//////    return false;
//////});
//////
////$('#categories-list').on("click", ".delete-parent_category", function () {
////    $(this).closest('li').fadeOut('slow').remove();
////    return false;
////});
////
////$('#categories-list').on("click", ".show-modal-category-select", function () {
////    $('#modal-category-select').modal('show');
////    return false;
////});
//////
//////$('#add-variant').click(function () {
//////    //$('#variants-list').append('<li class="dd-item dd3-item" data-id=""><div class="dd-handle dd3-handle"></div><div class="dd3-content form-inline"><input type="text"  name="variants[name][]" placeholder="Название варианта" class="form-control variant-name"><input type="text" name="variants[sku][]" placeholder="Артикул" class="form-control variant-sku"><input type="text" name="variants[price][]" placeholder="Цена" class="form-control variant-price"><input type="text" name="variants[price_old][]" placeholder="Старая цена" class="form-control variant-old-price" title="Старая цена"><input type="text" name="variants[stock][]"  value="∞" placeholder="Количество" class="form-control variant-count" title="Количество товара на складе"><input type="hidden" name="variants[visible][]" value="1"/><div class="controllinks"><a class="delete-item" href=""><i class="fa fa-times"></i></a><a class="toggle-item light-on" href=""></a></div></div></li>');
//////    $('#variants-list ol').append('<li class="dd-item dd3-item" data-id=""><div class="dd-handle dd3-handle"></div><div class="dd3-content form-inline"> <input type="text"  name="variants[name][]" placeholder="Название варианта" class="form-control variant-name"> <input type="text" name="variants[sku][]" placeholder="Артикул" class="form-control variant-sku"> <input type="text" name="variants[price][]" placeholder="0" value="" class="form-control variant-price"> <input type="text" name="variants[price_old][]" placeholder="0" value="" class="form-control variant-old-price" title="Старая цена"> <input type="text" name="variants[stock][]"  value="∞" placeholder="Количество" class="form-control variant-count" title="Количество товара на складе"> <input type="hidden" name="variants[visible][]" value="1"/><div class="controllinks"><a class="delete-item" href=""><i class="fa fa-times"></i></a><a class="toggle-item light-on" href=""></a></div></div></li>');
//////    return false;
//////});
//////
//////$('#variants-list').on("click", ".delete-item", function () {
//////    $(this).closest('li').fadeOut('slow').remove();
//////    return false;
//////});
//////
//////$('#variants-list').on("click", ".toggle-item", function () {
//////    var input = $(this).closest('li').find('input[name^="variants[visible]"]');
//////    var new_val = 1 - parseInt(input.val());
//////    if (new_val) {
//////        input.val(1);
//////        $(this).removeClass('light-off');
//////        $(this).addClass('light-on');
//////        $(this).closest('li').removeClass('disabledvar');
//////    }
//////    else {
//////        input.val(0);
//////        $(this).removeClass('light-on');
//////        $(this).addClass('light-off');
//////        $(this).closest('li').addClass('disabledvar');
//////    }
//////    return false;
//////});
//////
//////$('#delete-product').click(function () {
//////
//////    confirm_popover_box($(this), function (obj) {
//////        $.ajax({
//////            type: 'GET',
//////            url: "{$config->root_url}{$module->url}{url add=['id'=>$product->id, 'mode'=>'delete', 'ajax'=>1]}",
//////            success: function (data) {
//////                document.location = "{$config->root_url}{$main_module->url}{url add=['category_id'=>$category_id, 'page'=>$page]}";
//////                return false;
//////            }
//////        });
//////    });
//////    return false;
//});