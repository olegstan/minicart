var module = angular.module('app.minicart', ['ui.tree'])
    .config(["$httpProvider", "$locationProvider", "$interpolateProvider", function($httpProvider, $locationProvider, $interpolateProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
        //$interpolateProvider.startSymbol('[[');
        //$interpolateProvider.endSymbol(']]');
    }]).controller("ProductController", ["$scope", "$http", "$location", function ProductController($scope, $http, $location) {
        var self = this;

        self.sort = function ($event, type) {
            var change_sort_type = false;

            if($scope.sort === type){
                change_sort_type = true;
            }

            if(change_sort_type){
                if($scope.sort_type === 'ASC'){
                    $scope.sort_type = 'DESC';
                }else{
                    $scope.sort_type = 'ASC';
                }
            }

            $scope.sort = type;

            self.setLocation();
            self.getItems();
        };

        self.prev = function () {
            if($scope.page > 1){
                $scope.page--;
                self.setLocation();
                self.getItems();
            }
        };

        self.next = function () {
            if($scope.page < $scope.pages_count){
                $scope.page++;
                self.setLocation();
                self.getItems();
            }
        };

        self.page = function (number) {
            $scope.page = number;
            self.setLocation();
            self.getItems();
        };

        self.pages = function () {
            self.setLocation();
            self.getItems();
        };

        self.setLocation = function () {
            $location.search({page:$scope.page, sort: $scope.sort, sort_type: $scope.sort_type, category_id: $scope.category_id});
        };

        self.getItems = function () {
            var spinner = document.getElementById('products-spinner')
            spinner.style.display = 'inline-block';

            $http({
                url: '/admin/paginate/products',
                method: "GET",
                params: {
                    page: $scope.page,
                    sort: $scope.sort,
                    sort_type: $scope.sort_type,
                    category_id: $scope.category_id
                }
            }).success(
                function(response) {
                    $scope.pages = [];
                    response.pages++;
                    for(var i = 1; i < response.pages; i++) {
                        $scope.pages.push(i);
                    }
                    $scope.pages_count = $scope.pages.length;
                    $scope.items = response.items;
                    for(i in $scope.items){
                        $scope.items_map[$scope.items[i].id] = $scope.items[i];
                    }
                    $scope.items_count = response.count;
                    spinner.style.display = 'none';


                    /**
                     * найти нормальное решение
                     */
                    setTimeout(function(){
                        $('.product-checkbox-js').checkbox();
                    }, 1);
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.tree = function (array) {
            var roots = [];
            for(var i in array) {
                node = array[i];
                $scope.categories_map[node.id] = node;
            }
            for(var i in array) {
                node = array[i];
                node.is_collapsed = 0;
                node.is_highlight = 1;
                node.is_visible = 1;
                if (node.parent_id !== "0") {
                    $scope.categories_map[node.parent_id].nodes.push(node);
                }else{
                    roots.push(node);
                }
            }

            for(var i in array) {
                node = array[i];
                if($scope.category_id === node.id){
                    self.expandParent(node.id);
                }
            }

            return roots;
        };

        self.collapseChild = function (category_id) {
            for(i in $scope.categories_map[category_id].nodes){
                $scope.categories_map[category_id].nodes[i].is_collapsed = 0;
                if($scope.categories_map[category_id].nodes[i].nodes.length > 0){
                    self.collapseChild($scope.categories_map[category_id].nodes[i].id);
                }
            }
        };

        self.getRootNodesScope = function() {
            return angular.element(document.getElementById("tree-root")).scope();
        };

        self.collapseAll = function() {
            var scope = self.getRootNodesScope();
            for(i in $scope.categories_list){
                $scope.categories_list[i].is_collapsed = 0;
            }
            scope.collapseAll();
        };

        self.expandParent = function (category_id) {
            $scope.categories_map[category_id].is_collapsed = 1;
            if($scope.categories_map[category_id].parent_id !== "0"){
                self.expandParent($scope.categories_map[category_id].parent_id);
            }
        };

        self.resize = function(){

        };
        
        self.toggleVisible = function (product_id) {
            $scope.items_map[product_id].is_visible = !$scope.items_map[product_id].is_visible;
            $http({
                url: '/admin/product/updateVisible',
                method: "GET",
                params: {
                    product_id: product_id,
                    is_visible: $scope.items_map[product_id].is_visible
                }
            }).success(
                function(response) {

                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.getCategories = function () {
            $http({
                url: '/admin/catalog/categories',
                method: "GET",
                params: {}
            }).success(
                function(response) {
                    for(i in response){
                        $scope.all_categories_count += parseInt(response[i].count);
                    }
                    $scope.categories_list = response;
                    $scope.categories = self.tree(response);
                    $scope.categories_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.toggle = function (scope) {
            scope.$parent.$modelValue.is_collapsed = !scope.$parent.$modelValue.is_collapsed
            for(i in scope.$parent.$parentNodesScope.$modelValue){
                if(scope.$parent.$modelValue.id != scope.$parent.$parentNodesScope.$modelValue[i].id){
                    scope.$parent.$parentNodesScope.$modelValue[i].is_collapsed = 0;
                }
            }
            scope.toggle();
        };

        self.filterByCategory = function (scope, category_id) {

            $scope.page = 1;
            $scope.category_id = category_id;
            if(category_id != undefined){
                for(i in scope.$parentNodesScope.$modelValue){
                    if(scope.$modelValue.id != scope.$parentNodesScope.$modelValue[i].id){
                        scope.$parentNodesScope.$modelValue[i].is_collapsed = 0;
                    }
                }

                $scope.categories_map[category_id].is_collapsed = 1;
                if($scope.categories_map[category_id].nodes.length > 0){
                    self.collapseChild(category_id);
                }
            }else{
                self.collapseAll();
            }

            self.setLocation();
            self.getItems();
        };

        $scope.page = $location.search().page !== undefined ? $location.search().page : 1;
        $scope.sort = $location.search().sort !== undefined ? $location.search().sort : 'position';
        $scope.sort_type = $location.search().sort_type !== undefined ? $location.search().sort_type : 'ASC';
        $scope.category_id = $location.search().category_id !== undefined ? $location.search().category_id : '';


        $scope.pages = [];
        $scope.pages_count = 0;
        $scope.items = null;
        $scope.items_map = {};
        $scope.items_count = 0;

        $scope.all_categories_count = 0;
        $scope.categories = null;
        $scope.categories_count = 0;
        $scope.categories_map = {};

        //self.setLocation();
        self.getItems();
        self.getCategories();

    }]);