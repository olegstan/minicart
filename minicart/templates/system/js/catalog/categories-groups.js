var module = angular.module('app.minicart', [])
    .config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]).controller("CategoryGroupController", ["$scope", "$http", "$location", function CategoryGroupController($scope, $http, $location) {
        var self = this;

        self.prev = function () {
            if($scope.page > 1){
                $scope.page--;
                self.getItems();
            }
        };

        self.next = function () {
            if($scope.page < $scope.pages_count){
                $scope.page++;
                self.getItems();
            }
        };

        self.page = function (number) {
            $scope.page = number;
            self.getItems();
        };

        self.pages = function () {
            self.getItems();
        };

        self.getItems = function () {
            $http({
                url: '/admin/paginate/products-groups',
                method: "GET",
                params: {
                    page: $scope.page,
                    sort: $scope.sort,
                    sort_type: $scope.sort_type
                }
            }).success(
                function(response) {
                    $scope.pages = [];
                    response.pages++;
                    for(var i = 1; i < response.pages; i++) {
                        $scope.pages.push(i);
                    }
                    $scope.pages_count = $scope.pages.length;
                    $scope.items = response.items;
                    $scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        $scope.page = $location.search().page !== undefined ? $location.search().page : 1;

        $scope.pages = [];
        $scope.pages_count = 0;
        $scope.items = [];
        $scope.items_count = 0;

        self.getItems();
    }]);

