var module = angular.module('app.minicart', [])
    .config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]).controller("BadgeController", ["$scope", "$http", "$location", function BadgeController($scope, $http, $location) {
        var self = this;

        self.sort = function ($event, type) {
            var change_sort_type = false;

            if($scope.sort === type){
                change_sort_type = true;
            }

            if(change_sort_type){
                if($scope.sort_type === 'ASC'){
                    $scope.sort_type = 'DESC';
                }else{
                    $scope.sort_type = 'ASC';
                }
            }

            $scope.sort = type;

            self.setLocation();
            self.getItems();
        };

        self.prev = function () {
            if($scope.page > 1){
                $scope.page--;
                self.setLocation();
                self.getItems();
            }
        };

        self.next = function () {
            if($scope.page < $scope.pages_count){
                $scope.page++;
                self.setLocation();
                self.getItems();
            }
        };

        self.page = function (number) {
            $scope.page = number;
            self.setLocation();
            self.getItems();
        };

        self.pages = function () {
            self.setLocation();
            self.getItems();
        };

        self.setLocation = function () {
            $location.search({page:$scope.page, sort: $scope.sort, sort_type: $scope.sort_type});
        };

        self.getItems = function () {
            $http({
                url: '/admin/paginate/brands',
                method: "GET",
                params: {
                    page: $scope.page,
                    sort: $scope.sort,
                    sort_type: $scope.sort_type
                }
            }).success(
                function(response) {
                    $scope.pages = [];
                    response.pages++;
                    for(var i = 1; i < response.pages; i++) {
                        $scope.pages.push(i);
                    }
                    $scope.pages_count = $scope.pages.length;
                    $scope.items = response.items;
                    $scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        $scope.page = $location.search().page !== undefined ? $location.search().page : 1;
        $scope.sort = $location.search().sort !== undefined ? $location.search().sort : 'name';
        $scope.sort_type = $location.search().sort_type !== undefined ? $location.search().sort_type : 'ASC';

        $scope.pages = [];
        $scope.pages_count = 0;
        $scope.items = [];
        $scope.items_count = 0;

        self.getItems();
    }]);

