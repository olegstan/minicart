var module = angular.module('app.minicart', [])
    .config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]).controller("GroupController", ["$scope", "$http", "$location", function GroupController($scope, $http, $location) {
        var self = this;

        self.prev = function () {
            if($scope.page > 1){
                $scope.page--;
                self.setLocation();
                self.getItems();
            }
        };

        self.next = function () {
            if($scope.page < $scope.pages_count){
                $scope.page++;
                self.setLocation();
                self.getItems();
            }
        };

        self.page = function (number) {
            $scope.page = number;
            self.setLocation();
            self.getItems();
        };

        self.pages = function () {
            self.setLocation();
            self.getItems();
        };

        self.setLocation = function () {
            $location.search({page:$scope.page, group_id:$scope.group_id});
        };

        self.getItems = function () {
            $http({
                url: '/admin/paginate/groups',
                method: "GET",
                params: {
                    page: $scope.page
                }
            }).success(
                function(response) {
                    $scope.pages = [];
                    response.pages++;
                    for(var i = 1; i < response.pages; i++) {
                        $scope.pages.push(i);
                    }
                    $scope.pages_count = $scope.pages.length;
                    $scope.items = response.items;
                    $scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };


        $scope.page = $location.search().page !== undefined ? $location.search().page : 1;
        $scope.group_id = $location.search().group_id !== undefined ? $location.search().group_id : null

        $scope.pages = [];
        $scope.pages_count = 0;
        $scope.items = [];
        $scope.items_count = 0;

        self.getItems();
    }]);

