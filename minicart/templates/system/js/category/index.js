var module = angular.module('app.minicart', ['ui.tree'])
    .config(["$httpProvider", "$locationProvider", function($httpProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
    }]).controller("CategoryController", ["$scope", "$http", "$location", "$element", function CategoryController($scope, $http, $location, $element) {
        var self = this;

        //then you can access the old parent from here
        //
        //e.source.nodeScope.$parentNodeScope.$modelValue
        //

        //and you can access the new parent from here
        //
        //e.dest.nodesScope.$parent.$modelValue

        $scope.options = {
            dropped: function (e) {
                var parent_id = e.dest.nodesScope.$parent.$modelValue !== undefined ? e.dest.nodesScope.$parent.$modelValue.id : 0;
                //var category_id = e.source.nodeScope.$modelValue.id;
                var position = e.dest.index;

                angular.forEach(e.dest.nodesScope.$modelValue, function(v, k) {
                    self.updatePosition(v.id, parent_id, k);
                });

                //
                //console.log(e.dest);
                //console.log(e.dest.nodesScope);
                //detect parent_id is root

                //console.log(e.source);
                //console.log(e.source);
                //console.log(e.source.nodeScope.depth());
                //console.log(e.source.nodeScope);
                //console.log(e.source.nodeScope.$parentNodeScope);
                //console.log(e.source.nodeScope);

                //$http({
                //    url: '/admin/category/update-position',
                //    method: "GET",
                //    params: {
                //        category_id: category_id,
                //        parent_id: parent_id,
                //        position: position
                //    }
                //}).success(
                //    function(response) {
                //        //$scope.items = self.tree(response);
                //        //$scope.items_count = response.count;
                //    },
                //    function(response) {
                //        // called asynchronously if an error occurs
                //        // or server returns response with an error status.
                //    }
                //);
                //console.log($scope);
                //console.log(e);
                //console.log($scope.items);
                //console.log (e.source.nodeScope);
                //console.log (e.source.nodeScope.$modelValue);
                //console.log ();
                //console.log (e.source.nodeScope.siblings());
            },
            dragStop: function(e) {

            }
            //accept: function(sourceNodeScope, destNodesScope, destIndex) { console.log(1);; },
            //dragStart: function(placeholder, drag, pos) {
            //    //console.log(1);
            //    //log
            //},
            //dragMove: function(placeholder, drag, pos) {
            //    //console.log(1);
            //},
            //
        };

        self.remove = function (scope) {
            scope.remove();
        };

        self.toggle = function (scope) {
            scope.$parent.$modelValue.is_collapsed = !scope.$parent.$modelValue.is_collapsed
            self.updateCollapse(scope.$parent.$modelValue.id, scope.$parent.$modelValue.is_collapsed);
            scope.toggle();
        };

        //self.moveLastToTheBeginning = function () {
        //    var a = $scope.data.pop();
        //    $scope.data.splice(0, 0, a);
        //};

        //self.newSubItem = function (scope) {
        //    var nodeData = scope.$modelValue;
        //    nodeData.nodes.push({
        //        id: nodeData.id * 10 + nodeData.nodes.length,
        //        title: nodeData.title + '.' + (nodeData.nodes.length + 1),
        //        nodes: []
        //    });
        //};

        self.getRootNodesScope = function() {
            return angular.element(document.getElementById("tree-root")).scope();
        };

        self.collapseAll = function() {
            var scope = self.getRootNodesScope();
            for(i in $scope.items_list){
                $scope.items_list[i].is_collapsed = 0;
            }
            self.updateCollapseAll();
            scope.collapseAll();
        };


        self.expandAll = function() {
            var scope = self.getRootNodesScope();
            for(i in $scope.items_list){
                $scope.items_list[i].is_collapsed = 1;
            }
            self.updateExpandAll();
            scope.expandAll();
        };

        /**
         * дерево с сохранением сортировки
         * @param array
         * @returns {Array}
         */
        self.tree = function (array) {
            var roots = [];
            for(var i in array) {
                node = array[i];
                $scope.map[node.id] = node;
            }
            for(var i in array) {
                node = array[i];
                node.is_collapsed = parseInt(node.is_collapsed);
                node.is_highlight = 1;
                node.is_visible = 1;
                if (node.parent_id !== "0") {
                    $scope.map[node.parent_id].nodes.push(node);
                }else{
                    roots.push(node);
                }
            }
            return roots;
        };

        self.showParent = function (category_id) {
            $scope.map[category_id].is_visible = 1;
            if($scope.map[category_id].parent_id !== "0"){
                self.showParent($scope.map[category_id].parent_id);
            }
        };

        self.search = function (e) {
            self.expandAll();

            var search = new String($scope.search);
            if(search.length > 0) {
                for (i in $scope.items_list) {
                    if ($scope.items_list[i].name.match(new RegExp(search.toString(), "i"))) {
                        $scope.items_list[i].is_highlight = 1;
                    } else {
                        $scope.items_list[i].is_highlight = 0;
                        $scope.items_list[i].is_visible = 0;
                    }
                }
                for (i in $scope.items_list) {
                    if ($scope.items_list[i].name.match(new RegExp(search.toString(), "i"))) {
                        if($scope.items_list[i].parent_id !== "0"){
                            self.showParent($scope.items_list[i].parent_id);
                        }
                    }
                }
            }else{
                for (i in $scope.items_list) {
                    $scope.items_list[i].is_highlight = 1;
                }
            }
        };

        self.clearSearch = function () {
            $scope.search = '';
            for (i in $scope.items_list) {
                $scope.items_list[i].is_highlight = 1;
            }
        };


        self.getItems = function () {
            $http({
                url: '/admin/catalog/categories',
                method: "GET",
                params: {}
            }).success(
                function(response) {
                    $scope.items_list = response;
                    $scope.items = self.tree(response);
                    $scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.updateCollapse = function (category_id, is_collapsed) {
            $http({
                url: '/admin/category/update-collapse',
                method: "GET",
                params: {
                    category_id: category_id,
                    is_collapsed: is_collapsed
                }
            }).success(
                function(response) {
                    //$scope.items = self.tree(response);
                    //$scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.updatePosition = function (category_id, parent_id, position) {
            $http({
                url: '/admin/category/update-position',
                method: "GET",
                params: {
                    category_id: category_id,
                    parent_id: parent_id,
                    position: position
                }
            }).success(
                function(response) {
                    //$scope.items = self.tree(response);
                    //$scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.updateCollapseAll = function () {
            $http({
                url: '/admin/category/update-collapse-all',
                method: "GET",
                params: {}
            }).success(
                function(response) {
                    //$scope.items = self.tree(response);
                    //$scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        self.updateExpandAll = function () {
            $http({
                url: '/admin/category/update-expand-all',
                method: "GET",
                params: {}
            }).success(
                function(response) {
                    //$scope.items = self.tree(response);
                    //$scope.items_count = response.count;
                },
                function(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
                }
            );
        };

        $scope.search = '';

        $scope.items = null;
        $scope.items_count = 0;
        $scope.map = {};

        self.getItems();
    }]);

