function confirm(caption, body, callback){
    var confirmModal =
        $('<div id="delModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'+
            '<div class="modal-dialog">'+
                '<div class="modal-content">'+
                    '<div class="modal-header">'+
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>'+
                        '<h4 class="modal-title">'+caption+'</h4>'+
                    '</div>'+
                    '<div class="modal-body">'+body+'</div>'+
                    '<div class="modal-footer">'+
                        '<button type="button" class="btn btn-default" data-dismiss="modal">Нет</button>'+
                        '<button type="button" class="btn btn-primary" id="okDel">Да</button>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>');
    confirmModal.find('#okDel').click(function(event) {
        callback();
        confirmModal.modal('hide');
    });
    confirmModal.modal('show');
};

function info_box(obj, caption, message)
{
    /*var info_message = $('<div class="alert alert-success">' +
                            '<button data-dismiss="alert" class="close" type="button">×</button>' +
                            '<strong>' + caption + '</strong>' + message +
                            '</div>').hide();*/
    var info_message = $('<div class="alert alert-success" onclick="this.remove();">' +
                            '<strong>' + caption + '</strong>' + message +
                            '</div>').hide();
    /*obj.after(info_message);*/
    info_message.prependTo(obj);
    info_message.fadeIn('slow');
    setTimeout(function () {
            info_message.fadeOut('slow', function(){
            $(this).remove();
        })

    }, 2500);
};

function error_box(obj, caption, message)
{
    /*var error_message = $('<div class="alert alert-error">' +
                            '<button data-dismiss="alert" class="close" type="button">×</button>' +
                            '<strong>' + caption + '</strong>' + message +
                            '</div>').hide();*/
    var error_message = $('<div class="alert alert-danger">' +
                            '<strong>' + caption + '</strong> ' + message +
                            '</div>').hide();
    /*obj.after(error_message);*/
    error_message.prependTo(obj);
    error_message.fadeIn('slow');
    setTimeout(function () {
            error_message.fadeOut('slow', function(){
            $(this).remove();
        })

    }, 2500);
}

function confirm_popover_box(obj, callback){
    $('div.popover').each(function () {
        $(this).prev().popover('destroy');
    });

    var popoverContent = $('<button type="button" class="btn btn-danger">Удалить</button>');
    obj.popover({
        html: true,
        placement: 'bottom',
        trigger: 'focus',
        content: popoverContent
    });
    obj.popover('toggle');
    popoverContent.click(function(event) {
        callback(obj);
        obj.popover('destroy');
    });
    return false;
}