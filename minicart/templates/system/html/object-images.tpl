{*

Шаблон для вывода изображений объекта
Входные параметры:
    $object - объект
    $module - модуль
    $images - изображения
    $images_object_name - название объекта для ресайза изображений

*}

<ul id="list1">
    {if $images}
        {foreach $images as $img}
            <li data-id="{$img->id}">
                <div>
                    {if $object->id}
                        <a ng-click="deleteImage({$img->id});" class="delete_image close"><i class="fa fa-times"></i></a>
                        <a class="fancybox" rel="group" href="{$img|resize:1100:800}">
                            <i class="fa fa-search-plus"></i>
                        </a>
                        <img src="{$img|resize:90:90}" alt="{$img->name}" />
                    {else}
                        <a ng-click="deleteImage({$img->id});" class="delete_image close"><i class="fa fa-times"></i></a>
                        <a class="fancybox" rel="group" href="{$img|resize:1100:800}">
                            <i class="fa fa-search-plus"></i>
                        </a>
                        <img src="{$img|resize:90:90}" alt="{$img->name}" />
                    {/if}
                </div>
            </li>
        {/foreach}
    {/if}
</ul>