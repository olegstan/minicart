<div class="minicontainer">
    <nav class="navbar navbar-default noradius mininavbar" role="navigation">
        <div class="row">
            <div class="col-md-3">
                <ul class="nav navbar-nav adminmenu">
                    <li><a href="{$core->root}" class="to-frontend allow-jump" data-placement="bottom" data-toggle="tooltip" data-original-title="Перейти в магазин"><span>{$smarty.server.SERVER_NAME|capitalize:false:true}</span> <i class="fa fa-level-up"></i></a></li>
                </ul>
            </div>

            <div class="col-md-9">
                <ul class="nav navbar-nav otkliki">
                    {*{foreach $menu_tree_up->subitems as $item}*}
                        {*{assign var="show_menu_item" value=false}*}
                        {*{foreach $item->subitems as $subitem}*}
                            {*{if in_array($subitem->module_name, $global_group_permissions)}*}
                                {*{assign var="show_menu_item" value=true}*}
                            {*{/if}*}
                        {*{/foreach}*}
                        {*{if $item->is_visible && (in_array($item->module_name, $global_group_permissions) || $show_menu_item)}*}
                            {*<li data-module="{$item->module_name}" class="{if (in_array($active_menu_item,$item->children) && !$module->is_external) || ($module->is_external && $item->sub_url == "modules/")}active{assign var="active_tree" value=$item->subitems}{/if}">*}
                                {*{$active_url = ""}*}
                                {*{if in_array($item->module_name, $global_group_permissions)}*}
                                    {*{if $item->url}{$active_url = $item->url}{else}{if $item->sub_url}{$active_url = $item->sub_url}{/if}{/if}*}
                                {*{else}*}
                                    {*{foreach $item->subitems as $subitem}*}
                                        {*{if in_array($subitem->module_name, $global_group_permissions) && empty($active_url)}*}
                                            {*{$active_url = $subitem->url}*}
                                        {*{/if}*}
                                    {*{/foreach}*}
                                {*{/if}*}
                                {*{if $active_url == "/"}{$active_url = ""}{/if}*}
                                {*<a href="{$config->admin_url}{$active_url}" {if $item->css_class}class="{$item->css_class}"{/if} data-url="{$item->sub_url}">{if $item->icon}{$item->icon}{/if} <span>{$item->name|escape}</span> {if $item->sub_url == "contacts/orders-calls/" && $count_callbacks>0}<span class="badge">{$count_callbacks}</span>{elseif $item->sub_url == "community/reviews-products/" && $count_new_reviews>0}<span class="badge">{$count_new_reviews}</span>{/if}{if $item->sub_url == "orders-processed/" && $count_new_orders>0}<span class="badge">{$count_new_orders}</span>{/if}</a>*}
                            {*</li>*}
                        {*{/if}*}
                    {*{/foreach}*}
                </ul>

                <ul class="nav navbar-nav pull-right adminmenu">
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle usermenu" href="#">{$user->name} <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/logout/" class="logout"><i class="fa fa-power-off"></i> <span>Выход</span></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</div>
<div class="topmenu">
    <div role="navigation" class="navbar navbar-default mainmenu">
        <ul class="horizontal-menu nav navbar-nav">
            {foreach $second_menus as $item}
                {if $item->is_visible}
                    <li class="">
                        <a href="{$item->url}" target="_self" class="{$item->css_class}">
                           {$item->name}
                        </a>
                        {*{if $item->subitems}*}
                            {*<ul>*}
                                {*{foreach $item->subitems as $subitem}*}
                                    {*<li class="active"><a href="">{$subitem->name}</a></li>*}
                                {*{/foreach}*}
                            {*</ul>*}
                        {*{/if}*}
                    </li>
                {/if}
            {/foreach}



            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/" target="_self" class="mainpage" data-url="/">Главная</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/catalog/products/" target="_self" class="catalogpage" data-url="catalog/products/">Каталог</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/user/" target="_self" class="userspage" data-url="users/">Пользователи</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/page/materials/" target="_self" class="articlepage" data-url="pages/materials/">Материалы</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/module/" target="_self" class="modulespage" data-url="modules/">Модули</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/template/design/" target="_self" class="addons" data-url="">Дополнения</a>*}
            {*</li>*}
            {*<li data-url="" data-module="" class="">*}
                {*<a href="admin/setting/" target="_self" class="settingspage" data-url="settings/">Настройки</a>*}
            {*</li>*}
        </ul>
    </div>
</div>