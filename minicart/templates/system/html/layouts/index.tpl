<!DOCTYPE html>
<html lang="en" ng-app="app.minicart">
<head>
    <base href="{$core->root}/"/>
    <title>{$core->title|escape}</title>

    {* Метатеги *}
    <meta name="description" content="{$core->meta_description|escape}"/>
    <meta name="keywords" content="{$core->meta_keywords|escape}"/>

    <!-- Standard Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    {*bootstrap*}
    <link rel="stylesheet" href="{$libraries}bootstrap/dist/css/bootstrap.css" type="text/css" media="screen"/>
    {*bootstrap*}

    {*font-awesome*}
    <link rel="stylesheet" href="{$libraries}components-font-awesome/css/font-awesome.min.css" type="text/css" media="screen"/>
    {*font-awesome*}

    <link rel="stylesheet" href="{$libraries}fancybox/source/jquery.fancybox.css" type="text/css" media="screen"/>

    <link rel="stylesheet" type="text/css" href="{$libraries}semantic-ui/dist/semantic.css">

    <link rel="stylesheet" type="text/css" href="{$path_frontend_template}css/style.css">
    <link rel="stylesheet" type="text/css" href="{$path_backend_template}css/template.css">

    <link rel="stylesheet" type="text/css" href="{$libraries}angular-ui-tree/dist/angular-ui-tree.min.css">
    <link rel="stylesheet" type="text/css" href="{$path_backend_template}css/app.css">

    {foreach $core->asset->headerCSS as $asset}
        <link rel="stylesheet" type="text/css" href="{$asset}">
    {/foreach}


    {foreach $core->asset->headerJS as $asset}
        <script src="{$asset}"></script>
    {/foreach}

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="mainbody">

    <a href="#top"></a>
    <div id="wrap">
        <div class="container">

            {include file="parts/header.tpl"}

            <div class="mainbox">
                <div class="row">
                    <div class="col-md-12">
                        {$menu_third_level = false}

                        <div class="thirdmenu">
                            <ul class="nav">
                                <ul class="nav">
                                    <li>
                                        <a href="/admin/product/" target="_self" class="products"><span>Товары</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/category/" target="_self" class="categories"><span>Категории</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/brands/" target="_self" class="brandlist"><span>Бренды</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/filters-category/" target="_self" class="filters"><span>Фильтры</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/properties/" target="_self" class=""><span>Свойства</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/badges/" target="_self" class="badgepage"><span>Бейджи</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/modificators/" target="_self" class="modifierpage"><span>Модификаторы цены</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/products-groups/" target="_self" class="product-groups"><span>Группы товаров</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/categories-groups/" target="_self" class=""><span>Группы категорий</span></a>
                                    </li>
                                    <li>
                                        <a href="/admin/catalog/modificators-orders/" target="_self" class=""><span>Модификаторы заказов</span></a>
                                    </li>
                                </ul>
                            </ul>
                        </div>


                        {if $menu_third_level}
                            <div class="thirdmenu">
                                <ul class="nav">
                                    {foreach $menu_third_level as $item}
                                        {if $item->is_visible && in_array($item->module_name, $global_group_permissions)}
                                            <li {if in_array($active_menu_item,$item->children)}class="active"{/if}>
                                                <a href="{$config->admin_url}{if $item->url && $item->url != "/"}{$item->url}{else}{if $item->sub_url && $item->sub_url != "/"}{$item->sub_url}{/if}{/if}" class="{$item->css_class}"><span>{$item->name|escape}</span></a>
                                            </li>
                                        {/if}
                                    {/foreach}
                                </ul>
                            </div>
                        {/if}

                        <div id="right">
                            {$content}
                        </div>

                    </div>
                    <!-- Основная часть (The End) -->
                </div>
            </div>
        </div>
    </div>


    {include file="parts/footer.tpl"}

    {include file="parts/modal.tpl"}


<!-- Site Properities -->
<script src="{$libraries}jquery/dist/jquery.min.js"></script>
<script src="{$libraries}bootstrap/dist/js/bootstrap.min.js"></script>
<script src="{$libraries}semantic-ui/dist/semantic.min.js"></script>
<script src="{$libraries}jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>



{* Подключаем скрипт управления меню *}
{*<script src="{$path_backend_template}/js/jquery.nestable.js"></script>*}

{* Подключаем скрипт оповещений меню *}
{*<script src="{$path_backend_template}/js/messages.js"></script>*}

{* Подключаем сортировки превью изображений *}
{*<script src="{$path_backend_template}/js/jquery.dragsort-0.5.1.min.js"></script>*}

{* Подключаем служебные функции *}
{*<script src="{$path_backend_template}/js/functions.js"></script>*}

{* Подключаем ajax-заливку файлов *}
{*<script src="{$path_backend_template}/js/jquery.damnUploader.min.js"></script>*}

{* Подключаем Select2 *}
{*<link href="{$path_backend_template}/css/select2.css" rel="stylesheet" type="text/css" media="screen"/>*}
{*<script src="{$path_backend_template}/js/select2.min.js"></script>*}
{*<script src="{$path_backend_template}/js/select2_locale_ru.js"></script>*}

{* Подключаем маску ввода *}
<script src="{$path_backend_template}/js/jquery.maskedinput.min.js"></script>


{* Подключаем WayPoint - прекрепление кнопок управления к шапке *}
<script src="{$path_backend_template}/js/waypoints.min.js"></script>
<script src="{$path_backend_template}/js/waypoints-sticky.js"></script>

{* Подключаем авторесайз для textarea *}
<script src="{$path_backend_template}/js/jquery.autosize.min.js"></script>

{* Подключаем snippet для анимирования кнопки удаления *}
<link href="{$path_backend_template}/css/ladda-themeless.min.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="{$path_backend_template}/js/spin.min.js"></script>
<script src="{$path_backend_template}/js/ladda.min.js"></script>

{* Подключаем компонент для выставления рейтинга *}
<link type="text/css" rel="stylesheet" href="{$path_frontend_template}/css/jquery.rating.css" />
<script src="{$path_frontend_template}/js/jquery.rating-2.0.js"></script>



{* Подключаем Bootstrap Datepicker *}
<link href="{$path_backend_template}/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="{$path_backend_template}/js/bootstrap-datepicker.js"></script>
<script src="{$path_backend_template}/js/bootstrap-datepicker.ru.js"></script>


{foreach $core->asset->footerJS as $asset}
    <script src="{$asset}"></script>
{/foreach}

{*angular*}
<script src="{$libraries}angular/angular.min.js"></script>
<script src="{$libraries}angular-route/angular-route.min.js"></script>
{*<script src="{$libraries}angular-ui-sortable/sortable.min.js"></script>*}
<script src="{$libraries}angular-ui-tree/dist/angular-ui-tree.min.js"></script>
{*angular*}

<script src="/minicart/templates/{$core->tpl_admin_path}/js/common.js"></script>
<script src="/minicart/templates/{$core->tpl_admin_path}/js/{$core->controller}/{$core->action_uri}.js"></script>
</body>
</html>