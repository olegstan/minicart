<!DOCTYPE html>
<html>
<head>
    <base href="{$core->root}/"/>
    <title>Вход</title>

    {* Метатеги *}
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Вход" />
    <meta name="keywords" content="Вход" />

    {*bootstrap*}
    <link rel="stylesheet" href="{$libraries}bootstrap/dist/css/bootstrap.css" type="text/css" media="screen"/>
    {*bootstrap*}

    {* Подключаем css *}
    <link rel="stylesheet" type="text/css" href="{$path_backend_template}css/template.css">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="loginbg">

    <div class="container" id="center-vertikal">
        <div class="main">
            <div class="main_in_main">
                <div class="content">
                    {$content}
                    <div class="textcenter"><p><a href="{$config->root_url}/">Вернуться на сайт</a></p>
                        <p><a href="{$config->root_url}/login/?mode=forgot-password" data-toggle="modal">Забыли пароль?</a></p>
                        <p>Работает на <a href="http://minicart.ru/">MiniCart</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="push"></div>
        <!-- /container -->
    </div>

    <script src="{$libraries}jquery/dist/jquery.min.js"></script>
    {*bootstrap*}
    <script src="{$libraries}bootstrap/dist/js/bootstrap.min.js"></script>
    {*bootstrap*}

    <script type="text/javascript">
        $("#admin-login-form").submit(function(){
            if ($('input[name=username]').val().length == 0)
            {
                $('#name-required').show();
                return false;
            }
            if ($('input[name=password]').val().length == 0)
            {
                $('#password-required').show();
                return false;
            }
        });
    </script>

</body>
</html>