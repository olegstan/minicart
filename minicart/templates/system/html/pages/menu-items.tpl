<div id="main_list" class="pages_menu_items" ng-controller="MenuItemController as ctrl">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
            </div>
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['menu_id'=>$params_arr['menu_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <div ng-include="'minicart/templates/system/js-template/pages/menu-items.html'"></div>
            </div>
            <div class="col-md-4">
                <legend>Пользовательские</legend>
                <div class="list-group">
                    {foreach $menu as $m}
                        {if !$m->is_static}<a href="{$config->root_url}{$module->url}{url add=['menu_id'=>$m->id]}" class="list-group-item {if $params_arr['menu_id'] == $m->id}active{/if}">{$m->name}</a>{/if}
                    {/foreach}
                </div>
                <legend>Системные</legend>
                <div class="list-group">
                    {foreach $menu as $m}
                        {if $m->is_static}<a href="{$config->root_url}{$module->url}{url add=['menu_id'=>$m->id]}" class="list-group-item {if $params_arr['menu_id'] == $m->id}active{/if}">{$m->name}</a>{/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </form>
</div>