<div id="main_list" class="tags" ng-controller="CategoryController as ctrl">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <div class="controlgroup">
            <div class="currentfilter">
                <div class="input-group">
                    <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                    <span class="input-group-btn">
                    {*{if $keyword}*}
                        {*<button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                *}
                    {*{/if}*}
                    <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <a href="" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Категории материалов
                    <div id="nestable-menu" class="btn-group">
                        <button type="button" data-action="expand-all" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Развернуть все</button>
                        <button type="button" data-action="collapse-all" class="btn btn-sm btn-default"><i class="fa fa-minus"></i> Свернуть все</button>
                    </div>
                </legend>

                <div ng-include="'minicart/templates/system/js-template/pages/categories.html'"></div>

                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все &uarr;</div>
                    <select id="mass_operation" class="form-control">
                        <option value='1'>Сделать видимыми</option>
                        <option value='2'>Скрыть</option>
                        <option value='3'>Удалить</option>
                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
            </div>
        </div>
    </form>
</div>