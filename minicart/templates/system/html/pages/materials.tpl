<div id="main_list" class="tags_values" ng-controller="MaterialController as ctrl">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup form-inline">
            <div class="currentfilter products">
                <div class="row">
                    <div class="col-md-8">
                        <div class="input-group">
                            <input id="searchField" type="text"  class="form-control" placeholder="Поиск">
                            <span class="input-group-btn">
                                <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <label class="checkbox">
                                <input type="checkbox" id="search_in_current_category"> <label class="notchecked" for="search_in_current_category">В текущей категории</label>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <a href="" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>
        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>
                    <div class="row">
                        <div class="col-md-4">
                            Материалы <i class="fa fa-spinner fa fa-spin fa fa-large" id="materials-spinner" style="display:none;"></i>
                        </div>
                    </div>
                </legend>
                <div id="refreshpart" class="dd">
                    <div ng-include="'minicart/templates/system/js-template/pages/materials.html'"></div>
                </div>
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все ↑</div>
                    <select id="mass_operation" class="form-control">
                        <option value="1">Сделать видимыми</option>
                        <option value="2">Скрыть</option>
                                    <option value="3">Переместить на страницу</option>
                                    <option value="4">Переместить в категорию</option>
                        <option value="11">Удалить</option>
                    </select>
                    <select id="mass_operation_move_to_page" class="form-control" style="display:none;">
                        {section name=pages start=1 loop=$total_pages_num+1 step=1}
                        <option>{$smarty.section.pages.index}</option>
                        {/section}
                    </select>
                    <select id="mass_operation_move_to_category" class="form-control" style="display:none;">

                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
            </div>
            <div class="col-md-4">
                <div ng-include="'minicart/templates/system/js-template/pages/materials-categories.html'"></div>
            </div>
        </div>
    </form>
</div>