{* Шаблон для загрузки изображений *}
{* Входные параметры: 
        $object    - объект для которого подгружается шаблон (product, category, tag, etc...)
        $temp_id - если объект еще не существует, то временный id
        $module - модуль
        $images_object_name - название объекта для ресайза изображений
        $limit_images - ограничение на кол-во изображений (по умолчанию - 10)
*}

<div class="object-images main-object-images nopadding">
    <div ng-include="'minicart/templates/system/js-template/object-images.html'"></div>
</div>

<div class="controls">
    <span class="btn btn-file btn-default">
        <i class="fa fa-plus"></i> <span>Добавить изображения</span>
        <input id="upload" onchange="angular.element(this).scope().uploadImage(this.files)" ng-model="image" type="file" multiple name="uploaded-images" accept="image/*">
        <i class="fa fa-spin fa-spinner hidden" id="upload-anim"></i>
    </span>
    <a onclick="$('#internet-upload-box').show();event.preventDefault();" class="btn btn-default" type="button" title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Загрузить из интернета"><i class="fa fa-globe"></i></a>
    <div id="internet-upload-box" style="display:none">
        <input  type="text" class="form-control" id="image-upload-url" value="" placeholder="Введите URL изображения"/>
        <a ng-click="ctrl.uploadImageByUrl()" class="btn btn-default" href="#"><i class="fa fa-arrow-circle-right"></i></a>
        <a ng-click="ctrl.clearUrlInput()" class="btn btn-default" type="button" href="#"><i class="fa fa-times"></i></a>
    </div>
</div>

<div id="upload-zone" ondrop="angular.element(this).scope().uploadImageByZone(event)" ondragover="angular.element(this).scope().dragEnterLeave(event)" ondragenter="angular.element(this).scope().dragEnterLeave(event)" ondragleave="angular.element(this).scope().dragEnterLeave(event)" class="imguploader">или перетащите изображения сюда
    {*<ul id="img-list" class="hidden"></ul>*}
    <div class="clear"></div>
</div>

