{* Title *}
{$meta_title='Редактирование категории' scope=parent}

<form method=post id="category">
<fieldset>
<div class="controlgroup">
    {if $category}
        <div class="openlink">
            <a href="{$config->root_url}{$category_frontend_module->url}{$category->url}{$settings->postfix_category_url}"
               target="_blank"><i class="fa fa-external-link"></i> <span>Открыть категорию в новом окне</span></a>
        </div>
    {/if}

    <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
    <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
    <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i>
        Отменить</a>
</div>

{*if $message_success}
<div class="alert alert-success">
    <button data-dismiss="alert" class="close" type="button">×</button>
    <strong>Изменения сохранены!</strong>
</div>
{/if*}


<div class="block-header">{if $category}Редактирование категории{else}Добавление категории{/if}
</div>

<ul class="nav nav-tabs mini-tabs" role="tablist" id="category-tablist">
    <li {if !$tab || $tab == 'main'}class="active"{/if}><a href="#main" role="tab" data-toggle="tab" data-url="main"><span>Основное</span></a></li>
    <li {if $tab == 'gallery'}class="active"{/if}><a href="#gallery" role="tab" data-toggle="tab" data-url="gallery"><span>Галерея категории</span></a></li>
    <li {if $tab == 'seo'}class="active"{/if}><a id="seo-toggle" href="#seo" role="tab" data-toggle="tab" data-url="seo"><span>SEO</span></a></li>
    <li {if $tab == 'modifiers'}class="active"{/if}><a href="#modifiersprices" role="tab" data-toggle="tab" data-url="modifiers"><span>Модификаторы цены</span></a></li>
    <li {if $tab == 'more'}class="active"{/if}><a href="#other" role="tab" data-toggle="tab" data-url="more"><span>Дополнительно</span></a></li>
</ul>

<div class="tab-content">
    <div class="tab-pane {if !$tab || $tab == 'main'}active{/if}" id="main">
    <div class="row">
    <div class="col-md-8">
        <div class="row">
            <div class="col-md-8">

                <div class="form-group">
                    <label>Название пункта меню <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                   data-original-title="Отображается в меню категорий, а также в тегах H1 если не указан заголовок категории"><i
                                    class="fa fa-info-circle"></i></a></label>

                    <div class="input-group">
                        {*<input type="text" class="form-control input-bold" name="name" value="{$category->name|escape_string}"/>*}
                    <span class="input-group-addon">
                    <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                       data-original-title="Системный id категории">id:{$category->id}</a>
                    </span>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="statusbar">

                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $category->is_visible || !$category}active{/if}">
                            <input type="radio" name="visible" value=1 {if $category->is_visible || !$category}checked{/if}>
                            Активен
                        </label>
                        <label class="btn btn-default4 off {if $category && !$category->is_visible}active{/if}">
                            <input type="radio" name="visible" value=0 {if $category && !$category->is_visible}checked{/if}>
                            Скрыт
                        </label>
                    </div>
                    <div class="statusbar-text">Статус:</div>
                </div>
            </div>

        </div>


        <div class="form-group">
            <div class="row">
                <div class="col-md-6">
                    <label>Родительская категория</label>
                    {* Версия с Select2 (БЕЗ AJAX) *}
                    <select name="parent_id" data-placeholder="Корневая категория" style="width:100%">
                        {*<option></option>*}
                        <option value='0' {if $category->parent_id == 0 || !$category->id}selected{/if}>Корневая категория
                        </option>
                        {function name=categories_tree level=0}
                            {foreach $items as $item}
                                {if $category->id != $item->id}
                                    <option value='{$item->id}'
                                            {if $category->parent_id == $item->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                                    {categories_tree items=$item->subcategories level=$level+1}
                                {/if}
                            {/foreach}
                        {/function}
                        {categories_tree items=$categories}
                    </select>

                    {*<select name="parent_id" class="form-control">
                        <option value='0'>Корневая категория</option>
                        {function name=categories_tree level=0}
                            {foreach $items as $item}
                                {if $category->id != $item->id}
                                    <option value='{$item->id}' {if $category->parent_id == $item->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                                    {categories_tree items=$item->subcategories level=$level+1}
                                {/if}
                            {/foreach}
                        {/function}
                        {categories_tree items=$categories}
                    </select>*}
                </div>

                <div class="col-md-6">
                    <label>Фильтр для категории <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                   data-original-title="Определяет какой фильтр будет отображаться в этой категории на сайте и какие свойства у товаров этой категории будут доступны для заполнения в админке"><i
                                    class="fa fa-info-circle"></i></a> (<a href="admin/catalog/filter-for-category/"
                                                                           target="_blank" class="link">Создать
                            новый</a>)</label>
                    <select name="set_id" class="form-control">
                        <option value='0'>Не выбран</option>
                        {foreach $tags_sets as $set}
                            <option value='{$set->id}'
                                    {if $set->id == $category->set_id}selected{/if}>{$set->name|escape}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>

        <hr/>

        <div class="form-group">
            <lebel class="bigname">Описание верхнее <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                       data-original-title="Отображается до списка товаров категории"><i
                            class="fa fa-info-circle"></i></a></lebel>

            <textarea name="description" class="form-control ckeditor" rows=10>{$category->description}</textarea>
        </div>

        <div class="form-group">
            <label class="bigname">Описание нижнее <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается после списка товаров и постраничной навигации"><i class="fa fa-info-circle"></i></a></label>
            <textarea name="description2" class="form-control ckeditor" rows=15>{$category->description2}</textarea>
        </div>
    </div>
    <div class="col-md-4">
        <label>Изображение категории <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Это изображение используется для показа в списке категорий и в выпадающей строке поиска"><i class="fa fa-info-circle"></i></a></label>

        {*{include file='object-image-placeholder.tpl' object=$category images_object_name='categories' limit_images=1}*}

        <legend>Отображать товары</legend>

        <div class="form-group">
            <div class="btn-group btn-lists" data-toggle="buttons">
                <label class="btn btn-default on {if !$category || $category->show_mode == ''}active{/if}">
                    <input type="radio" name="show_mode" value='' {if !$category || $category->show_mode == ''}checked{/if}>
                    По умолчанию
                </label>
                <label class="btn btn-default on {if $category->show_mode == 'list'}active{/if}">
                    <input type="radio" name="show_mode" value='list' {if $category->show_mode == 'list'}checked{/if}> <i
                            class="fa fa-bars"></i> Списком
                </label>
                <label class="btn btn-default off {if $category->show_mode == 'tile'}active{/if}">
                    <input type="radio" name="show_mode" value='tile' {if $category->show_mode == 'tile'}checked{/if}> <i
                            class="fa fa-th"></i> Плиткой
                </label>
            </div>
        </div>
        <div class="form-group">
            <label>Порядок отображения <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                          data-original-title="Определяет порядок отображения товаров в этой категории по умолчанию, не распостраняется на подкатегории"><i
                            class="fa fa-info-circle"></i></a></label>

            <select class="form-control" name="sort_type">
                <option value=''>По умолчанию</option>
                <option value='position' {if $category->sort_type == 'position'}selected{/if}>По порядку</option>
                <option value='price_asc' {if $category->sort_type == 'price_asc'}selected{/if}>По цене (сначала дешевые)
                </option>
                <option value='price_desc' {if $category->sort_type == 'price_desc'}selected{/if}>По цене (сначала
                    дорогие)
                </option>
                <option value='popular_asc' {if $category->sort_type == 'popular_asc'}selected{/if}>По популярности (сначала
                    популярные)
                </option>
                <option value='popular_desc' {if $category->sort_type == 'popular_desc'}selected{/if}>По популярности
                    (сначала непопулярные)
                </option>
                <option value='newest_desc' {if $category->sort_type == 'newest_desc'}selected{/if}>По новизне (сначала
                    новые)
                </option>
                <option value='newest_asc' {if $category->sort_type == 'newest_asc'}selected{/if}>По новизне (сначала
                    старые)
                </option>
                <option value='name_asc' {if $category->sort_type == 'name_asc'}selected{/if}>По алфавиту (A-Z)</option>
                <option value='name_desc' {if $category->sort_type == 'name_desc'}selected{/if}>По алфавиту (Z-A)</option>
            </select>
        </div>
        <hr/>
        <div class="form-group">
                    <label>CSS-класс <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                        data-original-title="Стиль категории, выводится в стиле body"><i
                                    class="fa fa-info-circle"></i></a></label>
                    <input type="text" class="form-control" class="col-md-4" name="css_class" placeholder=""
                           value="{$category->css_class}"/>
                </div>
        
                <div class="form-group">
                    <label>Автоматические теги категории <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                            data-original-title="Эти теги назначаются всем товарам находящимся в этой категории и подкатегориях, если товар будет перемещен тег удалится автоматически"><i
                                    class="fa fa-info-circle"></i></a></label>
        
                    {assign var="tags_values" value=""}
                    {if $category_tags && array_key_exists($category_tags_group, $category_tags)}
                        {foreach $category_tags[$category_tags_group] as $t}
                            {if !empty($tags_values)}{assign var="tags_values" value=$tags_values|cat:'#%#'}{/if}
                            {assign var="tags_values" value=$tags_values|cat:$t->name}
                        {/foreach}
                    {/if}
        
                    <input type="hidden" name="category_tags[{$category_tags_group}]" id="category_tags" value="{$tags_values}">
                </div>

        <legend>Отображение в админке</legend>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="collapsed" id="collapsed" value="1" {if $category->collapsed}checked{/if}> По
                умолчанию свернута
            </label>
        </div>


    </div>
    </div>

    </div>
    <div class="tab-pane {if $tab == 'gallery'}active{/if}" id="gallery">
        <legend>Галерея категории</legend>

        {*{include file='object-gallery-placeholder.tpl' object=$category images_object_name='categories-gallery'}*}
    </div>
    <div class="tab-pane {if $tab == 'seo'}active{/if}" id="seo">
     {* SEO начало *}
    <legend class="mt20">Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>
    
        <div class="row">
        <div class="col-md-6">    
    
        <div class="form-group">
            <label>URL</label>

            <div class="input-prepend input-group">

                <span class="input-group-addon">{$settings->prefix_category_url}/</span>

                <input type="text" class="form-control" name="url" placeholder="Введите URL" value="{$category->url}"/>
                <span class="input-group-addon">{$settings->postfix_category_url}</span>
            </div>
        </div>
        
        <div class="form-group">
            <label>Заголовок категории <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Выводится в тегах h1 на странице категории, это поле опционально, используется чтобы Заголовок категории отличался от названия пункта меню"><i class="fa fa-info-circle"></i></a></label>
            {*<input type="text" class="form-control" name="frontend_name" placeholder="Введите заголовок категории"*}
                   {*value="{$category->frontend_name|escape_string}"/>*}
        </div>

        <div class="form-group">
            <label>Meta-title</label>
            <input type="text" class="form-control" name="meta_title" value="{$category->meta_title}"/>
        </div>
        
        </div>
        <div class="col-md-6">        
        
        <div class="form-group">
            <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                     data-original-title="Meta-keywords"><i class="fa fa-info-circle"></i></a></label>
            <textarea name="meta_keywords" class="form-control">{$category->meta_keywords}</textarea>
        </div>

        <div class="form-group">
            <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                               data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
            <textarea name="meta_description" class="form-control">{$category->meta_description}</textarea>
        </div>
        </div>
        </div>
     {* SEO конец *}
    </div>
    <div class="tab-pane {if $tab == 'modifiers'}active{/if}" id="modifiersprices">
    <div class="row">
        <div class="col-md-6">
        <div class="form-group">
            {foreach $modificators as $m}
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{$m->id}" name="modificators[]"
                               {if $category && in_array($m->id, $category->modificators)}checked=""{/if}> {$m->name}
                    </label>
                </div>
            {/foreach}
        </div>

        <legend>Группы модификаторов</legend>
        <div class="form-group">
            {foreach $modificators_groups as $g}
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="{$g->id}" name="modificators_groups[]"
                               {if $category && in_array($g->id, $category->modificators_groups)}checked=""{/if}> {$g->name}
                    </label>
                </div>
            {/foreach}
        </div>
        </div>
        </div>

    </div>
    <div class="tab-pane {if $tab == 'more'}active{/if}" id="other">
    
    {* Настройка появляется только у категорий 1 уровня - начало *}
    {if $show_menu_config_level1}
    <div class="row">
        <div class="col-md-6">
        <legend>Настройка меню для категорий 1 уровня</legend>
        <div class="form-group">
            <label>Выберите вид меню</label>
            <div class="btn-group noinline" data-toggle="buttons">
                  <label class="btn btn-default {if $category->menu_level1_type == 1}active{/if}">
                    <input type="radio" name="menu_level1_type" value="1" {if $category->menu_level1_type == 1}checked{/if}/>Простое меню
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_type == 2}active{/if}">
                    <input type="radio" name="menu_level1_type" value="2" {if $category->menu_level1_type == 2}checked{/if}/>Мегаменю
                  </label>
                </div>
        </div>
        {* Настройка появляется только если выбрали Мегаменю *}
        <div class="{if $category->menu_level1_type == 1}hidden{/if}" id="mega-menu-config">
        <div class="form-group">
            <label>На сколько колонок выпадает меню</label>
            <div class="btn-group noinline" data-toggle="buttons">
                  <label class="btn btn-default {if $category->menu_level1_columns == 1}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="1" {if $category->menu_level1_columns == 1}checked{/if}/>1
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_columns == 2}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="2" {if $category->menu_level1_columns == 2}checked{/if}/>2
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_columns == 3}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="3" {if $category->menu_level1_columns == 3}checked{/if}/>3
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_columns == 4}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="4" {if $category->menu_level1_columns == 4}checked{/if}/>4
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_columns == 5}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="5" {if $category->menu_level1_columns == 5}checked{/if}/>5
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_columns == 6}active{/if}">
                    <input type="radio" name="menu_level1_columns" value="6" {if $category->menu_level1_columns == 6}checked{/if}/>6
                  </label>                  
              </div>               
        </div>
        
        <div class="form-group">
            <label>На какую ширину выпадает меню</label>
            <div class="btn-group noinline" data-toggle="buttons" id="">
                  <label class="btn btn-default {if $category->menu_level1_width == 1}active{/if}">
                    <input type="radio" name="menu_level1_width" value="1" {if $category->menu_level1_width == 1}checked{/if}/>На ширину колонок
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_width == 2}active{/if}">
                    <input type="radio" name="menu_level1_width" value="2" {if $category->menu_level1_width == 2}checked{/if}/>На всю ширину
                  </label>                  
              </div>               
        </div>
        
        <div class="form-group">
            <label>Куда ориентировано меню</label>
            <div class="btn-group noinline" data-toggle="buttons">
                  <label class="btn btn-default {if $category->menu_level1_align == 1}active{/if}">
                    <input type="radio" name="menu_level1_align" value="1" {if $category->menu_level1_align == 1}checked{/if}/>Вправо
                  </label>
                  <label class="btn btn-default {if $category->menu_level1_align == 2}active{/if}">
                    <input type="radio" name="menu_level1_align" value="2" {if $category->menu_level1_align == 2}checked{/if}/>Влево
                  </label>                  
              </div>               
        </div>
        
        <div class="form-group">
            <label>Использовать рекламный баннер для этого пункта меню</label>
            <div class="btn-group noinline" data-toggle="buttons" id="">
                  <label class="btn btn-default {if $category->menu_level1_use_banner}active{/if}">
                    <input type="radio" name="menu_level1_use_banner" value="1" {if $category->menu_level1_use_banner}checked{/if}/>Да
                  </label>
                  <label class="btn btn-default {if !$category->menu_level1_use_banner}active{/if}">
                    <input type="radio" name="menu_level1_use_banner" value="0" {if !$category->menu_level1_use_banner}checked{/if}/>Нет
                  </label>                  
              </div>  
              <div class="input-group mt15 w200px {if !$category->menu_level1_use_banner}hidden{/if}" id="input-banner-id">
                      <span class="input-group-addon" id="basic-addon1">id баннера</span>
                      <input type="text" class="form-control" name="menu_level1_banner_id"  value="{$category->menu_level1_banner_id}" placeholder="">
                </div>
                           
        </div>
        
        <hr/>
         <div class="form-group">
            <label>Css-класс блока меню</label>
            <input type="text" class="form-control" placeholder="">                          
        </div>
        
    <hr/>
    <label>Толщина колонок</label>       
        
    <table class="table" id="columns-width">
    <tbody>
    <tr>
      <th>Колонка</td>
      <th>Толщина <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например у вас меню выпадает на 5 колонок и при этом вам нужно чтобы меню в третьей колонке было толщиной в 2 колонки, в этом случае укажите толщину блока 2 колонки"><i class="fa fa-info-circle"></i></a></td>
    </tr>
    <tr class="column1">
        <td>1</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column1_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column1_width" value="1" {if $category->menu_level1_column1_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column1_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column1_width" value="2" {if $category->menu_level1_column1_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column1_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column1_width" value="3" {if $category->menu_level1_column1_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column1_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column1_width" value="4" {if $category->menu_level1_column1_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column1_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column1_width" value="5" {if $category->menu_level1_column1_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
    <tr class="column2" {if $category->menu_level1_columns<2}style="display:none;"{/if}>
        <td>2</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column2_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column2_width" value="1" {if $category->menu_level1_column2_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column2_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column2_width" value="2" {if $category->menu_level1_column2_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column2_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column2_width" value="3" {if $category->menu_level1_column2_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column2_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column2_width" value="4" {if $category->menu_level1_column2_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column2_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column2_width" value="5" {if $category->menu_level1_column2_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
    <tr class="column3" {if $category->menu_level1_columns<3}style="display:none;"{/if}>
        <td>3</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column3_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column3_width" value="1" {if $category->menu_level1_column3_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column3_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column3_width" value="2" {if $category->menu_level1_column3_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column3_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column3_width" value="3" {if $category->menu_level1_column3_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column3_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column3_width" value="4" {if $category->menu_level1_column3_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column3_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column3_width" value="5" {if $category->menu_level1_column3_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
    <tr class="column4" {if $category->menu_level1_columns<4}style="display:none;"{/if}>
        <td>4</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column4_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column4_width" value="1" {if $category->menu_level1_column4_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column4_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column4_width" value="2" {if $category->menu_level1_column4_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column4_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column4_width" value="3" {if $category->menu_level1_column4_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column4_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column4_width" value="4" {if $category->menu_level1_column4_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column4_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column4_width" value="5" {if $category->menu_level1_column4_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
    <tr class="column5" {if $category->menu_level1_columns<5}style="display:none;"{/if}>
        <td>5</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column5_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column5_width" value="1" {if $category->menu_level1_column5_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column5_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column5_width" value="2" {if $category->menu_level1_column5_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column5_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column5_width" value="3" {if $category->menu_level1_column5_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column5_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column5_width" value="4" {if $category->menu_level1_column5_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column5_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column5_width" value="5" {if $category->menu_level1_column5_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
    <tr class="column6" {if $category->menu_level1_columns<6}style="display:none;"{/if}>
        <td>6</td>
        <td>
            <div class="btn-group noinline" data-toggle="buttons" id="">
              <label class="btn btn-default {if $category->menu_level1_column6_width == 1}active{/if}">
                <input type="radio" name="menu_level1_column6_width" value="1" {if $category->menu_level1_column6_width == 1}checked{/if}/>1
              </label>
              <label class="btn btn-default {if $category->menu_level1_column6_width == 2}active{/if}">
                <input type="radio" name="menu_level1_column6_width" value="2" {if $category->menu_level1_column6_width == 2}checked{/if}/>2
              </label>
              <label class="btn btn-default {if $category->menu_level1_column6_width == 3}active{/if}">
                <input type="radio" name="menu_level1_column6_width" value="3" {if $category->menu_level1_column6_width == 3}checked{/if}/>3
              </label>
              <label class="btn btn-default {if $category->menu_level1_column6_width == 4}active{/if}">
                <input type="radio" name="menu_level1_column6_width" value="4" {if $category->menu_level1_column6_width == 4}checked{/if}/>4
              </label>
              <label class="btn btn-default {if $category->menu_level1_column6_width == 5}active{/if}">
                <input type="radio" name="menu_level1_column6_width" value="5" {if $category->menu_level1_column6_width == 5}checked{/if}/>5
              </label>
            </div>
        </td>
    </tr>
  </tbody>
</table>
        
          </div>
    </div>
    {/if}
    {* Настройка появляется только у категорий 1 уровня - конец *}
    
    
    {* Настройка появляется только у категорий 2 уровня - начало *}
    {if $show_menu_config_level2}
    <div class="row">
        <div class="col-md-6">
        <legend>Настройка меню для категорий 2 уровня</legend>
        
            <div class="form-group">
                <label>В какой колонке мегаменю отображать пункт меню <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="По умолчанию все пункты второго уровня отображаются в 1 колонке, данная настройка доступна только если выбрано мегаменю в дизайне и только для категорий второго уровня"><i class="fa fa-info-circle"></i></a></label>

                <div class="btn-group noinline" data-toggle="buttons" id="">
                    {section name=columns start=1 loop=$parent_category->menu_level1_columns+1 step=1}
                        <label class="btn btn-default {if ($category->menu_level2_column == $smarty.section.columns.index) || ($smarty.section.columns.first && !$category->menu_level2_column) || ($smarty.section.columns.first && $category->menu_level2_column > $parent_category->menu_level1_columns)}active{/if}">
                            <input type="radio" name="menu_level2_column" value="{$smarty.section.columns.index}" {if ($category->menu_level2_column == $smarty.section.columns.index) || ($smarty.section.columns.first && !$category->menu_level2_column) || ($smarty.section.columns.first && $category->menu_level2_column > $parent_category->menu_level1_columns)}checked{/if}/> {$smarty.section.columns.index}
                        </label>
                    {/section}
                  {*<label class="btn btn-default {if $category->menu_level2_column == 1}active{/if}">
                    <input type="radio" name="menu_level2_column" value="1" {if $category->menu_level2_column == 1}checked{/if}/>1
                  </label>
                  <label class="btn btn-default {if $category->menu_level2_column == 2}active{/if}">
                    <input type="radio" name="menu_level2_column" value="2" {if $category->menu_level2_column == 2}checked{/if}/>2
                  </label>
                  <label class="btn btn-default {if $category->menu_level2_column == 3}active{/if}">
                    <input type="radio" name="menu_level2_column" value="3" {if $category->menu_level2_column == 3}checked{/if}/>3
                  </label>
                  <label class="btn btn-default {if $category->menu_level2_column == 4}active{/if}">
                    <input type="radio" name="menu_level2_column" value="4" {if $category->menu_level2_column == 4}checked{/if}/>4
                  </label>*}
                </div>
            </div>
        
        
            <div class="form-group">
                <label>На сколько колонок делить подпункты <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Для длинных списков рекомендуется разбить список на 2 и более колонок"><i class="fa fa-info-circle"></i></a></label>
                <div class="btn-group noinline" data-toggle="buttons" id="">
                      <label class="btn btn-default {if $category->menu_level2_columns == 1}active{/if}">
                        <input type="radio" name="menu_level2_columns" value="1" {if $category->menu_level2_columns == 1}checked{/if}/>1
                      </label>
                      <label class="btn btn-default {if $category->menu_level2_columns == 2}active{/if}">
                        <input type="radio" name="menu_level2_columns" value="2" {if $category->menu_level2_columns == 2}checked{/if}/>2
                      </label>
                      <label class="btn btn-default {if $category->menu_level2_columns == 3}active{/if}">
                        <input type="radio" name="menu_level2_columns" value="3" {if $category->menu_level2_columns == 3}checked{/if}/>3
                      </label>
                 </div>
            </div>
          </div>
    </div>
    {/if}
    {* Настройка появляется только у категорий 2 уровня - конец *}
    
    
    
    <div class="row">
        <div class="col-md-6">
            {if $show_select_menu_column}
            
            {/if}
        </div>
    </div>
    </div>

</div>




<input type="hidden" name="id" value="{$category->id}"/>
<input type="hidden" name="session_id" value="{$smarty.session.id}"/>
<input type="hidden" name="recreate_seo" value="0"/>
<input type="hidden" name="close_after_save" value="0"/>
<input type="hidden" name="add_after_save" value="0"/>
<input type="hidden" name="images_position" value=""/>
{if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
{if $temp_id_gallery}<input type="hidden" name="temp_id_gallery" value="{$temp_id_gallery}"/>{/if}
</fieldset>
</form>
