<div ng-class="tab === 'seo' ? 'active' : ''" class="tab-pane">
    {* SEO начало *}

    <legend class="mt20">Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>

    <div class="row">
        <div class="col-md-6">

            <div class="form-group">
                <label>URL</label>

                <div class="input-group">
                    <span class="input-group-addon">{$settings->prefix_product_url}</span>
                    <input type="text" class="form-control" name="url" value="{$product->url}"/>
                    <span class="input-group-addon">{$settings->postfix_product_url}</span>
                </div>
            </div>

            <div class="form-group">
                <label>Meta-title</label>
                <input type="text" class="form-control" name="meta_title" placeholder="Введите meta-title" value="{$product->meta_title}"/>
            </div>
        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                         data-original-title="Meta-keywords"><i
                                class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_keywords" class="form-control">{$product->meta_keywords}</textarea>
            </div>
            <div class="form-group">
                <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                   data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_description" class="form-control">{$product->meta_description}</textarea>
            </div>
        </div>
    </div>
    {* SEO конец *}
</div>
