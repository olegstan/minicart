<div ng-class="tab === 'related' ? 'active' : ''" class="tab-pane">

    {* Сопутствующие товары начало *}
    <div class="row">
        <div class="col-md-6">
            <legend>Сопутствующие товары</legend>
            <div id="related" class="dd">
                <ol class="dd-list">
                    {if $product->related}
                        {foreach $product->related as $related}
                            <li class="dd-item dd3-item relatedproducts {if !$related->is_visible}lampoff{/if}" data-id="{$related->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="statusrelated">
                                        {if $related->in_stock}
                                            <i class="fa fa-check" title="Есть в наличии"></i>
                                        {elseif $related->in_order}
                                            <i class="fa fa-truck" title="Под заказ"></i>
                                        {else}
                                            <i class="fa fa-times-circle" title="Нет в налчии"></i>
                                        {/if}
                                    </div>

                                    {*<div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a>*}
                                    {*</div>*}
                                    {*<div class="relatedimage">*}
                                        {*{if $related->image}<img src="{$related->image->filename|resize:products:30:30}"*}
                                                                 {*alt="{$related->image->name}"/>{/if}*}
                                    {*</div>*}
                                    {*<div class="relatedname">*}
                                        {*<input type="hidden" name="related_ids[]" value="{$related->id}"/>*}
                                        {*<a href="{$config->root_url}{$module->url}{url add=['id'=>$related->id]}"*}
                                           {*target="_blank">{$related->name}</a>*}

                                        {*<div class="related-sky">{if $related->variant}{$related->variant->sku}{/if}</div>*}
                                    {*</div>*}
                                </div>
                            </li>
                        {/foreach}
                    {else}
                        <div id="no-related-products">Пока нет товаров</div>
                    {/if}
                </ol>
            </div>

            <div id="main_list" class="tags_values">
                <div class="apsearch form-inline">
                    <input id="searchField" type="text" class="form-control" placeholder="Поиск" autocomplete="off">
                    <button id="searchCancelButton" style="display: none;" class="btn btn-default searchremove" type="button"><i class="fa fa-times"></i></button>
                </div>

                <div id="refreshpart" class="addcontainer" style="display:none;">
                    {*{include file='product-addrelated.tpl'}*}
                </div>
            </div>

            {* Сопутствующие товары конец *}
        </div>

        <div class="col-md-6">
            {* Аналоги начало *}

            <legend>Аналоги</legend>
            <div class="form-group">
                <button type="button" class="btn btn-default" id="create-analogs-group">Создать группу аналогов</button>
                <button type="button" class="btn btn-default" id="show-analogs-group">Выбрать группу аналогов</button>
            </div>
            <div class="form-group hidden" id="div-analogs-group">
                <select name="analogs_group" id="select-analogs-group">
                    {foreach $analogs_groups as $analog_group}
                        <option value="{$analog_group}" {if $analog_group==$analogs_group_id}selected{/if}>Группа аналогов {$analog_group}</option>
                    {/foreach}
                </select>
            </div>
            <div class="form-group {if !$analogs_group_id}hidden{/if}" id="analogs-group" data-analogs-group="{$analogs_group_id}">
                Группа аналогов {$analogs_group_id}
            </div>
            <div id="analogs" class="dd">
                <ol class="dd-list">
                    {if $analogs_products}
                        {foreach $analogs_products as $analog}
                            <li class="dd-item dd3-item relatedproducts {if !$analog->is_visible}lampoff{/if} {if $analog->id==$product->id}current{/if}"
                                data-id="{$analog->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="statusrelated">
                                        {if $analog->in_stock}
                                            <i class="fa fa-check" title="Есть в наличии"></i>
                                        {elseif $analog->in_order}
                                            <i class="fa fa-truck" title="Под заказ"></i>
                                        {else}
                                            <i class="fa fa-times-circle" title="Нет в налчии"></i>
                                        {/if}
                                    </div>

                                    <div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a>
                                    </div>
                                    <div class="relatedimage">
                                        {if $analog->image}<img src="{$analog->image->filename|resize:products:30:30}"
                                                                alt="{$analog->image->name}"/>{/if}
                                    </div>
                                    <div class="relatedname">
                                        <input type="hidden" name="analogs_ids[]" value="{$analog->id}"/>
                                        <a href="{$config->root_url}{$module->url}{url add=['id'=>$analog->id]}"
                                           target="_blank">{$analog->name}</a>

                                        <div class="analog-sky">{if $analog->variant}{$analog->variant->sku}{/if}</div>
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    {else}
                        {*<div id="no-analogs-products">Пока нет товаров</div>*}
                    {/if}
                </ol>
            </div>

            <div id="main_list_analogs" class="tags_values {if !$analogs_group_id}hidden{/if}">
                <div class="apsearch form-inline">
                    <input id="searchField_analogs" type="text" class="form-control" placeholder="Поиск" autocomplete="off">
                    <button id="searchCancelButton_analogs" style="display: none;" class="btn btn-default searchremove"
                            type="button"><i class="fa fa-times"></i></button>
                </div>
                <div id="refreshpart_analogs" class="addcontainer">
                    {*style="display:none;">{include file='product-addanalog.tpl'}*}
                </div>
            </div>
        </div>
    </div>
</div>
