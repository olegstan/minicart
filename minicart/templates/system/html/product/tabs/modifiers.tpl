<div ng-class="tab === 'modifiers' ? 'active' : ''" class="tab-pane">
    <div class="btn-group" data-toggle="buttons">
        <label class="btn btn-default {if !$product->modificators_mode || !$product}active{/if}">
            <input type="radio" name="modificators_mode" value=0 {if !$product->modificators_mode || !$product->id}checked{/if}/>  По умолчанию
        </label>
        <label class="btn btn-default {if $product->id && $product->modificators_mode}active{/if} {if $product->is_auto}disabled{/if}">
            <input type="radio" name="modificators_mode" value=1 {if $product->id && $product->modificators_mode}checked{/if}/> Вручную
        </label>
    </div>
    <hr/>
    <div class="row">
        <div class="col-md-6" id="modificators">
            <div class="form-group">
                {foreach $modificators as $m}
                    <div class="checkbox {if !$product->modificators_mode || !$product->id}disabled{/if}">
                        <label>
                            <input type="checkbox" value="{$m->id}" name="modificators[]"
                                    {if $product->modificators_mode}
                                        {if in_array($m->id, $product->modificators)}checked=""{/if}
                                    {else}
                                        {if $main_category && in_array($m->id, $main_category->modificators)}checked=""{/if}
                                    {/if}
                                    {if !$product->modificators_mode || !$product->id}disabled{/if}> {$m->name}
                        </label>
                    </div>
                {/foreach}
            </div>

            <label><strong>Группы модификаторов:</strong></label>
            <div class="form-group">
                {foreach $modificators_groups as $g}
                    <div class="checkbox {if !$product->modificators_mode || !$product->id}disabled{/if}">
                        <label>
                            <input type="checkbox" value="{$g->id}" name="modificators_groups[]"
                                    {if $product->modificators_mode}
                                        {if in_array($g->id, $product->modificators_groups)}checked=""{/if}
                                    {else}
                                        {if $main_category && in_array($g->id, $main_category->modificators_groups)}checked=""{/if}
                                    {/if}
                                    {if !$product->modificators_mode || !$product->id}disabled{/if}> {$g->name}
                        </label>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>
