<div ng-class="tab === 'more' ? 'active' : ''" class="tab-pane">
    <div class="row">
        <div class="col-md-6">
            <legend class="mb10">Авто свойства <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                  data-original-title="Эти теги имеют все товары"
                                                  class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
            </legend>
            {*{if $tags_groups && $product_tags}*}
            {*{assign var="tags_values" value=""}*}
            {*{foreach $tags_groups as $group}*}
            {*{if array_key_exists($group->id, $product_tags) && $group->is_auto}*}
            {*{foreach $product_tags[$group->id] as $t}*}
            {*{if !empty($tags_values)}{assign var="tags_values" value=$tags_values|cat:'#%#'}{/if}*}
            {*{if $group->numeric_sort}*}
            {*{assign var="tags_values" value=$tags_values|cat:($t|tag:true)}*}
            {*{else}*}
            {*{assign var="tags_values" value=$tags_values|cat:($t|tag)}*}
            {*{/if}*}
            {*{/foreach}*}
            {*{/if}*}
            {*{/foreach}*}
            {*{/if}*}
            <input type="hidden" id="autotags" name="product_autotags" data-value="{$tags_values}" value="{$tags_values}" readonly>

            {if $settings->catalog_show_currency}
                <legend class="mb10 mt20">Валюта товара <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                           data-original-title="Можно указать валюту отличную от установленной 'По умолчанию' в этом случае на сайте цена товара будет пересчитана в расчетную валюту по курсу, указанному в настройках валют"
                                                           class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
                </legend>
                <div class="form-group">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default {if !$product || empty($product->currency_id)}active{/if}">
                            <input type="radio" name="currency_id" value="0"
                                   {if !$product || empty($product->currency_id)}checked{/if}> По умолчанию
                        </label>
                        {foreach $all_currencies as $c}
                            <label class="btn btn-default {if $product->currency_id == $c->id}active{/if}">
                                <input type="radio" name="currency_id" value="{$c->id}"
                                       {if $product->currency_id == $c->id}checked{/if}> {$c->sign}
                            </label>
                        {/foreach}
                    </div>
                </div>
            {/if}

            {if $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
                <legend class="mb10 mt20">Дополнительные поля</legend>
                {if $settings->add_field1_enabled}
                    <div class="form-group">
                        <label>{$settings->add_field1_name}</label>
                        <textarea class="form-control" rows="1" name="add_field1">{$product->add_field1}</textarea>
                    </div>
                {/if}

                {if $settings->add_field2_enabled}
                    <div class="form-group">
                        <label>{$settings->add_field2_name}</label>
                        <textarea class="form-control" rows="1" name="add_field2">{$product->add_field2}</textarea>
                    </div>
                {/if}

                {if $settings->add_field3_enabled}
                    <div class="form-group">
                        <label>{$settings->add_field3_name}</label>
                        <textarea class="form-control" rows="1" name="add_field3">{$product->add_field3}</textarea>
                    </div>
                {/if}
            {/if}

            {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled}
                <legend class="mb10 mt20">Дополнительные флаги</legend>
                {if $settings->add_flag1_enabled}
                    <div class="form-group">
                        <label>{$settings->add_flag1_name}</label>
                        <div class="btn-group noinline" data-toggle="buttons">
                            <label class="btn btn-default4 on {if ($product->id && $product->add_flag1) || (!$product->id && $settings->add_flag1_default_value)}active{/if}">
                                <input type="radio" name="add_flag1" value=1 {if ($product->id && $product->add_flag1) || (!$product->id && $settings->add_flag1_default_value)}checked{/if}>Да
                            </label>
                            <label class="btn btn-default4 off {if ($product->id && !$product->add_flag1) || (!$product->id && !$settings->add_flag1_default_value)}active{/if}">
                                <input type="radio" name="add_flag1" value=0 {if ($product->id && !$settings->add_flag1) || (!$product->id && !$settings->add_flag1_default_value)}checked{/if}>Нет
                            </label>
                        </div>
                    </div>
                {/if}

                {if $settings->add_flag2_enabled}
                    <div class="form-group">
                        <label>{$settings->add_flag2_name}</label>
                        <div class="btn-group noinline" data-toggle="buttons">
                            <label class="btn btn-default4 on {if ($product->id && $product->add_flag2) || (!$product->id && $settings->add_flag2_default_value)}active{/if}">
                                <input type="radio" name="add_flag2" value=1 {if ($product->id && $product->add_flag2) || (!$product->id && $settings->add_flag2_default_value)}checked{/if}>Да
                            </label>
                            <label class="btn btn-default4 off {if ($product->id && !$product->add_flag2) || (!$product->id && !$settings->add_flag2_default_value)}active{/if}">
                                <input type="radio" name="add_flag2" value=0 {if ($product->id && !$settings->add_flag2) || (!$product->id && !$settings->add_flag2_default_value)}checked{/if}>Нет
                            </label>
                        </div>
                    </div>
                {/if}

                {if $settings->add_flag3_enabled}
                    <div class="form-group">
                        <label>{$settings->add_flag3_name}</label>
                        <div class="btn-group noinline" data-toggle="buttons">
                            <label class="btn btn-default4 on {if ($product->id && $product->add_flag3) || (!$product->id && $settings->add_flag3_default_value)}active{/if}">
                                <input type="radio" name="add_flag3" value=1 {if ($product->id && $product->add_flag3) || (!$product->id && $settings->add_flag3_default_value)}checked{/if}>Да
                            </label>
                            <label class="btn btn-default4 off {if ($product->id && !$product->add_flag3) || (!$product->id && !$settings->add_flag3_default_value)}active{/if}">
                                <input type="radio" name="add_flag3" value=0 {if ($product->id && !$settings->add_flag3) || (!$product->id && !$settings->add_flag3_default_value)}checked{/if}>Нет
                            </label>
                        </div>
                    </div>
                {/if}
            {/if}

        </div>

        <div class="col-md-6">

            <legend>Статистика</legend>

            <table class="table table-striped table-condensed">
                <tbody>
                <tr>
                    <td>Просмотры</td>
                    <td>1547</td>
                </tr>
                <tr>
                    <td>Покупки</td>
                    <td>14</td>
                </tr>
                <tr>
                    <td>CTR <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                               data-original-title="(Количество просмотров / количество покупок)*100%"><i
                                    class="fa fa-info-circle"></i></a></td>
                    <td>0.9 %</td>
                </tr>
                <tr>
                    <td>Рейтинг <a title="" class="nolink" data-toggle="tooltip" data-placement="bottom" href="#"
                                   data-original-title="Количество оценок">({$product->rating->rating_count})</a></td>
                    <td>{if $product->rating->rating_count > 0}
                            <div class="raiting rate{$product->rating->avg_rating*10}"
                                 title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}">
                            </div>
                        {/if}
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="form-group">
                <label>Дата добавления товара</label>

                <div class="input-group date" id="datepicker">
                    <input class="form-control" type="text" class="span2" name="created_date"
                           value="{if $product}{$product->created_dt|date}{else}{$smarty.now|date}{/if}"/>
                    <span class="input-group-addon add-on"><i class="fa fa-th"></i></span>
                </div>

                {*<input class="form-control" name="created_date" value="{if $product}{$product->created|date}{else}{$smarty.now|date}{/if}">*}
            </div>
            <hr/>
            <label>CSS-класс <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                data-original-title="Стиль товара, выводится в стиле body"><i
                            class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="css_class" placeholder="" value="{$product->css_class}"/>
            <hr/>
            <legend>Файлы</legend>

            {*{include file='object-attachment-placeholder.tpl' object=$product attachments_object_name='products'}*}
        </div>
    </div>
</div>
