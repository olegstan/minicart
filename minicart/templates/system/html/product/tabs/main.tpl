<div ng-class="tab === 'main' ? 'active' : ''" class="tab-pane">
    <div class="row">
        <div class="col-md-9">
            <div class="form-group">
                <label>Название товара</label>
                <div class="input-group product-name">
                    <input type="text" class="product-name-place form-control input-bold" name="name" value="{$product->name|escape_string}"/>
                    <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id товара">id:{$product->id}</a></span>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="statusbar">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $product->is_visible || !$product}active{/if}">
                        <input type="radio" name="is_visible" id="option1" value="1" {if $product->is_visible || !$product}checked{/if}> Активен
                    </label>
                    <label class="btn btn-default4 off {if $product && !$product->is_visible}active{/if}">
                        <input type="radio" name="is_visible" id="option2" value="0" {if $product && !$product->is_visible}checked{/if}> Скрыт
                    </label>
                </div>
                <div class="statusbar-text">Статус:</div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <label>Родительская категория</label>
            <a ng-click="ctrl.addCategory()" href="#" class="btn btn-default btn-sm btn-link dotted">
                <i class="fa fa-plus"></i>
                <span>Добавить категорию</span>
            </a>
            <div id="categories-list">
                {if $product_categories}
                    {foreach $product_categories as $pc}
                        <div class="ui search fluid selection dropdown categories-dropdown-js" >
                            <input type="hidden" name="categories_ids[]" value="{$pc->category_id}">
                            <i class="dropdown icon"></i>
                            <div class="default text">Выберите категорию...</div>
                            <div class="menu">
                                {foreach $all_categories as $c}
                                    <div class="item {if $pc->category_id === $c->id}active selected{/if} category-level-{$c->depth}" data-value="{$c->id}">
                                        <span>{$c->name}</span>
                                    </div>
                                {/foreach}
                            </div>
                        </div>
                    {/foreach}
                {else}
                    <div class="ui search fluid selection dropdown categories-dropdown-js" >
                        <input type="hidden" name="categories_ids[]">
                        <i class="dropdown icon"></i>
                        <div class="default text">Выберите категорию...</div>
                        <div class="menu">
                            {foreach $all_categories as $c}
                                <div class="item category-level-{$c->depth}" data-value="{$c->id}">
                                    {*<img class="ui mini image" src="{$c->image|resize:20:20}">*}
                                    <span>{$c->name}</span>
                                </div>
                            {/foreach}
                        </div>
                    </div>
                {/if}
            </div>
        </div>

        <div class="col-md-5 col-md-offset-1">
            <div class="form-group">
                <label class="label-control">Бренд</label>
                <select name="brand_id" class="ui search selection dropdown" id="brand_id">
                    {foreach $all_brands as $brand}
                        <option value="{$brand->id}" {if $product->brand_id == $brand->id}selected{/if}>{$brand->name}</option>
                    {/foreach}
                </select>
            </div>
        </div>
    </div>

    {*{if $settings->catalog_use_variable_amount}*}
    {*<div class="variable-amount">*}
    {*<div class="variable-amount-header">Покупка товара с переменным количеством</div>*}
    {*<div class="form-group">*}
    {*<label>Использовать покупку с переменным количеством:</label>*}
    {*<div class="btn-group noinline" data-toggle="buttons">*}
    {*<label class="btn btn-default4 on {if ($product->id && $product->use_variable_amount) || !$product->id}active{/if}">*}
    {*<input type="radio" name="use_variable_amount" value=1 {if ($product->id && $product->use_variable_amount) || !$product->id}checked{/if}> Да*}
    {*</label>*}
    {*<label class="btn btn-default4 off {if $product->id && !$product->use_variable_amount}active{/if}">*}
    {*<input type="radio" name="use_variable_amount" value=0 {if $product->id && !$product->use_variable_amount}checked{/if}> Нет*}
    {*</label>*}
    {*</div>*}
    {*</div>*}

    {*<div id="variable_amount_panel" {if $product->id && !$product->use_variable_amount}style="display:none;"{/if}>*}
    {*<div class="row">*}
    {*<div class="col-md-3">*}
    {*<div class="form-group">*}
    {*<label>Минимальное количество</label>*}
    {*<input type="text" class="form-control" name="min_amount" placeholder="" value="{if $product->use_variable_amount}{$product->min_amount}{else}{$settings->catalog_min_amount}{/if}">*}
    {*</div>*}
    {*</div>*}
    {*<div class="col-md-3">*}
    {*<div class="form-group">*}
    {*<label>Максимальное количество</label>*}
    {*<input type="text" class="form-control" name="max_amount" placeholder="" value="{if $product->use_variable_amount}{$product->max_amount}{else}{$settings->catalog_max_amount}{/if}">*}
    {*</div>*}
    {*</div>*}
    {*<div class="col-md-3">*}
    {*<div class="form-group">*}
    {*<label>Шаг изменения</label>*}
    {*<input type="text" class="form-control" name="step_amount" placeholder="" value="{if $product->use_variable_amount}{$product->step_amount}{else}{$settings->catalog_step_amount}{/if}">*}
    {*</div>*}
    {*</div>*}
    {*</div>*}
    {*</div>*}
    {*</div>*}
    {*{/if}*}

    <legend>Варианты</legend>
    <div class="well productvarariants">
        <div class="headlines">
            <div class="variant-name">
                Название варианта
            </div>
            <div class="variant-sku">
                Артикул
            </div>
            <div class="variant-price">
                Цена
            </div>
            <div class="variant-old-price">
                Старая цена
            </div>
            <div class="variant-count">
                Кол-во <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="'&#8734;' или любое положительное число - статус товара 'В наличии', '0' - статус товара 'Нет в наличии', '-1' - статус товара 'Под заказ'" class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
            </div>
        </div>

        <div class="dd">
            <ol id="variants-list" class="dd-list products">
                {if $product->variants}
                    {foreach $product->variants as $variant}
                        {include file="product/variant.tpl" variant=$variant}
                    {/foreach}
                {else}
                    {include file="product/variant.tpl"}
                {/if}
            </ol>
            <a href="#" ng-click="ctrl.addVariant()" class="btn btn-default btn-sm margin-top-10 bigi">
                <i class="fa fa-plus"></i>Добавить вариант
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <legend>Свойства
                <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Товар может иметь несколько значений из одного и того же свойства, например 2 ширины или 3 длинны, просто вводите значения по порядку в одну и ту же строку" class="fa fa-legend">
                    <i class="fa fa-info-circle"></i>
                </a>
            </legend>

            {* Блок для логических тегов начало *}
            {*<div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-sm btn-default4 on active">
                        <input type="radio" name="visible" id="option1" value=1 checked>  Да
                      </label>
                      <label class="btn btn-sm btn-default4 off active">
                        <input type="radio" name="visible" id="option2" value=0 {if $product && !$product->is_visible}checked{/if}> Нет
                      </label>
                    </div>*}

            {* Блок для логических тегов конец *}


            <table id="taggroups" class="table table-striped">
                <tbody>
                {if $tags_groups && $product_tags}
                    {foreach $product_tags_positions as $group_id}
                        {$group = $tags_groups[$group_id]}
                        {if !$group->is_auto}
                            <tr data-group="{$group->id}">
                                <td class="col-md-3">{$group->name}{if $group->postfix}, {$group->postfix}{/if}
                                    <a onclick="angular.element(this).scope().removeTagGroup(this)" class="delete-property"><i class="fa fa-times"></i></a>
                                </td>
                                <td class="col-md-9">
                                    {if $group->mode == "logical"}
                                        <div class="btn-group" data-toggle="buttons">
                                            <label class="btn btn-sm btn-default4 on {if $tags_values == 'да' || $tags_values == 'Да'}active{/if}">
                                                <input type="radio" name="tags[{$group->id}][]" value="да" {if $tags_values == 'да' || $tags_values == 'Да'}checked{/if}> Да
                                            </label>
                                            <label class="btn btn-sm btn-default4 off {if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}active{/if}">
                                                <input type="radio" name="tags[{$group->id}][]" value="нет"
                                                       {if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}checked{/if}> Нет
                                            </label>
                                        </div>
                                    {elseif $group->mode == "text"}
                                        <textarea class="form-control" name="tags[{$group->id}][]" data-group-id="{$group->id}">{$tags_values}</textarea>
                                    {else}
                                        <select name="tags[{$group->id}][]" class="ui selection search dropdown col-md-9 tags-dropdown-js" multiple="">
                                            {foreach $group->tags as $tag}
                                                <option value="{$tag->name}" {if $tag->selected}selected="selected"{/if}>{$tag->name}</option>
                                            {/foreach}
                                        </select>
                                    {/if}
                                </td>
                            </tr>
                        {/if}

                        {*{foreach $product_tags_positions as $group_id}*}
                        {*{$group = $tags_groups[$group_id]}*}
                        {*{if array_key_exists($group->id, $product_tags) && !$group->is_auto}*}
                        {*{assign var="tags_ids" value=""}*}
                        {*{assign var="tags_values" value=""}*}

                        {*<tr data-group="{$group->id}">*}
                        {*<td class="col-md-3">{$group->name}{if $group->postfix}, {$group->postfix}{/if}*}
                        {*<a href="#" class="delete-property"><i class="fa fa-times"></i></a>*}
                        {*</td>*}
                        {*<td class="col-md-9">*}
                        {*{if $group->mode == "logical"}*}
                        {*<div class="btn-group" data-toggle="buttons">*}
                        {*<label class="btn btn-sm btn-default4 on {if $tags_values == 'да' || $tags_values == 'Да'}active{/if}">*}
                        {*<input type="radio" name="product_tags[{$group->id}]" value="да" {if $tags_values == 'да' || $tags_values == 'Да'}checked{/if}> Да*}
                        {*</label>*}
                        {*<label class="btn btn-sm btn-default4 off {if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}active{/if}">*}
                        {*<input type="radio" name="product_tags[{$group->id}]" value="нет"*}
                        {*{if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}checked{/if}> Нет*}
                        {*</label>*}
                        {*</div>*}
                        {*{elseif $group->mode == "text"}*}
                        {*<textarea class="form-control" name="product_tags[{$group->id}]" data-group-id="{$group->id}">{$tags_values}</textarea>*}
                        {*{else}*}
                        {*<input type="hidden" name="product_tags[{$group->id}]" data-group-id="{$group->id}" value="{$tags_values}">*}
                        {*{/if}*}
                        {*</td>*}
                        {*</tr>*}
                        {*{/if}*}
                    {/foreach}
                {/if}
                </tbody>

                <tfoot>
                {* НОВАЯ ГРУППА ТЕГОВ *}
                <tr id="new_tag_group" style="display:none;">
                    <td class="col-md-3">Выберите свойство</td>
                    <td class="col-md-9">
                        {*<div class="row">
                        <div class="col-md-9">*}
                        <select id="new_group_id" class="form-control">
                        </select>
                        {*</div>
                        <div class="col-md-3">
                        <a id="add_tag_group" href="#" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Добавить свойство"><i class="fa fa-plus"></i></a>
                        <a id="cancel_tag_group" href="#" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Отменить"><i class="fa fa-times"></i></a>
                        </div>*}
                        {*</div>*}
                    </td>
                </tr>
                </tfoot>
                {* НОВАЯ ГРУППА ТЕГОВ *}
            </table>
            <a href="#" class="btn btn-default btn-sm" id="add_new_tag_group" data-toggle="modal"><i class="fa fa-plus"></i>
                Добавить свойство</a>

            <div class="form-group">
                <label class="bigname">Краткое описание
                    <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается в списке товаров">
                        <i class="fa fa-info-circle"></i>
                    </a>
                </label>
                <textarea name="annotation" class="form-control ckeditor" rows="10">{$product->annotation}</textarea>
            </div>

            <div class="form-group">
                <label class="bigname">Краткое описание 2 <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается в карточке товара сверху (это поле опционально)">
                        <i class="fa fa-info-circle"></i>
                    </a>
                </label>
                <textarea name="annotation2" class="form-control ckeditor" rows="10">{$product->annotation2}</textarea>
            </div>

            <div class="form-group">
                <label class="bigname">Полное описание
                    <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается в карточке товара снизу">
                        <i class="fa fa-info-circle"></i>
                    </a>
                </label>
                <textarea name="body" class="form-control ckeditor" rows="10">{$product->body}</textarea>
            </div>
        </div>

        <div class="col-md-4">
            <legend>Изображения</legend>

            {include file='object-image-placeholder.tpl' object=$product images_object_name='products'}

            <legend class="mb10">Бейджи
                <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Вы можете добавить бейдж для товара, нарпимер Новинка или Хит продаж или сразу несколько" class="fa fa-legend">
                    <i class="fa fa-info-circle"></i>
                </a>
            </legend>
            {if $product_badges}
                {assign var="badges_values" value=""}
                {foreach $product_badges as $b}
                    {if !empty($badges_values)}{assign var="badges_values" value=$badges_values|cat:'#%#'}{/if}
                    {assign var="badges_values" value=$badges_values|cat:($b->name)}
                {/foreach}
            {/if}
            <input type="hidden" id="badges" name="product_badges" data_value="{$badges_values}" value="{$badges_values}"/>

            <hr/>

            {if $product->id}
                <button name="delete" ng-click="ctrl.deleteProduct();" class="btn btn-default w100"><i class="fa fa-times"></i> Удалить товар</button>
            {/if}

        </div>
    </div>
    {* Основное описание товара конец *}
</div>
