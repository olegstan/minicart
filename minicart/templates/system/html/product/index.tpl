{* Title *}
{*{$meta_title='Товары' scope=parent}*}
<div id="main_list" class="tags_values" ng-controller="ProductController as ctrl">
    <div class="controlgroup form-inline">
        <div class="currentfilter products">
            <div class="row">
                <div class="col-md-8">
                    <div class="input-group">
                        <input id="searchField" type="text"  class="form-control" placeholder="Поиск">
                        <span class="input-group-btn">
                            <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                            <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="search_in_current_category"> Искать в текущей категории
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <a href="/admin/product/create" target="_self" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
        {*<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>*}
    </div>
    <div class="boxes {if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
        <div class="box-left-8">
            <div class="box-left-inside">
                <div class="box-main-header">
                    Товары <i class="fa fa-spinner fa-spin fa-large" id="products-spinner"></i>
                    <div class="expandvariants">
                        <div class="btn-group">
                            <button type="button" data-action="expand-all" class="btn btn-xs btn-link" id="expand-all-variants"><i class="fa fa-plus"></i> <span>Развернуть варианты</span></button>
                            <button type="button" data-action="collapse-all" class="btn btn-xs btn-link" id="collapse-all-variants"><i class="fa fa-minus"></i> <span>Свернуть варианты</span></button>
                        </div>
                    </div>
                </div>

                <div id="refreshpart" class="dd">
                    <div ng-include="'minicart/templates/system/js-template/product/products.html'"></div>
                </div>


            {*{$allow_edit_module = false}*}
            {*{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}*}
            {if $allow_edit_module}
            <div class="form-inline massoperations">
                <div class="checkall">Выделить все ↑</div>
                <select id="mass_operation" class="form-control">
                    <option value="0">Выберите действие</option>
                    <optgroup label="Цена и валюта">
                        <option value="15">Изменить цену</option>
                        <option value="12">Добавить/Убрать скидку</option>
                        {if $settings->catalog_show_currency}<option value="16">Изменить валюту</option>{/if}
                    </optgroup>
                    <optgroup label="Свойства товара">
                        <option value="8">Добавить значения свойств</option>
                        <option value="9">Удалить значения свойств</option>
                    </optgroup>
                    <optgroup label="Бренды">
                        <option value="6">Указать бренд</option>
                        <option value="7">Удалить бренд</option>
                    </optgroup>
                    <optgroup label="Категории">
                        <option value="4">Переместить в категорию</option>
                        <option value="5">Указать дополнительную категорию</option>
                    </optgroup>
                    {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled || $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
                    <optgroup label="Доп. поля и флаги">
                        {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled}
                        <option value="17">Установить значение доп. флагов</option>
                        {/if}
                        {if $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
                        <option value="18">Установить значение доп. полей</option>
                        {/if}
                    </optgroup>
                    {/if}
                    <optgroup label="Основные">
                        <option value="10">Установить количество</option>
                        <option value="1">Сделать видимыми</option>
                        <option value="2">Скрыть</option>
                        <option value="11">Удалить</option>
                    </optgroup>
                    {*<option value="3">Переместить на страницу</option>*}
                </select>
                <select id="mass_operation_set_brand" class="form-control" style="display:none;">
                    {foreach $all_brands as $brand}
                        <option value="{$brand->id}">{$brand->name}</option>
                    {/foreach}
                </select>
                <select id="mass_operation_add_tag_group" class="form-control" style="display:none;">
                    {*foreach $tags_groups as $group}
                        {if $group->is_auto == 0}
                        <option value="{$group->id}">{$group->name}</option>
                        {/if}
                    {/foreach*}
                </select>
                <div id="mass_operation_add_tag_value" style="display:none;">
                    <input type="hidden" name="add_tags" value="">
                </div>
                <select id="mass_operation_remove_tag_group" class="form-control" style="display:none;">
                    {foreach $tags_groups as $group}
                        {if $group->is_auto == 0}
                        <option value="{$group->id}">{$group->name}</option>
                        {/if}
                    {/foreach}
                </select>
                <div id="mass_operation_remove_tag_value" style="display:none;">
                    <input type="hidden" name="remove_tags" value="">
                </div>
                <select id="mass_operation_move_to_page" class="form-control" style="display:none;">
                    {section name=pages start=1 loop=$total_pages_num+1 step=1}
                    <option>{$smarty.section.pages.index}</option>
                    {/section}
                </select>
                <select id="mass_operation_move_to_category" class="form-control" style="display:none;">

                </select>
                <select id="mass_operation_add_category" class="form-control" style="display:none;">

                </select>
                <div id="mass_operation_set_stock" style="display:none;">
                    <input type="text" name="stock" class="form-control" value="∞">
                </div>
                {*<div id="mass_operation_set_discount" style="display:none;">
                    <input type="text" name="discount" class="form-control" value="0">
                </div>
                <div id="mass_operation_set_fix_discount" style="display:none;">
                    <input type="text" name="fix_discount" class="form-control" value="0">
                </div>*}
                <select id="mass_operation_set_discount" class="form-control" style="display:none;">
                    <option value="set_discount">Сделать скидку в %</option>
                    <option value="set_fix_discount">Сделать фиксированную скидку</option>
                    <option value="remove_discount">Убрать скидку</option>
                </select>
                <input id="mass_operation_set_discount_input" type="text" class="form-control" value="" style="display:none;"/>

                <select id="mass_operation_change_price" class="form-control" style="display:none;">
                    <option value="set_price">Установить цену</option>
                    <option value="inc_fix_price">Увеличить на фиксированную сумму</option>
                    <option value="sub_fix_price">Уменьшить на фиксированную сумму</option>
                    <option value="inc_percent">Увеличить на %</option>
                    <option value="sub_percent">Уменьшить на %</option>
                </select>
                <input id="mass_operation_change_price_input" type="text" class="form-control" value="" style="display:none;"/>
                <select id="mass_operation_set_currency" class="form-control" style="display:none;">
                    {foreach $currencies as $currency}
                        <option value="{$currency->id}">{$currency->name}</option>
                    {/foreach}
                </select>

                <select id="mass_operation_select_add_flag" class="form-control" style="display:none;">
                    {if $settings->add_flag1_enabled}<option value="add_flag1">{$settings->add_flag1_name}</option>{/if}
                    {if $settings->add_flag2_enabled}<option value="add_flag2">{$settings->add_flag2_name}</option>{/if}
                    {if $settings->add_flag3_enabled}<option value="add_flag3">{$settings->add_flag3_name}</option>{/if}
                </select>
                <div class="btn-group noinline" data-toggle="buttons" id="mass_operation_select_add_flag_input" style="display:none;">
                    <label class="btn btn-default4 on active">
                        <input type="radio" name="mass_operation_select_add_flag_input" value="1" checked="">Да
                    </label>
                    <label class="btn btn-default4 off ">
                        <input type="radio" name="mass_operation_select_add_flag_input" value="0">Нет
                    </label>
                </div>

                <select id="mass_operation_select_add_field" class="form-control" style="display:none;">
                    {if $settings->add_field1_enabled}<option value="add_field1">{$settings->add_field1_name}</option>{/if}
                    {if $settings->add_field2_enabled}<option value="add_field2">{$settings->add_field2_name}</option>{/if}
                    {if $settings->add_field3_enabled}<option value="add_field3">{$settings->add_field3_name}</option>{/if}
                </select>
                <input id="mass_operation_select_add_field_input" type="text" class="form-control" value="" style="display:none;"/>

                <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
            </div>
            {/if}
        </div>
    </div>

    <div class="box-right-4">

        <div ng-include="'minicart/templates/system/js-template/product/categories.html'"></div>

        <div class="panel-brands">
          <div class="box-header">
              {*<a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle" id="brands_toggle">*}
                {*{if $settings->brands_auto_open}<i class="fa fa-minus"></i>{else}<i class="fa fa-plus"></i>{/if} Бренды*}
                {**}
                {*</a>*}

          </div>
        </div>
    </div>

</div>