<li class="dd-item dd3-item {if isset($variant) && !$variant->is_visible}disabledvar{/if}" data-id="{$variant->id}">
    <div class="dd-handle dd3-handle"></div>
    <div class="dd3-content form-inline">
        <input type="hidden" name="variants[{(isset($variant))?$variant->id:$key}][id]" value="{(isset($variant->id))?$variant->id:''}"/>
        <input type="text" name="variants[{(isset($variant))?$variant->id:$key}][name]" value="{(isset($variant->name))?$variant->name:''}" placeholder="Название варианта" class="form-control variant-name">
        <input type="text" name="variants[{(isset($variant))?$variant->id:$key}][sku]" value="{(isset($variant->sku))?$variant->sku:''}" placeholder="Артикул" class="form-control variant-sku">
        <input type="text" name="variants[{(isset($variant))?$variant->id:$key}][price]" value="{(isset($variant->price))?$variant->price:''}" placeholder="Цена" class="form-control variant-price">
        <input type="text" name="variants[{(isset($variant))?$variant->id:$key}][price_old]" value="{(isset($variant->price_old))?$variant->price_old:''}" placeholder="Старая цена" class="form-control variant-old-price" title="Старая цена">
        <input type="text" name="variants[{(isset($variant))?$variant->id:$key}][stock]" value="{if (isset($variant) && !isset($variant->stock)) || !isset($variant)}∞{else}{$variant->stock|escape}{/if}" placeholder="Количество" class="form-control variant-count" title="Количество товара на складе">
        <input type="hidden" data-field="is_visible" name="variants[{(isset($variant))?$variant->id:$key}][is_visible]" value="{(isset($variant))?$variant->is_visible:1}"/>
        <div class="controllinks">
            <a onclick="angular.element(this).scope().removeVariant(this)" class="delete-item" href="#">
                <i class="fa fa-times"></i>
            </a>
            <a onclick="angular.element(this).scope().toggleVisibleVariant(this)" class="toggle-item {if $variant->is_visible || !isset($variant)}light-on{else}light-off{/if}" href="#"></a>
        </div>
    </div>
</li>