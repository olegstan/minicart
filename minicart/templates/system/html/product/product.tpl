{* Title *}

{*{if $product}*}
    {*{$meta_title='Редактирование товара' scope=parent}*}
{*{else}*}
    {*{$meta_title='Добавление товара' scope=parent}*}
{*{/if}*}

{*<form onkeypress="angular.element(this).scope().preventSubmit(event, this)" method="post" id="product" action="/admin/product/save{if $product}/{$product->id}{else}{/if}" ng-controller="ProductController as ctrl" data-object-id="{$product->id}">*}
{*<form onkeypress="angular.element(this).scope().preventSubmit(event)" method="post" id="product" action="/admin/product/save{if $product}/{$product->id}{else}{/if}" ng-controller="ProductController as ctrl" data-object-id="{$product->id}">*}
<form method="post" id="product" action="/admin/product/save{if $product}/{$product->id}{else}{/if}" ng-controller="ProductController as ctrl" data-object-id="{$product->id}">
    <div class="controlgroup">
        {if $product}
            <div class="openlink">
                <a href="{$product->front_url}" target="_blank"><i class="fa fa-external-link"></i> <span>Открыть товар в новом окне</span></a>
            </div>
        {/if}
        <button type="submit" name="save"  class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</button>
        <button type="submit" name="save_and_close" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</button>
        <button type="submit" name="save_and_create" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</button>
        <button type="submit" name="cancel" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</button>
    </div>


    <div class="block-header">{if $product}Редактирование товара <span>{$product->name}</span>{else}Добавление товара{/if} </div>

    <ul class="nav nav-tabs mini-tabs" role="tablist" id="tablist">
        <li ng-class="tab === 'main' ? 'active' : ''"><a ng-click="ctrl.changeTab('main')"><span>Основное</span></a></li>
        <li ng-class="tab === 'related' ? 'active' : ''"><a ng-click="ctrl.changeTab('related')"><span>Сопутствующие товары, аналоги, комплекты</span></a></li>
        <li ng-class="tab === 'seo' ? 'active' : ''"><a ng-click="ctrl.changeTab('seo')"><span>SEO</span></a></li>
        <li ng-class="tab === 'modifiers' ? 'active' : ''"><a ng-click="ctrl.changeTab('modifiers')"><span>Модификаторы цены</span></a></li>
        <li ng-class="tab === 'more' ? 'active' : ''"><a ng-click="ctrl.changeTab('more')"><span>Дополнительно</span></a></li>
    </ul>

    <div class="tab-content">
        {include file="product/tabs/main.tpl"}
        {include file="product/tabs/related.tpl"}
        {include file="product/tabs/seo.tpl"}
        {include file="product/tabs/modifiers.tpl"}
        {include file="product/tabs/more.tpl"}
    </div>


    <input type="hidden" name="recreate_seo" value="0"/>
    <input type="hidden" name="attachments_position" value=""/>
    <input type="hidden" name="create_analogs_group" value="0"/>
    <input type="hidden" name="change_analogs_group" value="0"/>
    <input type="hidden" name="analogs_group_id" value="{$analogs_group_id}"/>
    {*{if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}*}
    {*{if $page}<input type="hidden" name="page" value="{$page}"/>{/if}*}
    {*{if $category_id}<input type="hidden" name="category_id" value="{$category_id}"/>{/if}*}
    {*{if $sort}<input type="hidden" name="sort" value="{$sort}"/>{/if}*}
    {*{if $sort_type}<input type="hidden" name="sort_type" value="{$sort_type}"/>{/if}*}
</form>