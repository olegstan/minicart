<div class="ui search fluid selection dropdown categories-dropdown-js" >
    <input type="hidden" name="categories_ids[]">
    <i class="dropdown icon"></i>
    <div class="default text">Выберите категорию...</div>
    <div class="menu">
        {foreach $all_categories as $c}
            <div class="item category-level-{$c->depth}" data-value="{$c->id}">
                {*<img class="ui mini image" src="{$c->image|resize:20:20}">*}
                <span>{$c->name}</span>
            </div>
        {/foreach}
    </div>
</div>