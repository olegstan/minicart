<form class="form-signin" method="post" id="admin-login-form">
    <fieldset>
        <div class="login-logo"></div>

        <div class="form-group">
            <input type="email" class="form-control" name="username" placeholder="Почта или Номер мобильного" value={$username}>
            <div class="help-block-error"  id="name-required" style="display:none;">Введите e-mail в формате: ivanov@gmail.com<br/>
                или телефон в формате: +79150000000
            </div>
        </div>
        <div class="form-group">
            <input type="password" class="form-control" name="password" placeholder="Пароль" value="">

            <div class="help-block-error" id="password-required" style="display:none;">Введите пароль</div>

        </div>
        <button class="btn btn-primary w100" type="submit">Войти</button>
        {if $error_message}
            <div class="alert alert-danger">
                {if $error_message == "account_disabled"}Учетная запись отключена.
                {elseif $error_message == "access_denied"}У Вас нет доступа к этому модулю.
                {elseif $error_message == "auth_error"}Почта, телефон или пароль введены неверно.
                {else}{$error_message|escape}
                {/if}
            </div>
        {/if}
    </fieldset>
</form>