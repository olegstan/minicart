<div id="main_list" class="modificators">
    <form id="form-menu" method="post" action="">
        <div class="controlgroup form-inline">
            <a href="" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="ModificatorOrderController as ctrl">
                <div ng-include="'minicart/templates/system/js-template/catalog/modificators-orders.html'"></div>
            </div>
            
            {*<div class="col-md-4">*}
                {*<div class="list-group">*}
                  {*<a href="{$config->root_url}{$module->url}" class="list-group-item {if !array_key_exists('parent_id', $params_arr)}active{/if}">Все модификаторы заказов</a>*}
                  {*{foreach $modificators_groups as $group}*}
                    {*<a href="{$config->root_url}{$module->url}{url add=['parent_id'=>$group->id]}" class="list-group-item {if $params_arr['parent_id'] == $group->id}active{/if}"><i class="fa fa-angle-right"></i> {$group->name} <div class="label-modifier"><span>{if $group->type == 'checkbox'}Галочки (Чекбоксы){elseif $group->type == 'radio'}Радиобаттон{elseif $group->type == 'select'}Выпадающий список{/if}</span></div></a>*}
                  {*{/foreach}*}
                {*</div>*}
            {*</div>*}
        </form>
    </div>
</div>