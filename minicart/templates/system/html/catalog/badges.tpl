<div id="main_list" class="badges">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup form-inline">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['category_id'=>$category_id]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="BadgeController as ctrl">
                <div ng-include="'minicart/templates/system/js-template/catalog/badges.html'"></div>
            </div>
        </div>
    </form>
</div>