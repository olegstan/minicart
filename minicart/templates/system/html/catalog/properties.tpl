<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <div class="controlgroup">
            <div class="currentfilter">
                <div class="input-group">
                    <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                    <span class="input-group-btn">
                        {*{if $keyword}*}
                            <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                        {*{/if}*}
                        <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['return_mode'=>$mode]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="PropertyController as ctrl">

                <div ng-include="'minicart/templates/system/js-template/catalog/properties.html'"></div>

            </div>

            <div class="col-md-4">
                <div class="list-group">
                    <a href="{$config->root_url}{$module->url}{url add=['mode'=>'user-properties']}" class="list-group-item {if $mode=="user-properties" || !$mode}active{/if}">Пользовательские свойства</a>
                    <a href="{$config->root_url}{$module->url}{url add=['mode'=>'auto-properties']}" class="list-group-item {if $mode=="auto-properties"}active{/if}">Автоматические свойства</a>
                </div>
            </div>
        </div>
    </form>
</div>