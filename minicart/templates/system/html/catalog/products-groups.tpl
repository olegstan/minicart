<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="">
        <div class="controlgroup">
            <a href="" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="ProductGroupController as ctrl">
                <div ng-include="'minicart/templates/system/js-template/catalog/products-groups.html'"></div>
            </div>

            <div class="col-md-4">

            </div>
        </div>
    </form>
</div>