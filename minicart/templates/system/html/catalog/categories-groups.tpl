<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">

            <a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="CategoryGroupController as ctrl">
                <div ng-include="'minicart/templates/system/js-template/catalog/categories-groups.html'"></div>
            </div>

            <div class="col-md-4">

            </div>
        </div>
    </form>
</div>