<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">


        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {*{if $keyword}*}
                    {*<button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>*}
                {*{/if}*}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
            </div>

            <a href="" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8" ng-controller="FilterCategoryController as ctrl">
                <div ng-include="'minicart/templates/system/js-template/catalog/modificators.html'"></div>
            </div>
        </div>
    </form>
</div>