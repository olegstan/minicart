<div class="row mt20">
    <div class="col-md-6">
        <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Последние заказы</h3>
                <a href="" class="btn btn-default btn-xs">Все заказы</a>
            </div>
            <div class="panel-body">
                <table class="table lastorders">
                    <tbody>
                        {if $order_status->orders}
                            {foreach $order_status->orders as $order}
                                <tr>
                                    <td>{$order->day_str}</td>
                                    <td>{$order->created_t}</td>
                                    <td><a href="">Заказ №{$order->id}</a></td>
                                    <td>{$order->name|truncate:14:"...":true}</td>
                                    {*<td>{$order->total_price|convert} {$main_currency->sign}</td>*}
                                    <td><a href="" class="btn btn-default btn-xs">Смотреть заказ</a></td>
                                </tr>
                            {/foreach}
                        {else}
                            Нет заказов
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Заказы звонка</h3>
                <a href="" class="btn btn-default btn-xs">Все заказы звонка</a>
            </div>
            <div class="panel-body">
                <table class="table lastorders">
                    <tbody>
                        {if $callbacks}
                            {foreach $callbacks as $callback}
                                <tr>
                                    <td>{$callback->day_str}</td>
                                    <td>{$callback->created_t}</td>
                                    <td>№{$callback->id}</td>
                                    <td>{$callback->user_name}</td>
                                    {*<td>{if $callback->phone_code && $callback->phone}+7 ({$callback->phone_code}) {$callback->phone|phone_mask}{/if}</td>*}
                                </tr>
                            {/foreach}
                        {else}
                            Нет заказов звонка
                        {/if}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Отзывы о товарах</h3>
                <a href="" class="btn btn-default btn-xs">Все отзывы о товарах</a>
            </div>
            <div class="panel-body">
                <table class="table lastorders">
                    <tbody>
                        {if $reviews}
                            {foreach $reviews as $review}
                                <tr>
                                    <td>{$review->day_str}</td>
                                    <td>{$review->created_t}</td>
                                    <td><a href="">Отзыв №{$review->id}</a></td>
                                    <td><div class="raiting rate{$review->avg_rating*10}" title="Рейтинг товара {$review->rating|string_format:'%.1f'}"></div></td>
                                    <td>{$review->name}</td>
                                </tr>
                            {/foreach}
                        {else}
                            Нет отзывов
                        {/if}
                    <tbody>
                </table>
            </div>
        </div>
    </div>
</div>

