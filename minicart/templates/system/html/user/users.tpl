<div id="main_list" class="tags" ng-controller="UserController as ctrl">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <div class="controlgroup">
            <div class="currentfilter">
                <div class="input-group">
                    <input id="searchField" type="text" class="form-control" placeholder="Поиск" value="{$keyword}">
                    <span class="input-group-btn">
                    <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                    <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['group_id'=>$params_arr['group_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-9">
                <legend>Пользователи</legend>

                <div id="refreshpart" class="dd">
                    <div ng-include="'minicart/templates/system/js-template/user/users.html'"></div>
                </div>

            {* Массовые операции для пользователей *}
                {if $allow_edit_module}
                    <div class="form-inline massoperations">
                        <div class="checkall">Выделить все &uarr;</div>
                        <select id="mass_operation" class="form-control">
                            <option value='1'>Сделать видимыми</option>
                            <option value='2'>Скрыть</option>
                            <option value='3'>Удалить</option>
                        </select>
                        <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                    </div>
                {/if}
            </div>

            <div class="col-md-3">
                <div ng-include="'minicart/templates/system/js-template/user/users-groups.html'"></div>

                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#help">
                                    Помощь по разделу
                                </a>
                            </h4>
                        </div>
                        <div id="help" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>Поиск пользователей идет по 3 параметрам:</p>
                                <ol>
                                    <li>Имя пользователя;</li>
                                    <li>Почта в формате ivanov@mail.ru;</li>
                                    <li>Телефон в формате +79171234567;</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>