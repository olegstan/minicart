{* Title *}
{$meta_title='Редактирование пункта меню' scope=parent}

<form method=post id="menu_item">

        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{$menu_id}/" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

    <fieldset>
    
    <div class="row">
        <div class="col-md-8">
        
        <legend>{if $menu_item}Редактирование пункта меню{else}Добавление пункта меню{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Название пункта меню</label>
        <input type="text" class="form-control" name="name" placeholder="Введите название пункта меню" value="{$menu_item->name|escape_string}">
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $menu_item->is_visible || !$menu_item}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $menu_item->is_visible || !$menu_item}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $menu_item && !$menu_item->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $menu_item && !$menu_item->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <label>Уровень</label>
        
        <select name="parent_id" class="form-control">
            <option value='0'>Корень</option>
            {function name=parent_select level=0}
                {foreach from=$items item=item}
                    {if $menu_item->id != $item->id}
                        <option value='{$item->id}' {if $menu_item->parent_id == $item->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                        {parent_select items=$item->subitems level=$level+1}
                    {/if}
                {/foreach}
            {/function}
            {parent_select items=$menu_items->subitems}
        </select>
         
        </div>
        
        <div class="form-group">
        <label>CSS-класс</label>
        
        <input type="text"  class="form-control" name="css_class" class="form-control" placeholder="Введите CSS-класс" value="{$menu_item->css_class}">
        
        </div>
        
    <label class="checkbox">
          <input type="checkbox" name="use_default" id="use_default" value="1" {if $menu_item->use_default}checked{/if}> <label for="use_default" class="{if $menu_item->use_default}checked{else}notchecked{/if}">Открывать по умолчанию</label>
        </label>
        
        </div>
              
  <div class="col-md-6">
  <label>Иконка Fontawesome</label>
  <input type="text"  class="form-control" name="icon" class="form-control" placeholder="" value="{$menu_item->icon|escape_string}">
  <p class="help-block">Например: < i class="fa fa-level-up" > < /i ></p>
  </div>  
  </div>
        
        <input type="hidden" name="id" value="{$menu_item->id}"/>
        <input type="hidden" name="menu_id" value="{if $menu_item->id}{$menu_item->menu_id}{else}{$menu_id}{/if}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>
</form>






<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>