{*

Шаблон для вывода аттачей объекта
Входные параметры:
    $object - объект
    $module - модуль
    $attachments - аттачи
    $attachments_object_name - название объекта для которого переданы аттачи

*}

<div id="files" class="dd">
    {if $attachments}
    <ol class="dd-list">
        {foreach $attachments as $attach}
            <li class="dd-item dd3-item files" data-id="{$attach->id}">
                <div class="dd-handle dd3-handle"></div>
                <div class="dd3-content">
                    <div class="delfile">
                        {if $object->id}
                            <a href="{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'delete_attachment', 'object'=>$attachments_object_name, 'ajax'=>1, 'attachment_id'=>$attach->id]}" class="delete_attachment"><i class="fa fa-times"></i></a>
                        {else}
                            <a href="{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'delete_attachment', 'object'=>$attachments_object_name, 'ajax'=>1, 'attachment_id'=>$attach->id]}" class="delete_attachment"><i class="fa fa-times"></i></a>
                        {/if}
                    </div>
                    <div class="fileimage">
                        {$exts = ','|explode:"bmp,doc,docx,jpeg,jpg,pdf,png,ppt,pptx,psd,rar,xls,xlsx,zip"}
                        {if in_array($attach->extension, $exts)}
                            <img src="{$path_admin_template}img/fileext/{$attach->extension}.png">
                        {else}
                            <img src="{$path_admin_template}img/fileext/other.png">
                        {/if}
                    </div>

                    <div class="filename">
                        <a href="{$attach->filename|attachment:$attachments_object_name}" target="_blank" class="file-namelink">
                            {$attach->filename}
                        </a> <a class="file-settings"><i class="fa fa-cog"></i></a>
                        {if $attach->name}
                            <span>{$attach->name}</span>
                        {/if}


                        <div class="input-group hidden">
                            <input type="text" class="form-control input-sm" placeholder="Введите название файла" name="attachment_name[{$attach->id}]" value="{$attach->name}"/>
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-sm save-attachment-name" type="button"><i class="fa fa-check"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </li>
        {/foreach}
    </ol>
    {/if}
</div>