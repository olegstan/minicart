{* Title *}
{$meta_title='Редактирование пользователя' scope=parent}

<form method=post id="user">
<fieldset>
<div class="controlgroup">
    <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
    <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
    <a href="{$config->root_url}{$main_module->url}{url add=['group_id'=>$group_id]}"
       class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
</div>

<div class="block-header">{if $user_item}Редактирование пользователя{else}Добавление пользователя{/if}</div>

<ul class="nav nav-tabs mini-tabs" role="tablist">
    <li class="active"><a href="#main" role="tab" data-toggle="tab"><span>Основное</span></a></li>
    <li><a href="#usersorders" role="tab" data-toggle="tab"><span>Заказы пользователя {$orders_count}</span></a>
    </li>
    <li><a href="#bankdetails" role="tab" data-toggle="tab"><span>Банковские реквизиты</span></a></li>
</ul>

<div class="tab-content">
<div class="tab-pane active" id="main">

    <div class="row">
        <div class="col-md-8">


            <div class="row">
                <div class="col-md-8">
                    <div class="form-group">
                        <label>Имя пользователя</label>

                        <div class="input-group">
                            <input type="text" class="form-control" name="name" placeholder="Введите имя пользователя"
                                   value="{$user_item->name|escape_string}"/>
                    <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                       data-original-title="Системный id пользователя">id:{$user_item->id}</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="statusbar">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default4 on {if $user_item->enabled || !$user_item}active{/if}">
                                <input type="radio" name="enabled" id="option1" value=1
                                       {if $user_item->enabled || !$user_item}checked{/if}> Активен
                            </label>
                            <label class="btn btn-default4 off {if $user_item && !$user_item->enabled}active{/if}">
                                <input type="radio" name="enabled" id="option2" value=0
                                       {if $user_item && !$user_item->enabled}checked{/if}> Скрыт
                            </label>
                        </div>
                        <div class="statusbar-text">Статус:</div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>E-mail</label>
                        <input type="email" class="form-control" placeholder="Введите e-mail пользователя" name="email"
                               value="{$user_item->email|escape_string}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">

                    <div class="form-group">
                        <label>Пароль <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                         data-original-title="При сбросе пароля, новый пароль приходит пользователю на почту, указанную при регистрации (а также на сотовый если это указано в настройках)"><i
                                        class="fa fa-info-circle"></i></a></label>

                        <input {if $user_item->id}type="password" {else}type="text"{/if}
                               class="form-control" {if $user_item->password}placeholder="********"{/if} name="password"
                               value="" {if $user_item}disabled{/if}/>
                    </div>
                </div>
                <div class="col-md-6 mt25">
                    <div class="btn-group">
                        {if $user_item->id}
                            <button class="btn btn-default" type="button" id="change-password">Сменить</button>
                            <button class="btn btn-default" type="button" id="reset-password"><i
                                        class="fa fa-refresh"></i>
                                Сбросить
                            </button>
                        {else}
                            <button class="btn btn-default" type="button" id="generate-password"><i
                                        class="fa fa-refresh"></i>
                                Сгенерировать пароль
                            </button>
                        {/if}

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Телефон <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                          data-original-title="Сотовый телефон нужен для восстановления пароля и смс-уведомлений о заказах"><i
                                        class="fa fa-info-circle"></i></a></label>
                        <input type="text" id="phone"
                               class="form-control {if $settings->cart_phone_required}required{/if}"
                               name="phone"
                               value="{if $user_item->phone_code && $user_item->phone}+7 ({$user_item->phone_code}) {$user_item->phone|phone_mask}{/if}"
                               data-type="phone"
                               data-value-default="{if $user_item->phone_code && $user_item->phone}+7 ({$user_item->phone_code}) {$user_item->phone|phone_mask}{/if}"/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Доп. телефон</label>
                        <input type="text" id="phone2" class="form-control" name="phone2" value="" data-type="phone"
                               data-value-default="{if $user_item->phone2_code && $user_item->phone2}+7 ({$user_item->phone2_code}) {$user_item->phone2|phone_mask}{/if}"/>
                    </div>
                </div>
            </div>

            <legend class="margin-top-20">Данные доставки</legend>
            <div class="form-group">
                <label>Адрес доставки</label>
                <textarea class="form-control" rows="2" placeholder="Введите адрес"
                          name="delivery_address">{$user_item->delivery_address}</textarea>
            </div>
            {$allow_edit_order_module = false}
            {if in_array($edit_order_module->module, $global_group_permissions)}{$allow_edit_order_module = true}{/if}


        </div>

        <div class="col-md-4">

            <legend>Входит в группу</legend>
            <ul class="list-group" id="groups_list">
                {foreach $user_groups as $group}
                    <a href="#"
                       class="list-group-item {if $group->id == $user_item->group_id || $group_id == $group->id || (!$user_item && !$group_id && $group@first)}active{/if}"
                       data-id="{$group->id}">{$group->group_name|escape}</a>
                {/foreach}
            </ul>
            <input type="hidden" name="group_id"
                   value="{if $user_item}{$user_item->group_id}{elseif $group_id}{$group_id}{else}{$user_groups.0->id}{/if}"
                   id="group_id"/>

            <legend>Уведомления</legend>
            <div class="form-group">

                <label class="noinline">Пользователь согласен на получение e-mail уведомлений</label>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $user_item->mail_confirm || !$user_item}active{/if}">
                        <input type="radio" name="mail_confirm" value=1
                               {if $user_item->mail_confirm || !$user_item}checked{/if}/> Да
                    </label>
                    <label class="btn btn-default4 off {if $user_item && !$user_item->mail_confirm}active{/if}">
                        <input type="radio" name="mail_confirm" value=0
                               {if $user_item && !$user_item->mail_confirm}checked{/if}/> Нет
                    </label>
                </div>
            </div>

            <div class="form-group">
                <label class="noinline">Пользователь согласен на получение SMS уведомлений</label>

                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $user_item->sms_confirm || !$user_item}active{/if}">
                        <input type="radio" name="sms_confirm" value=1
                               {if $user_item->sms_confirm || !$user_item}checked{/if}/>
                        Да
                    </label>
                    <label class="btn btn-default4 off {if $user_item && !$user_item->sms_confirm}active{/if}">
                        <input type="radio" name="sms_confirm" value=0
                               {if $user_item && !$user_item->sms_confirm}checked{/if}/>
                        Нет
                    </label>
                </div>
            </div>


            <hr/>
            <div class="form-group">
                <label>Дата регистрации пользователя</label>
                <input type="text" class="form-control" value="{$user_item->created|date}" disabled>
            </div>
            <hr/>


        </div>


    </div>

</div>
<div class="tab-pane" id="usersorders">
    <div class="row">
        <div class="col-md-8">
            <div class="com-allorders nobottommargin">
                <div id="new_menu" class="dd orderslist">
                    {if $orders}
                        {include file='pagination.tpl'}
                        <div class="itemslist">
                            <ol class="dd-list">
                                {foreach $orders as $order}
                                    <li class="dd-item dd3-item" data-id="{$order->id}">
                                        <div class="dd3-content">
                                            <input type="checkbox" value="{$order->id}" class="checkb">

                                            <div id="orderblock">
                                                <div class="time">{$order->day_str} {if $order->day_str != 'Сегодня' && $order->day_str != 'Вчера'}{$order->date|date}{else}в{/if} {$order->date|time}</div>
                                                <div class="orderbody">
                                                    <a {if $allow_edit_order_module}href="{$config->root_url}{$edit_order_module->url}{url add=['id'=>$order->id, 'status_id'=>$params_arr['status_id']]}"{/if}
                                                       class="order-numder">Заказ №{$order->id}</a>

                                                    <div class="order-list-summ">{$order->total_price|convert} {$main_currency->sign}</div>
                                                </div>

                                                {if $allow_edit_order_module}
                                                    <div class="controllinks">
                                                        <a class="delete-item"
                                                           href="{$config->root_url}{$edit_order_module->url}{url add=['id'=>$order->id, 'mode'=>'delete', 'ajax'=>1]}/"><i
                                                                    class="fa fa-times"></i></a>
                                                    </div>
                                                {/if}
                                            </div>

                                            {if $order->note}
                                                <div class="notes">
                                                    {$order->note|escape}
                                                </div>
                                            {/if}
                                        </div>
                                    </li>
                                {/foreach}
                            </ol>
                        </div>
                        {include file='pagination.tpl'}
                    {else}
                        Нет заказов
                    {/if}
                </div>

            </div>
        </div>
    </div>

</div>
<div class="tab-pane" id="bankdetails">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Наименование организации</label>
                <input type="text" class="form-control" name="organization_name"
                       value="{$user_item->organization_name|escape_string}"/>
            </div>
            <hr/>

            <p><strong>Юридический адрес:</strong></p>

            <div class="form-group">
                <label>Индекс</label>
                <input type="text" class="form-control" name="yur_postcode" value="{$user_item->yur_postcode|escape_string}"/>
            </div>

            <div class="form-group">
                <label>Город</label>
                <input type="text" class="form-control" name="yur_city" value="{$user_item->yur_city|escape_string}"/>
            </div>

            <div class="form-group">
                <label>Адрес</label>
                <input type="text" class="form-control" name="yur_address" value="{$user_item->yur_address|escape_string}"/>
            </div>

            <div class="form-group">
                <label>ИНН</label>
                <input type="text" class="form-control" name="yur_inn" value="{$user_item->yur_inn|escape_string}"/>
            </div>

            <div class="form-group">
                <label>КПП</label>
                <input type="text" class="form-control" name="yur_kpp" value="{$user_item->yur_kpp|escape_string}"/>
            </div>

            <hr/>

            <div class="form-group">
                <label>Название банка</label>
                <input type="text" class="form-control" name="yur_bank_name"
                       value="{$user_item->yur_bank_name|escape_string}"/>
            </div>

            <div class="form-group">
                <label>Город (банка)</label>
                <input type="text" class="form-control" name="yur_bank_city"
                       value="{$user_item->yur_bank_city|escape_string}"/>
            </div>

            <div class="form-group">
                <label>БИК</label>
                <input type="text" class="form-control" name="yur_bank_bik" value="{$user_item->yur_bank_bik|escape_string}"/>
            </div>

            <div class="form-group">
                <label>Корреспондентский счет</label>
                <input type="text" class="form-control" name="yur_bank_corr_schet"
                       value="{$user_item->yur_bank_corr_schet|escape_string}"/>
            </div>

            <div class="form-group">
                <label>Расчетный счет</label>
                <input type="text" class="form-control" name="yur_bank_rasch_schet"
                       value="{$user_item->yur_bank_rasch_schet|escape_string}"/>
            </div>
        </div>
    </div>

</div>


</div>
<input type="hidden" name="id" value="{$user_item->id}"/>
{if $group_id}<input type="hidden" name="return_group_id" value="{$group_id}"/>{/if}
<input type="hidden" name="session_id" value="{$smarty.session.id}"/>
<input type="hidden" name="close_after_save" value="0"/>
<input type="hidden" name="add_after_save" value="0"/>
</fieldset>


</form>

<script type="text/javascript">
    $(document).ready(function () {

        {if $message_success}
        info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}

        {if $message_error == "error"}error_box($('#notice'), 'Ошибка при сохранении!', '');
        {/if}
        {if $message_error == "user_already_exists"}
        {if $count_users_name > 0}error_box($('#notice'), 'Пользователь с таким именем существует!', '');
        {/if}
        {if $count_users_email > 0}error_box($('#notice'), 'Пользователь с таким E-mail существует!', '');
        {/if}
        {if $count_users_phone > 0}error_box($('#notice'), 'Пользователь с таким телефоном существует!', '');
        {/if}
        {/if}
        {/if}

        $("#phone").mask("+7 (999) 999-99-99");
        $("#phone2").mask("+7 (999) 999-99-99");

        $('textarea[name=delivery_address]').autosize();
    });

    $("form#user a.save").click(function () {
        $(this).closest('form').submit();
        return false;
    });
    $("form#user a.saveclose").click(function () {
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#user a.saveadd").click(function () {
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("#phone_code,#phone2_code,#phone,#phone2").keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });
    $("#phone_code").keyup(function () {
        if ($(this).val().length == 3)
            $("#phone").focus();
    });
    $("#phone2_code").keyup(function () {
        if ($(this).val().length == 3)
            $("#phone2").focus();
    });
    $("#groups_list a").click(function () {
        $(this).closest('ul').find('a').each(function () {
            $(this).removeClass('active');
        });
        $(this).addClass('active');
        $('#group_id').val($(this).attr('data-id'));
        return false;
    });

    function generate_password() {
        var result = '';
        var words = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
        var max_position = words.length - 1;
        for (i = 0; i < 8; ++i) {
            position = Math.floor(Math.random() * max_position);
            result = result + words.substring(position, position + 1);
        }
        return result;
    }

    $('#reset-password').click(function () {
        $.ajax({
            type: 'POST',
            url: '{$config->root_url}/ajax/get_data.php',
            data: {
                'object': 'users',
                'mode': 'reset_password',
                'id': {if $user_item->id}{$user_item->id}{else}0{/if},
                'session_id': '{$smarty.session.id}'
            },
            success: function (data) {
                if (data.success)
                    info_box($('#notice'), 'Письмо с ссылкой на восстановление пароля отправлено', '');
                else
                    error_box($('#notice'), 'Ошибка при отправке письма с восстановлением пароля!', '');
                return false;
            }
        });
    });

    $('#generate-password').click(function () {
        var input = $('input[name=password]');
        input.val(generate_password());
        input.attr('type', 'text');
        return false;
    });

    $('#change-password').click(function () {
        var input = $('input[name=password]');
        input.val('');
        input.attr('type', 'text');
        input.prop('disabled', false);
        input.focus();
        return false;
    });

    $('.delete-item').on('click', function () {
        confirm_popover_box($(this), function (obj) {
            delete_order_item(obj);
        });
        return false;
    });

    function delete_order_item(obj) {
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function (data) {
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('div.checkall').click(function () {
        $("#new_menu input[type=checkbox]").each(function () {
            $(this).attr('checked', true);
        });
    });
</script>