<legend>Настройки хлебных крошек</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Открывающий тег хлебных крошек</label>
                    <input name="breadcrumbs_open_tag" type="text" class="form-control" value="{$settings->breadcrumbs_open_tag|escape_string}" />
                </div>
                </div>
                
                <div class="col-md-6">
                <div class="form-group">
                    <label>Закрывающий тег хлебных крошек</label>
                    <input name="breadcrumbs_close_tag" type="text" class="form-control" value="{$settings->breadcrumbs_close_tag|escape_string}" />
                </div>
            </div>
         </div>
         
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Первый элемент хлебных крошек</label>
                    <input name="breadcrumbs_first_element" type="text" class="form-control" value="{$settings->breadcrumbs_first_element|escape_string}" />
                </div>
                </div>
                
                <div class="col-md-6">
                <div class="form-group">
                    <label>Открывающий тег элемента хлебных крошек</label>
                    <input name="breadcrumbs_element_open_tag" type="text" class="form-control" value="{$settings->breadcrumbs_element_open_tag|escape_string}" />
                </div>
             </div>
         </div>
                
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Закрывающий тег элемента хлебных крошек</label>
                    <input name="breadcrumbs_element_close_tag" type="text" class="form-control" value="{$settings->breadcrumbs_element_close_tag|escape_string}" />
                </div>
                </div>
                
                <div class="col-md-6">
                <div class="form-group">
                    <label>Открывающий тег выбранного элемента хлебных крошек</label>
                    <input name="breadcrumbs_selected_element_open_tag" type="text" class="form-control" value="{$settings->breadcrumbs_selected_element_open_tag|escape_string}" />
                </div>
             </div>
         </div>
         
         <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Закрывающий тег выбранного элемента хлебных крошек</label>
                    <input name="breadcrumbs_selected_element_close_tag" type="text" class="form-control" value="{$settings->breadcrumbs_selected_element_close_tag|escape_string}" />
                </div>
                </div>
                
                <div class="col-md-6">
                <div class="form-group">
                    <label>Разделитель элементов хлебных крошек</label>
                    <input name="breadcrumbs_element_separator" type="text" class="form-control" value="{$settings->breadcrumbs_element_separator|escape_string}" />
                </div>
                </div>
         </div>

<input type="hidden" name="mode" value="breadcrumbs"/>