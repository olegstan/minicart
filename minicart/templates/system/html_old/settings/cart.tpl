<legend>Корзина</legend>

         <div class="form-group">
             <label><strong>Какие поля обязательны при оформлении заказа:</strong></label>   
         </div>
             
          <div class="row form-group">
            <div class="col-md-3 selectheight">
              ФИО получателя:
            </div>
            <div class="col-md-9">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->cart_fio_required}active{/if}">
                    <input type="radio" name="cart_fio_required" value="1" {if $settings->cart_fio_required}checked{/if}/>  Обязательно для заполнения
                  </label>
                  <label class="btn btn-default4 off {if !$settings->cart_fio_required}active{/if}">
                    <input type="radio" name="cart_fio_required" value="0" {if !$settings->cart_fio_required}checked{/if}/> Не обязательно
                  </label>
                </div>
            </div>
         </div>
              
             
         <div class="row form-group">
            <div class="col-md-3 selectheight">
              Мобильный телефон:
            </div>
            <div class="col-md-9">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->cart_phone_required}active{/if}">
                    <input type="radio" name="cart_phone_required" value="1" {if $settings->cart_fio_required}checked{/if}/>  Обязательно для заполнения
                  </label>
                  <label class="btn btn-default4 off {if !$settings->cart_phone_required}active{/if}">
                    <input type="radio" name="cart_phone_required" value="0" {if !$settings->cart_fio_required}checked{/if}/> Не обязательно
                  </label>
                </div>
            </div>
         </div>
             
             
         <div class="row form-group">
            <div class="col-md-3 selectheight">
              E-mail:
            </div>
            <div class="col-md-9">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->cart_email_required}active{/if}">
                    <input type="radio" name="cart_email_required" value="1" {if $settings->cart_email_required}checked{/if}/>  Обязательно для заполнения
                  </label>
                  <label class="btn btn-default4 off {if !$settings->cart_email_required}active{/if}">
                    <input type="radio" name="cart_email_required" value="0" {if !$settings->cart_email_required}checked{/if}/> Не обязательно
                  </label>
                </div>
            </div>
         </div>
             
             
         <div class="row form-group">
            <div class="col-md-3 selectheight">
              Адрес получателя:
            </div>
            <div class="col-md-9">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->cart_address_required}active{/if}">
                    <input type="radio" name="cart_address_required" value="1" {if $settings->cart_address_required}checked{/if}/>  Обязательно для заполнения
                  </label>
                  <label class="btn btn-default4 off {if !$settings->cart_address_required}active{/if}">
                    <input type="radio" name="cart_address_required" value="0" {if !$settings->cart_address_required}checked{/if}/> Не обязательно
                  </label>
                </div>
            </div>
         </div>

        
            <legend class="mt15">Настройка заказов</legend>
            <div class="row">
            <div class="col-md-6">
            <div class="form-group">
            <label>Минимальная сумма заказа</label>
            <div class="input-group">
                <input type="text" class="form-control minsumm" placeholder="" name="cart_order_min_price" value="{if $settings->cart_order_min_price}{$settings->cart_order_min_price}{else}0{/if}"/>
                <span class="input-group-addon"><i class="fa fa-rub"></i></span>
            </div>
            <p class="help-block">Пользователю будет выведено собщение в корзине о минимальной сумме заказа.<br/> По умолчанию "0".</p>
            </div>
            </div>
            <div class="col-md-6">
                    <div class="form-group">
        <label>Максимум товаров в заказе</label>
        <input name="max_order_amount" type="text" class="form-control" value="{$settings->max_order_amount|escape_string}" />
        <p class="help-block">Это ограничение не позволяет заказать более указанного количества товаров в карточке товара или при выборе количества в корзине</p>
        </div>
            </div>
            </div>
            
<legend>Заказ в 1 клик</legend>
 <div class="form-group">
            <label>Показываем поле Комментарий</label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->cart_one_click_show_comment}active{/if}">
                    <input type="radio" name="cart_one_click_show_comment" value=1 {if $settings->cart_one_click_show_comment}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->cart_one_click_show_comment}active{/if}">
                    <input type="radio" name="cart_one_click_show_comment" value=0 {if !$settings->cart_one_click_show_comment}active{/if}> Нет
                </label>
            </div>
        </div>

<legend class="mt15">Шаблоны писем</legend>
<div class="form-group">
                  <label>Показывать id товара после названия</label>
                    <div class="btn-group noinline" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $settings->cart_show_product_id_in_email}active{/if}">
                            <input type="radio" name="cart_show_product_id_in_email" value=1 {if $settings->cart_show_product_id_in_email}checked{/if}> Да
                        </label>
                        <label class="btn btn-default4 off {if !$settings->cart_show_product_id_in_email}active{/if}">
                            <input type="radio" name="cart_show_product_id_in_email" value=0 {if !$settings->cart_show_product_id_in_email}active{/if}> Нет
                        </label>
                    </div>
              </div>

<input type="hidden" name="mode" value="cart"/>