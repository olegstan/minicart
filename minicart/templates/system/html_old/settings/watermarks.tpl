<legend>Водяные знаки</legend>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Водяные знаки включены</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->watermark_enabled}active{/if}">
                    <input type="radio" name="watermark_enabled" value=1 {if $settings->watermark_enabled}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->watermark_enabled}active{/if}">
                    <input type="radio" name="watermark_enabled" value=0 {if !$settings->watermark_enabled}checked{/if}> Нет
                </label>
            </div>
        </div>
        
        <div class="form-group">
            <label>Водяной знак</label>
            <div class="controls">
            <span class="btn btn-file btn-default">
                <i class="fa fa-plus"></i> <span>Загрузить водяной знак</span>
                <input type="file" name="watermark_file" id="watermark-file">
            </span>

            {if $watermark_exists}
                <div class="watermark-bg" style="background:url({$config->root_url}/{$config->watermark_file}) no-repeat 50% 50% #000;">
                </div>
                 <button type="button" class="btn btn-default" id="delete-watermark-file"><i class="fa fa-times"></i> Удалить водяной знак</button>
            {/if}
            </div>
        </div>
        
        
        <legend>Положение знака</legend>
        <div class="form-group">
            <label>Положение по горизонтали <i class="fa fa-long-arrow-right"></i></label>
            <div class="input-group">
                <input type="text" name="watermark_offset_x" class="form-control" value="{$settings->watermark_offset_x}">
                <span class="input-group-addon">%</span>
            </div>
        </div>

        <div class="form-group">
            <label>Положение по вертикали <i class="fa fa-long-arrow-down"></i></label>
            <div class="input-group">
                <input type="text" name="watermark_offset_y" class="form-control" value="{$settings->watermark_offset_y}">
                <span class="input-group-addon">%</span>
            </div>
        </div>
        {*
        <div class="form-group">
            <label>Прозрачность <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="100% - знак прозрачен на 100%, то есть не виден. 0% прозрачность не используется."><i class="fa fa-info-circle"></i></a></label>
            <div class="input-group">
                <input type="text" name="watermark_transparency" class="form-control" value="{$settings->watermark_transparency}">
                <span class="input-group-addon">%</span>
            </div>
        </div>
        *}
        
        <legend>Размер изображений</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Ширина</label>
                    <div class="input-group">
                        <input type="text" name="watermark_image_min_width" class="form-control" value="{$settings->watermark_image_min_width}">
                        <span class="input-group-addon">px</span>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Высота</label>
                    <div class="input-group">
                        <input type="text" name="watermark_image_min_height" class="form-control" value="{$settings->watermark_image_min_height}">
                        <span class="input-group-addon">px</span>
                    </div>
                </div>
            </div>
        </div>
        
        <p class="help-block">Указано от какой ширины и высоты изображения накладывать водяные знаки, по умолчанию 80 на 80 пикселей. То есть если изображение более 80 пикселей по ширине или высоте водяной знак будет наложен.</p>
      
        <hr/>
        <div class="form-group">
            <label>Использовать для изображений пользователей в отзывах <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Водяные знаки будут накладываться на все изображения, загруженные пользователями в отзывах о товарах"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->watermark_reviews_enabled}active{/if}">
                    <input type="radio" name="watermark_reviews_enabled" value=1 {if $settings->watermark_reviews_enabled}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->watermark_reviews_enabled}active{/if}">
                    <input type="radio" name="watermark_reviews_enabled" value=0 {if !$settings->watermark_reviews_enabled}checked{/if}> Нет
                </label>
            </div>
        </div>
        
        
        <div class="form-group">
            <label>Использовать для галереи категории товраов</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {*active*}">
                    <input type="radio" name="watermark_reviews_enabled" value=1 {*checked*}> Да
                </label>
                <label class="btn btn-default4 off {*active*}">
                    <input type="radio" name="watermark_reviews_enabled" value=0 {*checked*}> Нет
                </label>
            </div>
        </div>
        
        
        {*
        <div class="form-group ">
            <label>Использовать для материалов <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Водяные знаки будут накладываться на изображения показанные в материалах"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->watermark_materials_enabled}active{/if}">
                    <input type="radio" name="watermark_materials_enabled" value=1 {if $settings->watermark_materials_enabled}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->watermark_materials_enabled}active{/if}">
                    <input type="radio" name="watermark_materials_enabled" value=0 {if !$settings->watermark_materials_enabled}checked{/if}> Нет
                </label>
            </div>
        </div>*}
          
  
        
        
    </div>
    <div class="col-md-6">
    <div class="form-group">
    <label>Пример большой картинки на странице товара</label>
    <div class="watermark-image-big">
        <img src="{$watermark_image->filename|resize:'image':330:500:w}" />
    </div>
    </div>
    
    <div class="form-group">
    <label>Пример маленькой картинки в списке товаров</label>
    <div class="watermark-image-small">
        <img src="{$watermark_image->filename|resize:'image':140:150:w}" />
    </div>
    </div>
    
    </div>
</div>

<input type="hidden" name="mode" value="watermarks"/>

<script type="text/javascript">
    $('#delete-watermark-file').click(function(){
        var href = "{$config->root_url}{$module->url}";
        $.ajax({
            type: 'POST',
            url: href,
            data:{
                mode: 'delete-watermark-file'
            },
            success: function(data){
                if (data){
                    $('div.watermark-bg').remove();
                    $('#delete-watermark-file').remove();
                }
            }
        });
        return false;
    });

    $('#watermark-file').change(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>