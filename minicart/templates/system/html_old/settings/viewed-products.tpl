<legend>Вы смотрели</legend>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Количество товаров в истории просмотров</label>
            <input type="text" name="count_history_products" class="form-control" value="{$settings->count_history_products|escape_string}">
        </div>
    </div>
</div>

<input type="hidden" name="mode" value="viewed-products"/>