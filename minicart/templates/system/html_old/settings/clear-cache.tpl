<legend>Очистка кеша</legend>

<div class="form-group">
    <button id="clear-image-cache" class="btn btn-primary">Очистить кеш изображений</button>
    <input type="hidden" name="clear-image-cache" value="0"/>
</div>

<div class="form-group">
    <button id="recreate-auto-properties" class="btn btn-primary"><i class="fa fa-refresh"></i> Пересоздать автосвойства</button>
    <input type="hidden" name="recreate-auto-properties" value="0"/>
</div>

<div class="form-group">
    <button id="clear-search-history" class="btn btn-primary"> Очистить историю поисковых запросов</button>
    <input type="hidden" name="clear-search-history" value="0"/>
</div>

<input type="hidden" name="mode" value="clear-cache"/>
