<legend>Лицензия</legend>       


        <div class="form-group">
            <label>Введите код лицензии</label>
            <div class="input-group">            
                <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Номер лицензии">id:1</a></span>
            
                <input type="text" class="form-control" name="licence_code" placeholder="" value="{$settings->licence_code}"/>           
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="check-button">Проверить</button>
                </span>                
            </div>
        </div>
        
        <div class="alert alert-success hidden" role="alert" id="licence-result">
            {*Лицензия №1 для домена <strong>minicart.su</strong> действительна!*}
        </div>
        
        {*<div class="alert alert-info" role="alert">
        При покупке лицензии вы получаете бесплатные обновления до 15.01.2016.<br/>
        Чтобы получать обновления после 15.01.2016 продлите подписку на обновления на сайте <a href="http://minicart.ru/" target="_blank">minicart.ru</a>
        </div>*}

<input type="hidden" name="mode" value="licence"/>


<script type="text/javascript">
    $('#check-button').click(function(){
        $(this).addClass('disabled');
        var url = "http://minicart.ru/check_licence.php?domain={$smarty.server.SERVER_NAME}&operation=check_licence&value=" + encodeURI($('input[name=licence_code]').val());

        $.ajax({
            type: 'POST',
            url: 'http://minicart.ru/check_licence.php',
            data: {
                'domain': '{$smarty.server.SERVER_NAME}',
                'operation': 'check_licence',
                'value': encodeURI($('input[name=licence_code]').val())
            },
            success: function (data) {
                $('#licence-result').removeClass('alert-success').removeClass('alert-warning');

                if (data == undefined || data.length == 0)
                {
                    $('#licence-result').addClass('alert-warning').removeClass('hidden').html('Запрос не выполнен, повторите попытку позже!');
                    return false;
                }

                if (data.result_code < 1)
                    $('#licence-result').addClass('alert-warning');
                if (data.result_code > 1)
                    $('#licence-result').addClass('alert-success');

                switch(data.result_code)
                {
                    case -1:
                        $('#licence-result').html('Неверный лицензионный код');
                        break;
                    case -2:
                        $('#licence-result').html('Лицензия для домена <b>{$smarty.server.SERVER_NAME}</b> недействительна!');
                        break;
                    case 1:
                        $('#licence-result').html('Лицензия №' + data.licence_id + ' для домена <b>{$smarty.server.SERVER_NAME}</b> действительна!');
                        break;
                    case 2:
                        $('#licence-result').html('Лицензионный ключ устарел, обновите его');
                        break;
                    case 3:
                        $('#licence-result').html('Лицензия №' + data.licence_id + ' для домена <b>{$smarty.server.SERVER_NAME}</b> действительна! У Вас установлена версия ' + data.version + ', актуальная версия ' + data.actual_version);
                        break;
                }

                $('#licence-result').removeClass('hidden');
                return false;
            },
            dataType: 'json'
        });
        return false;
    });
</script>