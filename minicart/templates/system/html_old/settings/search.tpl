<legend>Настройки поиска</legend>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    {*
                    <label>По каким полям идет поиск:</label>
                    <label class="checkbox">
                        <input type="checkbox" name="search_var_category_name" id="nazkat" {if $settings->search_var_category_name}checked{/if}>
                        <label for="nazkat" class="{if $settings->search_var_category_name}checked{else}notchecked{/if}">Искать по названиям категорий</label>
                    </label>
                    *}
                    
                    <label>Искать по названиям категорий <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если отмечено 'Да' - будет работать поиск по категориям. Найденые категории, соответствующие поисковому запросу будут показаны в выпадающей строке и на странице результатов поиска сразу после товаров"><i class="fa fa-info-circle"></i></a></label>
                    
                    <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $settings->search_var_category_name}active{/if}">
                        <input type="radio" name="search_var_category_name" value=1 {if $settings->search_var_category_name}checked{/if}> Да
                    </label>
                    <label class="btn btn-default4 off {if !$settings->search_var_category_name}active{/if}">
                        <input type="radio" name="search_var_category_name" value=0 {if !$settings->search_var_category_name}checked{/if}> Нет
                    </label>
                    </div>
                      </div>
                    
                   <div class="form-group"> 
                    
                    <label>Выводить артикул товара в поисковых подсказках</label>
                    
                    <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $settings->search_ajax_show_sku}active{/if}">
                        <input type="radio" name="search_ajax_show_sku" value=1 {if $settings->search_ajax_show_sku}checked{/if}> Да
                    </label>
                    <label class="btn btn-default4 off {if !$settings->search_ajax_show_sku}active{/if}">
                        <input type="radio" name="search_ajax_show_sku" value=0 {if !$settings->search_ajax_show_sku}checked{/if}> Нет
                    </label>
                    </div>
                   </div> 
                    
                    <div class="form-group">
                    <label>Выводить id товара в поисковых подсказках</label>
                    <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $settings->search_ajax_show_product_id}active{/if}">
                        <input type="radio" name="search_ajax_show_product_id" value=1 {if $settings->search_ajax_show_product_id}checked{/if}> Да
                    </label>
                    <label class="btn btn-default4 off {if !$settings->search_ajax_show_product_id}active{/if}">
                        <input type="radio" name="search_ajax_show_product_id" value=0 {if !$settings->search_ajax_show_product_id}checked{/if}> Нет
                    </label>
                    </div>
                    </div>
                    

            </div>
        </div>
        <legend>Подсказки для поиска </legend>
        <div class="row">
            <div class="col-md-6">            
            <div class="form-group">
                <label>Текст в поле поиска <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="По умолчанию это 'Поиск по товарам...', также часто встречаются фразы 'Поиск по 10000+ товарам' или 'Поиск среди 55664 товаров'"><i class="fa fa-info-circle"></i></a></label>
                <input type="text" name="search_placeholder" class="form-control" value="{$settings->search_placeholder|escape_string}">
            </div>
            
            <div class="form-group">
                <label>Текст подсказки 1</label>
                <input type="text" name="search_help_text1" class="form-control" value="{$settings->search_help_text1|escape_string}">
            </div>
            <div class="form-group">
                <label>Текст подсказки 2</label>
                <input type="text" name="search_help_text2" class="form-control" value="{$settings->search_help_text2|escape_string}">
            </div>
        </div>
        
        </div>
        
        
                <div class="form-group">
            <label>С какого символа начинать поиск <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="1 - для сайтов с количеством товаров не более 300, 2 - для сайтов с количеством товаров не более 1000, 3 - для сайтов с количеством товаров не более 30000, 4 - для медленных хостингов"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default {if $settings->search_min_lenght == 1}active{/if}">
                <input type="radio" name="search_min_lenght" value="1" {if $settings->search_min_lenght == 1}checked{/if}>1
              </label>
              <label class="btn btn-default {if $settings->search_min_lenght == 2}active{/if}">
                <input type="radio" name="search_min_lenght" value="2" {if $settings->search_min_lenght == 2}checked{/if}>2
              </label>
              <label class="btn btn-default {if $settings->search_min_lenght == 3}active{/if}">
                <input type="radio" name="search_min_lenght" value="3" {if $settings->search_min_lenght == 3}checked{/if}>3
              </label>
              <label class="btn btn-default {if $settings->search_min_lenght == 4}active{/if}">
                <input type="radio" name="search_min_lenght" value="4" {if $settings->search_min_lenght == 4}checked{/if}>4
              </label>
            </div>

        </div>
        

<input type="hidden" name="mode" value="search"/>