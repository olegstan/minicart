<legend>Отзывы о товарах</legend>

<div class="form-group">
            <label>Отзывы о товарах включены</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->reviews_enabled}active{/if}">
                    <input type="radio" name="reviews_enabled" value=1 {if $settings->reviews_enabled}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->reviews_enabled}active{/if}">
                    <input type="radio" name="reviews_enabled" value=0 {if !$settings->reviews_enabled}checked{/if}> Нет
                </label>
            </div>
        </div>

<hr/>

<div class="form-group">
            <label>Включена ли премодерация отзывов </label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->reviews_premoderate}active{/if}">
                    <input type="radio" name="reviews_premoderate" value=1 {if $settings->reviews_premoderate}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->reviews_premoderate}active{/if}">
                    <input type="radio" name="reviews_premoderate" value=0 {if !$settings->reviews_premoderate}checked{/if}> Нет
                </label>
            </div>
        </div>

<hr/>

<div class="form-group">
    <label>Какие поля дополнительные поля использовать:</label>   
</div>
                  
         <div class="row form-group">
            <div class="col-md-4 selectheight">
              Отзыв в двух словах
            </div>
            <div class="col-md-8">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->reviews_short_visible}active{/if}">
                    <input type="radio" name="reviews_short_visible" value="1" {if $settings->reviews_short_visible}checked{/if}/> Показать
                  </label>
                  <label class="btn btn-default4 off {if !$settings->reviews_short_visible}active{/if}">
                    <input type="radio" name="reviews_short_visible" value="0" {if !$settings->reviews_short_visible}checked{/if}/> Скрыть
                  </label>
                </div>
            </div>
         </div>
             
          <div class="row form-group">
            <div class="col-md-4 selectheight">
              Достоинства
            </div>
            <div class="col-md-8">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->reviews_pluses_visible}active{/if}">
                    <input type="radio" name="reviews_pluses_visible" value="1" {if $settings->reviews_pluses_visible}checked{/if}/> Показать
                  </label>
                  <label class="btn btn-default4 off {if !$settings->reviews_pluses_visible}active{/if}">
                    <input type="radio" name="reviews_pluses_visible" value="0" {if !$settings->reviews_pluses_visible}checked{/if}/> Скрыть
                  </label>
                </div>
            </div>
         </div>
         
         <div class="row form-group">
            <div class="col-md-4 selectheight">
              Недостатки
            </div>
            <div class="col-md-8">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->reviews_minuses_visible}active{/if}">
                    <input type="radio" name="reviews_minuses_visible" value="1" {if $settings->reviews_minuses_visible}checked{/if}/>  Показать
                  </label>
                  <label class="btn btn-default4 off {if !$settings->reviews_minuses_visible}active{/if}">
                    <input type="radio" name="reviews_minuses_visible" value="0" {if !$settings->reviews_minuses_visible}checked{/if}/> Скрыть
                  </label>
                </div>
                
            </div>
         </div>
         
         
         <div class="row form-group">
            <div class="col-md-4 selectheight">
              Галочка "Да - я рекомендую..."
            </div>
            <div class="col-md-8">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->reviews_recommended_visible}active{/if}">
                    <input type="radio" name="reviews_recommended_visible" value="1" {if $settings->reviews_recommended_visible}checked{/if}/>  Показать
                  </label>
                  <label class="btn btn-default4 off {if !$settings->reviews_recommended_visible}active{/if}">
                    <input type="radio" name="reviews_recommended_visible" value="0" {if !$settings->reviews_recommended_visible}checked{/if}/> Скрыть
                  </label>
                </div>
                
            </div>
         </div>
         
         <div class="row form-group">
            <div class="col-md-4 selectheight">
              Форма для загрузки изображений
            </div>
            <div class="col-md-8">
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $settings->reviews_images_visible}active{/if}">
                    <input type="radio" name="reviews_images_visible" value="1" {if $settings->reviews_images_visible}checked{/if}/>  Показать
                  </label>
                  <label class="btn btn-default4 off {if !$settings->reviews_images_visible}active{/if}">
                    <input type="radio" name="reviews_images_visible" value="0" {if !$settings->reviews_images_visible}checked{/if}/> Скрыть
                  </label>
                </div>
                
            </div>
         </div>
<hr/>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Количество отзывов на одной странице</label>
            <input name="reviews_num" type="text" class="form-control" value="{$settings->reviews_num}" />
            <p class="help-block">По умолчанию 10</p>
        </div>
    </div>
</div>

           
<hr/>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Количество жалоб для скрытия отзыва <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если количество жалоб будет больше указаного ниже - отзыв будет скрыт"><i class="fa fa-info-circle"></i></a></label>
            <input name="reviews_claim_count_for_disable" type="text" class="form-control" value="{$settings->reviews_claim_count_for_disable}" />
            <p class="help-block">По умолчанию 5, поставьте 0 если отзывы не нужно скрывать</p>
        </div>
    </div>
</div>

           
<hr/>


<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Количество изображений для отзыва <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Указывается максимальное количество изображений которое может загрузить пользователь"><i class="fa fa-info-circle"></i></a></label>
            <input name="reviews_images_num" type="text" class="form-control" value="{$settings->reviews_images_num}" />
        </div>
    </div>
</div>


            <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Ширина <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Оригинал изображения будет уменьшен до этих размеров"><i class="fa fa-info-circle"></i></a></label>
                    <div class="input-group">
                        <input type="text" name="reviews_images_max_width" class="form-control" value="{$settings->reviews_images_max_width}">
                        <span class="input-group-addon">px</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Высота <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Оригинал изображения будет уменьшен до этих размеров"><i class="fa fa-info-circle"></i></a></label>
                    <div class="input-group">
                        <input type="text" name="reviews_images_max_height" class="form-control" value="{$settings->reviews_images_max_height}">
                        <span class="input-group-addon">px</span>
                    </div>
                </div>
            </div>
            </div>
            <p class="help-block"></p>

<hr/>

<div class="form-group">
            <label>Сортировка отзывов по умолчанию</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->reviews_default_sort == "actual"}active{/if}">
                    <input type="radio" name="reviews_default_sort" value="actual" {if $settings->reviews_default_sort == "actual"}checked{/if}> Самые актуальные
                </label>
                
                <label class="btn btn-default4 on {if $settings->reviews_default_sort == "newest"}active{/if}">
                    <input type="radio" name="reviews_default_sort" value="newest" {if $settings->reviews_default_sort == "newest"}checked{/if}> Самые новые
                </label>
                
                <label class="btn btn-default4 on {if $settings->reviews_default_sort == "useful"}active{/if}">
                    <input type="radio" name="reviews_default_sort" value="useful" {if $settings->reviews_default_sort == "useful"}checked{/if}> Наиболее полезные
                </label>


            </div>
</div>

<hr/>

<legend>Настройка сортировки по актуальности <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Для каждого из 4-ех параметров будет определен вес, на основании суммы параметров умноженных на вес будет сформирован ранг отзыва."><i class="fa fa-info-circle"></i></a></legend>   
    
<div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Новизна <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Новые отзывы получают больше очков"><i class="fa fa-info-circle"></i></a></label>
                    <input name="koef_newest" type="text" class="form-control" value="{$settings->koef_newest}" />
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>Полезность <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Учитывается полезность отзыва - чем отзыв полезней тем больше очков"><i class="fa fa-info-circle"></i></a></label>
                    <input name="koef_popular" type="text" class="form-control" value="{$settings->koef_popular}" />
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>Наполненость <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Учитывается наполненость отзыва, содержательные отзывы с изображениями получают больше очков"><i class="fa fa-info-circle"></i></a></label>
                    <input name="koef_fill" type="text" class="form-control" value="{$settings->koef_fill}" />
                </div>
            </div>
            
            <div class="col-md-3">
                <div class="form-group">
                    <label>Жалобы <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отзывы на которые поуступают жалобы будут понижены в рейтинге"><i class="fa fa-info-circle"></i></a></label>
                    <input name="koef_claim" type="text" class="form-control" value="{$settings->koef_claim}" />
                </div>
            </div>

</div>

<input type="hidden" name="mode" value="reviews-products"/>

