<legend>Выгрузка в Яндекс-Маркет</legend>

<p>Выгрузка доступна по адресу: <a href="{$config->root_url}/yandex.xml" class="tdu">{$config->root_url}/yandex.xml</a></p>
<p>Сейчас в файле выгрузки <strong>{$categories_count}</strong> категорий, <strong>{$products_count}</strong> товаров и <strong>{$variants_count}</strong> вариантов товаров (offer)</p>

<hr>
<div class="form-group">
    <label>Выгружать в маркет товары с количеством 0 и статусом "под заказ"</label>

    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default4 on {if $settings->yandex_export_all_product}active{/if}">
            <input type="radio" name="yandex_export_all_product" value=1 {if $settings->yandex_export_all_product}checked{/if}> Выгружать
        </label>
        <label class="btn btn-default4 off {if !$settings->yandex_export_all_product}active{/if}">
            <input type="radio" name="yandex_export_all_product" value=0 {if !$settings->yandex_export_all_product}checked{/if}> Не выгружать
        </label>
    </div>
</div>
<div class="form-group">
    <label>Добавлять ко всем товарам тег <code>&lt;adult&gt;</code> (<a href="http://help.yandex.ru/partnermarket/adult.xml" target="_blank">Справка Яндекс-маркета</a>) <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Элемент <adult> обязателен для обозначения товара, имеющего отношение к удовлетворению сексуальных потребностей, либо иным образом эксплуатирующего интерес к сексу - требования Яндекс-Маркета"><i class="fa fa-info-circle"></i></a></label>

    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default4 on {if $settings->yandex_export_add_tag_adult}active{/if}">
            <input type="radio" name="yandex_export_add_tag_adult" value="1" {if $settings->yandex_export_add_tag_adult}checked{/if}> Да
        </label>
        <label class="btn btn-default4 off {if !$settings->yandex_export_add_tag_adult}active{/if}">
            <input type="radio" name="yandex_export_add_tag_adult" value="0" {if !$settings->yandex_export_add_tag_adult}checked{/if}> Нет
        </label>
    </div>
</div>
<hr>
<p><strong>Товар попадает в выгрузку если:</strong></p>
<ol>
  <li>Товар активен</li>
  <li>Товар имеет вариант с ценой &gt; 0</li>
  <li>Товар находится в активной категории</li>
  <li>Товар в наличии или имеет статус &quot;под заказ&quot; (в этом случае в маркет передаются параметры что данный товар имеет статус &quot;под заказ&quot;)</li>
</ol>

<p><strong>Дополнительно:</strong></p>
<ol>
  <li>Каждый вариант товара в выгрузке будет представлен как отдельный товар.</li>
  <li>Цена в выгрузке всегода указывается в рублях, если цена товара в админке указана в другой валюте, для выгрузки она будет автоматически конвертирована в рубли по курсу указанному в настройках.</li>
  <li>Если в настройках корзины указана &quot;Минимальная сумма заказа&quot;, например 1000 {$main_currency->sign}, в этом случае в файл выгрузки для всех товаров будет передан параметр <code>&lt;sales_notes&gt;минимальная сумма заказа XXX руб.&lt;/sales_notes&gt;</code></li>
  <li>Если в выгрузку будут попадать товары со статусом &quot;под заказ&quot;, то в выгрузку будет передан параметр <code>&lt;sales_notes&gt;Под заказ&lt;/sales_notes&gt;</code></li>
</ol>

<input type="hidden" name="mode" value="yandex-market"/>