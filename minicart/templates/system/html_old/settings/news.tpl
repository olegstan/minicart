<legend>Новости</legend>        

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Категория материалов</label>
            {*<select class="form-control">
              <option>Новости</option>
            </select>*}

        <select name="news_category_id" class="form-control">
            <option value='0'>Корневая категория</option>
            {function name=categories_tree level=0}
                {foreach $items as $item}
                    <option value='{$item->id}' {if $settings->news_category_id == $item->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                    {categories_tree items=$item->subcategories level=$level+1}
                {/foreach}
            {/function}
            {categories_tree items=$categories}
        </select>
        </div>
        <div class="form-group">
            <label>Количество новостей</label>
            <input type="text" name="news_show_count" class="form-control" value="{$settings->news_show_count}">
        </div>
    </div>
</div>



<input type="hidden" name="mode" value="news"/>