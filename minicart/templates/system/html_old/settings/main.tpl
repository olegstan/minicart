<legend>Общие настройки</legend>        
        <div class="form-group">
            <label>Название магазина</label>
            <input name="site_name" type="text" class="form-control" value="{$settings->site_name|escape_string}" />
        </div>
        <div class="form-group">
        <label>Название компании</label>
        <input name="company_name" type="text" class="form-control" value="{$settings->company_name|escape_string}" />
          </div>
        <div class="form-group">
        <label>Формат даты  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="d.m.Y - 01.01.2001 или j.n.y - 1.1.01 или Y-m-d - 2001-01-01"><i class="fa fa-info-circle"></i></a></label><input name="date_format" type="text" class="form-control" value="{$settings->date_format|escape_string}" />
        </div>

        <legend>Оповещения</legend>
        <div class="form-group">
            <label>E-mail для оповещений о заказах</label>
            <div class="row">
                <div class="col-md-6">
                    <input name="order_email" type="text" class="form-control" value="{$settings->order_email|escape_string}" />
                    <p class="help-block">Можно указать несколько e-mail через запятую</p>
                </div>
            </div>
        </div>
        
        <div class="form-group">
        <label>С какой почты будут приходить письма отправляемые магазином <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Эта почта будет указана как отправитель писем которые приходят администратору и пользователям о заказах и других уведомлениях"><i class="fa fa-info-circle"></i></a></label>
        <div class="row">
            <div class="col-md-6">
                <input name="template_email" type="text" class="form-control" value="{$settings->template_email|escape_string}" />
            </div>        
        </div>
    </div>
        
        
        <div class="form-group">
        <label>Почта для оповещений о уведомлениях <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="На этот ящик будут приходить оповещения о комментариях к товарам, отзывы и другие уведомления"><i class="fa fa-info-circle"></i></a></label>
        <div class="row">
        <div class="col-md-6">
        <input name="comment_email" type="text" class="form-control" value="{$settings->comment_email|escape_string}" />
        </div>
        
        </div>
    </div>
    
    
    
    <!-- Параметры (The End)-->

    <!-- Параметры -->

    <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <label>Время жизни сессии</label>
        <select class="form-control" name="session_lifetime">
            <option value=900{if $settings->session_lifetime==900} selected{/if}>15 минут</option>
            <option value=1800{if $settings->session_lifetime==1800} selected{/if}>30 минут</option>
            <option value=3600{if $settings->session_lifetime==3600} selected{/if}>1 час</option>
            <option value=10800{if $settings->session_lifetime==10800} selected{/if}>3 часа</option>
            <option value=43200{if $settings->session_lifetime==43200} selected{/if}>12 часов</option>
        </select>
        </div>

    </div>
    </div>
    
    <legend>Начальные настройки</legend>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>С какого номера начать следующий заказ</label>
                <input name="" type="text" class="form-control" value="1234" />
            </div>
            
            <div class="form-group">
                <label>С какого id начать нумерацию для товаров (работает только если в каталоге нет товаров)</label>
                <input name="" type="text" class="form-control" value="1024" />
            </div>
         </div>
     </div>
    
    
    <legend>Счетчики и коды онлайн-консультатов</legend>

    <div class="form-group">
        <label>Коды счетчиков</label>
        <textarea class="form-control" rows="10" name="counters_codes">{$settings->counters_codes}</textarea>
    </div>

    <div class="form-group">
        <label>Номер счетчика Яндекс.Метрики</label>
        <input name="yandex_metric_counter" type="text" class="form-control" value="{$settings->yandex_metric_counter}" />
        <p class="help-block">Укажите номер счетчика, взять его можно из кода счетчика yaCounter<strong>XXXXXX</strong>.hit, например 123456<br/><a data-toggle="collapse" data-parent="#accordion" href="#metrika" class="dottedlink">Зачем это нужно?</a></p>

        <div id="metrika" class="panel-collapse collapse">
        Этот параметр позволяет собирать данные о заказах в Яндекс-Метрике, в разделе "Содержание" - "Параметры интернет магазинов", для того чтобы этот пункт заработал необходимо в настройках счетчиках создать цель, название "Заказ", URL содержит "order", далее поставить галочку "Типы целей для интернет-магазинов", и точку "эта цель описывает подтверждение заказа на моем сайте"</div>
    </div>

    <legend>Свойства</legend>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
            <label>Количество свойств на странице</label>
            <input name="properties_num_admin" type="text" class="form-control" value="{$settings->properties_num_admin|escape_string}" />
            </div>
        </div>
    </div>

<input type="hidden" name="mode" value="main"/>
