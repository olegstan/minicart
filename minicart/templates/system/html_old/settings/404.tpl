<legend>Настройка 404 страницы </legend>
<div class="form-group">
    <label>Заголовок страницы</label>
    <input type="text" name="page404_header" class="form-control" value="{$settings->page404_header}">
</div>

<div class="form-group">
    <label>Meta-title</label>
    <input type="text" name="page404_meta_title" class="form-control" value="{$settings->page404_meta_title}">
</div>

<div class="form-group">
    <label>Описание страницы</label>
    <textarea name="page404_text" class="form-control ckeditor" rows="5">{$settings->page404_text}</textarea>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Ключевые слова</label>
            <textarea name="page404_meta_keywords" class="form-control" rows="2">{$settings->page404_meta_keywords}</textarea>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label>Описание</label>
            <textarea name="page404_meta_description" class="form-control" rows="2">{$settings->page404_meta_description}</textarea>
        </div>
    </div>
</div>

<input type="hidden" name="mode" value="404"/>

<script type="text/javascript">
    $(document).ready(function(){
        CKEDITOR.replace('page404_text');
    });
</script>