<legend>Формат цены</legend>
<div class="row">
    <div class="col-md-6">
    <div class="form-group">
    <label>Разделитель копеек</label>
    <select class="form-control" name="decimals_point">
    <option value='.' {if $settings->decimals_point == '.'}selected{/if}>точка: 12.45 рублей</option>
    <option value=',' {if $settings->decimals_point == ','}selected{/if}>запятая: 12,45 рублей</option>
    </select>
    </div>

    <div class="form-group">
    <label>Разделитель тысяч</label>
    <select class="form-control" name="thousands_separator">
    <option value='' {if $settings->thousands_separator == ''}selected{/if}>без разделителя: 1245678 рублей</option>
    <option value=' ' {if $settings->thousands_separator == ' '}selected{/if}>пробел: 1 245 678 рублей</option>
    <option value=',' {if $settings->thousands_separator == ','}selected{/if}>запятая: 1,245,678 рублей</option>
    </select>
    </div>
    </div>
</div>


<div class="form-group">
    <label>Показывать в списке товаров валюту и переключатель валют на странице товара в админке</label>

    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default4 on {if $settings->catalog_show_currency}active{/if}">
            <input type="radio" name="catalog_show_currency" value=1 {if $settings->catalog_show_currency}checked{/if}> Да, я использую мультивалютность
        </label>
        <label class="btn btn-default4 off {if !$settings->catalog_show_currency}active{/if}">
            <input type="radio" name="catalog_show_currency" value=0 {if !$settings->catalog_show_currency}checked{/if}> Нет, все товары в одной валюте
        </label>
    </div>
</div>

<input type="hidden" name="mode" value="currencies"/>