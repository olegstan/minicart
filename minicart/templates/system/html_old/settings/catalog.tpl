<legend>Каталог товаров на сайте</legend>


<div class="form-group">
    <label>Отображаем товары в категории по умолчанию</label>
    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default {if !$settings->default_show_mode || $settings->default_show_mode == 'list'}active{/if}">
            <input type="radio" name="default_show_mode" value="list" {if !$settings->default_show_mode || $settings->default_show_mode == 'list'}checked{/if}><i class="fa fa-bars"></i> Списком
        </label>
        <label class="btn btn-default {if $settings->default_show_mode == 'tile'}active{/if}">
            <input type="radio" name="default_show_mode" value="tile" {if $settings->default_show_mode == 'tile'}checked{/if}><i class="fa fa-th"></i> Плиткой
        </label>
    </div>
</div>

<hr/>       
<div class="form-group">
    <label>Отображем варианты товара по умолчанию <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="'По наличию' - показываются сначала варианты товара со статусом 'В наличии', потом 'Под заказ', потом 'Нет в наличии'; 'По порядку варинатов товара' - для сортировки используется порядок вариантов товара, который задается в карточке товара"><i class="fa fa-info-circle"></i></a></label>
    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default {if $settings->catalog_default_variants_sort == "stock"}active{/if}">
            <input type="radio" name="catalog_default_variants_sort" value="stock" {if $settings->catalog_default_variants_sort == "stock"}checked{/if}> По наличию
        </label>
        <label class="btn btn-default {if $settings->catalog_default_variants_sort == "position"}active{/if}">
            <input type="radio" name="catalog_default_variants_sort" value="position" {if $settings->catalog_default_variants_sort == "position"}checked{/if}>По порядку вариантов товара
        </label>
        <label class="btn btn-default {if $settings->catalog_default_variants_sort == "price"}active{/if}">
            <input type="radio" name="catalog_default_variants_sort" value="price" {if $settings->catalog_default_variants_sort == "price"}checked{/if}>По цене
        </label>
        <label class="btn btn-default {if $settings->catalog_default_variants_sort == "name"}active{/if}">
            <input type="radio" name="catalog_default_variants_sort" value="name" {if $settings->catalog_default_variants_sort == "name"}checked{/if}>По названию
        </label>
    </div>
</div>


    <div class="form-group">
            <label>Если у товара несколько вариантов (один из которых имеет статус отличный от "Нет в наличии") то скрывать варианты со статусом "Нет в наличии" <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Настройка нужна для товаров у которых несколько вариантов и есть варианты со статусом 'Нет в наличии' и их необходимо скрыть, если у товара только 1 вариант то настройка не будет учитываться"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_hide_nostock_variants}active{/if}">
                    <input type="radio" name="catalog_hide_nostock_variants" value=1 {if $settings->catalog_hide_nostock_variants}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_hide_nostock_variants}active{/if}">
                    <input type="radio" name="catalog_hide_nostock_variants" value=0 {if !$settings->catalog_hide_nostock_variants}checked{/if}>Нет
                </label>
            </div>
        </div>

<hr/>  

<div class="form-group">
<label class="noinline">Какие сортировки используются</label>     
<div class="btn-group" data-toggle="buttons" id="sort_methods">
    <label class="btn {if $settings->settings_sort_position}btn-success active{else}btn-default{/if}">
        <input type="checkbox" name="settings_sort_position" value="1" {if $settings->settings_sort_position}checked{/if}> По порядку
    </label>

    <label class="btn {if $settings->settings_sort_price}btn-success active{else}btn-default{/if}">
        <input type="checkbox" name="settings_sort_price" value="1" {if $settings->settings_sort_price}checked{/if}> По цене
    </label>

    <label class="btn {if $settings->settings_sort_newest}btn-success active{else}btn-default{/if}">
        <input type="checkbox" name="settings_sort_newest" value="1" {if $settings->settings_sort_newest}checked{/if}> По новизне
    </label>
    <label class="btn {if $settings->settings_sort_popular}btn-success active{else}btn-default{/if}">
        <input type="checkbox" name="settings_sort_popular" value="1" {if $settings->settings_sort_popular}checked{/if}> По популярности
    </label>
  
    <label class="btn {if $settings->settings_sort_name}btn-success active{else}btn-default{/if}">
        <input type="checkbox" name="settings_sort_name" value="1" {if $settings->settings_sort_name}checked{/if}> По алфавиту
    </label>

</div>
<hr/> 
<div class="form-group">
<label>Показывать подкатегории с картинками в правой части над товарами <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если отмечено 'Да' - при заходе в категорию содержащую подкатегории, эти подкатегории отобразятся до товаров в виде блоков (картинка + название)"><i class="fa fa-info-circle"></i></a></label>   
    <div class="btn-group noinline" data-toggle="buttons">
        <label class="btn btn-default4 on {if $settings->catalog_show_subcategories}active{/if}">
        <input type="radio" name="catalog_show_subcategories" value=1 {if $settings->catalog_show_subcategories}checked{/if}> Да
        </label>
        <label class="btn btn-default4 off {if !$settings->catalog_show_subcategories}active{/if}">
        <input type="radio" name="catalog_show_subcategories" value=0 {if !$settings->catalog_show_subcategories}checked{/if}> Нет
        </label>
    </div>
</div>  
</div>


<hr/>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label>Порядок отображения товаров по умолчанию</label>
            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="position" {if $settings->catalog_default_sort == "position" || empty($settings->catalog_default_sort)}checked{/if}/>
                    По порядку
                </label>
            </div>

            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="price_asc" {if $settings->catalog_default_sort == "price_asc"}checked{/if}/>
                    По цене (сначала дешевые)
                </label>
            </div>

            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="price_desc" {if $settings->catalog_default_sort == "price_desc"}checked{/if}/>
                    По цене (сначала дорогие)
                </label>
            </div>
            
            <div class="radio">       
                <label>
                    <input type="radio" name="catalog_default_sort" value="popular_asc" {if $settings->catalog_default_sort == "popular_asc"}checked{/if}/>
                    По популярности (сначала популярные)
                </label>
            </div>
            
            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="popular_desc" {if $settings->catalog_default_sort == "popular_desc"}checked{/if}/>
                    По популярности (сначала непопулярные)
                </label>
            </div>
            
            <div class="radio">       
                <label>
                    <input type="radio" name="catalog_default_sort" value="newest_desc" {if $settings->catalog_default_sort == "newest_desc"}checked{/if}/>
                    По новизне (сначала новые)
                </label>
            </div>
            
            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="newest_asc" {if $settings->catalog_default_sort == "newest_asc"}checked{/if}/>
                    По новизне (сначала старые)
                </label>
            </div>
            
            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="name_asc" {if $settings->catalog_default_sort == "name_asc"}checked{/if}/>
                    По алфавиту (A-Z)
                </label>
            </div>
            
            <div class="radio">
                <label>
                    <input type="radio" name="catalog_default_sort" value="name_desc" {if $settings->catalog_default_sort == "name_desc"}checked{/if}/>
                    По алфавиту (Z-A)
                </label>
            </div>

        </div>
    </div>
</div>  

<legend>Статусы товаров</legend>
    <div class="form-group">
            <label>Показывать товары со статусом "Нет в наличии" <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Статус 'Нет в наличии' выставляется для товаров с наличием 0. Если эти товары нужно скрыть с сайта выберите 'Да'"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if !$settings->catalog_show_all_products}active{/if}">
                    <input type="radio" name="catalog_show_all_products" value=0 {if !$settings->catalog_show_all_products}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if $settings->catalog_show_all_products}active{/if}">
                    <input type="radio" name="catalog_show_all_products" value=1 {if $settings->catalog_show_all_products}checked{/if}> Нет
                </label>
            </div>
        </div>
   
       <div class="form-group">
            <label>Использовать умную сортировку? (сначала будут показаны товары со статусом "Есть в наличии", после "Под заказ" и в конце товары "Нет в наличии"</label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_use_smart_sort}active{/if}">
                    <input type="radio" name="catalog_use_smart_sort" value=1 {if $settings->catalog_use_smart_sort}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_use_smart_sort}active{/if}">
                    <input type="radio" name="catalog_use_smart_sort" value=0 {if !$settings->catalog_use_smart_sort}checked{/if}> Нет
                </label>
            </div>
        </div>
        
        
        
       <div class="form-group">
            <label>Какой статус выставлять товару когда он закончился на складе <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если выбрана настройка 'Нет в наличии' товар после того как он закончится на складе будет недоступен для заказа, также если выбрана настройка о скрытии товаров со статусом 'Нет в наличии' эти товары будут скрыты. Если выбрана настройка 'Под заказ' в этом случае товар будет доступен для заказа со статусом 'Под заказ'"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if !$settings->new_status_when_product_is_out_of_stock || $settings->new_status_when_product_is_out_of_stock == 0}active{/if}">
                    <input type="radio" name="new_status_when_product_is_out_of_stock" value=0 {if !$settings->new_status_when_product_is_out_of_stock || $settings->new_status_when_product_is_out_of_stock == 0}checked{/if}>"<i class="fa fa-times-circle"></i> Нет в наличии" и количество "0"
                </label>
                <label class="btn btn-default4 on {if $settings->new_status_when_product_is_out_of_stock == -1}active{/if}">
                    <input type="radio" name="new_status_when_product_is_out_of_stock" value=-1 {if $settings->new_status_when_product_is_out_of_stock == -1}checked{/if}>"<i class="fa fa-truck"></i> Под заказ" и количество "-1"
                </label>
            </div>
        </div>
        
        
        <div class="form-group">
            <label>На странице товара показывать ли блок покупки нескольких единиц товара <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Блок используется для сайтов где покупают более 1 товара"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_show_multi_buy}active{/if}">
                    <input type="radio" name="catalog_show_multi_buy" value=1 {if $settings->catalog_show_multi_buy}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_show_multi_buy}active{/if}">
                    <input type="radio" name="catalog_show_multi_buy" value=0 {if !$settings->catalog_show_multi_buy}checked{/if}> Нет
                </label>
            </div>
        </div>
        

<legend>Код товара</legend>
        <div class="form-group">
            <label>Использовать на сайте код товара и поиск по нему</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->use_product_id}active{/if}">
                    <input type="radio" name="use_product_id" value=1 {if $settings->use_product_id}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->use_product_id}active{/if}">
                    <input type="radio" name="use_product_id" value=0 {if !$settings->use_product_id}checked{/if}> Нет
                </label>
            </div>
            
            <p class="help-block">Код товара это его уникальный идентификатор, он назначается автоматически и отображается в карточке товара.<br/>Если вы используете код товара, поиск автоматически будет искать по нему тоже.</p>
        </div>

<legend>Меню</legend>
        <div class="form-group">
            <label>Количество раскрываемых уровней в каталоге товаров (по умолчанию 0)</label>
            <div class="btn-group noinline" data-toggle="buttons" id="catalog_levels">
              <label class="btn btn-default {if $settings->catalog_count_opened_level == 0 || !$settings->catalog_count_opened_level}active{/if}">
                <input type="checkbox" name="catalog_count_opened_level" value="0" {if $settings->catalog_count_opened_level == 0 || !$settings->catalog_count_opened_level}checked{/if}>0
              </label>
              <label class="btn btn-default {if $settings->catalog_count_opened_level == 1}active{/if}">
                <input type="checkbox" name="catalog_count_opened_level" value="1" {if $settings->catalog_count_opened_level == 1}checked{/if}>1
              </label>
              <label class="btn btn-default {if $settings->catalog_count_opened_level == 2}active{/if}">
                <input type="checkbox" name="catalog_count_opened_level" value="2" {if $settings->catalog_count_opened_level == 2}checked{/if}>2
              </label>
              <label class="btn btn-default {if $settings->catalog_count_opened_level == 3}active{/if}">
                <input type="checkbox" name="catalog_count_opened_level" value="3" {if $settings->catalog_count_opened_level == 3}checked{/if}>3
              </label>
              <label class="btn btn-default {if $settings->catalog_count_opened_level == 99}active{/if}">
                <input type="checkbox" name="catalog_count_opened_level" value="99" {if $settings->catalog_count_opened_level == 99}checked{/if}>Все
              </label>
            </div>

        </div>

<legend>Количество и единицы измерения</legend>
        <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <label>Товаров на странице сайта <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если используется вывод товаров плиткой, например по 4 в ряд то тут рекомендуется значение кратное 4, например 16 или 20"><i class="fa fa-info-circle"></i></a></label>
        <input name="products_num" type="text" class="form-control" value="{$settings->products_num|escape_string}" />
        </div>
        
        <div class="form-group">
        <label>Единицы измерения товаров <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="По умолчанию это штуки, пишется как шт., но если товар продается килограммами то можно указать, например, кг."><i class="fa fa-info-circle"></i></a></label> 
        <input name="units" type="text" class="form-control" value="{$settings->units|escape_string}" />
           </div>

           </div>
        </div>
     
<legend>Покупка товара с переменным количеством <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Данная возможность используется для продажи товара по килограмам, сантиметрам или другим дробным еденицам, то есть можно купить 1.5 кг торта, или 35 см бус, при этом указав цену за 1 кг или 1 см бус"><i class="fa fa-info-circle"></i></a></legend>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
        <label>Использовать покупку с переменным количеством:</label>              <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_use_variable_amount}active{/if}">
                    <input type="radio" name="catalog_use_variable_amount" value=1 {if $settings->catalog_use_variable_amount}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_use_variable_amount}active{/if}">
                    <input type="radio" name="catalog_use_variable_amount" value=0 {if !$settings->catalog_use_variable_amount}checked{/if}> Нет
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-6" id="variable_amount_panel" {if !$settings->catalog_use_variable_amount}style="display:none;"{/if}>
        <div class="form-group">
            <label>Минимальное количество <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Значение будет выставлено по умолчанию для новых товаров"><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="catalog_min_amount" placeholder="" value="{if $settings->catalog_min_amount}{$settings->catalog_min_amount}{else}1{/if}">
        </div>
        <div class="form-group">
            <label>Максимальное количество <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Значение будет выставлено по умолчанию для новых товаров"><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="catalog_max_amount" placeholder="" value="{if $settings->catalog_max_amount}{$settings->catalog_max_amount}{else}10{/if}">
        </div>
        <div class="form-group">
            <label>Шаг изменения <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Шаг может быть дробным, например '0.5'"><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="catalog_step_amount" placeholder="" value="{if $settings->catalog_step_amount}{$settings->catalog_step_amount}{else}1{/if}">
        </div>
        <div class="form-group">
            <label>Название измерения <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например: Длина, Вес"><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="catalog_variable_amount_name" placeholder="" value="{$settings->catalog_variable_amount_name}">
        </div>
        <div class="form-group">
            <label>Единица измерения <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например: см., кг. или мм."><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="catalog_variable_amount_dimension" placeholder="" value="{$settings->catalog_variable_amount_dimension}">
        </div>
    </div>
</div>

<legend>Сопутствующие товары</legend>

        <div class="form-group">
            <label>По умолчанию отображем товары в категории</label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default {if $settings->default_related_products_mode == 'list'}active{/if}">
                    <input type="radio" name="default_related_products_mode" value="list" {if $settings->default_related_products_mode == 'list'}checked{/if}><i class="fa fa-bars"></i> Списком
                </label>
                <label class="btn btn-default {if $settings->default_related_products_mode == 'tile'}active{/if}">
                    <input type="radio" name="default_related_products_mode" value="tile" {if $settings->default_related_products_mode == 'tile'}checked{/if}><i class="fa fa-th"></i> Плиткой
                </label>
            </div>
        </div>


{*
        <div class="form-group">
            <label>По умолчанию отображем сопутвующие товары</label>
            <div class="btn-group noinline" id="related_mode">
                <a href="#" class="btn btn-default {if $settings->default_related_products_mode == 'list'}active{/if}" data-value='list'><i class="fa fa-bars"></i> Списком</a>
                <a href="#" class="btn btn-default {if $settings->default_related_products_mode == 'tile'}active{/if}" data-value='tile'><i class="fa fa-th"></i> Плиткой</a>
            </div>
            <input type=hidden name="default_related_products_mode" value="{$settings->default_related_products_mode}"/>
        </div>
*}

<hr/>        
<legend>Каталог товаров в админке</legend>

        <div class="form-group">
        <label>Товаров на странице в админке</label>
        <input name="products_num_admin" type="text" class="form-control" value="{$settings->products_num_admin|escape_string}" />
        </div>
        
        <div class="form-group">
            <label>Показывать код товара в списке товаров в админке</label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_show_product_id}active{/if}">
                    <input type="radio" name="catalog_show_product_id" value=1 {if $settings->catalog_show_product_id}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_show_product_id}active{/if}">
                    <input type="radio" name="catalog_show_product_id" value=0 {if !$settings->catalog_show_product_id}checked{/if}> Нет
                </label>
            </div>
        </div>
        
        <div class="form-group">
            <label>Показывать артикул в списке товаров в админке</label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_show_sku}active{/if}">
                    <input type="radio" name="catalog_show_sku" value=1 {if $settings->catalog_show_sku}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_show_sku}active{/if}">
                    <input type="radio" name="catalog_show_sku" value=0 {if !$settings->catalog_show_sku}active{/if}> Нет
                </label>
            </div>
        </div>

        <div class="form-group">
            <label>Показывать ID категорий в списке категорий в админке</label>

            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->catalog_show_category_id}active{/if}">
                    <input type="radio" name="catalog_show_category_id" value=1 {if $settings->catalog_show_category_id}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$settings->catalog_show_category_id}active{/if}">
                    <input type="radio" name="catalog_show_category_id" value=0 {if !$settings->catalog_show_category_id}active{/if}> Нет
                </label>
            </div>
        </div>
<hr/>          
        <div class="form-group">
        <label>Категорий на странице в админке <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Подсчитываются только категории 1 уровня"><i class="fa fa-info-circle"></i></a></label>
        <input name="categories_num_admin" type="text" class="form-control" value="{$settings->categories_num_admin|escape_string}" />
        </div>
        
        <div class="form-group">
        <label>Брендов на странице в админке</label>
        <input name="brands_num_admin" type="text" class="form-control" value="{$settings->brands_num_admin|escape_string}" />
        </div>
        
<legend class="mt15">Дополнительные поля <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Как правило в дополнительных полях хранят оптовую цену, название поставщика, % скидки или любую другую полезную информацию"><i class="fa fa-info-circle"></i></a></legend>

<table class="table">
    <thead>
        <tr>
          <th width="30%">Состояние</th>
          <th width="40%">Название поля</th>
          <th width="30%">Показывать на сайте</th>          
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field1_enabled}active{/if}">
                    <input type="radio" name="add_field1_enabled" value=1 {if $settings->add_field1_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field1_enabled}active{/if}">
                    <input type="radio" name="add_field1_enabled" value=0 {if !$settings->add_field1_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
          <td><input name="add_field1_name" type="text" class="form-control" value="{$settings->add_field1_name}" /></th>
          <td>
          <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field1_show_frontend}active{/if}">
                    <input type="radio" name="add_field1_show_frontend" value=1 {if $settings->add_field1_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field1_show_frontend}active{/if}">
                    <input type="radio" name="add_field1_show_frontend" value=0 {if !$settings->add_field1_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>

        </tr>
        
        <tr>
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field2_enabled}active{/if}">
                    <input type="radio" name="add_field2_enabled" value=1 {if $settings->add_field2_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field2_enabled}active{/if}">
                    <input type="radio" name="add_field2_enabled" value=0 {if !$settings->add_field2_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
          <td><input name="add_field2_name" type="text" class="form-control" value="{$settings->add_field2_name}" /></td>
          <td>
          <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field2_show_frontend}active{/if}">
                    <input type="radio" name="add_field2_show_frontend" value=1 {if $settings->add_field2_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field2_show_frontend}active{/if}">
                    <input type="radio" name="add_field2_show_frontend" value=0 {if !$settings->add_field2_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>

        </tr>
        
        <tr>
                  <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field3_enabled}active{/if}">
                    <input type="radio" name="add_field3_enabled" value=1 {if $settings->add_field3_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field3_enabled}active{/if}">
                    <input type="radio" name="add_field3_enabled" value=0 {if !$settings->add_field3_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
          <td>
           <input name="add_field3_name" type="text" class="form-control" value="{$settings->add_field3_name}" />
          </td>
          <td>
          <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_field3_show_frontend}active{/if}">
                    <input type="radio" name="add_field3_show_frontend" value=1 {if $settings->add_field3_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_field3_show_frontend}active{/if}">
                    <input type="radio" name="add_field3_show_frontend" value=0 {if !$settings->add_field3_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>

        </tr>       
      </tbody>      
</table>

<legend class="mt15">Дополнительные флаги <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Флаги отображают какую либо дополнительную настройку, например выгрузку в яндекс-маркет"><i class="fa fa-info-circle"></i></a></legend>

<div class="form-group">
    
<table class="table">
    <thead>
        <tr>
          <th width="30%">Состояние</th>
          <th width="10%">Иконки</th>
          <th width="20%">Название флага</th>
          <th width="20%">Показывать на сайте</th>
          <th width="20%">Значение по умолчанию</th>          
        </tr>
      </thead>
      <tbody>
        <tr>
           <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag1_enabled}active{/if}">
                    <input type="radio" name="add_flag1_enabled" value=1 {if $settings->add_flag1_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag1_enabled}active{/if}">
                    <input type="radio" name="add_flag1_enabled" value=0 {if !$settings->add_flag1_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
             <td>
          <a href="" class="flag1-on"></a>
          <a href="" class="flag1-off"></a>
          </td>
          <td><input name="add_flag1_name" type="text" class="form-control" value="{$settings->add_flag1_name}" /></td>
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag1_show_frontend}active{/if}">
                    <input type="radio" name="add_flag1_show_frontend" value=1 {if $settings->add_flag1_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag1_show_frontend}active{/if}">
                    <input type="radio" name="add_flag1_show_frontend" value=0 {if !$settings->add_flag1_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>
             <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag1_default_value}active{/if}">
                    <input type="radio" name="add_flag1_default_value" value=1 {if $settings->add_flag1_default_value}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag1_default_value}active{/if}">
                    <input type="radio" name="add_flag1_default_value" value=0 {if !$settings->add_flag1_default_value}checked{/if}>Нет
                </label>
            </div>
          </td>
        </tr>
        
        <tr>
                  <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag2_enabled}active{/if}">
                    <input type="radio" name="add_flag2_enabled" value=1 {if $settings->add_flag2_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag2_enabled}active{/if}">
                    <input type="radio" name="add_flag2_enabled" value=0 {if !$settings->add_flag2_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
          <td>
          <a href="" class="flag2-on"></a>
          <a href="" class="flag2-off"></a>
          </td>
          <td>
          <input name="add_flag2_name" type="text" class="form-control" value="{$settings->add_flag2_name}" />
          </td>
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag2_show_frontend}active{/if}">
                    <input type="radio" name="add_flag2_show_frontend" value=1 {if $settings->add_flag2_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag2_show_frontend}active{/if}">
                    <input type="radio" name="add_flag2_show_frontend" value=0 {if !$settings->add_flag2_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>
          
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag2_default_value}active{/if}">
                    <input type="radio" name="add_flag2_default_value" value=1 {if $settings->add_flag2_default_value}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag2_default_value}active{/if}">
                    <input type="radio" name="add_flag2_default_value" value=0 {if !$settings->add_flag2_default_value}checked{/if}>Нет
                </label>
            </div>
          </td>

        </tr>
        
        <tr>
                  <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag3_enabled}active{/if}">
                    <input type="radio" name="add_flag3_enabled" value=1 {if $settings->add_flag3_enabled}checked{/if}>Активен
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag3_enabled}active{/if}">
                    <input type="radio" name="add_flag3_enabled" value=0 {if !$settings->add_flag3_enabled}checked{/if}>Скрыт
                </label>
            </div>
          </td>
            <td>
            <a href="" class="flag3-on"></a>
            <a href="" class="flag3-off"></a>
            </td>
          <td>
           <input name="add_flag3_name" type="text" class="form-control" value="{$settings->add_flag3_name}" />
          </td>
          <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag3_show_frontend}active{/if}">
                    <input type="radio" name="add_flag3_show_frontend" value=1 {if $settings->add_flag3_show_frontend}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag3_show_frontend}active{/if}">
                    <input type="radio" name="add_flag3_show_frontend" value=0 {if !$settings->add_flag3_show_frontend}checked{/if}>Нет
                </label>
            </div>
          </td>
 <td>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $settings->add_flag3_default_value}active{/if}">
                    <input type="radio" name="add_flag3_default_value" value=1 {if $settings->add_flag3_default_value}checked{/if}>Да
                </label>
                <label class="btn btn-default4 off {if !$settings->add_flag3_default_value}active{/if}">
                    <input type="radio" name="add_flag3_default_value" value=0 {if !$settings->add_flag3_default_value}checked{/if}>Нет
                </label>
            </div>
          </td>
        </tr>       
      </tbody>      
</table>

</div>


<input type="hidden" name="mode" value="catalog"/>

<script type="text/javascript">

    $('input[name=catalog_use_variable_amount]').change(function(){
        if ($(this).val() == 1)
            $('#variable_amount_panel').show();
        else
            $('#variable_amount_panel').hide();
    });

    $('#catalog_levels input').change(function(){
        $('#catalog_levels label.active').removeClass('active');
        $('#catalog_levels input:checked').prop('checked', false);
        $(this).prop('checked', true);
        /*$(this).closest('label').addClass('active');
        return false;*/
    });

    $('#related_mode a').click(function(){
        $('#related_mode a.active').removeClass('active');
        $(this).addClass('active');
        $('input[name=default_related_products_mode]').val($(this).attr('data-value'));
        return false;
    });

    $('#sort_methods input').change(function(){
        $(this).closest('label').toggleClass('btn-success').toggleClass('btn-default');
    });
</script>