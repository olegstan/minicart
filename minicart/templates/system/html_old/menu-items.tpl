{* Title *}
{$meta_title='Управление меню' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

{if $menu_items} 
<div id="main_list" class="menu_items">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}?menu_id={$menu_id}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>


    <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
    <div class="col-md-8">
    <legend>Управление меню
    <div id="nestable-menu" class="btn-group">
            <button type="button" data-action="expand-all" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> Развернуть все</button>
            <button type="button" data-action="collapse-all" class="btn btn-default btn-sm"><i class="fa fa-minus"></i> Свернуть все</button>
        </div>
    </legend>
        <div id="new_menu" class="dd">
            {function name=menu_tree}
                {if $items}
                    <ol class="dd-list">
                        {foreach $items as $item}
                            <li class="dd-item dd3-item" data-id="{$item->id}">
                                {*<input type="checkbox" value="{$item->id}" class="checkb">*}
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id]}"{/if}>{$item->name|escape}</a>
                                    <div class="controllinks">
                                    {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                    <a class="toggle-item {if $item->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                    </div>
                                </div>
                                {if $item->subitems}{menu_tree items=$item->subitems}{/if}
                            </li>
                        {/foreach}
                    </ol>
                {/if}
            {/function}
            {menu_tree items=$menu_items->subitems}
        </div>
        
{* Для управления меню массовые операции не испльзуем *}
{*
<div class="form-inline massoperations">
    <div class="checkall">Выделить все &uarr;</div>
    <select>
                  <option>Сделать видимыми</option>
                  <option>Скрыть</option>
                  <option>Удалить</option>
    </select>
    <button class="btn btn-default" type="submit">Применить</button>
    </div>
</div>
*}
</div>        
<div class="col-md-4">
    <legend>Выберите меню</legend>
    <div class="list-group">
          <a href="{$config->root_url}{$module->url}0/" class="list-group-item {if $menu_id == 0}active{/if}">Основное меню</a>
          <a href="{$config->root_url}{$module->url}1/" class="list-group-item {if $menu_id == 1}active{/if}">Верхнее меню</a>
          <a href="{$config->root_url}{$module->url}2/" class="list-group-item {if $menu_id == 2}active{/if}">Дополнения</a>
    </div>
</div>

</div>
    </form>


    <script type="text/javascript">
        $(document).ready(function(){
            $('#new_menu').nestable();
        });

        $('.delete-item').on('click', function() {
            confirm_popover_box($(this), function(obj){
                delete_menu_item(obj);
            });
            return false;
        });

        function delete_menu_item(obj){
            var href = obj.attr('href');
            var li = obj.closest('li');
            $.get(href, function(data){
                if (data.success)
                    li.remove();
                return false;
            });
            return false;
        }

        $('.toggle-item').on('click', function() {
            var item = $(this);
            var href = item.attr('href');
            var enabled = item.hasClass('light-on')?1:0;
            $.get(href, function(data) {
                if (data.success)
                {
                    item.removeClass('light-on');
                    item.removeClass('light-off');
                    enabled = 1 - enabled;
                    if (enabled)
                        item.addClass('light-on');
                    else
                        item.addClass('light-off');
                }
            });
            return false;
        });

        $('#nestable-menu').on('click', function(e)
        {
            var target = $(e.target),
                action = target.data('action');
            if (action === 'expand-all') {
                $('#new_menu').nestable('expandAll');
            }
            if (action === 'collapse-all') {
                $('#new_menu').nestable('collapseAll');
            }
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    </script>
</div>
{else}
Нет пунктов меню
{/if}