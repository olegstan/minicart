{* Title *}
{if $review->id}{$meta_title='Редактирование отзыва' scope=parent}{else}{$meta_title='Добавление отзыва' scope=parent}{/if}

<div id="main_list">
    <form id="form-menu" method="post" action="">
        <div class="controlgroup">
            <div class="currentfilter">
                <div class="input-group">
                    <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                    <span class="input-group-btn">
                        {if $keyword}
                            <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                        {/if}
                        <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </div>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row {if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Отзыв №{$review->id}
                    <div class="btn-group floatright" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $review->is_visible}active{/if}">
                            <input type="radio" name="visible" value="1" {if $review->is_visible}checked=""{/if}> <i class="fa fa-check"></i> Одобрен
                        </label>
                        <label class="btn btn-default4 off {if !$review->is_visible}active{/if}">
                            <input type="radio" name="visible" value="0" {if !$review->is_visible}checked=""{/if}> Скрыт
                        </label>
                    </div>
                </legend>
        
                <div class="onereview-rate">
                    <div id="user-rating" class="rating">
                        <input type="hidden" name="val" value="{$review->rating}">
                        <input type="hidden" name="vote-id" value="1"/>
                    </div>
                    {*<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star-half-o"></i><br/>отлично*}
                    <input type="hidden" name="rating" value="{$review->rating}"/>
                </div>
          
                  <div class="row">
                    <div class="col-md-7">
                        <div class="form-group">
                            <label>Пользователь <a href="#" id="edit-user" class="btn btn-link btn-xs"><span>Редактировать</span></a></label>
                            <input id="user-name" type="text" class="form-control" placeholder="" value="{if $review->user}{$review->user->name|escape_string}{else}{$review->name|escape_string}{/if}" readonly>
                    {*if $review->user*}
                        <select id="user-select-id" name="user_id" style="width:100%" class="hidden">
                            <option></option>
                            {foreach $all_users as $u}
                                <option value='{$u->id}' {if $u->id == $review->user_id}selected{/if}>{$u->name|escape}</option>
                            {/foreach}
                        </select>
                    {*else*}
                        <input id="user-input-id" name="name" type="text" class="form-control hidden" value="{$review->name}"/>
                    {*/if*}
                </div>
                </div>
                    <div class="col-md-5 hidden" id="user-type-switch">
                        <div class="form-group">
                            <label>Отзыв от имени</label>
                            <div class="btn-group noinline" data-toggle="buttons">
                                <label class="btn btn-default {if $review->user}active{/if}">
                                <input type="radio" name="review_from" value="user" {if $review->user}checked=""{/if}><i class="fa fa-user"></i> Пользователя
                                </label>
                                <label class="btn btn-default {if !$review->user}active{/if}">
                                <input type="radio" name="review_from" value="anonym" {if !$review->user}checked=""{/if}> Анонима
                                </label>
                            </div>
                        </div>
                    </div>
                </div> 
        
                <div class="form-group">
                    <label>Ваш отзыв в 2-ух словах</label>
                    <input type="text" class="form-control" name="short" placeholder="" value="{$review->short|escape_string}">
                </div>
        
                <div class="form-group">
                    <label>Достоинства</label>
                    <textarea class="form-control" rows="2" name="pluses">{$review->pluses}</textarea>{* Здесь тоже ставим автоувеличение поля*}
                </div>
        
                <div class="form-group">
                    <label>Недостатки</label>
                    <textarea class="form-control" rows="2" name="minuses">{$review->minuses}</textarea>{* Здесь тоже ставим автоувеличение поля*}
                </div>
        
                <div class="form-group">
                    <label>Комментарий</label>
                    <textarea class="form-control" rows="2" name="comments">{$review->comments}</textarea>{* Здесь тоже ставим автоувеличение поля*}
                </div>
        
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="recommended" value=1 {if $review->recommended}checked=""{/if}> Да я рекомендую этот товар
                    </label>
                </div>

                <div class="form-group">
                    <legend>Изображения</legend>
                    {include file='object-image-placeholder.tpl' object=$review images_object_name='reviews'}
                </div>
            </div>
    
            <div class="col-md-4">
                <legend>Товар</legend>
                <div class="onereview-product">
                    {if $review->product_image}
                    <div class="onereview-prod-image">
                        <a href="{$config->root_url}{$product_module->url}{url add=['id'=>$review->product->id]}" target="_blank"><img src="{$review->product_image->filename|resize:'products':52:52}"></a>
                    </div>
                    {/if}
                    <div class="onereview-prod-title"><a href="{$config->root_url}{$product_module->url}{url add=['id'=>$review->product->id]}">{$review->product->name}</a></div>
                    {if $review->product_variant}<div class="onereview-prod-price">{$review->product_variant->price|convert:NULL:true} {if !empty($review->product->currency_id)}{$currencies[$review->product->currency_id]->sign}{else}{$admin_currency->sign}{/if}</div>{/if}
                    <div class="onereview-prod-availability">
                        <div class="onereview-prod-status">
                            {if $review->product->in_stock}
                                <i class="fa fa-check"></i> Есть в наличии <br/>
                            {elseif $review->product->in_order}
                                <i class="fa fa-truck"></i> Под заказ<br/>
                            {else}
                                <i class="fa fa-times-circle"></i> Нет в наличии
                            {/if}
                        </div>
                    </div>
                </div>
        
                <legend>Параметры</legend>
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Дата</label>
                        <div class="input-group date" id="datepicker">
                          <input class="form-control" type="text" class="span2" name="datetime_date" value="{$review->datetime|date}">
                          <span class="input-group-addon add-on"><i class="fa fa-th"></i></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Время</label>
                        <input id="timepicker" type="text" class="form-control" placeholder="" name="datetime_time" value="{$review->datetime|time}">
                    </div>
                </div>
            </div>
            <hr/>
            <div class="form-group">
                <label>Отзыв полезен?</label>
                <div class="row">
                    <div class="col-md-6">
                        <label><span class="usergrade-yes">Да</span></label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button id="helpful_sub" class="btn btn-default" type="button"><i class="fa fa-minus"></i></button>
                            </span>
                            <input type="text" class="form-control usergrade-input" placeholder="" name="helpful" value="{$review->helpful}">
                            <span class="input-group-btn">
                                <button id="helpful_add" class="btn btn-default" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label><span class="usergrade-no">Нет</span></label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button id="nothelpful_sub" class="btn btn-default" type="button"><i class="fa fa-minus"></i></button>
                            </span>
                            <input type="text" class="form-control usergrade-input" placeholder="" name="nothelpful" value="{$review->nothelpful}">
                            <span class="input-group-btn">
                                <button id="nothelpful_add" class="btn btn-default" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            
            <legend>Ранг отзыва <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Сортировка по актуальности выводит в верхнюю часть списка лучшие отзывы. Мы учитываем такие показатели как оценки полезности, время публикации, изображения и другие характеристики, которые могут быть полезны для читателей"><i class="fa fa-info-circle"></i></a></legend>
            
            <table class="table table-condensed">
              <thead>
                <tr>
                  <th>Итоговый ранг</th>
                  <th>{$review->rank}</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Новизна <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Новые отзывы получают больше очков"><i class="fa fa-info-circle"></i></a></td>
                  <td>{$rank_arr.newest_rank}</td>
                </tr>
                <tr>
                  <td>Полезность <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Учитывается полезность отзыва - чем отзыв полезней тем больше очков"><i class="fa fa-info-circle"></i></a></td>
                  <td>{$rank_arr.popular_rank}</td>
                </tr>
                <tr>
                  <td>Наполненность <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Учитывается наполненость отзыва, содержательные отзывы с изображениями получают больше очков"><i class="fa fa-info-circle"></i></a></td>
                  <td>{$rank_arr.fill_rank}</td>
                </tr>
                <tr>
                  <td>Жалобы <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отзывы на которые поуступают жалобы будут понижены в рейтинге"><i class="fa fa-info-circle"></i></a></td>
                  <td>{$rank_arr.claim_rank}</td>
                </tr>
              </tbody>
            </table>            
            
            <legend>Жалобы</legend>
            {if $review->claims}
                <div class="reports">
                    {foreach $review->claims as $claim}
                        <div class="alert alert-warning fade in">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true" data-href="{$config->root_url}{$module->url}{url add=['mode'=>'delete_claim', 'claim_id'=>$claim->id, 'ajax'=>1]}">×</button>
                            {if $claim->claim_type == "profanity"}
                                Ненормативная лексика
                            {elseif $claim->claim_type == "spam"}
                                Рассылка рекламной информации (спам)
                            {elseif $claim->claim_type == "foul"}
                                Нарушение правил сайта
                            {else}
                                <strong>Другая причина</strong><br/>
                                {$claim->claim_text}
                            {/if}
                        </div>
                    {/foreach}

                </div>
            {else}
                <p class="help-block">Жалоб нет</p>
            {/if}
        
            <hr/>
            <a href="#" class="btn btn-default w100" id="delete-review"><i class="fa fa-times"></i> Удалить отзыв</a>
        </div>
    </div>
</div>

    <input type="hidden" name="id" value="{$review->id}"/>
    <input type="hidden" name="return_page" value="{if $page}{$page}{else}1{/if}"/>
    <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
    <input type="hidden" name="close_after_save" value="0" id="close_after_save"/>
    {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
</form>

<script type="text/javascript">

    $(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $("select[name=user_id]").select2({
            placeholder: "Выберите пользователя",
            allowClear: true
        });

        $('#datepicker').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: true,
            language: "ru",
            autoclose: true,
            todayHighlight: true
        });

        $('#timepicker').mask("99:99");

        $('textarea[name=pluses], textarea[name=minuses], textarea[name=comments]').autosize();
    });

    $('#user-rating').rating({
        fx: 'full',
        image: '{$path_frontend_template}/img/stars/big.png',
        loader: '<i class="fa fa-spinner fa-spin"></i>',
        minimal: 1,
        preselectMode: true,
        click: function(data){
            $('input[name=rating]').val(data);
        }
    });

    $('#edit-user').click(function(){
        $(this).hide();
        $('#user-name').addClass('hidden');
        $('#user-type-switch').removeClass('hidden');
        if ($('input[name=review_from]:checked').val() == "user")
            $('#user-select-id').removeClass('hidden');
        else
            $('#user-input-id').removeClass('hidden');
        return false;
    });

    $('#user-type-switch label.btn').click(function(){
        if ($(this).find('input[name=review_from]').val() == 'user'){
            $('#user-input-id').addClass('hidden');
            $('#user-select-id').removeClass('hidden');
        }
        else{
            $('#user-input-id').removeClass('hidden');
            $('#user-select-id').addClass('hidden');
        }
    });

    $('#helpful_sub').click(function(){
        var v = parseInt($('input[name=helpful]').val());
        if (v>0)
            v -= 1;
        $('input[name=helpful]').val(v);
    });
    $('#helpful_add').click(function(){
        var v = parseInt($('input[name=helpful]').val());
        v += 1;
        $('input[name=helpful]').val(v);
    });
    $('#nothelpful_sub').click(function(){
        var v = parseInt($('input[name=nothelpful]').val());
        if (v>0)
            v -= 1;
        $('input[name=nothelpful]').val(v);
    });
    $('#nothelpful_add').click(function(){
        var v = parseInt($('input[name=nothelpful]').val());
        v += 1;
        $('input[name=nothelpful]').val(v);
    });

    $("a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $("a.saveclose").click(function(){
        $('#close_after_save').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $('#delete-review').click(function(){
        confirm_popover_box($(this), function(obj){
            $.ajax({
                type: 'GET',
                url: "{$config->root_url}{$module->url}{url add=['id'=>$review->id, 'mode'=>'delete', 'ajax'=>1]}",
                success: function(data) {
                    document.location = "{$config->root_url}{$main_module->url}{url add=['page'=>$page]}";
                    return false;
                }
            });
        });
        return false;
    });
    $('div.reports div.alert button.close').click(function(){
        var href = $(this).data('href');
        $.get(href, function(data) {
        });
    });
</script>