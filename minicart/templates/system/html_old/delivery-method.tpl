{* Title *}
{$meta_title='Редактирование способа доставки' scope=parent}

<form method=post id="delivery">

        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <fieldset>
        <legend>{if $delivery}Редактирование способа доставки{else}Добавление способа доставки{/if}</legend>


        <div class="row">
        <div class="col-md-8">
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Наименование способа доставки</label>
        <input type="text" class="form-control" name="name" placeholder="Введите наименование способа доставки" value="{$delivery->name|escape_string}"/>
        </div>
        </div>
        
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $delivery->enabled || !$delivery}active{/if}">
                        <input type="radio" name="enabled" id="option1" value=1 {if $delivery->enabled || !$delivery}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $delivery && !$delivery->enabled}active{/if}">
                        <input type="radio" name="enabled" id="option2" value=0 {if $delivery && !$delivery->enabled}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        
        <div class="row">        
            <div class="col-md-12">
                <label>Выберите тип доставки</label>
                <div class="btn-group noinline" data-toggle="buttons" id="delivery-types">
                      <label class="btn btn-default4 on {if $delivery->delivery_type == 'paid' || !$delivery}active{/if}" data-toggle="popover" title="" data-content="<strong>Платная</strong> &mdash; стандартная платная доставка, в списке способов доставки будет указана ее стоимость.">
                        <input type="radio" name="delivery_type" value="paid" {if $delivery->delivery_type == 'paid' || !$delivery}checked{/if}> Платная
                      </label>
                      <label class="btn btn-default4 on {if $delivery->delivery_type == 'free'}active{/if}" data-toggle="popover" title="" data-content="<strong>Бесплатная</strong> &mdash; доставка будет всегда бесплатна.">
                        <input type="radio" name="delivery_type" value="free" {if $delivery->delivery_type == 'free'}checked{/if} > Бесплатная
                      </label>
                      <label class="btn btn-default4 on {if $delivery->delivery_type == 'separately'}active{/if}" data-toggle="popover" title="" data-content="<strong>Оплачивается отдельно</strong> &mdash; в заказе не будет учитваться стоимость этого способа доставки, часто эта функция нужна когда стомость доставки заранее неизвестна, например при доставке крупногабаритных товаров и их подъеме.">
                        <input type="radio" name="delivery_type" value="separately" {if $delivery->delivery_type == 'separately'}checked{/if}> Оплачивается отдельно
                      </label>
                    </div>
            </div>
        </div>
        
        <div class="row mt15 {if $delivery->delivery_type == 'free' || $delivery->delivery_type == 'separately'}hidden{/if}" id="paid-section">        
            <div class="col-md-6">        
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Стоимость доставки</span>
                        <input type="text" class="form-control taright" name="price" placeholder="Введите стоимость" value="{$delivery->price|string_format:"%d"}"/>
                        <span class="input-group-addon">руб.</span>
                    </div>
                </div>
            </div>
        
            <div class="col-md-6">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Бесплатна от</span>
                        <input type="text" class="form-control taright" name="free_from" placeholder="" value="{if $delivery && $delivery->free_from!=0}{$delivery->free_from|string_format:"%d"}{/if}"/>
                        <span class="input-group-addon">руб.</span>
                       </div>
                    <p class="help-block">Если доставка платна всегда - оставьте это поле пустым</p>
                </div>
            </div>
        </div>
        
        <div class="form-group mt15">
        <label>Описание</label>
        <textarea name="description" class="form-control ckeditor" rows="6">{$delivery->description}</textarea>
        </div>

        
        
        </div>
        
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Доступные методы оплаты</h3>
                </div>
                 <ul class="list-group">
                {foreach $payment_methods as $payment_method}
                    <li class="list-group-item list-group-select {if !empty($delivery_payments) && in_array($payment_method->id, $delivery_payments)}list-group-item-success{/if} {if !$payment_method->enabled}list-group-item-hidden{/if}">
                        <div class="select-onoff onoff">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-default4 btn-xs on {if !empty($delivery_payments) && in_array($payment_method->id, $delivery_payments)}active{/if}">
                                    <input type="radio" value="{$payment_method->id}" name="delivery_payments[{$payment_method->id}]" {if !empty($delivery_payments) && in_array($payment_method->id, $delivery_payments)}checked{/if}><i class="fa fa-check"></i></label>
                                <label class="btn btn-default4 btn-xs off {if !empty($delivery_payments) && !in_array($payment_method->id, $delivery_payments)}active{/if}">
                                    <input type="radio" value="0" name="delivery_payments[{$payment_method->id}]" {if !empty($delivery_payments) && !in_array($payment_method->id, $delivery_payments)}active{/if}><i class="fa fa-times"></i></label>
                            </div>
                        </div>
                        {$payment_method->name} {if !$payment_method->enabled}(скрыт){/if}
                    </li>
                {/foreach}
            </ul>
        </div>        
        
       <div class="form-group">
                    <label>Этот способ доставки явлется пунктом самовывоза <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="В корзине при выборе этого способа доставки будет скрыто поле Адрес доставки"><i class="fa fa-info-circle"></i></a></label>
                    
                    <div class="btn-group noinline" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $delivery->is_pickup}active{/if}">
                        <input type="radio" name="is_pickup" value="1" {if $delivery->is_pickup}checked{/if}> Да
                      </label>
                      <label class="btn btn-default4 off {if !$delivery->is_pickup}active{/if}">
                        <input type="radio" name="is_pickup" value="0" {if !$delivery->is_pickup}checked{/if}> Нет
                      </label>
                    </div>
        </div>
         
         
            
                     
        </div>

        <input type="hidden" name="id" value="{$delivery->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
    </fieldset>
</form>

<script type="text/javascript">

    $('.list-group .list-group-item label.btn').click(function(){
        var li = $(this).closest('li.list-group-item');
        if ($(this).find('input').val().length > 0)
            li.addClass('list-group-item-success');
        else
            li.removeClass('list-group-item-success');
    });

    $("form#delivery a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#delivery a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $('#delivery-types label.btn').popover({
            html: true,
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });

        $('#delivery-types label.btn').click(function(){
            if ($(this).find('input').val() == "paid")
                $('#paid-section').removeClass('hidden');
            else
                $('#paid-section').addClass('hidden');
        });
    });
</script>