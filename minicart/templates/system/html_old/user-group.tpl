{* Title *}
{$meta_title='Редактирование группы пользователя' scope=parent}

<form method=post id="user_group">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

     <div class="row">
        <div class="col-md-9">
        <legend>{if $user_group}Редактирование группы пользователя{else}Добавление группы пользователей{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Название группы в единственном числе <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например: Постоянный покупатель"><i class="fa fa-info-circle"></i></a></label>
        <div class="input-group">
        <input type="text"  class="form-control" name="name" placeholder="Введите название группы" value="{$user_group->name|escape_string}"/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id группы">id:{$user_group->id}</a></span>
        </div>
        </div>
    <div class="form-group">
            <label>Наименование группы в множественном числе <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например: Постоянные покупатели"><i class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="group_name" placeholder="Введите наименование статуса группы" value="{$user_group->group_name|escape_string}"/>
        </div>
        </div>
        </div>
        
        <table class="table table-bordered">
            <tr>
                    <td>Элемент</td>
                    <td>Права</td>
        </tr>
        <tr>
            <td>Авторизация в админке</td>
            <td>

                    <label class="checkbox"><input id="check36" type="checkbox" name="modules[36]" {if in_array("LoginControllerAdmin", $group_permissions)}checked{/if}> <label for="check36" class="{if in_array("LoginControllerAdmin", $group_permissions)}checked {else}notchecked{/if}">Есть</label></label>

            </td>
        </tr>
            {foreach $menu_items as $item}
                <tr>
                    <td>{$item->name} {if $item->menuitem_name}({$item->menuitem_name}){/if}</td>
                    <td>
                        {if $item->module_name}

                            <label class="checkbox nomargin">
                            
                            <input id="check-{$item->module_name}" type="checkbox" name="modules[{$item->menuitem_module_id}]" {if in_array($item->module_name, $group_permissions)}checked{/if}> 
                            <label for="check-{$item->module_name}" class="{if in_array($item->module_name, $group_permissions)}checked{else}notchecked{/if}">Есть</label>
                            </label>

                        {/if}
                    </td>
                </tr>
                {foreach $item->subitems as $subitem}
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp; {$subitem->name} {if $subitem->menuitem_name}({$subitem->menuitem_name}){/if}</td>
                        <td>
                            {if $subitem->module_name}

                                <label class="checkbox nomargin"><input id="check_{$subitem->module_name}" type="checkbox" name="modules[{$subitem->menuitem_module_id}]" {if in_array($subitem->module_name, $group_permissions)}checked{/if}> <label for="check_{$subitem->module_name}" class="{if in_array($subitem->module_name, $group_permissions)}checked{else}notchecked{/if}">Есть</label></label>

                            {/if}
                        </td>
                    </tr>
                    {foreach $subitem->subitems as $sub2item}
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {$sub2item->name} {if $sub2item->menuitem_name}({$sub2item->menuitem_name}){/if}</td>
                            <td>
                                {if $sub2item->module_name}

                                    <label class="checkbox nomargin"><input id="check_{$sub2item->module_name}" type="checkbox" name="modules[{$sub2item->menuitem_module_id}]" {if in_array($sub2item->module_name, $group_permissions)}checked{/if}> <label for="check_{$sub2item->module_name}" class="{if in_array($sub2item->module_name, $group_permissions)}checked{else}notchecked{/if}">Есть</label></label>
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {/foreach}
            {/foreach}
        </table>
        
        
        <input type="hidden" name="id" value="{$user_group->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </div>
    
    <div class="col-md-3">
        <legend>Параметры</legend>
          <div class="form-group">
            <label>CSS-стиль</label>
            <input type="text" class="form-control" name="css_class" placeholder="" value="{if $user_group}{$user_group->css_class}{else}label-default{/if}">
          </div>
          <div class="form-group">
            <label>Скидка, %</label>
            <input type="text" class="form-control" name="discount" placeholder="" value="{if $user_group->discount}{$user_group->discount}{else}0{/if}">
        </div>
    
    </div> 
    
    
    </div>
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $("form#user_group a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#user_group a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#user_group a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>