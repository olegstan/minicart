{* Title *}
{$meta_title='Категории материалов' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}                
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>               
            </div>
            
            </div>
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Категории материалов
               <div id="nestable-menu" class="btn-group">
            <button type="button" data-action="expand-all" class="btn btn-sm btn-default"><i class="fa fa-plus"></i> Развернуть все</button>
            <button type="button" data-action="collapse-all" class="btn btn-sm btn-default"><i class="fa fa-minus"></i> Свернуть все</button>
        </div>
                </legend>

                {if $categories}

                <div id="new_menu" class="dd">
                    {function name=categories_tree}
                        {if $items}
                            <ol class="dd-list">
                                {foreach $items as $item}
                                    <li class="dd-item dd3-item" data-id="{$item->id}">
                                        <input type="checkbox" value="{$item->id}" class="checkb">
                                        <div class="dd-handle dd3-handle"></div>
                                        <div class="dd3-content {if $keyword && !in_array($item->id, $cats_filtered_ids) && array_intersect($item->children, $cats_filtered_ids)|count>0}searchnone{/if}">
                                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id]}"{/if} class="catname">{$item->name|escape}</a>

                                        </div>
                                        <div class="controllinks">

                                                
                                            <a title="Количество материалов в этой категории и подкатегориях"><code class="materials">
                                                {$item->materials_count}</code></a>
                                                <code class="opcity50">{$item->id}</code>
                                                {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                                <a class="toggle-item {if $item->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                            </div>
                                        {if $item->subcategories}{categories_tree items=$item->subcategories}{/if}
                                    </li>
                                {/foreach}
                            </ol>
                        {/if}
                    {/function}
                    {categories_tree items=$categories}
                </div>

                {else}
                    Категории не найдены
                {/if}

                {include file='pagination.tpl'}

                {* Для управления меню массовые операции не испльзуем *}
                {if $allow_edit_module}
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все &uarr;</div>
                    <select id="mass_operation" class="form-control">
                        <option value='1'>Сделать видимыми</option>
                        <option value='2'>Скрыть</option>
                        <option value='3'>Удалить</option>
                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
                {/if}
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $categories}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 8
            });

            {if !$keyword}
                var collapsed = {literal}{{/literal}{foreach $collapsed as $c}{$c@key}:{$c}{if !$c@last},{/if}{/foreach}{literal}}{/literal};

                for(var i in collapsed)
                {
                    if (collapsed[i])
                    {
                        var li = $('#new_menu li[data-id='+i+']');

                        var lists = li.children('ol');
                        if (lists.length) {
                            li.addClass('dd-collapsed');
                            li.children('[data-action="collapse"]').hide();
                            li.children('[data-action="expand"]').show();
                            li.children('ol').hide();
                        }
                    }
                }
            {/if}
        });

        $('a.save').click(function(){
            //var obj = $(this).closest('form').find('div.controlgroup');
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            var _collapsed = {};
            $('#new_menu li.dd-item').each(function(){
                _collapsed[$(this).attr('data-id')] = $(this).hasClass('dd-collapsed')?1:0;
            });
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu,
                    collapsed: _collapsed},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('#nestable-menu').on('click', function(e)
    {
        var target = $(e.target),
            action = target.data('action');
        if (action === 'expand-all') {
            $('#new_menu').nestable('expandAll');
        }
        if (action === 'collapse-all') {
            $('#new_menu').nestable('collapseAll');
        }
    });

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_material_category_item(obj);
        });
        return false;
    });

    function delete_material_category_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');

                var parent = item.closest('li');
                parent.find('li.dd-item').each(function(){
                    var subitem = $(this).find('a.toggle-item').first();
                    subitem.removeClass('light-off').removeClass('light-on');
                    if (enabled)
                        subitem.addClass('light-on');
                    else
                        subitem.addClass('light-off');
                });
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (operation == 3 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#new_menu input[type=checkbox]:checked").each(function(){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-on');
                                });

                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.first().hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-off');
                                });
                            }
                        });
                    break;
                {*case 3:    //Удаление
                    var subitem = item.find('a.delete-item').first();
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                                location.reload();
                        }
                    });
                    break;*}
            }
        });
        return false;
    });
</script>
