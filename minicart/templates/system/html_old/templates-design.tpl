{$meta_title = "Дизайн" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}"/>
    <input type=hidden name="theme" value="{$settings->theme}"/>

    <!-- Параметры -->

  <legend class="mt20">Шаблоны дизайна</legend>
  
  {if $templates}
    <ul class="tamplates-list">
        {foreach $templates as $template}
            <li>
                <div class="row">
                    <div class="template-image col-md-4">
                        <div class="template-priview" style="background:url({$template.image})"></div>
                    </div>
                    <div class="template-body col-md-8">
                        <div class="templante-name">{$template.name}</div>
                        <div class="template-version">Версия: {$template.version}</div>
                        <div class="template-description">{$template.description}</div>
                        <div class="template-autor">Разработчкик: <a href="{$template.developer}" target="_blank">{$template.developer}</a></div>

                        <div class="template-controls">
                            {if $settings->theme == $template.system_name}
                                <button type="button" class="btn btn-success"><i class="fa fa-check-square-o"></i> Текущий шаблон</button>
                            {else}
                                <button type="button" class="btn btn-default" data-theme="{$template.system_name}">Установить шаблоном по умолчанию</button>
                            {/if}
                        </div>
                    </div>
                </div>
            </li>
        {/foreach}
    </ul>
  {else}
    Нет доступных шаблонов
  {/if}
</form>
<!-- Основная форма (The End) -->

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $('ul.tamplates-list button.btn-default').click(function(){
        $(this).closest('form').find('input[name=theme]').val($(this).data('theme'));
        $(this).closest('form').submit();
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>