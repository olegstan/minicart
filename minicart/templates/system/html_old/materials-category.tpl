{* Title *}
{$meta_title='Редактирование категории материалов' scope=parent}

<form method=post id="category">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>
     <div class="row">
        <div class="col-md-8">
        <legend>{if $category}Редактирование категории материалов{else}Добавление категории материалов{/if}</legend>
        <div class="row">
        <div class="col-md-8">
        <div class="form-group"> 
        <label>Наименование категории материалов</label>
        <div class="input-group"> 
        <input type="text" class="form-control" name="name" placeholder="Введите наименование категории материалов" value="{$category->name|escape_string}"/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id категории материалов">id:{$category->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $category->is_visible || !$category}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $category->is_visible || !$category}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $category && !$category->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $category && !$category->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Родительская категория</label>
                    <select name="parent_id" class="form-control">
                        <option value='0'>Корневая категория</option>
                        {function name=categories_tree level=0}
                            {foreach $items as $item}
                                {if $category->id != $item->id}
                                    <option value='{$item->id}' {if $category->parent_id == $item->id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                                    {categories_tree items=$item->subcategories level=$level+1}
                                {/if}
                            {/foreach}
                        {/function}
                        {categories_tree items=$categories}
                    </select>
                </div>
            </div>        
        </div>
        <hr />
        <div class="form-group">
        <label class="bigname">Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Описание категории материалов"><i class="fa fa-info-circle"></i></a></label>
        <textarea name="description" class="form-control ckeditor" rows=8>{$category->description}</textarea>
        </div>
        </div>
        
        <div class="col-md-4">
            <legend>Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>

            <div class="form-group">
                <label>URL</label>
                <div class="input-group">
                    <input type="text"  class="form-control" name="url" placeholder="Введите URL" value="{$category->url}"/>
                    <span class="input-group-addon">/</span>
                </div>
            </div>

            <div class="form-group">
            <label>Meta-title</label>
            <input type="text"  class="form-control" name="meta_title" placeholder="Введите meta-title" value="{$category->meta_title}"/>
            </div>


            <div class="form-group">
                <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-keywords"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_keywords" class="form-control">{$category->meta_keywords}</textarea>
            </div>

            <div class="form-group">
                <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_description"  class="form-control">{$category->meta_description}</textarea>
            </div>
            
            <div class="form-group">
        <label>Заголовок страницы <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается внутри H1 на странице категории материалов, это поле опционально, оно нужно если необходимо чтобы заголовок категории материалов отличался от названия категории"><i class="fa fa-info-circle"></i></a></label>
        <input type="text" class="form-control" name="title" placeholder="Введите заголовок страницы" value="{$category->title|escape_string}"/>
        </div>

            <legend>Отображение</legend>
            <div class="form-group">
                <label>Показывать дату у материалов этой категории: <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Для новостей дата обязательна, а для статей она не нужна"><i class="fa fa-info-circle"></i></a></label>
               <div class="block">
                   <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $category->show_date || !$category}active{/if}">
                            <input type="radio" name="show_date" id="option1" value=1 {if $category && $category->show_date}checked{/if}> Показывать
                        </label>
                        <label class="btn btn-default4 off {if $category && !$category->show_date}active{/if}">
                            <input type="radio" name="show_date" id="option2" value=0 {if !$category->show_date || !$category}checked{/if}> Не показывать
                        </label>
                        
                    </div>
                </div>


            </div>
            
            <div class="form-group">
                <label>Отображать по: <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Параметр порядка отображения материалов в категории"><i class="fa fa-info-circle"></i></a></label>
               <div class="block">
                   <div class="btn-group" data-toggle="buttons">
                           <label class="btn btn-default4 on {if $category->sort_type == "position"}active{/if}">
                            <input type="radio" name="sort_type" {if $category->sort_type == "position"}checked{/if} value="position"/> Порядку
                        </label>
                        <label class="btn btn-default4 on {if $category->sort_type == "newest_desc" || !$category->sort_type}active{/if}">
                            <input type="radio" name="sort_type" {if $category->sort_type == "newest_desc" || !$category->sort_type}checked{/if} value="newest_desc"/> Новизне
                        </label>

                        
                    </div>
                </div>


            </div>
            
            <div class="form-group">
                <label>CSS-класс  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Стиль страницы материала, выводится в стиле body"><i class="fa fa-info-circle"></i></a></label>
                <input type="text" class="form-control" name="css_class" placeholder="" value="{$category->css_class}"/>
            </div>
        </div>
 

        <input type="hidden" name="id" value="{$category->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="recreate_seo" value="0"/>
        <input type="hidden" name="close_after_save" value="0"/>
    </fieldset>
       </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {*tinymce.init({
            selector: "textarea.tinymce_descr",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste filemanager"
            ],
            image_advtab: true,
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            setup: function(editor) {
                editor.on('change', function(e) {
                    $('input[name=meta_keywords]').val('changed');
                });
            }
        });*}
    });

    $('#recreate-seo').click(function(){
        $(this).closest('form').find('input[name=recreate_seo]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#category a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#category a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>