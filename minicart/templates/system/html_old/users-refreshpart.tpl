{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

{if $users}

{include file='pagination.tpl'}
<div class="itemslist">
    <ol class="dd-list" data-href="{$config->root_url}{$module->url}{url current_params=$current_params}">
            {foreach $users as $user}
                <li class="dd-item dd3-item" data-id="{$user->id}">
                    <div class="dd3-content">
                        <input type="checkbox" value="{$user->id}" class="checkb">
                        <div class="userblock">

                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'group_id'=>$params_arr['group_id']]}"{/if} class="user-name">{$user->name|escape}</a>

                            {if $user->phone_code && $user->phone}<a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'group_id'=>$params_arr['group_id']]}"{/if} class="user-mail">+7 ({$user->phone_code}) {$user->phone|phone_mask}</a>{/if}
                            {if $allow_edit_module}
                            <div class="controllinks">
                                <a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                <a class="toggle-item {if $user->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>
                            </div>
                            {/if}
                            <div class="user-group"><span class="label {$user->group->css_class}">{$user->group->name|escape}</span></div>
                        </div>
                    </div>
                </li>
            {/foreach}
    </ol>
    </div>

    {include file='pagination.tpl'}

{else}
    <p>Нет пользователей</p>
{/if}{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

{if $users}

{include file='pagination.tpl'}
<div class="itemslist">
    <ol class="dd-list" data-href="{$config->root_url}{$module->url}{url current_params=$current_params}">
            {foreach $users as $user}
                <li class="dd-item dd3-item" data-id="{$user->id}">
                    <div class="dd3-content">
                        <input type="checkbox" value="{$user->id}" class="checkb">
                        <div class="userblock">

                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'group_id'=>$params_arr['group_id']]}"{/if} class="user-name">{$user->name|escape}</a>

                            {if $user->phone_code && $user->phone}<a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'group_id'=>$params_arr['group_id']]}"{/if} class="user-mail">+7 ({$user->phone_code}) {$user->phone|phone_mask}</a>{/if}
                            {if $allow_edit_module}
                            <div class="controllinks">
                                <a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                <a class="toggle-item {if $user->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$user->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>
                            </div>
                            {/if}
                            <div class="user-group"><span class="label {$user->group->css_class}">{$user->group->name|escape}</span></div>
                        </div>
                    </div>
                </li>
            {/foreach}
    </ol>
    </div>

    {include file='pagination.tpl'}

{else}
    <p>Нет пользователей</p>
{/if}