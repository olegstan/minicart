{* Шаблон для загрузки аттачей *}
{* Входные параметры: 
        $object    - объект для которого подгружается шаблон (product, category, tag, etc...)
        $temp_id - если объект еще не существует, то временный id
        $module - модуль
        $attachments_object_name - название объекта
        $limit_attachments - ограничение на кол-во аттачей (по умолчанию - 10)
*}


{if $object}
    <div class="object-attachments main-object-attachments nopadding">
        {include file='object-attachments.tpl'}
    </div>

    <div class="controls">
        <span class="btn btn-file btn-default">
            <i class="fa fa-plus"></i> <span>Добавить файл</span>
            <input id="uploadAttachmentField" type="file" multiple name="uploaded-attachments">
            <i class="fa fa-spin fa-spinner hidden" id="upload-attachment-anim"></i>
        </span>
        <a id="attachment-upload-internet" class="btn btn-default" type="button" title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Загрузить из интернета"><i class="fa fa-globe"></i></a>
        <div id="internet-upload-attachment-box" style="display:none">
            <input type="text"  class="form-control" id="attachment-upload-url" value="" placeholder="Введите URL"/>
            <a id="attachment-upload-btn" class="btn btn-default" type="button" href="#"><i class="fa fa-arrow-circle-right"></i></a>
            <a id="attachment-upload-btn-cancel" class="btn btn-default" type="button" href="#"><i class="fa fa-times"></i></a>
        </div>
    </div>

    <div id="attachment-container" class="attachment-uploader">или перетащите файлы сюда
        <ul id="attachment-list" class="hidden"></ul>
        <div class="clear"></div>
    </div>

{else}
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Внимание!</strong> Для загрузки файлов необходимо сохранить объект
    </div>
{/if}



<script type="text/javascript">

    var attachmentList = $('#attachment-list');
    var dropBoxAttachments = $('#attachment-container');
    var fileInputAttachments = $('#uploadAttachmentField');

    $(document).ready(function(){
        fileInputAttachments.damnUploader({
            url: '{$config->root_url}{$module->url}',
            fieldName:  'uploaded-attachments',
            object_id: '{if $object->id}{$object->id}{else}{$temp_id}{/if}',
            dropBox: dropBoxAttachments,
            limit: {if $limit_attachments}{$limit_attachments}{else}10{/if},
            onSelect: function(file) {
                $('#upload-attachment-anim').removeClass('hidden');
                var addedItem = addFileToQueueAttachments(file);
                fileInputAttachments.damnUploader('startUploadItem', addedItem.queueId);
                return false;
            },
            onAllComplete: function(){
                $('#upload-attachment-anim').addClass('hidden');

                fileInputAttachments.damnUploader('cancelAll');
                attachmentList.html('');

                {if $object->id}
                href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_attachments', 'object'=> $attachments_object_name, 'ajax'=>1]}';
                {else}
                href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_attachments', 'object'=> $attachments_object_name, 'ajax'=>1]}';
                {/if}
                $.get(href, function(data) {
                    if (data.success)
                    {
                        $('div.main-object-attachments').html(data.data);
                    }
                });
                return false;
            },
        });

        // Обработка событий drag and drop при перетаскивании файлов на элемент dropBoxAttachments
        dropBoxAttachments.bind({
            dragenter: function() {
                $(this).addClass('highlighted');
                return false;
            },
            dragover: function() {
                return false;
            },
            dragleave: function() {
                $(this).removeClass('highlighted');
                return false;
            }
        });

        {*$("#files").dragsort({
            dragSelector: "div",
            placeHolderTemplate: "<li class='placeHolder'><div></div></li>",
        });*}
        $('#files').nestable({
                maxDepth: 1
            });
    });

    function updateProgressAttachments(bar, value) {
        bar.css('width', value+'%');
    }

    function addFileToQueueAttachments(file) {

        // Создаем элемент li и помещаем в него название, миниатюру и progress bar
        var li = $('<li/>').appendTo(attachmentList);
        var title = $('<div/>').text(file.name+' ').appendTo(li);
        var cancelButton = $('<a/>').attr({
            href: '#cancel',
            title: 'отменить'
        }).text('X').appendTo(title);

        // Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
        // обозначающий, что переданный параметр на самом деле лишь имитация настоящего File)
        if(!file.fake)
        {
            // Добавляем картинку и прогрессбар в текущий элемент списка
            var img = $('<img/>').appendTo(li);

            var pBar = $('<div class="progress progress-striped active"></div>').appendTo(li);
            var ppBar = $('<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>').appendTo(pBar);

            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах (только в браузерах, поддерживающих FileReader)
            if($.support.fileReading) {
                var reader = new FileReader();
                reader.onload = (function(aImg) {
                    return function(e) {
                        aImg.attr('src', e.target.result);
                        aImg.attr('width', 150);
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }

        // Создаем объект загрузки
        var uploadItem = {
            file: file,
            queueId: 0,
            onProgress: function(percents) {
                updateProgressAttachments(ppBar, percents);
            },
            onComplete: function(successfully, data, errorCode) {
                pBar.removeClass('active');
            },
        };

        // помещаем его в очередь
        var queueId = fileInputAttachments.damnUploader('addItem', uploadItem);
        uploadItem.queueId = queueId;

        // обработчик нажатия ссылки "отмена"
        cancelButton.click(function() {
            fileInputAttachments.damnUploader('cancel', queueId);
            li.remove();
            return false;
        });

        return uploadItem;
    }

    $('div.main-object-attachments').on("click", "a.delete_attachment", function(){
        var item = $(this).closest('li');
        var href = $(this).attr('href');
        $.get(href, function(data) {
            if (data.success)
            {
                item.fadeOut('fast').remove();
                return false;
            }
        });
        return false;
    });

    $('div.main-object-attachments').on("click", "a.file-settings", function(){
        $(this).closest('div').find('div.input-group').toggleClass('hidden');
        return false;
    });

    $('div.main-object-attachments').on("keydown", "input[name^=attachment_name]", function(e){
        if (e.keyCode == 13)
            $(this).closest('div').find('button.save-attachment-name').trigger('click');
    });

    $('div.main-object-attachments').on("click", "button.save-attachment-name", function(){
        var item = $(this).closest('li');
        var href = "{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'update_attachment_name', 'ajax'=>1]}";
        var attachment_id = item.attr('data-id');
        var attachment_name = encodeURIComponent(item.find('input[name^=attachment_name]').val());
        href += '&attachment_id='+attachment_id+'&attachment_name='+attachment_name;
        $.get(href, function(data) {
            if (data.success)
            {
                {if $object->id}
                var href2 = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_attachments', 'object'=>$attachments_object_name, 'ajax'=>1]}';
                {else}
                var href2 = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_attachments', 'object'=>$attachments_object_name, 'ajax'=>1]}';
                {/if}
                $.get(href2, function(data) {
                    if (data.success)
                    {
                        $('div.main-object-attachments').html(data.data);
                    }
                });
            }
        });
        return false;
    });

    $('#attachment-upload-internet').click(function(){
        $('#internet-upload-attachment-box').show();
        return false;
    });

    $('#attachment-upload-btn-cancel').click(function(){
        $('#internet-upload-attachment-box').hide();
        return false;
    });

    $('#attachment-upload-btn').click(function(){
        var image_url = $('#attachment-upload-url').val();
        if (image_url.length == 0)
            return false;
        {if $object->id}
        var href = '{$config->root_url}{$module->url}?id={$object->id}&mode=upload_internet_attachment&object={$attachments_object_name}&ajax=1&attachment_url='+encodebase64(image_url);
        {else}
        var href = '{$config->root_url}{$module->url}?id={$temp_id}&mode=upload_internet_attachment&object={$attachments_object_name}&ajax=1&attachment_url='+encodebase64(image_url);
        {/if}
        $.get(href, function(data) {
            if (data.success)
            {
                {if $object->id}
                var href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_attachments', 'object'=>$attachments_object_name, 'ajax'=>1]}';
                {else}
                var href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_attachments', 'object'=>$attachments_object_name, 'ajax'=>1]}';
                {/if}
                $.get(href, function(data) {
                    if (data.success)
                    {
                        $('div.main-object-attachments').html(data.data);
                    }
                });
            }
        });
        $('#attachment-upload-url').val('');
        $('#internet-upload-attachment-box').hide();
        return false;
    });

</script>