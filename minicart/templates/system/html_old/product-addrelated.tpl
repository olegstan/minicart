{if $products}

    <div id="new_menu" class="dd">
        {if $products}
            <ol class="addlist">
                {foreach $products as $product}
                    <li data-id="{$product->id}" data-image="{$product->image->filename|resize:products:40:40}" data-name="{$product->name}" data-modificators="{$product->modificators}" data-modificators-groups="{$product->modificators_groups}" {if !$product->is_visible}class="lampoff"{/if}>
                        <div class="apimage">{if $product->image}<img src='{$product->image->filename|resize:products:30:30}' alt='{$product->image->name}'>{/if}</div>
                        <div class="apstatus">
                            {if $product->in_stock}
                                <i class="fa fa-check"></i>
                            {elseif $product->in_order}
                                <i class="fa fa-truck"></i>
                            {else}
                                <i class="fa fa-times-circle"></i>
                            {/if}
                        </div>

                        <span class="apname">
                            {$product->name}
                            <span class="apsky">{if $product->variant}{$product->variant->sku}{/if}</span>
                            <span class="apcode">{$product->id}</span>
                        </span>
                    </li>
                {/foreach}
            </ol>
        {/if}

        {*include file='pagination.tpl'*}
    </div>

{else}
    <div class="noresults">Ничего не найдено...</div>
{/if}