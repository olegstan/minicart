{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}
<div id="sortview">
                <div class="itemcount">Материалов: {$materials_count}</div>
                <ul class="nav nav-pills" id="sort">
                <li class="disabled">Сортировать по:</li>
                
                    {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'position'}
                        <li><a class="btn btn-link btn-small current"><span>По порядку</span></a></li>
                    {else}
                        <li><a class="btn btn-link btn-small" href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'position','sort_type'=>'asc','page'=>1]}"><span>По порядку</span></a>
                        </li>
                    {/if}
                    
                    <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'newest'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'newest', 'sort_type'=>'desc','page'=>1]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}Сначала старые{else}Сначала новые{/if}"><i class="{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}fa fa-sort-numeric-desc{else}fa fa-sort-numeric-asc{/if}"></i> <span>Новизне</span></a>
                    </li>
                    
                    <li><a class="btn btn-link btn-small {if $params_arr['sort'] == 'popular'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'popular', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}fa fa-sort-amount-asc{else}fa fa-sort-amount-desc{/if}"></i> <span>Популярности</span></a></li>

                    <li><a class="btn btn-link btn-small {if $params_arr['sort'] == 'name'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'name', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}fa fa-sort-alpha-desc{else}fa fa-sort-alpha-asc{/if}"></i> <span>Алфавиту</span></a></li>
                    </ul>
                </div> 
                
                {if $materials}
                    {include file='pagination.tpl'}
                    <div class="itemslist">
                    <ol class="dd-list" data-href="{$config->root_url}{$module->url}{url current_params=$current_params}">
                        {foreach $materials as $material}
                            <li data-id="{$material->id}" class="dd-item dd3-item">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <input type="checkbox" class="checkb" value="{$item->id}">
                                
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url current_params=$current_params add=['id'=>$material->id, 'page'=>$current_page_num, 'category_id'=>$category_id]}"{/if}>{$material->name|escape}</a>
                                </div>
                                
                               

                                {if $allow_edit_module}
                                
                                    <div class="controllinks">
                                                <code class="opcity50">{$material->id}</code>
                                                <a href="{$config->root_url}{$edit_module->url}{url add=['id'=>$material->id, 'mode'=>'delete', 'ajax'=>1]}" class="delete-item"><i class="fa fa-times"></i></a>
                                                <a href="{$config->root_url}{$edit_module->url}{url add=['id'=>$material->id, 'mode'=>'toggle', 'ajax'=>1]}" class="toggle-item {if $material->is_visible}light-on{else}light-off{/if}"></a>
                                    </div>
                                    
                                     <div class="material-group"><span class="label label-default">{$material->parent_category->name}</span>{if $material->use_main}<span class="label label-default"><label><i class="fa fa-home"></i> Главная</label></span>{/if}</div>
                                    
                                {/if}
                            </li>
                        {/foreach}
                    </ol>
                    </div>
                    
                    {include file='pagination.tpl'}
                    
                {else}
                    <p>Нет материалов</p>
                {/if}

