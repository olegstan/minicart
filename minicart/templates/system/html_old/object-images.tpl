{*

Шаблон для вывода изображений объекта
Входные параметры:
    $object - объект
    $module - модуль
    $images - изображения
    $images_object_name - название объекта для ресайза изображений

*}

<ul id="list1">
    {if $images}
    {foreach $images as $img}
        <li data-id="{$img->id}"><div>
        {if $object->id}
        <a href="{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'delete_image', 'object'=>$images_object_name, 'ajax'=>1, 'image_id'=>$img->id]}" class="delete_image close"><i class="fa fa-times"></i></a>
        <a class="fancybox" rel="group" href="{$img->filename|resize:$images_object_name:1100:800}">
        <i class="fa fa-search-plus"></i>
        </a>
        <img src="{$img->filename|resize:$images_object_name:90:90}" alt="{$img->name}" />
        {else}
        <a href="{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'delete_image', 'object'=>$images_object_name, 'ajax'=>1, 'image_id'=>$img->id]}" class="delete_image close"><i class="fa fa-times"></i></a>
        <a class="fancybox" rel="group" href="{$img->filename|resize_temp:1100:800}">
        <i class="fa fa-search-plus"></i>
        </a>
        <img src="{$img->filename|resize_temp:90:90}" alt="{$img->name}" />
        {/if}
        </div></li>
    {/foreach}
    {/if}
</ul>