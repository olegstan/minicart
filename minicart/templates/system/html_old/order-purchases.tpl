{foreach $purchases as $purchase}
<li data-id="{$purchase->id}" data-product="{$purchase->product->id}" class="order-purchases">
    <input type="hidden" name="purchases[id][{$purchase->id}]" value="{$purchase->id}"/>
    <div class="order-productimage">{if $purchase->image}<img src="{$purchase->image->filename|resize:products:40:40}" alt="{$purchase->image->name|escape}"/>{/if}</div>

    <div class="right-side">
    <div class="controllinks">
        <a href="#" class="delete-item" {if !$edit_order_mode}style="display:none;"{/if}><i class="fa fa-times"></i></a>
    </div>

    <div class="product-price-order">
            <span class="purchase-price"
                data-original-price="{if !empty($purchase->variant)}{$purchase->variant->price|convert:NULL:false}{else}{$purchase->price|convert:NULL:false}{/if}"
                data-price="{$purchase->price|convert:NULL:false}"
                data-price-one-pcs="{$purchase->price|convert:NULL:false}"
                data-price-for-mul="{$purchase->price_for_mul|convert:NULL:false}"
                data-price-additional-sum="{$purchase->additional_sum|convert:NULL:false}">
                {$purchase->price|convert}
            </span>&nbsp;{$main_currency->sign}
    </div>
    <div class="product-quantity-order">
        <div class="input-group">
        <span class="input-group-btn" {if !$edit_order_mode}style="display:none;"{/if}>
            <button data-type="minus" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
        </span>
        <input type="text" class="form-control input-sm" name="purchases[amount][{$purchase->id}]" value="{$purchase->amount}" {if !$edit_order_mode}style="display:none;"{/if}>
        <span class="input-group-btn" {if !$edit_order_mode}style="display:none;"{/if}>
            <button data-type="plus" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
        </span>
        </div>
        {if !$edit_order_mode}
        <span class="purchase-amount">{$purchase->amount}</span>
        <span class="fs12">{$settings->units}</span>
        {/if}
    </div>
    </div>

    <div class="productname">
        <input type="hidden" name="purchases[product_id][{$purchase->id}]" value="{$purchase->product_id}"/>
        <input type="hidden" name="purchases[product_name][{$purchase->id}]" value="{$purchase->product_name|escape}"/>
        <a {if $purchase->product_id}href="{$config->root_url}{$product_module->url}{url add=['id' => $purchase->product_id]}" target="_blank"{/if}>{$purchase->product_name|escape}</a>
        <div class="selectvariant">
        {assign var="sku_value" value=""}
        {assign var="variant_name" value=""}
        {if $purchase->variants|count > 1}
            <select name="purchases[variant_id][{$purchase->id}]" class="form-control" {if !$edit_order_mode}style="display:none;"{/if}>
            {foreach $purchase->variants as $variant}
                <option value="{$variant->id}" {if $variant->id==$purchase->variant_id}selected{assign var="sku_value" value=$variant->sku}{assign var="variant_name" value=$variant->name}{/if} data-price="{$variant->price|convert:NULL:false}" data-format-price="{$variant->price|convert:NULL}" data-sku="{$variant->sku}" {if $variant->stock == 0}class="out-of-stock"{elseif $variant->stock < 0}class="to-order"{/if}>{$variant->name|escape}{if $variant->stock == 0} (нет в наличии){elseif $variant->stock < 0} (под заказ){/if}</option>
            {/foreach}
            </select>
            {if !$edit_order_mode}<label class="variant-name">{$variant_name}</label>{/if}
        {else}
            {assign var="sku_value" value=$purchase->variants.0->sku}
            {assign var="variant_name" value=$purchase->variants.0->name}
            <input type="hidden" name="purchases[variant_id][{$purchase->id}]" value="{$purchase->variants.0->id}"/><label>{$purchase->variants.0->name|escape}</label>
        {/if}
        </div>
        <div class="order-sky">

        <input type="hidden" name="purchases[variant_name][{$purchase->id}]" value="{$variant_name}"/>
        <input type="hidden" name="purchases[sku][{$purchase->id}]" value="{$sku_value}"/>
        <input type="hidden" name="purchases[var_amount][{$purchase->id}]" value="{$purchase->var_amount}"/>
        {if $sku_value}<div class="artikul">Артикул: <span class="sku_value">{$sku_value}</span></div>{/if}
    </div>

    {if $purchase->variant->stock < 0}
    <div class="cart-pod-zakaz">
        <span><i class="fa fa-truck"></i> Под заказ</span>
    </div>
    {/if}

    {if $purchase->var_amount > 0}
        <div class="cart-modifier">
            <ul>
                <li><div>
                    {$settings->catalog_variable_amount_name} {$purchase->var_amount|string_format:"%.1f"} {$settings->catalog_variable_amount_dimension}
                </div></li>
            </ul>
        </div>
    {/if}

    {if $purchase->modificators}
        <div class="cart-modifier">
            <ul>
                {foreach $purchase->modificators as $m}
                    <li data-type="{$m->modificator_type}" data-value="{$m->modificator_value}" data-amount="{$m->modificator_amount}" data-multi-apply="{$m->modificator_multi_apply}" data-multi-buy="{$m->modificator_multi_buy}"><div>{$m->modificator_name} {if $m->modificator_type == 'plus_fix_sum'}(+{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'minus_fix_sum'}(-{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'plus_percent'}(+{$m->modificator_value|convert}%){elseif $m->modificator_type == 'minus_percent'}(-{$m->modificator_value|convert}%){/if} {if $m->modificator_amount>1}{$m->modificator_amount} шт.{/if}</div></li>
                {/foreach}
            </ul>
        </div>
    {/if}

    {if $purchase->show_modificators || ($settings->catalog_use_variable_amount && $purchase->product->use_variable_amount)}
    <button type="button" class="btn btn-default btn-sm btn-edit-modificators" {if !$edit_order_mode}style="display: none;"{/if}>Редактировать модификаторы</button>
    {/if}
</div>


</li>
{/foreach}

{if $modificators}
    <legend>Дополнения:</legend>
    <div class="cart-modifier">
        <ul>
            {foreach $modificators as $m}
                <li><div>{$m->modificator_name} {if $m->modificator_type == 'plus_fix_sum'}(+{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'minus_fix_sum'}(-{$m->modificator_value|convert} {$main_currency->sign}){elseif $m->modificator_type == 'plus_percent'}(+{$m->modificator_value|convert}%){elseif $m->modificator_type == 'minus_percent'}(-{$m->modificator_value|convert}%){/if} {if $m->modificator_amount>1}{$m->modificator_amount} шт.{/if}</div></li>
            {/foreach}
        </ul>
    </div>
    <button type="button" class="btn btn-default btn-sm btn-edit-modificators-orders" {if !$edit_order_mode}style="display: none;"{/if}>Редактировать дополнения</button>
{/if}