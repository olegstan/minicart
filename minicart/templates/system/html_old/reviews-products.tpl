{* Title *}
{$meta_title='Отзывы к товарам' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}                
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>               
            </div>
            
            </div>

            {if $allow_edit_module && $categories_count>0}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

<div class="row {if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
    <div class="col-md-8">
        <legend>Отзывы к товарам</legend>
        
        <div id="refreshpart" class="dd">
        {include file='reviews-products-refreshpart.tpl'}
        </div>
        
        <div class="form-inline massoperations">
                <div class="checkall">Выделить все &uarr;</div>
                <select id="mass_operation" class="form-control">
                    <option value='1'>Одобрить</option>
                    <option value='2'>Скрыть</option>
                    <option value='3'>Удалить</option>
                </select>
                <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
            </div>
        
    </div>
    
    <div class="col-md-4">
            <div id="tree">
                <ol class="dd-list-small">
                    <li class="dd-item-small dd3-item-small dd-collapsed" data-id="0">
                        <div class="dd3-content-small">
                            <span>{$all_products_count}</span>
                            <a href="#" {if !$category_id}class="active"{/if}>Все категории</a>
                        </div>
                    </li>
                    {function name=categories_tree level=0}
                        {foreach $items as $item}
                            <li class="dd-item-small dd3-item-small {if !$item.subcategories}dd-collapsed{/if}" data-id="{$item.id}">
                            {if $item.folder}
                                <i class="fa fa-minus" style="{if $item.subcategories}display: block;{else}display: none;{/if}"></i>
                                <i class="fa fa-plus" style="{if $item.subcategories}display: none;{else}display: block;{/if}"></i>
                                <i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
                            {/if}
                                <div class="dd3-content-small">
                                    <span>{$item.products_count}</span>
                                    <a href="#" class="{if $item.id == $category_id}active{/if}">{$item.title}</a>
                                </div>
                                {if $item.subcategories}
                                    <ol class="dd-list-small" style="display: block;">
                                    {categories_tree items=$item.subcategories level=$level+1}
                                    </ol>
                                {/if}
                            </li>
                        {/foreach}
                    {/function}
                    {categories_tree items=$categories_admin}
                </ol>
            </div>
    </div>

</div>

</div>

<script type="text/javascript">
    $('#refreshpart').on('click', 'a.review-delete', function(e){
        confirm_popover_box($(this), function(obj){
            delete_review_item(obj);
        });
        return false;
    });

    function delete_review_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
            {
                //update products list
                /*var url = $('ol.dd-list').attr('data-href');
                if (url == undefined)
                    return false;
                var params = [{
                        'key': 'url',
                        'value': url
                    }];
                reviews_request_ajax(params, false);*/
                location.reload();
            }
            return false;
        });
        return false;
    }

    $('#refreshpart').on('click', 'a.review-moderate, a.review-ok', function(e){
        var review_buttons = $(this).closest('.review-buttons');
        var href = $(this).attr('href');
        $.get(href, function(data) {
            if (data.success)
            {
                if (data.visible){
                    review_buttons.find('a.review-ok').addClass('hidden');
                    review_buttons.find('a.review-moderate').removeClass('hidden');
                }
                else{
                    review_buttons.find('a.review-ok').removeClass('hidden');
                    review_buttons.find('a.review-moderate').addClass('hidden');
                }
            }
            return false;
        });
        return false;
    });

    $('#refreshpart').on('click', '.review-onoff label.on, .review-onoff label.off', function(){
        var li = $(this).closest('li');
        if (!$(this).hasClass('active')){
            var href = $(this).data('href');
            $.get(href, function(data) {
                li.removeClass('notmoderated');
            });
        }
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#refreshpart input[type=checkbox]").each(function(){
            $(this).prop('checked', true);
        });
    });

    function finish_mass_operation(){
        $('#mass_operation :first').attr('selected', 'selected');
        $('#mass_operation').trigger('change');
        return false;
    }

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#refreshpart input[type=checkbox]:checked").length;

        if (items_count == 0)
            return false;

        var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
        l.start();
        l.setProgress( 0 );
        var interval = 1 / items_count;
        var ok_count = 0;

        // 1 - show
        // 2 - hide
        // 3 - delete

        if (operation == 1){
            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('div.review-onoff label.on').first();
                var href = subitem.data('href');
                if (!subitem.hasClass('active'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            item.find('div.review-onoff label.off').removeClass('active');
                            item.find('div.review-onoff label.off input').prop('checked', false);
                            subitem.addClass('active');
                            subitem.find('input').prop('checked', true);
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 2){
            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('div.review-onoff label.off').first();
                var href = subitem.data('href');
                if (!subitem.hasClass('active'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            item.find('div.review-onoff label.on').removeClass('active');
                            item.find('div.review-onoff label.on input').prop('checked', false);
                            subitem.addClass('active');
                            subitem.find('input').prop('checked', true);
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });
                if (operation == 6){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.list-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('list-item-on'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('list-item-off');
                            subitem.addClass('list-item-on');
                            subitem.prop('title', 'Свойство отображается в списке товаров');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.list-item').first().removeClass('list-item-off').removeClass('list-item-on').addClass('list-item-on');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 7){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.list-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('list-item-off'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('list-item-on');
                            subitem.addClass('list-item-off');
                            subitem.prop('title', 'Свойство не отображается в товаре');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.list-item').first().removeClass('list-item-off').removeClass('list-item-on').addClass('list-item-off');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 3){
            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }

        return false;
    });
</script>