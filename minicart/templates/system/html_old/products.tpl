{* Title *}
{$meta_title='Товары' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags_values">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <input type="hidden" name="menu" value=""/>
        <input type="hidden" name="stocks" value=""/>
        <input type="hidden" name="prices" value=""/>

        <div class="controlgroup form-inline">
            <div class="currentfilter products">
            <div class="row">
            <div class="col-md-8">
                <div class="input-group">
                    <input id="searchField" type="text"  class="form-control" placeholder="Поиск">
                    <span class="input-group-btn">
                    <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                    <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                    </span>
                </div>

            </div>
            <div class="col-md-4">
                <div class="checkbox">
                    <label>
                    <input type="checkbox" id="search_in_current_category"> Искать в текущей категории
                </label>
                   </div>
            </div>
            </div>
            
            
            </div>
            
            {if $allow_edit_module && $categories_count>0}<a href="{$config->root_url}{$edit_module->url}{url add=['category_id'=>$category_id]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="boxes {if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">

            <div class="box-left-8">
                <div class="box-left-inside">
             
                <div class="box-main-header">
                Товары <i class="fa fa-spinner fa fa-spin fa fa-large" id="products-spinner" style="display:none;"></i>
                
                <div class=" expandvariants">
                    <div class="btn-group">
                        <button type="button" data-action="expand-all" class="btn btn-xs btn-link" id="expand-all-variants"><i class="fa fa-plus"></i> <span>Развернуть варианты</span></button>
                        <button type="button" data-action="collapse-all" class="btn btn-xs btn-link" id="collapse-all-variants"><i class="fa fa-minus"></i> <span>Свернуть варианты</span></button>
                    </div>
                    
                </div>
                
                </div>
                
                
            
              
              
              
                <div id="refreshpart" class="dd">{include file='products-refreshpart.tpl'}</div>
                {$allow_edit_module = false}
                {if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}
                {if $allow_edit_module}
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все ↑</div>
                    <select id="mass_operation" class="form-control">
                        <option value="0">Выберите действие</option>
                        <optgroup label="Цена и валюта">
                            <option value="15">Изменить цену</option>
                            <option value="12">Добавить/Убрать скидку</option>
                            {if $settings->catalog_show_currency}<option value="16">Изменить валюту</option>{/if}
                        </optgroup>
                        <optgroup label="Свойства товара">
                            <option value="8">Добавить значения свойств</option>
                            <option value="9">Удалить значения свойств</option>
                        </optgroup>
                        <optgroup label="Бренды">
                            <option value="6">Указать бренд</option>
                            <option value="7">Удалить бренд</option>
                        </optgroup>
                        <optgroup label="Категории">
                            <option value="4">Переместить в категорию</option>
                            <option value="5">Указать дополнительную категорию</option>
                        </optgroup>
                        {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled || $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
                        <optgroup label="Доп. поля и флаги">
                            {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled}
                            <option value="17">Установить значение доп. флагов</option>
                            {/if}
                            {if $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
                            <option value="18">Установить значение доп. полей</option>
                            {/if}
                        </optgroup>
                        {/if}
                        <optgroup label="Основные">
                            <option value="10">Установить количество</option>
                            <option value="1">Сделать видимыми</option>
                            <option value="2">Скрыть</option>
                            <option value="11">Удалить</option>
                        </optgroup>
                        {*<option value="3">Переместить на страницу</option>*}
                    </select>
                    <select id="mass_operation_set_brand" class="form-control" style="display:none;">
                        {foreach $all_brands as $brand}
                            <option value="{$brand->id}">{$brand->name}</option>
                        {/foreach}
                    </select>
                    <select id="mass_operation_add_tag_group" class="form-control" style="display:none;">
                        {*foreach $tags_groups as $group}
                            {if $group->is_auto == 0}
                            <option value="{$group->id}">{$group->name}</option>
                            {/if}
                        {/foreach*}
                    </select>
                    <div id="mass_operation_add_tag_value" style="display:none;">
                        <input type="hidden" name="add_tags" value="">
                    </div>
                    <select id="mass_operation_remove_tag_group" class="form-control" style="display:none;">
                        {foreach $tags_groups as $group}
                            {if $group->is_auto == 0}
                            <option value="{$group->id}">{$group->name}</option>
                            {/if}
                        {/foreach}
                    </select>
                    <div id="mass_operation_remove_tag_value" style="display:none;">
                        <input type="hidden" name="remove_tags" value="">
                    </div>
                    <select id="mass_operation_move_to_page" class="form-control" style="display:none;">
                        {section name=pages start=1 loop=$total_pages_num+1 step=1}
                        <option>{$smarty.section.pages.index}</option>
                        {/section}
                    </select>
                    <select id="mass_operation_move_to_category" class="form-control" style="display:none;">

                    </select>
                    <select id="mass_operation_add_category" class="form-control" style="display:none;">

                    </select>
                    <div id="mass_operation_set_stock" style="display:none;">
                        <input type="text" name="stock" class="form-control" value="∞">
                    </div>
                    {*<div id="mass_operation_set_discount" style="display:none;">
                        <input type="text" name="discount" class="form-control" value="0">
                    </div>
                    <div id="mass_operation_set_fix_discount" style="display:none;">
                        <input type="text" name="fix_discount" class="form-control" value="0">
                    </div>*}
                    <select id="mass_operation_set_discount" class="form-control" style="display:none;">
                        <option value="set_discount">Сделать скидку в %</option>
                        <option value="set_fix_discount">Сделать фиксированную скидку</option>
                        <option value="remove_discount">Убрать скидку</option>
                    </select>
                    <input id="mass_operation_set_discount_input" type="text" class="form-control" value="" style="display:none;"/>

                    <select id="mass_operation_change_price" class="form-control" style="display:none;">
                        <option value="set_price">Установить цену</option>
                        <option value="inc_fix_price">Увеличить на фиксированную сумму</option>
                        <option value="sub_fix_price">Уменьшить на фиксированную сумму</option>
                        <option value="inc_percent">Увеличить на %</option>
                        <option value="sub_percent">Уменьшить на %</option>
                    </select>
                    <input id="mass_operation_change_price_input" type="text" class="form-control" value="" style="display:none;"/>
                    <select id="mass_operation_set_currency" class="form-control" style="display:none;">
                        {foreach $currencies as $currency}
                            <option value="{$currency->id}">{$currency->name}</option>
                        {/foreach}
                    </select>

                    <select id="mass_operation_select_add_flag" class="form-control" style="display:none;">
                        {if $settings->add_flag1_enabled}<option value="add_flag1">{$settings->add_flag1_name}</option>{/if}
                        {if $settings->add_flag2_enabled}<option value="add_flag2">{$settings->add_flag2_name}</option>{/if}
                        {if $settings->add_flag3_enabled}<option value="add_flag3">{$settings->add_flag3_name}</option>{/if}
                    </select>
                    <div class="btn-group noinline" data-toggle="buttons" id="mass_operation_select_add_flag_input" style="display:none;">
                        <label class="btn btn-default4 on active">
                            <input type="radio" name="mass_operation_select_add_flag_input" value="1" checked="">Да
                        </label>
                        <label class="btn btn-default4 off ">
                            <input type="radio" name="mass_operation_select_add_flag_input" value="0">Нет
                        </label>
                    </div>

                    <select id="mass_operation_select_add_field" class="form-control" style="display:none;">
                        {if $settings->add_field1_enabled}<option value="add_field1">{$settings->add_field1_name}</option>{/if}
                        {if $settings->add_field2_enabled}<option value="add_field2">{$settings->add_field2_name}</option>{/if}
                        {if $settings->add_field3_enabled}<option value="add_field3">{$settings->add_field3_name}</option>{/if}
                    </select>
                    <input id="mass_operation_select_add_field_input" type="text" class="form-control" value="" style="display:none;"/>

                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
                {/if}
            </div>
        </div>
            
        <div class="box-right-4">
            <div id="tree">
                <ol class="dd-list-small">
                    <li class="dd-item-small dd3-item-small dd-collapsed" data-id="0">
                        <div class="dd3-content-small">
                            <span>{$all_products_count}</span>
                            <a href="#" {if !$category_id}class="active"{/if}>Все категории</a>
                        </div>
                    </li>
                    {function name=categories_tree level=0}
                        {foreach $items as $item}
                            <li class="dd-item-small dd3-item-small {if !$item.subcategories}dd-collapsed{/if}" data-id="{$item.id}">
                            {if $item.folder}
                                <i class="fa fa-minus" style="{if $item.subcategories}display: block;{else}display: none;{/if}"></i>
                                <i class="fa fa-plus" style="{if $item.subcategories}display: none;{else}display: block;{/if}"></i>
                                <i class="fa fa-spin fa fa-spinner" style="display: none;"></i>
                            {/if}
                                <div class="dd3-content-small">
                                    <span>{$item.products_count}</span>
                                    <a href="#" class="{if $item.id == $category_id}active{/if}">{$item.title}</a>
                                </div>
                                {if $item.subcategories}
                                    <ol class="dd-list-small" style="display: block;">
                                    {categories_tree items=$item.subcategories level=$level+1}
                                    </ol>
                                {/if}
                            </li>
                        {/foreach}
                    {/function}
                    {categories_tree items=$categories_admin}
                </ol>
            </div>
        
            <div class="panel-brands">
              <div class="box-header">
                  <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle" id="brands_toggle">
                    {if $settings->brands_auto_open}<i class="fa fa-minus"></i>{else}<i class="fa fa-plus"></i>{/if} Бренды
                    
                    </a>
                  
              </div>
              <div class="panel-collapse collapse {if $settings->brands_auto_open}in{/if}" id="collapseOne" style="height: auto;">
                <div id="brands">
                {foreach $brands as $brand}
                    <div class="checkbox">
                        <label><input type="checkbox" value="{$brand->id}" {if $brand_ids && in_array($brand->id, $brand_ids)}checked{/if}> {$brand->name}</label>
                    </div>
                {/foreach}
                </div>
              </div>
            </div>

            {* временно скроем эту часть *}
            {* 
            <div class="panel panel-default panel-filter">
        <div class="panel-heading">
            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
              Фильтр
            </a>
        </div>
        <div id="collapseTwo" class="panel-collapse collapse">
          <div class="panel-body">
          </div>
        </div>
      </div>
              *}

        </div>
        </div>
       

    </form>
</div>

<script type="text/javascript">
    // element - куда выводить, parent_id - какую категорию вывести
    function print_lazy_result(element, parent_id, icon_col, icon_spin, path, select_category_id){
        if (element == undefined)
        {
            if (icon_spin != undefined)
                icon_spin.hide();
            if (icon_col != undefined)
                icon_col.show();
            return false;
        }

        if (parent_id == undefined)
            parent_id = 0;
        var result = [];
        var href = '{$config->root_url}{$categories_module->url}?lazy_load=1&parent_id='+parent_id;

        $.get(href, function(data){
            if (data == undefined || data.length == 0)
                return false;
            var ol = $('<ol class="dd-list-small"></ol>');

            if (parent_id == 0)
            {
                var li = $('<li class="dd-item-small dd3-item-small dd-collapsed" data-id="0"></li>');
                li.append('<div class="dd3-content-small"><span>{$all_products_count}</span><a href="#">Все категории</a></div>');
                ol.append(li);
            }

            for(var i=0 ; i<data.length ; i++){
                var li = $('<li class="dd-item-small dd3-item-small dd-collapsed" data-id="'+data[i].id+'"></li>');
                if (data[i].folder){
                    li.append('<i class="fa fa-minus" style="display: none;"></i>');
                    li.append('<i class="fa fa-plus" style="display: block;"></i>');
                    li.append('<i class="fa fa-spin fa fa-spinner" style="display: none;"></i>');
                }
                li.append('<div class="dd3-content-small"><span>'+data[i].products_count+'</span><a href="#">'+data[i].title+'</a></div>');
                ol.append(li);
            }
            element.append(ol);
            if (icon_spin != undefined)
                icon_spin.hide();
            if (icon_col != undefined)
                icon_col.show();

            if (path != undefined && path.length > 0)
            {
                id = path[0];
                var _li = $('#tree li[data-id='+id+']');
                var _icon_col = _li.find('i.fa-minus:first');
                var _icon_exp = _li.find('i.fa-plus:first');
                var _icon_spin = _li.find('i.fa-spinner:first');

                _icon_exp.hide();
                _icon_spin.show();
                _li.removeClass('dd-collapsed');

                path.shift();
                print_lazy_result(_li, id, _icon_col, _icon_spin, path, select_category_id!=undefined?select_category_id:undefined);
            }
            else
                if (select_category_id != undefined)
                    $('#tree li[data-id='+select_category_id+'] a:first').addClass('active');
        });
    }

    function update_products_count(item){
        var ids = [];
        ids.push(item.attr('data-id'));
        item.parents('li.dd-item-small').each(function(){
            ids.push($(this).attr('data-id'));
        });
        var href = '{$config->root_url}{$categories_module->url}?lazy_update=1&ids='+ids.join(',');
        $.get(href, function(data){
            for(var i in data)
            {
                var li = $('#tree li[data-id='+data[i].id+']');
                if (li.length>0)
                    li.find('div.dd3-content-small span:first').html(data[i].value);
            }
        });
    }

    var search_ajax_context;

    function products_request_ajax(params, jump_to_body){
        $('#products-spinner').show();
        var url = "{$config->root_url}{$module->url}";
        {*if (params != undefined)
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    break;
                }
                if (index > 0)
                    url += "&";
                url += params[index].key + "=" + params[index].value;
            }
        var full_url = url;
        if (params != undefined && params.length > 0)
            full_url += "&";
        full_url += "ajax=1";*}

        if (params != undefined && params.length>0)
        {
            url += "?";
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    if (params.length > 1)
                        url += "?";
                    break;
                }
            }

            var index2 = 0;
            for(var index in params)
            {
                if (params[index].key != "url")
                {
                    if (index2 > 0)
                        url += "&";
                    url += params[index].key + "=" + params[index].value;
                    index2 += 1;
                }
            }
        }
        var full_url = url;
        if (params == undefined || params.length == 0 || (params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "?";
        if (params != undefined && params.length > 0 && !(params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "&";
        full_url += "ajax=1";

        if (jump_to_body != undefined && jump_to_body)
            $("body,html").animate({
                scrollTop:0
            }, 100);

        if (search_ajax_context != null)
            search_ajax_context.abort();

        search_ajax_context = $.ajax({
            type: 'GET',
            url: full_url,
            success: function(data) {
                if (typeof history.pushState != 'undefined') {
                    history.pushState(null,null,encodeURI(decodeURI(url)));
                }
                $('#refreshpart').html(data.products);
                $('#refreshpart').nestable({
                    maxDepth: 1
                });
                $('#brands').html('');
                for(var index in data.brands)
                {
                    var brand = data.brands[index];
                    $('#brands').append('<div class="checkbox">'+
                        '<label><input type="checkbox" value="'+brand.id+'" '+(brand.selected?'checked':'')+'/> '+brand.name+'</label>'+
                        '</div>');
                }

                $('#products-spinner').hide();
                $('a[data-toggle="tooltip"]').tooltip();
                $('a[data-toggle="tooltip"]').click(function(){
                    if (!$(this).hasClass('allow-jump'))
                        return false;
                });

                search_ajax_context = null;
            }
        });
        return false;
    }


    $('#refreshpart').on('click', 'a.showvariants', function(){
        $(this).find('span.uarr').toggle();
        $(this).find('span.darr').toggle();
        var variants_id = $(this).attr('href');
        $(variants_id).slideToggle('fast');
        return false;
    });

    $('#expand-all-variants').click(function(){
        $('#refreshpart div[id^=collapseVariants]').fadeIn('fast');
        $('#refreshpart span.darr').hide();
        $('#refreshpart span.uarr').show();
        return false;
    });

    $('#collapse-all-variants').click(function(){
        $('#refreshpart div[id^=collapseVariants]').fadeOut('fast');
        $('#refreshpart span.darr').show();
        $('#refreshpart span.uarr').hide();
        return false;
    });

    // развернуть категорию
    $('#tree').on('click', 'i.fa-plus', function(){
        var li = $(this).closest('li');

        //Свернем другие категории
        //$('#tree > ol > li:not(.dd-collapsed)').each(function(){
        $('#tree li:not(.dd-collapsed)').each(function(){
            var li_find = $(this).find('li[data-id='+li.attr('data-id')+']');
            if (li_find.length == 0)
            {
                var icon_minus = $(this).find('i.fa-minus');
                if (icon_minus.length > 0)
                    icon_minus.trigger('click');
            }
        });

        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');
        var icon_spin = li.find('i.fa-spinner:first');

        icon_exp.hide();

        //Подгрузим на ajax'e дерево
        if (ol.length == 0)
        {
            icon_spin.show();
            print_lazy_result(li, li.attr('data-id'), icon_col, icon_spin);
        }
        else
        {
            ol.show();
            icon_col.show();
        }

        li.removeClass('dd-collapsed');
    });

    // свернуть категорию
    $('#tree').on('click', 'i.fa-minus', function(){
        var li = $(this).closest('li');
        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');

        ol.hide();
        li.addClass('dd-collapsed');
        icon_col.hide();
        icon_exp.show();
    });

    // выбор категории
    $('#tree').on('click', 'a', function(){
        var li = $(this).closest('li');
        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');
        var icon_spin = li.find('i.fa-spinner:first');
        var id = li.attr('data-id');
        if (id == undefined)
            return false;
        id = parseInt(id);
        var params = [];
        $('#brands input:checked').each(function(){
            $(this).prop('checked', false);
            //selected_brands.push($(this).val());
        });
        if (id > 0)
        {
            params.push({
                'key': 'category_id',
                'value': id
            });
            $('a.add').attr('href', '{$config->root_url}{$edit_module->url}?category_id='+id);
        }
        else
        {
            $('a.add').attr('href', '{$config->root_url}{$edit_module->url}');

            //Свернем другие категории
            $('#tree li:not(.dd-collapsed)').each(function(){
                var icon_minus = $(this).find('i.fa-minus');
                icon_minus.trigger('click');
            });
        }

        //Свернем другие категории
        //$('#tree > ol > li:not(.dd-collapsed)').each(function(){
        $('#tree li:not(.dd-collapsed)').each(function(){
            var item = $(this).find('li[data-id='+id+']');
            if (item.length == 0)
            {
                var icon_minus = $(this).find('i.fa-minus');
                if (icon_minus.length > 0)
                    icon_minus.trigger('click');
            }
        });

        products_request_ajax(params, true);
        if (icon_col.length>0 && icon_exp.length>0 && icon_spin.length>0)
        {
            icon_exp.hide();

            //Подгрузим на ajax'e дерево, если оно еще не загружено
            if (ol.length == 0)
            {
                icon_spin.show();
                print_lazy_result(li, li.attr('data-id'), icon_col, icon_spin);
            }
            else
            {
                ol.show();
                icon_col.show();
            }
        }

        li.removeClass('dd-collapsed');
        $('#tree li a.active').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        return false;
    });

    $('#brands').on('click', 'input', function(){
        var params = [];
        var selected_brands = [];
        $('#brands input:checked').each(function(){
            selected_brands.push($(this).val());
        });
        if (selected_brands.length > 0)
            params.push({
                'key': 'brand_id',
                'value': selected_brands
            });
        var active_category = $('#tree a.active').closest('li');
        var id = parseInt(active_category.attr('data-id'));
        if (id > 0)
        {
            params.push({
                'key': 'category_id',
                'value': id
            });
        }
        products_request_ajax(params, true);
    });

    // сортировка
    $('#refreshpart').on('click', 'div.sort a', function(){
        var url = $(this).attr('href');
        if (url == undefined)
            return false;
        var params = [{
                'key': 'url',
                'value': url
            }];
        products_request_ajax(params, true);
        return false;
    });

    $('#searchButton').click(function(){
        $('#searchField').triggerHandler('keyup');
    });

    $('#searchField').on('keydown', function(e){
        if (e.keyCode == 13)
            return false;
    });

    // поиск keyword
    $('#searchField').on('keyup', function(e){
        var params = [];

        if (e.which == 13)
            return false;

        if ($('#search_in_current_category').is(':checked'))
            params.push({
                'key': 'category_id',
                'value': $('#tree a.active').closest('li').attr('data-id')
            });

        if ($(this).val().length > 0)
        {
            $('#searchCancelButton').show();
            params.push({
                'key': 'keyword',
                'value': encodeURIComponent($(this).val())
            });
        }
        else
        {
            $('#searchCancelButton').hide();
            if (!$('#search_in_current_category').is(':checked'))
            {
                $('#tree li a.active').each(function(){
                    $(this).removeClass('active');
                });
                $('#tree li[data-id=0] a').addClass('active');
            }
        }
        if ($(this).val().length >= {$settings->search_min_lenght} || $(this).val().length == 0)
            products_request_ajax(params, true);
        return false;
    });

    // сброс keyword
    $('#searchCancelButton').click(function(){
        var params = [];
        if ($('#search_in_current_category').is(':checked'))
            params.push({
                'key': 'category_id',
                'value': $('#tree a.active').closest('li').attr('data-id')
            });
        else
        {
            /*$('#tree li a.active').each(function(){
                $(this).removeClass('active');
            });
            $('#tree li[data-id=0] a').addClass('active');*/
            if ($('#tree a.active').closest('li').attr('data-id') > 0)
                params.push({
                    'key': 'category_id',
                    'value': $('#tree a.active').closest('li').attr('data-id')
                });
        }
        $('#searchField').val('');
        products_request_ajax(params, true);
        $('#searchCancelButton').hide();
        return false;
    });

    $('#refreshpart').on('click', 'ul.pagination a', function(){
        var url = $(this).attr('href');
        if (url == undefined)
            return false;
        var params = [{
                'key': 'url',
                'value': url
            }];
        products_request_ajax(params, true);
        return false;
    });

    $('#search_in_current_category').click(function(){
        $('#searchField').trigger('keyup');
    });

    $(document).ready(function(){
        {if $products}
            $('#refreshpart').nestable({
                maxDepth: 1
            });
        {/if}

        {*if $category_path}
        var path = "{$category_path}";
        path = path.split(',');
        print_lazy_result($('#tree'),0,undefined,undefined, path, {$category_id});
        {else}
            {if $category_id}
                print_lazy_result($('#tree'),0,undefined,undefined, [], {$category_id});
            {else}
                print_lazy_result($('#tree'),0,undefined,undefined,[],0);
            {/if}
        {/if*}

        {if $categories_count == 0}
            error_box('#notice', 'Категории отсутствуют! ', 'Для создания товаров нужно чтобы была создана хотя бы одна категория!');
        {/if}
    });

    $('a.save').click(function(){

        {if !$products}
        return false;
        {/if}

        var obj = $("#notice");
        var menu = $('#refreshpart').nestable('serialize');
        var stocks = [];
        var prices = [];

        $('#refreshpart').find('input[name*=variants_stock]').each(function(){
            stocks.push({
                'id' : $(this).attr('data-variant-id'),
                'value' : $(this).val()
            });
        });
        $('#refreshpart').find('input[name*=variants_price]').each(function(){
            prices.push({
                'id' : $(this).attr('data-variant-id'),
                'value' : $(this).val()
            });
        });

        $(this).closest('form').find('input[name=menu]').val(JSON.stringify(menu));
        $(this).closest('form').find('input[name=stocks]').val(JSON.stringify(stocks));
        $(this).closest('form').find('input[name=prices]').val(JSON.stringify(prices));
        $(this).closest('form').submit();

        {*$.ajax({
            type: 'POST',
            url: $(this).closest('form').attr('action'),
            data: {
                menu: menu,
                stocks: stocks,
                prices: prices},
            error: function() {
                error_box(obj, 'Изменения не сохранены!', '');
            },
            success: function(data) {
                info_box(obj, 'Изменения сохранены!', '');
            }
        });*}
        return false;
    });

    $('#refreshpart').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_product_item(obj);
        });
        return false;
    });

    function delete_product_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
            {
                //update right panel with products_count
                var tree_node = $('#tree li a.active').closest('li');
                if (tree_node.length > 0)
                    update_products_count(tree_node);

                //update products list
                var url = $('ol.dd-list').attr('data-href');
                if (url == undefined)
                    return false;
                var params = [{
                        'key': 'url',
                        'value': url
                    }];
                products_request_ajax(params, false);
                return false;
            }
            return false;
        });
        return false;
    }

    $('#refreshpart').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
            return false;
        });
        return false;
    });

    $('#refreshpart').on('click', 'a.toggle-flag1-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('flag1-on')?1:0;
        var title = item.attr('data-title');
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('flag1-on');
                item.removeClass('flag1-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('flag1-on');
                    title += ': Да';
                }
                else{
                    item.addClass('flag1-off');
                    title += ': Нет';
                }
                item.attr('title', title);
            }
            return false;
        });
        return false;
    });

    $('#refreshpart').on('click', 'a.toggle-flag2-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('flag2-on')?1:0;
        var title = item.attr('data-title');
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('flag2-on');
                item.removeClass('flag2-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('flag2-on');
                    title += ': Да';
                }
                else{
                    item.addClass('flag2-off');
                    title += ': Нет';
                }
                item.attr('title', title);
            }
            return false;
        });
        return false;
    });

    $('#refreshpart').on('click', 'a.toggle-flag3-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('flag3-on')?1:0;
        var title = item.attr('data-title');
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('flag3-on');
                item.removeClass('flag3-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('flag3-on');
                    title += ': Да';
                }
                else{
                    item.addClass('flag3-off');
                    title += ': Нет';
                }
                item.attr('title', title);
            }
            return false;
        });
        return false;
    });

    // infinity
    $("input[name^=variants_stock], #mass_operation_set_stock input").focus(function() {
        if($(this).val() == '∞')
            $(this).val('');
        return false;
    });

    $("input[name^=variants_stock], #mass_operation_set_stock input").blur(function() {
        if($(this).val() == '')
            $(this).val('∞');
    });

    $('div.checkall').click(function(){
        $("#refreshpart input[type=checkbox]").each(function(){
            $(this).prop('checked', true);
        });
    });

    function load_group_tags(group_id, action_name){
        $('div#mass_operation_'+action_name+'_tag_value input[name='+action_name+'_tags]').val('');
        $('div#mass_operation_'+action_name+'_tag_value input[name='+action_name+'_tags]').select2({
            tags: true,
            separator: '#%#',
            tokenSeparators: ["#%#"],
            createSearchChoice: function(term, data) {
                if ($(data).filter(function() {
                    return this.text.localeCompare(term) === 0;
                }).length === 0) {
                    return {
                        id: term,
                        text: term
                    };
                }
            },
            multiple: true,
            ajax: {
                {*url: "{$config->root_url}{$edit_module->url}?mode=get_tags&group_id="+group_id,*}
                url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id="+group_id,
                dataType: "json",
                data: function(term, page) {
                  return {
                    keyword: term
                  };
                },
                results: function(data, page) {
                  return {
                    results: data.data
                  };
                }
            }
        });
    }

    $('#mass_operation').change(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        if (operation == 3)
            $('#mass_operation_move_to_page').show();
        else
            $('#mass_operation_move_to_page').hide();
        if (operation == 4){
            $('#products-spinner').show();
            $.ajax({
                type: 'POST',
                url: '{$config->root_url}/ajax/get_data.php',
                data: {
                    'object': 'products',
                    'mode': 'categories_list',
                    'session_id': '{$smarty.session.id}'
                },
                {*type: 'GET',
                url: "{$config->root_url}{$module->url}?categories_list",*}
                success: function(data) {
                    $('#mass_operation_move_to_category').html('');

                    if (data.success)
                        for(var index in data.data)
                        {
                            var category = data.data[index];
                            $('#mass_operation_move_to_category').append("<option value='"+category.id+"'>"+category.level+category.text{*category.name*}+"</option>");
                        }

                    $('#products-spinner').hide();
                    $('#mass_operation_move_to_category').show();
                }
            });
        }
        else
            $('#mass_operation_move_to_category').hide();
        if (operation == 5){
            $('#products-spinner').show();
            $.ajax({
                {*type: 'GET',
                url: "{$config->root_url}{$module->url}?categories_list",*}
                type: 'POST',
                url: '{$config->root_url}/ajax/get_data.php',
                data: {
                    'object': 'products',
                    'mode': 'categories_list',
                    'session_id': '{$smarty.session.id}'
                },
                success: function(data) {
                    $('#mass_operation_add_category').html('');

                    if (data.success)
                        for(var index in data.data)
                        {
                            var category = data.data[index];
                            $('#mass_operation_add_category').append("<option value='"+category.id+"'>"+category.level+category.text{*category.name*}+"</option>");
                        }

                    $('#products-spinner').hide();
                    $('#mass_operation_add_category').show();
                }
            });
        }
        else
            $('#mass_operation_add_category').hide();
        if (operation == 6)
            $('#mass_operation_set_brand').show();
        else
            $('#mass_operation_set_brand').hide();
        if (operation == 8)
        {
            $.ajax({
                type: 'POST',
                url: '{$config->root_url}/ajax/get_data.php',
                data: {
                    'object': 'products',
                    'mode': 'get_tag_groups',
                    'session_id': '{$smarty.session.id}'
                },
                success: function(data){
                    if (data.success)
                    {
                        $("#mass_operation_add_tag_group").html(data.data);
                        $('#mass_operation_add_tag_group').show();
                        $('#mass_operation_add_tag_value').hide();

                        load_group_tags($('#mass_operation_add_tag_group option:selected').val(), 'add');
                    }
                },
                dataType: 'json'
            });
        }
        else
        {
            $('#mass_operation_add_tag_group').hide();
            $('#mass_operation_add_tag_value').hide();
        }
        if (operation == 9)
        {
            $('#mass_operation_remove_tag_group').show();
            $('#mass_operation_remove_tag_value').show();

            load_group_tags($('#mass_operation_remove_tag_group option:selected').val(), 'remove');
        }
        else
        {
            $('#mass_operation_remove_tag_group').hide();
            $('#mass_operation_remove_tag_value').hide();
        }
        if (operation == 10)
            $('#mass_operation_set_stock').show();
        else
            $('#mass_operation_set_stock').hide();
        {*if (operation == 12)
            $('#mass_operation_set_discount').show();
        else
            $('#mass_operation_set_discount').hide();
        if (operation == 13)
            $('#mass_operation_set_fix_discount').show();
        else
            $('#mass_operation_set_fix_discount').hide();*}

        if (operation == 12){
            $('#mass_operation_set_discount option:selected').attr('selected', false);
            $('#mass_operation_set_discount option:first').attr('selected', true);
            $('#mass_operation_set_discount').show();
            $('#mass_operation_set_discount_input').show();
        }
        else{
            $('#mass_operation_set_discount').hide();
            $('#mass_operation_set_discount_input').hide();
        }


        if (operation == 15){
            $('#mass_operation_change_price').show();
            $('#mass_operation_change_price_input').show();
        }
        else{
            $('#mass_operation_change_price').hide();
            $('#mass_operation_change_price_input').hide();
        }

        if (operation == 16)
            $('#mass_operation_set_currency').show();
        else
            $('#mass_operation_set_currency').hide();

        if (operation == 17){
            $('#mass_operation_select_add_flag').show();
            $('#mass_operation_select_add_flag_input').show();
        }
        else{
            $('#mass_operation_select_add_flag').hide();
            $('#mass_operation_select_add_flag_input').hide();
        }

        if (operation == 18){
            $('#mass_operation_select_add_field').show();
            $('#mass_operation_select_add_field_input').show();
        }
        else{
            $('#mass_operation_select_add_field').hide();
            $('#mass_operation_select_add_field_input').hide();
        }

        return false;
    });

    $('#mass_operation_add_tag_group').change(function(){
        if ($(this).val() == 0)
            $('#mass_operation_add_tag_value').hide();
        else
            $('#mass_operation_add_tag_value').show();
        load_group_tags($(this).val(), 'add');
    });

    $('#mass_operation_remove_tag_group').change(function(){
        load_group_tags($(this).val(), 'remove');
    });

    $('#mass_operation_set_discount').change(function(){
        var v = $(this).find('option:selected').val();
        if (v == "remove_discount")
            $('#mass_operation_set_discount_input').hide();
        else
            $('#mass_operation_set_discount_input').show();
    });

    function finish_mass_operation(){
        $('#mass_operation :first').attr('selected', 'selected');
        $('#mass_operation').trigger('change');
        return false;
    }

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#refreshpart input[type=checkbox]:checked").length;

        if (operation == 11 && items_count > 0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            if ($('ol.dd-list').length > 0)
                            {
                                var tree_node = $('#tree li a.active').closest('li');
                                if (tree_node.length > 0)
                                    update_products_count(tree_node);

                                var url = $('ol.dd-list').attr('data-href');
                                if (url == undefined)
                                    return false;
                                var params = [{
                                    'key': 'url',
                                    'value': url
                                }];
                                products_request_ajax(params, false);
                            }
                            else
                                location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-on');
                                });
                                item.find('input[type=checkbox]').prop('checked', false);
                                items_count--;
                                if (items_count == 0){
                                    info_box($('#notice'), 'Выполнено!', '');
                                    finish_mass_operation();
                                }
                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.first().hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-off');
                                });
                                item.find('input[type=checkbox]').prop('checked', false);
                                items_count--;
                                if (items_count == 0){
                                    info_box($('#notice'), 'Выполнено!', '');
                                    finish_mass_operation();
                                }
                            }
                        });
                    break;
                case 3: //Переместить на страницу move_page/product_id/category_id/page/index
                    var href = "{$config->root_url}{$edit_module->url}?mode=move_page&id="+item.attr('data-id')+"&category_id="+$('#tree a.active').closest('li').attr('data-id')+"&page="+$('#mass_operation_move_to_page option:selected').val()+"&items_count="+items_count;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                    });
                    break;
                case 4: //Переместить в категорию
                    var href = "{$config->root_url}{$edit_module->url}?mode=move_category&ajax=1&id="+item.attr('data-id')+"&category_id="+$('#mass_operation_move_to_category option:selected').val();
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0)
                            document.location = "{$config->root_url}{$module->url}?category_id="+$('#mass_operation_move_to_category option:selected').val();
                    });
                    break;
                case 5: //Добавить дополнительную категорию
                    var href = "{$config->root_url}{$edit_module->url}?mode=add_category&id="+item.attr('data-id')+"&category_id="+$('#mass_operation_add_category option:selected').val();
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            info_box($('#notice'), 'Добавление дополнительной категории выполнено!', '');
                            finish_mass_operation();
                        }
                    });
                    break;
                case 6: //Указать бренд
                    if ($('#mass_operation_set_brand option:selected').length > 0)
                    {
                        var href = "{$config->root_url}{$module->url}?add_brand="+item.attr('data-id')+","+$('#mass_operation_set_brand option:selected').val();
                        $.get(href, function(data) {
                            item.find('input[type=checkbox]').prop('checked', false);
                            items_count--;
                            if (items_count == 0){
                                info_box($('#notice'), 'Бренд добавлен!', '');
                                finish_mass_operation();
                            }
                        });
                    }
                    break;
                case 7:    //Удалить бренд
                    var href = "{$config->root_url}{$module->url}?remove_brand="+item.attr('data-id');
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            info_box($('#notice'), 'Бренд удален!', '');
                            finish_mass_operation();
                        }
                    });
                    break;
                case 8: //Добавить тег(и)
                    var href = "{$config->root_url}{$edit_module->url}?mode=add_tags&id="+item.attr('data-id')+"&group_id="+$('#mass_operation_add_tag_group option:selected').val()+"&tags_values="+$('div#mass_operation_add_tag_value input[name=add_tags]').select2("val");
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            info_box($('#notice'), 'Значения свойств добавлены!', '');
                            finish_mass_operation();
                        }
                    });
                    break;
                case 9: //Удалить тег(и)
                    var href = "{$config->root_url}{$edit_module->url}?mode=remove_tags&id="+item.attr('data-id')+"&group_id="+$('#mass_operation_remove_tag_group option:selected').val()+"&tags_values="+$('div#mass_operation_remove_tag_value input[name=remove_tags]').select2("val");
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            info_box($('#notice'), 'Значения свойств удалены!', '');
                            finish_mass_operation();
                        }
                    });
                    break;
                case 10: //Установить количество
                    var stock = $('#mass_operation_set_stock input').val();
                    var href = "{$config->root_url}{$edit_module->url}?mode=set_stock&id="+item.attr('data-id')+"&stock="+stock;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        item.find('input[name^=variants_stock]').val(stock);
                        items_count--;
                        if (items_count == 0){
                            info_box($('#notice'), 'Количество установлено!', '');
                            finish_mass_operation();
                        }
                    });
                    break;
                {*case 12: //Установить скидку в %
                    var discount = $('#mass_operation_set_discount input').val();
                    var href = "{$config->root_url}{$edit_module->url}?mode=set_discount&id="+item.attr('data-id')+"&discount="+discount;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            location.reload();
                        }
                    });
                    break;
                case 13: //Установить фисксированную скидку
                    var discount = $('#mass_operation_set_fix_discount input').val();
                    var href = "{$config->root_url}{$edit_module->url}?mode=set_fix_discount&id="+item.attr('data-id')+"&discount="+discount;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            location.reload();
                        }
                    });
                    break;
                case 14: //Убрать скидку
                    var href = "{$config->root_url}{$edit_module->url}?mode=remove_discount&id="+item.attr('data-id');
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            location.reload();
                        }
                    });
                    break;*}
                case 12: //Добавить/Удалить скидку
                    var value = $('#mass_operation_set_discount_input').val();
                    var type = $('#mass_operation_set_discount option:selected').val();
                    var href = "{$config->root_url}{$edit_module->url}?mode="+type+"&id="+item.attr('data-id')+"&discount="+value;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            location.reload();
                        }
                    });
                    break;
                case 15: //Изменить цену
                    var value = $('#mass_operation_change_price_input').val();
                    var type = $('#mass_operation_change_price option:selected').val();
                    var href = "{$config->root_url}{$edit_module->url}?mode="+type+"&id="+item.attr('data-id')+"&value="+value;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                        items_count--;
                        if (items_count == 0){
                            location.reload();
                        }
                    });
                    break;
                case 16: //Изменить валюту
                    if ($('#mass_operation_set_currency option:selected').length > 0)
                    {
                        var href = "{$config->root_url}{$edit_module->url}?mode=set_currency&id="+item.attr('data-id')+"&value="+$('#mass_operation_set_currency option:selected').val();
                        $.get(href, function(data) {
                            item.find('input[type=checkbox]').prop('checked', false);
                            items_count--;
                            if (items_count == 0){
                                location.reload();
                            }
                        });
                    }
                    break;
                case 17: //Установить значение доп флага
                    if ($('#mass_operation_select_add_flag option:selected').length > 0)
                    {
                        var href = "{$config->root_url}{$edit_module->url}?mode=set_add_flag&id="+item.attr('data-id')+"&flag="+$('#mass_operation_select_add_flag option:selected').val()+"&value="+$('input[name=mass_operation_select_add_flag_input][checked]').val();
                        $.get(href, function(data) {
                            item.find('input[type=checkbox]').prop('checked', false);
                            items_count--;
                            if (items_count == 0){
                                location.reload();
                            }
                        });
                    }
                    break;
                case 18: //Установить значение доп поля
                    if ($('#mass_operation_select_add_field option:selected').length > 0)
                    {
                        var href = "{$config->root_url}{$edit_module->url}?mode=set_add_field&id="+item.attr('data-id')+"&field="+$('#mass_operation_select_add_field option:selected').val()+"&value="+$('#mass_operation_select_add_field_input').val();
                        $.get(href, function(data) {
                            item.find('input[type=checkbox]').prop('checked', false);
                            items_count--;
                            if (items_count == 0){
                                location.reload();
                            }
                        });
                    }
                    break;
                {*case 11:    //Удаление
                    var subitem = item.find('a.delete-item').first();
                    var href = subitem.attr('href');
                    //var l = $( '#apply_mass_operation' ).ladda();
                    var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
                    l.start();
                    l.setProgress( 0 );
                    var interval = 1 / items_count;
                    var ok_count = 0;
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            ok_count++;
                            l.setProgress( ok_count * interval );
                            if (items_count == 0)
                            {
                                l.stop();
                                l.toggle();
                                l.isLoading();
                                var url = $('ol.dd-list').attr('data-href');
                                if (url == undefined)
                                    return false;
                                var params = [{
                                    'key': 'url',
                                    'value': url
                                }];
                                products_request_ajax(params, false);
                            }
                            return false;
                        }
                    });
                    break;*}
            }
        });

        return false;
    });

    $('#brands_toggle').click(function(){
        $(this).find('i').toggleClass('fa-plus').toggleClass('fa-minus');
        var href = "{$config->root_url}{$module->url}{url add=['toggle_brands_auto_open'=>1]}";
        $.get(href, function(data) {
            info_box($('#notice'), 'Состояние блока сохранено!', '');
        });
    });
</script>