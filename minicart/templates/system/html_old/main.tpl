{* Главная страница админки *}

{$v = $smarty.server.SERVER_NAME|ucfirst|cat:' - административная панель'}
{$meta_title=$v scope=parent}

{* Тело страницы *}

<div class="row mt20">
    <div class="col-md-6">
        <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Последние заказы</h3>
                <a href="{$config->root_url}{$orders_module->url}" class="btn btn-default btn-xs">Все заказы</a>
            </div>
              <div class="panel-body">
                <table class="table lastorders">
        <tbody>
        {if $last_orders}
        {foreach $last_orders as $order}
            <tr>
                <td>{$order->day_str}</td>
                <td>{$order->date|time}</td>
                <td><a href="{$config->root_url}{$order_module->url}{url add=['id'=>$order->id]}">Заказ №{$order->id}</a></td>
                <td>{$order->name|truncate:14:"...":true}</td>
                <td>{$order->total_price|convert} {$main_currency->sign}</td>
                {*<td><a href="{$config->root_url}{$order_module->url}{url add=['id'=>$order->id]}" class="btn btn-default btn-xs">Смотреть заказ</a></td>*}
            </tr>
        {/foreach}
        {else}
            Нет заказов
        {/if}
        
      </table>
      </div>
      </div>
    </div>
    <div class="col-md-6">
            <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Заказы звонка</h3>
                <a href="{$config->root_url}{$callbacks_module->url}" class="btn btn-default btn-xs">Все заказы звонка</a>
            </div>
              <div class="panel-body">
                <table class="table lastorders">
        <tbody>
            {if $last_callbacks}
            {foreach $last_callbacks as $callback}
                <tr>
                    <td>{$callback->day_str} в {$callback->date|time}</td>
                    <td>№{$callback->id}</td>
                    <td>{$callback->user_name}</td>
                    <td>{if $callback->phone_code && $callback->phone}+7 ({$callback->phone_code}) {$callback->phone|phone_mask}{/if}</td>
                </tr>
            {/foreach}
            {else}
                Нет заказов звонка
            {/if}

            {*<tr>
                <td>Вчера в 16:17</td>
                <td><a href="#">Заказ звонка №335</a></td>
                <td>Вася</td>
                <td>+7 (926) 264-41-24</td>
            </tr>
                        <tr>
                <td>Вчера в 16:17</td>
                <td><a href="#">Заказ звонка №334</a></td>
                <td>Иван</td>
                <td>+7 (926) 264-41-24</td>
            </tr>*}
      </table>
      </div>
      </div>    
      
      
          <div class="panel panel-default main-orders topbtn">
            <div class="panel-heading">
                <h3 class="panel-title">Отзывы о товарах</h3>
                <a href="{$config->root_url}{$reviews_module->url}" class="btn btn-default btn-xs">Все отзывы о товарах</a>
            </div>
              <div class="panel-body">
                <table class="table lastorders">
        <tbody>
            {if $last_reviews}
            {foreach $last_reviews as $review}
                <tr>
                    <td>{$review->day_str} в {$review->date|time}</td>
                    <td><a href="{$config->root_url}{$review_module->url}{url add=['id'=>$review->id]}">Отзыв №{$review->id}</a></td>
                    <td><div class="raiting rate{$review->avg_rating*10}" title="Рейтинг товара {$review->rating|string_format:'%.1f'}"></div></td>
                    <td>{$review->name}</td>
                </tr>
            {/foreach}
            {else}
                Нет отзывов
            {/if}
      </table>
      </div>
      </div> 
      
      
    </div>



</div>




{*$page->body*}

