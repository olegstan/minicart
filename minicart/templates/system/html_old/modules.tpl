{* Title *}
{$meta_title='Модули' scope=parent}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}                
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>               
            </div>
            
            </div>
            {*<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>*}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Модули</legend>

                {foreach $modules as $m}
                <div class="module">
                    <div class="module-left">
                        <div class="module-image">
                            {if $m->icon}
                            <a {if $m->openable && $m->enabled}href="{$config->root_url}{$m->url}"{/if}>
                                <img src="{$config->root_url}/modules/{$m->path}/{$m->icon}"  width="48"/> 
                            </a>
                            {/if}
                        </div>
                    </div>
                    <div class="module-right">
                        <div class="module-name">{if $m->openable && $m->enabled}<a href="{$config->root_url}{$m->url}">{/if}{$m->name}{if $m->openable}</a>{/if}</div>
                        {if $m->category != 'system'}
                        <div class="module-onoff">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default4 btn-xs on {if $m->enabled}active{/if}">
                                    <input type="radio" name="enabled[{$m->id}]" value="1" {if $m->enabled}checked{/if}>  Активен
                                </label>
                                <label class="btn btn-default4 btn-xs off {if !$m->enabled}active{/if}">
                                    <input type="radio" name="enabled[{$m->id}]" value="0" {if !$m->enabled}checked{/if}> Скрыт
                                </label>
                            </div>
                        </div>
                        {/if}
                        <div class="module-desc">{$m->description}</div>
                        <div class="module-version"><span>Версия: </span>{$m->version}</div>
                        {if $m->is_settings_page}<a href="{$config->root_url}{$m->url}settings">Настройки</a>{/if}
                    </div>
                </div>
                {/foreach}

                {*<div class="module">
                    <div class="module-left">
                        <div class="module-image">
                            <a href="#">
                                <img src="{$path_admin_template}/img/icons/module.png"  width="48"/> 
                            </a>
                        </div>
                    </div>
                    <div class="module-right">
                        <div class="module-name"><a href="#">Корзина</a></div>
                        <div class="module-onoff">
                            <div class="btn-group" data-toggle="buttons">
                              <label class="btn btn-default4 btn-xs on">
                                <input type="radio" name="visible" id="option1" value="1">  Активен
                              </label>
                              <label class="btn btn-default4 btn-xs off">
                                <input type="radio" name="visible" id="option2" value="0"> Скрыт
                              </label>
                    </div>
                        </div>
                        <div class="module-desc">Настойки корзины: минимальная сумма заказа, обязательные поля и другие.</div>
                        <div class="module-version"><span>Версия: </span>1.0</div>
                    </div>
                </div>

                <div class="module">
                    <div class="module-left">
                        <div class="module-image">
                            <a href="#">
                                <img src="{$path_admin_template}/img/icons/module.png"  width="48"/> 
                            </a>
                        </div>
                    </div>
                    <div class="module-right">
                        <div class="module-name"><a href="#">Хлебные крошки</a></div>
                        <div class="module-desc">Настройки хлебных крошек</div>
                        <div class="module-version"><span>Версия: </span>1.0</div>
                    </div>
                </div>
                
                <div class="module">
                    <div class="module-left">
                        <div class="module-image">
                            <a href="#">
                                <img src="{$path_admin_template}/img/icons/module.png"  width="48"/> 
                            </a>
                        </div>
                    </div>
                    <div class="module-right">
                        <div class="module-name"><a href="#">E-mail сообщения о заказах</a></div>
                        <div class="module-desc">Шаблоны писем</div>
                        <div class="module-version"><span>Версия: </span>1.0</div>
                    </div>
                </div>
                
                <div class="module">
                    <div class="module-left">
                        <div class="module-image">
                            <a href="#">
                                <img src="{$path_admin_template}/img/icons/module.png"  width="48"/> 
                            </a>
                        </div>
                    </div>
                    <div class="module-right">
                        <div class="module-name"><a href="#">Контакт-центр</a></div>
                        <div class="module-desc">Настройки контакт-центра сайта</div>
                        <div class="module-version"><span>Версия: </span>1.0</div>
                    </div>
                </div>*}
            </div>
            
            <div class="col-md-4">
                <ul class="list-group">
                  <a href="{$config->root_url}{$module->url}" class="list-group-item {if empty($category)}active{/if}"><span class="badge">{$modules_count['all']}</span>Все модули</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'system']}" class="list-group-item {if $category == 'system'}active{/if}"><span class="badge">{$modules_count['system']}</span>Система</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'cart']}" class="list-group-item {if $category == 'cart'}active{/if}"><span class="badge">{$modules_count['cart']}</span>Корзина</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'payment']}" class="list-group-item {if $category == 'payment'}active{/if}"><span class="badge">{$modules_count['payment']}</span>Оплата</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'delivery']}" class="list-group-item {if $category == 'delivery'}active{/if}"><span class="badge">{$modules_count['delivery']}</span>Доставка</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'materials']}" class="list-group-item {if $category == 'materials'}active{/if}"><span class="badge">{$modules_count['materials']}</span>Материалы</a>
                  <a href="{$config->root_url}{$module->url}{url add=['category'=>'other']}" class="list-group-item {if $category == 'other'}active{/if}"><span class="badge">{$modules_count['other']}</span>Другое</a>
                </ul>

            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>