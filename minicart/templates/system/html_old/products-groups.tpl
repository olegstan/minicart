{* Главная страница админки *}



{* Тело страницы *}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">

            <a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Группы товаров</legend>

                <div id="sortview">
                <div class="itemcount">Групп: {$groups_on_main_count}</div>
                </div>
                <div id="new_menu" class="dd">
                    {if $groups_on_main}
                    {include file='pagination.tpl'}
                    <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $groups_on_main as $g}
                            <li class="dd-item dd3-item" data-id="{$g->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a href="{$config->root_url}{$edit_module->url}{url add=['id'=>$g->id]}">{$g->name}</a>
                                    <div class="controllinks">
                                        <code class="goods">{$g->products|count}</code>
                                        <code class="opcity50" style="display:none;">{$g->id}</code>
                                        <a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$g->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $g->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$g->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    </div>
                    {include file='pagination.tpl'}
                    {else}
                        <p>Нет групп товаров</p>
                    {/if}
                </div>
            </div>

            <div class="col-md-4">

            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        {if $groups_on_main}
            $('#new_menu').nestable({
                maxDepth: 1
            });
        {/if}

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    });

    $('#new_menu').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_group_item(obj);
        });
        return false;
    });

    function delete_group_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                li.remove();
            return false;
        });
        return false;
    }

    $('#new_menu').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
            return false;
        });
        return false;
    });

    // infinity
</script>