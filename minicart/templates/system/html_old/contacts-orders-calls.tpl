{* Title *}
{$meta_title='Заказы звонков' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}


    <form id="form-menu" method="post" action="">
        <input type="hidden" name="session_id" value="">

        <div class="controlgroup form-inline">
            <div class="currentfilter">
                    <div class="input-group">
                        <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                        <span class="input-group-btn">
                        {if $keyword}
                        <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                        {/if}
                        <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                        

            </div>
        </div>
           </div>




<div class="row com-calls">
    <div class="col-md-8">
        <legend>Заказы звонков</legend>
        <div id="new_menu" class="dd callslist">
                {if $callbacks}
                
                {include file='pagination.tpl'}
                
                <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $callbacks as $callback}
                            <li class="dd-item dd3-item {if !$callback->moderated}notmoderated{/if}" data-id="{$callback->id}">
                                <div class="dd3-content">
                                    <input type="checkbox" value="{$callback->id}" class="checkb">
                                    <div class="callblock">
                                         Заказ звонка №{$callback->id}  
                                            
                                    </div>

                                    <div class="call-notes">
                                        <table>
                                        <tr>
                                        <td>Имя:</td>
                                        <td>
                                            {if $callback->user && $callback->user_name && ($callback->user->name != $callback->user_name)}
                                                <a href="{$config->root_url}{$user_module->url}{url add=['id'=>$callback->user->id]}">{$callback->user->name}</a> от имени {$callback->user_name}
                                            {elseif $callback->user}
                                                <a href="{$config->root_url}{$user_module->url}{url add=['id'=>$callback->user->id]}">{$callback->user->name}</a>
                                            {elseif $callback->user_name}
                                                {$callback->user_name}
                                            {else}
                                                Аноним
                                            {/if}
                                        </td>
                                        </tr>
                                        <tr>
                                        <td>Номер телефона:</td>
                                        <td>+7 ({$callback->phone_code}) {$callback->phone|phone_mask}</td>
                                        </tr>
                                        {if $callback->call_time}
                                        <tr>
                                        <td>Когда звонить:</td>
                                        <td>{$callback->call_time}</td>
                                        </tr>
                                        {/if}
                                        
                                        
                                        {if $callback->message}
                                        <tr>
                                        <td>Сообщение:</td>
                                        <td>{$callback->message}</td>
                                        </tr>
                                        {/if}
                                        
                                        <tr>
                                        <td>Дата:</td>
                                        <td>{$callback->day_str}{*if $callback->day_str != 'Сегодня' && $callback->day_str != 'Вчера'} {$callback->created|date}{else}{/if*} в {$callback->created|time}</td>
                                        </tr>

                                        <tr class="opacity03">
                                            <td>IP-адрес:</td>
                                            <td>{$callback->ip}</td>
                                        </tr>
                                       </table>

                                    </div>

                                
                                

                                    <div class="controllinks">
                                
                                    <a class="delete-item" href="{$config->root_url}{$module->url}{url add=['id'=>$callback->id, 'mode'=>'delete']}"><i class="fa fa-times"></i></a>
                                    
                                    <div class="callback-onoff">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-default4 btn-xs on {if !$callback->state}active{/if}" data-href="{$config->root_url}{$module->url}{url add=['mode'=>'toggle','id'=>$callback->id,'ajax'=>1]}">
                                            <input type="radio" value="1" {if !$callback->state}checked=""{/if}>Новый
                                        </label>
                                        <label class="btn btn-default4 btn-xs off {if $callback->state}active{/if}" data-href="{$config->root_url}{$module->url}{url add=['mode'=>'toggle','id'=>$callback->id,'ajax'=>1]}">
                                            <input type="radio" value="0" {if $callback->state}checked=""{/if}><i class="fa fa-check"></i> В обработке
                                        </label>
                                    </div>
                                </div>


                                    </div>
                                </div>
                            </li>
                        {/foreach}

                    </ol>
                </div>

                {include file='pagination.tpl'}

                {else}
                    <p>Нет заказов звонка</p>
                {/if}

            </div>
            {* Массовые операции для заказов звонка *}


                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все &uarr;</div>
                    <select id="mass_operation" class="form-control">
                        <option value='1'>Удалить</option>
                    </select>

                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
           
        </div>
    </div>
    <div class="col-md-4">
    </div>
    


<script type="text/javascript">

    $('#new_menu').on('click', '.callback-onoff label.on, .callback-onoff label.off', function(){
        var li = $(this).closest('li');
        if (!$(this).hasClass('active')){
            var href = $(this).data('href');
            $.get(href, function(data) {
                li.removeClass('notmoderated');
            });
        }
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_callback_item(obj);
        });
        return false;
    });

    function delete_callback_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (operation == 1 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        return false;
    });
</script>