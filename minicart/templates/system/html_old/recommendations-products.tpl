{$meta_title = "Рекомендации товары" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    </div>

    <!-- Параметры -->
    <div class="row">
        <div class="col-md-8">
           <legend>Рекомендации товары</legend>
        </div>
        
        <div class="col-md-4">
        <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="gheading1">
                <h4 class="panel-title">
                  <a class="" data-toggle="collapse" href="#group1" aria-expanded="true" aria-controls="collapseListGroup1">
                    <span class="badge">14</span>
                    Общие
                  </a>
                </h4>
              </div>
              <div id="group1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="collapseListGroupHeading1" aria-expanded="true">
                <div class="list-group">
                  <a href="#" class="list-group-item {*active*}">
                    <span class="badge">4</span>
                    Нет цены
                  </a>
                  <a href="#" class="list-group-item">
                  <span class="badge">4</span>
                  Цена = 0</a>
                  <a href="#" class="list-group-item">
                  Нет изображений
                  </a>
                  <a href="#" class="list-group-item">
                  Не уникальный артикул
                  </a>
                  
                </div>
              </div>
            </div>
            
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="gheading2">
                <h4 class="panel-title">
                  <a class="" data-toggle="collapse" href="#group2" aria-expanded="true" aria-controls="group2">
                    <span class="badge">14</span>
                    SEO
                  </a>
                </h4>
              </div>
              <div id="group2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="geading2" aria-expanded="true">
                <div class="list-group">
                  <a href="#" class="list-group-item {*active*}">
                    <span class="badge">4</span>
                    Неверный урл
                  </a>
                  <a href="#" class="list-group-item">
                  <span class="badge">4</span>
                  Цена = 0</a>
                  <a href="#" class="list-group-item">
                  Пустой title
                  </a>
                  <a href="#" class="list-group-item">
                  Не уникальный title
                  </a>
                  <a href="#" class="list-group-item">
                  Пустые метатеги
                  </a>
                  
                  
                </div>
              </div>
            </div>
            
            
        </div>
    </div>


</form>
<!-- Основная форма (The End) -->

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>