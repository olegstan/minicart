<!DOCTYPE html>
{*
    Общий вид страницы
    Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>

    <base href="{$config->root_url}/"/>
    <title>{$meta_title|escape}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    {* Метатеги *}
    <meta name="description" content="{$meta_description|escape}" />
    <meta name="keywords" content="{$meta_keywords|escape}" />
    <link rel="shortcut icon" href="{$path_admin_template}/img/favicon.ico">
    
    {* Подключаем jquery последнюю версию из библиотек Google *}
    {*<script src="http://code.jquery.com/jquery-latest.min.js"></script>*}
    {*<script src="http://code.jquery.com/jquery-2.0.0.min.js"></script>*}
    {*<script src="http://yandex.st/jquery/2.0.3/jquery.min.js"></script>*}
    <script src="{$path_admin_template}/js/jquery.min.js"></script>
    

    {* Подключаем бустрап 3*}
    <link href="{$path_admin_template}/css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$path_admin_template}/js/bootstrap.min.js"></script>
       
    {* Подключаем Font Awesome *}
    <link rel="stylesheet" href="{$path_admin_template}/css/font-awesome.min.css">
    
    {* Подключаем css *}    
    <link href="{$path_admin_template}/css/template.css" rel="stylesheet"/>
    
    {* Подключаем скрипт управления меню *}    
    <script src="{$path_admin_template}/js/jquery.nestable.js"></script>
    
    {* Подключаем скрипт оповещений меню *}    
    <script src="{$path_admin_template}/js/messages.js"></script>
    
    {* Подключаем сортировки превью изображений *}    
    <script src="{$path_admin_template}/js/jquery.dragsort-0.5.1.min.js"></script>
    
    {* Подключаем CKEditor *}
    <script src="{$config->root_url}/public/assets/ckeditor/ckeditor.js"></script>

    {* Подключаем Fancybox *}
    <script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.mousewheel-3.0.6.pack.js"></script>
    <link href="{$config->root_url}/public/assets/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$config->root_url}/public/assets/fancybox/jquery.fancybox.pack.js"></script>

    <link rel="stylesheet" href="{$config->root_url}/public/assets/fancybox/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.fancybox-buttons.js"></script>
    <link rel="stylesheet" href="{$config->root_url}/public/assets/fancybox/jquery.fancybox-thumbs.css" type="text/css" media="screen" />
    <script type="text/javascript" src="{$config->root_url}/public/assets/fancybox/jquery.fancybox-thumbs.js"></script>
    {* /Подключаем Fancybox *}

    {* Подключаем служебные функции *}
    <script src="{$path_admin_template}/js/functions.js"></script>

    {* Подключаем ajax-заливку файлов *}
    <script src="{$path_admin_template}/js/jquery.damnUploader.min.js"></script>

    {* Подключаем Select2 *}
    <link href="{$path_admin_template}/css/select2.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$path_admin_template}/js/select2.min.js"></script>
    <script src="{$path_admin_template}/js/select2_locale_ru.js"></script>

    {* Подключаем маску ввода *}
    <script src="{$path_admin_template}/js/jquery.maskedinput.min.js"></script>

    {* Подключаем Bootstrap Datepicker *}
    <link href="{$path_admin_template}/css/datepicker.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$path_admin_template}/js/bootstrap-datepicker.js"></script>
    <script src="{$path_admin_template}/js/bootstrap-datepicker.ru.js"></script>

    {* Подключаем WayPoint - прекрепление кнопок управления к шапке *}
    <script src="{$path_admin_template}/js/waypoints.min.js"></script>
    <script src="{$path_admin_template}/js/waypoints-sticky.js"></script>

    {* Подключаем авторесайз для textarea *}
    <script src="{$path_admin_template}/js/jquery.autosize.min.js"></script>

    {* Подключаем snippet для анимирования кнопки удаления *}
    <link href="{$path_admin_template}/css/ladda-themeless.min.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$path_admin_template}/js/spin.min.js"></script>
    <script src="{$path_admin_template}/js/ladda.min.js"></script>

    {* Подключаем компонент для выставления рейтинга *}
    <link type="text/css" rel="stylesheet" href="{$path_frontend_template}/css/jquery.rating.css" />
    <script src="{$path_frontend_template}/js/jquery.rating-2.0.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body id="mainbody">
    <a href="#top"></a>
    <div id="notice">
    </div>
    <div id="wrap">
    <div class="container">
    <div class="minicontainer"> 
    <nav class="navbar navbar-default noradius mininavbar" role="navigation">
     
    <div class="row">
    <div class="col-md-3">
    <ul class="nav navbar-nav adminmenu">
             
             <li><a href="{$config->root_url}" class="to-frontend allow-jump" data-placement="bottom" data-toggle="tooltip" data-original-title="Перейти в магазин"><span>{$smarty.server.SERVER_NAME|capitalize:false:true}</span> <i class="fa fa-level-up"></i></a></li>
            
          </ul>
          </div>
    
    <div class="col-md-9">
           <ul class="nav navbar-nav otkliki">
                {foreach $menu_tree_up->subitems as $item}
                    {assign var="show_menu_item" value=false}
                    {foreach $item->subitems as $subitem}
                        {if in_array($subitem->module_name, $global_group_permissions)}
                            {assign var="show_menu_item" value=true}
                        {/if}
                    {/foreach}
                    {if $item->is_visible && (in_array($item->module_name, $global_group_permissions) || $show_menu_item)}
                        <li data-module="{$item->module_name}" class="{if (in_array($active_menu_item,$item->children) && !$module->is_external) || ($module->is_external && $item->sub_url == "modules/")}active{assign var="active_tree" value=$item->subitems}{/if}">
                            {$active_url = ""}
                            {if in_array($item->module_name, $global_group_permissions)}
                                {if $item->url}{$active_url = $item->url}{else}{if $item->sub_url}{$active_url = $item->sub_url}{/if}{/if}
                            {else}
                                {foreach $item->subitems as $subitem}
                                    {if in_array($subitem->module_name, $global_group_permissions) && empty($active_url)}
                                        {$active_url = $subitem->url}
                                    {/if}
                                {/foreach}
                            {/if}
                            {if $active_url == "/"}{$active_url = ""}{/if}
                            <a href="{$config->admin_url}{$active_url}" {if $item->css_class}class="{$item->css_class}"{/if} data-url="{$item->sub_url}">{if $item->icon}{$item->icon}{/if} <span>{$item->name|escape}</span> {if $item->sub_url == "contacts/orders-calls/" && $count_callbacks>0}<span class="badge">{$count_callbacks}</span>{elseif $item->sub_url == "community/reviews-products/" && $count_new_reviews>0}<span class="badge">{$count_new_reviews}</span>{/if}{if $item->sub_url == "orders-processed/" && $count_new_orders>0}<span class="badge">{$count_new_orders}</span>{/if}</a>
                        </li>
                    {/if}
                {/foreach}

            </ul>
            
            
             <ul class="nav navbar-nav pull-right adminmenu">
             <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle usermenu" href="#">{$user->name} <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="/admin/logout/" class="logout"><i class="fa fa-power-off"></i> <span>Выход</span></a></li>
                </ul>
              </li>
            
          </ul>
         
       </div>
       </div>
        
</nav>
     </div>
    <div class="topmenu">    
    
    <div role="navigation" class="navbar navbar-default mainmenu">
          <ul class="horizontal-menu nav navbar-nav">
          
          {foreach $menu_tree->subitems as $item}

            {if $item->is_visible && (in_array($item->module_name, $global_group_permissions) || $item->show_item)}
          
            <li data-url="{$url_menu_item}" data-module="{$item->module_name}" class="{*if $item->subitems}dropdown{/if*} {if (in_array($active_menu_item,$item->children) && !$module->is_external) || ($module->is_external && $item->sub_url == "modules/")}active{assign var="active_tree" value=$item->subitems}{/if}">
                <a href="{$config->admin_url}{$item->active_url}" {if $item->css_class}class="{$item->css_class}"{/if} data-url="{$item->sub_url}">{if $item->icon}{$item->icon}{/if}{$item->name|escape}{*if $item->subitems} <b class="caret"></b>{/if*}</a>
                {*if $item->subitems}
                    <ul class="dropdown-menu">
                        {foreach $item->subitems as $subitem}
                            {if $subitem->is_visible && in_array($subitem->module_name, $global_group_permissions)}
                                <li{if $active_menu_item == $subitem->id} class="active"{/if}><a href="{$config->admin_url}{if $subitem->url}{$subitem->url}{else}{if $subitem->sub_url}{$subitem->sub_url}{/if}{/if}">{if $subitem->icon}{$subitem->icon} {/if}{$subitem->name|escape}</a></li>
                            {/if}
                        {/foreach}
                    </ul>
                {/if*}

          
            {*<li><a href="{$config->admin_url}{if $item->url}{$item->url}{else}{if $item->sub_url}{$item->sub_url}{/if}{/if}" class="{$item->css_class} {if in_array($active_menu_item,$item->children)}active{assign var="active_tree" value=$item->subitems}{/if}"><span>{$item->name|escape}</span></a></li>*}
            {/if}
        {/foreach}

          </ul>
      </div>
     </div>
    
    <div class="mainbox">
    
    <div class="row">
        <div class="col-md-12">

            {$menu_third_level = false}

            <div class="thirdmenu">
            <ul class="nav">
                {foreach $active_tree as $item}
                    {if $item->is_visible && (in_array($item->module_name, $global_group_permissions) || $item->show_item)}
                    <li {if in_array($active_menu_item,$item->children)}class="active"{if $item->subitems}{$menu_third_level = $item->subitems}{/if}{/if}>
                        <a href="{$config->admin_url}{if $item->url && $item->url != "/"}{$item->url}{else}{if $item->sub_url && $item->sub_url != "/"}{$item->sub_url}{/if}{/if}" class="{$item->css_class}"><span>{$item->name|escape}</span></a>
                    </li>
                    {/if}
                {/foreach}
            </ul>
            </div>


            {if $menu_third_level}
                <div class="thirdmenu">
                <ul class="nav">
                    {foreach $menu_third_level as $item}
                        {if $item->is_visible && in_array($item->module_name, $global_group_permissions)}
                        <li {if in_array($active_menu_item,$item->children)}class="active"{/if}>
                            <a href="{$config->admin_url}{if $item->url && $item->url != "/"}{$item->url}{else}{if $item->sub_url && $item->sub_url != "/"}{$item->sub_url}{/if}{/if}" class="{$item->css_class}"><span>{$item->name|escape}</span></a>
                        </li>
                        {/if}
                    {/foreach}
                </ul>
                </div>
            {/if}

                <div id="right">
                    {$content}
                </div>

        </div>
        <!-- Основная часть (The End) -->
    </div>
    </div>
    </div>
    </div>


    <div id="footer">
    <div class="container">
          <div class="row">
          <div class="col-md-6">
        <p class="text-muted credit">Работает на <a href="http://minicart.ru/">Minicart 1.0.0</a></p>
        </div>
        <div class="col-md-6 tright">
        <p class="text-muted credit"><a href="http://minicart.userecho.com/forum/30886-idei-i-predlozheniya/" target="_blank">Сообщить об ошибке</a></p>
        </div>
 </div>
  </div>
    </div>

<script type="text/javascript">
    $(function() {
        $('a[data-toggle="tooltip"]').tooltip();
        $('a[data-toggle="tooltip"]').click(function(){
            if (!$(this).hasClass('allow-jump'))
                return false;
        });
        $('#mainbody').on('change','label.checkbox input[type=checkbox]', function(){
            $(this).next('label').toggleClass('checked').toggleClass('notchecked');
        });
        $('ul.horizontal-menu li.dropdown').hover(function() {
            $(this).find('.dropdown-menu:first').stop(true, true).show();
        }, function() {
            $(this).find('.dropdown-menu:first').stop(true, true).hide();
        });

        $('div.controlgroup').waypoint('sticky');

        $('body').on('click', function (e) {
            $('div.popover').each(function () {
                $(this).prev().popover('destroy');
            });
        });
    });
</script>
</body>
</html>