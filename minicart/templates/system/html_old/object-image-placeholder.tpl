{* Шаблон для загрузки изображений *}
{* Входные параметры: 
        $object    - объект для которого подгружается шаблон (product, category, tag, etc...)
        $temp_id - если объект еще не существует, то временный id
        $module - модуль
        $images_object_name - название объекта для ресайза изображений
        $limit_images - ограничение на кол-во изображений (по умолчанию - 10)
*}


{*if $object*}
    <div class="object-images main-object-images nopadding">
        {include file='object-images.tpl'}
    </div>

    <div class="controls">
        <span class="btn btn-file btn-default">
            <i class="fa fa-plus"></i> <span>Добавить изображения</span>
            <input id="uploadImageField" type="file" multiple name="uploaded-images">
            <i class="fa fa-spin fa-spinner hidden" id="upload-anim"></i>
        </span>
        <a id="image-upload-internet" class="btn btn-default" type="button" title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Загрузить из интернета"><i class="fa fa-globe"></i></a>
        <div id="internet-upload-box" style="display:none">
            <input type="text"  class="form-control" id="image-upload-url" value="" placeholder="Введите URL изображения"/>
            <a id="image-upload-btn" class="btn btn-default" type="button" href="#"><i class="fa fa-arrow-circle-right"></i></a>
            <a id="image-upload-btn-cancel" class="btn btn-default" type="button" href="#"><i class="fa fa-times"></i></a>
        </div>
    </div>

    <div id="img-container" class="imguploader">или перетащите изображения сюда
        <ul id="img-list" class="hidden"></ul>
        <div class="clear"></div>
    </div>

{*else}
    <div class="alert alert-info">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <strong>Внимание!</strong> Для загрузки изображений необходимо сохранить объект
    </div>
{/if*}



<script type="text/javascript">

    var imgList = $('#img-list');
    var dropBox = $('#img-container');
    var fileInput = $('#uploadImageField');

    $(document).ready(function(){
        fileInput.damnUploader({
            url: '{$config->root_url}{$module->url}',
            fieldName:  'uploaded-images',
            object_id: '{if $object->id}{$object->id}{else}{$temp_id}{/if}',
            dropBox: dropBox,
            limit: {if $limit_images}{$limit_images}{else}10{/if},
            onSelect: function(file) {
                $('#upload-anim').removeClass('hidden');
                var addedItem = addFileToQueue(file);
                fileInput.damnUploader('startUploadItem', addedItem.queueId);
                return false;
            },
            onAllComplete: function(){
                $('#upload-anim').addClass('hidden');

                fileInput.damnUploader('cancelAll');
                imgList.html('');

                {if $object->id}
                href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_images', 'object'=> $images_object_name, 'ajax'=>1]}';
                {else}
                href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_images', 'object'=> $images_object_name, 'ajax'=>1]}';
                {/if}
                $.get(href, function(data) {
                    if (data.success)
                    {
                        $('div.main-object-images').html(data.data);
                    }
                });
                return false;
            },
        });

        // Обработка событий drag and drop при перетаскивании файлов на элемент dropBox
        dropBox.bind({
            dragenter: function() {
                $(this).addClass('highlighted');
                return false;
            },
            dragover: function() {
                return false;
            },
            dragleave: function() {
                $(this).removeClass('highlighted');
                return false;
            }
        });

        $("#upload-all").click(function() {
            fileInput.damnUploader('startUpload');
            return false;
        });

        $("ul#list1").dragsort({
            dragSelector: "div",
            placeHolderTemplate: "<li class='placeHolder'><div></div></li>",
        });

        $(".fancybox").fancybox({
                prevEffect    : 'none',
                nextEffect    : 'none',
                helpers    : {
                    title    : {
                        type: 'outside'
                    },
                    thumbs    : {
                        width    : 50,
                        height    : 50
                    },
                    overlay : {
                        locked     : false
                    }
                }
        });
    });

    function updateProgress(bar, value) {
        bar.css('width', value+'%');
    }

    function addFileToQueue(file) {

        // Создаем элемент li и помещаем в него название, миниатюру и progress bar
        var li = $('<li/>').appendTo(imgList);
        var title = $('<div/>').text(file.name+' ').appendTo(li);
        var cancelButton = $('<a/>').attr({
            href: '#cancel',
            title: 'отменить'
        }).text('X').appendTo(title);

        // Если браузер поддерживает выбор файлов (иначе передается специальный параметр fake,
        // обозначающий, что переданный параметр на самом деле лишь имитация настоящего File)
        if(!file.fake)
        {
            // Отсеиваем не картинки
            var imageType = /image.*/;
            if (!file.type.match(imageType)) {
                return true;
            }

            // Добавляем картинку и прогрессбар в текущий элемент списка
            var img = $('<img/>').appendTo(li);

            var pBar = $('<div class="progress progress-striped active"></div>').appendTo(li);
            var ppBar = $('<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>').appendTo(pBar);

            // Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
            // инфу обо всех файлах (только в браузерах, поддерживающих FileReader)
            if($.support.fileReading) {
                var reader = new FileReader();
                reader.onload = (function(aImg) {
                    return function(e) {
                        aImg.attr('src', e.target.result);
                        aImg.attr('width', 150);
                    };
                })(img);
                reader.readAsDataURL(file);
            }
        }

        // Создаем объект загрузки
        var uploadItem = {
            file: file,
            queueId: 0,
            onProgress: function(percents) {
                updateProgress(ppBar, percents);
            },
            onComplete: function(successfully, data, errorCode) {
                pBar.removeClass('active');
            },
        };

        // помещаем его в очередь
        var queueId = fileInput.damnUploader('addItem', uploadItem);
        uploadItem.queueId = queueId;

        // обработчик нажатия ссылки "отмена"
        cancelButton.click(function() {
            fileInput.damnUploader('cancel', queueId);
            li.remove();
            return false;
        });

        return uploadItem;
    }

    $('div.main-object-images').on("click", "a.delete_image", function(){
        var item = $(this).closest('li');
        var href = $(this).attr('href');
        $.get(href, function(data) {
            if (data.success)
            {
                item.fadeOut('fast').remove();
            }
        });
        return false;
    });

    $('#image-upload-internet').click(function(){
        $('#internet-upload-box').show();
        return false;
    });

    $('#image-upload-btn-cancel').click(function(){
        $('#internet-upload-box').hide();
        return false;
    });

    $('#image-upload-btn').click(function(){
        var image_url = $('#image-upload-url').val();
        if (image_url.length == 0)
            return false;
        {if $object->id}
        var href = '{$config->root_url}{$module->url}?id={$object->id}&mode=upload_internet_image&object={$images_object_name}&ajax=1&image_url='+encodebase64(image_url);
        {else}
        var href = '{$config->root_url}{$module->url}?id={$temp_id}&mode=upload_internet_image&object={$images_object_name}&ajax=1&image_url='+encodebase64(image_url);
        {/if}
        $.get(href, function(data) {
            if (data.success)
            {
                {if $object->id}
                var href = '{$config->root_url}{$module->url}{url add=['id'=>$object->id, 'mode'=>'get_images', 'object'=>$images_object_name, 'ajax'=>1]}';
                {else}
                var href = '{$config->root_url}{$module->url}{url add=['id'=>$temp_id, 'mode'=>'get_images', 'object'=>$images_object_name, 'ajax'=>1]}';
                {/if}
                $.get(href, function(data) {
                    if (data.success)
                    {
                        $('div.main-object-images').html(data.data);
                    }
                });
            }
        });
        $('#image-upload-url').val('');
        $('#internet-upload-box').hide();
        return false;
    });

</script>