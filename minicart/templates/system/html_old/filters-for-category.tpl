{* Title *}
{$meta_title='Фильтры для категорий' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">


        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}<button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>{/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Фильтры для категорий</legend>

                <div id="sortview">
                <div class="itemcount">Фильтров: {$sets_count}</div>
                <ul class="nav nav-pills" id="sort"> 
                <li class="disabled">Сортировать по:</li>
                
                <li>
                    <a class="btn btn-link btn-small {if $params_arr['sort'] == 'name'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'name', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}fa fa-sort-alpha-desc{else}fa fa-sort-alpha-asc{/if}"></i> <span>Алфавиту</span></a>
                </li>
                </ul>
                </div>

        <div id="new_menu" class="dd">
                 {if $tags_sets}

            {include file='pagination.tpl'}
              <div class="itemslist">
             <ol class="dd-list">
                {foreach $tags_sets as $set}
                    <li class="dd-item dd3-item" data-id="{$set->id}">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content">
                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$set->id]}"{/if}>{$set->name|escape}</a>
                            <div class="controllinks">
                                    <code class="opcity50">{$set->id}</code>
                                    {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$set->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $set->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$set->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                    </div>
                                </div>
                            </li>
                {/foreach}
            </ol>
            </div>

            {include file='pagination.tpl'}

            {else}
            <p>Нет фильров для категорий</p>
            {/if}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $tags_sets}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_filter_item(obj);
        });
        return false;
    });

    function delete_filter_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });
</script>