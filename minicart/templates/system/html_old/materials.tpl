{* Title *}
{$meta_title='Материалы' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags_values">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup form-inline">
            <div class="currentfilter products">
            <div class="row">
            <div class="col-md-8">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск">
                <span class="input-group-btn">
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
            </div>
            
            <div class="col-md-4">
            <div class="checkbox">
                    <label class="checkbox">
                    <input type="checkbox" id="search_in_current_category"> <label class="notchecked" for="search_in_current_category">В текущей категории</label>
                </label>
            </div>
  

  </div>
  </div>
            
            </div>
           

             
            
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['category_id'=>$category_id]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">

            <div class="col-md-8">
                <legend>
                <div class="row">
                <div class="col-md-4">
                Материалы <i class="fa fa-spinner fa fa-spin fa fa-large" id="materials-spinner" style="display:none;"></i>
                </div>
                
                </div>
                </legend>
                <div id="refreshpart" class="dd">{include file='materials-refreshpart.tpl'}</div>
                {if $allow_edit_module}
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все ↑</div>
                    <select id="mass_operation" class="form-control">
                        <option value="1">Сделать видимыми</option>
                        <option value="2">Скрыть</option>
                                    <option value="3">Переместить на страницу</option>
                                    <option value="4">Переместить в категорию</option>
                        <option value="11">Удалить</option>
                    </select>
                    <select id="mass_operation_move_to_page" class="form-control" style="display:none;">
                        {section name=pages start=1 loop=$total_pages_num+1 step=1}
                        <option>{$smarty.section.pages.index}</option>
                        {/section}
                    </select>
                    <select id="mass_operation_move_to_category" class="form-control" style="display:none;">

                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
                {/if}
            </div>
        
        <div class="col-md-4">
            <div id="tree">
            </div>
        </div>
        </div>

    </form>
</div>

<script type="text/javascript">
    // element - куда выводить, parent_id - какую категорию вывести
    function print_lazy_result(element, parent_id, icon_col, icon_spin, path, select_category_id){
        if (element == undefined)
        {
            if (icon_spin != undefined)
                icon_spin.hide();
            if (icon_col != undefined)
                icon_col.show();
            return false;
        }

        if (parent_id == undefined)
            parent_id = 0;
        var result = [];
        var href = '{$config->root_url}{$categories_module->url}?lazy_load=1&parent_id='+parent_id;

        $.get(href, function(data){
            if (data == undefined || data.length == 0)
                return false;
            var ol = $('<ol class="dd-list-small"></ol>');

            if (parent_id == 0)
            {
                var li = $('<li class="dd-item-small dd3-item-small dd-collapsed" data-id="0"></li>');
                li.append('<div class="dd3-content-small"><span>{$all_materials_count}</span><a href="#">Все категории</a></div>');
                ol.append(li);
            }

            for(var i=0 ; i<data.length ; i++){
                var li = $('<li class="dd-item-small dd3-item-small dd-collapsed" data-id="'+data[i].id+'"></li>');
                if (data[i].folder){
                    li.append('<i class="fa fa-minus" style="display: none;"></i>');
                    li.append('<i class="fa fa-plus" style="display: block;"></i>');
                    li.append('<i class="fa fa-spin fa fa-spinner" style="display: none;"></i>');
                }
                li.append('<div class="dd3-content-small"><span>'+data[i].materials_count+'</span><a href="#">'+data[i].title+'</a></div>');
                ol.append(li);
            }
            element.append(ol);
            if (icon_spin != undefined)
                icon_spin.hide();
            if (icon_col != undefined)
                icon_col.show();

            if (path != undefined && path.length > 0)
            {
                id = path[0];
                var _li = $('#tree li[data-id='+id+']');
                var _icon_col = _li.find('i.fa fa-minus:first');
                var _icon_exp = _li.find('i.fa fa-plus:first');
                var _icon_spin = _li.find('i.fa fa-spinner:first');

                _icon_exp.hide();
                _icon_spin.show();
                _li.removeClass('dd-collapsed');

                path.shift();
                print_lazy_result(_li, id, _icon_col, _icon_spin, path, select_category_id!=undefined?select_category_id:undefined);
            }
            else
                if (select_category_id != undefined)
                    $('#tree li[data-id='+select_category_id+'] a:first').addClass('active');
        });
    }

    function update_materials_count(item){
        var ids = [];
        ids.push(item.attr('data-id'));
        item.parents('li.dd-item-small').each(function(){
            ids.push($(this).attr('data-id'));
        });
        var href = '{$config->root_url}{$categories_module->url}?lazy_update=1&ids='+ids.join(',');
        $.get(href, function(data){
            for(var i in data)
            {
                var li = $('#tree li[data-id='+data[i].id+']');
                if (li.length>0)
                    li.find('div.dd3-content-small span:first').html(data[i].value);
            }
        });
    }

    var search_ajax_context;

    function materials_request_ajax(params){
        $('#materials-spinner').show();
        var url = "{$config->root_url}{$module->url}";
        {*if (params != undefined)
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    break;
                }
                if (index > 0)
                    url += "&";
                url += params[index].key + "=" + params[index].value;
            }
        var full_url = url;
        if (params != undefined && params.length > 0)
            full_url += "&";
        full_url += "ajax=1";*}

        if (params != undefined && params.length>0)
        {
            url += "?";
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    if (params.length > 1)
                        url += "?";
                    break;
                }
            }

            var index2 = 0;
            for(var index in params)
            {
                if (params[index].key != "url")
                {
                    if (index2 > 0)
                        url += "&";
                    url += params[index].key + "=" + params[index].value;
                    index2 += 1;
                }
            }
        }
        var full_url = url;
        if (params == undefined || params.length == 0 || (params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "?";
        if (params != undefined && params.length > 0 && !(params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "&";
        full_url += "ajax=1";

        $("body,html").animate({
            scrollTop:0
        }, 100);

        if (search_ajax_context != null)
            search_ajax_context.abort();

        search_ajax_context = $.ajax({
            type: 'GET',
            url: full_url,
            success: function(data) {
                if (typeof history.pushState != 'undefined') {
                    history.pushState(null,null,encodeURI(url));
                }
                $('#refreshpart').html(data);
                $('#refreshpart').nestable({
                    maxDepth: 1
                });
                $('#materials-spinner').hide();

                search_ajax_context = null;
            }
        });
        return false;
    }

    // развернуть категорию
    $('#tree').on('click', 'i.fa-plus', function(){
        var li = $(this).closest('li');
        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');
        var icon_spin = li.find('i.fa-spinner:first');

        icon_exp.hide();

        //Подгрузим на ajax'e дерево
        if (ol.length == 0)
        {
            icon_spin.show();
            print_lazy_result(li, li.attr('data-id'), icon_col, icon_spin);
        }
        else
        {
            ol.show();
            icon_col.show();
        }

        li.removeClass('dd-collapsed');
    });

    // свернуть категорию
    $('#tree').on('click', 'i.fa-minus', function(){
        var li = $(this).closest('li');
        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');

        ol.hide();
        li.addClass('dd-collapsed');
        icon_col.hide();
        icon_exp.show();
    });

    // выбор категории
    $('#tree').on('click', 'a', function(){
        var li = $(this).closest('li');
        var ol = li.find('ol.dd-list-small:first');
        var icon_col = li.find('i.fa-minus:first');
        var icon_exp = li.find('i.fa-plus:first');
        var icon_spin = li.find('i.fa-spinner:first');
        var id = li.attr('data-id');
        if (id == undefined)
            return false;
        id = parseInt(id);
        var params = [];
        $('#brands input:checked').each(function(){
            $(this).prop('checked', false);
            //selected_brands.push($(this).val());
        });
        if (id > 0)
        {
            params.push({
                'key': 'category_id',
                'value': id
            });
            $('a.add').attr('href', '{$config->root_url}{$edit_module->url}?category_id='+id);
        }
        else
        {
            $('a.add').attr('href', '{$config->root_url}{$edit_module->url}');

            //Свернем другие категории
            $('#tree li:not(.dd-collapsed)').each(function(){
                var icon_minus = $(this).find('i.fa-minus');
                icon_minus.trigger('click');
            });
        }
        materials_request_ajax(params);
        if (icon_col.length>0 && icon_exp.length>0 && icon_spin.length>0)
        {
            icon_exp.hide();

            //Подгрузим на ajax'e дерево, если оно еще не загружено
            if (ol.length == 0)
            {
                icon_spin.show();
                print_lazy_result(li, li.attr('data-id'), icon_col, icon_spin);
            }
            else
            {
                ol.show();
                icon_col.show();
            }
        }

        li.removeClass('dd-collapsed');
        $('#tree li a.active').each(function(){
            $(this).removeClass('active');
        });
        $(this).addClass('active');

        return false;
    });

    // сортировка
    $('#refreshpart').on('click', 'div.sort a', function(){
        var url = $(this).attr('href');
        if (url == undefined)
            return false;
        var params = [{
                'key': 'url',
                'value': url
            }];
        materials_request_ajax(params);
        return false;
    });

    $('#searchButton').click(function(){
        $('#searchField').triggerHandler('keyup');
    });

    // поиск keyword
    $('#searchField').on('keyup', function(){
        var params = [];

        if ($('#search_in_current_category').is(':checked'))
            params.push({
                'key': 'category_id',
                'value': $('#tree a.active').closest('li').attr('data-id')
            });

        if ($(this).val().length > 0)
        {
            $('#searchCancelButton').show();
            params.push({
                'key': 'keyword',
                'value': encodeURIComponent($(this).val())
            });
        }
        else
        {
            $('#searchCancelButton').hide();
            if (!$('#search_in_current_category').is(':checked'))
            {
                $('#tree li a.active').each(function(){
                    $(this).removeClass('active');
                });
                $('#tree li[data-id=0] a').addClass('active');
            }
        }
        materials_request_ajax(params);
        return false;
    });

    // сброс keyword
    $('#searchCancelButton').click(function(){
        var params = [];
        if ($('#search_in_current_category').is(':checked'))
            params.push({
                'key': 'category_id',
                'value': $('#tree a.active').closest('li').attr('data-id')
            });
        else
        {
            $('#tree li a.active').each(function(){
                $(this).removeClass('active');
            });
            $('#tree li[data-id=0] a').addClass('active');
        }
        $('#searchField').val('');
        materials_request_ajax(params);
        $('#searchCancelButton').hide();
        return false;
    });

    $('#refreshpart').on('click', 'ul.pagination a', function(){
        var url = $(this).attr('href');
        if (url == undefined)
            return false;
        var params = [{
                'key': 'url',
                'value': url
            }];
        materials_request_ajax(params);
        return false;
    });

    $(document).ready(function(){
        {if $materials}
            $('#refreshpart').nestable({
                maxDepth: 1
            });
        {/if}

        {if $category_path}
        var path = "{$category_path}";
        path = path.split(',');
        print_lazy_result($('#tree'),0,undefined,undefined, path, {$category_id});
        {else}
            {if $category_id}
                print_lazy_result($('#tree'),0,undefined,undefined, [], {$category_id});
            {else}
                print_lazy_result($('#tree'),0,undefined,undefined,[],0);
            {/if}
        {/if}
    });

    $('a.save').click(function(){
        var obj = $("#notice");
        var menu = $('#refreshpart').nestable('serialize');
        $.ajax({
            type: 'POST',
            url: $(this).closest('form').attr('action'),
            data: {
                menu: menu},
            error: function() {
                error_box(obj, 'Изменения не сохранены!', '');
            },
            success: function(data) {
                info_box(obj, 'Изменения сохранены!', '');
            }
        });
        return false;
    });

    $('#refreshpart').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_material_item(obj);
        });
        return false;
    });

    function delete_material_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
            {
                //update right panel with products_count
                var tree_node = $('#tree li a.active').closest('li');
                if (tree_node.length > 0)
                    update_materials_count(tree_node);

                var url = $('ol.dd-list').attr('data-href');
                if (url == undefined)
                    return false;
                var params = [{
                        'key': 'url',
                        'value': url
                    }];
                materials_request_ajax(params, false);
                return false;
            }
            return false;
        });
        return false;
    }

    $('#refreshpart').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
            return false;
        });
        return false;
    });

    $('div.checkall').click(function(){
        $("#refreshpart input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#mass_operation').change(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        if (operation == 3)
            $('#mass_operation_move_to_page').show();
        else
            $('#mass_operation_move_to_page').hide();
        if (operation == 4)
        {
            $('#materials-spinner').show();
            $.ajax({
                {*type: 'GET',
                url: "{$config->root_url}{$module->url}?categories_list",*}
                type: 'POST',
                url: '{$config->root_url}/ajax/get_data.php',
                data: {
                    'object': 'materials',
                    'mode': 'categories_list',
                    'session_id': '{$smarty.session.id}'
                },
                success: function(data) {
                    $('#mass_operation_move_to_category').html('');

                    if (data.success)
                        for(var index in data.data)
                        {
                            var category = data.data[index];
                            $('#mass_operation_move_to_category').append("<option value='"+category.id+"'>"+category.level+category.text{*category.name*}+"</option>");
                        }

                    $('#materials-spinner').hide();
                    $('#mass_operation_move_to_category').show();
                }
            });
        }
        else
            $('#mass_operation_move_to_category').hide();
        return false;
    });

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#refreshpart input[type=checkbox]:checked").length;

        if (operation == 11 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            if ($('ol.dd-list').length > 0)
                            {
                                var tree_node = $('#tree li a.active').closest('li');
                                if (tree_node.length > 0)
                                    update_materials_count(tree_node);

                                var url = $('ol.dd-list').attr('data-href');
                                if (url == undefined)
                                    return false;
                                var params = [{
                                        'key': 'url',
                                        'value': url
                                    }];
                                materials_request_ajax(params, false);
                            }
                            else
                                location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-on');
                                });

                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.first().hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-off');
                                });
                            }
                        });
                    break;
                case 3: //Переместить на страницу move_page/product_id/category_id/page/index
                    var href = "{$config->root_url}{$edit_module->url}move_page/"+item.attr('data-id')+"/"+$('#tree a.active').closest('li').attr('data-id')+"/"+$('#mass_operation_move_to_page option:selected').val()+"/"+items_count;
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                    });
                    break;
                case 4: //Переместить в категорию
                    var href = "{$config->root_url}{$edit_module->url}move_category/"+item.attr('data-id')+"/"+$('#mass_operation_move_to_category option:selected').val();
                    $.get(href, function(data) {
                        item.find('input[type=checkbox]').prop('checked', false);
                    });
                    break;
                {*case 11: //Удаление
                    var subitem = item.find('a.delete-item').first();
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                            {
                                var url = $('ol.dd-list').attr('data-href');
                                if (url == undefined)
                                    return false;
                                var params = [{
                                        'key': 'url',
                                        'value': url
                                    }];
                                materials_request_ajax(params, false);
                            }
                            return false;
                        }
                    });
                    break;*}
            }
        });
        return false;
    });
</script>
