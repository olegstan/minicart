{$meta_title = "Слайдшоу" scope=parent}

{* Настройки слайд-шоу *}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row slides">
            <div class="col-md-8">
                <legend>Слайды</legend>
                    <div id="new_menu" class="dd">
                        <ol class="dd-list">
                            {foreach $slides as $slide}
                            <li class="dd-item dd3-item" data-id="{$slide->id}">
                                <div class="dd-handle dd3-handle">
                                </div>
                                <div class="dd3-content">
                                    <div class="form-group">
                                        {if $slide->image}
                                            <img src="{$slide->image->filename|resize:'slideshow'}" class="slide"/>
                                        {/if}
                                    </div>
                                    {if !$slide->image}
                                    <div class="form-group controls">
                                        <span class="btn btn-file btn-default">
                                            <i class="fa fa-plus"></i> <span>Загрузить изображение</span>
                                            <input class="uploadImageField" type="file" multiple name="uploaded-images">
                                        </span>
                                    </div>
                                    {/if}
                                    <div class="form-group">
                                        <div class="input-group">
                                             <span class="input-group-addon">Ссылка:</span>
                                            <input type="text" class="form-control" name="url[{$slide->id}]" value="{$slide->url}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="controllinks">
                                    <a class="delete-item" href="{$config->root_url}{$module->url}{url add=['id'=>$slide->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                    <a class="toggle-item {if $slide->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$module->url}{url add=['id'=>$slide->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>
                                </div>

                                <input type="hidden" name="position[]" value="{$slide->id}"/>
                            </li>
                            {/foreach}
                        </ol>
                    </div>
                    <div class="massoperations">
                        <a href="#" class="btn btn-default add"><i class="fa fa-plus"></i> Добавить слайд</a>
                    </div>
            </div>
            <div class="col-md-4">
                <legend>Настойки слайдшоу</legend>
                <div class="form-group">
                    <label class="noinline">Сладшоу включено</label>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $settings->slideshow_enabled}active{/if}">
                            <input type="radio" name="slideshow_enabled" id="option1" value=1 {if $settings->slideshow_enabled}checked{/if}> Да
                        </label>
                        <label class="btn btn-default4 off {if !$settings->slideshow_enabled}active{/if}">
                            <input type="radio" name="slideshow_enabled" id="option2" value=0 {if !$settings->slideshow_enabled}checked{/if}> Нет
                        </label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Скорость смены слайда в милисекундах</label>
                    <input type="text" class="form-control" name="slideshow_animation_speed" value="{if $settings->slideshow_animation_speed}{$settings->slideshow_animation_speed}{else}800{/if}">
                    <p class="help-block">По умолчанию 800 (0.8 секунд)</p>
                </div>

                <div class="form-group">
                    <label>Время задержки смены слайдов в милисекундах</label>
                    <input type="text" class="form-control" name="slideshow_change_speed" value="{if $settings->slideshow_change_speed}{$settings->slideshow_change_speed}{else}4200{/if}">
                    <p class="help-block">По умолчанию 4200 (4.2 секунды)</p>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    function addFileToQueue(file, fileInput) {
        var uploadItem = {
            file: file,
            queueId: 0
        };
        var queueId = fileInput.damnUploader('addItem', uploadItem);
        uploadItem.queueId = queueId;
        return uploadItem;
    }

    function initFileUploaders(){
        $('.uploadImageField').each(function(){
            var fileInput = $(this).closest('li').find('.uploadImageField');

            fileInput.damnUploader({
                url: '{$config->root_url}{$module->url}',
                fieldName:  'uploaded-images',
                object_id: $(this).closest('li').attr('data-id'),
                limit: 1,
                onSelect: function(file) {
                    var addedItem = addFileToQueue(file, fileInput);
                    fileInput.damnUploader('startUploadItem', addedItem.queueId);
                    return false;
                },
                onAllComplete: function(){
                    fileInput.damnUploader('cancelAll');
                    href = '{$config->root_url}{$module->url}?id='+$(this).closest('li').attr('data-id')+'&mode=get_images&ajax=1';
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            fileInput.closest('li').find('div.dd3-content div.form-group:first').append('<img src="'+data.data[0]+'" class="slide"/>');
                            fileInput.closest('li').find('div.dd3-content div.controls').remove();
                        }
                    });
                    return false;
                },
            });
        });
    }

    $(document).ready(function(){
        $('#new_menu').nestable({
            maxDepth: 1
        });

        {if $message_success}info_box($('#notice'), 'Настройки сохранены!', '');{/if}

        {if $message_error}error_box($('#notice'), 'Ошибка при сохранении!', '');{/if}

        initFileUploaders();
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $("form a.add").click(function(){
        $.ajax({
            type: 'GET',
            url: "{$config->root_url}{$module->url}{url add=['mode'=>'add_slide', 'ajax'=>1]}",
            success: function(data) {
                if (data.success){
                    $('#new_menu ol.dd-list').append('<li class="dd-item dd3-item" data-id="'+data.data+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="form-group"></div><div class="form-group controls"><span class="btn btn-file btn-default"><i class="fa fa-plus"></i> <span>Загрузить изображение</span><input class="uploadImageField" type="file" multiple="multiple" name="uploaded-images"></span></div> <div class="form-group"><div class="input-group"><span class="input-group-addon">Ссылка:</span><input type="text" class="form-control" name="url['+data.data+']" value=""/></div></div></div><div class="controllinks"><a class="delete-item" href="{$config->root_url}{$module->url}?id='+data.data+'&mode=delete&ajax=1"><i class="fa fa-times"></i></a><a class="toggle-item light-on" href="{$config->root_url}{$module->url}?id='+data.data+'&mode=toggle&ajax=1"></a></div><input type="hidden" name="position[]" value="'+data.id+'"/></li>');

                    $('#new_menu').nestable({
                        maxDepth: 1
                    });
                    initFileUploaders();
                }
            }
        });

        return false;
    });

    $('#new_menu').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_slideshow(obj);
        });
        return false;
    });

    function delete_slideshow(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                li.remove();
            return false;
        });
        return false;
    }

    $('#new_menu').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
            return false;
        });
        return false;
    });

</script>