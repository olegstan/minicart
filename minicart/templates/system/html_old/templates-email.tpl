{$meta_title = "E-mail" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    </div>

    <!-- Параметры -->
    <div class="row">
        <div class="col-md-8">
            <legend>
                {if $template->url == "registration"}Письмо о регистрации пользователя
                {elseif $template->url == "reset-password"}Письмо о сбросе пароля
                {elseif $template->url == "new-order"}Письмо о поступлении заказа
                {elseif $template->url == "change-order-status"}Письмо о смене статуса заказа
                {elseif $template->url == "change-order-status-admin"}Письмо о смене статуса заказа
                {elseif $template->url == "notification-product-available"}Письмо о появление товара
                {elseif $template->url == "new-order-admin"}Письмо о новом заказе
                {elseif $template->url == "callback"}Письмо заказе звонка
                {elseif $template->url == "user-question"}Письмо о вопросе от пользователя
                {elseif $template->url == "cancel-order"}Письмо об отмене заказа
                {elseif $template->url == "order-paid"}Письмо об оплате заказа
                {elseif $template->url == "order-paid-admin"}Письмо об оплате заказа
                {elseif $template->url == "paid-accepted"}Письмо о разрешении оплаты{/if}
             <div class="email-onoff">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-default4 btn-xs on {if $template->enabled}active{/if}">
                                    <input type="radio" {if $template->enabled}checked{/if} value="1" name="enabled">Отправляем
                                </label>
                                <label class="btn btn-default4 btn-xs off {if !$template->enabled}active{/if}">
                                    <input type="radio"{if !$template->enabled}checked{/if} value="0" name="enabled">Не отправляем
                                </label>
                            </div>
                        </div></legend>

            <div class="form-group">
                <label>Тема письма</label>
                <input type="text" name="message_header" class="form-control" value="{$template->message_header|escape_string}">
            </div>
            <div class="form-group">
                <label>Текст сообщения</label>
                <textarea class="form-control ckeditor" name="message_text" rows="15">{$template->message_text}</textarea>
            </div>
        </div>
        <div class="col-md-4">
            <legend>Письма пользователю</legend>
            <div class="list-group">
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'registration']}" class="list-group-item {if $template->url == 'registration'}active{/if}" data-title="Письмо о регистрации пользователя">Регистрация пользователя</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'reset-password']}" class="list-group-item {if $template->url == 'reset-password'}active{/if}" data-title="Письмо о сбросе пароля">Сброс пароля</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'new-order']}" class="list-group-item {if $template->url == 'new-order'}active{/if}"  data-title="Письмо о поступлении заказа">Новый заказ</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'change-order-status']}" class="list-group-item {if $template->url == 'change-order-status'}active{/if}"  data-title="Письмо о смене статуса заказа">Смена статуса заказа</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'notification-product-available']}" class="list-group-item {if $template->url == 'notification-product-available'}active{/if}"  data-title="Письмо о появление товара">Уведомление о появлении товара</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'cancel-order']}" class="list-group-item {if $template->url == 'cancel-order'}active{/if}"  data-title="Письмо об отмене заказа">Отмена заказа</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'order-paid']}" class="list-group-item {if $template->url == 'order-paid'}active{/if}"  data-title="Письмо об оплате заказа">Оплата заказа</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'paid-accepted']}" class="list-group-item {if $template->url == 'paid-accepted'}active{/if}"  data-title="Письмо о разрешении оплаты">Разрешение оплаты</a>
            </div>
            <legend>Письма администратору</legend>
            <div class="list-group">
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'new-order-admin']}" class="list-group-item {if $template->url == 'new-order-admin'}active{/if}"  data-title="Письмо о новом заказе">Новый заказ</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'change-order-status-admin']}" class="list-group-item {if $template->url == 'change-order-status-admin'}active{/if}"  data-title="Письмо о смене статуса заказа">Смена статуса заказа</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'callback']}" class="list-group-item {if $template->url == 'callback'}active{/if}"  data-title="Письмо заказе звонка">Заказ звонка</a> 
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'user-question']}" class="list-group-item {if $template->url == 'user-question'}active{/if}"  data-title="Письмо о вопросе от пользователя">Вопрос от пользователя</a>
              <a href="{$config->root_url}{$module->url}{url add=['template'=>'order-paid-admin']}" class="list-group-item {if $template->url == 'order-paid-admin'}active{/if}"  data-title="Письмо об оплате заказа">Оплата заказа</a>
            </div>
            
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title">Переменные</h3></div>
            <div class="panel-body">
                <table class="table table-condensed noboldth">
                <thead>
                  <tr>
                    <th>Переменная</th>
                    <th>Занчение</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><code>$site_url$</code></td>
                        <td>URL магазина</td>
                    </tr>
                    <tr>
                        <td><code>$shop_name$</code></td>
                        <td>Название магазина</td>
                    </tr>
                    <tr>
                        <td><code>$id$</code></td>
                        <td>Номер заказа</td>
                    </tr>
                    <tr>
                        <td><code>$name$</code></td>
                        <td>Имя пользователя</td>
                    </tr>
                    <tr>
                        <td><code>$mail$</code></td>
                        <td>Почта пользователя</td>
                    </tr>
                    <tr>
                        <td><code>$phone$</code></td>
                        <td>Телефон пользователя</td>
                    </tr>
                    <tr>
                        <td><code>$phone2$</code></td>
                        <td>Телефон получателя</td>
                    </tr>
                    <tr>
                        <td><code>$delivery_method$</code></td>
                        <td>Способ доставки</td>
                    </tr>
                    <tr>
                        <td><code>$address$</code></td>
                        <td>Адрес доставки</td>
                    </tr>
                    <tr>
                        <td><code>$delivery_price$</code></td>
                        <td>Стоимость доставки</td>
                    </tr>
                    <tr>
                        <td><code>$order_link$</code></td>
                        <td>Ссылка на заказ</td>
                    </tr>
                    <tr>
                        <td><code>$order_sum$</code></td>
                        <td>Сумма заказа</td>
                    </tr>
                    <tr>
                        <td><code>$order_discount$</code></td>
                        <td>Скидка, %</td>
                    </tr>
                    <tr>
                        <td><code>$order_discount_sum$</code></td>
                        <td>Скидка, {$main_currency->sign}</td>
                    </tr>
                    <tr>
                        <td><code>$order_status$</code></td>
                        <td>Статус заказа</td>
                    </tr>
                    <tr>
                        <td><code>$order_date$</code></td>
                        <td>Дата заказа</td>
                    </tr>
                    <tr>
                        <td><code>$order_time$</code></td>
                        <td>Время заказа</td>
                    </tr>
                    <tr>
                        <td><code>$products$</code></td>
                        <td>Заказанные товары</td>
                    </tr>
                    <tr>
                        <td><code>$order_comment$</code></td>
                        <td>Комментарий к заказу</td>
                    </tr>
                    <tr>
                        <td><code>$call_id$</code></td>
                        <td>Номер заявки на заказ звонка</td>
                    </tr>
                    <tr>
                        <td><code>$call_time$</code></td>
                        <td>Когда удобно позвонить (Заказ звонка)</td>
                    </tr>
                    <tr>
                        <td><code>$call_message$</code></td>
                        <td>Сообщение (Заказ звонка)</td>
                    </tr>
                </tbody>
              </table>
              
            </div>
        </div>

        </div>
    </div>

    <input type="hidden" name="id" value="{$template->id}"/>
    <input type="hidden" name="url" value="{$template->url}"/>
    <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
</form>
<!-- Основная форма (The End) -->

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>