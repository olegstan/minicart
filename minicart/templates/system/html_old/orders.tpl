{* Title *}
{$meta_title='Заказы' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags_values">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            <div class="currentfilter">

                    <div class="input-group">
                        <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                        <span class="input-group-btn">
                        {if $keyword}
                        <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                        {/if}
                        <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                        </span>
                        

                </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['status_id'=>$params_arr['status_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
        <div class="col-md-9">
            <legend>
            {foreach $orders_statuses as $status}{if array_key_exists('status_id', $params_arr) && $params_arr['status_id']==$status->id}{$status->name}{/if}{/foreach}
            </legend>
            <div id="new_menu" class="dd orderslist">
                {if $orders}
                
                {include file='pagination.tpl'}
                <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $orders as $order}
                            <li class="dd-item dd3-item {if !$order->moderated}neworder{/if}" data-id="{$order->id}">
                                <div class="dd3-content">
                                    <input type="checkbox" value="{$order->id}" class="checkb">
                                    <div id="orderblock">
                                        <div class="time">{$order->day_str}</div>
                                        <div class="orderbody">

                                            <a class="accordion-toggle composition" data-toggle="collapse" data-parent="#accordion{$order->id}" href="#collapse{$order->id}" title="Показать состав заказа"><i class="fa fa-plus"></i> <i class="fa fa-spinner fa fa-spin fa fa-large spinner" style="display:none;"></i></a>
                                            
                                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$order->id, 'status_id'=>$params_arr['status_id']]}"{/if} class="order-numder">Заказ №{$order->id}</a>
                                            <div class="order-user">{$order->name|escape_string|truncate:24:"...":true} </div>
                                        {if $order->user}
                                        {*
                                        <span class="label label-default label-user">{$order->user->name|escape_string}</span>
                                        *}
                                        {/if}



                                        <div class="order-list-summ">{$order->total_price|convert} {$main_currency->sign}</div>
                                        
                                        
                                        <div class="order-status label-{$order->status->css_class}">
                                            {if $order->status}{$order->status->group_name}{/if}
                                        </div>
                                        
                                        {if $order->payment_method && !$order->payment_method->allow_payment}
                                        <div class="payment-badges">
                                            {if !$order->allow_payment}
                                            <a data-toggle="tooltip" data-placement="bottom" title="Ожидает разрешения оплаты" class="order-badge money-wait"></a>
                                            {/if}
                                            {if $order->allow_payment && !$order->paid}
                                            <a data-toggle="tooltip" data-placement="bottom" title="Ожидает оплаты" class="order-badge money-waitpay"></a>
                                            {/if}
                                            {if $order->allow_payment && $order->paid}
                                            <a data-toggle="tooltip" data-placement="bottom" title="Заказ оплачен" class="order-badge money-ok"></a>
                                            {/if}
                                        </div>
                                        {/if}
                                                                                
                                        </div>
                                        
                                        {if $allow_edit_module}
                                        <div class="controllinks">
                                            <a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$order->id, 'mode'=>'delete', 'ajax'=>1]}/"><i class="fa fa-times"></i></a>
                                        </div>
                                        {/if}
                                    </div>

                                    {if $order->note}
                                    <div class="notes">
                                        {$order->note|escape}
                                    </div>
                                    {/if}
                                    
                                    {if $order->buy1click}
                                    <div class="oneclick"><i class="fa fa-location-arrow"></i> Заказ в 1 клик</div>
                                    {/if}

                                    <div class="panel-group" id="accordion{$order->id}">
                                        <div id="collapse{$order->id}" class="panel-collapse whitepanel collapse">

                                        </div>
                                    </div>
                                    
                                    
                                    
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    </div>
                    {include file='pagination.tpl'}
                {else}
                    <p>Нет заказов</p>
                {/if}
            </div>
            {* Массовые операции для товаров *}

                {if $allow_edit_module}
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все &uarr;</div>
                    <select id="mass_operation" class="form-control">
                        <option value='1'>Изменить статус</option>
                        <option value='2'>Удалить</option>
                    </select>
                    <select id="mass_operation_change_status" class="form-control" {*style="display:none;"*}>
                        {foreach $all_orders_statuses as $status}
                            <option value="{$status->id}">{$status->name}</option>
                        {/foreach}
                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
                {/if}
        </div>
        
        <div class="col-md-3">
        <div class="list-group ordergroups">
            <a href="{$config->root_url}{$module->url}" class="list-group-item {if !array_key_exists('status_id', $params_arr)}active{/if}">
                <span class="badge">{$all_orders_count}</span>
                Все
            </a>
            {foreach $orders_statuses as $status}
                <a href="{$config->root_url}{$module->url}{url add=['status_id'=>$status->id]}" class="list-group-item {if array_key_exists('status_id', $params_arr) && $params_arr['status_id']==$status->id}active {/if} {$status->css_class}">
                    <span class="badge">{$status->orders_count}</span>
                    {$status->name|escape}
                </a>
            {/foreach}
         </div>
         </div>
    </form>
</div>
</div>
<script type="text/javascript">

    $('#new_menu a.accordion-toggle').click(function(){
        var order_id = $(this).closest('li').attr('data-id');
        var div_collapse = $(this).closest('li').find('div.collapse');
        var spinner = $(this).closest('li').find('i.spinner');
        if (div_collapse.length > 0)
        {
            var ol = div_collapse.find('ol.product-list');
            if (ol.length == 0)
            {
                spinner.show();
                $.ajax({
                    {*type: 'GET',
                    url: "{$config->root_url}{$edit_module->url}?mode=get_purchases&ajax=1&id="+order_id,*}
                    type: 'POST',
                    url: '{$config->root_url}/ajax/get_data.php',
                    data: {
                        'object': 'orders',
                        'mode': 'get_purchases',
                        'id': order_id,
                        'session_id': '{$smarty.session.id}'
                    },
                    success: function(data) {
                        if (data.success)
                        {
                            div_collapse.html('');
                            var ol = $('<ol class="order-list"></ol>');
                            for(var key in data.data)
                            {
                                var row = data.data[key];
                                ol.append('<li><div class="order-list-image">'+(row.image?'<img src="'+row.image+'"/>':'')+'</div><div class="order-list-productname">'+(row.product_id>0?'<a href="{$config->root_url}{$product_module->url}?id='+row.product_id+'">'+row.product_name+'</a>':'<span>'+row.product_name+'</span>')+'<span>'+(row.variant_name?' '+row.variant_name+'':'')+'</span><span class="product-sky">'+row.sku+'</span><span class="product-id">'+row.product_id+'</span></div><div class="order-list-right"><div class="order-list-count">'+row.amount+' шт.</div><div class="order-list-price">'+row.format_price+' руб.</div></div></li>');
                            }
                            div_collapse.append(ol);
                            spinner.hide();
                        }
                    }
                });
            }
        }
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val() + '/');
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val() + '/');
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $(document).ready(function(){
        {if $orders}
            $('#new_menu').nestable({
                maxDepth: 1
            });
        {/if}
    });

    $('a.save').click(function(){
        var obj = $(this).closest('form').find('div.controlgroup');
        var _menu = $('#new_menu').nestable('serialize');
        $.ajax({
            type: 'POST',
            url: $(this).closest('form').attr('action'),
            data: {
                menu: _menu},
            error: function() {
                error_box(obj, 'Изменения не сохранены!', '');
            },
            success: function(data) {
                info_box(obj, 'Изменения сохранены!', '');
            }
        });
        return false;
    });

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_order_item(obj);
        });
        return false;
    });

    function delete_order_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#mass_operation').change(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        if (operation == 1)
            $('#mass_operation_change_status').show();
        else
            $('#mass_operation_change_status').hide();
        return false;
    });
    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (operation == 2 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#new_menu input[type=checkbox]:checked").each(function(index, value){
            var item = $(this).closest('li');
            switch(operation){
                case 1: //Изменить статус
                    if ($('#mass_operation_change_status option:selected').val() > 0)
                    {
                        $.ajax({
                            type: 'POST',
                            url: '{$config->root_url}/ajax/get_data.php',
                            data: {
                                'object': 'orders',
                                'mode': 'change_status',
                                'id': item.attr('data-id'),
                                'status_id': $('#mass_operation_change_status option:selected').val(),
                                'session_id': '{$smarty.session.id}'
                            },
                            success: function(data) {
                                if (data.success)
                                {
                                    items_count--;
                                    if (items_count == 0)
                                        location.reload();
                                    return false;
                                }
                            }
                        });
                    }
                    break;
                case 2:    //Удаление
                    var subitem = item.find('a.delete-item').first();
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                                location.reload();
                        }
                    });
                    break;
            }
        });
        return false;
    });
</script>
