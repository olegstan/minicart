{* Title *}
{$meta_title='Редактирование свойства' scope=parent}

<form method=post id="tag">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url add=['mode'=>$return_mode, 'page'=>$page]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

     <div class="row">
        <div class="col-md-8">
        <legend>{if $tag_group}Редактирование свойства{else}Добавление свойства{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Название свойства</label>
        <div class="input-group">
        <input type="text" class="form-control" name="name" placeholder="Введите наименование группы тега" value="{$tag_group->name|escape_string}" {if $tag_group->is_auto}disabled{/if}/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id группы тегов">id:{$tag_group->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $tag_group->enabled || !$tag_group}active{/if} {if $tag_group->is_auto}disabled{/if}">
                        <input type="radio" name="enabled" id="option1" value=1 {if $tag_group->enabled || !$tag_group}checked{/if} {*if $tag_group->is_auto}disabled{/if*}/>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $tag_group && !$tag_group->enabled}active{/if} {if $tag_group->is_auto}disabled{/if}">
                        <input type="radio" name="enabled" id="option2" value=0 {if $tag_group && !$tag_group->enabled}checked{/if} {*if $tag_group->is_auto}disabled{/if*}/> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <label>Префикс свойства <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если префикс указан то при фильтрации свойство будет отображаться в виде 'Ширина: 80 см', если же префикс не указан будет '80 см'"><i class="fa fa-info-circle"></i></a></label>
                <input type="text" class="form-control" name="prefix" value="{$tag_group->prefix|escape_string}"/>
                <p class="help-block">Например "Высота" или "Ширина"</p>
            </div>
            <div class="col-md-6">
                <label>Постфикс свойства <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Постфикс отображается при фильтрации, если он указан то будет писаться 'Ширина: 80 см', если нет то 'Ширина: 20'. Если свойство выгружается в яндекс-маркет то постфикс выгружается в теге unit"><i class="fa fa-info-circle"></i></a></label>
                <input type="text" class="form-control" name="postfix" value="{$tag_group->postfix|escape_string}"/>
                <p class="help-block">Еденица измерения свойства: "см" или "кг."</p>
            </div>
        </div>
        
        <legend>Вид фильтра</legend>


<div class="taggroupselect">
    <div class="taggroupselect-inline">
                <div class="taggroup-method {if !$tag_group || $tag_group->mode == "select"}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="select" {if !$tag_group || $tag_group->mode == "select"}checked{/if}/>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/list.png"></div>
                        <div class="taggroup-header">Выпадающий список</div>
                        <div class="taggroup-description">Позволяющий выбрать 1 значение из списка.</div>
                        
                    </label>
                </div>
                </div>
                <div class="taggroup-method {if $tag_group->mode == "checkbox"}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="checkbox" {if $tag_group->mode == "checkbox"}checked{/if}>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/checkbox.png"></div>
                        <div class="taggroup-header">Галочки (Чекбоксы)</div>
                        <div class="taggroup-description">Позволяют выбрать несколько значений из предложенных.</div>
                        
                    </label>
                </div>
                </div>
                <div class="taggroup-method {if $tag_group->mode == "radio"}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="radio" {if $tag_group->mode == "radio"}checked{/if}>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/radio.png"></div>
                        <div class="taggroup-header">Радиобаттон</div>
                        <div class="taggroup-description">Позволяет выбрать 1 значение из предложенных.</div>
                        
                    </label>
                </div>
                </div>
                <div class="taggroup-method {if $tag_group->mode == "range"}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="range" {if $tag_group->mode == "range"}checked{/if}>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/interval.png"></div>
                        <div class="taggroup-header">Диапазонный фильтр</div>
                        <div class="taggroup-description">Позволяет указать диапазон из предложенных численных значений.</div>
                        
                    </label>
                </div>
                </div>

                <div class="taggroup-method {if $tag_group->mode == "logical"}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="logical" {if $tag_group->mode == "logical"}checked{/if}>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/logic.png"></div>
                        <div class="taggroup-header">Логический</div>
                        <div class="taggroup-description">Либо это значение есть либо нет. Отображается в виде одной галочки.</div>
                        
                    </label>
                </div>
                </div>
                
                <div class="taggroup-method {if $tag_group->mode == ""}active{/if}">
                <div class="radio">
                    <label>
                        <input type="radio" name="mode" value="text" {if $tag_group->mode == "text"}checked{/if}>
                        <div class="taggroup-image"><img src="{$path_admin_template}/img/properties/text.png"></div>
                        <div class="taggroup-header">Текст</div>
                        <div class="taggroup-description">Просто текст, данное свойство не учавствует в фильтрации.</div>
                        
                    </label>
                </div>
                </div>

        </div>

</div>
        <hr/>
        <div class="form-group">
        <label class="bigname">Помощь свойству <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Показывается при наведении на знак вопроса после названия группы тегов"><i class="fa fa-info-circle"></i></a></label>
        <div class="form-group">
        <textarea class="form-control ckeditor" rows="5" name="help_text" id="help_text">{$tag_group->help_text}</textarea>
        </div>



        </div>
</div>

    <div class="col-md-4">
        <legend>Настройки свойства</legend>

        <div class="form-group">
            <label>Показывать в товаре <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Показывать ли свойство и его значения в таблице в карточке товара"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default4 on {if !$tag_group || $tag_group->show_in_frontend}active{/if}">
                <input type="radio" name="show_in_frontend" value="1" {if !$tag_group || $tag_group->show_in_frontend}checked{/if}> Да
              </label>
              <label class="btn btn-default4 off {if $tag_group && !$tag_group->show_in_frontend}active{/if}">
                <input type="radio" name="show_in_frontend" value="0" {if $tag_group && !$tag_group->show_in_frontend}checked{/if}> Нет
              </label>
              </div>
        </div>
        
        <hr/>
        <label>Сортировка значений свойства</label>

        <div class="form-group">
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По порядку (по умолчанию)
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По алфавиту A-Z
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По алфавиту Z-A
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По возрастанию 1-9 (для числовых значений)
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По убыванию 9-1 (для числовых значений)
              </label>
            </div>
            <div class="radio">
              <label>
                <input type="radio" name="optionsRadios" id="" value="" checked>
                По популярности значений
              </label>
            </div>
</div>
        <hr/>
        
        
        <div class="form-group">
            <label>Показывать в списке товаров</label>
            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default4 on {if $tag_group && $tag_group->show_in_product_list}active{/if}">
                <input type="radio" name="show_in_product_list" value="1" {if $tag_group && $tag_group->show_in_product_list}checked{/if}> Да
              </label>
              <label class="btn btn-default4 off {if !$tag_group || !$tag_group->show_in_product_list}active{/if}">
                <input type="radio" name="show_in_product_list" value="0" {if !$tag_group || !$tag_group->show_in_product_list}checked{/if}> Нет
              </label>
              </div>
        </div>


        <div class="form-group">
            <label>Свойство содержит только числовые значения <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если свойство содержит только числовые значения (например Длинна) то значения этого свойства в фильтре будут сортироваться по возрастанию, а не по алфавиту как у нечисловых свойств"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default4 on {if $tag_group->numeric_sort}active{/if}">
                <input type="radio" name="numeric_sort" value="1" {if $tag_group->numeric_sort}checked{/if}/> Да
              </label>
              <label class="btn btn-default4 off {if !$tag_group || !$tag_group->numeric_sort}active{/if}">
                <input type="radio" name="numeric_sort" value="0" {if !$tag_group || !$tag_group->numeric_sort}checked{/if}/> Нет
              </label>
              </div>
            
          </div>
          
            
            <div class="form-group">
            <label>Показывать префикс свойства при отображении в виде тега в результатах фильтрации <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="В результатах фильтрации свойство будет отображено вместе с префиксом, например: Высота: 11 см, где слово Высота - будет префиксом"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default4 on {if !$tag_group || $tag_group->show_prefix_in_frontend_filter}active{/if}">
                <input type="radio" name="show_prefix_in_frontend_filter" value="1" {if !$tag_group || $tag_group->show_prefix_in_frontend_filter}checked{/if}/> Да
              </label>
              <label class="btn btn-default4 off {if $tag_group && !$tag_group->show_prefix_in_frontend_filter}active{/if}">
                <input type="radio" name="show_prefix_in_frontend_filter" value="0" {if $tag_group && !$tag_group->show_prefix_in_frontend_filter}checked{/if}/> Нет
              </label>
              </div>
        </div>
        
        <div id="diapason-section" {if $tag_group->mode != "range"}style="display: none;"{/if}>
        <div class="form-group">
            <label>Шаг диапазонного фильтра</label>
            <input type="text" class="form-control" name="diapason_step" placeholder="" value="{if $tag_group->id}{$tag_group->diapason_step}{else}1{/if}">
        </div>
        <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#help">
                     Подробнее про шаг фильтра
                    </a>
                  </h4>
                </div>
                <div id="help" class="panel-collapse collapse">
                  <div class="panel-body">
            <p>По умолчанию 1, например у цены шаг рекомендуется шаг=100, иногда также нужен шаг в 1000, 0.5  или 0.01, длинну шага указывайте либо в виде целых значений либо через точку</p>
            <p>Шаг также используется для определения левой и правой границ фильтра, если, например, шаг=100, в этом случае правая и левая границы фильтра будут округлены до 100 в большую и меньшую сторону соотвественно</p>
                  </div>
                </div>
              </div>

    </div>

    </div>
    
    
    <div class="word-picture">
        <legend>Настройки отображения в фильтре</legend>
        <div class="form-group">
            <label>Отображение свойства</label>
            <div class="btn-group  noinline" data-toggle="buttons">
              <label class="btn btn-default active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked>Текст
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option2" autocomplete="off">Картинка
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option3" autocomplete="off">Цвет
              </label>
            </div>
        </div>
              
        <div class="form-group">
            <label>Css-класс блока</label>
            <input type="text" class="form-control" name="" value="">
        </div>
       
       <div class="form-group">
            <label>На сколько колонок разбить содержимое</label>
            <div class="btn-group  noinline" data-toggle="buttons">
              <label class="btn btn-default active">
                <input type="radio" name="options" id="option1" autocomplete="off" checked>1
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option2" autocomplete="off">2
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option3" autocomplete="off">3
              </label>
              <label class="btn btn-default">
                <input type="radio" name="options" id="option3" autocomplete="off">4
              </label>
            </div>
        </div>
        
        
    </div>
    
    
    <legend>Выгрузка в Яндекс-маркет</legend>
    <div class="form-group">
            <label>Выгружать свойство в файл выгрузки для Яндекс-Маркета <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Свойство будет выгружено в виде тега param, например: <param name='Вес' unit='кг'>13.8</param>"><i class="fa fa-info-circle"></i></a></label>

            <div class="btn-group noinline" data-toggle="buttons">
              <label class="btn btn-default4 on {if $tag_group->export2yandex}active{/if}">
                <input type="radio" name="export2yandex" value="1" {if $tag_group->export2yandex}checked{/if}/> Да
              </label>
              <label class="btn btn-default4 off {if !$tag_group->export2yandex}active{/if}">
                <input type="radio" name="export2yandex" value="0" {if !$tag_group->export2yandex}checked{/if}/> Нет
              </label>
              </div>
        </div>

        <input type="hidden" name="id" value="{$tag_group->id}"/>
        {if $return_mode}<input type="hidden" name="return_mode" value="{$return_mode}"/>{/if}
        {if $page}<input type="hidden" name="page" value="{$page}"/>{/if}
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>
        </div>




       </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
        error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название свойства не может быть пустым{/if}')
        {/if}
    });

    $('.taggroupselect input').click(function(){
        $('.taggroupselect div.taggroup-method.active').removeClass('active');
        $(this).closest('div.taggroup-method').addClass('active');
        if ($(this).val() == "range")
        {
            $('input[name=diapason_step]').val(100);
            $('#diapason-section').show();
        }
        else
            $('#diapason-section').hide();
    });

    $("form#tag a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>