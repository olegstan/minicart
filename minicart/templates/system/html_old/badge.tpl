{* Title *}
{$meta_title='Редактирование бейджа' scope=parent}

<form method=post id="badge">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

     <div class="row">
        <div class="col-md-8">
        <legend>{if $badge->id}Редактирование бейджа{else}Добавление бейджа{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Название бейджа</label>
        <div class="input-group">
        <input type="text" class="form-control" name="name" placeholder="Введите наименование бейджа" value="{$badge->name|escape_string}"/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id бейджа">id:{$badge->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $badge->is_visible || !$badge}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $badge->is_visible || !$badge}checked{/if}> Активен
                      </label>
                      <label class="btn btn-default4 off {if $badge && !$badge->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $badge && !$badge->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        

        

        </div>

        <div class="form-group">
            <label>CSS-cтиль по умолчанию</label>
            <input type="text" name="css_class" class="form-control" value="{if $badge->css_class}{$badge->css_class}{else}label label-default{/if}">
        </div>
        <div class="form-group">
        <label>Примеры:</label>


        <table class="table">
            <thead>
                <tr>
                      <th width="150">Пример бейджа</th>
                      <th>Css-класс</th>
                </tr>
              </thead>
            <tbody>
            <tr>
                <td><span class="label label-default">Хит продаж</span></td>
                <td>label label-default</td>
            </tr>
            <tr>
                <td><span class="label label-primary">Рекомендуем</span></td>
                <td>label label-primary</td>
            </tr>
            <tr>
                <td><span class="label label-success">Новинка!</span></td>
                <td>label label-success</td>
            </tr>
            <tr>
                <td><span class="label label-info">Товар дня!</span></td>
                <td>label label-info</td>
            </tr>
            <tr>
                <td><span class="label label-warning">Только у нас</span></td>
                <td>label label-warning</td>
            </tr>
            <tr>
                <td><span class="label label-danger">Распродажа!</span></td>
                <td>label label-danger</td>
            </tr>
            </tbody>
        </table>


            

        </div>

        </div>
        <div class="col-md-4">
        <legend>Настройки отображения</legend>
        <label>Css-класс накладываемый на товар <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Этот класс будет указан у товара в списке товаров и у товара в карточке товара" class="fa fa-legend"><i class="fa fa-info-circle"></i></a></label>
        <input type="text" name="css_class_product" class="form-control" value="{$badge->css_class_product}"/>

        </div>
       </div>
    
    <input type="hidden" name="id" value="{$badge->id}"/>
    <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
    <input type="hidden" name="close_after_save" value="0"/>
    <input type="hidden" name="add_after_save" value="0"/>
    
    </fieldset>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название бейджа не может быть пустым{/if}')
        {/if}
    });

    $("form#badge a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#badge a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#badge a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>