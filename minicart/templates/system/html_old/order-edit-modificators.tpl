<div class="product-modifier">
    {foreach $modificators_groups as $gr}
        {if $gr->name}
            <div class="product-modifier-group">
            <label class="modifier-group-header">{$gr->name}</label>
        {/if}

        {if $gr->type == 'checkbox'}
        <div class="modifier-checkbox">
            {foreach $gr->modificators as $m}

                {$checked = false}
                {$count = 1}

                {foreach $order->modificators as $om}
                    {if $m->id == $om->modificator_id}
                        {$checked = true}
                        {$count = $om->modificator_amount}
                    {/if}
                {/foreach}

                <div class="checkbox">
                    <label>
                    <input type="checkbox" value="{$m->id}" name="modificators[{$gr->id}]" data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}" {if $checked}checked{/if}><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators-orders':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators-orders':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}
                    </label>

                    {if $m->multi_buy}
                    <div class="modifier-counter">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
                        </span>
                        <input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$count}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
                        <span class="input-group-btn">
                            <button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                    </div>

                    {/if}
                </div>
            {/foreach}
            </div>
        {/if}

        {if $gr->type == 'radio'}
            {foreach $gr->modificators as $m}
                <div class="radio">
                    <label>
                    <input type="radio" name="modificators[{$gr->id}]" value="{$m->id}" {if $m@first}checked{/if} data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"><div class="product-modifier-img">{if $m->image}<a href="{$m->image->filename|resize:'modificators-orders':1100:800}" class="fancybox" rel="group_modificators"><img src="{$m->image->filename|resize:'modificators-orders':40:40}" /></a>{/if}</div> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if} {if $m->description}<a class="property-help" data-type="qtip" data-content="{$m->description|escape}" data-title="{$m->name}"><i class="fa fa-info-circle"></i></a>{/if}
                    </label>

                    {if $m->multi_buy}
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button data-type="minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
                        </span>
                        <input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$m->multi_buy_min}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
                        <span class="input-group-btn">
                            <button data-type="plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                    {/if}
                </div>
            {/foreach}
        {/if}

        {if $gr->type == 'select'}
            <div class="modifier-select">
            <select name="modificators[{$gr->id}]" class="form-control">
                <option>�� �������</option>
                {foreach $gr->modificators as $m}
                    <option value="{$m->id}" data-type="{$m->type}" data-value="{$m->value}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}">
                    {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
                    </option>
                {/foreach}
            </select>
            </div>
        {/if}

        {if $gr->name}
            </div>
        {/if}
    {/foreach}
</div>