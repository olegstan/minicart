{* Title *}
{$meta_title='Свойства' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}<button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>{/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>              
                
                  </span>
            </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['return_mode'=>$mode]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Свойства</legend>

                <div id="sortview">
                <div class="itemcount">Свойств: {$groups_count}</div>
                <ul class="nav nav-pills" id="sort">
                   <li class="disabled">Сортировать по:</li>
                <li><a class="btn btn-link btn-small current"><span>По порядку</span></a></li>
                </ul>
                </div>

                <div id="new_menu" class="dd">
                    {if $tags_groups}
                    
                    {include file='pagination.tpl'}
                      <div class="itemslist">
                        <ol class="dd-list">
                            {foreach $tags_groups as $tag}
                                {if $tag->mode == "select"}{$tag_mode = "Выпадающий список"}{/if}
                                {if $tag->mode == "checkbox"}{$tag_mode = "Галочки (Чекбоксы)"}{/if}
                                {if $tag->mode == "radio"}{$tag_mode = "Радиобаттон"}{/if}
                                {if $tag->mode == "range"}{$tag_mode = "Диапазонный фильтр"}{/if}
                                {if $tag->mode == "logical"}{$tag_mode = "Логический"}{/if}
                                <li class="dd-item dd3-item" data-id="{$tag->id}">
                                    <div class="dd-handle dd3-handle"></div>
                                    <div class="dd3-content">
                                        <input type="checkbox" value="{$item->id}" class="checkb">
                                        <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'return_mode'=>$mode, 'page'=>$current_page_num]}"{/if}>{$tag->name|escape}{if $tag->postfix}, {$tag->postfix}{/if}</a>
                                        
                                        <div class="controllinks">                                               <code class="opcity50">{$tag->id}</code>
                                            {if $mode == 'user-properties'}
                                                {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                                <a class="toggle-item {if $tag->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                            {/if}
                                            <a class="flag-item {if $tag->show_in_frontend}flag-on{else}flag-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'mode'=>'flag', 'ajax'=>1]}" title="{if $tag->show_in_frontend}Свойство отображается в товаре{else}Свойство не отображается в товаре{/if}"></a>
                                            
                                            <a class="list-item {if $tag->show_in_product_list}list-item-on{else}list-item-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'mode'=>'list', 'ajax'=>1]}" title="{if $tag->show_in_product_list}Свойство отображается в списке товаров{else}Свойство не отображается в списке товаров{/if}"></a>
                                            
                                            <a class="market-item {if $tag->export2yandex}market-on{else}market-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag->id, 'mode'=>'market', 'ajax'=>1]}" title="{if $tag->export2yandex}Свойство выгружается в Яндекс-Маркет{else}Свойство не выгружается в Яндекс-Маркет{/if}"></a>
                                            
                                        </div>
                                        <div class="type">
                                            <span class="label label-default">{$tag_mode}</span>
                                        </div>
                                    </div>
                                </li>
                            {/foreach}
                        </ol>
                        </div>
                        {include file='pagination.tpl'}

                    {else}
                        <p>Нет свойств</p>
                    {/if}
                </div>
                
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все ↑</div>
                    <select id="mass_operation" class="form-control">
                        <option value="1">Отобразить свойство в товаре</option>
                        <option value="2">Скрыть свойство в товаре</option>
                        <option value="6">Отобразить свойства в списке товаров</option>
                        <option value="7">Не показывать свойства в списке товаров</option>
                        <option value="3">Сделать видимыми</option>
                        <option value="4">Скрыть</option>
                        <option value="5">Удалить</option>
                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
            </div>

            <div class="col-md-4">
                <div class="list-group">
                    <a href="{$config->root_url}{$module->url}{url add=['mode'=>'user-properties']}" class="list-group-item {if $mode=="user-properties" || !$mode}active{/if}">Пользовательские свойства</a>
                    <a href="{$config->root_url}{$module->url}{url add=['mode'=>'auto-properties']}" class="list-group-item {if $mode=="auto-properties"}active{/if}">Автоматические свойства</a>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), '{$message_success}', '');
        {/if}
    });

    {if $tags_groups}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_property_item(obj);
        });
        return false;
    });

    function delete_property_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('.flag-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('flag-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('flag-on');
                item.removeClass('flag-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('flag-on');
                    item.prop('title', 'Свойство отображается в товаре');
                }
                else{
                    item.addClass('flag-off');
                    item.prop('title', 'Свойство не отображается в товаре');
                }
            }
        });
        return false;
    });

    $('.list-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('list-item-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('list-item-on');
                item.removeClass('list-item-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('list-item-on');
                    item.prop('title', 'Свойство отображается в списке товаров');
                }
                else{
                    item.addClass('list-item-off');
                    item.prop('title', 'Свойство не отображается в списке товаров');
                }
            }
        });
        return false;
    });

    $('.market-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('market-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('market-on');
                item.removeClass('market-off');
                enabled = 1 - enabled;
                if (enabled){
                    item.addClass('market-on');
                    item.prop('title', 'Свойство выгружается в Яндекс-Маркет');
                }
                else{
                    item.addClass('market-off');
                    item.prop('title', 'Свойство не выгружается в Яндекс-Маркет');
                }
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).prop('checked', true);
        });
    });

    function finish_mass_operation(){
        $('#mass_operation :first').attr('selected', 'selected');
        $('#mass_operation').trigger('change');
        return false;
    }

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (items_count == 0)
            return false;

        var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
        l.start();
        l.setProgress( 0 );
        var interval = 1 / items_count;
        var ok_count = 0;

        // 1 - show_in_frontend
        // 2 - hide_in_frontend
        // 3 - show
        // 4 - hide
        // 5 - delete
        // 6 - show_in_product_list
        // 7 - hide_in_product_list


        if (operation == 1){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.flag-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('flag-on'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('flag-off');
                            subitem.addClass('flag-on');
                            subitem.prop('title', 'Свойство отображается в товаре');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.flag-item').first().removeClass('flag-off').removeClass('flag-on').addClass('flag-on');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 2){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.flag-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('flag-off'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('flag-on');
                            subitem.addClass('flag-off');
                            subitem.prop('title', 'Свойство не отображается в товаре');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.flag-item').first().removeClass('flag-off').removeClass('flag-on').addClass('flag-off');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 3){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.toggle-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('light-on'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('light-off');
                            subitem.addClass('light-on');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-on');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 4){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.toggle-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('light-off'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('light-on');
                            subitem.addClass('light-off');
                            subitem.prop('title', 'Свойство не отображается в товаре');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-off');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 5){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }

        if (operation == 6){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.list-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('list-item-on'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('list-item-off');
                            subitem.addClass('list-item-on');
                            subitem.prop('title', 'Свойство отображается в списке товаров');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.list-item').first().removeClass('list-item-off').removeClass('list-item-on').addClass('list-item-on');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        if (operation == 7){
            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.list-item').first();
                var href = subitem.attr('href');
                if (!subitem.hasClass('list-item-off'))
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            subitem.removeClass('list-item-on');
                            subitem.addClass('list-item-off');
                            subitem.prop('title', 'Свойство не отображается в товаре');
                            item.find('li.dd-item').each(function(){
                                $(this).find('a.list-item').first().removeClass('list-item-off').removeClass('list-item-on').addClass('list-item-off');
                            });
                            item.find('input[type=checkbox]').prop('checked', false);
                            return false;
                        }
                    });

                items_count--;
                ok_count++;
                l.setProgress( ok_count * interval );
                if (items_count == 0)
                {
                    info_box($('#notice'), 'Выполнено!', '');
                    l.stop();
                    finish_mass_operation();
                    return false;
                }
            });
        }

        return false;
    });
</script>
