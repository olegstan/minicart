<div class="modified-product">
    <div class="order-productimage">{if $purchase->image}<img src="{$purchase->image->filename|resize:products:40:40}" alt="{$purchase->image->name|escape}"/>{/if}</div>
    
    <div class="modified-product-price-order">                            
        <span id="modal-price" class="purchase-price" data-original-price="{if !empty($purchase->variant)}{$purchase->variant->price|convert:NULL:false}{else}{$purchase->price|convert:NULL:false}{/if}" data-price="{$purchase->price|convert:NULL:false}">-</span>&nbsp;{$main_currency->sign}
    </div>
    
    <div class="productname">
        <a {if $purchase->product_id}href="{$config->root_url}{$product_module->url}{url add=['id' => $purchase->product_id]}" target="_blank"{/if}>{$purchase->product_name|escape}</a>
    
        {if $purchase->variant_name}
        <div class="selectvariant">
            <span class="purchase-amount">{$purchase->amount}</span>
            <span class="fs12">{$settings->units}</span>
            <label class="variant-name">({$purchase->variant_name})</label>
        </div>
        {/if}
    
        {if $purchase->variant->sku}<div class="artikul">Артикул: <span class="sku_value">{$purchase->variant->sku}</span></div>{/if}
    </div>
</div>

{if $settings->catalog_use_variable_amount && $purchase->product->use_variable_amount}
<div class="row">
    <div class="form-group">
        <label for="lengthInput">{$settings->catalog_variable_amount_name}{if $settings->catalog_variable_amount_dimension}, {$settings->catalog_variable_amount_dimension}{/if}</label>
        <div class="input-group">
                <span class="input-group-btn">
                    <button data-type="minus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button>
                </span>
                
                <input type="text" class="form-control input-sm" placeholder="" 
                        value="{$purchase->var_amount|string_format:'%.1f'}" name="var_amount"
                        data-stock="999">
                        
                <span class="input-group-btn">
                    <button data-type="plus-var-amount" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button>
                </span>
            </div>
    </div>
</div>
{/if}

{if $purchase->category}
<div class="product-modifier">
    {foreach $modificators_groups as $gr}
        {if $gr->id == 0 || ($gr->id > 0 && in_array($gr->id, $purchase->category->modificators_groups))}

            {if $gr->name}
                <div class="product-modifier-group">
                <label class="modifier-group-header">{$gr->name}</label>
            {/if}

            {if $gr->type == 'checkbox'}
            <div class="modifier-checkbox">
                {foreach $gr->modificators as $m}
                    {if $gr->id > 0 || $gr->id == 0 && in_array($m->id, $purchase->category->modificators)}
                    <div class="checkbox">
                        <label>
                        {$checked = ""}
                        {foreach $purchase->modificators as $pm}
                            {if $pm->modificator_id == $m->id}{$checked = "checked"}{/if}
                        {/foreach}
                        <input type="checkbox" value="{$m->id}" name="modificators[{$gr->id}][]" {$checked} data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
                        </label>

                        {if $m->multi_buy}
                        <div class="modifier-counter">
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button data-type="modificator-minus" class="btn btn-default btn-sm" {if !$checked}disabled=""{/if} type="button"><i class="fa fa-minus"></i></button>
                            </span>
                            <input type="text" class="form-control input-sm" {if !$checked}disabled=""{/if} placeholder="" value="{$pm->modificator_amount}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
                            <span class="input-group-btn">
                                <button data-type="modificator-plus" class="btn btn-default btn-sm" {if !$checked}disabled=""{/if} type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                        </div>

                        {/if}
                    </div>
                    {/if}
                {/foreach}
                </div>
            {/if}

            {if $gr->type == 'radio'}
                {$prechecked = ""}
                {foreach $gr->modificators as $m}
                    {foreach $purchase->modificators as $pm}
                        {if $pm->modificator_id == $m->id}{$prechecked = "checked"}{/if}
                    {/foreach}
                {/foreach}

                {foreach $gr->modificators as $m}
                    <div class="radio">
                        {$checked = ""}
                        {foreach $purchase->modificators as $pm}
                            {if $pm->modificator_id == $m->id}{$checked = "checked"}{/if}
                        {/foreach}

                        <label>
                        <input type="radio" name="modificators[{$gr->id}]" value="{$m->id}" {if $m@first && empty($prechecked)}checked{/if} {$checked} data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}"> {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}</label>

                        {if $m->multi_buy}
                        <div class="input-group">
                            <span class="input-group-btn">
                                <button data-type="modificator-minus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-minus"></i></button>
                            </span>
                            <input type="text" class="form-control input-sm" disabled="" placeholder="" value="{$pm->modificator_amount}" name="modificators_count_{$m->id}" data-min="{$m->multi_buy_min}" data-max="{$m->multi_buy_max}">
                            <span class="input-group-btn">
                                <button data-type="modificator-plus" class="btn btn-default btn-sm" disabled="" type="button"><i class="fa fa-plus"></i></button>
                            </span>
                        </div>
                        {/if}
                    </div>
                {/foreach}
            {/if}

            {if $gr->type == 'select'}
                <div class="modifier-select">
                <select name="modificators[{$gr->id}]" class="form-control">
                    <option>Не выбрано</option>
                    {foreach $gr->modificators as $m}
                        {$selected = ""}
                        {foreach $purchase->modificators as $pm}
                            {if $pm->modificator_id == $m->id}{$selected = "selected"}{/if}
                        {/foreach}
                        <option value="{$m->id}" data-type="{$m->type}" data-value="{$m->value}" data-multi-apply="{$m->multi_apply}" data-multi-buy="{$m->multi_buy}" data-multi-buy-min="{$m->multi_buy_min}" data-multi-buy-max="{$m->multi_buy_max}" {$selected}>
                        {$m->name} {if $m->type == 'plus_fix_sum'}(+{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'minus_fix_sum'}(-{$m->value|convert} {$main_currency->sign}){elseif $m->type == 'plus_percent'}(+{$m->value|convert} %){elseif $m->type == 'minus_percent'}(-{$m->value|convert} %){/if}
                        </option>
                    {/foreach}
                </select>
                </div>
            {/if}

            {if $gr->name}
                </div>
            {/if}
        {/if}
    {/foreach}
</div>
{/if}

<input type="hidden" name="purchase_id" value="{$purchase->id}"/>

<script type="text/javascript">

    $('button[data-type=plus-var-amount]').click(function(){
        var input = $('input[name=var_amount]');
        var max_stock = input.data('stock');
        if (input.length > 0)
        {
            var value = parseFloat(input.val()) + {$purchase->product->step_amount};

            if (value > {$purchase->product->max_amount})
                value = {$purchase->product->max_amount};

            input.val(value.toFixed(1));

            recalc_modal_purchase();

            return false;
        }
    });

    $('button[data-type=minus-var-amount]').click(function(){
        var input = $('input[name=var_amount]');
        if (input.length > 0)
        {
            var value = parseFloat(input.val()) - {$purchase->product->step_amount};

            if (value < {$purchase->product->min_amount})
                value = {$purchase->product->min_amount};

            input.val(value.toFixed(1));

            recalc_modal_purchase();

            return false;
        }
    });

    $(function(){
        recalc_modal_purchase();
    });

    function recalc_modal_purchase(){
        var total_products_sum = 0;
        var total_products_sum_wdiscount = 0;
        var total_sum = 0;

        var var_amount = 1;
        var var_amount_input = $('input[name=var_amount]');
        if (var_amount_input.length > 0)
            var_amount = var_amount_input.val();

        var price = parseFloat($('#modal-price').attr('data-original-price'));
        price *= var_amount;
        var price_calc = price;

        $('.product-modifier input:checked, .product-modifier select option:selected').each(function(){
            var obj = $(this);
            var obj_type = obj.data('type');
            var obj_value = parseFloat(obj.data('value'));
            var obj_multi_apply = obj.data('multi-apply');
            var obj_multi_buy = obj.data('multi-buy');
            var obj_multi_buy_value = 1;
            if (obj_multi_buy == 1)
                obj_multi_buy_value = parseInt($('input[name=modificators_count_'+obj.val()+']').val());

            if (obj_type == 'plus_fix_sum')
                price_calc += obj_value * obj_multi_buy_value;
            if (obj_type == 'minus_fix_sum')
                price_calc -= obj_value * obj_multi_buy_value;
            if (obj_type == 'plus_percent')
                price_calc += price * obj_value * obj_multi_buy_value / 100;
            if (obj_type == 'minus_percent')
                price_calc -= price * obj_value * obj_multi_buy_value / 100;
        });

        $('#modal-price').attr('data-price', price_calc);
        $('#modal-price').html(String(price_calc.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    }

    $('.product-modifier button[data-type=modificator-minus]').click(function(){
        var i = $(this).closest('.input-group').find('input');
        if (i.val() > i.data('min'))
            i.val(parseInt(i.val()) - 1);
        recalc_modal_purchase();
    });

    $('.product-modifier button[data-type=modificator-plus]').click(function(){
        var i = $(this).closest('.input-group').find('input');
        if (i.val() < i.data('max'))
            i.val(parseInt(i.val()) + 1);
        recalc_modal_purchase();
    });

    $('.product-modifier').on('change keyup input click', 'input[name^=modificators_count]', function(){
        if (this.value.match(/[^0-9]/g)) {
            this.value = this.value.replace(/[^0-9]/g, '');
        }
        var min = parseInt($(this).data('min'));
        var max = parseInt($(this).data('max'));
        if (this.value < min)
            this.value = min;
        if (this.value > max)
            this.value = max;
        recalc_modal_purchase();
    });

    $('.product-modifier input, .product-modifier select').change(function(){
        var checked = $(this).prop('checked');
        var d = $(this).closest('div');
        if (checked){
            d.find('input[name^=modificators_count]').prop('disabled', false);
            d.find('button[data-type=modificator-minus]').prop('disabled', false);
            d.find('button[data-type=modificator-plus]').prop('disabled', false);
        }
        else{
            d.find('input[name^=modificators_count]').prop('disabled', true);
            d.find('button[data-type=modificator-minus]').prop('disabled', true);
            d.find('button[data-type=modificator-plus]').prop('disabled', true);
        }

        recalc_modal_purchase();
    });
</script>