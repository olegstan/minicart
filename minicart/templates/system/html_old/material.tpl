{* Title *}
{$meta_title='Редактирование материала' scope=parent}

<form method=post id="material">
    <fieldset>
        <div class="controlgroup">

                        {if $material}
            <div class="openlink">
                <a href="{makeurl module='materials' item=$material}" target="_blank"><i class="fa fa-external-link"></i> <span>Открыть материал в новом окне</span></a>
            </div>
                        {/if}

            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url current_params=$current_params add=['category_id'=>$materials_category_id, 'page'=>$page]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

     <div class="row">
        <div class="col-md-8">
        <legend>{if $material}Редактирование материала{else}Добавление материала{/if}</legend>
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Наименование материала</label>
        <div class="input-group">
        <input type="text" class="form-control" name="name" placeholder="Введите наименование материала" value="{$material->name|escape_string}"/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id материала">id:{$material->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $material->is_visible || !$material}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $material->is_visible || !$material}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $material && !$material->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $material && !$material->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Категория материалов</label>
                    <select name="parent_id" class="form-control">
                    <option value='0'>Корневая категория</option>
                    {function name=categories_tree level=0}
                        {foreach $items as $item}
                            <option value='{$item->id}' {if $material->parent_id == $item->id || $item->id == $materials_category_id}selected{/if}>{section name=sp loop=$level}&nbsp;&nbsp;&nbsp;&nbsp;{/section}{$item->name}</option>
                            {categories_tree items=$item->subcategories level=$level+1}
                        {/foreach}
                    {/function}
                    {categories_tree items=$categories}
                </select>
                </div>
            </div>        
            <div class="col-md-4 col-md-offset-2">
            <label>Дата</label>
            <div class="input-group date" id="datepicker">
                <input class="form-control" type="text" class="span2" name="date_date" value="{if $material}{$material->date|date}{else}{$smarty.now|date}{/if}"/>
                <span class="input-group-addon add-on"><i class="fa fa-th"></i></span>
            </div>
            </div>
        </div>
        
       <hr />
        <div class="form-group">
        <label class="bigname">Текст материала <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается до списка товаров бренда"><i class="fa fa-info-circle"></i></a></label>
        <textarea name="description" class="form-control ckeditor" rows=8 id="material_description">{$material->description}</textarea>
        </div>
    <div class="form-group material-addons">
        <button class="btn btn-default" onclick="ckeditor_try_insert_delimeter('material_description'); return false;">Подробнее</button>
        
        <a class="btn btn-default{if !$images_gallery} collapsed{/if}" data-toggle="collapse" href="#gallerycollapse" aria-expanded="{if $images_gallery}true{else}false{/if}" aria-controls="gallerycollapse"><i class="fa fa-picture-o"></i> Галлерея</a>
        
        <a class="btn btn-default{if !$attachments} collapsed{/if}" data-toggle="collapse" href="#filescollapse" aria-expanded="{if $attachments}true{else}false{/if}" aria-controls="filescollapse"><i class="fa fa-file-o"></i> Файлы</a>
    </div>
    
    <div class="collapse{if $images_gallery} in{/if}" id="gallerycollapse">
    <legend class="mt30">Галерея</legend>
    {include file='object-gallery-placeholder.tpl' object=$material images_object_name='materials-gallery'}

    <div class="form-group">
        <label>Отображение галлереи</label>
        <div class="btn-group noinline" data-toggle="buttons">
            <label class="btn btn-default {if $material->gallery_mode == "list" || !$material->id}active{/if}">
                <input type="radio" name="gallery_mode" value="list" {if $material->gallery_mode == "list" || !$material->id}checked{/if}><i class="fa fa-bars"></i> Список
            </label>
            <label class="btn btn-default {if $material->gallery_mode == "tile"}active{/if}">
                <input type="radio" name="gallery_mode" value="tile" {if $material->gallery_mode == "tile"}checked{/if}><i class="fa fa-th"></i> Плитка
            </label>
        </div>
    </div>
    {* Настройки для отображения плиткой начало *}
    <div class="row {if $material->gallery_mode == "list" || !$material->id}hidden{/if}" id="settings-for-tile">    
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Ширина</label>
                        <div class="input-group">
                            <input type="text" name="gallery_tile_width" class="form-control" value="{if $material->id}{$material->gallery_tile_width}{else}180{/if}">
                            <span class="input-group-addon">px</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Высота</label>
                        <div class="input-group">
                            <input type="text" name="gallery_tile_height" class="form-control" value="{if $material->id}{$material->gallery_tile_height}{else}180{/if}">
                            <span class="input-group-addon">px</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {* Настройки для отображения плиткой конец *}
        
        
    {* Настройки для отображения плиткой списком *}
    <div class="row {if $material->gallery_mode == "tile"}hidden{/if}" id="settings-for-list">    
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Ширина</label>
                        <div class="input-group">
                            <input type="text" name="gallery_list_width" class="form-control" value="{if $material->id}{$material->gallery_list_width}{else}970{/if}">
                            <span class="input-group-addon">px</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Высота</label>
                        <div class="input-group">
                            <input type="text" name="gallery_list_height" class="form-control" value="{if $material->id}{$material->gallery_list_height}{else}970{/if}">
                            <span class="input-group-addon">px</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {* Настройки для отображения плиткой конец *}
     </div> 
     
     
    <div class="collapse{if $attachments} in{/if}" id="filescollapse">  
        <legend class="mt30">Файлы</legend>

            {include file='object-attachment-placeholder.tpl' object=$material attachments_object_name='materials'}
       </div> 
        
        </div>
        
        <div class="col-md-4">
                <legend>Изображения</legend>

                {include file='object-image-placeholder.tpl' object=$material images_object_name='materials'}

                <legend>Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>

                <div class="form-group">
                <label>URL</label>
                <div class="input-group">
                <input type="text"  class="form-control" name="url" placeholder="Введите URL" value="{$material->url}"/>
                <span class="input-group-addon">.html</span>
                </div>
                </div>

                <div class="form-group">
                <label>Meta-title</label>
                <input type="text"  class="form-control" name="meta_title" placeholder="Введите meta-title" value="{$material->meta_title}"/>
                </div>
                
                <div class="form-group">
                <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-keywords"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_keywords" class="form-control">{$material->meta_keywords}</textarea>
                </div>
                
                <div class="form-group">
                <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_description"  class="form-control">{$material->meta_description}</textarea>
                </div>
                
                <div class="form-group">
                <label>Заголовок страницы <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается внутри H1 на странице материала, это поле опционально, оно нужно если необходимо чтобы заголовок страницы материала (title) отличался от названия материала"><i class="fa fa-info-circle"></i></a></label>
                <input type="text"  class="form-control" name="title" placeholder="" value="{$material->title|escape_string}"/> 
                </div>
                
               <hr />
               <div class="form-group">
               <label>CSS-класс  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Стиль страницы материала, выводится в стиле body"><i class="fa fa-info-circle"></i></a></label>
               <input type="text" class="form-control" name="css_class" placeholder="" value="{$material->css_class}"/>
               </div>
               
               <div class="form-group">
               <label>Вставка кода <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Можно разместить произвольный php или js код, например карту яндекса"><i class="fa fa-info-circle"></i></a></label>
               <textarea class="form-control" rows="3" name="script_text">{$material->script_text}</textarea>

               </div>
            </div>





        
        

        <input type="hidden" name="id" value="{$material->id}"/>
        <input type="hidden" name="return_page" value="{if $page}{$page}{else}1{/if}"/>
        <input type="hidden" name="attachments_position" value=""/>
        {if $materials_category_id}<input type="hidden" name="materials_category_id" value="{$materials_category_id}"/>{/if}
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        <input type="hidden" name="recreate_seo" value="0"/>
        <input type="hidden" name="date_time" value="{if $material->date}{$material->date|time}{else}{$smarty.now|time}{/if}">
        {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
    </fieldset>
        </div>

</form>

<script type="text/javascript">

    $("input[name=gallery_mode]").change(function(){
        var val = $("input[name=gallery_mode]:checked").val();
        if (val == 'tile'){
            $("#settings-for-list").addClass('hidden');
            $("#settings-for-tile").removeClass('hidden');
        }
        else{
            $("#settings-for-list").removeClass('hidden');
            $("#settings-for-tile").addClass('hidden');
        }
    });

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $('#datepicker').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: true,
            language: "ru",
            autoclose: true,
            todayHighlight: true
        });

        $('textarea[name=meta_keywords], textarea[name=meta_description], textarea[name=script_text]').autosize();
    });

    $('form#material').submit(function () {
        var as = '';
        $("#files li").each(function () {
            if (as.length > 0)
                as += ",";
            as += $(this).attr('data-id');
        });

        if (as.length > 0)
            $('form#material input[name=attachments_position]').val(as);
    });

    $('#recreate-seo').click(function(){
        $(this).closest('form').find('input[name=recreate_seo]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#material a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#material a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#material a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $('form#material input[name="name"]').keyup(function(){
        var v = $('form#material input[name="name"]').val();
        $('form#material input[name="meta_title"]').val(v);
        $('form#material textarea[name="meta_keywords"]').val(v);
    });
</script>