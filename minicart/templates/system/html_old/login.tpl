{* Страница авторизации админки *}
{$wrapper = 'login.tpl' scope=parent}

<!DOCTYPE html>
{*
    Общий вид страницы
    Этот шаблон отвечает за общий вид страниц без центрального блока.
*}
<html>
<head>
    <base href="{$config->root_url}/"/>
    <title>Вход</title>

    {* Метатеги *}
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="Вход" />
    <meta name="keywords" content="Вход" />
    
    {* Подключаем jquery последнюю версию из библиотек Google *}
    <script src="http://code.jquery.com/jquery-latest.min.js"  type="text/javascript"></script>

    {* Подключаем бустрап *}
    <link href="{$path_admin_template}css/bootstrap.css" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{$path_admin_template}js/bootstrap.min.js"  type="text/javascript"></script>
    
    {* Подключаем css *}    
    <link href="{$path_admin_template}css/template.css" rel="stylesheet" type="text/css" media="screen"/>
    
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>
<body class="loginbg">

 <div class="container" id="center-vertikal">


    <div class="main">
        <div class="main_in_main">
             <div class="content">
                <form class="form-signin" method="post" id="admin-login-form">
                    <fieldset>
                    <div class="login-logo"></div>

                    <div class="form-group">
                    <input type="email" class="form-control" name="username" placeholder="Почта или Номер мобильного" value={$username}>
                    <div class="help-block-error"  id="name-required" style="display:none;">Введите e-mail в формате: ivanov@gmail.com<br/>
                    или телефон в формате: +79150000000
                    </div>
                    </div>
                    <div class="form-group">
                    <input type="password" class="form-control" name="password" placeholder="Пароль" value="">
                   
                    <div class="help-block-error" id="password-required" style="display:none;">Введите пароль</div>
                    
                    </div>
                    <button class="btn btn-primary w100" type="submit">Войти</button>
                    {if $error_message}
                    <div class="alert alert-danger">
                    {if $error_message == "account_disabled"}Учетная запись отключена.
                    {elseif $error_message == "access_denied"}У Вас нет доступа к этому модулю.
                    {elseif $error_message == "auth_error"}Почта, телефон или пароль введены неверно.
                    {else}{$error_message|escape}
                    {/if}
                    </div>
                    {/if}
                    </fieldset>
                  </form>
          <div class="textcenter"><p><a href="{$config->root_url}/">Вернуться на сайт</a></p>
          <p><a href="{$config->root_url}/login/?mode=forgot-password" data-toggle="modal">Забыли пароль?</a></p>
          <p>Работает на <a href="http://minicart.ru/">MiniCart</a></p></div>
            </div>
        </div>
   </div>
  <div id="push"></div>
     <!-- /container -->
</div>

<script type="text/javascript">
    $("#admin-login-form").submit(function(){
        if ($('input[name=username]').val().length == 0)
        {
            $('#name-required').show();
            return false;
        }
        if ($('input[name=password]').val().length == 0)
        {
            $('#password-required').show();
            return false;
        }
    });
</script>

</body>
</html>