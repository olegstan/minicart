{* Title *}

{if $order->id}
{$meta_title='Заказ №'|cat:$order->id scope=parent}
{else}
{$meta_title='Добавление заказа' scope=parent}
{/if}

<form method=post id="order">
    <fieldset>
        <div class="controlgroup">
            {if $order->id}<div class="openlink">
                <a href="{$config->root_url}{$order_module->url}{$order->url}/" target="_blank"><i class="fa fa-external-link"></i> <span>{$config->root_url}{$order_module->url}{$order->url}/</span></a>
            </div>{/if}


            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url add=['status_id'=>$status_id]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row orderproducts">
            <div class="col-md-8">
                <legend>
                <div class="row">
                    <div class="col-md-4">
                {if $order->id}Заказ №{$order->id}{else}Добавление заказа{/if} 
                    </div>
                    <div class="col-md-8">
                    <select class="form-control orderstatus input-lg" name="status_id">
                        {foreach $orders_statuses as $status}
                            <option value='{$status->id}' {if $status->id == $order->status_id || $status_id == $status->id}selected{/if}>{$status->group_name|escape}</option>
                        {/foreach}
                    </select>
                    <div class="status">Статус:</div>
                    </div>
                </div>
                </legend>
                
               <ol class="product-list">
                    {include file='order-purchases.tpl'}
                </ol>
                    <div id="edit-order" class="tags_values" style="display:none;">
                    <div class="apsearch form-inline">

                            <input id="searchField" type="text"  class="form-control" placeholder="Поиск" autocomplete="off">

                            <button id="searchCancelButton" style="display: none;" class="btn btn-default searchremove" type="button"><i class="fa fa-times"></i></button>


                    </div>
                    <div id="refreshpart" class="addcontainer" style="display:none;">{include file='product-addrelated.tpl'}</div>
                </div>

                <a href="#" class="order-edit"><span>Редактировать состав заказа</span></a>
                <div class="ordersumm">Итого: <div class="order-sub-price"> <span id="total_products_sum">-</span> {$main_currency->sign}</div></div>
                
            <div class="discount">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Скидка</span>
                        <input type="hidden" name="discount_type" value="{$order->discount_type}"/>
                        <input type="text" class="form-control order-discount" placeholder="0" name="discount" value="{$order->discount|string_format:"%d"}"/>
                        <div class="input-group-btn search-panel">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                <span id="discount-type-button">{if $order->discount_type == 0}%{else}{$main_currency->sign}{/if} </span> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu" id="discount-type">
                              <li><a href="#contains" data-type="0">%</a></li>
                              <li><a href="#its_equal" data-type="1">{$main_currency->sign}</a></li>
                            </ul>
                        </div>
                        </div>
                </div>
            
                <div class="ordersumm" {if $order->discount == 0}style="display:none;"{/if}>Итого c учетом скидки: <div class="order-sub-price"><span id="total_products_sum_wdiscount">-</span> {$main_currency->sign}</div></div>
            </div>
            
           <div class="delivery">
            <legend>Доставка</legend>
            <div class="form-inline">
                <select class="form-control deliveryselect" name="delivery_id">
                    {foreach $deliveries as $delivery}
                        <option value='{$delivery->id}' {if $delivery->id == $order->delivery_id}selected{/if}
                        data-separate="{if $delivery->delivery_type == 'separately'}1{else}0{/if}"
                        data-price="{if $delivery->delivery_type == 'paid'}{$delivery->price|string_format:"%d"}{else}0{/if}"
                        data-free="{if $delivery->delivery_type == 'paid'}{if $delivery->free_from == ''}9999999999{else}{$delivery->free_from}{/if}{else}0{/if}">{$delivery->name|escape}</option>
                    {/foreach}
                </select>
                <div class="input-group deliverycost">
                <input type="text" class="form-control" placeholder="0" name="delivery_price" value="{if $order}{$order->delivery_price|string_format:"%d"}{else}{if $deliveries.0->delivery_type == 'paid'}{$deliveries.0->price|string_format:"%d"}{else}0{/if}{/if}">
                <span class="input-group-addon">{$main_currency->sign}</span>
                </div>
                
                <div class="checkbox separately">
                <label class="checkbox">
                  <input type="checkbox" name="separate_delivery" id="separate_delivery" value="1" {if $order->separate_delivery}checked{/if}> 
                  <label class="{if $order->separate_delivery}checked{else}notchecked{/if}" for="separate_delivery">Оплачивается отдельно</label>
                </label>
             </div>
             
            
            <legend class="margin-top-20">Оплата</legend>
            <div class="form-inline">
                <select class="form-control deliveryselect" name="payment_method_id">
                    {$selected_payment_method = $payment_methods.0}
                    {foreach $payment_methods as $payment}
                        <option value='{$payment->id}' {if $payment->id == $order->payment_method_id}selected{$selected_payment_method = $payment}{/if} data-allow-payment="{$payment->allow_payment}">{$payment->name|escape}</option>
                    {/foreach}
                </select>
                
                <div class="form-group {if $selected_payment_method->allow_payment}hidden{/if}" id="allow-payment">
                    <label>Разрешить оплату</label>
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $order->allow_payment || (!$order && $selected_payment_method->allow_payment)}active{/if}">
                            <input type="radio" name="allow_payment" value=1 {if $order->allow_payment || (!$order && $selected_payment_method->allow_payment)}checked{/if}> Да
                        </label>
                        <label class="btn btn-default4 off {if !$order->allow_payment || (!$order && !$selected_payment_method->allow_payment)}active{/if}">
                            <input type="radio" name="allow_payment" value=0 {if !$order->allow_payment || (!$order && !$selected_payment_method->allow_payment)}checked{/if}> Нет
                        </label>
                    </div>
                </div>
                <div class="checkbox separately">
                <label class="checkbox">
                  <input type="checkbox" name="paid" id="paid" value="1" {if $order->paid}checked{/if}>
                  <label class="{if $order->paid}checked{else}notchecked{/if}" for="paid">Заказ оплачен</label>
                </label>
             </div>
             </div>
             
            <div class="ordersumm total"><span class="minisumm" id="label_order_total_price">Итого к оплате <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="{if $order->separate_delivery}Итого без учета доставки{else}Итого c доставкой{/if}"><i class="fa fa-info-circle"></i></a>:</span> <div class="order-sub-price"><span id="total_sum">-</span> {$main_currency->sign}</div></div> 
            </div>
            </div>
            
                        
            
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Детали заказа <a href="#" id="order-detail-mode" class="btn btn-link btn-xs"><span>Редактировать</span></a></h3>
                    </div>
                  <div class="panel-body locked" id="order-detail-edit" {if $order->id}style="display:none;"{/if}>
                     <div class="form-group">

                        <div class="row">
                        <div class="col-md-7">
                        <label>Дата:</label>


                        <div class="input-group date" id="datepicker">
                          <input class="form-control" type="text" class="span2" name="date_date" value="{if $order->id}{$order->date|date}{else}{$smarty.now|date}{/if}">
                          <span class="input-group-addon add-on"><i class="fa fa-th"></i></span>
                        </div>

                        </div>
                        <div class="col-md-5">
                        <label>Время:</label>
                        <input id="timepicker" type="text" name="date_time" class="form-control" {*placeholder="Введите время заказа"*} value="{if $order->date}{$order->date|time}{else}{$smarty.now|time}{/if}">
                        </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Имя:</label>
                        <input type="text" name="name" class="form-control" placeholder="Введите Имя" value="{$order->name|escape_string}">
                    </div>
                  
                    <div class="form-group">
                        <label>E-mail:</label>
                        <div class="notify-onoff">
                                <div data-toggle="buttons" class="btn-group">
                                    <label class="btn btn-default4 btn-xs on {if $order->notify_news || !$order}active{/if}">
                                        <input type="radio" value="1" name="notify_news" {if $order->notify_news || !$order}checked{/if}><i class="fa fa-check"></i></label>
                                    <label class="btn btn-default4 btn-xs off {if $order && !$order->notify_news}active{/if}">
                                        <input type="radio" value="0" name="notify_news" {if $order && !$order->notify_news}checked{/if}><i class="fa fa-times"></i></label>
                                </div>
                        </div>
                        <input type="email" name="email" class="form-control" placeholder="Введите e-mail" value="{$order->email|escape_string}">
                    </div>
                    
                    <div class="form-group">
                        <label>Телефон:</label>
                        <div class="notify-onoff">
                                <div data-toggle="buttons" class="btn-group">
                                    <label class="btn btn-default4 btn-xs on {if $order->notify_order || !$order}active{/if}">
                                        <input type="radio" value="1" name="notify_order" {if $order->notify_order || !$order}checked{/if}><i class="fa fa-check"></i></label>
                                    <label class="btn btn-default4 btn-xs off {if $order && !$order->notify_order}active{/if}">
                                        <input type="radio" value="0" name="notify_order" {if $order && !$order->notify_order}checked{/if}><i class="fa fa-times"></i></label>
                                </div>
                        </div>
                        <input type="text" id="phone" class="form-control" name="phone" value="{if $order->phone_code && $order->phone}+7 ({$order->phone_code}) {$order->phone|phone_mask}{/if}" data-type="phone" data-value-default="{if $order->phone_code && $order->phone}+7 ({$order->phone_code}) {$order->phone|phone_mask}{/if}"/>
                        </div>
                    <label class="phonetwo"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#phoneTwo">Дополнительный телефон</a></label>
                    <div id="phoneTwo" class="panel-collapse collapse {if !empty($order->phone2_code) || !empty($order->phone2)}in{/if}">
                        <div class="form-group">
                            {*<input type="text" name="" class="form-control" placeholder="" value="">*}
                            <input type="text" id="phone2" class="form-control" name="phone2" value="{if $order->phone2_code && $order->phone2}+7 ({$order->phone2_code}) {$order->phone2|phone_mask}{/if}" data-type="phone" data-value-default="{if $order->phone2_code && $order->phone2}+7 ({$order->phone2_code}) {$order->phone2|phone_mask}{/if}"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label>Адрес</label>
                        <textarea class="form-control" rows="2" name="address">{$order->address|escape}</textarea>

                    </div>
                    
                    <div class="form-group">
                        <label>Комментарий клиента</label>
                        <textarea class="form-control" rows="2" name="comment">{$order->comment}</textarea>
                    </div>
                    
                  </div>
                  
                  
                  <div class="panel-body" id="order-detail-view" {if !$order->id}style="display:none;"{/if}>
                     <div class="form-group">

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                <label><span class="opacity05">Дата</span> {if $order->id}{$order->date|date}{else}{$smarty.now|date}{/if}</label>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                <label><span class="opacity05">Время</span> {if $order->date}{$order->date|time}{else}{$smarty.now|time}{/if}</label>
                                </div>
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <label id="order-detail-view-name"><span class="opacity05">Имя:</span> {$order->name|escape_string}</label>
                        </div>

                        <div class="form-group">
                            <label><span class="opacity05">E-mail</span> <a href="mailto:{$order->email}" id="order-detail-view-email">{$order->email|escape_string}</a>
                            {if $order->notify_news}
                            <span class="send-ok"><a title="" data-toggle="tooltip" data-placement="top" href="#" data-original-title="Согласен на получение E-mail" class="fa fa-legend"><i class="fa fa-check"></i></a></span>
                            {else}
                            <span class="send-notok"><a title="" data-toggle="tooltip" data-placement="top" href="#" data-original-title="Не присылать E-mail" class="fa fa-legend"><i class="fa fa-times"></i></a></span>
                            {/if}
                            </label>
                        </div>


                        <div class="form-group">
                            <label class="w100"><span class="opacity05">Телефон:</span> {if $order->phone_code || $order->phone}+7 (<span id="order-detail-view-phone-code">{$order->phone_code}</span>) <span id="order-detail-view-phone">{$order->phone|phone_mask}</span>{/if}
                            {if $order->notify_order}
                            <span class="send-ok"><a title="" data-toggle="tooltip" data-placement="top" href="#" data-original-title="Согласен на получение SMS" class="fa fa-legend"><i class="fa fa-check"></i></a></span>
                            {else}
                            <span class="send-notok"><a title="" data-toggle="tooltip" data-placement="top" href="#" data-original-title="Не присылать SMS" class="fa fa-legend"><i class="fa fa-times"></i></a></span>
                            {/if}
                            </label>
                        </div>

                        {if $order->phone2_code || $order->phone2}
                        <div class="form-group">
                            <label class="w100"><span class="opacity05">Доп. тел.:</span> +7 (<span id="order-detail-view-phone2-code">{$order->phone2_code}</span>) <span id="order-detail-view-phone2">{$order->phone2|phone_mask}</span> <a href="#" class="sendsms btn btn-default btn-xs">SMS</a></label>
                        </div>
                        {/if}
                    </div>
                    
                    <div class="form-group">
                        <label class="w100"><span class="opacity05">Адрес</span> <button id="show-map" class="mapyandex btn-link btn-xs">Показать на карте</button></label>
                        <div class="textarea-alternative" id="order-detail-view-address">{$order->address|escape}</div>
                    </div>
                    
                    <div class="form-group">
                        <label><span class="opacity05">Комментарий</span></label>
                        <div class="textarea-alternative">{$order->comment}</div>
                    </div>
                    
                </div>
                  
                  
               </div>
 
             <div class="panel panel-default">
              <div class="panel-heading">
                <h3 class="panel-title">Пользователь <a href="#" id="order-user-mode" class="btn btn-link btn-xs"><span>Редактировать</span></a></h3>
              </div>
              <div class="panel-body userselect" id="order-user-edit" style="display:none;">
                <select name="user_id" style="width:100%">
                    <option></option>
                    {foreach $all_users as $u}
                        <option value='{$u->id}' {if $u->id == $order->user_id}selected{/if} data-email="{$u->email}" data-phone-code="{$u->phone_code}" data-phone="{$u->phone}" data-phone-mask="{$u->phone|phone_mask}" data-phone2-code="{$u->phone2_code}" data-phone2="{$u->phone2}" data-phone2-mask="{$u->phone2|phone_mask}" data-address="{$u->delivery_address}" data-discount="{$u->discount|string_format:"%d"}">{$u->name|escape}</option>
                    {/foreach}
                </select>
              </div>
              <div class="panel-body userselect" id="order-user-view">
                {foreach $all_users as $u}
                    {if $u->id == $order->user_id}
                        <i class="fa fa-user"></i> <a href="{$config->root_url}{$user_module->url}{url add=['id'=>$u->id]}" class="user">{$u->name}</a>
                    {/if}
                {/foreach}
                {*<i class="fa fa-user"></i> <a href="#" class="user">{$order->name}</a>*}
              </div>
            </div>
 
            <div class="panel panel-warning notbordersandpadding">
                <div class="panel-heading">
                    <h3 class="panel-title">Примечания</h3>
                    <div class="minimessage">Видны только администратору</div>
                </div>
                <div class="panel-body" id="order-note-edit">
                    <textarea class="form-control" rows="3" name="note">{$order->note|escape}</textarea>
                </div>
            </div>
            
            <hr/>

            <div class="panel-group" id="accordion">
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#extra">
                     Дополнительно
                    </a>
                  </h4>
                </div>
                <div id="extra" class="panel-collapse collapse">
                  <div class="panel-body">
                    <p>IP-адрес: {$order->ip}
                    </p>
                    <p>
                    <button type="button" class="btn btn-default"><i class="fa fa-envelope-o"></i> Отправить заказ клиенту</button>
                    </p>
                    {*
                    <p>
                    <button type="button" class="btn btn-default"><i class="fa fa-print"></i> Распечатать накладную</button>
                    </p>
                    *}
                  </div>
                </div>
              </div>
            </div>


 
            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="delivery-map" id="delivery-map" class="panel-collapse collapse">

                </div>
            </div>
        </div>



            <a href="#" id="hide-map" class="mapyandex-show" style="display: none;"><span>Скрыть карту</span></a>
            <div id="deliverymap" class="panel-collapse collapse in" style="width: 100%; height: 450px; display: none; clear:both;">
            </div>



        </div>

        <input type="hidden" name="id" value="{$order->id}"/>
        {if $status_id}<input type="hidden" name="return_status_id" value="{$status_id}"/>{/if}
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        <input type="hidden" name="images_position" value=""/>
    </fieldset>
</form>

<!-- Modal -->
<div class="modal modificators-dialog" id="modificators-dialog" tabindex="-1" role="dialog" aria-labelledby="callmemodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Радактирование модификаторов</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <button id="save-modificators" type="button" class="btn btn-success" >Сохранить</a>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal modificators-order-dialog" id="modificators-order-dialog" tabindex="-1" role="dialog" aria-labelledby="callmemodal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModal2Label">Радактирование модификаторов заказа</h4>
        </div>
        <div class="modal-body">

        </div>
        <div class="modal-footer">
            <button id="save-modificators-order" type="button" class="btn btn-success" >Сохранить</a>
        </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="http://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>

<script type="text/javascript">
    $('ol.product-list').on('click', 'button.btn-edit-modificators', function(){

        if ($(this).hasClass('not-saved-purchase'))
        {
            error_box($('#notice'), 'Чтобы редактировать модификаторы сохраните заказ!', '')
            return false;
        }

        var purchase_id = $(this).closest('li').attr('data-id');

        $('#modificators-dialog .modal-body').html('');

        $.get("{$config->root_url}{$module->url}?mode=purchase-modificators&ajax=1&purchase_id="+purchase_id, function(data){
            if (data.success)
                $('#modificators-dialog .modal-body').html(data.data);
            $('#modificators-dialog').modal();
        });
    });

    $('#save-modificators').click(function(){
        var purchase_id = $('#modificators-dialog .modal-body input[name=purchase_id]').val();
        var modificators = [];
        var modificators_count = [];
        var var_amount = 1;
        if ($('#modificators-dialog .modal-body input[name=var_amount]').length > 0)
            var_amount = $('#modificators-dialog .modal-body input[name=var_amount]').val();

        $('#modificators-dialog .modal-body .product-modifier input:checked, #modificators-dialog .modal-body .product-modifier select option:selected').each(function(){
            modificators.push($(this).val());
            var ic = $(this).closest('div').find('input[name^=modificators_count]');
            if (ic.length > 0)
                modificators_count.push(ic.val());
            else
                modificators_count.push(1);
        });

        var href = "{$config->root_url}{$module->url}?mode=purchase-save-modificators&purchase_id="+purchase_id+"&ajax=1&modificators="+modificators.join(',')+"&modificators_count="+modificators_count.join(',')+"&var_amount="+var_amount;
        $.get(href, function(data){
            if (data.success){
                $('#modificators-dialog').modal('hide');
                $('ol.product-list').html(data.data);
                recalc_order();
            }
        });
    });

    $('ol.product-list').on('click', 'button.btn-edit-modificators-orders', function(){

        if ($(this).hasClass('not-saved-purchase'))
        {
            error_box($('#notice'), 'Чтобы редактировать модификаторы заказа сохраните заказ!', '')
            return false;
        }

        $('#modificators-order-dialog .modal-body').html('');

        $.get("{$config->root_url}{$module->url}?mode=order-modificators&ajax=1&order_id={$order->id}", function(data){
            if (data.success)
                $('#modificators-order-dialog .modal-body').html(data.data);
            $('#modificators-order-dialog').modal();
        });
    });

    var myMap;

    $('#hide-map').click(function(){
        $('#hide-map').hide();
        $('#deliverymap').hide();

        if (myMap != undefined){
            myMap.destroy();
        }

        return false;
    });

    $('#show-map').click(function(){
        $('#show-map').prop('disabled', true);
        $('#deliverymap').show();

        if (myMap != undefined){
            myMap.destroy();
        }

        // Дождёмся загрузки API и готовности DOM.
        setTimeout(function() {
              ymaps.ready(init_map);
        }, 500);

        function init_map () {
            // Создание экземпляра карты и его привязка к контейнеру с
            // заданным id ("map").
            myMap = new ymaps.Map('deliverymap', {
                // При инициализации карты обязательно нужно указать
                // её центр и коэффициент масштабирования.
                center: [55.76, 37.64], // Москва
                zoom: 10
            });

            var address = $('textarea[name=address]').val();

            ymaps.geocode(address, {
                // boundedBy: myMap.getBounds(), // Сортировка результатов от центра окна карты
                // strictBounds: true, // Вместе с опцией boundedBy будет искать строго внутри области, указанной в boundedBy
                //results: 1 // Если нужен только один результат, экономим трафик пользователей
            }).then(function (res) {
                    // Выбираем первый результат геокодирования.
                    var firstGeoObject = res.geoObjects.get(0),
                        // Координаты геообъекта.
                        coords = firstGeoObject.geometry.getCoordinates(),
                        // Область видимости геообъекта.
                        bounds = firstGeoObject.properties.get('boundedBy');

                    // Добавляем первый найденный геообъект на карту.
                    myMap.geoObjects.add(firstGeoObject);
                    // Масштабируем карту на область видимости геообъекта.
                    myMap.setBounds(bounds, {
                        checkZoomRange: true // проверяем наличие тайлов на данном масштабе.
                    });

                    myMap.container.fitToViewport();
                });
            $('#show-map').prop('disabled', false);
            $('#hide-map').show();
        }

        return false;
    });

    $('#discount-type a').click(function(e){
        e.preventDefault();
        $('input[name=discount_type]').val($(this).data('type'));
        $('#discount-type-button').html($(this).html());
        recalc_order();
        //return false;
    });

    $('select[name=payment_method_id]').change(function(){
        var option = $(this).find('option:selected');
        if (option.attr('data-allow-payment') == "1")
        {
            $('#allow-payment').addClass('hidden');
            $('#allow-payment input[name=allow_payment][value=1]').attr('checked', true).closest('label').addClass('active');
            $('#allow-payment input[name=allow_payment][value=0]').attr('checked', false).closest('label').removeClass('active');
        }
        else
        {
            $('#allow-payment').removeClass('hidden');
            $('#allow-payment input[name=allow_payment][value=1]').attr('checked', false).closest('label').removeClass('active');
            $('#allow-payment input[name=allow_payment][value=0]').attr('checked', true).closest('label').addClass('active');
        }
    });

    $('select[name=delivery_id]').change(function(){
        payment_select = $('select[name=payment_method_id]');
        payment_select.html('');
        var delivery_id = $(this).find('option:selected').val();
        var separate = $(this).find('option:selected').data('separate');
        if (separate == "1")
            $('#separate_delivery').prop('checked', true);
        else
            $('#separate_delivery').prop('checked', false);
        $('#separate_delivery').trigger('change');
        $.ajax({
            type: 'POST',
            url: '{$config->root_url}/ajax/get_data.php',
            data: {
                'object': 'orders',
                'mode': 'get_payment_methods',
                'delivery_id': delivery_id,
                'session_id': '{$smarty.session.id}'
            },
            success: function(data) {
                if (data.success)
                {
                    for(var index in data.data)
                    {
                        var d = data.data[index];
                        payment_select.append('<option value="'+d.id+'">'+d.name+'</option>');
                    }
                    payment_select.find('option:first').prop('selected', true);
                }
            }
        });
    });

    $('#separate_delivery').change(function(){
        if ($(this).prop('checked'))
            $('#label_order_total_price').html('Итого без учета доставки:');
        else
            $('#label_order_total_price').html('Итого с доставкой:');
    });

    $("select[name=user_id]").select2({
        placeholder: "Выберите пользователя",
        allowClear: true
    });

    $("select[name=user_id]").on("change", function(e){
        var user = $(this).find('option:selected');
        if (user.val.length > 0)
        {
            if ($('#order-detail-edit input[name=name]').val().length == 0)
            {
                $('#order-detail-edit input[name=name]').val(user.html());
                $('#order-detail-view-name').html('<span class="opacity05">Имя:</span> ' + user.html());
            }
            if ($('#order-detail-edit input[name=email]').val().length == 0)
            {
                $('#order-detail-edit input[name=email]').val(user.attr('data-email'));
                $('#order-detail-view-email').html(user.attr('data-email'));
            }
            if ($('#order-detail-edit input[name=phone]').mask().length == 0)
            {
                $('#order-detail-edit input[name=phone]').val('+7 (' + user.attr('data-phone-code') + ') ' + user.attr('data-phone'));
                $("#phone").mask("+7 (999) 999-99-99");
                /*$('#order-detail-edit input[name=phone_code]').val(user.attr('data-phone-code'));
                $('#order-detail-edit input[name=phone]').val(user.attr('data-phone'));
                $('#order-detail-view-phone-code').html(user.attr('data-phone-code'));
                $('#order-detail-view-phone').html(user.attr('data-phone-mask'));*/
            }
            if ($('#order-detail-edit input[name=phone2]').mask().length == 0)
            {
                $('#order-detail-edit input[name=phone2]').val('+7 (' + user.attr('data-phone2-code') + ') ' + user.attr('data-phone2'));
                $("#phone2").mask("+7 (999) 999-99-99");
                /*$('#order-detail-edit input[name=phone2_code]').val(user.attr('data-phone2-code'));
                $('#order-detail-edit input[name=phone2]').val(user.attr('data-phone2'));
                $('#order-detail-view-phone2-code').html(user.attr('data-phone2-code'));
                $('#order-detail-view-phone2').html(user.attr('data-phone2-mask'));*/
            }
            if ($('#order-detail-edit textarea[name=address]').val().length == 0)
            {
                $('#order-detail-edit textarea[name=address]').val(user.attr('data-address'));
                $('#order-detail-view-address').html(user.attr('data-address'));
            }
            if ($('input[name=discount]').val().length == 0 || parseInt($('input[name=discount]').val())==0)
            {
                $('input[name=discount]').val(user.attr('data-discount'));
                recalc_order();
            }
        }
        return false;
    });

    $(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            {if $message_error == "empty_purchases"}error_box($('#notice'), 'Пустой список товаров!', '')
            {else}error_box($('#notice'), 'Ошибка при сохранении!', '')
            {/if}
        {/if}

        recalc_order();

        //$('#datepicker').datepicker();

        $('#datepicker').datepicker({
            format: "dd.mm.yyyy",
            todayBtn: true,
            language: "ru",
            autoclose: true,
            todayHighlight: true
        });

        $('#timepicker').mask("99:99");
        $("#phone").mask("+7 (999) 999-99-99");
        $("#phone2").mask("+7 (999) 999-99-99");

        $('textarea[name=comment], textarea[name=note], textarea[name=address]').autosize();
    });

    var search_ajax_context;

    //запрос товаров на ajax'e
    function products_request_ajax(params){

        if (params == undefined || params.length == 0)
        {
            $('#refreshpart').html('');
            return false;
        }

        $('#products-spinner').show();
        {*var url = "{$config->root_url}{$products_module->url}";*}
        var url = "{$config->root_url}/ajax/get_data.php?object=products&mode=product-addrelated";
        if (params != undefined && params.length>0)
        {
            var index2 = 0;
            for(var index in params)
            {
                if (params[index].key != "url")
                {
                    url += "&";
                    url += params[index].key + "=" + params[index].value;
                    index2 += 1;
                }
            }
        }
        var exception_ids = "{$product->id}";
        $('ol.product-list li[data-product]').each(function(){
            if (exception_ids.length>0)
                exception_ids += ',';
            exception_ids += $(this).attr('data-product');
        });
        var full_url = url + "&exception="+exception_ids;

        $('#refreshpart').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');

        if (search_ajax_context != null)
            search_ajax_context.abort();

        search_ajax_context = $.ajax({
            type: 'GET',
            url: full_url,
            success: function(data) {
                if (data.success)
                {
                    $('#refreshpart').html(data.data);
                    $('#refreshpart li:first').addClass('selected');
                    $('#products-spinner').hide();
                }
                search_ajax_context = null;
            }
        });
        return false;
    }


    //пересчет стоимости заказа
    function recalc_order(){
        var total_products_sum = 0;
        var total_products_sum_wdiscount = 0;
        var total_sum = 0;
        var discount = 0;
        if ($('input[name=discount]').val().length > 0)
            discount = parseFloat($('input[name=discount]').val());
        var discount_type = parseInt($('input[name=discount_type]').val());

        if (discount == 0)
            $('#total_products_sum_wdiscount').closest('.ordersumm').hide();
        else
            $('#total_products_sum_wdiscount').closest('.ordersumm').show();

        $('ol.product-list > li').each(function(){
            var price = parseFloat($(this).find('span.purchase-price').data('price-for-one-pcs'));
            var price_for_mul = parseFloat($(this).find('span.purchase-price').data('price-for-mul'));
            var additional_sum = parseFloat($(this).find('span.purchase-price').data('price-additional-sum'));
            var amount = parseFloat($(this).find('input[name*=amount]').val());
            total_products_sum += price_for_mul*amount + additional_sum;
            /*var price = parseFloat($(this).find('span.purchase-price').data('price'));
            var amount = parseFloat($(this).find('input[name*=amount]').val());
            total_products_sum += price*amount;*/
        });

        if (discount_type == 0 && discount > 100)
        {
            discount = 100;
            $('input[name=discount]').val(discount);
        }

        if (discount_type == 1 && discount > total_products_sum)
        {
            discount = total_products_sum;
            $('input[name=discount]').val(discount);
        }

        if (discount_type == 0)    // in %
            total_products_sum_wdiscount = total_products_sum - total_products_sum * discount / 100;
        else // in currency
            total_products_sum_wdiscount = total_products_sum - discount;

        $('#total_products_sum').html(String(total_products_sum.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        $('#total_products_sum_wdiscount').html(String(total_products_sum_wdiscount.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));

        total_sum = total_products_sum_wdiscount;
        if (!$('input[name=separate_delivery]').is(':checked'))
            total_sum += parseFloat($('input[name=delivery_price]').val());
        $('#total_sum').html(String(total_sum.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
    }

    {* нажатие Enter в поле поиска *}
    $('#searchField').on('keydown', function(e){
        if (e.keyCode == 13)
        {
            var curli = $('#refreshpart li.selected');
            curli.click();
            return false;
        }
    });

    {* поиск keyword *}
    $('#searchField').on('keyup', function(e){
        var params = [];

        //down or up
        if (e.keyCode == 40 || e.keyCode == 38)
        {
            var curli = $('#refreshpart li.selected');

            if (e.keyCode == 40)
            {
                var nextli = curli.next();
                if (nextli.length == 0)
                    nextli = $('#refreshpart li:first');
            }
            if (e.keyCode == 38)
            {
                var nextli = curli.prev();
                if (nextli.length == 0)
                    nextli = $('#refreshpart li:last');
            }

            curli.removeClass('selected');
            nextli.addClass('selected');
        }
        else
        {
            if ($(this).val().length > 0)
            {
                $('#refreshpart').show();
                $('#searchCancelButton').show();
                params.push({
                    'key': 'keyword',
                    'value': $(this).val()
                });
            }
            else
            {
                $('#refreshpart').hide();
            }
            products_request_ajax(params);
        }
        return false;
    });

    $('#refreshpart').on('mouseenter', 'li', function(){
        $('#refreshpart li.selected').removeClass('selected');
        $(this).addClass('selected');
    });

    // сброс keyword
    $('#searchCancelButton').click(function(){
        $('#searchCancelButton').hide();
        $('#searchField').val('');
        products_request_ajax();
        $('#refreshpart').hide();
        return false;
    });

    $('ol.product-list').on("click", ".fa-times", function(){
        $(this).closest('li').fadeOut('slow').remove();
        recalc_order();
        return false;
    });

    $('ol.product-list').on("change", "select", function(){
        var option = $(this).find('option:selected');
        var price = parseFloat(option.attr('data-price'));
        var amount = $(this).closest('li').find('input[name*=amount]').val();
        var total_price = price;
        $(this).closest('li').find('.cart-modifier ul li').each(function(){
            var obj = $(this);
            var obj_type = obj.attr('data-type');
            var obj_value = parseFloat(obj.attr('data-value'));
            var obj_multi_apply = obj.attr('data-multi-apply');
            if (obj_multi_apply == 0 && amount > 1)    //skip processing modificator
                return true;
            var obj_multi_buy = obj.attr('data-multi-buy');
            var obj_multi_buy_value = 1;
            if (obj_multi_buy == 1)
                obj_multi_buy_value = obj.attr('data-amount');

            if (obj_type == 'plus_fix_sum')
                total_price += obj_value * obj_multi_buy_value;
            if (obj_type == 'minus_fix_sum')
                total_price -= obj_value * obj_multi_buy_value;
            if (obj_type == 'plus_percent')
                total_price += price * obj_value * obj_multi_buy_value / 100;
            if (obj_type == 'minus_percent')
                total_price -= price * obj_value * obj_multi_buy_value / 100;
        });
        var total_price_variant_for_multi = total_price;

        $(this).closest('li').find('.cart-modifier ul li').each(function(){
            var obj = $(this);
            var obj_type = obj.attr('data-type');
            var obj_value = parseFloat(obj.attr('data-value'));
            var obj_multi_apply = obj.attr('data-multi-apply');
            if (!(obj_multi_apply == 0 && amount > 1))    //skip processing modificator
                return true;
            var obj_multi_buy = obj.attr('data-multi-buy');
            var obj_multi_buy_value = obj.attr('data-amount');
            if (obj_multi_buy == 1)
                obj_multi_buy_value = parseInt($('input[name=modificators_count_'+obj.val()+']').val());

            if (obj_type == 'plus_fix_sum')
                total_price += obj_value * obj_multi_buy_value;
            if (obj_type == 'minus_fix_sum')
                total_price -= obj_value * obj_multi_buy_value;
            if (obj_type == 'plus_percent')
                total_price += price * obj_value * obj_multi_buy_value / 100;
            if (obj_type == 'minus_percent')
                total_price -= price * obj_value * obj_multi_buy_value / 100;
        });

        var additional_sum = total_price - total_price_variant_for_multi;

        //$(this).closest('li').find('input[name*=price]').val(option.attr('data-price'));
        $(this).closest('li').find('span.purchase-price').attr('data-original-price', option.attr('data-price'));
        $(this).closest('li').find('span.purchase-price').attr('data-price', total_price);
        $(this).closest('li').find('span.purchase-price').attr('data-price-one-pcs', total_price);
        $(this).closest('li').find('span.purchase-price').attr('data-price-for-mul', total_price_variant_for_multi);
        $(this).closest('li').find('span.purchase-price').attr('data-price-additional-sum', additional_sum);

        total_price_formatted = String(total_price.toFixed()).replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        $(this).closest('li').find('span.purchase-price').html(total_price_formatted);
        $(this).closest('li').find('span.sku_value').html(option.attr('data-sku'));

        $(this).closest('li').find('button.btn-edit-modificators').addClass('not-saved-purchase');
        /*
                data-original-price="{if !empty($purchase->variant)}{$purchase->variant->price|convert:NULL:false}{else}{$purchase->price|convert:NULL:false}{/if}"
                data-price="{$purchase->price|convert:NULL:false}"
                data-price-one-pcs="{$purchase->price|convert:NULL:false}"
                data-price-for-mul="{$purchase->price_for_mul|convert:NULL:false}"
                data-price-additional-sum="{$purchase->additional_sum|convert:NULL:false}"
        */
        recalc_order();
        return false;
    });


    $('#refreshpart').on("click", "li[data-id]", function(){
        var li = $(this);
        var product_id = li.attr('data-id');
        var image_src = "";
        /*var image = li.find('div.apimage img');
        if (image.length > 0)
            image_src = image.attr('src');*/
        var image_src = li.data('image');
        var product_name = li.data('name');

        $('ol.product-list').append('<li data-product="'+product_id+'" class="order-purchases"><div class="order-productimage"><img src="'+image_src+'"/></div><div class="right-side"><div class="controllinks"><a href="#" class="delete-item""><i class="fa fa-times"></i></a></div><div class="product-price-order"><span class="purchase-price" data-price=""></span>&nbsp;{$main_currency->sign}</div><div class="product-quantity-order"><div class="input-group"><span class="input-group-btn"><button data-type="minus" class="btn btn-default btn-sm" type="button"><i class="fa fa-minus"></i></button></span><input type="text" class="form-control input-sm" name="purchases[amount][]" value="1"><span class="input-group-btn"><button data-type="plus" class="btn btn-default btn-sm" type="button"><i class="fa fa-plus"></i></button></span></div></div></div><div class="productname"><input type="hidden" name="purchases[product_id][]" value="'+product_id+'"/><input type="hidden" name="purchases[product_name][]" value="'+product_name+'"/><a href="{$config->root_url}{$product_module->url}?id='+product_id+'" target="_blank">'+product_name+'</a><div class="selectvariant"></div><div class="variantname"></div></div></li>');

        $.ajax({
            type: 'POST',
            url: '{$config->root_url}/ajax/get_data.php',
            data: {
                'object': 'products',
                'mode': 'get_variants',
                'id': product_id,
                'session_id': '{$smarty.session.id}'
            },
            {*type: 'GET',
            url: "{$config->root_url}{$product_module->url}?mode=get_variants&ajax=1&id="+product_id,*}
            success: function(data) {
                if (data.success)
                {
                    if (data.data.length > 1)
                    {
                        var select = $('<select name="purchases[variant_id][]"></select>');
                        for(var index in data.data)
                        {
                            var variant = data.data[index];
                            var status = "";
                            var status_class = "";
                            if (variant.stock < 0)
                            {
                                status = " (под заказ)";
                                status_class = "to-order";
                            }
                            if (variant.stock == 0)
                            {
                                status = " (нет в наличии)";
                                status_class = "out-of-stock";
                            }
                            select.append('<option value="'+variant.id+'" data-price="'+variant.price+'" data-price-one-pcs="'+variant.price+'" data-price-for-mul="'+variant.price+'" data-price-additional-sum="0" data-stock="'+variant.stock+'" data-format-price="'+variant.format_price+'" data-sku="'+variant.sku+'" class="'+status_class+'">'+variant.name+status+'</option>');
                        }
                        $('ol.product-list li[data-product="'+product_id+'"] div.selectvariant').append(select);
                    }
                    else
                        $('ol.product-list li[data-product="'+product_id+'"] div.selectvariant').append('<input type="hidden" name="purchases[variant_id][]" value="'+data.data[0].id+'"/><label>'+data.data[0].name+'</label>');
                    var var_sku = "";
                    var var_name = "";
                    var var_price = "";
                    var var_show_modificators = false;
                    if (data.data.length >= 1)
                    {
                        var_sku = data.data[0].sku;
                        var_name = data.data[0].name;
                        var_price = data.data[0].price;
                        var_format_price = data.data[0].format_price;
                        var_show_modificators = data.data[0].show_modificators;
                    }
                    if (var_sku.length > 0)
                        $('ol.product-list li[data-product="'+product_id+'"] div.variantname').append('<input type="hidden" name="purchases[variant_name][]" value="'+var_name+'"/><input type="hidden" name="purchases[sku][]" value="'+var_sku+'"/><div class="artikul">Артикул: <span class="sku_value">'+var_sku+'</span></div>');
                    else
                        $('ol.product-list li[data-product="'+product_id+'"] div.variantname').append('<input type="hidden" name="purchases[variant_name][]" value="'+var_name+'"/><input type="hidden" name="purchases[sku][]" value="'+var_sku+'"/>');
                    //$('ol.product-list li[data-product="'+product_id+'"] input[name*="price"]').val(var_price);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').attr('data-price', var_price);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').attr('data-price-one-pcs', var_price);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').attr('data-price-for-mul', var_price);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').attr('data-price-additional-sum', 0);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').attr('data-original-price', var_price);
                    $('ol.product-list li[data-product="'+product_id+'"] span.purchase-price').html(var_format_price);

                    if (data.data[0].stock < 0)
                        $('ol.product-list li[data-product="'+product_id+'"] div.productname').append('<div class="cart-pod-zakaz"><span><i class="fa fa-truck"></i> Под заказ</span></div>');

                    if (var_show_modificators)
                        $('ol.product-list li[data-product="'+product_id+'"] div.productname').append('<button type="button" class="btn btn-default btn-sm btn-edit-modificators not-saved-purchase">Редактировать модификаторы</button>');
                }
                li.fadeOut('slow').remove();
                $('#searchField').val('');
                $('#refreshpart').html('');
                $('#refreshpart').hide();
                $('#related').nestable({
                    maxDepth: 1
                });
                recalc_order();
            }
        });
    });

    $('ol.product-list').on('click', 'button[data-type=plus]', function(){
        var input = $(this).closest('div').find('input');
        var max_stock = 99999;
        var select = $(this).closest('li').find('select');
        if (select.length > 0){
            max_stock = select.find('option:selected').data('stock');
            if (max_stock <= 0)
                max_stock = 1;
        }
        if (input.length > 0)
        {
            var value = parseInt(input.val()) + 1;
            if (value > max_stock)
                value = max_stock;
            input.val(value);
            recalc_order();
        }
    });

    $('ol.product-list').on('click', 'button[data-type=minus]', function(){
        var input = $(this).closest('div').find('input');
        if (input.length > 0)
        {
            var value = parseInt(input.val()) - 1;
            if (value <=0 )
                value = 1;
            input.val(value);
            recalc_order();
        }
    });

    $('select[name=delivery_id]').change(function(){
        $('input[name=delivery_price]').val($(this).find('option:selected').attr('data-price'));
        recalc_order();
    });

    $('input[name=delivery_price], input[name=discount]').keyup(function(){
        recalc_order();
    });

    $('input[name=separate_delivery]').change(function(){
        recalc_order();
    });

    $('ol.product-list').on("keyup", "input[name*=amount]", function(){
        recalc_order();
    });

    $('a.order-edit').click(function(){
        $(this).hide();
        $('#edit-order-label').show();
        $('#edit-order').show();
        $("form#order input[name*=amount]").each(function(){
            $(this).show();
        });

        $("form#order div.product-quantity-order span.input-group-btn").each(function(){
            $(this).show();
        });

        $("form#order div.product-quantity-order span:not(.input-group-btn)").each(function(){
            $(this).hide();
        });

        $("form#order select[name*=variant_id]").each(function(){
            $(this).show();
        });
        $("form#order label.variant-name").each(function(){
            $(this).hide();
        });
        $("ol.product-list li a.delete-item").each(function(){
            $(this).show();
        });

        //$('div.cart-modifier').hide();
        $('.btn-edit-modificators').show();
        $('.btn-edit-modificators-orders').show();

        return false;
    });

    $('#order-detail-mode').click(function(){
        $(this).hide();
        $('#order-detail-view').hide();
        $('#order-detail-edit').show();
        $('textarea[name=comment]').trigger('autosize.resize');
        $('textarea[name=address]').trigger('autosize.resize');
        return false;
    });


    $('#order-user-mode').click(function(){
        $(this).hide();
        $('#order-user-view').hide();
        $('#order-user-edit').show();
        return false;
    });

    $("#phone_code,#phone2_code,#phone,#phone2").keydown(function(event) {
            if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
                (event.keyCode == 65 && event.ctrlKey === true) ||
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                     return;
            }
            else {
                if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                    event.preventDefault();
                }
            }
        });
    $("#phone_code").keyup(function(){
        if ($(this).val().length == 3)
            $("#phone").focus();
    });
    $("#phone2_code").keyup(function(){
        if ($(this).val().length == 3)
            $("#phone2").focus();
    })

    $("form#order a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $("form#order a.saveclose").click(function(){
        if ($(this).closest('form').find('input[name=close_after_save]').val() == "1")
            return false;
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#order a.saveadd").click(function(){
        if ($(this).closest('form').find('input[name=add_after_save]').val() == "1")
            return false;
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#order").submit(function(){
        $(this).find('input:disabled').each(function(){
            $(this).prop('disabled', false);
        });
    });
</script>
