{* Title *}
{$meta_title='Редактирование статуса заказов' scope=parent}

<form method=post id="status">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

     <div class="row">
        <div class="col-md-8">
        <legend>{if $status}Редактирование статуса заказов{else}Добавление статуса заказов{/if}</legend>
        <div class="row">
        <div class="col-md-8">
        <label>Наименование статуса заказов</label>
            <div class="input-group">
            <input type="text" class="form-control" name="name" placeholder="Введите наименование статуса заказов" value="{$status->name|escape_string}"/>
            <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id статуса">id:{$status->id}</a></span>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $status->enabled || !$status}active{/if}">
                        <input type="radio" name="enabled" id="option1" value=1 {if $status->enabled || !$status}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $status && !$status->enabled}active{/if}">
                        <input type="radio" name="enabled" id="option2" value=0 {if $status && !$status->enabled}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        
        <div class="row">
        <div class="col-md-8">
        
        <div class="form-group mt15">
            <label>Наименование группы заказов</label>
            <input type="text" class="form-control" name="group_name" placeholder="Введите наименование группы заказов" value="{$status->group_name|escape_string}"/>
        </div>
        </div>
        </div>
        
        
        <div class="form-group">
            <label>Тип статуса</label>
            <div class="btn-group noinline" data-toggle="buttons" id="status-types">
                <label class="btn btn-default4 on {if $status->status_type == 'processed'}active{/if}" data-toggle="popover" data-content="<strong>Заказы в работе</strong> — это те заказы с которыми Вы работаете, все они будут отображены в соответствующей вкладке, также все заказы в работе будут показаны у пользователя в личном кабинете в разделе 'Текущие заказы', в отличие от выполненных заказов пользователь может отменить заказ если он находится в 'Текущих заказах'">
                    <input type="radio" name="status_type" value="processed" {if $status->status_type == 'processed'}checked{/if}>Заказ в работе
                </label>
                <label class="btn btn-default4 off {if $status->status_type == 'finished'}active{/if}" data-toggle="popover" data-content="<strong>Выполненные заказы</strong> — это те заказы с которыми Вы завершили работу, их пользователь уже не может отменить, поскольку работа по ним завершена">
                    <input type="radio" name="status_type" value="finished" {if $status->status_type == 'finished'}checked{/if}>Заказ выполнен
                </label>
    </div>
        </div>
       
       
       <hr/>
       
       <div class="row">
        <div class="col-md-6">
       <div class="form-group">
            <label>Css-стиль статуса</label>
            <input type="text" class="form-control" name="css_class" placeholder="" value="{$status->css_class}"/>
        </div>
        
        {*
        <div class="form-group">
            <label>Цвет статуса</label>
            <input type="text" class="form-control" name="" placeholder="" value=""/>
        </div>
        *}
       </div>
       </div>
        
        </div>
        
        </div>
        <input type="hidden" name="id" value="{$status->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>

</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $('#status-types label.btn').popover({
            html: true,
            placement: 'bottom',
            trigger: 'hover',
            container: 'body'
        });
    });

    $("form#status a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#status a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#status a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>