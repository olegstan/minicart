<div class="panel panel-default main-orders topbtn">
    <div class="panel-heading">
        <h3 class="panel-title">Популярные поисковые запросы</h3>
    </div>
    <div class="panel-body">
        <p>График 10 популярных поисковых запросов - график в виде пирога</p>
    </div>
</div>

<legend>История поиска</legend>
        
{* Верх таблицы поисковых запросов *}
<table class="table table-striped searchtable">
<thead>
  <tr>
    <th class="search-id">ID</th>
    <th class="search-word"><a href="#"><i class="fa fa-sort-alpha-asc{*fa fa-sort-alpha-desc*}"></i> Поисковый запрос</a></th>
    <th class="search-count"><a href="#"><i class="fa fa-sort-amount-asc {*fa fa-sort-amount-desc *}"></i> Кол-во запросов</a></th>
    <th class="search-date"><a href="#"><i class="fa fa-sort-amount-asc {*fa fa-sort-amount-desc *}"></i> Последний запрос</a></th>
    <th class="search-result"><a href="#"><i class="fa fa-sort-amount-asc {*fa fa-sort-amount-desc *}"></i> Есть результат</a></th>
  </tr>
</thead>
</table>
       
{* Низ таблицы поисковых запросов *}
<table class="table table-striped searchtable">
<tbody>
    {foreach $histories as $h}
        <tr>
            <td class="search-id">{$h->id}</td>
            <td class="search-word"><a href="{$config->root_url}{$products_module->url}{url add=['format'=>'search','keyword'=>$h->keyword]}" target="_blank">{$h->keyword}</a></td>
            <td class="search-count">{$h->amount}</td>
            <td class="search-date">{$h->last_updated|date}</td>
            <td class="search-result">{if ($h->products_count+$h->categories_count)>0}Есть{else}Нет{/if}</td>
        </tr>
    {/foreach}
</table>
       
<ul class="pagination">
  <li><a href="#"><i class="fa fa-angle-left"></i> Назад</a></li>
  <li><a href="#">1</a></li>
  <li><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">Вперед <i class="fa fa-angle-right"></i></a></li>
</ul>