            <div class="panel panel-default main-orders topbtn">
                <div class="panel-heading">
                    <h3 class="panel-title">Воронка продаж</h3>
                    <div class="panel-buttons">
                        <div class="btn-group" data-toggle="buttons">
                          <label class="btn btn-default btn-xs active">
                            <input type="radio" name="options" id="option1"> День
                          </label>
                          <label class="btn btn-default btn-xs">
                            <input type="radio" name="options" id="option2"> Неделя
                          </label>
                          <label class="btn btn-default btn-xs">
                            <input type="radio" name="options" id="option3"> Месяц
                          </label>
                        </div>
                    </div>
                </div>
                  <div class="panel-body">
            <table class="table table-striped">
            <tbody>
              <tr>
                <td>Посетители</td>
                <td>1447</td>
              </tr>
              <tr>
                <td>Посетители, положившие товары в корзину <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Показывает количество разных посетителей которые положили 1 или несколько товаров в корзину в течение определенного времени и после этого оформили или не оформили заказ" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>115</td>

              </tr>
              <tr>
                <td>Оформленных заказов <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Количество оформленных заказов" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>35</td>
              </tr>

              <tr>
                <td>Выполненных заказов <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Количество заказов со статусом Выполнен" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>25</td>
              </tr>
            </tbody>
          </table>
          </div>
        </div>


<div class="panel panel-default main-orders topbtn">
                <div class="panel-heading">
                    <h3 class="panel-title">Общие данные</h3>
                </div>
                  <div class="panel-body">
            <table class="table table-striped">
            <tbody>
              <tr>
                <td>Товаров <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Показывается сколько всего товаров и сколько из них отображено на сайте и находятся в видимых категориях, то есть их можно найти на сайте" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>1290 (Всего 1440, скрыто или в скрытых категориях 240)</td>
              </tr>
              
              <tr>
                <td>Категорий <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Показывается сколько всего категорий и сколько из них отображено на сайте, то есть видимы" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>20 (Всего 30, скрыто 10)</td>
              </tr>
              
              
              <tr>
                <td>Заказов</td>
                <td>115</td>

              </tr>


              <tr>
                <td>Средний чек заказа <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Общая сумма всех заказов поделенная на количество заказов" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>3 040 руб.</td>
              </tr>

              <tr>
                <td>Наиболее часто встречающаяся сумма заказа <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Эта величина более точно показывает среднюю сумму заказа" class="fa fa-legend"><i class="fa fa-info-circle" ></i></a></td>
                <td>2 540 руб.</td>
              </tr>

              <tr>
                <td>Среднее количество товаров в заказе</td>
                <td>3.4</td>
              </tr>

            </tbody>
          </table>
          </div>
        </div>
        

