{$meta_title = "Дизайн писем" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    </div>

<legend>Дизайн писем</legend>

<div class="form-group">
    <label>Верхняя часть</label>
    <textarea class="form-control" rows="10" name="up_part">{$settings->design_email_up_part}</textarea>
</div>

<div class="form-group">
    <label>Нижняя часть</label>
    <textarea class="form-control" rows="10" name="down_part">{$settings->design_email_down_part}</textarea>
</div>

</form>

<script type="text/javascript">
    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>