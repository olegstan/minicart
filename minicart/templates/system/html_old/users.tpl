{* Title *}
{$meta_title='Пользователи' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text" class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove" {if !$keyword}style="display:none;"{/if}><i class="fa fa-times"></i></button>
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>              
                </span>
            </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['group_id'=>$params_arr['group_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-9">
            <legend>Пользователи</legend>

            <div id="refreshpart" class="dd">{include file='users-refreshpart.tpl'}</div>

            {* Массовые операции для пользователей *}
            {if $allow_edit_module}
            <div class="form-inline massoperations">
                <div class="checkall">Выделить все &uarr;</div>
                <select id="mass_operation" class="form-control">
                    <option value='1'>Сделать видимыми</option>
                    <option value='2'>Скрыть</option>
                    <option value='3'>Удалить</option>
                </select>
                <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
            </div>
            {/if}
        </div>
        
        <div class="col-md-3">
            <div class="list-group ordergroups">
            <a href="{$config->root_url}{$module->url}" class="list-group-item {if !array_key_exists('group_id', $params_arr)}active{/if}"><span class="badge">{$all_users_count}</span> Все группы</a>
            {foreach $user_groups as $group}
                <a href="{$config->root_url}{$module->url}{url add=['group_id'=>$group->id]}" class="list-group-item {if array_key_exists('group_id', $params_arr) && $params_arr['group_id']==$group->id}active {/if}>">
                <span class="badge">{$group->users_count}</span>
                    {$group->group_name|escape}
                </a>
            {/foreach}
         </div>
        


 <div class="panel-group" id="accordion">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#help">
          Помощь по разделу
        </a>
      </h4>
    </div>
    <div id="help" class="panel-collapse collapse">
      <div class="panel-body">
            <p>Поиск пользователей идет по 3 параметрам:</p>
            <ol>
                <li>Имя пользователя;</li>
                <li>Почта в формате ivanov@mail.ru;</li>
                <li>Телефон в формате +79171234567;</li>
            </ol>
      </div>
    </div>
  </div>

</div>






    </form>
</div>
</div>

</div>

<script type="text/javascript">

    var search_ajax_context;

    function users_request_ajax(params, jump_to_body){
        $('#users-spinner').show();
        var url = "{$config->root_url}{$module->url}";

        if (params != undefined && params.length>0)
        {
            url += "?";
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    if (params.length > 1)
                        url += "?";
                    break;
                }
            }

            var index2 = 0;
            for(var index in params)
            {
                if (params[index].key != "url")
                {
                    if (index2 > 0)
                        url += "&";
                    url += params[index].key + "=" + encodeURI(params[index].value);
                    index2 += 1;
                }
            }
        }
        var full_url = url;
        if (params == undefined || params.length == 0 || (params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "?";
        if (params != undefined && params.length > 0 && !(params.length == 1 && params[0].key == 'url' && params[0].value.indexOf('?')==-1))
            full_url += "&";
        full_url += "ajax=1";

        if (jump_to_body != undefined && jump_to_body)
            $("body,html").animate({
                scrollTop:0
            }, 100);

        if (search_ajax_context != null)
            search_ajax_context.abort();

        search_ajax_context = $.ajax({
            type: 'GET',
            url: full_url,
            success: function(data) {
                if (typeof history.pushState != 'undefined') {
                    history.pushState(null,null,encodeURI(url));
                }
                $('#refreshpart').html(data.users);
                $('#refreshpart').nestable({
                    maxDepth: 1
                });

                $('#users-spinner').hide();
                $('a[data-toggle="tooltip"]').tooltip();
                $('a[data-toggle="tooltip"]').click(function(){
                    if (!$(this).hasClass('allow-jump'))
                        return false;
                });

                search_ajax_context = null;
            }
        });
        return false;
    }

    {if $users}
        $(document).ready(function(){
            $('#refreshpart').nestable({
                maxDepth: 1
            });
        });
    {/if}

    $('#refreshpart').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_user_item(obj);
        });
        return false;
    });

    function delete_user_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;

        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
            {
                var url = $('ol.dd-list').attr('data-href');
                if (url == undefined)
                    return false;
                var params = [{
                        'key': 'url',
                        'value': url
                    }];
                users_request_ajax(params, false);
                return false;
            }
            return false;
        });
        return false;
    }

    $('#refreshpart').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        $('#searchField').triggerHandler('keyup');
    });

    $('#searchField').on('keydown', function(e){
        if (e.keyCode == 13)
            return false;
    });

    // поиск keyword
    $('#searchField').on('keyup', function(e){
        var params = [];

        if (e.which == 13)
            return false;

        if ($(this).val().length > 0)
        {
            $('#searchCancelButton').show();
            params.push({
                'key': 'keyword',
                'value': encodeURIComponent($(this).val())
            });
        }
        else
            $('#searchCancelButton').hide();

        users_request_ajax(params, true);
        return false;
    });

    // сброс keyword
    $('#searchCancelButton').click(function(){
        var params = [];
        $('#searchField').val('');
        users_request_ajax(params, true);
        $('#searchCancelButton').hide();
        return false;
    });

    $('#refreshpart').on('click', 'ul.pagination a', function(){
        var url = $(this).attr('href');
        if (url == undefined)
            return false;
        var params = [{
                'key': 'url',
                'value': url
            }];
        users_request_ajax(params, true);
        return false;
    });

    $('div.checkall').click(function(){
        $("#refreshpart input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#refreshpart input[type=checkbox]:checked").length;

        if (operation == 3 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#refreshpart input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#refreshpart input[type=checkbox]:checked").each(function(){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item');
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item');
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                            }
                        });
                    break;
                {*case 3:    //Удаление
                    var subitem = item.find('a.delete-item');
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                                location.reload();
                        }
                    });
                    break;*}
            }
        });
        return false;
    });
</script>
