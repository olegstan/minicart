{* Title *}
{$meta_title='Статусы и бейджи' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Статусы и бейджи</legend>

                <div id="sortview">
                <div class="itemcount">Статусов: {$statuses_count}</div>
                </div>

                <div id="new_menu" class="dd">
                
                 {if $orders_statuses}
                 {include file='pagination.tpl'}
                 <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $orders_statuses as $status}
                            <li class="dd-item dd3-item" data-id="{$status->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$status->id]}"{/if}>{$status->name|escape}</a>
                                    <div class="controllinks">
                                        <code class="opcity50">{$status->id}</code>
                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$status->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $status->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$status->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    </div>
                    {include file='pagination.tpl'}

                    {else}
                        <p>Нет статусов</p>
                    {/if}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $orders_statuses}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_order_status_item(obj);
        });
        return false;
    });

    function delete_order_status_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });
</script>