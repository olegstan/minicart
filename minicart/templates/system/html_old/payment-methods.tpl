{* Title *}
{$meta_title='Методы оплаты' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="deliveries">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">



        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>
        
        
        <legend>Методы оплаты</legend>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-9">
                <div id="new_menu" class="dd">

                    {if $payment_methods}
                    <ol class="dd-list">
                        {foreach $payment_methods as $method}
                            <li class="dd-item dd3-item" data-id="{$method->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$method->id]}"{/if}>{$method->name|escape}</a>
                                    
                                    <div class="typeof">                                    {if $method->operation_type == 'plus_fix_sum'}<span class="label label-default">+{rtrim(rtrim($method->operation_value, '0'), '.')} {$main_currency->sign}</span>{elseif $method->operation_type == 'minus_fix_sum'}<span class="label label-default">-{rtrim(rtrim($method->operation_value, '0'), '.')} {$main_currency->sign}</span>{elseif $method->operation_type == 'plus_percent'}<span class="label label-default">+{rtrim(rtrim($method->operation_value, '0'), '.')}%</span>{elseif $method->operation_type == 'minus_percent'}<span class="label label-default">-{rtrim(rtrim($method->operation_value, '0'), '.')}%</span>{/if}
                                    </div>
                                    
                                    
                                    <div class="controllinks">
                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$method->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $method->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$method->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                    </div>
                                    
                                    
                                    
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    {else}
                        <p>Нет способов оплаты</p>
                    {/if}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $payment_methods}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_payment_item(obj);
        });
        return false;
    });

    function delete_payment_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });
</script>