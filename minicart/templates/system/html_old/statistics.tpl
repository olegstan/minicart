{* Главная страница админки *}

<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data" action="{$config->root_url}{$module->url}mode/{$mode}/">
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    {* Тело страницы *}
    <div class="controlgroup">
        <a href="" class="btn btn-success btn-sm save"><i class="fa fa-check"></i>  Сохранить</a>
    </div>

    <div class="row">
        <div class="col-md-9" id="statistics-content">

            <!-- Параметры -->
            {if $mode}
                {include file='statistics/'|cat:$mode|cat:'.tpl'}
            {else}
                {include file='statistics/totals.tpl'}
            {/if}
            <!-- Параметры (The End)-->

        </div>

        <div class="col-md-3">
            <div class="list-group" id="statistics-switch">
                <a href="{$config->root_url}{$module->url}mode/totals/" data-statistics="totals" class="list-group-item {if $mode=="totals" || !$mode}active{/if}">Общие данные</a>
                <a href="{$config->root_url}{$module->url}mode/visitors/" data-statistics="visitors" class="list-group-item {if $mode=="visitors"}active{/if}">Посетители</a>
                <a href="{$config->root_url}{$module->url}mode/orders/" data-statistics="orders" class="list-group-item {if $mode=="orders"}active{/if}">Заказы</a>
                <a href="{$config->root_url}{$module->url}mode/products/" data-statistics="products" class="list-group-item {if $mode=="products"}active{/if}">Товары</a>
                <a href="{$config->root_url}{$module->url}mode/categories/" data-statistics="categories" class="list-group-item {if $mode=="categories"}active{/if}">Категории</a>
                <a href="{$config->root_url}{$module->url}mode/history/" data-statistics="history" class="list-group-item {if $mode=="history"}active{/if}">История поисковых запросов</a>
                <a href="{$config->root_url}{$module->url}mode/orders-time/" data-statistics="orders-time" class="list-group-item {if $mode=="orders-time"}active{/if}">В какое время заказывают</a>
            </div>
        </div>

    </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $('#statistics-switch a').click(function(){
        $('#statistics-switch a.active').removeClass('active');
        $(this).addClass('active');
        var template = $(this).attr('data-statistics');
        var new_url = $(this).attr('href');
                $(this).closest('form').attr('action', new_url);
        if (template.length > 0)
            $.ajax({
                type: 'GET',
                url: '{$config->root_url}{$module->url}get/'+template,
                success: function(data) {
                    if (data.success)
                    {
                        if (typeof history.pushState != 'undefined') {
                            history.pushState(null,null,encodeURI(new_url));
                        }
                        $('#statistics-content').html(data.data);
                        $('a[data-toggle="tooltip"]').tooltip();
                    }
                }
            });
        return false;
    });
</script>