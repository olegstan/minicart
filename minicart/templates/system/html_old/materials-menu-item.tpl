{* Title *}
{$meta_title='Редактирование пункта меню' scope=parent}

<form method=post id="pages_menu_item">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        {*if $message_success}
        <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <strong>Изменения сохранены!</strong>
        </div>
        {/if*}

     <div class="row">
        <div class="col-md-9">
        <legend>{if $pages_menu_item->id}Редактирование пункта меню{else}Добавление пункта меню{/if}</legend>
        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                <label>Наименование пункта меню</label>
                <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="Введите наименование пункта меню" value="{$pages_menu_item->name|escape_string}"/>
                <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id пункта меню">id:{$pages_menu_item->id}</a></span>
                </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="statusbar">

                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $pages_menu_item->is_visible || !$pages_menu_item}active{/if}">
                    <input type="radio" name="visible" id="option1" value=1 {if $pages_menu_item->is_visible || !$pages_menu_item}checked{/if}>Активен
                  </label>
                  <label class="btn btn-default4 off {if $pages_menu_item && !$pages_menu_item->is_visible}active{/if}">
                    <input type="radio" name="visible" id="option2" value=0 {if $pages_menu_item && !$pages_menu_item->is_visible}checked{/if}>Скрыт
                  </label>
                </div>
                <div class="statusbar-text">Статус:</div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="form-group">
                    <label class="label-control">Меню</label>
                    <select name="menu_id" class="form-control" id="menu_id">
                        {foreach $menu as $m}
                            <option value="{$m->id}" {if $pages_menu_item->menu_id == $m->id || $menu_id == $m->id}selected{/if}>{$m->name}</option>
                        {/foreach}
                    </select>
                </div>
             </div>
             <div class="col-md-4">
             
                 <div class="statusbar">

                <div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-default4 on {if $pages_menu_item->is_main}active{/if}">
                    <input type="radio" name="is_main" id="option1" value=1 {if $pages_menu_item->is_main}checked{/if}>Да
                  </label>
                  <label class="btn btn-default4 off {if !$pages_menu_item || !$pages_menu_item->is_main}active{/if}">
                    <input type="radio" name="is_main" id="option2" value=0 {if !$pages_menu_item || !$pages_menu_item->is_main}checked{/if}>Нет
                  </label>
                </div>
                <div class="statusbar-text">Главная:</div>
                </div>
             
             
             </div>
        </div> 
        
        <div class="row">
            <div class="col-md-8">
               
                <div class="form-group">
                <label class="label-control">Родитель</label>
                <select name="parent_id" class="form-control" id="parent_id">
                </select>
                </div>
            
            
        
                <div class="form-group">
                <label class="label-control">Тип пункта меню</label><i class="fa fa-spinner fa fa-spin fa fa-large" id="menu-spinner" style="display:none;"></i>
                    <select name="object_type" class="form-control" id="object_type">
                        <option value="none" data-description="" {if !$pages_menu_item->id || $pages_menu_item->object_type == "none"}selected{/if}>Не выбрано</option>
                        <option value="material" data-description="Выберите материал" {if $pages_menu_item->object_type == "material"}selected{/if}>Материал</option>
                        <option value="material-category" data-description="Выберите категорию материалов" {if $pages_menu_item->object_type == "material-category"}selected{/if}>Категория материалов</option>
                        <option value="product" data-description="Выберите товар" {if $pages_menu_item->object_type == "product"}selected{/if}>Товар</option>
                        <option value="category" data-description="Выберите категорию товаров" {if $pages_menu_item->object_type == "category"}selected{/if}>Категория товаров</option>
                        <option value="href" data-description="Введите ссылку" {if $pages_menu_item->object_type == "href"}selected{/if}>Ссылка</option>
                        <option value="separator" data-description="" {if $pages_menu_item->object_type == "separator"}selected{/if}>Разделитель</option>
                        <option value="sitemap" data-description="" {if $pages_menu_item->object_type == "sitemap"}selected{/if}>Карта сайта</option>
                    </select>
                </div>
            </div>
            <div class="col-md-8" id="upd-area" style="display:none;"> {* Эта часть всегда на select2 - выпадающий список с поиском - появляется только когда выбрали первый пункт *}
                <label class="label-control"><span id="object-description">Выберите материал</span></label>
                <input type="hidden" name="object_id_select2" id="object_id_input" style="width:100%;" data-placeholder="Не выбрано" value="{$pages_menu_item->object_id}" data-text="{$select2_text}"/>
                <select name="object_id_select" class="form-control" id="object_id_select" style="display:none;">
                </select>
                <input type="textbox" name="object_text" class="form-control" id="object_id_textbox" style="width:100%;" data-placeholder="" value="{$pages_menu_item->object_text|escape_string}" style="display:none;"/>
            </div>
        </div>
        
         <hr />
         <p>&nbsp;</p>
         <p>&nbsp;</p>
         <p>&nbsp;</p>
         <p>&nbsp;</p>
        </div>
        
        <div class="col-md-3">
            <legend>Оформление</legend>
            <div class="form-group">
                <label>CSS-стиль пункта меню</label>
                <input type="textbox" name="css_class" class="form-control" value="{$pages_menu_item->css_class}">
            </div>
        </div>
        
        <input type="hidden" name="posted_menu_id" value="{$menu_id}"/>
        <input type="hidden" name="id" value="{$pages_menu_item->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>

       </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Наименование пункта меню не может быть пустым{elseif $message_error == "empty_material"}Материал не выбран{elseif $message_error == "empty_material_category"}Категория материалов не выбрана{elseif $message_error == "empty_product"}Товар не выбран{elseif $message_error == "empty_category"}Категория не выбрана{/if}')
        {/if}
        $('#menu_id').trigger('change');
        $('#object_type').trigger('change');
    });

    $('#menu_id').change(function(){
        $('#parent_id').html('');
        var option = $(this).find('option:selected');
        $.ajax({
            type: 'POST',
            url: '{$config->root_url}/ajax/get_data.php',
            data: {
                'object': 'materials',
                'mode': 'get_menu_items',
                'menu_id': option.val(),
                'exclude': {if $pages_menu_item->id}{$pages_menu_item->id}{else}0{/if},
                'session_id': '{$smarty.session.id}'
            },
            {*type: 'GET',
            url: "{$config->root_url}{$main_module->url}?action=get_menu_items&menu_id="+option.val()+"&exclude={if $pages_menu_item->id}{$pages_menu_item->id}{else}0{/if}",*}
            success: function(data) {
                if (data.success){
                    for(var index in data.data){
                        var row = data.data[index];
                        var option = $('<option></option>');
                        option.val(row.id);
                        option.addClass(row.class);
                        option.html(row.text);
                        {if $pages_menu_item->id}if (row.id == {$pages_menu_item->parent_id})
                            option.prop('selected', true);{/if}
                        $('#parent_id').append(option);
                    }
                }
            }
        });
    });

    $('#object_type').change(function(){

        $('#object_id_input').select2('destroy');
        $('#object_id_select').html('').hide();
        $('#object_id_textbox').hide();

        var option = $(this).find('option:selected');
        var value = option.val();
        var descr = option.attr('data-description');

        $('#object-description').html(descr);

        switch(value){
            case 'material':
                $('#object_id_input').select2({
                    placeholder: "Не выбрано",
                    query: function(options){
                        var term = options.term;
                        $.getJSON( "{$config->root_url}/ajax/get_data.php?object=materials&mode=select2&keyword="+term, function( data, status, xhr ) {
                            options.callback({
                                results: data.data
                            });
                        });
                    },
                    initSelection: function(element, callback){
                        var data = {
                            id: element.val(),
                            text: element.attr('data-text')
                        };
                        callback(data);
                    }
                });
                $('#object_id_input').focus();
                $('#upd-area').show();
                break;
            case 'material-category':
                $('#menu-spinner').show();
                $.ajax({
                    type: 'POST',
                    url: '{$config->root_url}/ajax/get_data.php',
                    data: {
                        'object': 'materials',
                        'mode': 'categories_list',
                        'session_id': '{$smarty.session.id}'
                    },
                    {*type: 'GET',
                    url: "{$config->root_url}{$materials_categories_module->url}?ajax=1&format=select2",*}
                    success: function(data) {
                        if (data.success){
                            for(var index in data.data){
                                var row = data.data[index];
                                var option = $('<option></option>');
                                option.val(row.id);
                                option.addClass(row.class);
                                option.html(row.text);
                                {if $pages_menu_item->id}if (row.id == {$pages_menu_item->object_id})
                                option.prop('selected', true);{/if}
                                $('#object_id_select').append(option);
                            }
                            $('#object_id_select').show();
                            $('#object_id_select').focus();
                            $('#upd-area').show();
                            $('#menu-spinner').hide();
                        }
                    }
                });
                break;
            case 'product':
                $('#object_id_input').select2({
                    placeholder: "Не выбран",
                    query: function(options){
                        var term = options.term;
                        {*$.getJSON( "{$config->root_url}{$products_module->url}?ajax=1&format=select2&keyword="+term, function( data, status, xhr ) {*}
                        $.getJSON( "{$config->root_url}/ajax/get_data.php?object=products&mode=select2&keyword="+term, function( data, status, xhr ) {
                            options.callback({
                                results: data.data
                            });
                        });
                    },
                    initSelection: function(element, callback){
                        var data = {
                            id: element.val(),
                            text: element.attr('data-text')
                        };
                        callback(data);
                    }
                });
                $('#object_id_input').focus();
                $('#upd-area').show();
                break;
            case 'category':
                $('#object_id_input').select2({
                    placeholder: "Не выбран",
                    query: function(options){
                        var term = options.term;
                        {*$.getJSON( "{$config->root_url}{$products_module->url}?ajax=1&format=select2&keyword="+term, function( data, status, xhr ) {*}
                        $.getJSON( "{$config->root_url}/ajax/get_data.php?object=categories&mode=select2&keyword="+term, function( data, status, xhr ) {
                            options.callback({
                                results: data.data
                            });
                        });
                    },
                    initSelection: function(element, callback){
                        var data = {
                            id: element.val(),
                            text: element.attr('data-text')
                        };
                        callback(data);
                    },
                    escapeMarkup: function(m) {
                        return m;
                    }
                });
                $('#object_id_input').focus();
                $('#upd-area').show();
                break;
            {*case 'category':
                $('#menu-spinner').show();
                $.ajax({
                    type: 'POST',
                    url: '{$config->root_url}/ajax/get_data.php',
                    data: {
                        'object': 'categories',
                        'mode': 'select2',
                        'session_id': '{$smarty.session.id}'
                    },
                    success: function(data) {
                        if (data.success){
                            for(var index in data.data){
                                var row = data.data[index];
                                var option = $('<option></option>');
                                option.val(row.id);
                                option.addClass(row.class);
                                option.html(row.text);
                                {if $pages_menu_item->id}if (row.id == {$pages_menu_item->object_id})
                                option.prop('selected', true);{/if}
                                $('#object_id_select').append(option);
                            }
                            $('#object_id_select').show();
                            $('#object_id_select').focus();
                            $('#upd-area').show();
                            $('#menu-spinner').hide();
                        }
                    }
                });
                break;*}
            case 'none':
                $('#upd-area').hide();
                break;
            case 'href':
                $('#object_id_textbox').show();
                $('#object_id_textbox').focus();
                $('#upd-area').show();
                break;
        }

        return false;
    });

    $("form#pages_menu_item a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#pages_menu_item a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#pages_menu_item a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>