{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}
<div id="sortview">
    <div class="itemcount">Отзывов: {$reviews_count}</div>
    <ul class="nav nav-pills" id="sort">
        <li class="disabled">Сортировать по:</li>

        <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'id'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'id' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'id' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'id', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'id' && $params_arr['sort_type'] == 'desc'}fa fa-sort-numeric-desc{else}fa fa-sort-numeric-asc{/if}"></i> <span>По порядку</span></a></li>

        {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'date'}
        <li><a class="btn btn-link btn-small allow-jump current" href="{$config->root_url}{$module->url}{if $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'date', 'sort_type'=>'asc','page'=>1]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort_type'] == 'desc'}Сначала старые{else}Сначала новые{/if}"><i class="{if $params_arr['sort_type'] == 'desc'}fa fa-sort-numeric-desc{else}fa fa-sort-numeric-asc{/if}"></i> <span>Новизне</span></a></li>
        {else}
        <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'date'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'date' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'date' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'date', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'date' && $params_arr['sort_type'] == 'desc'}fa fa-sort-numeric-desc{else}fa fa-sort-numeric-asc{/if}"></i> <span>Новизне</span></a></li>
        {/if}

    </ul>
</div>
    {if $reviews}
        {include file='pagination.tpl'}

        <ol class="reviews-list-products" data-href="{$config->root_url}{$module->url}{url current_params=$current_params}">
            {foreach $reviews as $review}
                <li data-id="{$review->id}" class="reviews-li {if !$review->moderated}notmoderated{/if}">

                    <div class="reviews-list-body">

                        <div class="last-review">
                            <div class="review-id"><input type="checkbox" value="" class="checkb"> <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$review->id, 'page'=>$current_page_num, 'sort'=>$params_arr['sort'], 'sort_type'=>$params_arr['sort_type']]}"{/if}>Отзыв {$review->id}</a>
                            <div class="review-controls">
                            
                                <a class="review-delete delete-item" href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'delete','id'=>$review->id,'ajax'=>1]}"><i class="fa fa-times"></i></a>
                            
                                <div class="review-onoff">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-default4 btn-xs on {if $review->is_visible}active{/if}" data-href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle','id'=>$review->id,'ajax'=>1]}">
                                            <input type="radio" value="1" {if $review->is_visible}checked=""{/if}><i class="fa fa-check"></i> Одобрен
                                        </label>
                                        <label class="btn btn-default4 btn-xs off {if !$review->is_visible}active{/if}" data-href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle','id'=>$review->id,'ajax'=>1]}">
                                            <input type="radio" value="0" {if !$review->is_visible}checked=""{/if}>Скрыт
                                        </label>
                                    </div>
                                </div>
                            </div>

                            </div>

                            <div class="review-body">
                                <div class="review-title">
                                    <div class="review-title-box">
                                    <div class="review-autor">{$review->name}</div>
                                    <div class="review-user-count">{$review->review_count} {$review->review_count|plural:'отзыв':'отзывов':'отзыва'}</div>
                                    <div class="review-date">{$review->day_str} {if $review->day_str != 'Сегодня' && $review->day_str != 'Вчера'}{$review->datetime|date}{else}в{/if} {$review->datetime|time}</div>
                                    
                                    <div class="review-rank">Ранг: {$review->rank}</div>
                                    
                                    {if $review->claims_count > 0}
                                    <div class="review-complain {*complain-no*}">Жалобы: {$review->claims_count}{*Жалоб нет*}</div>
                                    {/if}
                                    </div>
                                <div class="review-header">
                                {if $review->product_image}
                                <div class="reviews-list-image"><img src="{$review->product_image->filename|resize:'products':52:52}" alt=""></div>
                                {/if}
                    
                    
                    <div class="reviews-list-productname">
                            <a href="{$config->root_url}{$product_module->url}{url add=['id'=>$review->product->id]}" target="_blank">
                                {$review->product->name}
                                {*<div class="raiting rate{$review->product_rating->avg_rating*10}" title="Рейтинг товара {$review->product_rating->avg_rating_real|string_format:'%.1f'}">
                                </div>*}
                            </a>
                            {*<span>&mdash; {$review->product_rating->rating_count} {$review->product_rating->rating_count|plural:'оценка':'оценок':'оценки'}</span>*}
                        </div>
                    </div>
                                    
                                
                                </div>
                                <div class="review-body-text">                                 <div class="last-review-topline">
                                    <div class="raiting rate{$review->avg_rating*10}" title="Рейтинг товара {$review->rating|string_format:'%.1f'}"></div> {if $review->short} &mdash; {$review->short}{/if}
                                    </div>
                                
                                

                                    {if $review->pluses}
                                    <div class="review-verdict">
                                    <div class="review-verdict-type">
                                        <div class="review-verdict-header"><i class="fa fa-plus-circle" title="Достоинства"></i></div>
                                            <div class="review-verdict-text">{$review->pluses}</div>
                                            </div>

                                    </div>
                                    {/if}

                                     {if $review->minuses}
                                    <div class="review-verdict">
                                        <div class="review-verdict-type">
                                            <div class="review-verdict-header"><i class="fa fa-minus-circle" title="Недостатки"></i></div>
                                            <div class="review-verdict-text">{$review->minuses}</div>
                                        </div>
                                    </div>
                                    {/if}

                                    <div class="review-verdict">
                                        <div class="review-verdict-type">
                                            <div class="review-verdict-header"><i class="fa fa-pencil" title="Коментарий"></i></div>
                                            <div class="review-verdict-text">
                                                {$review->comments}
                                                {if $review->images && $settings->reviews_images_visible}
                                                    <ul class="review-images">
                                                    {foreach $review->images as $img}
                                                        <li><a href="{$img->filename|resize:'reviews':$settings->reviews_images_max_width:$settings->reviews_images_max_height}" class="fancybox-review" rel="group_review{$review->id}"><img src="{$img->filename|resize:'reviews':62:62}"></a></li>
                                                    {/foreach}
                                                    </ul>
                                                {/if}
                                            </div>
                                        </div>
                                    </div>
                                    
                                    {if $review->recommended}
                                    <div class="irecommend">
                                        <i class="fa fa-check"></i>Да - я рекомендую этот товар
                                    </div>
                                    {/if}

                                    
                                    <div class="usergrade">Отзыв полезен? <span class="usergrade-yes">Да</span> <span class="usergrade-numbers">{$review->helpful}</span> / <span class="usergrade-no">Нет</span> <span class="usergrade-numbers">{$review->nothelpful}</span></div>
                                    
                               

                                </div>

                            </div>
                        </div>
            </div>
                </li>

            {/foreach}

        </ol>
  
        {include file='pagination.tpl'}
    {else}
        <div class="non-reviews">Нет отзывов</div>
    {/if}


<script type="text/javascript">

    $(document).ready(function(){
        $(".fancybox-review").fancybox({
                prevEffect    : 'none',
                nextEffect    : 'none',
                helpers    : {
                    title    : {
                        type: 'outside'
                    },
                    thumbs    : {
                        width    : 50,
                        height    : 50
                    },
                    overlay : {
                        locked     : false
                    }
                }
        });
    });

</script>