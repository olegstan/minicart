{* Title *}
{if $modificator->id}{$meta_title='Редактирование модификатора' scope=parent}{else}{$meta_title='Добавление модификатора' scope=parent}{/if}

<form method=post id="modificator">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url add=['parent_id'=>$parent_id]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
            <div class="col-md-8">
                <legend>{if $modificator->id}Редактирование модификатора{else}Добавление модификатора{/if}</legend>
                
                
                <div class="row">
                   <div class="col-md-8">
                
                    <div class="form-group">
                        <label>Название</label>
                        <input type="text" class="form-control" name="name" placeholder="" value="{$modificator->name|escape_string}"/>
                    </div>
                </div>
                
                <div class="col-md-4">
                
                    <div class="statusbar">
                    
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if !$modificator || $modificator->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if !$modificator || $modificator->is_visible}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $modificator && !$modificator->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $modificator && !$modificator->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
                    <div class="statusbar-text">Статус:</div>
                    </div>
                </div>
                
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Группа модификаторов</label>
                            <select name="parent_id" class="form-control">
                                <option value='0' {if $modificator->parent_id == 0 || !$modificator->id}selected{/if}>Все модификаторы</option>
                                {foreach $modificator_groups as $cat}
                                    <option value='{$cat->id}' {if $modificator->parent_id == $cat->id || $cat->id == $parent_id}selected{/if}>- {$cat->name}</option>
                                {/foreach}
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <label>Вид операции</label>
                        <select class="form-control" name="type">
                          <option value="plus_fix_sum" {if $modificator->type == "plus_fix_sum"}selected{/if}>+ фиксированная сумма</option>
                          <option value="minus_fix_sum" {if $modificator->type == "minus_fix_sum"}selected{/if}>- фиксированная сумма</option>
                          <option value="plus_percent" {if $modificator->type == "plus_percent"}selected{/if}>+ % от цены товара</option>
                          <option value="minus_percent" {if $modificator->type == "minus_percent"}selected{/if}>- % от цены товара</option>
                          <option value="nothing" {if $modificator->type == "nothing"}selected{/if}>Не изменяем цену</option>
                        </select>
                    </div>
                    
                    <div class="col-md-4" id="fix-sum" {if !$modificator || $modificator->type == 'plus_fix_sum' || $modificator->type == 'minus_fix_sum'}{else}style="display: none;"{/if}>
                        <label>Сумма в {$main_currency->sign}</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="value_fix_sum" value="{if $modificator->type == 'plus_fix_sum' || $modificator->type == 'minus_fix_sum'}{$modificator->value}{/if}">
                          <span class="input-group-addon">{$main_currency->sign}</span>
                        </div>
                    </div>
                    
                    <div class="col-md-4" id="percent" {if $modificator->type == 'plus_percent' || $modificator->type == 'minus_percent'}{else}style="display: none;"{/if}>
                        <label>Укажите %</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="value_percent" value="{if $modificator->type == 'plus_percent' || $modificator->type == 'minus_percent'}{$modificator->value}{/if}">
                          <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>                               
                <hr/>
                <div class="form-group">
                    <label>Описание модификатора <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Описание будет отображено в подсказке по модификатору" class="fa fa-legend"><i class="fa fa-info-circle"></i></a></label>
                    <textarea class="form-control ckeditor" rows="3" name="description">{$modificator->description}</textarea>
                </div>
                
                
            </div>
        
            <div class="col-md-4">
                <legend>Изображения</legend>

                {include file='object-image-placeholder.tpl' object=$modificator images_object_name='modificators'}
                
                <legend>Настройки</legend>
                <div class="form-group">
                    <label>Модификатор применяется на несколько единиц товара единожды <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если отмечено 'Да' то модификатор будет 1 раз применяться 1 раз вне зависимости от количества купленных товаров, то есть если например модификатор это 'Подарочная упаковка' и купили 3 товара, то наценка сделается не на каждую еденицу, а только на первую, то есть модификатор применится единожды. Если же отмечено 'Нет' - модификатор будет применен к каждой еденице товара."><i class="fa fa-info-circle"></i></a></label>
                    <div class="btn-group noinline" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $modificator && !$modificator->multi_apply}active{/if}">
                            <input type="radio" name="multi_apply" value=0 {if $modificator && !$modificator->multi_apply}checked{/if}> Да
                        </label>
                        <label class="btn btn-default4 off {if !$modificator || $modificator->multi_apply}active{/if}">
                            <input type="radio" name="multi_apply" value=1 {if !$modificator || $modificator->multi_apply}checked{/if}> Нет
                        </label>
                    </div>
                </div>
        
                <div class="form-group">
                    <label>Модификатор можно купить несколько раз <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если отмечено 'Да' то рядом с модификатором появится выпадающий список, который позволит добавить к товару несколько модификаторов, например если товар это торт, а модификатор это пирожные, то к торту можно будет заказать несколько пирожных"><i class="fa fa-info-circle"></i></a></label>
                    <div class="btn-group noinline" data-toggle="buttons">
                        <label class="btn btn-default4 on {if $modificator->multi_buy}active{/if}">
                            <input type="radio" name="multi_buy" value=1 {if $modificator->multi_buy}checked{/if}> Да
                        </label>
                        <label class="btn btn-default4 off {if !$modificator || !$modificator->multi_buy}active{/if}">
                            <input type="radio" name="multi_buy" value=0 {if !$modificator || !$modificator->multi_buy}checked{/if}> Нет
                        </label>
                    </div>
                </div>

                <div class="form-group" id="multi-buy-amounts" {if !$modificator->multi_buy}style="display:none;"{/if}>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">min</span>
                                <input type="text" class="form-control" name="multi_buy_min" value="{if $modificator->multi_buy_min}{$modificator->multi_buy_min}{else}1{/if}">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="input-group">
                                <span class="input-group-addon">max</span>
                                <input type="text" class="form-control" name="multi_buy_max" value="{if $modificator->multi_buy_max}{$modificator->multi_buy_max}{else}100{/if}">
                            </div>
                        </div>
                    </div>
                    <p class="help-block">Укажите минимальное и максимальное количество единиц которые будут указаны у модификатора</p>
                </div>
            </div>
        </div>

        <input type="hidden" name="id" value="{$modificator->id}"/>
        <input type="hidden" name="return_parent_id" value="{$parent_id}"/>
        <input type="hidden" name="return_page" value="{if $page}{$page}{else}1{/if}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $('input[name=multi_buy]').change(function(){
        if ($(this).val() == 1)
            $('#multi-buy-amounts').show();
        else
            $('#multi-buy-amounts').hide();
    });

    $('form#modificator select[name=type]').change(function(){
        var option = $(this).find('option:selected').val();
        if (option == 'plus_fix_sum' || option == 'minus_fix_sum'){
            $('#fix-sum').show();
            $('#percent').hide();
        }
        else if (option == 'plus_percent' || option == 'minus_percent'){
            $('#fix-sum').hide();
            $('#percent').show();
        }
        else{
            $('#fix-sum').hide();
            $('#percent').hide();
        }
    });

    $("form#modificator a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#modificator a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#modificator a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>