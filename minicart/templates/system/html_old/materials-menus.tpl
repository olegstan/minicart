{* Title *}
{$meta_title='Меню' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="menus">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            {*<a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>*}
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                {if $menus}
                <legend>Пользовательские меню</legend>
                    <div id="new_menu" class="dd">
                        <ol class="dd-list">
                            {foreach $menus as $menu}
                                {if !$menu->is_static}
                                <li data-id="{$menu->id}" class="dd-item dd3-item">
                                    {*<div class="dd-handle dd3-handle"></div>*}
                                    <div class="dd3-content">
                                        <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$menu->id]}"{/if}>{$menu->name|escape}</a>
                                    </div>
                                    <div class="controllinks">
                                        <code class="opcity50">{$menu->id}</code>
                                        {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['action'=>'delete','id'=>$menu->id]}" class="delete-item"><i class="fa fa-times"></i></a>
                                        <a href="{$config->root_url}{$edit_module->url}{url add=['action'=>'toggle','id'=>$menu->id]}" class="toggle-item {if $menu->is_visible}light-on{else}light-off{/if}"></a>{/if}
                                    </div>
                                </li>
                                {/if}
                            {/foreach}
                        </ol>
                    </div>
                
                
                    <legend  class="mt20">Системные меню</legend>
                    <div id="new_menu" class="dd">
                        <ol class="dd-list">
                            {foreach $menus as $menu}
                                {if $menu->is_static}
                                <li data-id="{$menu->id}" class="dd-item dd3-item">
                                    {*<div class="dd-handle dd3-handle"></div>*}
                                    <div class="dd3-content">
                                        <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$menu->id]}"{/if}>{$menu->name|escape}</a>
                                    </div>
                                    <div class="controllinks">
                                        <code class="opcity50">{$menu->id}</code>
                                    </div>
                                </li>
                                {/if}
                            {/foreach}
                        </ol>
                    </div>


                {else}
                    <p>Нет меню</p>
                {/if}

            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        {*if $menus}
            $('#new_menu').nestable({
                maxDepth: 1
            });
        {/if*}
    });

    $('#right').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_menu(obj);
        });
        return false;
    });

    function delete_menu(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var visible = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                visible = 1 - visible;
                if (visible)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });
</script>