{* Title *}
{if $menu}
    {$meta_title='Редактирование меню' scope=parent}
{else}
    {$meta_title='Добавление меню' scope=parent}
{/if}


<form method=post id="menu">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>
        <div class="row">
            <div class="col-md-8">
                <legend>{if $menu}Редактирование меню{else}Добавление меню{/if}</legend>
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Наименование меню</label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" placeholder="Введите наименование меню" value="{$menu->name|escape_string}"/>
                                <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id меню">id:{$menu->id}</a></span>
                            </div>
                        </div>
                    </div>

                    {if !$menu->is_static}
                    <div class="col-md-4">
                        <div class="statusbar">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default4 on {if $menu->is_visible || !$menu}active{/if}">
                                    <input type="radio" name="visible" id="option1" value=1 checked="checked"> Активен
                                </label>
                                <label class="btn btn-default4 off {if $menu && !$menu->is_visible}active{/if}">
                                    <input type="radio" name="visible" id="option2" value=0 {if $menu && !$menu->is_visible}checked{/if}> Скрыт
                                </label>
                            </div>
                            <div class="statusbar-text">Статус:</div>
                        </div>
                    </div>
                    {else}
                        <input type=hidden name="visible" value="{$menu->is_visible}"/>
                    {/if}
                </div>
            </div>
            <div class="col-md-4">
                <legend>Оформление</legend>
                <div class="form-group">
                    <label>CSS-стиль меню</label>
                    <input type="textbox" name="css_class" class="form-control" value="{$menu->css_class}">
                </div>
            </div>

            <input type="hidden" name="id" value="{$menu->id}"/>
            <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
            <input type="hidden" name="close_after_save" value="0"/>
        </div>
    </fieldset>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $("form#menu a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#menu a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>