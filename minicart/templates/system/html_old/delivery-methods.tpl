{* Title *}
{$meta_title='Доставка' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="deliveries">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">


        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        
        <legend>Доставка</legend>
        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-9">
                <div id="new_menu" class="dd">

                    {if $deliveries}
                    <ol class="dd-list">
                        {foreach $deliveries as $delivery}
                            <li class="dd-item dd3-item" data-id="{$delivery->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$delivery->id]}"{/if}>{$delivery->name|escape}</a>
                                    <div class="typeof">
                                    
                                    {if $delivery->delivery_type == 'paid'}<span class="label label-default">Платная - {$delivery->price|convert} {$main_currency->sign}{if $delivery->free_from > 0}, бесплатная от {$delivery->free_from|convert} {$main_currency->sign}{/if}</span>{elseif $delivery->delivery_type == 'free'}<span class="label label-default">Бесплатная</span>{elseif $delivery->delivery_type == 'separately'}<span class="label label-default">Оплачивается отдельно</span>{/if}
                                    </div>
                                    
                                    <div class="controllinks">
                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$delivery->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $delivery->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$delivery->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                        
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    {else}
                        <p>Нет способов доставки</p>
                    {/if}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $deliveries}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_delivery_method_item(obj);
        });
        return false;
    });

    function delete_delivery_method_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                li.remove();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });
</script>