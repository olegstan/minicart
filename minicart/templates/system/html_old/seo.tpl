{$meta_title = "Настройки SEO" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    </div>

    <!-- Параметры -->

    <legend>Настройки SEO</legend>


        <div class="row">
            <div class="col-md-8">
            <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title">Настройки title товаров</h3>
            </div>
           <div class="panel-body">
           <div class="row">
               <div class="col-md-6">
                    <label>Префикс для title товаров</label>
                    <input name="prefix_product" type="text"  class="form-control" value="{$settings->prefix_product|escape}" placeholder="Введите префикс для title товаров"/>
                </div>

            
            <div class="col-md-6">
                <label>Постфикс для title товаров</label>
                <input name="postfix_product" type="text"  class="form-control" value="{$settings->postfix_product|escape}" placeholder="Введите постфикс для title товаров"/>
                </div>
                </div>
            </div>
        </div>
        
        
            <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title">URL категорий</h3>
            </div>
            <div class="panel-body">
            <div class="row">
               <div class="col-md-6"> 
                <label>Префикс для URL категорий</label>
                <input name="prefix_category_url" type="text"  class="form-control" value="{$settings->prefix_category_url|escape}" placeholder="Введите префикс для URL категорий"/>
                </div>

                
            <div class="col-md-6">
            <label>Постфикс для URL категорий</label>
            <input name="postfix_category_url" type="text"  class="form-control" value="{$settings->postfix_category_url|escape}" placeholder="Введите постфикс для URL категорий"/>            </div>
            </div>
            </div>
            </div>
            
            <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title">URL товаров</h3>
            </div>
            <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                <label>Префикс для URL товаров</label>
                <input name="prefix_product_url" type="text"  class="form-control" value="{$settings->prefix_product_url|escape}" placeholder="Введите префикс для URL товаров"/>
                </div>
            
            <div class="col-md-6">
            <label>Постфикс для URL товаров</label>
            <input name="postfix_product_url" type="text"  class="form-control" value="{$settings->postfix_product_url|escape}" placeholder="Введите постфикс для URL товаров"/>
            </div>
            </div>
            </div>
            </div>

            <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title">URL брендов</h3>
            </div>
            <div class="panel-body">
            <div class="row">
               <div class="col-md-6">
                <label>Префикс для URL брендов</label>
                <input name="prefix_brand_url" type="text"  class="form-control" value="{$settings->prefix_brand_url|escape}" placeholder="Введите префикс для URL брендов"/>
                </div>
            
            <div class="col-md-6">
            <label>Постфикс для URL брендов</label>
            <input name="postfix_brand_url" type="text"  class="form-control" value="{$settings->postfix_brand_url|escape}" placeholder="Введите постфикс для URL брендов"/>
            </div>
            </div>
            </div>
            </div>

            <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title">Управление урлами</h3>
            </div>
             <div class="panel-body">
            <table class="table table-bordered">
            <tr class="info">
                <td>#</td>
                <td>Модуль</td>
                <td>Опции</td>
                <td>URL</td>
            </tr>
            {foreach $frontend_modules as $module}
                <tr>
                    <td>{$module@index+1}</td>
                    <td>{$module->module}</td>
                    <td>{$module->options}</td>
                    <td><input type="text" class="form-control" name="module[{$module->id}]" placeholder="Введите URL" value="{$module->url}"/></td>
                </tr>
            {/foreach}
            </table>

          
    </div></div>
    </div>
    </div>
</form>
<!-- Основная форма (The End) -->

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>