{* Title *}
{$meta_title='Редактирование фильтра категории' scope=parent}

<form method=post id="tag">
    <fieldset>

        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
            <div class="col-md-8">
        <legend>{if $tag_set}Редактирование фильтра категории{else}Создание фильтра категории{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
            <div class="form-group">
                <label>Название фильтра</label>
                <div class="input-group">
                <input type="text" class="form-control" name="name" placeholder="Введите наименование набора групп тегов" value="{$tag_set->name|escape_string}"/>
                <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id набора">id:{$tag_set->id}</a></span>
                </div>
            </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $tag_set->is_visible || !$tag_set}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $tag_set->is_visible || !$tag_set}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $tag_set && !$tag_set->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $tag_set && !$tag_set->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>

        <legend>Свойства, входящие в фильтр</legend>
        <div id="new_menu" class="dd">
            <ol class="dd-list property-list">
                {if $tags_groups}
                    {foreach $tags_groups as $group}
                        <li class="dd-item dd3-item" data-id="{$group->tag_id}">
                            <div class="dd-handle dd3-handle"></div>
                            <div class="dd3-content">

{if $group->mode == "select"}{$group_mode = "Выпадающий список"}{/if}
{if $group->mode == "checkbox"}{$group_mode = "Галочки (Чекбоксы)"}{/if}
{if $group->mode == "radio"}{$group_mode = "Радиобаттон"}{/if}
{if $group->mode == "range"}{$group_mode = "Диапазонный фильтр"}{/if}
{if $group->mode == "logical"}{$group_mode = "Логический"}{/if}

                                {$group->name|escape}{if $group->postfix}, {$group->postfix}{/if}

                                <div class="type">
                                    <span class="label label-default">{$group_mode}</span>
                                </div>

                                <div class="controllinks">
                                    <code class="opcity50">{$group->tag_id}</code>
                                    
                                    <a class="delete-item" href="{$config->root_url}{$module->url}{url add=['id'=>$tag_set->id, 'mode'=>'delete_set_tag', 'tag_id'=>$group->tag_id, 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                    <a class="toggle-item {if $group->in_filter}light-on{else}light-off{/if}" href="{$config->root_url}{$module->url}{url add=['id'=>$tag_set->id, 'mode'=>'toggle_set_tag', 'tag_id'=>$group->tag_id, 'ajax'=>1]}"></a>
                                    <input type="hidden" id="in_filter_{$group->tag_id}" name="in_filter[{$group->tag_id}]" value="{$group->in_filter}"/>
                                </div>
                                
                                

                                {if $group->mode == 'select' || $group->mode == 'logical'}
                                    <input type="hidden" value="0" name="default_expand[{$group->tag_id}]" checked/>
                                {else}
                                <div class="roll-onoff">
                                    <div data-toggle="buttons" class="btn-group">
                                        <label class="btn btn-default4 btn-xs on {if $group->default_expand}active{/if}">
                                            <input type="radio" value="1" name="default_expand[{$group->tag_id}]" {if $group->default_expand}checked{/if}/>  Раскрыть
                                        </label>
                                        <label class="btn btn-default4 btn-xs off {if !$group->default_expand}active{/if}">
                                            <input type="radio" value="0" name="default_expand[{$group->tag_id}]" {if !$group->default_expand}checked{/if}/> Свернуть
                                        </label>
                                    </div>
                                </div>
                                {/if}
                            </div>
                        </li>
                    {/foreach}
                {/if}
            </ol>
        </div>
        <button class="btn btn-default" id="show_groups_list"><i class="fa fa-plus"></i> Добавить в фильтр свойство</button>
        
        <div class="form-inline massoperations" style="display:none" id="add_group_div">
        <select id="groups_list" class="form-control"></select>
        {*<button class="btn btn-default" id="add_group">Добавить</button>
        <button class="btn btn-default" id="cancel_add">Отмена</button>*}
        </div>

        <input type="hidden" name="id" value="{$tag_set->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        <input type="hidden" name="tags_groups" value=""/>
    </fieldset>
    </div>
        <div class="col-md-4"></div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), '{if $message_error == "error"}Ошибка при сохранении!{else}{$message_error}{/if}', '')
        {/if}
    });

    $("form#tag a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $(document).ready(function(){
        $('#new_menu').nestable({
            maxDepth: 1
        });
    });
    $("#show_groups_list").click(function(){
        var href = $(this).closest('form').attr('action');
        var ids = [];
        $("#new_menu li.dd-item").each(function(){
            ids[ids.length] = $(this).attr('data-id');
        });
        $.ajax({
            type: 'POST',
            url: '{$config->root_url}/ajax/get_data.php',
            data: {
                'object': 'filters',
                'mode': 'get_tag_groups',
                'list': ids,
                'session_id': '{$smarty.session.id}'},
            success: function(data){
                if (data.success){
                    $("#show_groups_list").hide();
                    $("#add_group_div").show();

                    $("#groups_list").html(data.data);
                    {*$("#groups_list").html('');
                    $("#groups_list").append("<option value=\"0\" data-name=\"\" data-postfix=\"\">Не выбрано</option>");
                    for(var k in data[0])
                    {
                        var group = data[0][k];
                        $("#groups_list").append("<option value=\""+group.id+"\" data-name=\""+group.name+"\" data-mode=\""+group.mode+"\" data-postfix=\""+group.postfix+"\">"+group.name+(group.postfix.length>0?", "+group.postfix:"")+" ("+group.id+")</option>");
                    }*}
                }
            },
            error: function(data){
                $("#show_groups_list").show();
                $("#add_group_div").hide();
            },
            dataType: 'json'
        });
        return false;
    });
    $('#groups_list').change(function(){
        var opt = $("#groups_list option:selected");
        if (opt.length > 0)
        {
            if (opt.val() == 0)
                return false;
            var mode = opt.attr('data-mode');
            var mode_str = "";
            if (mode == 'select' || mode == 'logical')
                mode_str = "<input type=\"hidden\" value=\"0\" name=\"default_expand["+opt.attr('value')+"]\" checked/>";
            else
                mode_str = "<div class=\"roll-onoff\">"+
                                "<div data-toggle=\"buttons\" class=\"btn-group\">"+
                                    "<label class=\"btn btn-default4 btn-xs on active\">"+
                                        "<input type=\"radio\" value=\"1\" name=\"default_expand["+opt.attr('value')+"]\" checked/>  Раскрыть"+
                                    "</label>"+
                                    "<label class=\"btn btn-default4 btn-xs off\">"+
                                        "<input type=\"radio\" value=\"0\" name=\"default_expand["+opt.attr('value')+"]\"/> Свернуть"+
                                    "</label>"+
                                "</div>"+
                            "</div>";

            var group_mode = "";
            if (mode == "select") group_mode = "Выпадающий список";
            if (mode == "checkbox") group_mode = "Галочки (Чекбоксы)";
            if (mode == "radio") group_mode = "Радиобаттон";
            if (mode == "range") group_mode = "Диапазонный фильтр";
            if (mode == "logical") group_mode = "Логический";

            $("#new_menu ol.dd-list").append("<li class=\"dd-item dd3-item\" data-id=\""+opt.attr('value')+"\">"+
                                                "<div class=\"dd-handle dd3-handle\"></div>"+
                                                "<div class=\"dd3-content\">"+
                                                    opt.attr('data-name')+(opt.attr('data-postfix').length>0?", "+opt.attr('data-postfix'):"")+
                                                    "<div class=\"type\">"+
                                                        "<span class=\"label label-default\">"+group_mode+"</span>"+
                                                    "</div>"+
                                                    "<div class=\"controllinks\">"+
                                                        "<code class=\"opcity50\">"+opt.attr('value')+"</code>"+
                                                        "<a class=\"delete-item\" href=\"#\"><i class=\"fa fa-times\"></i></a>"+
                                                        "<a class=\"toggle-item light-on\" href=\"#\"></a>"+
                                                        "<input type=\"hidden\" id=\"in_filter_"+opt.attr('value')+"\" name=\"in_filter["+opt.attr('value')+"]\" value=\"1\"/>"+
                                                    "</div>"+
                                                    mode_str+
                                                "</div>"+
                                            "</li>");
        }
        $("#show_groups_list").show();
        $("#add_group_div").hide();
        return false;
    });

    {*$("#cancel_add").click(function(){
        $("#show_groups_list").show();
        $("#add_group_div").hide();
        return false;
    });
    $("#add_group").click(function(){
        var opt = $("#groups_list option:selected");
        if (opt.length > 0)
        {
            var mode = opt.attr('data-mode');
            var mode_str = "";
            if (mode == 'select' || mode == 'logical')
                mode_str = "<input type=\"hidden\" value=\"0\" name=\"default_expand["+opt.attr('value')+"]\" checked/>";
            else
                mode_str = "<div class=\"roll-onoff\">"+
                                "<div data-toggle=\"buttons\" class=\"btn-group\">"+
                                    "<label class=\"btn btn-default4 btn-xs on active\">"+
                                        "<input type=\"radio\" value=\"1\" name=\"default_expand["+opt.attr('value')+"]\" checked/>  Раскрыть"+
                                    "</label>"+
                                    "<label class=\"btn btn-default4 btn-xs off\">"+
                                        "<input type=\"radio\" value=\"0\" name=\"default_expand["+opt.attr('value')+"]\"/> Свернуть"+
                                    "</label>"+
                                "</div>"+
                            "</div>";
            $("#new_menu ol.dd-list").append("<li class=\"dd-item dd3-item\" data-id=\""+opt.attr('value')+"\">"+
                                                "<div class=\"dd-handle dd3-handle\"></div>"+
                                                "<div class=\"dd3-content\">"+
                                                    opt.attr('data-name')+(opt.attr('data-postfix').length>0?", "+opt.attr('data-postfix'):"")+
                                                    "<div class=\"controllinks\">"+
                                                        "<code class=\"opcity50\">"+opt.attr('value')+"</code>"+
                                                        "<a class=\"delete-item\" href=\"#\"><i class=\"fa fa-times\"></i></a>"+
                                                        "<a class=\"toggle-item light-on\" href=\"#\"></a>"+
                                                        "<input type=\"hidden\" id=\"in_filter_"+opt.attr('value')+"\" name=\"in_filter["+opt.attr('value')+"]\" value=\"1\"/>"+
                                                    "</div>"+
                                                    mode_str+
                                                "</div>"+
                                            "</li>");
        }
        $("#show_groups_list").show();
        $("#add_group_div").hide();
        return false;
    });*}

    $("#new_menu ol.dd-list").on("click", "a.delete-item", function(){
        var item = $(this);
        var href = item.attr('href');
        var li = item.closest('li');
        if (href == "#"){
            li.remove();
            return false;
        }
        $.get(href, function(data) {
            if (data.success)
            {
                li.remove();
            }
        });
        return false;
    });

    $("#new_menu ol.dd-list").on("click", "a.toggle-item", function(){
        var item = $(this);
        var href = item.attr('href');
        var li = item.closest('li');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
                $("#in_filter_"+li.attr('data-id')).val(enabled);
            }
        });
        return false;
    });

    $("form#tag").submit(function(){
        var tags_groups = $('#new_menu').nestable('serialize');
        $(this).find('input[name=tags_groups]').val(JSON.stringify(tags_groups));
    });
</script>