{* Title *}
{$meta_title='Бренды' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="brands">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}               
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>      
                </span>                
            </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Бренды</legend>

                <div id="sortview">
                <div class="itemcount">Брендов: {$brands_count}</div>
                <ul class="nav nav-pills" id="sort"> 
                <li class="disabled">Сортировать по:</li>
                
                    <li>
                        <a class="btn btn-link btn-small {if $params_arr['sort'] == 'name' || !array_key_exists('sort', $params_arr)}current{/if}" href="{$config->root_url}{$module->url}{if ($params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'asc') || (!array_key_exists('sort', $params_arr) && !array_key_exists('sort_type', $params_arr))}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif ($params_arr['sort'] == 'name' || !array_key_exists('sort', $params_arr)) && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'name', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}fa fa-sort-alpha-desc{else}fa fa-sort-alpha-asc{/if}"></i> Алфавиту</a>
                    </li>
                
                </ul>
                </div>
                
               

                <div id="new_menu" class="dd">
                 {if $brands}
                 
                  {include file='pagination.tpl'}
                  <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $brands as $brand}
                            <li class="dd-item dd3-item" data-id="{$brand->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <input type="checkbox" class="checkb" value="{$item->id}">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url current_params=$current_params add=['id'=>$brand->id, 'page'=>$current_page_num]}"{/if}>{$brand->name|escape}</a>
                                </div>
                                <div class="controllinks">
                                        {if $allow_edit_module}<a class="favorite-brand {if $brand->is_popular}on{else}off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$brand->id, 'mode'=>'toggle_popular', 'ajax'=>1]}"  title="Бренд {if $brand->is_popular}популярный{else}не популярный{/if}"></a>{/if}
                                        <code class="goods"  title="Количество товаров этого бренда">{$brand->products_count}</code>


                                        <code class="opcity50">{$brand->id}</code>

                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$brand->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $brand->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$brand->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                        
                                    </div>
                            </li>
                        {/foreach}
                    </ol>
                    </div>

                    {include file='pagination.tpl'}

                    <div class="form-inline massoperations">
                        <div class="checkall">Выделить все &uarr;</div>
                        <select id="mass_operation" class="form-control">
                            <option value='1'>Сделать видимыми</option>
                            <option value='2'>Скрыть</option>
                            <option value='3'>Удалить</option>
                        </select>
                        <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                    </div>



                    {else}
                        <p>Нет брендов</p>
                    {/if}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $brands}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_brand_item(obj);
        });
        return false;
    });

    function delete_brand_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
                {*li.remove();*}
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var visible = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                visible = 1 - visible;
                if (visible)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });
        
        $('.favorite-brand').on('click', function() {
            var item = $(this);
        var href = item.attr('href');
        var visible = item.hasClass('on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('on');
                item.removeClass('off');
                visible = 1 - visible;
                if (visible)
                    item.addClass('on');
                else
                    item.addClass('off');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (operation == 3 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#new_menu input[type=checkbox]:checked").each(function(index, value){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-on');
                                });

                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item').first();
                    var href = subitem.attr('href');
                    if (!subitem.first().hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                                item.find('li.dd-item').each(function(){
                                    $(this).find('a.toggle-item').first().removeClass('light-off').removeClass('light-on').addClass('light-off');
                                });
                            }
                        });
                    break;
                {*case 3:    //Удаление
                    var subitem = item.find('a.delete-item').first();
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                                location.reload();
                            return false;
                        }
                    });
                    break;*}
            }
        });

        return false;
    });
</script>