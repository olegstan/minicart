{* Title *}
{$meta_title='Модификаторы' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="modificators">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup form-inline">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['parent_id'=>$params_arr['parent_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Модификаторы</legend>

                <div id="sortview">
                <div class="itemcount">Модификаторов: {$modificators_count}</div>
                <ul class="nav nav-pills" id="sort">  
                <li class="disabled">Сортировать по:</li>
                {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'position'}
                <li><a class="btn btn-link btn-small current"><span>По порядку</span></a></li>
                {else}
                <li><a class="btn btn-link btn-small allow-jump" href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'position','sort_type'=>'asc','page'=>1]}"><span>По порядку</span></a></li>
                {/if}
                </ul>
                </div>

                {if $modificators}
                <div id="new_menu" class="dd">
                    <div class="itemslist">
                        <ol class="dd-list">

                            {foreach $modificators as $modificator}
                                <li class="dd-item dd3-item" data-id="{$modificator->id}">
                                    <div class="dd-handle dd3-handle"></div>
                                    <div class="dd3-content">
                                        <a href="{$config->root_url}{$edit_module->url}{url add=['id'=>$modificator->id, 'parent_id'=>$params_arr['parent_id']]}">{$modificator->name}</a>
                                        <div class="controllinks">
                                            {if $modificator->type == "plus_fix_sum"}
                                                <code>+{$modificator->value} {$main_currency->sign}</code>
                                            {elseif $modificator->type == "minus_fix_sum"}
                                                <code>-{$modificator->value} {$main_currency->sign}</code>
                                            {elseif $modificator->type == "plus_percent"}
                                                <code>+{$modificator->value}%</code>
                                            {elseif $modificator->type == "minus_percent"}
                                                <code>-{$modificator->value}%</code>
                                            {elseif $modificator->type == "nothing"}

                                            {/if}
                                            {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$modificator->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>{/if}
                                            <a class="toggle-item {if $modificator->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$modificator->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>
                                        </div>
                                        <div class="modificatorimage">{if $modificator->image}<img src='{$modificator->image->filename|resize:modificators:50:50}' alt='{$modificator->image->name}'>{/if}</div>
                                    </div>
                                </li>
                            {/foreach}
                        </ol>
                    </div>
                </div>
                {else}
                    <p>Нет модификаторов</p>
                {/if}
            </div>
            
            <div class="col-md-4">
                <div class="list-group">
                  <a href="{$config->root_url}{$module->url}" class="list-group-item {if !array_key_exists('parent_id', $params_arr)}active{/if}">Все модификаторы</a>
                  {foreach $modificators_groups as $group}
                    <a href="{$config->root_url}{$module->url}{url add=['parent_id'=>$group->id]}" class="list-group-item {if $params_arr['parent_id'] == $group->id}active{/if}"><i class="fa fa-angle-right"></i> {$group->name} <div class="label-modifier"><span>{if $group->type == 'checkbox'}Галочки (Чекбоксы){elseif $group->type == 'radio'}Радиобаттон{elseif $group->type == 'select'}Выпадающий список{/if}</span></div></a>
                  {/foreach}
                </div>
        </div>
    </form>
</div>
</div>
<script type="text/javascript">
    {if $modificators}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action') + '?save_positions',
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_modificator_item(obj);
        });
        return false;
    });

    function delete_modificator_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });
</script>