{* Title *}
{$meta_title='Валюты' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="currencies">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}save_positions">

        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
            <legend>Валюты</legend>
                <div id="new_menu" class="dd">

                    {if $currencies}
                    <ol class="dd-list">
                        {foreach $currencies as $currency_item}
                            <li class="dd-item dd3-item" data-id="{$currency_item->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$currency_item->id]}"{/if}>{$currency_item->name|escape}</a>


                                    <div class="controllinks">
                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$currency_item->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="light-item {if $currency_item->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$currency_item->id, 'mode'=>'light', 'ajax'=>1]}"></a>
                                        <a class="favorite-item {if $currency_item->use_admin}favorite-on{else}favorite-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$currency_item->id, 'mode'=>'favorite', 'ajax'=>1]}"></a>
                                        <a class="flag-item {if $currency_item->use_main}flag-on{else}flag-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$currency_item->id, 'mode'=>'flag', 'ajax'=>1]}"></a>{/if}

                                    </div>

                                    {if !$currency_item->use_main}
                                    <div class="kurs">
                                        1 {$currency_item->sign} = {1|convert:$currency_item->id} {$main_currency->sign}
                                    </div>
                                    {/if}
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    {else}
                        <p>Нет валют</p>
                    {/if}
                </div>
            </div>
            <div class="col-md-4">
            <legend>Пояснение</legend>
            <p class="flag-help"> &mdash; это основная расчетная валюта, которая будет показываться на сайте и использоваться во всех расчетах: стоимости заказа, скидке, стоимости доставки.</p>
            <p class="favorite-help"> &mdash; это валюта, в которой задается цена товара в админке. Для отображения на сайте она будет пересчитана по курсу, указанному в ее настройках.</p>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    {if $currencies}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.delete-item').on('click', function() {
        confirm_popover_box($(this), function(obj){
            delete_currency_item(obj);
        });
        return false;
    });

    function delete_currency_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                li.remove();
            return false;
        });
        return false;
    }

    $('.light-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('.favorite-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('favorite-on')?1:0;
        if (!enabled)
        {
            $.get(href, function(data) {
                if (data.success)
                    location.reload();
                {*
                    item.removeClass('favorite-on');
                    item.removeClass('favorite-off');
                    enabled = 1 - enabled;
                    if (enabled)
                    {
                        var ol = item.closest('ol');
                        ol.find('a.favorite-item').each(function(){
                            if ($(this).hasClass('favorite-on'))
                            {
                                $(this).removeClass('favorite-on');
                                $(this).addClass('favorite-off');
                            }
                        });
                        item.addClass('favorite-on');
                    }
                    else
                        item.addClass('favorite-off');
                    location.reload();
                *}
            });
        }
        return false;
    });

    $('.flag-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('flag-on')?1:0;
        if (!enabled)
        {
            $.get(href, function(data) {
                if (data.success)
                    location.reload();
            });
        }
        return false;
    });
</script>