{* Title *}

{if $product}
    {$meta_title='Редактирование товара' scope=parent}
{else}
    {$meta_title='Добавление товара' scope=parent}
{/if}

<form method=post id="product" {*action="{$config->root_url}{$module->url}"*}>
<fieldset>
<div class="controlgroup">
    {if $product}
        <div class="openlink">
            <a href="{$config->root_url}{$product_frontend_module->url}{$product->url}{$settings->postfix_product_url}"
               target="_blank"><i class="fa fa-external-link"></i> <span>Открыть товар в новом окне</span></a>
        </div>
    {/if}
    <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
    <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
    <a href="{$config->root_url}{$main_module->url}{url add=['category_id'=>$category_id, 'page'=>$page, 'sort'=>$sort, 'sort_type'=>$sort_type]}"
       class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
</div>


<div class="block-header">{if $product}Редактирование товара <span>{$product->name}</span>{else}Добавление товара{/if} </div>

<ul class="nav nav-tabs mini-tabs" role="tablist" id="tablist">
    <li {if !$tab || $tab == "main"}class="active"{/if}><a href="#main" role="tab" data-toggle="tab" data-url="main"><span>Основное</span></a></li>
    <li {if $tab == "related"}class="active"{/if}><a href="#relatedproducts" role="tab" data-toggle="tab" data-url="related"><span>Сопутствующие товары, аналоги, комплекты</span></a></li>
    <li {if $tab == "seo"}class="active"{/if}><a id="seo-toggle" href="#seo" role="tab" data-toggle="tab" data-url="seo"><span>SEO</span></a></li>
    <li {if $tab == 'modifiers'}class="active"{/if}><a href="#modifiersprices" role="tab" data-toggle="tab" data-url="modifiers"><span>Модификаторы цены</span></a></li>
    <li {if $tab == "more"}class="active"{/if}><a href="#additionally" role="tab" data-toggle="tab" data-url="more"><span>Дополнительно</span></a></li>
</ul>

<div class="tab-content">
<div class="tab-pane {if !$tab || $tab == "main"}active{/if}" id="main">
{* Основное описание товара начало *}
<div class="row">
    <div class="col-md-9">
        <div class="form-group">
            <label>Название товара</label>

            <div class="input-group product-name">
                <input type="text" class="product-name-place form-control input-bold" name="name" value="{$product->name|escape_string}"/>
                <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id товара">id:{$product->id}</a></span>
            </div>
        </div>
    </div>

    <div class="col-md-3">

        <div class="statusbar">

            <div class="btn-group" data-toggle="buttons">
                <label class="btn btn-default4 on {if $product->is_visible || !$product}active{/if}">
                    <input type="radio" name="visible" id="option1" value=1 {if $product->is_visible || !$product}checked{/if}> Активен
                </label>
                <label class="btn btn-default4 off {if $product && !$product->is_visible}active{/if}">
                    <input type="radio" name="visible" id="option2" value=0 {if $product && !$product->is_visible}checked{/if}> Скрыт
                </label>
            </div>
            <div class="statusbar-text">Статус:</div>
        </div>
    </div>

</div>


<div class="row">

    <div class="col-md-6">
        <label>Родительская категория</label>
        <a href="#" id="add-category" class="btn btn-default btn-sm btn-link dotted"><i class="fa fa-plus"></i> <span>Добавить категорию</span></a>
        <ul class="nolist" id="categories-list">

            {if $product_categories}
                {foreach $product_categories as $pc}
                    <li>
                        {* Версия с Select2 (AJAX) *}
                        {*{foreach $all_categories as $c}
                            {if $c->id == $pc->id}
                                <input type="hidden" name="categories_ids[]" value="{$pc->id}" data-text="{$c->name}" style="width:600px"/>
                                {if $pc@index>0}<a href="#" class="delete-parent_category btn btn-sm btn-link"><i class="fa fa-times"></i></a>{/if}
                            {/if}
                        {/foreach}*}

                        {* Версия с Select2 (БЕЗ AJAX) *}
                        <select name="categories_ids[]" data-placeholder="Выберите категорию..." style="width:100%">
                            <option></option>
                            {foreach $all_categories as $c}
                                <option value='{$c->id}' {if $c->id == $pc->id}selected{/if}
                                        class="level{$c->level}">{$c->name}</option>
                            {/foreach}
                        </select>


                        <div class="categories-right">
                            {if $pc@index>0}
                                <a href="#" class="delete-parent_category btn btn-sm btn-link"><i
                                            class="fa fa-times"></i></a>
                            {/if}
                        </div>
                    </li>
                {/foreach}
            {else}
                <li>
                    {* Версия с Select2 (AJAX) *}
                    <input type="hidden" name="categories_ids[]" value="" style="width:600px"/>

                    {* Версия с Select2 (БЕЗ AJAX) *}
                    <div class="categories-left">
                        <select name="categories_ids[]" data-placeholder="Выберите категорию..." style="width:100%">
                            <option></option>
                            {foreach $all_categories as $c}
                                <option value='{$c->id}'
                                        {if ($c@index==0 && !$product && !$category_id) || $c->id==$category_id}selected{/if} class="level{$c->level}">{$c->name}</option>
                            {/foreach}
                        </select>
                    </div>
                </li>
            {/if}

        </ul>

        {* ВЕРСИЯ С SELECT2 (AJAX) *}
        {*<input type="hidden" value="" id="new_parent_category_proto" style="width:600px; display:none;"/>*}

        {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX) *}

        <select id="new_parent_category_proto" style="width:100%; display:none;"
                data-placeholder="Выберите категорию...">
            <option></option>
            {foreach $all_categories as $c}
                <option value='{$c->id}' class="level{$c->level}">{$c->name}</option>
            {/foreach}
        </select>

    </div>

    <div class="col-md-5 col-md-offset-1">
        <div class="form-group">
            <label class="label-control">Бренд</label>
            {*<select name="brand_id" class="form-control" id="brandselect">
                <option value="0">Не выбран</option>
                {foreach $brands as $brand}
                    <option value="{$brand->id}" {if $product->brand_id == $brand->id}selected{/if}>{$brand->name}</option>
                {/foreach}
            </select>*}
            {* Версия с Select2 (БЕЗ AJAX) *}
            <select name="brand_id" data-placeholder="Выберите бренд..." style="width:100%" id="brandselect">
                <option></option>
                <option value="0">Не выбран</option>
                {foreach $brands as $brand}
                    <option value="{$brand->id}"
                            {if $product->brand_id == $brand->id}selected{/if}>{$brand->name}</option>
                {/foreach}
            </select>
        </div>


    </div>
</div>

{if $settings->catalog_use_variable_amount}
<div class="variable-amount">
<div class="variable-amount-header">Покупка товара с переменным количеством</div>
        <div class="form-group">
        <label>Использовать покупку с переменным количеством:</label>
        <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if ($product->id && $product->use_variable_amount) || !$product->id}active{/if}">
                    <input type="radio" name="use_variable_amount" value=1 {if ($product->id && $product->use_variable_amount) || !$product->id}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if $product->id && !$product->use_variable_amount}active{/if}">
                    <input type="radio" name="use_variable_amount" value=0 {if $product->id && !$product->use_variable_amount}checked{/if}> Нет
                </label>
        </div>
        </div>



    <div id="variable_amount_panel" {if $product->id && !$product->use_variable_amount}style="display:none;"{/if}>
    <div class="row">
        <div class="col-md-3">
        <div class="form-group">
            <label>Минимальное количество</label>
            <input type="text" class="form-control" name="min_amount" placeholder="" value="{if $product->use_variable_amount}{$product->min_amount}{else}{$settings->catalog_min_amount}{/if}">
        </div>
        </div>
        <div class="col-md-3">
        <div class="form-group">
            <label>Максимальное количество</label>
            <input type="text" class="form-control" name="max_amount" placeholder="" value="{if $product->use_variable_amount}{$product->max_amount}{else}{$settings->catalog_max_amount}{/if}">
        </div>
        </div>
        
        <div class="col-md-3">
        <div class="form-group">
            <label>Шаг изменения</label>
            <input type="text" class="form-control" name="step_amount" placeholder="" value="{if $product->use_variable_amount}{$product->step_amount}{else}{$settings->catalog_step_amount}{/if}">
        </div>
        </div>
        
    </div>
    </div>
</div>
{/if}

<legend>Варианты</legend>
<div class="well productvarariants">
    <div class="headlines">
        <div class="variant-name">
            Название варианта
        </div>
        <div class="variant-sku">
            Артикул
        </div>
        <div class="variant-price">
            Цена
        </div>
        <div class="variant-old-price">
            Старая цена
        </div>
        <div class="variant-count">
            Кол-во <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="'&#8734;' или любое положительное число - статус товара 'В наличии', '0' - статус товара 'Нет в наличии', '-1' - статус товара 'Под заказ'" class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
        </div>
    </div>


    <div class="dd" id="variants-list">
        <ol class="dd-list products">
            {if $product_variants}
                {foreach $product_variants as $variant}
                    <li class="dd-item dd3-item {if !$variant->is_visible}disabledvar{/if}" data-id="{$variant->id}">
                        <div class="dd-handle dd3-handle"></div>
                        <div class="dd3-content form-inline">
                            <input type="hidden" name="variants[id][{$variant->id}]" value="{$variant->id|escape_string}"/>
                            <input type="text" name="variants[name][{$variant->id}]" value="{$variant->name|escape_string}"
                                   placeholder="Название варианта" class="form-control variant-name">
                            <input type="text" name="variants[sku][{$variant->id}]" value="{$variant->sku}"
                                   placeholder="Артикул" class="form-control variant-sku">
                            <input type="text" name="variants[price][{$variant->id}]"
                                   value="{$variant->price|convert:NULL:false}" placeholder="Цена"
                                   class="form-control variant-price">
                            <input type="text" name="variants[price_old][{$variant->id}]"
                                   value="{$variant->price_old|convert:NULL:false}" placeholder="Старая цена"
                                   class="form-control variant-old-price" title="Старая цена">
                            <input type="text" name="variants[stock][{$variant->id}]"
                                   value="{if $variant->infinity || $variant->stock == ''}∞{else}{$variant->stock|escape}{/if}"
                                   placeholder="Количество" class="form-control variant-count"
                                   title="Количество товара на складе">
                            <input type="hidden" name="variants[visible][{$variant->id}]" value="{$variant->is_visible}"/>

                            <div class="controllinks">
                                <a class="delete-item" href="#"><i class="fa fa-times"></i></a>
                                <a class="toggle-item {if $variant->is_visible}light-on{else}light-off{/if}" href="#"></a>
                            </div>
                        </div>
                    </li>
                {/foreach}
            {else}
                <li class="dd-item dd3-item" data-id="">
                    <div class="dd-handle dd3-handle"></div>
                    <div class="dd3-content form-inline">
                        <input type="hidden" name="variants[id][]" value=""/>
                        <input type="text" name="variants[name][]" placeholder="Название варианта"
                               class="form-control variant-name">
                        <input type="text" name="variants[sku][]" placeholder="Артикул"
                               class="form-control variant-sku">
                        <input type="text" name="variants[price][]" placeholder="0" class="form-control variant-price"
                               value="">
                        <input type="text" name="variants[price_old][]" placeholder="0"
                               class="form-control variant-old-price" value="" title="Старая цена">
                        <input type="text" name="variants[stock][]" placeholder="Количество" value="∞"
                               class="form-control variant-count" title="Количество товара на складе">
                        <input type="hidden" name="variants[visible][{$variant->id}]" value="1"/>

                        <div class="controllinks">
                            <a class="delete-item" href="#"><i class="fa fa-times"></i></a>
                            <a class="toggle-item light-on" href=""></a>
                        </div>
                    </div>
                </li>
            {/if}
        </ol>
        <a href="#" id="add-variant" class="btn btn-default btn-sm margin-top-10 bigi"><i class="fa fa-plus"></i>
            Добавить вариант</a>
    </div>
</div>


<div class="row">
    <div class="col-md-8">

        <legend>Свойства <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Товар может иметь несколько значений из одного и того же свойства, например 2 ширины или 3 длинны, просто вводите значения по порядку в одну и ту же строку" class="fa fa-legend"><i class="fa fa-info-circle"></i></a></legend>


        {* Блок для логических тегов начало *}
        {*<div class="btn-group" data-toggle="buttons">
                  <label class="btn btn-sm btn-default4 on active">
                    <input type="radio" name="visible" id="option1" value=1 checked>  Да
                  </label>
                  <label class="btn btn-sm btn-default4 off active">
                    <input type="radio" name="visible" id="option2" value=0 {if $product && !$product->is_visible}checked{/if}> Нет
                  </label>
                </div>*}

        {* Блок для логических тегов конец *}


        <table id="taggroups" class="table table-striped">
            <tbody>
            {if $tags_groups && $product_tags}
                {*foreach $tags_groups as $group*}
                {foreach $product_tags_positions as $group_id}
                    {$group = $tags_groups[$group_id]}
                    {if array_key_exists($group->id, $product_tags) && !$group->is_auto}
                        {assign var="tags_ids" value=""}
                        {assign var="tags_values" value=""}
                        {foreach $product_tags[$group->id] as $t}
                            {if !empty($tags_values)}{assign var="tags_values" value=$tags_values|cat:'#%#'}{/if}
                            {assign var="tags_values" value=$tags_values|cat:$t->name}
                            {*if !empty($tags_ids)}{assign var="tags_ids" value=$tags_ids|cat:'#%#'}{/if}
                            {assign var="tags_ids" value=$tags_ids|cat:$t->id*}
                        {/foreach}
                        <tr data-group="{$group->id}">
                            <td class="col-md-3">{$group->name}{if $group->postfix}, {$group->postfix}{/if}
                                <a href="#" class="delete-property"><i class="fa fa-times"></i></a>
                            </td>
                            <td class="col-md-9">
                                {if $group->mode == "logical"}
                                    <div class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-sm btn-default4 on {if $tags_values == 'да' || $tags_values == 'Да'}active{/if}">
                                            <input type="radio" name="product_tags[{$group->id}]" value="да"
                                                   {if $tags_values == 'да' || $tags_values == 'Да'}checked{/if}> Да
                                        </label>
                                        <label class="btn btn-sm btn-default4 off {if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}active{/if}">
                                            <input type="radio" name="product_tags[{$group->id}]" value="нет"
                                                   {if $tags_values == 'нет' || $tags_values == 'Нет' || empty($tags_values)}checked{/if}> Нет
                                        </label>
                                    </div>
                                {elseif $group->mode == "text"}
                                    <textarea class="form-control" name="product_tags[{$group->id}]" data-group-id="{$group->id}">{$tags_values}</textarea>
                                {else}
                                    <input type="hidden" name="product_tags[{$group->id}]" data-group-id="{$group->id}"
                                           value="{$tags_values}">
                                {/if}
                            </td>
                        </tr>
                    {/if}
                {/foreach}
            {/if}

            <tfoot>
            {* НОВАЯ ГРУППА ТЕГОВ *}
            <tr id="new_tag_group" style="display:none;">
                <td class="col-md-3">Выберите свойство</td>
                <td class="col-md-9">
                    {*<div class="row">
                    <div class="col-md-9">*}
                    <select id="new_group_id" class="form-control">
                    </select>
                    {*</div>
                    <div class="col-md-3">
                    <a id="add_tag_group" href="#" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Добавить свойство"><i class="fa fa-plus"></i></a>
                    <a id="cancel_tag_group" href="#" class="btn btn-default" data-toggle="tooltip" data-placement="bottom" title="Отменить"><i class="fa fa-times"></i></a>
                    </div>*}
    </div>
    </td>
    </tr>
    {* НОВАЯ ГРУППА ТЕГОВ *}

    </table>
    <a href="#" class="btn btn-default btn-sm" id="add_new_tag_group" data-toggle="modal"><i class="fa fa-plus"></i>
        Добавить свойство</a>

    <div class="form-group">
        <label class="bigname">Краткое описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                   data-original-title="Отображается в списке товаров"><i
                        class="fa fa-info-circle"></i></a></label>
        <textarea name="annotation" class="form-control ckeditor" rows="10">{$product->annotation}</textarea>
    </div>

    <div class="form-group">
        <label class="bigname">Краткое описание 2 <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается в карточке товара сверху (это поле опционально)"><i class="fa fa-info-circle"></i></a></label>
        <textarea name="annotation2" class="form-control ckeditor" rows="10">{$product->annotation2}</textarea>
    </div>

    <div class="form-group">
        <label class="bigname">Полное описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                  data-original-title="Отображается в карточке товара снизу"><i
                        class="fa fa-info-circle"></i></a></label>
        <textarea name="body" class="form-control ckeditor" rows="10">{$product->body}</textarea>
    </div>
</div>

<div class="col-md-4">
    <legend>Изображения</legend>

    {include file='object-image-placeholder.tpl' object=$product images_object_name='products'}
    
    
    <legend class="mb10">Бейджи <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                           data-original-title="Вы можете добавить бейдж для товара, нарпимер Новинка или Хит продаж или сразу несколько"
                                           class="fa fa-legend"><i class="fa fa-info-circle"></i></a></legend>
            {if $product_badges}
                {assign var="badges_values" value=""}
                {foreach $product_badges as $b}
                    {if !empty($badges_values)}{assign var="badges_values" value=$badges_values|cat:'#%#'}{/if}
                    {assign var="badges_values" value=$badges_values|cat:($b->name)}
                {/foreach}
            {/if}
            <input type="hidden" id="badges" name="product_badges" data_value="{$badges_values}" value="{$badges_values}"/>

    <hr/>

    {if $product->id}
        <a href="#" class="btn btn-default w100" id="delete-product"><i class="fa fa-times"></i> Удалить товар</a>
    {/if}

</div>
</div>
{* Основное описание товара конец *}
</div>
<div class="tab-pane {if $tab == "related"}active{/if}" id="relatedproducts">

    {* Сопутствующие товары начало *}
    <div class="row">
        <div class="col-md-6">
            <legend>Сопутствующие товары</legend>
            <div id="related" class="dd">
                <ol class="dd-list">
                    {if $related_products}
                        {foreach $related_products as $related}
                            <li class="dd-item dd3-item relatedproducts {if !$related->is_visible}lampoff{/if}"
                                data-id="{$related->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="statusrelated">
                                        {if $related->in_stock}
                                            <i class="fa fa-check" title="Есть в наличии"></i>
                                        {elseif $related->in_order}
                                            <i class="fa fa-truck" title="Под заказ"></i>
                                        {else}
                                            <i class="fa fa-times-circle" title="Нет в налчии"></i>
                                        {/if}
                                    </div>

                                    <div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a>
                                    </div>
                                    <div class="relatedimage">
                                        {if $related->image}<img src="{$related->image->filename|resize:products:30:30}"
                                                                 alt="{$related->image->name}"/>{/if}
                                    </div>
                                    <div class="relatedname">
                                        <input type="hidden" name="related_ids[]" value="{$related->id}"/>
                                        <a href="{$config->root_url}{$module->url}{url add=['id'=>$related->id]}"
                                           target="_blank">{$related->name}</a>

                                        <div class="related-sky">{if $related->variant}{$related->variant->sku}{/if}</div>
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    {else}
                        <div id="no-related-products">Пока нет товаров</div>
                    {/if}
                </ol>
            </div>

            <div id="main_list" class="tags_values">
                <div class="apsearch form-inline">

                    <input id="searchField" type="text" class="form-control" placeholder="Поиск" autocomplete="off">
                    <button id="searchCancelButton" style="display: none;" class="btn btn-default searchremove"
                            type="button"><i class="fa fa-times"></i></button>
                </div>


                <div id="refreshpart" class="addcontainer"
                     style="display:none;">{include file='product-addrelated.tpl'}</div>

            </div>

            {* Сопутствующие товары конец *}

              </div>
    
    <div class="col-md-6">
    
    
    {* Аналоги начало *}

            <legend>Аналоги</legend>
            <div class="form-group">
                <button type="button" class="btn btn-default" id="create-analogs-group">Создать группу аналогов</button>
                <button type="button" class="btn btn-default" id="show-analogs-group">Выбрать группу аналогов</button>
            </div>
            <div class="form-group hidden" id="div-analogs-group">
            <select name="analogs_group" id="select-analogs-group">
                {foreach $analogs_groups as $analog_group}
                <option value="{$analog_group}" {if $analog_group==$analogs_group_id}selected{/if}>Группа аналогов {$analog_group}</option>
                {/foreach}
            </select>
            </div>
            <div class="form-group {if !$analogs_group_id}hidden{/if}" id="analogs-group" data-analogs-group="{$analogs_group_id}">
                Группа аналогов {$analogs_group_id}
            </div>
            <div id="analogs" class="dd">
                <ol class="dd-list">
                    {if $analogs_products}
                        {foreach $analogs_products as $analog}
                            <li class="dd-item dd3-item relatedproducts {if !$analog->is_visible}lampoff{/if} {if $analog->id==$product->id}current{/if}"
                                data-id="{$analog->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <div class="statusrelated">
                                        {if $analog->in_stock}
                                            <i class="fa fa-check" title="Есть в наличии"></i>
                                        {elseif $analog->in_order}
                                            <i class="fa fa-truck" title="Под заказ"></i>
                                        {else}
                                            <i class="fa fa-times-circle" title="Нет в налчии"></i>
                                        {/if}
                                    </div>

                                    <div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a>
                                    </div>
                                    <div class="relatedimage">
                                        {if $analog->image}<img src="{$analog->image->filename|resize:products:30:30}"
                                                                 alt="{$analog->image->name}"/>{/if}
                                    </div>
                                    <div class="relatedname">
                                        <input type="hidden" name="analogs_ids[]" value="{$analog->id}"/>
                                        <a href="{$config->root_url}{$module->url}{url add=['id'=>$analog->id]}"
                                           target="_blank">{$analog->name}</a>

                                        <div class="analog-sky">{if $analog->variant}{$analog->variant->sku}{/if}</div>
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    {else}
                        {*<div id="no-analogs-products">Пока нет товаров</div>*}
                    {/if}
                </ol>
            </div>

            <div id="main_list_analogs" class="tags_values {if !$analogs_group_id}hidden{/if}">
                <div class="apsearch form-inline">

                    <input id="searchField_analogs" type="text" class="form-control" placeholder="Поиск" autocomplete="off">
                    <button id="searchCancelButton_analogs" style="display: none;" class="btn btn-default searchremove"
                            type="button"><i class="fa fa-times"></i></button>
                </div>


                <div id="refreshpart_analogs" class="addcontainer"
                     style="display:none;">{include file='product-addanalog.tpl'}</div>

            </div>
        </div>

    {* Аналоги конец *}
    
    </div>
    
  
</div>
<div class="tab-pane {if $tab == 'seo'}active{/if}" id="seo">
    {* SEO начало *}

    <legend class="mt20">Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>

    <div class="row">
        <div class="col-md-6">

            <div class="form-group">
                <label>URL</label>

                <div class="input-group">
                    <span class="input-group-addon">{$settings->prefix_product_url}/</span>
                    <input type="text" class="form-control" name="url" value="{$product->url}"/>
                    <span class="input-group-addon">{$settings->postfix_product_url}</span>
                </div>
            </div>

            <div class="form-group">
                <label>Meta-title</label>
                <input type="text" class="form-control" name="meta_title" placeholder="Введите meta-title"
                       value="{$product->meta_title}"/>
            </div>

        </div>
        <div class="col-md-6">

            <div class="form-group">
                <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                         data-original-title="Meta-keywords"><i
                                class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_keywords" class="form-control">{$product->meta_keywords}</textarea>
            </div>
            <div class="form-group">
                <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                   data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_description" class="form-control">{$product->meta_description}</textarea>
            </div>
        </div>
    </div>
    {* SEO конец *}

</div>

    <div class="tab-pane {if $tab == 'modifiers'}active{/if}" id="modifiersprices">

        <div class="btn-group" data-toggle="buttons">
            <label class="btn btn-default {if !$product->modificators_mode || !$product}active{/if}">
                <input type="radio" name="modificators_mode" value=0 {if !$product->modificators_mode || !$product->id}checked{/if}/>  По умолчанию
            </label>
            <label class="btn btn-default {if $product->id && $product->modificators_mode}active{/if} {if $product->is_auto}disabled{/if}">
                <input type="radio" name="modificators_mode" value=1 {if $product->id && $product->modificators_mode}checked{/if}/> Вручную
            </label>
        </div>
    <hr/>
    <div class="row">
        <div class="col-md-6" id="modificators">
            <div class="form-group">
                {foreach $modificators as $m}
                    <div class="checkbox {if !$product->modificators_mode || !$product->id}disabled{/if}">
                        <label>
                            <input type="checkbox" value="{$m->id}" name="modificators[]"
                                {if $product->modificators_mode}
                                    {if in_array($m->id, $product->modificators)}checked=""{/if}
                                {else}
                                    {if $main_category && in_array($m->id, $main_category->modificators)}checked=""{/if}
                                {/if}
                                {if !$product->modificators_mode || !$product->id}disabled{/if}> {$m->name}
                        </label>
                    </div>
                {/foreach}
            </div>

            <label><strong>Группы модификаторов:</strong></label>
            <div class="form-group">
                {foreach $modificators_groups as $g}
                    <div class="checkbox {if !$product->modificators_mode || !$product->id}disabled{/if}">
                        <label>
                            <input type="checkbox" value="{$g->id}" name="modificators_groups[]"
                                {if $product->modificators_mode}
                                    {if in_array($g->id, $product->modificators_groups)}checked=""{/if}
                                {else}
                                    {if $main_category && in_array($g->id, $main_category->modificators_groups)}checked=""{/if}
                                {/if}
                                {if !$product->modificators_mode || !$product->id}disabled{/if}> {$g->name}
                        </label>
                    </div>
                {/foreach}
            </div>
        </div>
        </div>

    </div>


<div class="tab-pane {if $tab == 'more'}active{/if}" id="additionally">

    <div class="row">
        <div class="col-md-6">

            


            <legend class="mb10">Авто свойства <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                       data-original-title="Эти теги имеют все товары"
                                                       class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
            </legend>
            {if $tags_groups && $product_tags}
                {assign var="tags_values" value=""}
                {foreach $tags_groups as $group}
                    {if array_key_exists($group->id, $product_tags) && $group->is_auto}
                        {foreach $product_tags[$group->id] as $t}
                            {if !empty($tags_values)}{assign var="tags_values" value=$tags_values|cat:'#%#'}{/if}
                            {if $group->numeric_sort}
                                {assign var="tags_values" value=$tags_values|cat:($t|tag:true)}
                            {else}
                                {assign var="tags_values" value=$tags_values|cat:($t|tag)}
                            {/if}
                        {/foreach}
                    {/if}
                {/foreach}
            {/if}
            <input type="hidden" id="autotags" name="product_autotags" data-value="{$tags_values}" value="{$tags_values}" readonly>

            {if $settings->catalog_show_currency}
            <legend class="mb10 mt20">Валюта товара <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                                       data-original-title="Можно указать валюту отличную от установленной 'По умолчанию' в этом случае на сайте цена товара будет пересчитана в расчетную валюту по курсу, указанному в настройках валют"
                                                       class="fa fa-legend"><i class="fa fa-info-circle"></i></a>
            </legend>
            <div class="form-group">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default {if !$product || empty($product->currency_id)}active{/if}">
                        <input type="radio" name="currency_id" value="0"
                               {if !$product || empty($product->currency_id)}checked{/if}> По умолчанию
                    </label>
                    {foreach $all_currencies as $c}
                        <label class="btn btn-default {if $product->currency_id == $c->id}active{/if}">
                            <input type="radio" name="currency_id" value="{$c->id}"
                                   {if $product->currency_id == $c->id}checked{/if}> {$c->sign}
                        </label>
                    {/foreach}
                </div>
            </div>
            {/if}
            
        {if $settings->add_field1_enabled || $settings->add_field2_enabled || $settings->add_field3_enabled}
            <legend class="mb10 mt20">Дополнительные поля</legend>
            {if $settings->add_field1_enabled}
            <div class="form-group">
                <label>{$settings->add_field1_name}</label>
                <textarea class="form-control" rows="1" name="add_field1">{$product->add_field1}</textarea>
            </div>
            {/if}

            {if $settings->add_field2_enabled}
            <div class="form-group">
                <label>{$settings->add_field2_name}</label>
                <textarea class="form-control" rows="1" name="add_field2">{$product->add_field2}</textarea>
            </div>
            {/if}

            {if $settings->add_field3_enabled}
            <div class="form-group">
                <label>{$settings->add_field3_name}</label>
                <textarea class="form-control" rows="1" name="add_field3">{$product->add_field3}</textarea>
            </div>
            {/if}
        {/if}

        {if $settings->add_flag1_enabled || $settings->add_flag2_enabled || $settings->add_flag3_enabled}
            <legend class="mb10 mt20">Дополнительные флаги</legend>
            {if $settings->add_flag1_enabled}
            <div class="form-group">
                <label>{$settings->add_flag1_name}</label>
                <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if ($product->id && $product->add_flag1) || (!$product->id && $settings->add_flag1_default_value)}active{/if}">
                        <input type="radio" name="add_flag1" value=1 {if ($product->id && $product->add_flag1) || (!$product->id && $settings->add_flag1_default_value)}checked{/if}>Да
                    </label>
                    <label class="btn btn-default4 off {if ($product->id && !$product->add_flag1) || (!$product->id && !$settings->add_flag1_default_value)}active{/if}">
                        <input type="radio" name="add_flag1" value=0 {if ($product->id && !$settings->add_flag1) || (!$product->id && !$settings->add_flag1_default_value)}checked{/if}>Нет
                    </label>
                </div>
            </div>
            {/if}

            {if $settings->add_flag2_enabled}
            <div class="form-group">
                <label>{$settings->add_flag2_name}</label>
                <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if ($product->id && $product->add_flag2) || (!$product->id && $settings->add_flag2_default_value)}active{/if}">
                        <input type="radio" name="add_flag2" value=1 {if ($product->id && $product->add_flag2) || (!$product->id && $settings->add_flag2_default_value)}checked{/if}>Да
                    </label>
                    <label class="btn btn-default4 off {if ($product->id && !$product->add_flag2) || (!$product->id && !$settings->add_flag2_default_value)}active{/if}">
                        <input type="radio" name="add_flag2" value=0 {if ($product->id && !$settings->add_flag2) || (!$product->id && !$settings->add_flag2_default_value)}checked{/if}>Нет
                    </label>
                </div>
            </div>
            {/if}

            {if $settings->add_flag3_enabled}
            <div class="form-group">
                <label>{$settings->add_flag3_name}</label>
                <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if ($product->id && $product->add_flag3) || (!$product->id && $settings->add_flag3_default_value)}active{/if}">
                        <input type="radio" name="add_flag3" value=1 {if ($product->id && $product->add_flag3) || (!$product->id && $settings->add_flag3_default_value)}checked{/if}>Да
                    </label>
                    <label class="btn btn-default4 off {if ($product->id && !$product->add_flag3) || (!$product->id && !$settings->add_flag3_default_value)}active{/if}">
                        <input type="radio" name="add_flag3" value=0 {if ($product->id && !$settings->add_flag3) || (!$product->id && !$settings->add_flag3_default_value)}checked{/if}>Нет
                    </label>
                </div>
            </div>
            {/if}
        {/if}
            
        </div>

        <div class="col-md-6">

            <legend>Статистика</legend>

            <table class="table table-striped table-condensed">
                <tbody>
                <tr>
                    <td>Просмотры</td>
                    <td>1547</td>
                </tr>
                <tr>
                    <td>Покупки</td>
                    <td>14</td>
                </tr>
                <tr>
                    <td>CTR <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                               data-original-title="(Количество просмотров / количество покупок)*100%"><i
                                    class="fa fa-info-circle"></i></a></td>
                    <td>0.9 %</td>
                </tr>
                <tr>
                    <td>Рейтинг <a title="" class="nolink" data-toggle="tooltip" data-placement="bottom" href="#"
                                   data-original-title="Количество оценок">({$product->rating->rating_count})</a></td>
                    <td>{if $product->rating->rating_count > 0}
                            <div class="raiting rate{$product->rating->avg_rating*10}"
                                 title="Рейтинг товара {$product->rating->avg_rating_real|string_format:"%.1f"}">
                            </div>
                        {/if}
                    </td>
                </tr>
                </tbody>
            </table>

            <div class="form-group">
                <label>Дата добавления товара</label>

                <div class="input-group date" id="datepicker">
                    <input class="form-control" type="text" class="span2" name="created_date"
                           value="{if $product}{$product->created|date}{else}{$smarty.now|date}{/if}"/>
                    <span class="input-group-addon add-on"><i class="fa fa-th"></i></span>
                </div>

                {*<input class="form-control" name="created_date" value="{if $product}{$product->created|date}{else}{$smarty.now|date}{/if}">*}
            </div>
            <hr/>
            <label>CSS-класс <a title="" data-toggle="tooltip" data-placement="bottom" href="#"
                                data-original-title="Стиль товара, выводится в стиле body"><i
                            class="fa fa-info-circle"></i></a></label>
            <input type="text" class="form-control" name="css_class" placeholder="" value="{$product->css_class}"/>
               <hr/>
            <legend>Файлы</legend>

            {include file='object-attachment-placeholder.tpl' object=$product attachments_object_name='products'}
                        
        </div>
    </div>
</div>
</div>


<input type="hidden" name="id" value="{$product->id}"/>
<input type="hidden" name="session_id" value="{$smarty.session.id}"/>
<input type="hidden" name="close_after_save" value="0"/>
<input type="hidden" name="add_after_save" value="0"/>
<input type="hidden" name="recreate_seo" value="0"/>
<input type="hidden" name="images_position" value=""/>
<input type="hidden" name="attachments_position" value=""/>
<input type="hidden" name="create_analogs_group" value="0"/>
<input type="hidden" name="change_analogs_group" value="0"/>
<input type="hidden" name="analogs_group_id" value="{$analogs_group_id}"/>
{if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
{if $page}<input type="hidden" name="page" value="{$page}"/>{/if}
{if $category_id}<input type="hidden" name="category_id" value="{$category_id}"/>{/if}
{if $sort}<input type="hidden" name="sort" value="{$sort}"/>{/if}
{if $sort_type}<input type="hidden" name="sort_type" value="{$sort_type}"/>{/if}
</fieldset>
</form>

<script type="text/javascript">

$('#show-analogs-group').click(function(){
    $('#div-analogs-group').removeClass('hidden');
    return false;
});

$('#select-analogs-group').change(function(){
    $('input[name=change_analogs_group]').val(1);
    $(this).closest('form').submit();
    return false;
});

$('#create-analogs-group').click(function(){
    $('input[name=create_analogs_group]').val(1);
    $(this).closest('form').submit();
    return false;
});

$('input[name=use_variable_amount]').change(function(){
    if ($(this).val() == 1)
        $('#variable_amount_panel').show();
    else
        $('#variable_amount_panel').hide();
});

$('input[name=modificators_mode]').change(function(){
    if ($(this).val() == 1)
    {
        $('#modificators div.checkbox').removeClass('disabled');
        $('#modificators div.checkbox input').attr('disabled', false);
    }
    else
    {
        $('#modificators div.checkbox').addClass('disabled');
        $('#modificators div.checkbox input').attr('disabled', true);
    }
});

$('#tablist li a').click(function(){
    var href = "{$config->root_url}{$module->url}{url current_params=$current_params}";
    if (href.indexOf('?')==-1)
        href += "?";
    else
        href += "&";
    href += 'tab=' + $(this).attr('data-url');
    if (typeof history.pushState != undefined) {
        history.pushState(null,null,encodeURI(decodeURI(href)));
    }
});

{***** СОПУТСТВУЮЩИЕ ТОВАРЫ *****}

var search_ajax_context;

//запрос товаров на ajax'e
function products_request_ajax(params) {

    if (params == undefined || params.length == 0) {
        $('#refreshpart').html('');
        return false;
    }

    $('#products-spinner').show();
    {*var url = "{$config->root_url}{$products_module->url}";*}
    var url = "{$config->root_url}/ajax/get_data.php?object=products&mode=product-addrelated";
    if (params != undefined && params.length > 0) {
        var index2 = 0;
        for (var index in params) {
            if (params[index].key != "url") {
                url += "&";
                url += params[index].key + "=" + params[index].value;
                index2 += 1;
            }
        }
    }
    var exception_ids = "{$product->id}";
    $('#related input[type=hidden]').each(function () {
        if (exception_ids.length > 0)
            exception_ids += ',';
        exception_ids += $(this).val();
    });
    var full_url = url + "&exception=" + exception_ids;

    if (search_ajax_context != null)
        search_ajax_context.abort();

    $('#refreshpart').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');

    search_ajax_context = $.ajax({
        type: 'GET',
        url: full_url,
        success: function (data) {
            if (data.success) {
                $('#refreshpart').html(data.data);
                $('#refreshpart li:first').addClass('selected');
                $('#products-spinner').hide();
            }
            search_ajax_context = null;
        }
    });
    return false;
}

{* нажатие Enter в поле поиска *}
$('#searchField').on('keydown', function (e) {
    if (e.keyCode == 13) {
        var curli = $('#refreshpart li.selected');
        curli.click();
        return false;
    }
});

{* поиск keyword *}
$('#searchField').on('keyup', function (e) {
    var params = [];

    //down or up
    if (e.keyCode == 40 || e.keyCode == 38) {
        var curli = $('#refreshpart li.selected');

        if (e.keyCode == 40) {
            var nextli = curli.next();
            if (nextli.length == 0)
                nextli = $('#refreshpart li:first');
        }
        if (e.keyCode == 38) {
            var nextli = curli.prev();
            if (nextli.length == 0)
                nextli = $('#refreshpart li:last');
        }

        curli.removeClass('selected');
        nextli.addClass('selected');
    }
    else {
        if ($(this).val().length > 0) {
            $('#refreshpart').show();
            $('#searchCancelButton').show();
            params.push({
                'key': 'keyword',
                'value': $(this).val()
            });
        }
        else {
            $('#refreshpart').hide();
            $('#searchCancelButton').hide();
        }
        if ($(this).val().length >= {$settings->search_min_lenght} || $(this).val().length == 0)
            products_request_ajax(params);
    }
    return false;
});

$('#refreshpart').on('mouseenter', 'li', function () {
    $('#refreshpart li.selected').removeClass('selected');
    $(this).addClass('selected');
});

// сброс keyword
$('#searchCancelButton').click(function () {
    $('#searchField').val('');
    products_request_ajax();
    $('#refreshpart').hide();
    $('#searchCancelButton').hide();
    return false;
});

$('#related ol').on("click", ".fa-times", function () {
    $(this).closest('li').fadeOut('slow').remove();
    return false;
});

$('#refreshpart').on("click", "li[data-id]", function () {
    var product_id = $(this).attr('data-id');
    var image_src = "";
    var image = $(this).find('div.apimage img');
    if (image.length > 0)
        image_src = image.attr('src');
    var product_name = $(this).find('span.apname').clone().children().remove().end().text();

    //$('#related ol').append('<li class="dd-item dd3-item relatedproducts" data-id="'+product_id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="'+image_src+'"/></div><div class="relatedname"><input type="hidden" name="related_ids[]" value="'+product_id+'"/><a href="
    {$config->root_url}{$module->url}?id='+product_id+'" target="_blank">'+product_name+'</a></div></div></li>');

    $('#no-related-products').hide();

    $('#related ol').append('<li class="dd-item dd3-item relatedproducts ' + ($(this).hasClass('lampoff') ? 'lampoff' : '') + '" data-id="' + product_id + '"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="statusrelated">' + $(this).find('div.apstatus').html() + '</div><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="' + image_src + '"/></div><div class="relatedname"><input type="hidden" name="related_ids[]" value="' + product_id + '"/><a href="{$config->root_url}{$module->url}?id=' + product_id + '" target="_blank">' + product_name + '</a><div class="related-sky">' + $(this).find('span.apsky').html() + '</div></div></div></li>');

    $(this).fadeOut('slow').remove();
    $('#searchField').val('');
    $('#refreshpart').html('');
    $('#refreshpart').hide();
    $('#searchCancelButton').hide();

    $('#related').nestable({
        maxDepth: 1
    });
});

{***** АНАЛОГИ *****}

var search_ajax_context_analogs;

//запрос товаров на ajax'e
function products_request_analogs_ajax(params) {

    if (params == undefined || params.length == 0) {
        $('#refreshpart_analogs').html('');
        return false;
    }

    $('#products-spinner').show();
    {*var url = "{$config->root_url}{$products_module->url}";*}
    var url = "{$config->root_url}/ajax/get_data.php?object=products&mode=product-addrelated";
    if (params != undefined && params.length > 0) {
        var index2 = 0;
        for (var index in params) {
            if (params[index].key != "url") {
                url += "&";
                url += params[index].key + "=" + params[index].value;
                index2 += 1;
            }
        }
    }
    var exception_ids = "{$product->id}";
    $('#analogs input[type=hidden]').each(function () {
        if (exception_ids.length > 0)
            exception_ids += ',';
        exception_ids += $(this).val();
    });
    var full_url = url + "&exception=" + exception_ids;

    if (search_ajax_context_analogs != null)
        search_ajax_context_analogs.abort();

    $('#refreshpart_analogs').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');

    search_ajax_context_analogs = $.ajax({
        type: 'GET',
        url: full_url,
        success: function (data) {
            if (data.success) {
                $('#refreshpart_analogs').html(data.data);
                $('#refreshpart_analogs li:first').addClass('selected');
                $('#products-spinner').hide();
            }
            search_ajax_context_analogs = null;
        }
    });
    return false;
}

{* нажатие Enter в поле поиска *}
$('#searchField_analogs').on('keydown', function (e) {
    if (e.keyCode == 13) {
        var curli = $('#refreshpart_analogs li.selected');
        curli.click();
        return false;
    }
});

{* поиск keyword *}
$('#searchField_analogs').on('keyup', function (e) {
    var params = [];

    //down or up
    if (e.keyCode == 40 || e.keyCode == 38) {
        var curli = $('#refreshpart_analogs li.selected');

        if (e.keyCode == 40) {
            var nextli = curli.next();
            if (nextli.length == 0)
                nextli = $('#refreshpart_analogs li:first');
        }
        if (e.keyCode == 38) {
            var nextli = curli.prev();
            if (nextli.length == 0)
                nextli = $('#refreshpart_analogs li:last');
        }

        curli.removeClass('selected');
        nextli.addClass('selected');
    }
    else {
        if ($(this).val().length > 0) {
            $('#refreshpart_analogs').show();
            $('#searchCancelButton_analogs').show();
            params.push({
                'key': 'keyword',
                'value': $(this).val()
            });
        }
        else {
            $('#refreshpart_analogs').hide();
            $('#searchCancelButton_analogs').hide();
        }
        if ($(this).val().length >= {$settings->search_min_lenght} || $(this).val().length == 0)
            products_request_analogs_ajax(params);
    }
    return false;
});

$('#refreshpart_analogs').on('mouseenter', 'li', function () {
    $('#refreshpart_analogs li.selected').removeClass('selected');
    $(this).addClass('selected');
});

// сброс keyword
$('#searchCancelButton_analogs').click(function () {
    $('#searchField_analogs').val('');
    products_request_analogs_ajax();
    $('#refreshpart_analogs').hide();
    $('#searchCancelButton_analogs').hide();
    return false;
});

$('#analogs ol').on("click", ".fa-times", function () {
    $(this).closest('li').fadeOut('slow').remove();
    return false;
});

$('#refreshpart_analogs').on("click", "li[data-id]", function () {
    var product_id = $(this).attr('data-id');
    var image_src = "";
    var image = $(this).find('div.apimage img');
    if (image.length > 0)
        image_src = image.attr('src');
    var product_name = $(this).find('span.apname').clone().children().remove().end().text();

    $('#no-analogs-products').hide();

    $('#analogs ol').append('<li class="dd-item dd3-item relatedproducts ' + ($(this).hasClass('lampoff') ? 'lampoff' : '') + '" data-id="' + product_id + '"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="statusrelated">' + $(this).find('div.apstatus').html() + '</div><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="' + image_src + '"/></div><div class="relatedname"><input type="hidden" name="analogs_ids[]" value="' + product_id + '"/><a href="{$config->root_url}{$module->url}?id=' + product_id + '" target="_blank">' + product_name + '</a><div class="related-sky">' + $(this).find('span.apsky').html() + '</div></div></div></li>');

    $(this).fadeOut('slow').remove();
    $('#searchField_analogs').val('');
    $('#refreshpart_analogs').html('');
    $('#refreshpart_analogs').hide();
    $('#searchCancelButton_analogs').hide();

    $('#analogs').nestable({
        maxDepth: 1
    });
});


// infinity
$("input[name*=variant][name*=stock]").focus(function () {
    if ($(this).val() == '∞')
        $(this).val('');
    return false;
});

$("input[name*=variant][name*=stock]").blur(function () {
    if ($(this).val() == '')
        $(this).val('∞');
});

$(document).ready(function () {

    {if $message_success}
    info_box($('#notice'), 'Изменения сохранены!', '');
    {/if}

    {if $message_error}
    error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название товара не может быть пустым{/if}')
    {/if}

    $('#datepicker').datepicker({
        format: "dd.mm.yyyy",
        todayBtn: true,
        language: "ru",
        autoclose: true,
        todayHighlight: true
    });

    $("#taggroups tr[data-group] input[type=hidden]").each(function () {
        $(this).select2({
            tags: true,
            separator: '#%#',
            tokenSeparators: ['#%#'],
            createSearchChoice: function (term, data) {
                if ($(data).filter(function () {
                    return this.text.localeCompare(term) === 0;
                }).length === 0) {
                    return {
                        id: term,
                        text: term
                    };
                }
            },
            multiple: true,
            ajax: {
                url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id=" + $(this).attr('data-group-id'),
                {*url: "{$config->root_url}{$module->url}?id={$product->id}&mode=get_tags&ajax=1&group_id="+$(this).attr('data-group-id'),*}
                dataType: "json",
                data: function (term, page) {
                    return {
                        keyword: term
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.data
                    };
                }
            },
            initSelection: function (element, callback) {
                var values = element.val().split('#%#');
                var data = [];

                for (var i = 0; i < values.length; i++)
                    data.push({
                        id: values[i],
                        text: values[i]
                    });

                callback(data);
            }
        });
    });

    $("#autotags").select2({
        tags: true,
        separator: '#%#',
        tokenSeparators: ["#%#"],
        multiple: true,
        initSelection: function (element, callback) {
            var values = element.val().split('#%#');
            var data = [];

            for (var i = 0; i < values.length; i++)
                data.push({
                    id: values[i],
                    text: values[i],
                    locked: true
                });

            callback(data);
        }
    });

    $("#badges").select2({
        tags: true,
        separator: '#%#',
        tokenSeparators: ["#%#"],
        createSearchChoice: function (term, data) {
            {*if ($(data).filter(function () {
                return this.text.localeCompare(term) === 0;
            }).length === 0) {
                return {
                    id: term,
                    text: term
                };
            }*}
        },
        multiple: true,
        ajax: {
            url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_badges",
            {*url: "{$config->root_url}{$module->url}?id={$product->id}&mode=get_badges&ajax=1",*}
            dataType: "json",
            data: function (term, page) {
                return {
                    keyword: term
                };
            },
            results: function (data, page) {
                return {
                    results: data.data
                };
            }
        },
        initSelection: function (element, callback) {
            var values = element.val().split('#%#');
            var data = [];

            for (var i = 0; i < values.length; i++)
                data.push({
                    id: values[i],
                    text: values[i]
                });

            callback(data);
        }
    });

    $('#categories-list select[name="categories_ids[]"]').each(function () {

        {* ВЕРСИЯ С SELECT2 (AJAX)
        $(this).select2({
            minimumInputLength: 1,
            query: function(options){
                var term = options.term;
                $.getJSON( "{$config->root_url}{$module->url}search_category/"+term, function( data, status, xhr ) {
                    options.callback({
                        results: data.data
                    });
                });
            },
            initSelection: function(element, callback){
                var data = {
                    id: element.val(),
                    text: element.attr('data-text')
                };
                callback(data);
            }
        }); *}

        {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX)*}
        $(this).select2({
            placeholder: "Выберите категорию",
            allowClear: true
        });
    });

    {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX)*}
    $('#brandselect').select2({
        placeholder: "Выберите бренд",
        allowClear: true
    });

    $('#variants-list').nestable({
        maxDepth: 1
    });

    $('#related').nestable({
        maxDepth: 1
    });

    $('#analogs').nestable({
        maxDepth: 1
    });
});

$('#seo-toggle').on('shown.bs.tab', function (e) {
  $('textarea[name=meta_keywords], textarea[name=meta_description]').autosize();
});

$('#recreate-seo').click(function(){
    $(this).closest('form').find('input[name=recreate_seo]').val(1);
    $(this).closest('form').submit();
    return false;
});

$("form#product a.save").click(function () {
    $(this).closest('form').submit();
    return false;
});

$("form#product a.saveclose").click(function () {
    $(this).closest('form').find('input[name=close_after_save]').val(1);
    $(this).closest('form').submit();
    return false;
});

$("form#product a.saveadd").click(function () {
    $(this).closest('form').find('input[name=add_after_save]').val(1);
    $(this).closest('form').submit();
    return false;
});

$('form#product').submit(function () {
    var ps = '';
    $("ul#list1 li").each(function () {
        if (ps.length > 0)
            ps += ",";
        ps += $(this).attr('data-id');
    });

    if (ps.length > 0)
        $('form#product input[name=images_position]').val(ps);

    var as = '';
    $("#files li").each(function () {
        if (as.length > 0)
            as += ",";
        as += $(this).attr('data-id');
    });

    if (as.length > 0)
        $('form#product input[name=attachments_position]').val(as);
});

$('#add_new_tag_group').click(function () {
    var ids = [];
    $('#taggroups tr[data-group]').each(function () {
        ids[ids.length] = $(this).attr('data-group');
    });

    $.ajax({
        type: 'POST',
        url: '{$config->root_url}/ajax/get_data.php',
        data: {
            'object': 'products',
            'mode': 'get_tag_groups',
            'ids': ids,
            'session_id': '{$smarty.session.id}'
        },
        {*url: '{$config->root_url}{$module->url}?id={$product->id}&mode=get_tag_groups&ajax=1',
        data: {
            'ids': ids,
            'session_id': '{$smarty.session.id}'
        },*}
        success: function (data) {
            if (data.success) {
                $("#new_group_id").html(data.data);
                $("#new_tag_group").show();
            }
        },
        dataType: 'json'
    });
});

$('#new_group_id').change(function () {
    var group_id = $('#new_group_id option:selected').val();
    var group_name = $('#new_group_id option:selected').attr('data-name');
    var group_postfix = $('#new_group_id option:selected').attr('data-postfix');
    var group_mode = $('#new_group_id option:selected').attr('data-mode');

    if (group_id == undefined || group_name == undefined || group_mode == undefined || group_id == 0 || group_name.length == 0 || group_mode.length == 0) {
        $("#new_tag_group").hide();
        return false;
    }

    switch(group_mode){
        case "logical":
            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><div class="btn-group" data-toggle="buttons"><label class="btn btn-sm btn-default4 on"><input type="radio" name="product_tags[' + group_id + ']" value="да">  Да</label><label class="btn btn-sm btn-default4 off active"><input type="radio" name="product_tags[' + group_id + ']" value="нет" checked> Нет</label></div></td></tr>');
            break;
        case "text":
            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><textarea class="form-control" name="product_tags[' + group_id + ']" data-group-id="' + group_id + '"></textarea></td></tr>');
            break;
        default:
            $('#taggroups tbody').append('<tr data-group="' + group_id + '"><td class="col-md-3">' + group_name + (group_postfix.length > 0 ? ", " + group_postfix : "") + '<a href="#" class="delete-property"><i class="fa fa-times"></i></a></td><td class="col-md-9"><input type="hidden" name="product_tags[' + group_id + ']" data-group-id="' + group_id + '" value=""></td></tr>');
            break;
    }
        
    $('#taggroups tr[data-group=' + group_id + '] a[data-toggle=tooltip]').tooltip();

    if (group_mode != "logical" && group_mode != "text")
        $('#taggroups tr[data-group=' + group_id + '] input[type=hidden]').select2({
            tags: true,
            separator: '#%#',
            tokenSeparators: ["#%#"],
            createSearchChoice: function (term, data) {
                if ($(data).filter(function () {
                    return this.text.localeCompare(term) === 0;
                }).length === 0) {
                    return {
                        id: term,
                        text: term
                    }
                }
            },
            multiple: true,
            ajax: {
                url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id=" + group_id,
                dataType: "json",
                data: function (term, page) {
                    return {
                        keyword: term
                    };
                },
                results: function (data, page) {
                    return {
                        results: data.data
                    };
                }
            }
        });

    $("#new_tag_group").hide();
    return false;
});

$('#taggroups').on('click', '.delete-property', function () {
    $(this).closest('tr').remove();
    return false;
});

{*$('#cancel_tag_group').click(function(){
    $("#new_tag_group").hide();
});

$('#add_tag_group').click(function(){
    var group_id = $('#new_group_id option:selected').val();
    var group_name = $('#new_group_id option:selected').attr('data-name');

    if (group_id == undefined || group_name == undefined)
    {
        $("#new_tag_group").hide();
        return false;
    }

    $('#taggroups tbody').append('<tr data-group="'+group_id+'"><td class="col-md-3">'+group_name+'</td><td class="col-md-9"><input type="hidden" name="product_tags['+group_id+']" data-group-id="'+group_id+'" value=""></td></tr>');
    $('#taggroups tr[data-group='+group_id+'] a[data-toggle=tooltip]').tooltip();

    $('#taggroups tr[data-group='+group_id+'] input[type=hidden]').select2({
        tags: true,
        separator: '#%#',
        tokenSeparators: ["#%#"],
        createSearchChoice: function(term, data) {
            if ($(data).filter(function() {
                return this.text.localeCompare(term) === 0;
            }).length === 0) {
                return {
                    id:term,
                    text: term
                    }
            }
        },
        multiple: true,
        ajax: {
            url: "{$config->root_url}/ajax/get_data.php?object=products&mode=get_tags&group_id="+group_id,
            dataType: "json",
            data: function(term, page) {
              return {
                keyword: term
              };
            },
            results: function(data, page) {
              return {
                results: data.data
              };
            }
        }
    });

    $("#new_tag_group").hide();
    return false;
});*}

$('#add-category').click(function () {
    var li = $('<li></li>');
    var s = $('#new_parent_category_proto').clone();
    s.removeAttr('id');
    //s.removeAttr('style');
    s.show();
    s.attr('name', 'categories_ids[]');
    li.append(s);
    li.append('<div class="categories-right"><a href="#" class="delete-parent_category btn btn-sm btn-link"><i class="fa fa-times"></i></a><div>');
    $('#categories-list').append(li);
    {* ВЕРСИЯ С CHOOSEN
    /*s.chosen({
        allow_single_deselect: true,
        no_results_text: 'Ничего не найдено!'
    });*/*}

    {* ВЕРСИЯ С SELECT2 (AJAX)
    s.select2({
        minimumInputLength: 0,
        query: function(options){
            var term = options.term;
            $.getJSON( "{$config->root_url}{$module->url}search_category/"+term, function( data, status, xhr ) {
                options.callback({
                    results: data.data
                });
            });
        }
    });*}

    {* ВЕРСИЯ С SELECT2 (БЕЗ AJAX)*}
    s.select2({
        placeholder: "Выберите категорию",
        allowClear: true
    });

    return false;
});

$('#categories-list').on("click", ".delete-parent_category", function () {
    $(this).closest('li').fadeOut('slow').remove();
    return false;
});

$('#categories-list').on("click", ".show-modal-category-select", function () {
    $('#modal-category-select').modal('show');
    return false;
});

$('#add-variant').click(function () {
    //$('#variants-list').append('<li class="dd-item dd3-item" data-id=""><div class="dd-handle dd3-handle"></div><div class="dd3-content form-inline"><input type="text"  name="variants[name][]" placeholder="Название варианта" class="form-control variant-name"><input type="text" name="variants[sku][]" placeholder="Артикул" class="form-control variant-sku"><input type="text" name="variants[price][]" placeholder="Цена" class="form-control variant-price"><input type="text" name="variants[price_old][]" placeholder="Старая цена" class="form-control variant-old-price" title="Старая цена"><input type="text" name="variants[stock][]"  value="∞" placeholder="Количество" class="form-control variant-count" title="Количество товара на складе"><input type="hidden" name="variants[visible][]" value="1"/><div class="controllinks"><a class="delete-item" href=""><i class="fa fa-times"></i></a><a class="toggle-item light-on" href=""></a></div></div></li>');
    $('#variants-list ol').append('<li class="dd-item dd3-item" data-id=""><div class="dd-handle dd3-handle"></div><div class="dd3-content form-inline"> <input type="text"  name="variants[name][]" placeholder="Название варианта" class="form-control variant-name"> <input type="text" name="variants[sku][]" placeholder="Артикул" class="form-control variant-sku"> <input type="text" name="variants[price][]" placeholder="0" value="" class="form-control variant-price"> <input type="text" name="variants[price_old][]" placeholder="0" value="" class="form-control variant-old-price" title="Старая цена"> <input type="text" name="variants[stock][]"  value="∞" placeholder="Количество" class="form-control variant-count" title="Количество товара на складе"> <input type="hidden" name="variants[visible][]" value="1"/><div class="controllinks"><a class="delete-item" href=""><i class="fa fa-times"></i></a><a class="toggle-item light-on" href=""></a></div></div></li>');
    return false;
});

$('#variants-list').on("click", ".delete-item", function () {
    $(this).closest('li').fadeOut('slow').remove();
    return false;
});

$('#variants-list').on("click", ".toggle-item", function () {
    var input = $(this).closest('li').find('input[name^="variants[visible]"]');
    var new_val = 1 - parseInt(input.val());
    if (new_val) {
        input.val(1);
        $(this).removeClass('light-off');
        $(this).addClass('light-on');
        $(this).closest('li').removeClass('disabledvar');
    }
    else {
        input.val(0);
        $(this).removeClass('light-on');
        $(this).addClass('light-off');
        $(this).closest('li').addClass('disabledvar');
    }
    return false;
});

$('#delete-product').click(function () {

    confirm_popover_box($(this), function (obj) {
        $.ajax({
            type: 'GET',
            url: "{$config->root_url}{$module->url}{url add=['id'=>$product->id, 'mode'=>'delete', 'ajax'=>1]}",
            success: function (data) {
                document.location = "{$config->root_url}{$main_module->url}{url add=['category_id'=>$category_id, 'page'=>$page]}";
                return false;
            }
        });
    });
    return false;
});
</script>