{* Title *}
{$meta_title='Значения свойств' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions) && !$is_auto}{$allow_edit_module = true}{/if}

<div id="main_list" class="tags_values">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">

        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text" class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}                
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>
                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
                {* последнюю кнопку сброса поиска показывем только на странице где есть результаты поиска *}
            </div>
            </div>

            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['group_id'=>$params_arr['group_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row {if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Значения свойств</legend>
                
                
                <div id="sortview">
                    <div class="itemcount">Тегов: {$tags_count}</div>
                    <ul class="nav nav-pills" id="sort">
                        <li class="disabled">Сортировать по:</li>
                        {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'position'}
                        <li><a class="btn btn-link btn-small current"><span>По порядку</span></a></li>
                        {else}
                        <li><a class="btn btn-link btn-small allow-jump" href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'position','sort_type'=>'asc','page'=>1]}"><span>По порядку</span></a></li>
                        {/if}
                        <li>
                            <a class="btn btn-link btn-small {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'popular'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'popular','sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}fa fa-sort-amount-asc{else}fa fa-sort-amount-desc{/if}"></i> <span>Популярности</span></a>
                        </li>
                        <li>
                            <a class="btn btn-link btn-small {if $params_arr['sort'] == 'name'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'name', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}fa fa-sort-alpha-desc{else}fa fa-sort-alpha-asc{/if}"></i> <span>Алфавиту</span></a>
                        </li>
                    </ul>
                </div>
                
                <div id="new_menu" class="dd">
                 {if $tags}
                 
                 {include file='pagination.tpl'}
                 <div class="itemslist">
                    <ol class="dd-list">
                        {foreach $tags as $tag_value}
                            <li class="dd-item dd3-item" data-id="{$tag_value->id}">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <input type="checkbox" value="{$item->id}" class="checkb">
                                    
                                    <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag_value->id, 'group_id'=>$params_arr['group_id']]}"{/if}>{$tag_value->name|escape}</a>
                                    <div class="controllinks">

                                    <a class="favorite-tag {if $tag_value->is_popular}on{else}off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag_value->id, 'mode'=>'toggle_popular', 'ajax'=>1]}"  title="Свойство {if $tag_value->is_popular}популярное{else}непопулярное{/if}"></a>

                                        <code class="opcity50">{$tag_value->id}</code>
                                        {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag_value->id, 'mode'=>'delete', 'ajax'=>1]}"><i class="fa fa-times"></i></a>
                                        <a class="toggle-item {if $tag_value->enabled}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$tag_value->id, 'mode'=>'toggle', 'ajax'=>1]}"></a>{/if}
                                        
                                    </div>
                                </div>
                            </li>
                        {/foreach}
                    </ol>
                    </div>
                    {include file='pagination.tpl'}

                    {else}
                        <p>Нет значений свойств</p>
                    {/if}
                    
                    {* Массовые операции для тегов *}

                {if $allow_edit_module}
                <div class="form-inline massoperations">
                    <div class="checkall">Выделить все &uarr;</div>
                    <select id="mass_operation" class="form-control">
                        <option value='1'>Сделать видимыми</option>
                        <option value='2'>Скрыть</option>
                        <option value='3'>Удалить</option>
                    </select>
                    <button id="apply_mass_operation" data-color="purple" class="btn btn-default ladda-button" data-style="expand-right"><span class="ladda-label">Применить</span></button>
                </div>
                {/if}
                    
                </div>
            </div>
        
        <div class="col-md-4">
        <legend>Группы тегов</legend>
        <div class="list-group taggroups">


                 
                 <a href="{$config->root_url}{$module->url}" class="list-group-item {if !array_key_exists('group_id', $params_arr)}active{/if}"><span class="badge">{$all_tags_count}</span> Все группы</a>
                 
                {foreach $tags_groups as $group}

                    <a href="{$config->root_url}{$module->url}{url add=['group_id'=>$group->id]}" class="list-group-item {if array_key_exists('group_id', $params_arr) && $params_arr['group_id']==$group->id}active {/if}">
                    <span class="badge">{$group->tags_count}</span>
                    {$group->name}
                    </a>
                {/foreach}
        </div>
        </div>

    </form>
</div>
</div>
<script type="text/javascript">
    {if $tags}
        $(document).ready(function(){
            $('#new_menu').nestable({
                maxDepth: 1
            });
        });

        $('a.save').click(function(){
            var obj = $('#notice');
            var _menu = $('#new_menu').nestable('serialize');
            $.ajax({
                type: 'POST',
                url: $(this).closest('form').attr('action'),
                data: {
                    menu: _menu},
                error: function() {
                    error_box(obj, 'Изменения не сохранены!', '');
                },
                success: function(data) {
                    info_box(obj, 'Изменения сохранены!', '');
                }
            });
            return false;
        });
    {/if}

    $('.favorite-tag').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var visible = item.hasClass('on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('on');
                item.removeClass('off');
                visible = 1 - visible;
                if (visible)
                    item.addClass('on');
                else
                    item.addClass('off');
            }
        });
        return false;
    });

    $('.delete-item').on('click', function() {
        var href = $(this).attr('href');
        var li = $(this).closest('li');
        var menu_title = $(this).closest('li').find('a:first').text();
        $.get(href, function(data) {
            if (data.success)
                location.reload();
        });
        return false;
    });

    $('.toggle-item').on('click', function() {
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('div.checkall').click(function(){
        $("#new_menu input[type=checkbox]").each(function(){
            $(this).attr('checked', true);
        });
    });

    $('#apply_mass_operation').click(function(){
        var operation = parseInt($('#mass_operation option:selected').attr('value'));
        var items_count = $("#new_menu input[type=checkbox]:checked").length;

        if (operation == 3 && items_count>0){
            var l = Ladda.create(document.querySelector( '#apply_mass_operation' ));
            l.start();
            l.setProgress( 0 );
            var interval = 1 / items_count;
            var ok_count = 0;

            $("#new_menu input[type=checkbox]:checked").each(function(index, value){
                var item = $(this).closest('li');
                var subitem = item.find('a.delete-item').first();
                var href = subitem.attr('href');
                $.get(href, function(data) {
                    if (data.success)
                    {
                        items_count--;
                        ok_count++;
                        l.setProgress( ok_count * interval );
                        if (items_count == 0)
                        {
                            l.stop();
                            location.reload();
                        }
                        return false;
                    }
                });
            });
        }
        else
        $("#new_menu input[type=checkbox]:checked").each(function(){
            var item = $(this).closest('li');
            switch(operation){
                case 1:    //Видимость
                    var subitem = item.find('a.toggle-item');
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-on'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-off');
                                subitem.addClass('light-on');
                            }
                        });
                    break;
                case 2: //Скрытие
                    var subitem = item.find('a.toggle-item');
                    var href = subitem.attr('href');
                    if (!subitem.hasClass('light-off'))    //выполним только для выключенных
                        $.get(href, function(data) {
                            if (data.success)
                            {
                                subitem.removeClass('light-on');
                                subitem.addClass('light-off');
                            }
                        });
                    break;
                {*case 3:    //Удаление
                    var subitem = item.find('a.delete-item');
                    var href = subitem.attr('href');
                    $.get(href, function(data) {
                        if (data.success)
                        {
                            items_count--;
                            if (items_count == 0)
                                location.reload();
                        }
                    });
                    break;*}
            }
        });
        return false;
    });
</script>