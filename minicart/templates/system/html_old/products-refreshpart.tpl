{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}
<div id="sortview">
    <div class="itemcount">Товаров: {$products_count}</div>
    <ul class="nav nav-pills" id="sort">
                   <li class="disabled">Сортировать по:</li>
                {if !array_key_exists('sort', $params_arr) || $params_arr['sort'] == 'position'}
                <li><a class="btn btn-link btn-small current"><span>По порядку</span></a></li>
                {else}
                <li><a class="btn btn-link btn-small allow-jump" href="{$config->root_url}{$module->url}{url current_params=$current_params add=['sort'=>'position','sort_type'=>'asc','page'=>1]}"><span>По порядку</span></a></li>
                {/if}
                    
                <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'price'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'price' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'price' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'price', 'sort_type'=>'asc','page'=>1]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == 'price' && $params_arr['sort_type'] == 'asc'}Сначала дорогие{else}Сначала дешевые{/if}"><i class="{if $params_arr['sort'] == 'price' && $params_arr['sort_type'] == 'desc'}fa fa-sort-amount-desc{else}fa fa-sort-amount-asc{/if}"></i> <span>Цене</span></a></li>
                <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'popular'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'popular', 'sort_type'=>'asc','page'=>1]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'asc'}Сначала не популярные{else}Сначала популярные{/if}"><i class="{if $params_arr['sort'] == 'popular' && $params_arr['sort_type'] == 'desc'}fa fa-sort-amount-asc{else}fa fa-sort-amount-desc{/if}"></i> <span>Популярности</span></a></li>
                <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'newest'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'newest', 'sort_type'=>'desc','page'=>1]}{/if}" data-placement="bottom" data-toggle="tooltip" data-original-title="{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}Сначала старые{else}Сначала новые{/if}"><i class="{if $params_arr['sort'] == 'newest' && $params_arr['sort_type'] == 'desc'}fa fa-sort-numeric-desc{else}fa fa-sort-numeric-asc{/if}"></i> <span>Новизне</span></a></li>
                <li><a class="btn btn-link btn-small allow-jump {if $params_arr['sort'] == 'name'}current{/if}" href="{$config->root_url}{$module->url}{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'asc'}{url current_params=$current_params add=['sort_type'=>'desc','page'=>1]}{elseif $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}{url current_params=$current_params add=['sort_type'=>'asc','page'=>1]}{else}{url current_params=$current_params add=['sort'=>'name', 'sort_type'=>'asc','page'=>1]}{/if}"><i class="{if $params_arr['sort'] == 'name' && $params_arr['sort_type'] == 'desc'}fa fa-sort-alpha-desc{else}fa fa-sort-alpha-asc{/if}"></i> <span>Алфавиту</span></a></li>
                    
    </ul>
</div>


                {if $products}
                
                {include file='pagination.tpl'}
                    <div class="itemslist">
                    <ol class="dd-list" data-href="{$config->root_url}{$module->url}{url current_params=$current_params}">
                        {foreach $products as $product}
                            <li data-id="{$product->id}" class="dd-item dd3-item">
                                <div class="dd-handle dd3-handle"></div>
                                <div class="dd3-content">
                                    <input type="checkbox" class="checkb" value="{$item->id}">
                                    <div class="product-block">
                                            
                                            <div class="product-left">
                                                <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$product->id, 'page'=>$current_page_num, 'category_id'=>$category_id, 'sort'=>$params_arr['sort'], 'sort_type'=>$params_arr['sort_type']]}"{/if} class="product-name-list">{$product->name}
                                                </a>
                                                
                                                {if $settings->catalog_show_product_id}<div class="product-id">{$product->id}</div>{/if}
                                                {if $settings->catalog_show_sku && $product->variant->sku}<div class="product-sky">{$product->variant->sku}</div>{/if}

                                                {if $settings->add_field1_enabled && !empty($product->add_field1)}{$settings->add_field1_name}: {$product->add_field1}{/if}

                                                {if $settings->add_field2_enabled && !empty($product->add_field2)}{$settings->add_field2_name}: {$product->add_field2}{/if}

                                                {if $settings->add_field3_enabled && !empty($product->add_field3)}{$settings->add_field3_name}: {$product->add_field3}{/if}

                                                {if $settings->add_flag1_enabled}
                                                    <a href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle_flag1', 'id'=>$product->id, 'ajax'=>1]}" class="toggle-flag1-item {if $product->add_flag1}flag1-on{else}flag1-off{/if}" title="{$settings->add_flag1_name}: {if $product->add_flag1}Да{else}Нет{/if}" data-title="{$settings->add_flag1_name}"></a>
                                                {/if}

                                                {if $settings->add_flag2_enabled}
                                                    <a href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle_flag2', 'id'=>$product->id, 'ajax'=>1]}" class="toggle-flag2-item {if $product->add_flag2}flag2-on{else}flag2-off{/if}" title="{$settings->add_flag2_name}: {if $product->add_flag2}Да{else}Нет{/if}" data-title="{$settings->add_flag2_name}"></a>
                                                {/if}

                                                {if $settings->add_flag3_enabled}
                                                    <a href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle_flag3', 'id'=>$product->id, 'ajax'=>1]}" class="toggle-flag3-item {if $product->add_flag3}flag3-on{else}flag3-off{/if}" title="{$settings->add_flag3_name}: {if $product->add_flag3}Да{else}Нет{/if}" data-title="{$settings->add_flag3_name}"></a>
                                                {/if}
                                            </div>


                                            <div class="variants-block">
                                                <div class="onevariant">
                                                    <div class="variantname"><span>{$product->variant->name|truncate:10:"...":true}</span></div>
                                                    <div class="product-quantity">{if $product->variant}<input type="text" class="form-control" name="variants_stock[{$product->variant->id}]" data-variant-id="{$product->variant->id}" value="{if $product->variant->infinity || $product->variant->stock == ''}∞{else}{$product->variant->stock|escape}{/if}">{/if}</div>

                                                    <div class="product-price">{if $product->variant}

                                                    <input type="text" class="form-control" name="variants_price[{$product->variant->id}]" data-variant-id="{$product->variant->id}" value="{$product->variant->price|convert:NULL:false}">
                                                    {if $settings->catalog_show_currency}<div class="product-currency">{if !empty($product->currency_id)}{$currencies[$product->currency_id]->sign}{else}{$admin_currency->sign}{/if}</div>{/if}
                                                    </div>
                                                    {/if}
                                                </div>


                                
                                            {if $product->variants|count > 1}
                                            <div id="accordion{$product->id}">
                                                <div id="collapseVariants{$product->id}" style="display:none;">
                                                    {foreach $product->variants|cut as $variant}
                                                        <div class="onevariant">
                                                            <div class="variantname">{$variant->name|truncate:10:"...":true}</div>
                                                            <div class="product-quantity"><input type="text" class="form-control" name="variants_stock[{$variant->id}]" data-variant-id="{$variant->id}" value="{if $variant->infinity || $variant->stock == ''}∞{else}{$variant->stock|escape}{/if}"></div>
                                                            <div class="product-price">

                                                                    <input type="text" class="form-control" name="variants_price[{$variant->id}]" data-variant-id="{$variant->id}" value="{$variant->price|convert:NULL:false}">
                                                                    <div class="product-currency">{if !empty($product->currency_id)}{$currencies[$product->currency_id]->sign}{else}{$admin_currency->sign}{/if}</div>
                                                </div>
                                                

                                                        </div>
                                                    {/foreach}
                                                </div>
                                                <a class="accordion-toggle showvariants" href="#collapseVariants{$product->id}">Варианты <span class="darr">&darr;</span><span class="uarr" style="display:none;">&uarr;</span></a>
                                            </div>
                                            {/if}
                                        </div>
                                        </div>
                                        
                                        <div class="productimage">{if $product->image}<img src='{$product->image->filename|resize:products:50:50}' alt='{$product->image->name}'>{/if}</div>
                                        
                                    </div>
                                        
                                        
                                        
                                    <div class="rightside">
                                        <div class="controllinks">
                                            <code class="opcity50">{$product->id}</code>
                                            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'delete', 'id'=>$product->id, 'ajax'=>1]}" class="delete-item"><i class="fa fa-times"></i></a>
                                            <a href="{$config->root_url}{$edit_module->url}{url add=['mode'=>'toggle', 'id'=>$product->id, 'ajax'=>1]}" class="toggle-item {if $product->is_visible}light-on{else}light-off{/if}"></a>{/if}
                                        </div>
                                    </div>
                            </li>
                        {/foreach}

                    </ol>
                </div>
                {include file='pagination.tpl'} 
                
                {else}
                    <p>Нет товаров</p>
                {/if}


