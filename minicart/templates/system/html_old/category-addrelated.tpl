{if $categories}

    <div id="new_menu" class="dd">
        {if $categories}
            <ol class="addlist">
                {foreach $categories as $category}
                    <li data-id="{$category->id}" data-image="{$category->image->filename|resize:categories:40:40}" data-name="{$category->name}" {if !$category->is_visible}class="lampoff"{/if}>
                        <div class="apimage">{if $category->image}<img src='{$category->image->filename|resize:categories:30:30}' alt='{$category->image->name}'>{/if}</div>

                        <span class="apname">
                            {$category->name}
                            <span class="apcode">{$category->id}</span>
                        </span>
                    </li>
                {/foreach}
            </ol>
        {/if}

        {*include file='pagination.tpl'*}
    </div>

{else}
    <div class="noresults">Ничего не найдено...</div>
{/if}