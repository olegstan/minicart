{* Title *}
{if $modificators_group->id}{$meta_title='Редактирование группы модификаторов' scope=parent}{else}{$meta_title='Добавление группы модификаторов' scope=parent}{/if}

<form method=post id="modificator">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url add=['parent_id'=>$params_arr['parent_id']]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
            <div class="col-md-8">
                <legend>{if $modificators_group->id}Редактирование группы модификаторов{else}Добавление группы модификаторов{/if}</legend>

            
            <div class="row">
                    <div class="col-md-8">
            
                    <div class="form-group">
                        <label>Название группы</label>
                        <input type="text" class="form-control" name="name" value="{$modificators_group->name|escape_string}">
                    </div>

                    </div>
                    
                    <div class="col-md-4">
                        <div class="statusbar">
                    
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if !$modificators_group || $modificators_group->is_visible}active{/if}">
                        <input type="radio" name="visible" value=1 {if !$modificators_group || $modificators_group->is_visible}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $modificators_group && !$modificators_group->is_visible}active{/if}">
                        <input type="radio" name="visible" value=0 {if $modificators_group && !$modificators_group->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
                    <div class="statusbar-text">Статус:</div>
                    </div>
                    </div>
                
                </div>
                
                <div class="form-group">
                        <label>Вид отображения модификатора</label>
                        <div class="radio">
                          <label>
                            <input type="radio" name="type" value="checkbox" {if $modificators_group->type == "checkbox" || !$modificators_group}checked{/if}>Галочки (Чекбоксы)
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="type" value="radio" {if $modificators_group->type == "radio"}checked{/if}>Радиобаттон
                          </label>
                        </div>
                        <div class="radio">
                          <label>
                            <input type="radio" name="type" value="select" {if $modificators_group->type == "select"}checked{/if}>Выпадающий список
                          </label>
                        </div>
                        
                    </div>
                
                
                <div class="col-md-4">
                </div>
        </div>

        <input type="hidden" name="id" value="{$modificators_group->id}"/>
        <input type="hidden" name="parent_id" value="{if $parent_id}{$parent_id}{else}0{/if}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $('form#modificator select[name=type]').change(function(){
        var option = $(this).find('option:selected').val();
        if (option == 'plus_fix_sum' || option == 'minus_fix_sum'){
            $('#fix-sum').show();
            $('#percent').hide();
        }
        else if (option == 'plus_percent' || option == 'minus_percent'){
            $('#fix-sum').hide();
            $('#percent').show();
        }
        else{
            $('#fix-sum').hide();
            $('#percent').hide();
        }
    });

    $("form#modificator a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#modificator a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#modificator a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>