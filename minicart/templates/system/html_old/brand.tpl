{* Title *}
{if $brand->id}{$meta_title='Редактирование бренда' scope=parent}{else}{$meta_title='Добавление бренда' scope=parent}{/if}

<form method=post id="brand">
    <fieldset>
        <div class="controlgroup">
                        {if $brand}
            <div class="openlink">
                <a href="{$config->root_url}{$brand_frontend_module->url}{$brand->url}{$settings->postfix_brand_url}" target="_blank"><i class="fa fa-external-link"></i> <span>Открыть бренд в новом окне</span></a>
            </div>
                        {/if}

            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
        <div class="col-md-8">
        <legend>{if $brand}Редактирование бренда{else}Добавление бренда{/if}</legend>
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Наименование бренда</label>
        <div class="input-group">
            <input type="text"  class="form-control input-bold" name="name" placeholder="Введите наименование бренда" value="{$brand->name}"/>
             <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id бренда">id:{$brand->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">                
            <div class="statusbar">
                    
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $brand->is_visible || !$brand}active{/if}">
                        <input type="radio" name="visible" id="option1" value=1 {if $brand->is_visible || !$brand}checked{/if}> Активен
                      </label>
                      <label class="btn btn-default4 off {if $brand && !$brand->is_visible}active{/if}">
                        <input type="radio" name="visible" id="option2" value=0 {if $brand && !$brand->is_visible}checked{/if}> Скрыт
                      </label>
                    </div>
                    <div class="statusbar-text">Статус:</div>
                    
                    </div>
        
     
        
            </div>
        

          
        </div>
       
        <hr />
        <div class="form-group">
        <label class="bigname">Описание верхнее <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается до списка товаров бренда"><i class="fa fa-info-circle"></i></a></label>
        <textarea name="description" class="form-control ckeditor" rows="4">{$brand->description}</textarea>
        </div>
        <div class="form-group">
        <label class="bigname">Описание нижнее <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается после списка товаров бренда"><i class="fa fa-info-circle"></i></a></label>
        <textarea name="description2" class="form-control ckeditor" rows="8">{$brand->description2}</textarea>
        </div>
</div>
<div class="col-md-4">
                <legend>Изображения</legend>

                {include file='object-image-placeholder.tpl' object=$brand images_object_name='brands'}


                        <hr/>

        <div class="form-group">
            <label>Бренд популярный <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Если отмечено 'Да' бренд будет показан в списке популярных брендов, если отмечено 'Нет' - бренд будет отображаться в блоке брендов только при клике на кнопку 'Все'"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if $brand->is_popular}active{/if}">
                    <input type="radio" name="is_popular" id="option1" value=1 {if $brand->is_popular}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if !$brand || !$brand->is_popular}active{/if}">
                        <input type="radio" name="is_popular" id="option2" value=0 {if !$brand || !$brand->is_popular}checked{/if}> Нет
                </label>
            </div>
        </div>
            <hr/>
                
                <legend class="mt20">Параметры SEO <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Все параметры заполняются автоматически, но при необходимости их можно заполнить вручную"><i class="fa fa-info-circle"></i></a> <a href="#" id="recreate-seo" class="btn btn-default btn-refresh" title="Пересоздать данные"><i class="fa fa-refresh"></i></a></legend>
                
                <div class="form-group">
                <label>URL</label>
                <div class="input-group">
                <span class="input-group-addon">{$settings->prefix_brand_url}/</span>
                <input type="text"  class="form-control" name="url" placeholder="Введите URL" value="{$brand->url}"/>
                <span class="input-group-addon">{$settings->postfix_brand_url}</span>
                </div>
                </div>





                        <div class="form-group">
        <label>Meta-title</label>
        <input type="text"  class="form-control" name="meta_title" placeholder="Введите meta-title" value="{$brand->meta_title}"/>
        </div>
                
                
                <div class="form-group">
                <label>Ключевые слова <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-keywords"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_keywords" class="form-control">{$brand->meta_keywords}</textarea>
                </div>
                
                <div class="form-group">
                <label>Описание <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Meta-description"><i class="fa fa-info-circle"></i></a></label>
                <textarea name="meta_description"  class="form-control">{$brand->meta_description}</textarea>
                </div>
                
                <div class="form-group">
                    <label>Заголовок страницы бренда <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Отображается внутри H1 на странице бренда, это поле опционально, оно нужно если необходимо чтобы заголовок страницы бренда отличался от названия бренда"><i class="fa fa-info-circle"></i></a></label>
                    <input type="text"  class="form-control" name="frontend_name" placeholder="Введите наименование бренда" value="{$brand->frontend_name}"/>
                </div>
                
               <hr />
               <div class="form-group">
               <label>CSS-класс  <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Стиль страницы бренда, выводится в стиле body"><i class="fa fa-info-circle"></i></a></label>
               <input type="text" class="form-control" name="css_class" placeholder="" value="{$brand->css_class}"/>
               </div>

                <div class="form-group">
                <label>Автоматические теги бренда <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Эти теги назначаются всем товарам этого бренда"><i class="fa fa-info-circle"></i></a></label>

        <input type="hidden" id="brand_autotags" value="{$brand_tag->name}">
        </div>
                
               <hr /> 
                
</div> 
        
        <input type="hidden" name="id" value="{$brand->id}"/>
        <input type="hidden" name="tag_id" value="{if $brand->tag_id}{$brand->tag_id}{else}0{/if}"/>
        <input type="hidden" name="return_page" value="{if $page}{$page}{else}1{/if}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="recreate_seo" value="0"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
    </fieldset>
        </div>

</form>

<script type="text/javascript">
    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $('#brand_autotags').select2({
            tags: true,
            separator: '#%#',
            tokenSeparators: ["#%#"],
            createSearchChoice: function(term, data) {
                if ($(data).filter(function() {
                    return this.text.localeCompare(term) === 0;
                }).length === 0) {
                    return {
                        id: term,
                        text: term
                    };
                }
            },
            multiple: true,
            locked: true,
            initSelection: function(element, callback){
                var values = element.val().split('#%#');
                var data = [];

                for(var i = 0; i < values.length; i++)
                    data.push({
                        id: values[i],
                        text: values[i],
                        locked: true
                    });

                callback(data);
            }
        });

        $('textarea[name=meta_keywords], textarea[name=meta_description]').autosize();
    });

    $('#recreate-seo').click(function(){
        $(this).closest('form').find('input[name=recreate_seo]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#brand a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#brand a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#brand a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $('form#brand').submit(function(){
        var ps = '';
        $("ul#list1 li").each(function(){
            if (ps.length > 0)
                ps += ",";
            ps += $(this).attr('data-id');
        });

        if (ps.length > 0)
            $('form#brand input[name=images_position]').val(ps);
    });
</script>