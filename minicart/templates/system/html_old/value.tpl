{* Title *}
{$meta_title='Редактирование значения свойства' scope=parent}

<form method=post id="tag" action="{$config->root_url}{$module->url}">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url add=['group_id'=>$group_id]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

<div class="row">
    <div class="col-md-8">
        <legend>{if $tag}Редактирование значения свойства{else}Добавление значения свойства{/if}</legend>
        
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Название значения свойства</label>
        <div class="input-group">
        <input type="text"  class="form-control" name="name" placeholder="Введите тег" value="{$tag->name|escape_string}"/>
        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id тега">id:{$tag->id}</a></span>
        </div>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
        <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $tag->enabled || !$tag}active{/if}">
                        <input type="radio" name="enabled" id="option1" value=1 {if $tag->enabled || !$tag}checked{/if}>  Активен
                      </label>
                      <label class="btn btn-default4 off {if $tag && !$tag->enabled}active{/if}">
                        <input type="radio" name="enabled" id="option2" value=0 {if $tag && !$tag->enabled}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
       
           <div class="form-group">
        </div> 
        <div class="row">
        <div class="col-md-6">
        <div class="form-group">
        <label>Группа тегов</label>
        <select name="group_id" required class="form-control">
            {foreach $tags_groups as $group}
                <option value="{$group->id}" {if $group->id == $tag->group_id || $group_id == $group->id}selected{/if}>{$group->name|escape_string}</option>
            {/foreach}
        </select>
        </div>        
        </div>
        </div>

        <input type="hidden" name="id" value="{$tag->id}"/>
        {if $group_id}<input type="hidden" name="group_id" value="{$group_id}"/>{/if}
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
        {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
    </div>
        <div class="col-md-4">
            <legend>Альтернативные значения</legend>
            <table class="table table-striped" id="alternative_values">
                <colgroup>
                  <col class="span6">
                  <col class="span2">
                </colgroup>
                <thead>
                  <tr>
                    <th colspan="">Альт. значение</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {foreach $alternative_values as $v}
                    <tr>
                        <td><input type="hidden" name="alternative_values[id][{$v->id}]" value="{$v->id}"/><input type="text"  сlass="form-control" name="alternative_values[name][{$v->id}]" value="{$v->name}"/></td>
                        <td><a class="delete-item" href="{$config->root_url}{$module->url}{url add=['id'=>$tag->id, 'mode'=>'delete_alternative', 'alternative_id'=>$v->id, 'ajax'=>1]}"><i class="fa fa-times"></i></a></td>
                    </tr>
                  {/foreach}
                </tbody>
            </table>
            <table class="table table-stripped">
                <colgroup>
                  <col class="span6">
                  <col class="span2">
                </colgroup>
                <thead>
                  <tr>
                    <th colspan="2">Добавление альтернативного значения</th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td ><input type="text" class="form-control" id="alternative_value" value=""/></td>
                        <td><button type="button" class="btn btn-success" id="add_alternative_value"><i class="fa fa-plus fa fa-white"></i></button></td>
                    </tr>
                </tbody>
            </table>

            <div class="tagimage">
                <legend>Изображение тега</legend>

                {include file='object-image-placeholder.tpl' object=$tag images_object_name='tags'}

            </div>
       
    </fieldset>
</form>

</div>    
</div>

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), '{if $message_error == "error"}Ошибка при сохранении!{else}{$message_error}{/if}', '')
        {/if}
    });

    $("form#tag a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#tag").on("click", "a.delete-item", function(){
        var href = $(this).attr('href');
        var tr = $(this).closest('tr');
        if (href == '#')
        {
            tr.remove();
        }
        else
        {
            $.get(href, function(data) {
                if (data.success)
                {
                    tr.remove();
                }
            });
        }
        return false;
    });
    $("#add_alternative_value").click(function(){
        var v = $("#alternative_value").val();
        if (v.length == 0)
            return false;
        {if $tag}
            var href = $(this).closest('form').attr('action') + "?id={$tag->id}&mode=add_alternative&ajax=1&name=" + encodeURIComponent(v);
            $.get(href, function(data) {
                if (data.success)
                {
                    $("table#alternative_values tbody").append("<tr><td><input type=\"hidden\" name=\"alternative_values[id]["+data.id+"]\" value=\""+data.id+"\"/><input type=\"text\" class=\"form-control\" name=\"alternative_values[name]["+data.id+"]\" value=\""+v+"\"/></td><td><a class=\"delete-item\" href=\"{$config->root_url}{$module->url}?id={$tag->id}&mode=delete_alternative&ajax=1&alternative_id="+data.id+"\"><i class=\"fa fa-times\"></i></a></td></tr>");
                }
            });
        {else}
            $("table#alternative_values tbody").append("<tr><td><input type=\"hidden\" name=\"alternative_values[id][]\" value=\"\"/><input type=\"text\" class=\"form-control\" name=\"alternative_values[name][]\" value=\""+v+"\"/></td><td><a class=\"delete-item\" href=\"#\"><i class=\"fa fa-times\"></i></a></td></tr>");
        {/if}
        $("#alternative_value").val('');
        return false;
    });
</script>

</div>