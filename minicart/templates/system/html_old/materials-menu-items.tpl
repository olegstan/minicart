{* Title *}
{$meta_title='Пункты меню' scope=parent}

{$allow_edit_module = false}
{if in_array($edit_module->module, $global_group_permissions)}{$allow_edit_module = true}{/if}

<div id="main_list" class="pages_menu_items">
    <form id="form-menu" method="post" action="{$config->root_url}{$module->url}?save_positions">
        <input type="hidden" name="session_id" value="{$smarty.session.id}">
        <div class="controlgroup">
            <div class="currentfilter">
            <div class="input-group">
                <input id="searchField" type="text"  class="form-control" placeholder="Поиск" value="{$keyword}">
                <span class="input-group-btn">
                {if $keyword}
                <button id="searchCancelButton" class="btn btn-default" type="button serchremove"><i class="fa fa-times"></i></button>                {/if}
                <button id="searchButton" class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
            </div>
            {if $allow_edit_module}<a href="{$config->root_url}{$edit_module->url}{url add=['menu_id'=>$params_arr['menu_id']]}" class="btn btn-default btn-sm add"><i class="fa fa-plus"></i> Создать</a>{/if}
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        </div>

        <div class="row{if $active_menu && $active_menu->css_class} com-{$active_menu->css_class}{/if}">
            <div class="col-md-8">
                <legend>Меню</legend>
                {if $menu_items}

                <div id="new_menu" class="dd">
                    {function name=categories_tree}
                        {if $items}
                            <ol class="dd-list">
                                {foreach $items as $item}
                                    <li class="dd-item dd3-item" data-id="{$item->id}">
                                        <input type="checkbox" value="{$item->id}" class="checkb">
                                        <div class="dd-handle dd3-handle"></div>
                                        <div class="dd3-content {if $keyword && !in_array($item->id, $items_filtered_ids)}searchnone{/if}">
                                            <a {if $allow_edit_module}href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id]}"{/if} class="catname">{$item->name|escape}</a>

                                        </div>
                                        <div class="controllinks">
                                                <span class="label">{if $item->object_type == "none"}Не выбрано
                                                {elseif $item->object_type == "material"}Материал
                                                {elseif $item->object_type == "material-category"}Категория материалов
                                                {elseif $item->object_type == "product"}Товар
                                                {elseif $item->object_type == "category"}Категория товаров
                                                {elseif $item->object_type == "href"}Ссылка
                                                {elseif $item->object_type == "separator"}Разделитель
                                                {elseif $item->object_type == "sitemap"}Карта сайта
                                                {/if}</span>
                                                {*
                                                <code class="opcity50">{$item->id}</code>
                                                *}
                                                {if $allow_edit_module}<a class="delete-item" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'action'=>'delete']}"><i class="fa fa-times"></i></a>
                                                <a class="toggle-item {if $item->is_visible}light-on{else}light-off{/if}" href="{$config->root_url}{$edit_module->url}{url add=['id'=>$item->id, 'action'=>'toggle']}"></a>{/if}
                                                {if $item->is_main}<span title="Этот материал будет отображен на главной странице"><i class="fa fa-home""></i></span>{/if}
                                            </div>
                                        {if $item->subitems}{categories_tree items=$item->subitems}{/if}
                                    </li>
                                {/foreach}
                            </ol>
                        {/if}
                    {/function}
                    {categories_tree items=$menu_items}
                </div>

                {else}
                    <p>Нет пунктов меню</p>
                {/if}
            </div>
            <div class="col-md-4">
                <legend>Пользовательские</legend>
                <div class="list-group">
                    {foreach $menu as $m}
                        {if !$m->is_static}<a href="{$config->root_url}{$module->url}{url add=['menu_id'=>$m->id]}" class="list-group-item {if $params_arr['menu_id'] == $m->id}active{/if}">{$m->name}</a>{/if}
                    {/foreach}
                </div>
                <legend>Системные</legend>
                <div class="list-group">
                    {foreach $menu as $m}
                        {if $m->is_static}<a href="{$config->root_url}{$module->url}{url add=['menu_id'=>$m->id]}" class="list-group-item {if $params_arr['menu_id'] == $m->id}active{/if}">{$m->name}</a>{/if}
                    {/foreach}
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        $('#new_menu').nestable({
            maxDepth: 10
        });
    });

    $('a.save').click(function(){
        var obj = $('#notice');
        var _menu = $('#new_menu').nestable('serialize');
        $.ajax({
            type: 'POST',
            url: $(this).closest('form').attr('action'),
            data: {
                menu: _menu},
            error: function() {
                error_box(obj, 'Изменения не сохранены!', '');
            },
            success: function(data) {
                info_box(obj, 'Изменения сохранены!', '');
            }
        });
        return false;
    });

    $('#searchButton').click(function(){
        if ($('#searchField').val().length > 0)
            document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
        return false;
    });

    $('#searchField').keypress(function(e){
        if (e.which == 13)
        {
            if ($(this).val().length > 0)
                document.location.href = '{$config->root_url}{$module->url}?keyword=' + encodeURI($('#searchField').val());
            return false;
        }
    });

    $('#searchCancelButton').click(function(){
        var href = '{$config->root_url}{$module->url}';
        document.location.href = href;
        return false;
    });

    $('#right').on('click', 'a.delete-item', function(e){
        confirm_popover_box($(this), function(obj){
            delete_menu_item(obj);
        });
        return false;
    });

    function delete_menu_item(obj){
        var href = obj.attr('href');
        var li = obj.closest('li');
        $.get(href, function(data){
            if (data.success)
                location.reload();
            return false;
        });
        return false;
    }

    $('#right').on('click', 'a.toggle-item', function(e){
        var item = $(this);
        var href = item.attr('href');
        var enabled = item.hasClass('light-on')?1:0;
        $.get(href, function(data) {
            if (data.success)
            {
                item.removeClass('light-on');
                item.removeClass('light-off');
                enabled = 1 - enabled;
                if (enabled)
                    item.addClass('light-on');
                else
                    item.addClass('light-off');
            }
            return false;
        });
        return false;
    });
</script>