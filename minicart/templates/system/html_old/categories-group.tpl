{* Title *}
{$meta_title='Редактирование группы категорий' scope=parent}

<form method=post id="group">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}{url current_params=$current_params add=['category_id'=>$materials_category_id, 'page'=>$page]}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
            <div class="col-md-8">
                <legend>{if $group_on_main->id}Редактирование группы категорий{else}Добавление группы категорий{/if}</legend>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Название блока <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Например: Популярные категории! или Новые разделы!"><i class="fa fa-info-circle"></i></a></label>
                            <div class="input-group">
                                <input type="text" class="form-control" name="name" value="{$group_on_main->name|escape_string}">
                                <span class="input-group-addon">
                                <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id группы категорий">id:{$group_on_main->id}</a>
                                </span>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="statusbar">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default4 on {if $group_on_main->is_visible || !$group_on_main}active{/if}">
                                <input type="radio" name="visible" id="option1" value=1 {if $group_on_main->is_visible || !$group_on_main}checked{/if}>  Активен
                                </label>
                                <label class="btn btn-default4 off {if $group_on_main && !$group_on_main->is_visible}active{/if}">
                                <input type="radio" name="visible" id="option2" value=0 {if $group_on_main && !$group_on_main->is_visible}checked{/if}> Скрыт
                                </label>
                            </div>
                            <div class="statusbar-text">Статус:</div>
                        </div>
                    </div>
                </div>


                        <legend>Категории</legend>
                        <div id="related" class="dd">
                        <ol class="dd-list">
                            {if $related_categories}
                                {foreach $related_categories as $related}
                                    <li class="dd-item dd3-item relatedproducts {if !$related->is_visible}lampoff{/if}" data-id="{$related->id}">
                                        <div class="dd-handle dd3-handle"></div>
                                        <div class="dd3-content">
                                            <div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div>
                                            <div class="relatedimage">
                                            {if $related->image}<img src="{$related->image->filename|resize:categories:30:30}" alt="{$related->image->name}"/>{/if}
                                            </div>
                                            <div class="relatedname">
                                                <input type="hidden" name="related_ids[]" value="{$related->id}"/>
                                                <a href="{$config->root_url}{$category_module->url}{url add=['id'=>$related->id]}" target="_blank">{$related->name}</a>
                                            </div>
                                         </div>
                                    </li>
                                {/foreach}
                            {else}
                                <div id="no-related-categories">Пока нет категорий</div>
                            {/if}
                        </ol>
                        </div>

                        <div id="main_list" class="tags_values">
                            <div class="apsearch form-inline">

                                    <input id="searchField" type="text"  class="form-control" placeholder="Поиск" autocomplete="off">
                                    <button id="searchCancelButton" style="display: none;" class="btn btn-default searchremove" type="button"><i class="fa fa-times"></i></button>

                            </div>
                            <div id="refreshpart" class="addcontainer" style="display:none;">{include file='category-addrelated.tpl'}</div>
                        </div>

            </div>



            <div class="col-md-4">
                <legend>Настройки отображения</legend>
                <div class="form-group">
                    <label>Количество категорий в блоке</label>
                    <input type="text" class="form-control" name="categories_count" value="{if $group_on_main}{$group_on_main->categories_count}{else}9{/if}">
                </div>
                
               {* Вот этот код правильный нужно из него добавить кнопку По умолчанию *}
               {*
               <div class="form-group">
                    <div class="btn-group btn-lists" data-toggle="buttons">
                       <label class="btn btn-default on {if !$category || $category->show_mode == ''}active{/if}">
                        <input type="radio" name="show_mode" value='' {if !$category || $category->show_mode == ''}checked{/if}> По умолчанию
                      </label>
                      <label class="btn btn-default on {if $category->show_mode == 'list'}active{/if}">
                        <input type="radio" name="show_mode" value='list' {if $category->show_mode == 'list'}checked{/if}>  <i class="fa fa-bars"></i> Списком
                      </label>
                      <label class="btn btn-default off {if $category->show_mode == 'tile'}active{/if}">
                        <input type="radio" name="show_mode" value='tile' {if $category->show_mode == 'tile'}checked{/if}> <i class="fa fa-th"></i> Плиткой
                      </label>
                    </div>                    
                </div>
                *}
                
                

                <div class="form-group">
                    <label>Css-класс блока</label>
                    <input type="text" class="form-control" name="css_class" value="{$group_on_main->css_class}">
                </div>

            </div>
        </div>

        <input type="hidden" name="id" value="{$group_on_main->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        $('#related').nestable({
            maxDepth: 1
        });

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название группы не может быть пустым{/if}')
        {/if}
    });

    var search_ajax_context;

    //запрос категорий на ajax'e
    function categories_request_ajax(params){

        if (params == undefined || params.length == 0){
            $('#refreshpart').html('');
            return false;
        }


        var url = "{$config->root_url}{$main_module->url}";
        if (params != undefined && params.length>0){
            url += "?";
            for(var index in params)
            {
                if (params[index].key == "url")
                {
                    url = params[index].value;
                    if (params.length > 1)
                        url += "?";
                    break;
                }
            }

            var index2 = 0;
            for(var index in params)
            {
                if (params[index].key != "url")
                {
                    if (index2 > 0)
                        url += "&";
                    url += params[index].key + "=" + params[index].value;
                    index2 += 1;
                }
            }
        }
        var full_url = url;
        if (params != undefined && params.length > 0)
            full_url += "&";
        var exception_ids = "";
        $('#related input[type=hidden]').each(function(){
            if (exception_ids.length>0)
                exception_ids += ',';
            exception_ids += $(this).val();
        });
        full_url += "ajax=1&format=category-addrelated&exception="+exception_ids;

        if (search_ajax_context != null)
            search_ajax_context.abort();

        $('#refreshpart').show().html('<div class="noresults"><i class="fa fa-spinner fa-spin fa-large"></i></div>');

        search_ajax_context = $.ajax({
            type: 'GET',
            url: full_url,
            success: function(data) {
                $('#refreshpart').html(data);
                search_ajax_context = null;
            }
        });
        return false;
    }

    {* нажатие Enter в поле поиска *}
    $('#searchField').on('keydown', function(e){
        if (e.keyCode == 13)
        {
            var curli = $('#refreshpart li.selected');
            curli.click();
            return false;
        }
    });

    {* поиск keyword *}
    $('#searchField').on('keyup', function(e){
        var params = [];

        //down or up
        if (e.keyCode == 40 || e.keyCode == 38)
        {
            var curli = $('#refreshpart li.selected');

            if (e.keyCode == 40)
            {
                var nextli = curli.next();
                if (nextli.length == 0)
                    nextli = $('#refreshpart li:first');
            }
            if (e.keyCode == 38)
            {
                var nextli = curli.prev();
                if (nextli.length == 0)
                    nextli = $('#refreshpart li:last');
            }

            curli.removeClass('selected');
            nextli.addClass('selected');
        }
        else
        {
            if ($(this).val().length > 0)
            {
                $('#refreshpart').show();
                $('#searchCancelButton').show();
                params.push({
                    'key': 'keyword',
                    'value': $(this).val()
                });
            }
            else
            {
                $('#refreshpart').hide();
                $('#searchCancelButton').hide();
            }
            categories_request_ajax(params);
        }
        return false;
    });

    $('#refreshpart').on('mouseenter', 'li', function(){
        $('#refreshpart li.selected').removeClass('selected');
        $(this).addClass('selected');
    });

    // сброс keyword
    $('#searchCancelButton').click(function(){
        $('#searchField').val('');
        categories_request_ajax();
        $('#refreshpart').hide();
        $('#searchCancelButton').hide();
        return false;
    });

    $('#related ol').on("click", ".fa-times", function(){
        $(this).closest('li').fadeOut('slow').remove();
        return false;
    });

    $('#refreshpart').on("click", "li[data-id]", function(){
        var category_id = $(this).attr('data-id');
        var image_src = "";
        var image = $(this).find('div.apimage img');
        if (image.length > 0)
            image_src = image.attr('src');
        var category_name = $(this).find('span.apname').html();

        $('#no-related-categories').hide();

        $('#related ol').append('<li class="dd-item dd3-item relatedproducts ' + ($(this).hasClass('lampoff')?'lampoff':'') + '" data-id="'+category_id+'"><div class="dd-handle dd3-handle"></div><div class="dd3-content"><div class="delrelated"><a href="#" class="delete-item"><i class="fa fa-times"></i></a></div><div class="relatedimage"><img src="'+image_src+'"/></div><div class="relatedname"><input type="hidden" name="related_ids[]" value="'+category_id+'"/><a href="{$config->root_url}{$module->url}?id='+category_id+'" target="_blank">'+category_name+'</a></div></div></li>');

        $(this).fadeOut('slow').remove();
        $('#searchField').val('');
        $('#refreshpart').html('');
        $('#refreshpart').hide();
        $('#searchCancelButton').hide();

        $('#related').nestable({
            maxDepth: 1
        });
    });

    $("form#group a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $("form#group a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $("form#group a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
</script>