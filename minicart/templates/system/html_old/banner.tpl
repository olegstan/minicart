{* Title *}
{if $banner->id}{$meta_title='Редактирование баннера' scope=parent}{else}{$meta_title='Добавление баннера' scope=parent}{/if}

<form method=post id="banner">
    <fieldset>
        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="#" class="btn btn-default btn-sm saveadd"><i class="fa fa-plus"></i> Сохранить и создать</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

        <div class="row">
            <div class="col-md-8">
                <legend>{if $banner->id}Редактирование баннера{else}Добавление баннера{/if}</legend>

            <div class="row">
                   <div class="col-md-8">
                    <div class="form-group">
                        <label>Название баннера</label>
                        <div class="input-group">
                        <input type="text" class="form-control" name="name" value="{$banner->name|escape_string}"/>
                        <span class="input-group-addon"><a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Системный id баннера">id:{$banner->id}</a></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                        <div class="statusbar">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default4 on {if $banner->is_visible || !$banner}active{/if}">
                                    <input type="radio" name="visible" value=1 {if $banner->is_visible || !$banner}checked{/if}> Активен
                                </label>
                                <label class="btn btn-default4 off {if $banner && !$banner->is_visible}active{/if}">
                                    <input type="radio" name="visible" value=0 {if $banner && !$banner->is_visible}checked{/if}> Скрыт
                                </label>
                            </div>
                            <div class="statusbar-text">Статус:</div>
                        </div>
                </div>
               </div>
            
            <div class="form-group">
                <label>Редактор</label>
                <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $banner->show_editor || !$banner}active{/if}">
                        <input type="radio" name="show_editor" value=1 {if $banner->show_editor || !$banner}checked{/if}> Показать
                    </label>
                    <label class="btn btn-default4 off {if $banner && !$banner->show_editor}active{/if}">
                        <input type="radio" name="show_editor" value=0 {if $banner && !$banner->show_editor}checked{/if}> Скрыть
                    </label>
                </div>
            </div>
            
            <div class="form-group">
                <textarea name="text" class="form-control {if $banner->show_editor || !$banner}ckeditor{/if}" rows="10">{$banner->text}</textarea>
            </div>
        
        </div>


        <div class="col-md-4">
            {if $banner->id}
            <legend>Код для вставки на сайт</legend>
            <code class="widecode">{literal}{banner id={/literal}{$banner->id}{literal}}{/literal}</code>
            <p class="help-block">Вставьте этот код в нужное место в шаблоне сайта.</p>
            {/if}
           
            <div class="form-group">
                <label>Css-стиль баннера</label>
                <input type="text" class="form-control" name="css_class" value="{$banner->css_class}"/>
            </div>
            
            <hr/>
            
            <div class="form-group">
                <label>Баннер является ссылкой?</label> {* По умолчанию нет *}
                <div class="btn-group noinline" data-toggle="buttons">
                    <label class="btn btn-default4 on {if $banner->is_link}active{/if}">
                        <input type="radio" name="is_link" value=1 {if $banner->is_link}checked{/if}> Да
                    </label>
                    <label class="btn btn-default4 off {if !$banner->is_link}active{/if}">
                        <input type="radio" name="is_link" value=0 {if !$banner->is_link}checked{/if}> Нет
                    </label>
                </div>
            </div>
            
            <div id="link-section" {if !$banner->is_link}style="display:none;"{/if}>
                <div class="form-group">
                    <label>URL ссылки</label>
                    <input type="text" class="form-control" name="link" value="{$banner->link}"/>
                </div>

                <div class="form-group">
                    <label>Как открыть</label>
                    <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default {if !$banner->link_in_new_window}active{/if}">
                        <input type="radio" name="link_in_new_window" autocomplete="off" value=0 {if !$banner->link_in_new_window}checked{/if}>В текущем окне</label>
                      <label class="btn btn-default {if $banner->link_in_new_window}active{/if}">
                        <input type="radio" name="link_in_new_window" autocomplete="off" value=1 {if $banner->link_in_new_window}checked{/if}> В новом окне</label>
                    </div>
                </div>
            </div>
            
        </div>
        

        <input type="hidden" name="id" value="{$banner->id}"/>
        <input type="hidden" name="return_page" value="{if $page}{$page}{else}1{/if}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="add_after_save" value="0"/>
    </fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '{if $message_error == "empty_name"}Название баннера не может быть пустым{/if}')
        {/if}
    });

    $('input[name=is_link]').change(function(){
        if ($(this).val() == 0)
            $('#link-section').hide();
        else
            $('#link-section').show();
    });

    $("form#banner a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#banner a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });
    $("form#banner a.saveadd").click(function(){
        $(this).closest('form').find('input[name=add_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $('form#banner').submit(function(){
        var ps = '';
        $("ul#list1 li").each(function(){
            if (ps.length > 0)
                ps += ",";
            ps += $(this).attr('data-id');
        });

        if (ps.length > 0)
            $('form#banner input[name=images_position]').val(ps);
    });

    $('input[name=show_editor]').change(function(){
        if ($(this).val() == 0)
            CKEDITOR.instances['text'].destroy();
        else
            CKEDITOR.replace('text');
    });
</script>