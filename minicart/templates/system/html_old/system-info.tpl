{$meta_title = "Информация о системе" scope=parent}

<!-- Основная форма -->
<form method=post id=product>
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
    </div>

    <!-- Параметры -->
    <div class="row">
        <div class="col-md-8">

            {if $mode == "system"}

            <legend>Права доступа на каталоги</legend>
               
               
             <table class="table table-striped">
              <thead>
                <tr>
                  <th>Каталог</th>
                  <th>Состояние доступа</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>compiled</td>
                  <td><span class="label {if $compiled_permissions == '777'}label-success{else}label-danger{/if}">{if $compiled_permissions == '777'}Доступно{else}Недоступно{/if}</span></td>
                </tr>
                <tr>
                  <td>files</td>
                  <td><span class="label {if $files_permissions == '777'}label-success{else}label-danger{/if}">{if $files_permissions == '777'}Доступно{else}Недоступно{/if}</span></td>
                </tr>
                <tr>
                  <td>system/compiled</td>
                  <td><span class="label {if $system_compiled_permissions == '777'}label-success{else}label-danger{/if}">{if $system_compiled_permissions == '777'}Доступно{else}Недоступно{/if}</span></td>
                </tr>
              </tbody>
            </table>     
            
            
           <legend>Информация о системе</legend>               
               
             <table class="table table-striped">
              <thead>
                <tr>
                  <th>Параметр</th>
                  <th>Текущее значение</th>
                  <th>Рекомендуемое</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Версия PHP</td>
                  <td>{$php_version}</td>
                  <td>5.3 или выше</td>
                </tr>
                <tr>
                  <td>Версия базы данных</td>
                  <td>{$mysql_version}</td>
                  <td>5.1 или выше</td>
                </tr>
                <tr>
                  <td>Кодировка базы данных</td>
                  <td>{$database_codepage}</td>
                  <td>utf8</td>
                </tr>
              </tbody>
            </table>

            {else}

                {$php_info}

            {/if}
        </div>
        
        <div class="col-md-4">
            <div class="list-group">
              <a href="{$config->root_url}{$module->url}{url add=['mode'=>'system']}" class="list-group-item {if $mode == "system"}active{/if}">
                Основные проверки
              </a>
              <a href="{$config->root_url}{$module->url}{url add=['mode'=>'php']}" class="list-group-item {if $mode == "php"}active{/if}">Информация о PHP</a>
            </div>
        </div>
    </div>


</form>
<!-- Основная форма (The End) -->

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
</script>