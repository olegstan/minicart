{$meta_title = "Настройки" scope=parent}

<!-- Основная форма -->
<form method=post id=product enctype="multipart/form-data" action="{$config->root_url}{$module->url}mode/{if $mode}{$mode}{else}main{/if}/">
    <input type=hidden name="session_id" value="{$smarty.session.id}">

    <div class="controlgroup">
        <a href="" class="btn btn-success btn-sm save"><i class="fa fa-check"></i>  Сохранить</a>
    </div>

    <div class="row">
        <div class="col-md-9" id="settings-content">

        <!-- Параметры -->
        {if $mode}
            {include file='settings/'|cat:$mode|cat:'.tpl'}
        {else}
            {include file='settings/main.tpl'}
        {/if}
        <!-- Параметры (The End)-->

            </div>

        <div class="col-md-3">
            <div class="list-group" id="settings-switch">
                  <a href="{$config->root_url}{$module->url}mode/main/" data-settings="main" class="list-group-item {if $mode=="main" || !$mode}active{/if}">Общие настройки</a>
                 <a href="{$config->root_url}{$module->url}mode/catalog/" data-settings="catalog" class="list-group-item {if $mode=="catalog"}active{/if}">Каталог товаров</a>
                  <a href="{$config->root_url}{$module->url}mode/cart/" data-settings="cart" class="list-group-item {if $mode=="cart"}active{/if}">Корзина и Заказы</a>
                <a href="{$config->root_url}{$module->url}mode/reviews-products/" data-settings="reviews-products" class="list-group-item {if $mode=="reviews-products"}active{/if}">Отзывы о товарах</a>
                <a href="{$config->root_url}{$module->url}mode/news/" data-settings="news" class="list-group-item {if $mode=="news"}active{/if}">Новости</a>
                  <a href="{$config->root_url}{$module->url}mode/watermarks/" data-settings="watermarks" class="list-group-item {if $mode=="watermarks"}active{/if}">Водяные знаки</a>
                  <a href="{$config->root_url}{$module->url}mode/search/" data-settings="search" class="list-group-item {if $mode=="search"}active{/if}">Поиск</a>
                  <a href="{$config->root_url}{$module->url}mode/viewed-products/" data-settings="viewed-products" class="list-group-item {if $mode=="viewed-products"}active{/if}">Вы смотрели</a>
                  <a href="{$config->root_url}{$module->url}mode/404/" data-settings="404" class="list-group-item {if $mode=="404"}active{/if}">Страница 404</a>
                  <a href="{$config->root_url}{$module->url}mode/yandex-market/" data-settings="yandex-market" class="list-group-item {if $mode=="yandex-market"}active{/if}">Выгрузка в Яндекс-Маркет</a>
                <a href="{$config->root_url}{$module->url}mode/clear-cache/" data-settings="clear-cache" class="list-group-item {if $mode=="clear-cache"}active{/if}">Очистка кеша</a>

                <a href="{$config->root_url}{$module->url}mode/breadcrumbs/" data-settings="breadcrumbs" class="list-group-item {if $mode=="breadcrumbs"}active{/if}">Хлебные крошки</a>

                <a href="{$config->root_url}{$module->url}mode/currencies/" data-settings="currencies" class="list-group-item {if $mode=="currencies"}active{/if}">Цена и валюта</a>

                <a href="{$config->root_url}{$module->url}mode/discounts-and-promo/" data-settings="discounts-and-promo" class="list-group-item {if $mode=="discounts-and-promo"}active{/if}">Скидки и акции</a>

                <a href="{$config->root_url}{$module->url}mode/social-networks/" data-settings="social-networks" class="list-group-item {if $mode=="social-networks"}active{/if}">Вход через соц.сети</a>
              </div>
            
            <hr/>
            
            <div class="list-group" id="settings-switch">
                <a href="{$config->root_url}{$module->url}mode/licence/" data-settings="licence" class="list-group-item {if $mode=="licence"}active{/if}">Лицензия</a>
            </div>
            
        </div>
    </div>
</form>

<script type="text/javascript">

    $(document).ready(function(){

        {if $message_success_ex}
            info_box($('#notice'), '{$message_success_ex}', '');
        {elseif $message_success}
            info_box($('#notice'), 'Настройки сохранены!', '');
        {/if}

        {if $message_error}
            error_box($('#notice'), 'Ошибка при сохранении!', '')
        {/if}
    });

    $("form a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });

    $('#settings-switch a').click(function(){
        $('#settings-switch a.active').removeClass('active');
        $(this).addClass('active');
        var template = $(this).attr('data-settings');
        var new_url = $(this).attr('href');
                $(this).closest('form').attr('action', new_url);
        if (template.length > 0)
            $.ajax({
                type: 'GET',
                url: '{$config->root_url}{$module->url}get/'+template,
                success: function(data) {
                    if (data.success)
                    {
                        if (typeof history.pushState != 'undefined') {
                            history.pushState(null,null,encodeURI(new_url));
                        }
                        $('#settings-content').html(data.data);
                        $('a[data-toggle="tooltip"]').tooltip();
                    }
                }
            });
        return false;
    });

    $('#settings-content').on('click', '#clear-image-cache', function(){
        $('input[name=clear-image-cache]').val(1);
        $(this).closest('form').submit();
    });

    $('#settings-content').on('click', '#recreate-auto-properties', function(){
        $('input[name=recreate-auto-properties]').val(1);
        $(this).closest('form').submit();
    });

    $('#settings-content').on('click', '#clear-search-history', function(){
        $('input[name=clear-search-history]').val(1);
        $(this).closest('form').submit();
    });

</script>
