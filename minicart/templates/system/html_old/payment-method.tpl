{* Title *}
{$meta_title='Редактирование метода оплаты' scope=parent}

<form method=post id="payment_method">

        <div class="controlgroup">
            <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
            <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
            <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
        </div>

    <fieldset>
        <legend>{if $payment_method}Редактирование метода оплаты{else}Добавление метода оплаты{/if}</legend>

    <div class="row">
        <div class="col-md-8">
        <div class="row">
        <div class="col-md-8">
        <div class="form-group">
        <label>Наименование метода оплаты</label>
        <input type="text" class="form-control" name="name" placeholder="Введите наименование метода оплаты" value="{$payment_method->name|escape_string}"/>
        </div>
        </div>
        <div class="col-md-4">
              <div class="statusbar">
              <div class="btn-group" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $payment_method->enabled || !$payment_method}active{/if}">
                        <input type="radio" name="enabled" id="option1" value=1 {if $payment_method->enabled || !$payment_method}checked{/if}> Активен
                      </label>
                      <label class="btn btn-default4 off {if $payment_method && !$payment_method->enabled}active{/if}">
                        <input type="radio" name="enabled" id="option2" value=0 {if $payment_method && !$payment_method->enabled}checked{/if}> Скрыт
                      </label>
                    </div>
              <div class="statusbar-text">Статус:</div>
              </div>
        </div>
        </div>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Валюта</label>
                    <select name="currency_id" class="form-control">
                        {foreach $currencies as $currency}
                            <option value='{$currency->id}' {if $currency->id==$payment_method->currency_id}selected{/if}>{$currency->name|escape}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label>Модуль оплаты <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="В случае 'Ручной обработки' модуль не производит никаких действий и подходит, например, для оплаты Наличными, другие модули подразумевают какое-либо действие при их выборе, например, формирование квитанции сбербанка или ввод данных пластиковой карты" class="fa fa-legend"><i class="fa fa-info-circle"></i></a></label> {*$payment_method->module*}
                    <select class="form-control" name="payment_module_id">
                      <option value="0">Ручная обработка</option>
                      {foreach $all_external_modules as $exm}
                        <option value="{$exm->id}" {if $payment_method->payment_module_id == $exm->id}selected{/if}>{$exm->name}</option>
                      {/foreach}
                    </select>
                </div>
            </div>
        </div>
        
        
         <div class="row">
                    <div class="col-md-6">
                        <label>Вид операции</label>
                        <select class="form-control" name="operation_type">
                           <option value="nothing" {if $payment_method->operation_type == "nothing"}selected{/if}>Не изменяем цену</option>
                          <option value="plus_fix_sum" {if $payment_method->operation_type == "plus_fix_sum"}selected{/if}>+ фиксированная сумма</option>
                          <option value="minus_fix_sum" {if $payment_method->operation_type == "minus_fix_sum"}selected{/if}>- фиксированная сумма</option>
                          <option value="plus_percent" {if $payment_method->operation_type == "plus_percent"}selected{/if}>+ % от цены товара</option>
                          <option value="minus_percent" {if $payment_method->operation_type == "minus_percent"}selected{/if}>- % от цены товара</option>
                        </select>
                    </div>
                    
                    <div class="col-md-6" id="fix-sum" {if $payment_method->operation_type == 'plus_fix_sum' || $payment_method->operation_type == 'minus_fix_sum'}{else}style="display: none;"{/if}>
                        <label>Сумма в {$main_currency->sign}</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="value_fix_sum" value="{if $payment_method->operation_type == 'plus_fix_sum' || $payment_method->operation_type == 'minus_fix_sum'}{$payment_method->operation_value}{/if}">
                          <span class="input-group-addon">{$main_currency->sign}</span>
                        </div>
                    </div>
                    
                    <div class="col-md-4" id="percent" {if $payment_method->operation_type == 'plus_percent' || $payment_method->operation_type == 'minus_percent'}{else}style="display: none;"{/if}>
                        <label>Укажите %</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="value_percent" value="{if $payment_method->operation_type == 'plus_percent' || $payment_method->operation_type == 'minus_percent'}{$payment_method->operation_value}{/if}">
                          <span class="input-group-addon">%</span>
                        </div>
                    </div>
            </div> 
            
        <div class="form-group mt15">
            <label>Разрешить оплату без подтверждения администратором <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="'Да' - пользователи смогут оплачивать заказ сразу после оформления; 'Нет' - для оплаты нужно подтверждение администратора, только после него пользователи смогут оплатить заказ"><i class="fa fa-info-circle"></i></a></label>
            <div class="btn-group noinline" data-toggle="buttons">
                <label class="btn btn-default4 on {if !$payment_method || $payment_method->allow_payment}active{/if}">
                    <input type="radio" name="allow_payment" value=1 {if !$payment_method || $payment_method->allow_payment}checked{/if}> Да
                </label>
                <label class="btn btn-default4 off {if $payment_method && !$payment_method->allow_payment}active{/if}">
                    <input type="radio" name="allow_payment" value=0 {if $payment_method && !$payment_method->allow_payment}checked{/if}> Нет
                </label>
            </div>
            
            
        </div>
        
        <hr/>
        
        <div class="form-group">
        <label>Описание</label>
        <textarea name="description" class="ckeditor form-control" rows="6">{$payment_method->description}</textarea>
        </div> 

        <input type="hidden" name="id" value="{$payment_method->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
        <input type="hidden" name="images_position" value=""/>
        {if $temp_id}<input type="hidden" name="temp_id" value="{$temp_id}"/>{/if}
        </div>
        
    
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Способы доставки</h3>
                </div>
                <ul class="list-group">
                {foreach $all_delivery_methods as $delivery_method}
                    {*
                    <div class="form-group">
                    <label class="checkbox">
                        <input type=checkbox name="delivery_methods[]" value='{$delivery_method->id}' id="name{$delivery_method->id}" {if !empty($delivery_methods) && in_array($delivery_method->id, $delivery_methods)}checked{/if}> <label for="name{$delivery_method->id}" class="{if !empty($delivery_methods) && in_array($delivery_method->id, $delivery_methods)}checked{else}notchecked{/if}">{$delivery_method->name}</label>
                    </label>
                    </div>
                    *}
                    
                    <li class="list-group-item list-group-select  {if !empty($delivery_methods) && in_array($delivery_method->id, $delivery_methods)}list-group-item-success{/if} {if !$delivery_method->enabled}list-group-item-hidden{/if}">
                        <div class="select-onoff onoff">
                            <div data-toggle="buttons" class="btn-group">
                                <label class="btn btn-default4 btn-xs on {if !empty($delivery_methods) && in_array($delivery_method->id, $delivery_methods)}active{/if}">
                                    <input type="radio" value="{$delivery_method->id}" name="delivery_methods[{$delivery_method->id}]" {if !empty($delivery_methods) && in_array($delivery_method->id, $delivery_methods)}checked{/if}><i class="fa fa-check"></i></label>
                                <label class="btn btn-default4 btn-xs off {if (!empty($delivery_methods) && !in_array($delivery_method->id, $delivery_methods)) || empty($delivery_methods)}active{/if}">
                                    <input type="radio" value="" name="delivery_methods[{$delivery_method->id}]" {if (!empty($delivery_methods) && !in_array($delivery_method->id, $delivery_methods)) || empty($delivery_methods)}checked{/if}><i class="fa fa-times"></i></label>
                            </div>
                        </div>
                        {$delivery_method->name} {if !$delivery_method->enabled}(скрыт){/if}
                    </li>
                    
                    
                {/foreach}

            </ul>
        </div>
    
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Изображения</h3>
            </div>
            <div class="panel-body">
                {include file='object-image-placeholder.tpl' object=$payment_method images_object_name='payment'}
            </div>
        </div>
    </div>
    </div>
        
    </fieldset>
</form>

<script type="text/javascript">

    $('.list-group .list-group-item label.btn').click(function(){
        var li = $(this).closest('li.list-group-item');
        if ($(this).find('input').val().length > 0)
            li.addClass('list-group-item-success');
        else
            li.removeClass('list-group-item-success');
    });

    $('form#payment_method select[name=operation_type]').change(function(){
        var option = $(this).find('option:selected').val();
        if (option == 'plus_fix_sum' || option == 'minus_fix_sum'){
            $('#fix-sum').show();
            $('#percent').hide();
        }
        else if (option == 'plus_percent' || option == 'minus_percent'){
            $('#fix-sum').hide();
            $('#percent').show();
        }
        else{
            $('#fix-sum').hide();
            $('#percent').hide();
        }
    });

    $(document).ready(function(){

        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $("form#payment_method a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#payment_method a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $('form#payment_method').submit(function(){
        var ps = '';
        $("ul#list1 li").each(function(){
            if (ps.length > 0)
                ps += ",";
            ps += $(this).attr('data-id');
        });

        if (ps.length > 0)
            $('form#payment_method input[name=images_position]').val(ps);
    });
</script>