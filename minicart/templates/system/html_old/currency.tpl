{* Title *}
{$meta_title='Редактирование валюты' scope=parent}

<form method=post id="currency">
    <div class="controlgroup">
        <a href="#" class="btn btn-success btn-sm save"><i class="fa fa-check"></i> Сохранить</a>
        <a href="#" class="btn btn-default btn-sm saveclose"><i class="fa fa-check-square-o"></i> Сохранить и закрыть</a>
        <a href="{$config->root_url}{$main_module->url}" class="btn btn-danger btn-sm cancel"><i class="fa fa-times"></i> Отменить</a>
    </div>


    <fieldset>
        <legend>{if $currency_item}Редактирование валюты{else}Добавление валюты{/if}</legend>

        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Наименование валюты</label>
                            <input type="text" class="form-control" name="name" placeholder="Введите наименование валюты" value="{$currency_item->name|escape_string}"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="statusbar">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-default4 on {if $currency_item->enabled || !$currency_item}active{/if}">
                                <input type="radio" name="enabled" id="option1" value=1 {if $currency_item->enabled || !$currency_item}checked{/if}>  Активен
                                </label>
                                <label class="btn btn-default4 off {if $currency_item && !$currency_item->enabled}active{/if}">
                                <input type="radio" name="enabled" id="option2" value=0 {if $currency_item && !$currency_item->enabled}checked{/if}> Скрыт
                                </label>
                            </div>
                            <div class="statusbar-text">Статус:</div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Курс валюты относительно валюты по умолчанию</label>
                    <div class="row">
                        <div class="col-md-1 ravno">1{$currency_item->sign} =</div>

                        <div class="col-md-3 kurs-right">
                            <div class="input-group">
                                <input type="text" class="form-control" name="rate_to" value="{$currency_item->rate_to}">
                                <span class="input-group-addon">{$main_currency->sign}</span>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <a href="#" class="btn btn-default" id="update_currency_rate"><i class="fa fa-refresh"></i> Обновить курс с CBR.ru</a>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label>Знак</label>
                    <input type="text" class="form-control" name="sign" placeholder="Введите знак" value="{$currency_item->sign|escape_string}"/>
                </div>

                <div class="form-group">
                    <label>Упрощенный знак (для экспорта)</label>
                    <input type="text" class="form-control" name="sign_simple" placeholder="Введите знак" value="{$currency_item->sign_simple|escape_string}"/>
                </div>
 
                <div class="form-group">
                    <label>Код ISO</label>
                    <input type="text" class="form-control" name="code" placeholder="Введите код ISO" value="{$currency_item->code}"/>
                </div>

                <div class="form-group">
                    <label>Знаков после запятой</label>
                    <input type="text" class="form-control" name="cents" placeholder="Знаков после запятой" value="{$currency_item->cents}"/>
                </div>
            </div>

            <div class="col-md-4">
                <legend>Статус</legend>
                                
                <div class="form-group">
                    <label>Расчетная валюта <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Используем эту валюту для расчетов на сайте"><i class="fa fa-info-circle"></i></a></label>
                
                    <div class="btn-group noinline" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $currency_item->use_main}active{/if}">
                        <input type="radio" name="use_main" value="1" {if $currency_item->use_main}checked{/if}> Да
                      </label>
                      <label class="btn btn-default4 off {if !$currency_item || !$currency_item->use_main}active{/if}">
                        <input type="radio" name="use_main" value="0" {if !$currency_item || !$currency_item->use_main}checked{/if}> Нет
                      </label>
                    </div>
                </div>
                
                <div class="form-group">
                    <label>Использовать эту валюту для задания цен на товары в админке <a title="" data-toggle="tooltip" data-placement="bottom" href="#" data-original-title="Эта валюта будет использована для задания цен на товары в админке, на сайте будет отображена расчетная валюта"><i class="fa fa-info-circle"></i></a></label>
                    
                    <div class="btn-group noinline" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $currency_item->use_admin}active{/if}">
                        <input type="radio" name="use_admin" value="1" {if $currency_item->use_admin}checked{/if}> Да
                      </label>
                      <label class="btn btn-default4 off {if !$currency_item || !$currency_item->use_admin}active{/if}">
                        <input type="radio" name="use_admin" value="0" {if !$currency_item || !$currency_item->use_admin}checked{/if}> Нет
                      </label>
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label>Обновлять курс автоматически</label>
                    
                    <div class="btn-group noinline" data-toggle="buttons">
                      <label class="btn btn-default4 on {if $currency_item->auto_update}active{/if}">
                        <input type="radio" name="auto_update" value="1" {if $currency_item->auto_update}checked{/if}> Да
                      </label>
                      <label class="btn btn-default4 off {if !$currency_item || !$currency_item->auto_update}active{/if}">
                        <input type="radio" name="auto_update" value="0" {if !$currency_item || !$currency_item->auto_update}checked{/if}> Нет
                      </label>
                    </div>
                </div>

                
                
                
                
            </div>
        </div>

        <input type="hidden" name="id" value="{$currency_item->id}"/>
        <input type="hidden" name="session_id" value="{$smarty.session.id}"/>
        <input type="hidden" name="close_after_save" value="0"/>
    </fieldset>
</form>

<script type="text/javascript">

    $(document).ready(function(){
        {if $message_success}
            info_box($('#notice'), 'Изменения сохранены!', '');
        {/if}
    });

    $("form#currency a.save").click(function(){
        $(this).closest('form').submit();
        return false;
    });
    $("form#currency a.saveclose").click(function(){
        $(this).closest('form').find('input[name=close_after_save]').val(1);
        $(this).closest('form').submit();
        return false;
    });

    $('#update_currency_rate').click(function(){
        var iso = $('input[name=code]').val();
        if (iso.length == 0)
        {
            error_box($('#notice'), 'Код ISO не заполнен!', '');
            return false;
        }
        var href = '{$config->root_url}{$module->url}?mode=update_rate&ajax=1&iso='+iso;
        $.get(href, function(data) {
            if (data.success)
            {
                $('input[name=rate_to]').val(data.data);
                info_box($('#notice'), 'Обновление курса валюты выполнено!', '');
            }
        });
        return false;
    });
</script>